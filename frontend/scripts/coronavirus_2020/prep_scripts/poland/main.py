import requests
import json
import sys
import io
import csv
from bs4 import BeautifulSoup

# from https://github.com/znstrider/corona_germany/blob/master/corona_germany.ipynb

map_ = [
    ["Województwo", "Province"],
    ["Liczba", "Number"],
    ["Liczba zgonów", "Number of deaths"]
]


def get_covid_data():
    url = 'https://www.gov.pl/web/koronawirus/wykaz-zarazen-koronawirusem-sars-cov-2'
    request = requests.get(url)
    html = BeautifulSoup(request.text, 'html.parser')
    text = html.find(id="registerData").string
    register_data = json.loads(text)
    reader = csv.DictReader(io.StringIO(register_data['data']), delimiter=';')
    translated = []
    for region in list(reader):
        obj = {}
        for k, v in region.items():
            for pl, en in map_:
                if k == pl:
                    if pl == "Województwo":
                        v = v.upper()
                    obj[en] = v

        translated.append(obj)
    return translated


def pl_as_json(request):
    if request.method == 'OPTIONS':
        # Allows GET requests from any origin with the Content-Type
        # header and caches preflight response for an 3600s
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }

        return ('', 204, headers)

    # Set CORS headers for main requests
    headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
    }

    return (get_covid_data(), 200, headers)
