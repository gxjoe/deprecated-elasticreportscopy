import pandas as pd
import re
from pprint import pprint

# from https://github.com/znstrider/corona_germany/blob/master/corona_germany.ipynb


def get_covid_data():
    """VOVID-19 Fälle in Deutschland"""
    df = pd.read_html('https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_Poland',
                      thousands='.', decimal=',')[6][:-2:2]

    new_columns = []
    for col in df.columns[1:]:
        new_col = " ".join([col[1], col[0]])
        if '(' in new_col:
            new_col = re.sub(r'\([^)]*\)', '', new_col)
        new_columns.append(new_col)

    df.columns = df.columns.droplevel(0)
    df = df.set_index('Date (CET)')
    df.columns = new_columns

    for col in new_columns:
        df[col] = (df[col].astype('str')
                   .str.replace('—', '0', regex=False))

    return df


def de_as_json(request):
    if request.method == 'OPTIONS':
        # Allows GET requests from any origin with the Content-Type
        # header and caches preflight response for an 3600s
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }

        return ('', 204, headers)

    # Set CORS headers for main requests
    headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
    }

    return (get_covid_data().to_json(), 200, headers)


print(get_covid_data().to_json())
