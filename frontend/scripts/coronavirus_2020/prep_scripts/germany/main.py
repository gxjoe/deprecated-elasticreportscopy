# from https://github.com/znstrider/corona_germany/blob/master/corona_germany.ipynb


import pandas as pd
import re
import unicodedata

laender_dict = {'BW': 'Baden-Württemberg',
                'BY': 'Bayern',
                'BE': 'Berlin',
                'BB': 'Brandenburg',
                'HB': 'Bremen',
                'HH': 'Hamburg',
                'HE': 'Hessen',
                'MV': 'Mecklenburg-Vorpommern',
                'NI': 'Niedersachsen',
                'NW': 'Nordrhein-Westfalen',
                'RP': 'Rheinland-Pfalz',
                'SL': 'Saarland',
                'SN': 'Sachsen',
                'ST': 'Sachsen-Anhalt',
                'SH': 'Schleswig-Holstein',
                'TH': 'Thüringen'}

# from https://github.com/znstrider/corona_germany/blob/master/corona_germany.ipynb


def get_covid_data():
    """COVID-19 Fälle in Deutschland"""
    # old webpage
    # df = pd.read_html('https://de.wikipedia.org/wiki/COVID-19-F%C3%A4lle_in_Deutschland',
    #              thousands='.', decimal = ',')[3][:-2:2]
    # new page from March 17
    # From March 17 on, RKI reports electronically submitted cases to the RKI
    # which can deviate from the cases reported on state level
    # dfs = pd.read_html('https://de.wikipedia.org/wiki/COVID-19-Pandemie_in_Deutschland',
    #              thousands='.', decimal = ',')
    # new page from March 19:
    dfs = pd.read_html('https://de.wikipedia.org/wiki/COVID-19-Pandemie_in_Deutschland',
                       thousands='.', decimal=',')

    df = dfs[2].set_index('Datum').iloc[:, :-2].rename(columns=laender_dict).T
    df.columns = [col.replace(' 2020', '') for col in df.columns]

    # df = dfs[2][:-2:2]
    # dfs[19][:-2:2]
    # df = dfs[2].set_index('Datum').iloc[:, :-2].rename(columns=laender_dict).T
    # df2 = dfs[3][:-2:2]

    def clean_df(dataframe):
        new_columns = []
        del dataframe['Datum']
        for col in dataframe.columns:
            if '(' in col:
                new_col = unicodedata.normalize("NFKD", col.split('(')[0])
                new_columns.append(new_col)
            else:
                new_columns.append(col)

        dataframe.columns = new_columns

        for col in new_columns:
            dataframe[col] = (dataframe[col].astype('str')
                              .str.replace('—', '0', regex=False).str.strip()
                              .astype('float')
                              .astype('int'))
        return dataframe

    df = clean_df(df)

    return df


def de_as_json(request):
    if request.method == 'OPTIONS':
        # Allows GET requests from any origin with the Content-Type
        # header and caches preflight response for an 3600s
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }

        return ('', 204, headers)

    # Set CORS headers for main requests
    headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
    }

    return (get_covid_data().to_json(), 200, headers)


print(get_covid_data().to_json())
