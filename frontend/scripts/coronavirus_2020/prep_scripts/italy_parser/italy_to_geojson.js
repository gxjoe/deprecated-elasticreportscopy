var fs = require("fs");
var turf = require("@turf/turf");
var moment = require("moment");

let _data = "";

fs.createReadStream("./dpc-covid19-ita-province.json", {
    encoding: "utf-8"
  })
  .on("data", data => {
    _data += data;
  })
  .on("end", () => {
    const data = JSON.parse(_data);

    const italian_keys = [
      "data",
      "stato",
      "codice_regione",
      "denominazione_regione",
      "codice_provincia",
      "denominazione_provincia",
      "sigla_provincia",
      "lat",
      "long",
      "totale_casi"
    ];
    const en_keys = [
      "date",
      "status",
      "region_code",
      "denomination_region",
      "province_code",
      "denomination_provincia",
      "province_code",
      "lat",
      "long",
      "total_cases"
    ];

    const grouped_by_latlong = {};
    const sortByDateDesc = (a, b) => {
      const a_date = moment(a.date, "YYYY-MM-DD HH:mm:ss");
      const b_date = moment(b.date, "YYYY-MM-DD HH:mm:ss");
      return -1 * (a_date.unix() < b_date.unix() ? -1 : a_date.unix() > b_date.unix() ? 1 : 0);
    };

    const cleaned = data
      .map(item => {
        const translated = {};
        Object.keys(item).map((ita_key, ita_key_index) => {
          translated[en_keys[ita_key_index]] = item[ita_key];
        });
        return translated;
      })
      .filter(item => item.lat !== 0 && item.long !== 0)
      .sort(sortByDateDesc)
      .map(item => {
        const latlong_hash = `${item.lat},${item.long}`;
        if (latlong_hash in grouped_by_latlong) {
          grouped_by_latlong[latlong_hash].push(item);
        } else {
          grouped_by_latlong[latlong_hash] = [item];
        }
      });


    const collection = turf.featureCollection([]);

    Object.keys(grouped_by_latlong).map(hash => {
      let group = grouped_by_latlong[hash];
      const latest = group[0];
      group = group.slice(1, group.length);

      collection.features.push(turf.point(
        [latest.long, latest.lat], {
          latest_case: {
            ...latest
          },
          previous_cases: group
        }
      ));
    })

    console.info(collection);

    fs.writeFile("italian_collection.json", JSON.stringify(collection), function (err) {
      if (err) {
        return console.log(err);
      }

      console.log("The file was saved!");
    });
  });
