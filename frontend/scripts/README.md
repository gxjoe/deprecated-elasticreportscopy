1. Load Excel To Google Sheets: example https://docs.google.com/spreadsheets/d/1VN-KUWmkMdQjrQx96Xpax2uzZWTLV771M-SOdOzPpeU/edit?usp=sharing
2. Convert to JSON: example tool http://convertcsv.com/csv-to-json.htm and save as `data/industry_categories.json`
3. `node parseIndustryCategories.json`
4. Copy data from generated file and replace `LobAndIndustryBasedBusinessActivityBreakdown`