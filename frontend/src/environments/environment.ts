import { environment as base_env } from './environment.prod';

const host = 'https://er-azure.uniqa-international.eu';

const elastic_host = `${host}/es`;
const auth_host = `${host}/auth/CBNWebServices`;

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = Object.assign({}, base_env, {
  production: false,
  elasticsearch: {
    ...base_env.elasticsearch,
    host: elastic_host,
  },
  cbn_authentication: {
    host: auth_host
  }
});

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
