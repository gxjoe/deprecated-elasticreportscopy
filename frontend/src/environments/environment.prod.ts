
const host = 'https://er-azure.uniqa-international.eu'; // 'https://elastic-report-test.uniqa-cluster.eu';

const elastic_host = `${host}/es`;
const auth_host = `${host}/auth/CBNWebServices`;

export const environment = {
  production: true,
  gmaps_api_key:
    'AIzaSyDQ7cwQF6G0768swdMxD-pYIwKAzPWa4II&libraries=visualization,geometry,drawing,places&v=quarterly',
  gmaps_settings: {
    center: {
      lat: 48.2082,
      lng: 16.3738
    },
    zoom: 5,
    zoom_to_show_individual_markers: 8,

    carto: {
      CARTO_ACCESS_TOKEN: 'eyJhbGciOiJIUzI1NiJ9.eyJhIjoiYWNfMXJoZXoyNGsiLCJqdGkiOiI4NGUzNzcyNyJ9.lSlMX34HxLeY_CHaVtQvpWVnPtfB9vvlQOaLfgFq1ao',
      CARTO_API_BASE_URL: 'https://gcp-europe-west1.api.carto.com',
    },

    GEAM_PROXY_URL_BASE: 'https://geam-v2.uniqa-cluster.eu',

    catnet: {
      CATNET_SUBSCRIPTION_KEY: '47008165e429416a97b0dbb9477f2803',
      CATNET_BASE_API_URL: 'https://swissre.azure-api.net/wms/v2/rest/services/'
    },

    COLUMBIA_WMS_BASE_URL: 'https://sedac.ciesin.columbia.edu/geoserver/wms',

    drawingColors: {
      strokeColor: '#FF0000',
      strokeOpacity: 1,
      strokeWeight: 2.5,
      fillColor: '#AAA',
      fillOpacity: 0.09,
      editable: false,
      draggable: false
    },

    mapStyledArrName: 'Map',
    mapStyledArrNamewithRailroads: 'MapStyledArrNamewithRailroads',

    mapStyledArr: [
      {
        featureType: 'administrative',
        elementType: 'all',
        stylers: [
          {
            visibility: 'on'
          },
          {
            saturation: -100
          },
          {
            lightness: 20
          }
        ]
      },
      {
        featureType: 'road',
        elementType: 'all',
        stylers: [
          {
            visibility: 'on'
          },
          {
            saturation: -100
          },
          {
            lightness: 40
          }
        ]
      },
      {
        featureType: 'water',
        elementType: 'all',
        stylers: [
          {
            visibility: 'on'
          },
          {
            saturation: -10
          },
          {
            lightness: 30
          }
        ]
      },
      {
        featureType: 'landscape.man_made',
        elementType: 'all',
        stylers: [
          {
            visibility: 'simplified'
          },
          {
            saturation: -60
          },
          {
            lightness: 10
          }
        ]
      },
      {
        featureType: 'landscape.natural',
        elementType: 'all',
        stylers: [
          {
            visibility: 'simplified'
          },
          {
            saturation: -60
          },
          {
            lightness: 60
          }
        ]
      },
      {
        featureType: 'poi',
        elementType: 'all',
        stylers: [
          {
            visibility: 'off'
          },
          {
            saturation: -100
          },
          {
            lightness: 60
          }
        ]
      },
      {
        featureType: 'transit',
        elementType: 'all',
        stylers: [
          {
            visibility: 'off'
          },
          {
            saturation: -100
          },
          {
            lightness: 60
          }
        ]
      }
    ]
  },
  elasticsearch: {
    host: elastic_host,
    username: 'user',
    password: 'user',
    index_name: 'prod_data',
    address_based_index_name: 'prod_addresses'
  },
  cbn_authentication: {
    host: auth_host
  },

  memcached_wrapper: {
    endpoint: '' // auth_host + '/co/memcached_wrapper'
  },
  log_queries: false,

  mapbox: {
    accessToken:
      'pk.eyJ1Ijoianp6ZnMiLCJhIjoiY2swY2k3Z3ExMTI3NzNubnZ2cHRoM3JkMiJ9.WFzGLnrz2FsQOa75cCkgUg'
  },

  defaultCbnToken: 'L8Rdsun6wFZYrKqB3EtEHyrOySZpY4c2'
};
