/// <reference types="@types/googlemaps" />

import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  AfterViewInit,
  ChangeDetectionStrategy,
} from "@angular/core";

import { SearchResponse } from "elasticsearch";

import { environment } from "../../environments/environment";
import { GoogleMapsService } from "../services/gmaps-service.service";
import { ElasticService } from "../services/elastic.service";
import { MAP_KLASSES } from "../elastic/queries";
import { bucketsToHeatmapData, groupGeoHits } from "../elastic/utils";
import { GeojsonService } from "../services/geojson.service";
import { DataMarkerService } from "../services/data-marker.service";
import { InfowindowService } from "../services/infowindow.service";
import {
  transformJSON2html,
  prepareOfferJSONEntry,
  prepareClientJSONEntry,
} from "../utils/helpers/json2html";
import { generatePolylineOptions } from "../utils/helpers/gmaps";
import { MapdrawingService } from "../services/mapdrawing.service";
import { BehaviorSubject, Observable } from "rxjs";
import { CsvService } from "../services/csv.service";
import { rowMapToCSV } from "../charts/histogram-table-chart/common";
import { buildDeckOverlay, DECK_OVERLAYS } from "../utils/helpers/deck.gl";
import { Hotspots } from "../map/hotspot/hotspot.component";

@Component({
  selector: "app-gmap",
  templateUrl: "./gmap.component.html",
  styleUrls: ["./gmap.component.scss"],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GmapComponent implements OnInit, AfterViewInit {
  @Input()
  klass: MAP_KLASSES;

  @Input() activeKey: Observable<string>;

  @ViewChild("autofill", { static: false }) autofill: ElementRef;

  @ViewChild("circle", { static: false }) circle: any;
  @ViewChild("polygon", { static: false }) polygon: any;
  @ViewChild("trash", { static: false }) trash: any;
  @ViewChild("done", { static: false }) done: any;

  private native_map: google.maps.Map | any;
  center: google.maps.LatLngLiteral = environment.gmaps_settings.center;
  zoom = environment.gmaps_settings.zoom;

  current_heatmap: google.maps.visualization.HeatmapLayer | null;
  public clusters: Array<any>;

  public preselected_lobs = [];

  table_csv = "0;0";

  canFilterByAddressIDs = false;
  shouldFilterByAddressIDs = false;
  onlyShowFocusedHotspot = this.isSomeHotspotFocused();

  hotspots$: BehaviorSubject<Hotspots> = new BehaviorSubject([]);

  isLoading = false;

  constructor(
    public mapService: GoogleMapsService,
    public es: ElasticService,
    private geo: GeojsonService,
    private dm: DataMarkerService,
    private infowin: InfowindowService,
    private mdraw: MapdrawingService,
    private cdr: ChangeDetectorRef,
    private csvService: CsvService
  ) {
    this.es.filterByAddressIDs.subscribe((address_ids) => {
      this.canFilterByAddressIDs = Boolean(address_ids.length);
    });

    this.es.shouldFilterByAddressIDs.subscribe((should) => {
      this.shouldFilterByAddressIDs = should;
    });
  }

  shouldFilterByAddressIDsChanged($event: boolean) {
    this.es.shouldFilterByAddressIDs.next($event);
  }

  onHotspotDivsChanged($event: Hotspots) {
    this.hotspots$.next($event);
  }

  isSomeHotspotFocused() {
    return this.hotspots$?.getValue()?.some((h) => h.selected);
  }

  ngOnInit() {
    this.es.app_component.setComponent(this.klass, this);
    // if (this.klass === MAP_KLASSES.OfferDistribution) {
    this.es.activeMapComponent = this;
    // }

    this.mapService.containerLoadingObservable.subscribe((evt) => {
      this.toggleLoading(evt);
    });
  }

  ngAfterViewInit() {
    try {
      this.cdr.detectChanges();
    } catch (err) { }
  }

  public syncZoomAndCenter(m: google.maps.Map) {
    this.map.setZoom(m.getZoom());
    this.map.setCenter(m.getCenter());
  }

  private unsetAllActiveDataLayers() {
    try {
      this.geo.unsetActivePolylines();
      this.dm.unsetActiveDataMarkers();
    } catch (err) {
      console.warn(err);
    }
  }

  onMapReady($event: google.maps.Map) {
    const self = this;
    this.native_map = $event;
    this.mapService.setMap(this.native_map);
    this.setMapDataStyle();

    $event.setOptions({
      scrollwheel: false,
      streetViewControl: true,
      zoom: environment.gmaps_settings.zoom,

      rotateControl: true,
      rotateControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER,
      },
      tilt: 45,
      heading: 90,
      disableDefaultUI: true,

      mapTypeControl: true,
      mapTypeControlOptions: {
        position: google.maps.ControlPosition.TOP_RIGHT,
        mapTypeIds: [
          environment.gmaps_settings.mapStyledArrName,
          google.maps.MapTypeId.HYBRID,
          google.maps.MapTypeId.SATELLITE,
        ],
      },
    });

    (window as any).MAP = $event;

    const mapTypeArr = new google.maps.StyledMapType(
      environment.gmaps_settings.mapStyledArr as any,
      {
        name: environment.gmaps_settings.mapStyledArrName,
      }
    );
    $event.mapTypes.set(
      environment.gmaps_settings.mapStyledArrName,
      mapTypeArr
    );

    const railroadTypeArr = new google.maps.StyledMapType(
      [].concat(environment.gmaps_settings.mapStyledArr).concat(
        {
          featureType: "transit",
          elementType: "all",
          stylers: [
            {
              visibility: "on",
            },
            {
              saturation: 0,
            },
            {
              lightness: 0,
            },
          ],
        },
        {
          featureType: "transit.line",
          elementType: "geometry.fill",
          stylers: [
            {
              color: "#000000",
            },
            {
              weight: 3,
            },
          ],
        },
        {
          featureType: "poi",
          elementType: "labels",
          stylers: [
            {
              visibility: "off",
            },
          ],
        }
      ) as any,
      {
        name: environment.gmaps_settings.mapStyledArrNamewithRailroads,
      }
    );

    $event.mapTypes.set(
      environment.gmaps_settings.mapStyledArrNamewithRailroads,
      railroadTypeArr
    );

    $event.setMapTypeId(environment.gmaps_settings.mapStyledArrName);

    google.maps.event.addListener($event, "zoom_changed", function () {
      if (
        $event.getZoom() >
        environment.gmaps_settings.zoom_to_show_individual_markers
      ) {
        self.preselected_lobs = [];
      }
    });

    google.maps.event.addListener($event, "click", function () {
      self.unsetAllActiveDataLayers();

      self.preselected_lobs = [];
    });

    // wait up to 2sec for gmaps to fully load
    setTimeout(() => {
      self.initOnViewportChanged();
      self.initAutofill();

      self.initMapDrawing();
    }, 2e3);
  }

  refreshMap() {
    this.es.search(this.klass);
  }

  initMapDrawing() {
    this.mdraw.handleMapDrawings(
      this.circle.elementRef.nativeElement,
      this.polygon.elementRef.nativeElement,
      this.trash.elementRef.nativeElement,
      this.done.elementRef.nativeElement,
      this.es.activeMapComponent.map
    );
  }

  initAutofill() {
    const self = this;

    const autocomplete = new google.maps.places.Autocomplete(
      this.autofill.nativeElement
    );
    autocomplete.addListener("place_changed", function () {
      const place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        self.map.fitBounds(place.geometry.viewport);
      } else {
        self.map.setCenter(place.geometry.location);
        self.map.setZoom(17); // Why 17? Because it looks good.
      }
    });
  }

  initOnViewportChanged() {
    // const self = this;
    // google.maps.event.addListener(this.native_map, 'tilesloaded', function() {
    //   google.maps.event.addDomListenerOnce(self.native_map, 'idle', () => {
    //     // self.es.search(self.klass);
    //   });
    // });
  }

  get self(): GmapComponent {
    return this;
  }

  get map(): google.maps.Map {
    return this.native_map;
  }

  get heatMap(): google.maps.Map {
    return this.klass === MAP_KLASSES.heatmap ? this.native_map : null;
  }

  get clusterMap(): google.maps.Map {
    return this.klass === MAP_KLASSES.clustermap ? this.native_map : null;
  }

  setMapDataStyle() {
    // Style the GeoJSON features (stations & lines)

    this.map.data.setStyle((feature) => {
      const point = feature.getProperty("icon");
      // Stations have line property, while lines do not.
      if (point) {
        return {
          icon: feature.getProperty("icon"),
        };
      }

      return {
        ...generatePolylineOptions(feature.getProperty("color")),
      };
    });
  }

  private __addObjectListeners(
    id: string,
    instance: google.maps.Marker,
    trigger_key: string
  ) {
    const self = this;
    const ids = Array.from(
      new Set(
        (self.dm.getByPathAndId("grouped_by.lob", id) as any).map(
          (l) => l.properties.ids.lob
        )
      )
    );

    if (instance instanceof google.maps.Marker) {
      google.maps.event.addListener(instance, "click", function () {
        self.preselected_lobs = Array.from(new Set(ids));

        try {
          // self.dm.setActiveDataMarkers(ids);
          // self.geo.setActivePolylines(ids);
        } catch (err) {
          console.warn(err);
        }

        switch (self.klass) {
          case MAP_KLASSES.OfferDistribution:
            self.infowin.setContentAndShow(
              transformJSON2html(
                self.dm.toGeoJSON()[trigger_key],
                prepareOfferJSONEntry
              ),
              instance,
              function () {
                self.unsetAllActiveDataLayers();
                self.preselected_lobs = [];
              }
            );
            break;
          case MAP_KLASSES.ClientDistribution:
            self.infowin.setContentAndShow(
              transformJSON2html(
                self.dm.toGeoJSON()[trigger_key],
                prepareClientJSONEntry
              ),
              instance,
              function () {
                self.unsetAllActiveDataLayers();
                self.preselected_lobs = [];
              }
            );
            break;
        }
      });
    }
  }

  handleHeatmap(
    resp: SearchResponse<any>,
    layer?: google.maps.visualization.HeatmapLayer
  ): void {
    if (this.current_heatmap) {
      this.current_heatmap.setMap(null);
    }

    this.geo.unsetPolylines();
    this.dm.unsetDataMarkers();

    this.csvService._current_csv_value_shareable.next(" ");
    this.csvService._underlying_data_shareable.next(" ");

    if (resp.hits.hits.length <= 0) {
      if (resp.aggregations) {
        layer = bucketsToHeatmapData(
          resp.aggregations.filter_agg.aggs.distribution.buckets
        );
        layer.setMap(this.map);
        this.current_heatmap = layer;
      } else {
        this.geo.unsetPolylines();
        this.dm.unsetDataMarkers();
        this.csvService._current_csv_value_shareable.next(" ");
        this.csvService._underlying_data_shareable.next(" ");
      }
    } else {
      if (this.klass.includes("Experimental")) {
        const overlay = buildDeckOverlay(
          DECK_OVERLAYS.TSI_HEXAGON,
          resp.hits.hits
        );
        console.info(overlay);
        overlay.setMap(this.map);
        return;
      }

      const { collection, polylines } = groupGeoHits(
        { mapKlasses: MAP_KLASSES, current: this.klass },
        resp,
        this.map.getBounds(),
        this.mapService.hotspotModeActive$.value
      );

      // const combined = this.geo.mergeCollections(collection, lineStringCollection);

      this.geo.setPolylines(polylines);
      const { header, rows } = this.dm.setDataMarkers(collection as any);

      const nested_address_json = resp.hits.hits.map((hit) => hit._source);
      const flattened_address_csv =
        this.csvService.nestedJSONtoCSV(nested_address_json);
      this.csvService._underlying_data_shareable.next(flattened_address_csv);

      const table_csv = rowMapToCSV(header, rows);
      this.csvService._current_csv_value_shareable.next(table_csv);

      const all_keys = Object.keys(this.dm.all);

      switch (this.klass) {
        case MAP_KLASSES.OfferDistribution:
          for (let ind = 0; ind < all_keys.length; ind++) {
            const key = all_keys[ind];
            const lob_key = this.dm.toGeoJSON()[key].properties.ids.lob;
            this.__addObjectListeners(lob_key, this.dm.all[key], key);
          }
          if (this.preselected_lobs.length) {
            this.dm.setActiveDataMarkers(this.preselected_lobs);
            this.geo.setActivePolylines(this.preselected_lobs);
          }
          break;
        case MAP_KLASSES.ClientDistribution:
          for (let ind = 0; ind < all_keys.length; ind++) {
            const key = all_keys[ind];
            const lob_key = this.dm.toGeoJSON()[key].properties.ids.lob;
            this.__addObjectListeners(lob_key, this.dm.all[key], key);
          }
          break;
      }
    }
  }

  handleClusters(list: Array<Object>) {
    this.clusters = list;
  }

  toggleLoading(is_loading) {
    this.isLoading = is_loading;
  }
}
