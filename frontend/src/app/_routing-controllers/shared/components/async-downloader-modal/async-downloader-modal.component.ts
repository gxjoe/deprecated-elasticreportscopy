import { Component, OnInit } from '@angular/core';
import { CsvService } from 'src/app/services/csv.service';
import { DivDownloaderService } from 'src/app/services/div-downloader.service';
import { ElasticService } from 'src/app/services/elastic.service';
import { SearchResponse } from 'elasticsearch';
import { FilterComponent } from 'src/app/utils/higher-order/FilterComponent';
import { QueryService } from 'src/app/services/query.service';
import { buildQueries } from 'src/app/elastic/queries';
import { currencyResolver } from 'src/app/elastic/constants';
import { CURRENCY_PREFIX_ISO, chunkArray, flattenArrays, uniq } from 'src/app/utils/helpers/helpers';
import { BehaviorSubject, distinctUntilChanged, forkJoin, map, take } from 'rxjs';
import { LobDoc } from '../../../../interfaces/elastic';
import { dynamicSort } from '../../../../charts/utils';
import { ElasticMappingService } from 'src/app/services/elastic-mapping.service';
import { dotNotationToHumanReadable } from 'src/app/utils/helpers/string';

type checkBoxModel = Array<{ label: string, value: string, checked: boolean, disabled?: boolean }>;

@Component({
  selector: 'app-async-downloader-modal',
  templateUrl: './async-downloader-modal.component.html',
  styleUrls: ['./async-downloader-modal.component.scss'],
})
export class AsyncDownloaderModalComponent implements OnInit {

  isVisible = false;
  isDownloading = false;
  hasFailed = false;

  // UDGB-56
  filterColumnNames = '';

  allColumnsChecked = false;
  indeterminateColumns = false;
  allProps: string[]
  csvColumnsModel: checkBoxModel;

  sizeList = [
    { row_size: String(100), eta: 'miliseconds' },
    { row_size: String(1e3), eta: 'seconds' },
    { row_size: String(10e3), eta: 'minutes' },
    { row_size: String(100e3), eta: 'couple minutes' },
  ];
  size = String(1e3);

  export_name = this.getExportName();

  search$ = new BehaviorSubject<string>('')

  csvColumnsModel$ = this.search$.asObservable().pipe(
    map((search) => this.csvColumnsModel.filter(({ label }) => label.toLowerCase().includes(search.toLowerCase()))),
    distinctUntilChanged((a, b) => JSON.stringify(a) === JSON.stringify(b))
  )

  constructor(
    private csvs: CsvService,
    private div: DivDownloaderService,
    private es: ElasticService,
    private queryService: QueryService,
    private esMapping: ElasticMappingService
  ) {
  }

  ngOnInit(): void {
    this.setCsvColumnsModel();
  }

  setCsvColumnsModel() {
    this.esMapping.getMappingProps(4)
      .pipe(take(1))
      .subscribe((props) => {
        const headers = uniq(props.map((p) => p.split(".")[0]))

        const columnEntries = headers.reduce((acc, curr) => ({
          ...acc,
          [curr]: props.filter((p) => p.includes(curr)).map((p) => ([p, dotNotationToHumanReadable(p)]))
        }), {})

        this.csvColumnsModel = flattenArrays(Object.entries(columnEntries)
          .map(([_, arr_group]) => {
            return (arr_group as any).map(
              ([column_value, column_name]) => {
                return {
                  label: column_name,
                  value: column_value,
                  checked: column_name.includes('address') ? false : true,
                  disabled: this.getMandatoryFields().some((field) => field === column_value),
                };
              });
          }));

      });
  }

  getMandatoryFields(): string[] {
    return ['offer.number', 'offer.client.name', 'lob.id', `lob.premium_${CURRENCY_PREFIX_ISO()}`, `lob.totalSumInsured_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`]
  }

  getExportName() {
    const now = new Date().toISOString().replace("T", "_").replace(":", "-").substring(0, 16);
    return `${now}_Elastic-Export`;
  }

  showModal(): void {
    this.isVisible = true;
  }

  handleDownload(): void {
    this.hasFailed = false;
    this.isDownloading = true;

    // parse because the radio btn value
    // would always be a string
    const size = parseInt(this.size, 10);

    // don't use es.search(query_name) for fuller control
    const filterComponentMetaQueries = this.es.app_component.filterComponents.map(
      (component: FilterComponent) => {
        return component.filterQueryBuilder();
      },
    );

    const query_obj = this.queryService.construct(
      'TableExploration',
      null,
      this.es.app_component,
      true,
      filterComponentMetaQueries,
      buildQueries('TableExploration')['TableExploration'],
    );

    const fields_included_in_source = this.prepareSourceParam();
    query_obj.query._source = fields_included_in_source;

    const query: any = currencyResolver(query_obj.query, false);

    const num_array_of_size = [].rangeOfNumbers(0, size - 1, 1);
    const chunk_size = size <= 2e3 ? size : 2e3;
    const chunked_arr = chunkArray(num_array_of_size, chunk_size);

    const size_and_from_params = chunked_arr.map(chunk => {
      const first = chunk[0];
      return {
        size_param: chunk_size,
        from_param: first,
      };
    });

    console.debug({ chunked_arr, size_and_from_params });

    const request_observables = size_and_from_params.map(({ from_param }) => {
      return this.es.POST(
        {
          ...query,
          size: chunk_size,
          from: from_param,
        },
        null,
        `TableExploration${Math.random() * 1e6}`,
      );
    });

    forkJoin(request_observables).subscribe(
      (
        responseList: Array<{
          cached: boolean;
          code: number;
          query_key: string;
          response: SearchResponse<any>;
          success: boolean;
        }>,
      ) => {
        let hits: LobDoc[] = [];

        responseList.map(r => {
          hits = hits.concat((
            (r.response
              // if coming from memcache
              ? r.response
              // if coming directly from ES
              : r) as any).hits.hits.map(hit => hit._source));
        });

        console.debug('hits.length', hits.length);

        // sort the ESG criteria in-place (https://github.com/sudolabs-io/uniqa-elastic-reports/issues/5)
        // if they're included in the response
        if (fields_included_in_source?.some(field => ['offer.client', 'offer.client.esg'].includes(field))) {
          hits.forEach((hit, i) => {
            const applicableCriteria = hit.offer?.client?.esg?.criteria;

            if (applicableCriteria?.length) {
              hits[i].offer.client.esg.criteria = applicableCriteria.sort(
                // Alphabetical sort ascending. We're assuming the length of the ESG criteria is
                // constant, thus ensuring the Excel columns' comparability.
                dynamicSort('shortName'),
              );
            }
          });
        }

        const csv_rows = this.csvs.nestedJSONtoCSV(hits, { columnsWithOrderPriority: this.getMandatoryFields() }, fields_included_in_source);

        console.debug('csv rows', { csv_rows });
        this.div.downloadNestedCSV(null, this.export_name, csv_rows);

        this.isVisible = false;
        this.isDownloading = false;
      },
      err => {
        this.hasFailed = true;
        this.isDownloading = false;
        console.error(err, { err });
      },
    );
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  updateAllCheckedcCsvColumns(): void {
    this.indeterminateColumns = false;
    if (this.allColumnsChecked) {
      this.csvColumnsModel = this.csvColumnsModel.map(item => {
        return {
          ...item,
          checked: item.disabled ? item.checked : true,
        };
      });
    } else {
      this.csvColumnsModel = this.csvColumnsModel.map(item => {
        return {
          ...item,
          checked: item.disabled ? item.checked : false,
        };
      });
    }
    this.search$.next(this.search$.getValue())
  }

  updateSingleCheckedCsvColumns(newModel: checkBoxModel): void {
    this.csvColumnsModel = this.csvColumnsModel.map((modelItem) => {
      const newModelItem = newModel.find(({ value }) => value === modelItem.value)
      return newModelItem || modelItem
    })
    if (this.csvColumnsModel.every(item => !item.checked)) {
      this.allColumnsChecked = false;
      this.indeterminateColumns = false;
    } else if (this.csvColumnsModel.every(item => item.checked)) {
      this.allColumnsChecked = true;
      this.indeterminateColumns = false;
    } else {
      this.indeterminateColumns = true;
    }
  }

  prepareSourceParam() {
    return this.csvColumnsModel.filter(({ checked }) => checked === true).flatMap(({ value }) => {
      if (value.includes('*')) {
        return this.esMapping.getChildrenFromAsteriskProp(value);
      }
      return [value];
    });
  }
}
