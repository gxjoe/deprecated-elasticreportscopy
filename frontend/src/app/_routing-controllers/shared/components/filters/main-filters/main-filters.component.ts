import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { DashboardEnums } from 'src/app/utils/constants/dashboardEnums';

@Component({
  selector: 'app-main-filters',
  templateUrl: './main-filters.component.html',
  styleUrls: ['./main-filters.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MainFiltersComponent implements OnInit {
  constructor() {}

  dashboardTypes = DashboardEnums;

  @Input() dashboardKind: string;

  ngOnInit() {}
}
