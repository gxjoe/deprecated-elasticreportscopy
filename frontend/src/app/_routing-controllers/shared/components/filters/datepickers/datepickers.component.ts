import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ElasticService } from 'src/app/services/elastic.service';

@Component({
  selector: 'app-datepickers',
  templateUrl: './datepickers.component.html',
  styleUrls: ['./datepickers.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatepickersAndSlidersComponent implements OnInit {
  constructor(private es: ElasticService) { }
  slidersVisiblity = false;

  ngOnInit() { }

  toggleSlidersVisibility($event) {
    this.slidersVisiblity = $event;

    if (this.slidersVisiblity) {
      this.es.readyForRefresh();
    }
  }
}
