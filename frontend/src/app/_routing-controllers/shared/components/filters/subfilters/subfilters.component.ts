import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ElasticService } from 'src/app/services/elastic.service';

@Component({
  selector: 'app-subfilters',
  templateUrl: './subfilters.component.html',
  styleUrls: ['./subfilters.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SubfiltersComponent implements OnInit {
  expanderBtnTitle = 'Show more filters';
  buildingSlidersVisiblity = false;
  currentYear = new Date().getFullYear();

  constructor(private es: ElasticService) {}

  areFiltersExpanded() {
    return this.expanderBtnTitle.includes('fewer');
  }

  ngOnInit() {}

  toggleFilters() {
    this.expanderBtnTitle = this.areFiltersExpanded() ? 'Show more filters' : 'Show fewer filters';
  }

  toggleSlidersVisibility($event) {
    this.buildingSlidersVisiblity = $event;

    if (this.buildingSlidersVisiblity) {
      this.es.readyForRefresh();
    }
  }
}
