import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LISTS_OF_HISTOGRAM_CATEGORIES } from 'src/app/charts/histogram-table-chart/histogramListsAndCategories';
import { ElasticService } from 'src/app/services/elastic.service';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class CurrencyComponent implements OnInit {
  listOfOptions: Array<{
    value: string;
    label: string;
  }>;
  listOfSelectedValues: Array<{
    value: string;
    label?: string;
  }> = [];

  constructor(private es: ElasticService) { }

  get globallySelectedISOcurrency() {
    return (window as any).SELECTED_ISO_CURRENCY;
  }

  ngOnInit() {
    this.listOfOptions = LISTS_OF_HISTOGRAM_CATEGORIES.listOfCurrencies.map(currency => {
      return {
        value: currency,
        label: currency
      };
    });

    this.listOfSelectedValues = [
      {
        label: this.globallySelectedISOcurrency || 'EUR',
        value: this.globallySelectedISOcurrency || 'EUR'
      }
    ];
  }

  onChange($event) {
    (window as any).SELECTED_ISO_CURRENCY = $event;
    this.es.readyForRefresh();
  }
}
