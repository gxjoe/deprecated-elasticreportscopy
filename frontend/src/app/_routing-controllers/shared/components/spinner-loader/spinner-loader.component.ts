import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-spinner-loader',
  templateUrl: './spinner-loader.component.html',
  styleUrls: ['./spinner-loader.component.scss']
})
export class SpinnerLoaderComponent implements OnInit {
  @Input() color: string;
  @Input() minHeight: string;

  constructor() { }

  ngOnInit(): void {
  }

}
