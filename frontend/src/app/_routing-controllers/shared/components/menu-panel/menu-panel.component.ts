import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { ModulesToMenuPanelParams } from 'src/app/utils/constants/dashboardEnums';
import { sortArrayByKey } from 'src/app/charts/utils';
import { AuthService } from 'src/app/services/auth.service';
import packageJson from '../../../../../../package.json';

@Component({
  selector: 'app-menu-panel',
  templateUrl: './menu-panel.component.html',
  styleUrls: ['./menu-panel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuPanelComponent implements OnInit {
  constructor(private auth: AuthService) { }

  @Input()
  active_item;

  menu_items = [];
  logoutTitle = '';
  version = packageJson.version

  logout($event: MouseEvent) {
    $event.preventDefault();

    this.auth.logout();
    window.location.reload();
  }

  get _menuEnumToSymbol() {
    return ModulesToMenuPanelParams;
  }

  ngOnInit() {
    this.auth.doCheckAuth().then(parsed_auth => {
      this.logoutTitle = `Logout ${parsed_auth.user}`;
      this.menu_items = sortArrayByKey(this._menuEnumToSymbol, 'key')
        .filter(menu_group => {
          return [
            'RIM',
            ...parsed_auth.sections, 'ESG'].some(
              section => section === menu_group.shortened
            );
        })
        .map(menu_group => {
          if (menu_group.key === this.active_item) {
            menu_group = {
              active: true,
              ...menu_group
            };
          }
          return menu_group;
        });
    });

  }
}
