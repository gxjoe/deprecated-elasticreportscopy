import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-collapse-all-panels-btn',
  templateUrl: './collapse-all-panels-btn.component.html',
  styleUrls: ['./collapse-all-panels-btn.component.scss']
})
export class CollapseAllPanelsBtnComponent implements OnInit {
  @Output() doClosePanelsEvent = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  closeParentPanels() {
    this.doClosePanelsEvent.emit(true);
  }
}
