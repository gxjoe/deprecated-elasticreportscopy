import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-back-to-top',
  templateUrl: './back-to-top.component.html',
  styleUrls: ['./back-to-top.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BackToTopComponent implements OnInit {
  hint = 'Scroll to the top';

  constructor() { }

  ngOnInit(): void {
  }

}
