import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { GoogleMapsModule } from '@angular/google-maps';

import { MenuPanelComponent } from './components/menu-panel/menu-panel.component';
import { AsyncDownloaderModalComponent } from './components/async-downloader-modal/async-downloader-modal.component';
import { CollapseAllPanelsBtnComponent } from './components/collapse-all-panels-btn/collapse-all-panels-btn.component';

import { GmapComponent } from '../../gmap/gmap.component';
import { TriggerComponent } from '../../trigger/trigger.component';
import { MarkerClusterComponent } from '../../marker-cluster/marker-cluster.component';
import { RelativeDatepickerComponent } from '../../relative-datepicker/relative-datepicker.component';
import { PiechartComponent } from '../../charts/piechart/piechart.component';
import { VerticalBarChart } from '../../charts/vertical-bar-chart/vertical-bar-chart.component';
import { HorizontalBarChart } from '../../charts/horizontal-bar-chart/horizontal-bar-chart.component';
import { LineChart } from '../../charts/line-chart/line-chart.component';
import { CheckboxComponent } from '../../checkbox/checkbox.component';
import { MultiselectComponent } from '../../multiselect/multiselect.component';
import { SliderComponent } from '../../slider/slider.component';
import { TextCardComponent } from '../../text-card/text-card.component';
import { TableComponent } from '../../table/table.component';
import { HistogramTableChartComponent } from '../../charts/histogram-table-chart/histogram-table-chart.component';

import { CounterPipe } from '../../pipes/counter.pipe';
import { DictKeysPipe } from '../../pipes/dict-keys.pipe';
import { DictValsPipe } from '../../pipes/dict-vals.pipe';
import { EnumeratePipe } from '../../pipes/enumerate.pipe';
import { SortByAlphaKeyPipe } from '../../pipes/sort-by-alpha-key.pipe';
import { ExtractAndWrapLinksPipe } from '../../pipes/extract-and-wrap-links.pipe';
import { SafeHtmlPipe } from 'src/app/pipes/safe-html.pipe';


import { NZ_ICONS } from 'ng-zorro-antd/icon';
import { NZ_I18N, en_US } from 'ng-zorro-antd/i18n';
import en from '@angular/common/locales/en';

import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzBackTopModule } from 'ng-zorro-antd/back-top';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzSliderModule } from 'ng-zorro-antd/slider';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzNotificationServiceModule } from 'ng-zorro-antd/notification';
import { NzTagModule } from 'ng-zorro-antd/tag';

import { GoogleMapsService } from '../../services/gmaps-service.service';
import { ElasticService } from '../../services/elastic.service';
import { ElasticInterceptor } from '../../elastic/interceptor';
import { ProgressIndicatorComponent } from 'src/app/progress-indicator/progress-indicator.component';
import { RouterModule } from '@angular/router';
import { DatepickersAndSlidersComponent } from './components/filters/datepickers/datepickers.component';
import { MainFiltersComponent } from './components/filters/main-filters/main-filters.component';
import { SubfiltersComponent } from './components/filters/subfilters/subfilters.component';
import { GeamLayersComponent } from 'src/app/geam-layers/geam-layers.component';
import { CurrencyComponent } from './components/filters/currency/currency.component';
import { CanvasDrawingComponent } from 'src/app/canvas-drawing/canvas-drawing.component';
import { CssPropsDirective } from 'src/app/directives/css-props.directive';
import { TextInputComponent } from '../../text-input/text-input.component';
import { HotspotComponent } from 'src/app/map/hotspot/hotspot.component';
import { RulerComponent } from 'src/app/map/ruler/ruler.component';
import { LegendsComponent } from 'src/app/map/legends/legends.component';
import { LegendComponent } from 'src/app/map/legend/legend.component';

registerLocaleData(en);

import * as AllIcons from '@ant-design/icons-angular/icons';
import { IconDefinition } from '@ant-design/icons-angular';
import { BackToTopComponent } from './components/back-to-top/back-to-top.component';
import { SpinnerLoaderComponent } from './components/spinner-loader/spinner-loader.component';

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(
  key => antDesignIcons[key]
);

@NgModule({
  declarations: [
    AsyncDownloaderModalComponent,
    MenuPanelComponent,
    DatepickersAndSlidersComponent,
    MainFiltersComponent,
    SubfiltersComponent,
    CollapseAllPanelsBtnComponent,
    BackToTopComponent,
    SpinnerLoaderComponent,

    GmapComponent,
    TriggerComponent,
    MarkerClusterComponent,
    RelativeDatepickerComponent,

    PiechartComponent,
    VerticalBarChart,
    HorizontalBarChart,
    LineChart,

    TextInputComponent,
    CurrencyComponent,
    CheckboxComponent,
    MultiselectComponent,
    SliderComponent,
    TextCardComponent,
    TableComponent,
    HistogramTableChartComponent,

    CanvasDrawingComponent,
    GeamLayersComponent,
    ProgressIndicatorComponent,

    HotspotComponent,
    RulerComponent,
    LegendsComponent,
    LegendComponent,

    CounterPipe,
    DictKeysPipe,
    DictValsPipe,
    EnumeratePipe,
    SortByAlphaKeyPipe,
    ExtractAndWrapLinksPipe,
    SafeHtmlPipe,

    CssPropsDirective,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    HttpClientModule,

    GoogleMapsModule,

    RouterModule,

    NzModalModule,
    NzAlertModule,
    NzButtonModule,
    NzCollapseModule,
    NzCheckboxModule,
    NzRadioModule,
    NzListModule,
    NzToolTipModule,
    NzSwitchModule,
    NzIconModule,
    NzBackTopModule,
    NzSpinModule,
    NzSliderModule,
    NzDatePickerModule,
    NzSelectModule,
    NzInputNumberModule,
    NzInputModule,
    NzGridModule,
    NzTableModule,
    NzPopoverModule,
    NzProgressModule,
    NzDividerModule,
    NzStepsModule,
    NzCardModule,
    NzLayoutModule,
    NzTabsModule,
    NzNotificationModule,
    NzNotificationServiceModule,
    NzTagModule
  ],
  providers: [
    GmapComponent,
    TriggerComponent,
    GoogleMapsService,
    ElasticService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ElasticInterceptor,
      multi: true
    },
    { provide: NZ_I18N, useValue: en_US },
    {
      provide: NZ_ICONS,
      useValue: icons
    }
  ],
  exports: [
    AsyncDownloaderModalComponent,
    MenuPanelComponent,
    DatepickersAndSlidersComponent,
    MainFiltersComponent,
    SubfiltersComponent,
    CollapseAllPanelsBtnComponent,
    BackToTopComponent,
    SpinnerLoaderComponent,

    GmapComponent,
    TriggerComponent,
    MarkerClusterComponent,
    RelativeDatepickerComponent,

    PiechartComponent,
    VerticalBarChart,
    HorizontalBarChart,
    LineChart,

    TextInputComponent,
    CurrencyComponent,
    CheckboxComponent,
    MultiselectComponent,
    SliderComponent,
    TextCardComponent,
    TableComponent,
    HistogramTableChartComponent,

    CanvasDrawingComponent,
    GeamLayersComponent,
    ProgressIndicatorComponent,

    CounterPipe,
    DictKeysPipe,
    DictValsPipe,
    EnumeratePipe,
    SortByAlphaKeyPipe,
    ExtractAndWrapLinksPipe,
    SafeHtmlPipe,

    HotspotComponent,
    RulerComponent,
    LegendsComponent,
    LegendComponent,

    CssPropsDirective,

    NzModalModule,
    NzAlertModule,
    NzButtonModule,
    NzCollapseModule,
    NzCheckboxModule,
    NzRadioModule,
    NzListModule,
    NzToolTipModule,
    NzSwitchModule,
    NzIconModule,
    NzBackTopModule,
    NzSpinModule,
    NzSliderModule,
    NzDatePickerModule,
    NzSelectModule,
    NzInputNumberModule,
    NzInputModule,
    NzGridModule,
    NzTableModule,
    NzPopoverModule,
    NzProgressModule,
    NzDividerModule,
    NzStepsModule,
    NzCardModule,
    NzLayoutModule,
    NzTabsModule,
    NzNotificationModule,
    NzNotificationServiceModule,
    NzTagModule
  ],
  bootstrap: []
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule
    };
  }
}
