import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  AfterViewChecked,
  ViewChildren,
  QueryList
} from '@angular/core';
import { RelativeDatepickerComponent } from '../../relative-datepicker/relative-datepicker.component';
import { CheckboxComponent } from '../../checkbox/checkbox.component';
import { DashboardEnums } from '../../utils/constants/dashboardEnums';
import { DivDownloaderService } from 'src/app/services/div-downloader.service';
import { ElasticService } from 'src/app/services/elastic.service';
import { AuthService } from 'src/app/services/auth.service';
import { NzCollapsePanelComponent } from 'ng-zorro-antd/collapse';
import { UiInteractionService } from 'src/app/services/ui-interaction.service';

@Component({
  selector: 'app-risk-engineering',
  templateUrl: './risk-engineering.component.html',
  styleUrls: ['./risk-engineering.component.scss']
})
export class RiskEngineeringComponent implements OnInit, AfterViewInit {
  @ViewChild('toggleEnlargedButtonIcon', { static: false })
  toggleEnlargedButtonIcon: ElementRef;

  @ViewChildren(NzCollapsePanelComponent) panels!: QueryList<NzCollapsePanelComponent>;

  dashboardTypes = DashboardEnums;
  currentDashboardType = this.dashboardTypes.RiskEngineeringManagement;

  public startDateComponent: RelativeDatepickerComponent;
  public endDateComponent: RelativeDatepickerComponent;
  public interactiveSearchCheckboxComponent: CheckboxComponent;

  expanded_map = true;

  constructor(
    private divService: DivDownloaderService,
    private es: ElasticService,
    private auth: AuthService,
    private interaction: UiInteractionService) { }

  ngOnInit() { }

  ngAfterViewInit() {
    this.es._forceFlushExecutionMonitors();
    this.auth.doCheckAuth().then(() => this.es.search('risk_engineering_management'));
  }

  setComponent(id, component) {
    this[id + 'Component'] = component;
  }

  refreshSite() {
    window.location.reload();
  }

  toggleExpandedCard() {
    this.expanded_map = !this.expanded_map;
    this.toggleEnlargedButtonIcon.nativeElement.classList.toggle('anticon-arrows-alt');
    this.toggleEnlargedButtonIcon.nativeElement.classList.toggle('anticon-shrink');
  }

  downloadDivAsJPG($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    const target = trigger.closest('.ant-card');
    const nzTitle = target.getAttribute('nztitle');
    const backup_target = document.querySelector(`[nztitle="${nzTitle}"] canvas.has-content`);

    this.divService.downloadAsJPG(target, nzTitle).catch(_ => {
      this.divService.downloadAsJPG(backup_target, nzTitle);
    });
  }

  downloadDivCsv($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    const target = trigger.closest('.ant-card');
    this.divService.downloadNestedCSV(target, target.getAttribute('nztitle'));
  }

  downloadDivRawCsv($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    const target = trigger.closest('.ant-card');
    this.divService.downloadNestedCSV(target, target.getAttribute('nztitle'), undefined, true);
  }

  regenerateContent($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    let target = trigger.closest('nz-card');
    target = target.querySelector('app-histogram-table-chart');
    this.es.search(target.getAttribute('klass'));
  }

  getCurrentlySelectedAssessmentKeyFactor() {
    if ((window as any).selected_key_factor_kind) {
      return ` (${(window as any).selected_key_factor_kind})`;
    }

    return '';
  }

  closeAllPanels($event: boolean) {
    if ($event) {
      this.interaction.closeAllCollapsePanels(this.panels);
    }
  }
}
