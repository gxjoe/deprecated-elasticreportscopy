import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  ViewChildren,
  QueryList
} from '@angular/core';
import { DashboardEnums } from '../../utils/constants/dashboardEnums';
import { DivDownloaderService } from 'src/app/services/div-downloader.service';
import { ElasticService } from 'src/app/services/elastic.service';
import { AuthService } from 'src/app/services/auth.service';
import { UiInteractionService } from 'src/app/services/ui-interaction.service';
import { NzCollapsePanelComponent } from 'ng-zorro-antd/collapse';

@Component({
  selector: 'app-re-insurance-management',
  templateUrl: './re-insurance-management.component.html',
  styleUrls: ['./re-insurance-management.component.scss']
})
export class ReInsuranceManagementComponent
  implements OnInit, AfterViewInit {
  @ViewChild('toggleEnlargedButtonIcon', { static: false })
  toggleEnlargedButtonIcon: ElementRef;

  @ViewChildren(NzCollapsePanelComponent) panels!: QueryList<NzCollapsePanelComponent>;

  dashboardTypes = DashboardEnums;
  currentDashboardType = this.dashboardTypes.ReInsuranceManagement;

  expanded_map = true;

  constructor(
    private divService: DivDownloaderService,
    private es: ElasticService,
    private auth: AuthService,
    private interaction: UiInteractionService) { }

  ngOnInit() { }

  ngAfterViewInit() {
    this.es._forceFlushExecutionMonitors();
    this.auth
      .doCheckAuth()
      .then(() => this.es.search('re_insurance_management'));
  }

  setComponent(id, component) {
    this[id + 'Component'] = component;
  }

  refreshSite() {
    window.location.reload();
  }

  toggleExpandedCard() {
    this.expanded_map = !this.expanded_map;
    this.toggleEnlargedButtonIcon.nativeElement.classList.toggle(
      'anticon-arrows-alt'
    );
    this.toggleEnlargedButtonIcon.nativeElement.classList.toggle(
      'anticon-shrink'
    );
  }

  downloadDivAsJPG($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    const target = trigger.closest('.ant-card');
    const nzTitle = target.getAttribute('nztitle');
    const backup_target = document.querySelector(`[nztitle="${nzTitle}"] canvas.has-content`);

    this.divService.downloadAsJPG(target, nzTitle).catch(_ => {
      this.divService.downloadAsJPG(backup_target, nzTitle);
    });
  }

  downloadDivCsv($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    const target = trigger.closest('.ant-card');
    this.divService.downloadNestedCSV(target, target.getAttribute('nztitle'));
  }

  downloadDivRawCsv($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    const target = trigger.closest('.ant-card');
    this.divService.downloadNestedCSV(target, target.getAttribute('nztitle'), undefined, true);
  }

  regenerateContent($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    let target = trigger.closest('nz-card');
    target = target.querySelector('app-histogram-table-chart');
    this.es.search(target.getAttribute('klass'));
  }

  closeAllPanels($event: boolean) {
    if ($event) {
      this.interaction.closeAllCollapsePanels(this.panels);
    }
  }
}
