import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewChecked,
  AfterViewInit,
  ViewEncapsulation,
  ViewChildren,
  QueryList
} from '@angular/core';
import { DashboardEnums } from 'src/app/utils/constants/dashboardEnums';
import { DivDownloaderService } from 'src/app/services/div-downloader.service';
import { ElasticService } from 'src/app/services/elastic.service';
import { AuthService } from 'src/app/services/auth.service';
import { SEUM_COLORS, invertColor } from 'src/app/charts/common/constants';
import { UiInteractionService } from 'src/app/services/ui-interaction.service';
import { NzCollapsePanelComponent } from 'ng-zorro-antd/collapse';

@Component({
  selector: 'app-sales-efficiency-management',
  templateUrl: './sales-efficiency-management.component.html',
  styleUrls: ['./sales-efficiency-management.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SalesEfficiencyManagementComponent implements OnInit, AfterViewInit, AfterViewChecked {
  @ViewChild('toggleEnlargedButtonIcon', { static: false })
  toggleEnlargedButtonIcon: ElementRef;

  @ViewChildren(NzCollapsePanelComponent) panels!: QueryList<NzCollapsePanelComponent>;

  dashboardTypes = DashboardEnums;
  currentDashboardType = this.dashboardTypes.SalesEfficiencyManagement;

  expanded_map = true;
  SEUM_COLORS = SEUM_COLORS;
  invertColor = invertColor;

  constructor(
    private divService: DivDownloaderService,
    private es: ElasticService,
    private auth: AuthService,
    private interaction: UiInteractionService) { }

  ngOnInit() { }

  ngAfterViewInit() {
    this.es._forceFlushExecutionMonitors();
    this.auth.doCheckAuth().then(() => this.es.search('sales_efficiency_management'));
  }

  ngAfterViewChecked() { }

  setComponent(id, component) {
    this[id + 'Component'] = component;
  }

  refreshSite() {
    window.location.reload();
  }

  toggleExpandedCard() {
    this.expanded_map = !this.expanded_map;
    this.toggleEnlargedButtonIcon.nativeElement.classList.toggle('anticon-arrows-alt');
    this.toggleEnlargedButtonIcon.nativeElement.classList.toggle('anticon-shrink');
  }

  downloadDivAsJPG($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    const target = trigger.closest('.ant-card');
    this.divService.downloadAsJPG(target, target.getAttribute('nztitle'));
  }

  downloadDivCsv($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    const target = trigger.closest('.ant-card');
    this.divService.downloadNestedCSV(target, target.getAttribute('nztitle'));
  }

  downloadDivRawCsv($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    const target = trigger.closest('.ant-card');
    this.divService.downloadNestedCSV(target, target.getAttribute('nztitle'), undefined, true);
  }

  regenerateContent($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    const target = trigger.closest('nz-card').querySelector('app-histogram-table-chart');
    if (!target) {
      this.es.search('AllOffers');
      return;
    }
    const target_class = target.getAttribute('klass');
    this.es.search(target_class || 'AllOffers');
  }

  closeAllPanels($event: boolean) {
    if ($event) {
      this.interaction.closeAllCollapsePanels(this.panels);
    }
  }
}
