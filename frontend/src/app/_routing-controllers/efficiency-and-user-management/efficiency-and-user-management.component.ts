import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit
} from '@angular/core';
import { DashboardEnums } from '../../utils/constants/dashboardEnums';
import { DivDownloaderService } from 'src/app/services/div-downloader.service';
import { ElasticService } from 'src/app/services/elastic.service';
import { AuthService } from 'src/app/services/auth.service';
import {
  RISK_PROFILE_COLORS,
  SEUM_COLORS,
  invertColor
} from 'src/app/charts/common/constants';

@Component({
  selector: 'app-efficiency-and-user-management',
  templateUrl: './efficiency-and-user-management.component.html',
  styleUrls: ['./efficiency-and-user-management.component.scss']
})
export class EfficiencyAndUserManagementComponent
  implements OnInit, AfterViewInit {
  @ViewChild('toggleEnlargedButtonIcon', { static: false })
  toggleEnlargedButtonIcon: ElementRef;

  dashboardTypes = DashboardEnums;
  currentDashboardType = this.dashboardTypes.EfficiencyUserManagement;

  expanded_map = true;

  SEUM_COLORS = SEUM_COLORS;
  RISK_PROFILE_COLORS = RISK_PROFILE_COLORS;
  invertColor = invertColor;

  constructor(
    private divService: DivDownloaderService,
    private es: ElasticService,
    private auth: AuthService
  ) { }

  ngOnInit() { }

  ngAfterViewInit() {
    this.es._forceFlushExecutionMonitors();
    this.auth
      .doCheckAuth()
      .then(() => this.es.search('efficiency_user_management'));
  }

  setComponent(id, component) {
    this[id + 'Component'] = component;
  }

  refreshSite() {
    window.location.reload();
  }

  toggleExpandedCard() {
    this.expanded_map = !this.expanded_map;
    this.toggleEnlargedButtonIcon.nativeElement.classList.toggle(
      'anticon-arrows-alt'
    );
    this.toggleEnlargedButtonIcon.nativeElement.classList.toggle(
      'anticon-shrink'
    );
  }

  downloadDivAsJPG($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    const target = trigger.closest('.ant-card');
    this.divService.downloadAsJPG(target, target.getAttribute('nztitle'));
  }

  downloadDivCsv($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    const target = trigger.closest('.ant-card');
    this.divService.downloadNestedCSV(target, target.getAttribute('nztitle'));
  }

  downloadDivRawCsv($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    const target = trigger.closest('.ant-card');
    this.divService.downloadNestedCSV(target, target.getAttribute('nztitle'), undefined, true);
  }
}
