import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy
} from '@angular/core';
import { GmapComponent } from '../../gmap/gmap.component';
import { RelativeDatepickerComponent } from '../../relative-datepicker/relative-datepicker.component';
import { CheckboxComponent } from '../../checkbox/checkbox.component';
import { DashboardEnums } from '../../utils/constants/dashboardEnums';
import { DivDownloaderService } from 'src/app/services/div-downloader.service';
import { ElasticService } from 'src/app/services/elastic.service';
import { Subject } from 'rxjs';
import { MAP_KLASSES } from 'src/app/elastic/queries';
import { CsvService } from 'src/app/services/csv.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-standlone-maps',
  templateUrl: './standlone-maps.component.html',
  styleUrls: ['./standlone-maps.component.scss']
})
export class StandaloneMapsComponent
  implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('heatmap', { static: false })
  heatmap: any;

  @ViewChild('OfferDistributionHeatmap', { static: false })
  OfferDistributionHeatmap: any;

  @ViewChild('ClientDistributionHeatmap', { static: false })
  ClientDistributionHeatmap: any;

  @ViewChild('ExperimentalHeatmap', { static: false })
  ExperimentalHeatmap: any;

  @ViewChild('clustermap', { static: false })
  clustermap: any;

  @ViewChild('toggleEnlargedButtonIcon', { static: false })
  toggleEnlargedButtonIcon: ElementRef;

  dashboardTypes = DashboardEnums;
  currentDashboardType = this.dashboardTypes.MapsAndPositions;

  public heatmapComponent: GmapComponent;
  public ClientDistributionHeatmapComponent: GmapComponent;
  public OfferDistributionHeatmapComponent: GmapComponent;
  public ExperimentalHeatmapComponent: GmapComponent;
  public clustermapComponent: GmapComponent;
  public startDateComponent: RelativeDatepickerComponent;
  public endDateComponent: RelativeDatepickerComponent;
  public interactiveSearchCheckboxComponent: CheckboxComponent;

  expanded_map = true;

  private activeKey: Subject<string> = new Subject<string>();
  activeKlass = MAP_KLASSES.OfferDistribution;

  _current_csv_value_shareable: string;
  _underlying_data_shareable: string;

  constructor(
    private divService: DivDownloaderService,
    private es: ElasticService,
    private csvService: CsvService,
    private auth: AuthService
  ) { }

  ngOnInit() {
    this.csvService._current_csv_value_shareable.subscribe(val => {
      this._current_csv_value_shareable = val;
    });

    this.csvService._underlying_data_shareable.subscribe(val => {
      this._underlying_data_shareable = val;
    });
  }

  ngAfterViewInit() {
    this.es._forceFlushExecutionMonitors();
    this.auth.doCheckAuth().then(() => this.es.search('maps_and_positions'));
  }

  ngOnDestroy() {
    console.error('ngdestroy');
    this.es.app_component.unsetComponent(MAP_KLASSES.ClientDistribution);
    this.es.app_component.unsetComponent(MAP_KLASSES.OfferDistribution);
  }

  setComponent(id, component) {
    this[id + 'Component'] = component;
  }

  refreshSite() {
    window.location.reload();
  }

  toggleExpandedCard() {
    this.expanded_map = !this.expanded_map;
    this.toggleEnlargedButtonIcon.nativeElement.classList.toggle(
      'anticon-arrows-alt'
    );
    this.toggleEnlargedButtonIcon.nativeElement.classList.toggle(
      'anticon-shrink'
    );
  }

  _activeComponentId() {
    return this.es.activeMapComponent.klass;
  }

  tabChanged($event) {
    const title = $event.tab.nzTitle;

    switch (true) {
      case title.includes('Clients'):
        this.activeKlass = MAP_KLASSES.ClientDistribution;
        break;
      case title.includes('Risks'):
        this.activeKlass = MAP_KLASSES.OfferDistribution;
        break;
      case title.includes('Experimental'):
        this.activeKlass = MAP_KLASSES.ExperimentalHeatmap;
        break;
    }

    this.activeKey.next(this.activeKlass);
  }

  downloadDivAsJPG($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    const target = trigger.closest('.ant-card');
    this.divService.downloadAsJPG(target, target.getAttribute('nztitle'));
  }

  downloadDivCsv($event: MouseEvent) {
    const trigger = $event.target as HTMLElement;
    const target = trigger.closest('.ant-card');
    this.divService.downloadNestedCSV(target, target.getAttribute('nztitle'));
  }
}
