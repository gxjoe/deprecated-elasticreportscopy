export const filteringComponentVsAffectedComponents = {
  LoB_Multiselect: [
    'OffersAggregatedByLineOfBusiness',
    'OffersAggregatedByLineOfBusinessVsPremium',
    'AcceptedOffersAggregatedByLineOfBusiness',
    'DeclinedOffersAggregatedByLineOfBusiness'
  ],
  OfferStatus_Multiselect: ['OffersAggregatedByStatus']
};

const componentVsAffectingFilters = {
  OffersAggregatedByLineOfBusiness: ['LoB_Multiselect'],
  OffersAggregatedByLineOfBusinessVsPremium: ['LoB_Multiselect'],
  AcceptedOffersAggregatedByLineOfBusiness: ['LoB_Multiselect'],
  DeclinedOffersAggregatedByLineOfBusiness: ['LoB_Multiselect'],
  OffersAggregatedByStatus: ['OfferStatus_Multiselect'],
  AggregatedSI_histogram_chart: ['LoB_Multiselect'],
  AggregatedPremium_histogram_chart: ['LoB_Multiselect'],
  ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndRiskCode_histogram_chart: ['LoB_Multiselect'],
  ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndNaceCode_histogram_chart: ['NaceCodesStandalone_Multiselect'],
  ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndOccupancy_histogram_chart: ['LoB_Multiselect', 'Industry_Multiselect', 'TypeOfBusiness_SemiStatic_Multiselect'],
  ESG_AggregatedEmissionsPerOccupancy_histogram_chart: ['LoB_Multiselect', 'Industry_Multiselect', 'TypeOfBusiness_SemiStatic_Multiselect'],
  ESG_AggregatedEmissionsPerNaceCode_histogram_chart: ['NaceCodesStandalone_Multiselect'],
  AggregatedTotalPremiumPerIndustry_histogram_chart: ['LoB_Multiselect'],
  AggregatedTotalSumInsuredPerIndustry_histogram_chart: ['LoB_Multiselect'],
  AggregatedPremiumsVsTotalSumInsuredPerIndustry_histogram_chart: [
    'LoB_Multiselect'
  ],
  AggregatedLOBCountPerIndustry_histogram_chart: ['LoB_Multiselect'],
  AggregatedLocationsCountPerIndustry_histogram_chart: ['LoB_Multiselect'],

  PPM_GlobalStatsPerBusinessActivity_NoCanvas_histogram_chart: [
    'LoB_Multiselect'
  ],
  AggregatedTotalPremiumPerBusinessActivity_histogram_chart: [
    'LoB_Multiselect',
    'Industry_Multiselect'
  ],
  AggregatedTotalSumInsuredPerBusinessActivity_histogram_chart: [
    'LoB_Multiselect',
    'Industry_Multiselect'
  ],
  AggregatedTotalOffersPerBusinessActivity_histogram_chart: [
    'LoB_Multiselect',
    'Industry_Multiselect'
  ],
  AggregatedLocationsPerBusinessActivity_histogram_chart: [
    'LoB_Multiselect',
    'Industry_Multiselect'
  ],
  AggregatedPremiumsVsTotalSumInsuredPerBusinessActivity_histogram_chart: [
    'LoB_Multiselect',
    'Industry_Multiselect'
  ],

  PPM_GlobalStatsPerNaceCode_NoCanvas_histogram_chart: [
    'LoB_Multiselect',
    'NaceCodesStandalone_Multiselect'
  ],
  AggregatedTotalPremiumPerNaceCodes_histogram_chart: [
    'LoB_Multiselect',
    'Industry_Multiselect',
    'NaceCodesStandalone_Multiselect'
  ],
  AggregatedTotalOffersPerNaceCodes_histogram_chart: [
    'LoB_Multiselect',
    'Industry_Multiselect',
    'NaceCodesStandalone_Multiselect'
  ],
  AggregatedTotalSumInsuredPerNaceCodes_histogram_chart: [
    'LoB_Multiselect',
    'Industry_Multiselect',
    'NaceCodesStandalone_Multiselect'
  ],
  AggregatedLocationsPerNaceCodes_histogram_chart: [
    'LoB_Multiselect',
    'Industry_Multiselect',
    'NaceCodesStandalone_Multiselect'
  ],
  AggregatedTurnoverVsPremium_histogram_chart: ['LoB_Multiselect'],
  AggregatedTurnoverVsTSI_histogram_chart: ['LoB_Multiselect'],
  AggregatedTurnoverVsCount_histogram_chart: ['LoB_Multiselect'],

  GrossRiskProfile_colored_histogram_chart: [],

  AggregatedTotalPremiumPerRegionOfOrigin_histogram_chart: [
    'LoB_Multiselect',
    'RegionOfOrigin_Multiselect'
  ],
  AggregatedLOBCountPerRegionOfOrigin_histogram_chart: [
    'LoB_Multiselect',
    'RegionOfOrigin_Multiselect'
  ],
  AggregatedTotalSumInsuredPerRegionOfOrigin_histogram_chart: [
    'LoB_Multiselect',
    'RegionOfOrigin_Multiselect'
  ],
  AggregatedLocationsCountPerRegionOfOrigin_histogram_chart: [
    'LoB_Multiselect',
    'RegionOfOrigin_Multiselect'
  ],

  AggregatedTotalPremiumPerSalesPartner_histogram_chart: [
    'LoB_Multiselect',
    'SalesPartner_Multiselect'
  ],
  AggregatedLOBCountPerSalesPartner_histogram_chart: [
    'LoB_Multiselect',
    'SalesPartner_Multiselect'
  ],
  AggregatedTotalSumInsuredPerSalesPartner_histogram_chart: [
    'LoB_Multiselect',
    'SalesPartner_Multiselect'
  ],
  AggregatedLocationsCountPerSalesPartner_histogram_chart: [
    'LoB_Multiselect',
    'SalesPartner_Multiselect'
  ],

  AggregatedTotalPremiumPerAvgRiskClassPerPeril_histogram_chart: [
    'LoB_Multiselect'
  ],
  AggregatedTotalSumInsuredPerAvgRiskClassPerPeril_histogram_chart: [
    'LoB_Multiselect'
  ],
  AggregatedLOBCountPerAvgRiskClassPerPeril_histogram_chart: [
    'LoB_Multiselect'
  ],
  AggregatedGroupedPremiumPerAvgRiskClassPerPeril_LoBBased_histogram_chart: [],
  AggregatedGroupedPremiumPerAvgRiskClassPerPeril_location_based_histogram_chart: [],

  AggregatedAvgPDAPerAvgRiskClassPerPeril_histogram_chart: ['LoB_Multiselect'],
  AggregatedAvgPDAPerTSIPerPeril_histogram_chart: ['LoB_Multiselect'],

  AggregatedBrokerCommission_histogram_chart: ['LoB_Multiselect'],

  AggregatedTotalMPremiumPerPeril_histogram_chart: ['LoB_Multiselect'],
  AggregatedTotalTPremiumPerPeril_histogram_chart: ['LoB_Multiselect'],
  AggregatedRiskScorePerAvgRiskClassPerPeril_histogram_chart: [
    'LoB_Multiselect'
  ],
  AggregatedRiskScoreVsPeril_histogram_chart: ['LoB_Multiselect'],
  AggregatedInfoLevelVsRiskClassVsPeril_histogram_chart: ['LoB_Multiselect'],

  AggregatedSuperscoresPerLocationsPerIndustry_histogram_chart: [
    'LoB_Multiselect'
  ],
  AggregatedSuperscoresPerRequestFromCountry_histogram_chart: [
    'LoB_Multiselect'
  ],
  AggregatedSuperscoresPerBucketedTSI_histogram_chart: ['LoB_Multiselect'],

  AggregatedTSIPerInsuredObject_histogram_chart: ['LoB_Multiselect'],

  AggregatedTSIPerConstructionMaterials_location_based_histogram_chart: [
    'ConstructionMaterial_Multiselect'
  ],

  // RISK ENGINEERING BEGIN
  AggregatedAddressesVsIndustryCategory_histogram_chart: [],

  AggregatedAddressesPerIL_histogram_chart: [],
  AggregatedAddressesPerScore_histogram_chart: [],
  AggregatedAddressesPerPML_histogram_chart: [],
  AggregatedAddressesPerRC_histogram_chart: [],

  AggregatedAddressesPerRC_vsPML_address_based_histogram_chart: [],
  AggregatedAddressesPerRC_vsIL_address_based_histogram_chart: [],
  AggregatedAddressesPerRC_vsScore_address_based_histogram_chart: [],
  AggregatedAddressesPerRC_vsRecommendations_address_based_histogram_chart: [],

  AggregatedAddressesPerIC_vsRecommendations_address_based_histogram_chart: [],
  AggregatedAddressesPerIC_vsPML_address_based_histogram_chart: [],
  AggregatedAddressesPerIC_vsIL_address_based_histogram_chart: [],
  AggregatedAddressesPerIC_vsScore_address_based_histogram_chart: [],

  AggregatedAddressesPerScorePerPML_address_based_histogram_chart: [],
  AggregatedAddressesPerAsIfScorePerPML_address_based_histogram_chart: [],

  AggregatedAddressesPerAsIfScorePerPML_EML_Ratio_address_based_histogram_chart: [],

  AggregatedAddressesPerRiskScore_vsPremiums_address_based_histogram_chart: [],
  AggregatedAddressesPerRiskScore_vsPremiums_IndividualLocations_address_based_histogram_chart: [],

  AggregatedAddressesPerRecommendationKinds_vsPriority_address_based_histogram_chart: [],
  AggregatedAddressesPerRecommendationKinds_vsRecommendations_address_based_histogram_chart: [],
  AggregatedAddressesPerRecommendationKinds_vsLocationCounts_address_based_histogram_chart: [],
  AggregatedAddressesPerRecommendationKinds_vsNegativeCondition_address_based_histogram_chart: [],

  AggregatedAddressesPerEvaluations_vsProbabilityCriterias_address_based_histogram_chart: [],
  AggregatedAddressesPerEvaluations_vsPotentialCriterias_address_based_histogram_chart: [],
  AggregatedAddressesPerEvaluations_vsPreventionCriterias_address_based_histogram_chart: [],
  // RISK ENGINEERING END

  // RE INSURANCE BEGIN
  RIM_Property_AggregatedStatsByPML_address_based_histogram_chart: [],
  RIM_Property_AggregatedStatsByPMLminusTimeRange_AbsoluteValues_address_based_histogram_chart: [],
  RIM_Property_AggregatedStatsByPMLminusTimeRange_PercentualValues_address_based_histogram_chart: [],
  RIM_Property_AggregatedStatsByTSI_address_based_histogram_chart: [],
  RIM_Property_AggregatedStatsByTSIminusTimeRange_AbsoluteValues_address_based_histogram_chart: [],
  RIM_Property_AggregatedStatsByTSIminusTimeRange_PercentualValues_address_based_histogram_chart: []
};

const getComponentsAffectingFilters = key => {
  return componentVsAffectingFilters[key];
};

export const getValuesOfAffectingFilters = (app, key, all = false) => {
  const resp = {};

  if (!all) {
    const affectingKlass = getComponentsAffectingFilters(key)[0];
    const component = app.getComponentByKlass(affectingKlass);
    if (!component) {
      console.debug(`no component ${affectingKlass}`);
      return resp;
    }
    return component.filterQueryBuilder().values;
  }

  getComponentsAffectingFilters(key).map(klass => {
    if (app.getComponentByKlass(klass)) {
      resp[klass] = app.getComponentByKlass(klass).filterQueryBuilder().values;
    } else {
      resp[klass] = [];
    }
  });

  return resp;
};
