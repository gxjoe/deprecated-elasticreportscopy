export enum DashboardEnums {
  EfficiencyUserManagement = 'EfficiencyUserManagement',
  MapsAndPositions = 'MapsAndPositions',
  ProductPortfolioManagement = 'ProductPortfolioManagement',
  RiskEngineeringManagement = 'RiskEngineeringManagement',
  SalesEfficiencyManagement = 'SalesEfficiencyManagement',
  ReInsuranceManagement = 'ReInsuranceManagement',
  EnvironmentalSocialGovernance = 'EnvironmentalSocialGovernance',
  Demo = 'Demo'
}

export const ModulesToMenuPanelParams = Object.keys(DashboardEnums)
  .filter(item => item !== DashboardEnums.Demo)
  .map(item => {
    return {
      key: item,
      shortened: item.match(/[A-Z]/g).join(''),
      tooltipContent: item.split(/(?=[A-Z])/g).join(' '),
      href: item
        .split(/(?=[A-Z])/)
        .join('_')
        .toLowerCase()
    };
  });
