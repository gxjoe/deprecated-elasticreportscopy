export default {
  AccountStatus_Multiselect: 'lob.accountStatus',
  City_Multiselect: 'client.city',
  Country_Multiselect: 'offer.requestFromCountry',
  CB_or_SMB_Multiselect: 'offer.cbOrSme',

  International_Business_Multiselect: 'offer.internationalProgram',
  International_Business_Multiselect_LogicalFilter: '', // UDGB-48

  Cross_Border_Businesss_Multiselect: 'offer.crossBorderBusiness',
  DistributionChannel_Multiselect: 'offer.distributionChannel',
  EUM_ResponsibleLocalUnderwriterMultiselect: 'lob.localUnderwriters.fullName',
  Industry_Multiselect: 'offer.industry',
  LoB_Multiselect: 'lob.lineOfBusiness',
  LocationPeril_Multiselect:
    'address.locationProperty.locationAssessments.peril',
  LocationPerilRiskClass_Multiselect:
    'address.locationProperty.locationAssessments.riskClass',
  LocationAssessmentLevel3_IgnoreNullScores_Multiselect: '',
  LoBRisk_Multiselect: 'lob.risks.risk',
  LoB_Peril_RiskDescription_Multiselect: 'lob.risks.perils.riskDescription',
  NaceCodesStandalone_Multiselect: 'offer.client.nacePrimaryCodes',
  OfferStatus_Multiselect: 'lob.status',
  PerilRiskClass_Multiselect: 'lob.riskClass',
  Peril_Multiselect: 'lob.rating.ratingToolPerils',
  Public_Tender_Multiselect: 'offer.publicTender',
  Region_Multiselect: 'offer.client.region',
  RegionOfOrigin_Multiselect: 'offer.regionalDirectorate',
  SourceSystem_Multiselect: 'offer.sourceSystem.keyword',
  RiskEngineer_Multiselect: 'address.locationProperty.globalLocationDataCreatedByFullName.keyword',
  RiskClass_Multiselect: 'lob.riskClass',
  SalesManager_Multiselect: 'offer.salesManagers.fullName',
  SalesPartner_Multiselect: 'lob.salesPartner.name',
  TotalPremium: 'lob.premium_$CURRENCY$',
  TotalSumInsured: 'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',
  TurnoverRange: 'offer.client.revenueInEuro',
  LobUniqaPMLRange: 'lob.uniqaPML_$CURRENCY$',
  AddressUniqaPMLRange: 'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
  AddressLocationAssessmentScoreRange: 'address.locationProperty.locationAssessments.score',
  TypeOfBusiness_Multiselect: 'lob.typeOfBusiness',
  UCB_Confirmation_needed_Multiselect: 'offer.uicbConfirmNeeded',
  UCB_Involved_Multiselect: 'offer.uicbInvolved',
  UNIQA_Leading_Multiselect: 'lob.uniqaLeading',

  TypeOfBusiness_SemiStatic_Multiselect: 'lob.typeOfBusiness',

  // Building & Construction
  BuildingType_Multiselect: 'address.locationProperty.buildingType',
  RoofGeometry_Multiselect: 'address.locationProperty.roofGeometry',
  RoofSystem_Multiselect: 'address.locationProperty.roofSystem',
  ConstructionMaterial_Multiselect: 'address.locationProperty.constructionMaterial',
  ShapeOfBuilding_Multiselect: 'address.locationProperty.shapeOfBuilding',
  NumberOfBuildings: 'address.locationProperty.numberOfBuildings',
  NumberOfStories: 'address.locationProperty.numberOfStories',
  NumberOfBasementFloors: 'address.locationProperty.basementFloors',
  YearOfConstruction: 'address.locationProperty.yearOfConstruction',

  LocationAssessmentLevel3_KeyFactorKind_Multiselect: '',
  KeyCriterion_Multiselect: '',
  AssessmentRecommendations_Multiselect: '',
  NegativeAspects_forGlobalWindowAggsOnly_Multiselect: '',
  IndustryVsTypeOfBusiness_forGlobalWindowAggsOnly_Multiselect: '',
  NACECodes_forGlobalWindowAggsOnly_Multiselect: '',

  // RecommendationKinds_forGlobalWindowAggsOnly_Multiselect: '',
  RecommendationKindsNegativeConditions_forGlobalWindowAggsOnly_Multiselect: '',
  ProbabilityKeyCriterias_forGlobalWindowAggsOnly_Multiselect: '',
  PotentialKeyCriterias_forGlobalWindowAggsOnly_Multiselect: '',
  PreventionKeyCriterias_forGlobalWindowAggsOnly_Multiselect: '',

  ESGRiskCode_Multiselect: '',
  ESGCriterion_Multiselect: ''
};
