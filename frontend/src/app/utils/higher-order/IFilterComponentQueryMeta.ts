import { ElasticRangeOperators, ElasticFilterSubtypes, ElasticQueryClauses } from '../../elastic/constants';

export enum DashboardEventTriggerCategory {
  SINGLE_SLIDER,
  DOUBLE_SLIDER,
  SINGLE_SELECT,
  MULTI_SELECT
}

export default interface IFilterComponentQueryMeta {
  klass: string;
  elastic_variable: string;
  subtype: ElasticFilterSubtypes;
  operators: Array<ElasticRangeOperators> | null;
  values: any | Array<any>;
  clause?: ElasticQueryClauses | ElasticQueryClauses.filter;
  nested?: boolean;
  finished_query?: object;
}
