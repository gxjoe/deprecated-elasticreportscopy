import {
    AddressesDoc,
    KeyCriterionAssessment,
    KeyFactorType,
    PerilName,
    SomeKeyFactor,
} from 'src/app/interfaces/elastic';

type EnrichedKeyCriterionAssessment = KeyCriterionAssessment & {
    keyFactorName: KeyFactorType;
    keyFactorScore: SomeKeyFactor['score'];
};

const extractKeyCriterionAssessments = ({
    keyFactorName,
    keyFactorGroup,
}: {
    keyFactorName: KeyFactorType;
    keyFactorGroup: SomeKeyFactor;
}): EnrichedKeyCriterionAssessment[] => {
    const finalList: EnrichedKeyCriterionAssessment[] = [];

    for (const [_, criterionAssessment] of Object.entries(
        keyFactorGroup?.keyCriteria
    )) {
        finalList.push({
            ...criterionAssessment,
            keyFactorName,
            keyFactorScore: keyFactorGroup.score,
        });
    }

    return finalList;
};

/**
 * Iterate the depths of Level 3 location assessments and pre-construct CSV rows
 * as per UDGB-29.
 * @param param0
 * @returns
 */
export const flattenAssessmentsFromAddresses = ({
    innerHits,
    basedOn,
    targetPeril,
}: {
    innerHits: AddressesDoc[];
    basedOn: 'recommendations' | 'negativeAspects';
    targetPeril: PerilName;
}): Array<{ [key: string]: any }> => {
    const smallestUnitRows = [];

    for (const hit of innerHits) {
        const { address, offer, lob } = hit;

        const { number: offerNumber, client } = offer;
        const { name: clientName } = client;

        const { id: lobId } = lob;

        const { street, houseNumber, zipCode, city, country } = address;

        const { locationAssessmentsLevel3 } = address.locationProperty;
        const { keyFactors } = locationAssessmentsLevel3[targetPeril];

        for (const [keyFactorName, keyFactorGroup] of Object.entries(keyFactors)) {
            const criterionAssessments = extractKeyCriterionAssessments({
                keyFactorName: keyFactorName as KeyFactorType,
                keyFactorGroup,
            });

            if (criterionAssessments?.length) {
                for (const keyCriterionAssessment of criterionAssessments) {
                    const propsToPrepend = {
                        'Offer No': offerNumber,
                        'LoB ID': lobId,
                        'Address ID': address.id,
                        'Full Address': `${street} ${houseNumber}, ${zipCode} ${city}, ${country}`,
                        'Client Name': clientName,
                        'Assessed Peril': targetPeril,
                    };

                    const {
                        recommendations,
                        negativeAspects,

                        keyFactorScore,

                        name: keyCriterionName,
                        score: keyCriterionScore,
                    } = keyCriterionAssessment;

                    const propsToAppend = {
                        'Key Criterion': keyCriterionName,
                        'Key Criterion Score': keyCriterionScore,
                        'Key Factor': keyFactorName,
                        'Key Factor Score': keyFactorScore,
                    };

                    let props: { [key: string]: any } = {};

                    if (basedOn === 'recommendations') {
                        if (!recommendations.length) {
                            continue;
                        }
                        for (const recomm of recommendations) {
                            const { priority, statusFeedback } = recomm;
                            const { aspect, name, description } =
                                recomm;

                            props = {
                                Aspect: aspect,
                                'Recomm. Name': name || '-',
                                'Recomm. Description': description || '-',
                                Priority: priority || '-',
                                'Status Feedback': statusFeedback || '-',
                            };

                            smallestUnitRows.push({
                                ...propsToPrepend,
                                ...props,
                                ...propsToAppend,
                            });
                        }
                    } else if (basedOn === 'negativeAspects') {
                        if (!negativeAspects.length) {
                            continue;
                        }
                        for (const negAspect of negativeAspects) {

                            const { negativeAspect, name, description } = negAspect;

                            props = {
                                'Neg. Aspect': negativeAspect,
                                'Recomm. Name': name || '-',
                                'Recomm. Description': description || '-'
                            };

                            smallestUnitRows.push({
                                ...propsToPrepend,
                                ...props,
                                ...propsToAppend,
                            });

                        }
                    }
                }
            }
        }
    }

    return smallestUnitRows;
};
