import * as currencyFormatter from 'currency-formatter';
import * as _ from 'lodash';
import { JSONPrettifier } from './highlightJSON';
import { LISTS_OF_HISTOGRAM_CATEGORIES } from 'src/app/charts/histogram-table-chart/histogramListsAndCategories';
import { roundUp } from './numerical';
import { AddressesDoc, RatingToolPeril } from 'src/app/interfaces/elastic';

export const capitalize = str => str.charAt(0).toUpperCase() + str.slice(1);

declare global {
  interface Array<T> {
    sumByReduce<T>(): Array<T>;
    rangeOfNumbers(start: number, end: number, step: number): Array<T>;
    makeDistinct<T>(): Array<T>;
  }
}

export function add(a, b) {
  return a + b;
}

Array.prototype.sumByReduce = function () {
  return this && this.length ? this.reduce((a, b) => a + b) : 0;
};

Array.prototype.rangeOfNumbers = function (
  start: number,
  end: number,
  step: number
) {
  const A = [];

  A[0] = start;
  step = step || 1;
  if (step < 0) {
    while (start + step >= end) {
      A[A.length] = start += step;
    }
  } else {
    while (start + step <= end) {
      A[A.length] = start += step;
    }
  }

  return A;
};

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

Array.prototype.makeDistinct = function () {
  return this && this.length ? this.filter(onlyUnique) : [];
};

export function everyTruthy(collection) {
  return _.every(collection, Boolean);
}

export function everyIsNull(collection) {
  return collection.map(val => val !== null).length === 0;
}

export function isObject(item) {
  return item && typeof item === 'object' && !Array.isArray(item);
}

export function isString(item: any) {
  return typeof item === 'string';
}

export function isNumeric(str: any) {
  return !isNaN(str as any) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
    !isNaN(parseFloat(str)); // ...and ensure strings of whitespace fail
}

export function isFunction(functionToCheck) {
  return (
    functionToCheck && {}.toString.call(functionToCheck) === '[object Function]'
  );
}

export const extractFirstValueFromDict = obj => {
  return obj[Object.keys(obj)[0]];
};

export const concatMaps = (map, ...iterables) => {
  for (const iterable of iterables) {
    for (const item of iterable) {
      map.set(...item);
    }
  }
};

/*!
 * Merge two or more objects together.
 * (c) 2017 Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param   {Boolean}  isDeep     If true, do a deep (or recursive) merge [optional]
 * @param   {Object}   args       The objects to merge together
 * @returns {Object}              Merged values of defaults and options
 */
const extend = function (isDeep, ...args) {
  // Variables
  const extended = {};
  let i = 0;

  // Check if a deep merge
  if (Object.prototype.toString.call(arguments[0]) === '[object Boolean]') {
    i++;
  }

  // Merge the object into the extended object
  const merge = function (obj) {
    for (const prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        // If property is an object, merge properties
        if (
          isDeep &&
          Object.prototype.toString.call(obj[prop]) === '[object Object]'
        ) {
          extended[prop] = extend(isDeep, extended[prop], obj[prop]);
        } else {
          extended[prop] = obj[prop];
        }
      }
    }
  };

  // Loop through each object and conduct a merge
  for (; i < arguments.length; i++) {
    merge(arguments[i]);
  }

  return extended;
};

export function mergeDeep(target, should_pass, ...sources) {
  if (typeof should_pass !== 'undefined' && should_pass === false) {
    return {};
  }

  if (!sources.length) {
    return target;
  }

  return extend(true, { ...target }, ...sources);
}

export const sortArrOfStringsByLength = (arr, dir = 'asc') => {
  return arr.sort(function (a, b) {
    // ASC  -> a.length - b.length
    // DESC -> b.length - a.length
    return dir === 'asc' ? a.length - b.length : b.length - a.length;
  });
};

export const sortByExtractingNumbersFromStrings = arr => {
  return arr.sort(function (a, b) {
    const nameA = parseFloat(a.match(/\d+/g)[0]);
    const nameB = parseFloat(b.match(/\d+/g)[0]);

    if (nameA < nameB) {
      // sort string ascending
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0; // default return value (no sorting)
  });
};

export const sortObjectKeysByPropertyValue = obj => {
  return Object.keys(obj).sort(function (a, b) {
    return extractNumberFromString(obj[b]) - extractNumberFromString(obj[a]);
  });
};

export const sortArrOfStringsAlphabetically = (
  arr,
  dir = 'asc',
  ignore_chars = new RegExp('')
) => {
  return arr.sort(function (a, b) {
    if (dir === 'desc') {
      // reverse items
      const c = b;
      a = b;
      b = c;
    }

    const nameA = a.toLowerCase().replace(ignore_chars, ''),
      nameB = b.toLowerCase().replace(ignore_chars, '');

    if (nameA < nameB) {
      // sort string ascending
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0; // default return value (no sorting)
  });
};

export const sortArrayOfRisks = (arr: Array<any>): Array<any> => {
  const splitByRisk = {};
  let sorted = [];
  arr.map(bkey => {
    const risk =
      bkey.match(/risk \d/gim) &&
      bkey
        .match(/risk \d/gim)[0]
        .split(' ')
        .join('');
    if (!splitByRisk[risk]) {
      splitByRisk[risk] = [];
    }
    splitByRisk[risk].push(bkey);
  });
  sortArrOfStringsAlphabetically(Object.keys(splitByRisk)).map(k => {
    sorted = sorted.concat(sortArrOfStringsAlphabetically(splitByRisk[k]));
  });
  return sorted;
};

export const sortArrayOfRiskClasses = (arr: Array<any>): Array<any> => {
  const splitByRisk = {};
  let sorted = [];
  arr.map(bkey => {
    const risk = bkey.match(/RC \d+/gim) && bkey.match(/\d+/gim)[0];
    if (!splitByRisk[risk]) {
      splitByRisk[risk] = [];
    }
    splitByRisk[risk].push(bkey);
  });

  Object.keys(splitByRisk)
    .map(parseFloat)
    .sort((a, b) => a - b)
    .map(String)
    .map(k => {
      sorted = sorted.concat(splitByRisk[k]);
    });
  return sorted;
};

export function mergeArraysByProp(a, b, prop) {
  const reduced = a.filter(
    aitem => !b.find(bitem => aitem[prop] === bitem[prop])
  );
  return reduced.concat(b);
}

export const mergeArraysOfObjectsByKey = (
  a,
  b,
  fill_nonexistent_with_value?
) => {
  let fin = [];

  const shorter_one: Array<any> = a.length > b.length ? b.slice() : a.slice();
  const other_one: Array<any> = a.length > b.length ? a.slice() : b.slice();

  if (
    typeof fill_nonexistent_with_value === 'undefined' ||
    fill_nonexistent_with_value === null
  ) {
    shorter_one.forEach((itm, i) => {
      fin.push(Object.assign({}, itm, other_one[i]));
    });
  } else {
    const tmp = other_one.map(item1 => {
      return Object.assign(
        item1,
        shorter_one.find(item2 => {
          return item2 && item1.key === item2.key;
        })
      );
    });

    const uniq_shorter_keys = uniq(
      [].concat.apply(
        [],
        shorter_one.map(it => Object.keys(it).filter(k => k !== 'key'))
      )
    );

    fin = tmp.map(item => {
      const temp = Object.assign({}, item);
      uniq_shorter_keys.map(ukey => {
        if (typeof temp[ukey] === 'undefined') {
          temp[ukey] = fill_nonexistent_with_value;
        }
      });
      return temp;
    });
  }

  return fin;
};

export function nFormatter(num, digits, use_short?) {
  if (num === 0) {
    return '0';
  }
  const si = [
    { value: 1, symbol: '' },
    { value: 1e3, symbol: use_short ? 'K' : 'Thou' },
    { value: 1e6, symbol: use_short ? 'M' : 'Mio' },
    { value: 1e9, symbol: use_short ? 'B' : 'Bln' },
    { value: 1e12, symbol: use_short ? 'T' : 'Tln' }
  ];
  const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  let i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }
  return (
    (num / si[i].value).toFixed(digits).replace(rx, '$1') + ' ' + si[i].symbol
  );
}

export const premiumDeviation = (m: any, t: any, in_percent?: number) => {
  m = extractNumberFromString(m);
  t = extractNumberFromString(t);

  let r = ((m - t) / t) * 100;

  if (isNaN(r)) {
    r = 0;
  }

  if (!in_percent) {
    return r;
  }

  return `${roundUp(r, in_percent)} %`;
};

export const CURRENCY_PREFIX_ISO = () => {
  const selected_iso_currency = (window as any).SELECTED_ISO_CURRENCY;
  return selected_iso_currency || 'EUR';
};

export const CURRENCY_PREFIX_UTF = () => {
  const selected_iso_currency = (window as any).SELECTED_ISO_CURRENCY;
  if (selected_iso_currency) {
    return LISTS_OF_HISTOGRAM_CATEGORIES.mapOfISOcurreciesToUTF[
      selected_iso_currency
    ];
  }

  return LISTS_OF_HISTOGRAM_CATEGORIES.mapOfISOcurreciesToUTF.EUR;
};

/**
 * Invert the ISO->UTF currency hash map.
 * @returns
 */
export const mapOfUTFcurrenciesToISO = (() => {
  const isoToUtf = LISTS_OF_HISTOGRAM_CATEGORIES.mapOfISOcurreciesToUTF;
  return Object.assign({}, ...Object.entries(isoToUtf).map(([a, b]) => ({ [b]: a })));
})();

export function pprintCurrency(input, symbol = CURRENCY_PREFIX_UTF()) {
  const preform = currencyFormatter.format(extractNumberFromString(input), {
    symbol: symbol,
    decimal: '.',
    thousand: ',',
    precision: 0,
    format: symbol ? '%s %v' : '%v' // %s is the symbol and %v is the value
  });
  return preform;
}

export const numberWithCommas = x => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

export function pprintFloat(input) {
  return currencyFormatter.format(input, {
    symbol: '',
    decimal: ',',
    thousand: ' ',
    precision: 2,
    format: '%v' // %s is the symbol and %v is the value
  });
}

export const pprintLargePrice = (price, currency = CURRENCY_PREFIX_UTF()) => {
  return `${currency} ${nFormatter(extractNumberFromString(price), 3)}`;
};

export const extractNumberFromString = (str: any): number => {
  if (('' + str).includes('NaN')) {
    return NaN;
  }
  return Number(('' + str).replace(/[^0-9.-]+/g, ''));
};

export const isBetween = (x: number, min: number, max: number) => {
  return x >= min && x <= max;
};

export function compare(a, b) {
  if (a.last_nom < b.last_nom) {
    return -1;
  }
  if (a.last_nom > b.last_nom) {
    return 1;
  }
  return 0;
}

export const isLowercaseAtPos = (str: string, pos: number = 0) => {
  const first = str.charAt(pos);
  return first !== first.toUpperCase();
};

const modyfyingFunctions = {
  text: input => input,
  date: input => input, // potential todo
  price: input => pprintCurrency(input),
  float: input => pprintFloat(input),
  int: input => pprintFloat(input)
};

export const ESMappingToTextModifyingFunctions = {
  'Additional LoB Information (No. of vehicles / Size of group)':
    modyfyingFunctions.text,
  'CB+SME': modyfyingFunctions.text,
  Coinsured: modyfyingFunctions.text,
  'Country of Origin of the Request': modyfyingFunctions.text,
  createdDate: modyfyingFunctions.date,
  Creator: modyfyingFunctions.text,
  'Cross-border Business': modyfyingFunctions.text,
  'Details / Perils': modyfyingFunctions.text,
  'Distribution Channel*': modyfyingFunctions.text,
  'End Date': modyfyingFunctions.date,
  'Inception Date': modyfyingFunctions.date,
  'Industry Category*': modyfyingFunctions.text,
  'International Program/Fronting': modyfyingFunctions.text,
  'Line of Business (LoB)': modyfyingFunctions.text,
  'LoB Premium UNIQA retention': modyfyingFunctions.price,
  'Modified by': modyfyingFunctions.text,
  Month: modyfyingFunctions.text,
  'New/Existing Account': modyfyingFunctions.text,
  'Offer No': modyfyingFunctions.text,
  'Offer issuance Date': modyfyingFunctions.text,
  'Policy Holder': modyfyingFunctions.text,
  'Policy issuer In Core System': modyfyingFunctions.text,
  'Policy issuing Country': modyfyingFunctions.text,
  'Policy issuing Region': modyfyingFunctions.text,
  Premium: modyfyingFunctions.price,
  'Premium + row idx': modyfyingFunctions.price,
  'Premium Adjustment': modyfyingFunctions.text,
  'Public Tender': modyfyingFunctions.text,
  'Reason for declining': modyfyingFunctions.text,
  'Region of Origin': modyfyingFunctions.text,
  'Responsible local Underwriter': modyfyingFunctions.text,
  'Risk Class': modyfyingFunctions.text,
  'Sales Manager': modyfyingFunctions.text,
  'Sales Partner': modyfyingFunctions.text,
  Status: modyfyingFunctions.text,
  Subject: modyfyingFunctions.text,
  'Total PML': modyfyingFunctions.price,
  'Total Sum Insured': modyfyingFunctions.price,
  'Trade Register No.': modyfyingFunctions.int,
  'Type of Business': modyfyingFunctions.text,
  'UCB Confirmation needed': modyfyingFunctions.text,
  'UCB involved': modyfyingFunctions.text,
  'UNIQA Leading': modyfyingFunctions.text,
  'UNIQA PML': modyfyingFunctions.price,
  'UNIQA Share in %': modyfyingFunctions.int,
  'UNIQA new Policy No.': modyfyingFunctions.int,
  Year: modyfyingFunctions.int,
  city: modyfyingFunctions.text,
  coordinates: modyfyingFunctions.text,
  country: modyfyingFunctions.text,
  doc: modyfyingFunctions.text,
  region: modyfyingFunctions.text,
  street: modyfyingFunctions.text,
  zip: modyfyingFunctions.text
};

export function sortObject(object) {
  const sortedObj = {},
    keys = Object.keys(object);

  keys.sort(function (key1, key2) {
    (key1 = key1.toLowerCase()), (key2 = key2.toLowerCase());
    if (key1 < key2) {
      return -1;
    }
    if (key1 > key2) {
      return 1;
    }
    return 0;
  });

  for (const index in keys) {
    const key = keys[index];
    if (typeof object[key] === 'object' && !(object[key] instanceof Array)) {
      sortedObj[key] = sortObject(object[key]);
    } else {
      sortedObj[key] = object[key];
    }
  }

  return sortedObj;
}

export function sortObjKeysAlphabetically(
  obj,
  apply_es_mapping_transformations?
) {
  const ordered = {};
  if (!obj || !Object.keys(obj).length) {
    return ordered;
  }

  delete obj['id'];

  Object.keys(obj)
    .sort()
    .forEach(function (key) {
      if (
        apply_es_mapping_transformations &&
        key in ESMappingToTextModifyingFunctions
      ) {
        ordered[key] = ESMappingToTextModifyingFunctions[key].apply(null, [
          obj[key]
        ]);
      } else {
        ordered[key] = obj[key];
      }
    });
  return ordered;
}

export const highlightJSON = (inp, main_key, do_sort = true) => {
  const sorter = !do_sort
    ? o => o
    : o => {
      if (Array.isArray(o)) {
        return o;
      }
      return sortObject(o);
    };
  return {
    main_val: inp[main_key],
    html: new JSONPrettifier(sorter(inp)).outputHTML
  };
};

export function chunkArray(
  myArray: Array<any>,
  chunk_size: number
): Array<Array<any>> {
  let index = 0;
  const arrayLength = myArray.length;
  const tempArray = [];

  for (index = 0; index < arrayLength; index += chunk_size) {
    const myChunk = myArray.slice(index, index + chunk_size);
    // Do something if you want with the group
    tempArray.push(myChunk);
  }

  return tempArray;
}

export const splitWordIntoParts = (word: string) => {
  const chunks = chunkArray(word.split(''), 5);
  const res = chunks.map(chunk => {
    chunk = chunk.join('') as any;
    return chunk;
  });
  return res.join('-\n');
};

export const nthLetterOfAlphabet = (n: number): string => {
  const alpha = 'abcdefghijklmnopqrstuvwxyz';
  const large_index = n > alpha.length - 1;
  const index = large_index ? n % alpha.length : n;

  let base_to_return = alpha.split('')[index];
  if (large_index) {
    base_to_return += index;
  }
  return base_to_return;
};

export const jsonToKeyValueTable = (obj: Object) => {
  const rows = [];
  const extra_space_re = /^[A-Z]{1}[0-9]{1,2}$/; // match item keys like Aa, Bb, Cd etc

  for (const key of Object.entries(obj)) {
    if (isObject(key[1])) {
      const needs_extra_space = extra_space_re.test(key[0]);

      rows.push(`
        <th class=${needs_extra_space ? 'needs_extra_space' : ''}>${key[0]}</th>
        <td>${key[1].key}</td>
        <td>${key[1].count}</td>
        <td>${key[1].percentage}</td>
      `);
    } else {
      rows.push(`<th>${key[0]}</th><td>${key[1]}</td>`);
    }
  }

  const ready_rows = rows.map(item => {
    return `<tr class=${item.includes('needs_extra_space') ? 'needs_extra_space' : ''
      }>${item}</tr>`;
  });

  const table = `<hr><table>
  ${ready_rows.join('')}
  </table>`;

  return table;
};

export const addToArrayOfObjects = (arr, name, key, val) => {
  if (!arr.length) {
    arr.push({
      name: name,
      values: {
        [key]: val
      }
    });
    return arr;
  }

  for (let i = 0; i < arr.length; i++) {
    const elem = arr[i];
    if (elem.name === name) {
      arr[i].values[key] = val;
      break;
    } else {
      arr.push({
        name: name,
        values: {
          [key]: val
        }
      });
      break;
    }
  }
};

export const createLegendMapItem = (
  key,
  actual_count,
  doc_count,
  total,
  percFn,
  trailing_description?
) => {
  return {
    key: `${key}`,
    count: `${actual_count}`,
    percentage: `${percFn(doc_count, total, trailing_description)}`
  };
};

export const regexMatches = (rex: RegExp, val: any) => {
  if (!val) {
    return false;
  }
  if (!val.match) {
    val = `${val}`;
  }
  const tmp = val.match(rex);
  return tmp && tmp.length > 0;
};

export const safeStringForRegex = (str: string) => {
  return str.replace(/[-[\]{}()*+?.,\\^$|#:]/g, '\\$&');
};

export const uniq = a => {
  const prims = { boolean: {}, number: {}, string: {} },
    objs = [];

  return a.filter(function (item) {
    const type = typeof item;
    if (type in prims) {
      return prims[type].hasOwnProperty(item)
        ? false
        : (prims[type][item] = true);
    } else {
      return objs.indexOf(item) >= 0 ? false : objs.push(item);
    }
  });
};

const objSize = o => JSON.stringify(o).length;

const arrOfObjectsSize = (arr: Array<any>): Number =>
  arr.map(o => objSize(o)).reduce((a, b) => a + b);

export const splitArrayOfRequestsToReasonablyLargeChunks = (
  arr: Array<String>
): Array<Array<any>> => {
  const MAX_REASONABLE_LENGTH = 10;
  const new_arr = [];

  for (let i = 0; i < arr.length; i++) {
    const item = arr[i];

    if (!new_arr.length) {
      new_arr.push([item]);
      continue;
    }

    for (let j = 0; j < new_arr.length; j++) {
      const chunk = new_arr[j];
      if (arrOfObjectsSize(chunk) >= MAX_REASONABLE_LENGTH) {
        new_arr.push([item]);
        break;
      }

      new_arr[j].push(item);
    }
  }

  return new_arr;
};

export const extractInt = (s: string) => {
  const tmp = `${s}`;
  return parseInt(tmp.replace(/\D/g, ''), 10);
};

export const arrayToObject = (array, keyField, nested_key) =>
  array.reduce((obj, item) => {
    obj[item[keyField]] = item[nested_key];
    return obj;
  }, {});

export const isFalsey = (entity: any) => {
  return (
    typeof entity === 'undefined' ||
    entity === null ||
    String(entity).length === 0 ||
    isNaN(entity) ||
    entity === Infinity ||
    entity === -Infinity
  );
};

export const flattenArrays = (arrays: Array<Array<any>>): Array<any> => {
  return [].concat.apply([], arrays);
};

export function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    const r = (Math.random() * 16) | 0,
      v = c === 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

/**
 * A wrapper around the frequently used window.selected_location_peril
 * @returns string | undefined
 */
export const validLocationPeril = (): string | undefined => {
  // a truthy boolean of all 3 of these will return the actual peril string
  return (window as any).selected_location_risk_classes
    && (window as any).selected_location_risk_classes.length
    && (window as any).selected_location_peril;
};


/**
 * Attept to extract the peril-specifc location TSI
 * for use in hotspots and in the map markers
 * @param location_source
 * @param selected_location_peril
 * @returns { perilLimitValue, tsi }
 */
export const getPerilSpecificLocationTSI = (
  location_source: AddressesDoc,
  selected_location_peril = validLocationPeril()
): {
  perilLimitValue: number,
  tsi: number
} => {
  let perilLimitValue: number = null;

  // look for the corresponding limit value
  if (selected_location_peril) {
    try {
      perilLimitValue = (location_source.lob.rating.ratingToolPerils[
        selected_location_peril
      ] as RatingToolPeril)['limits']['Property Damage'][`limitValue_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`];
    } catch (err) {
      // no big deal -- we'll be defaulting to TSI per location either way
    }
  }

  const source_tsi = location_source.address[
    `tsiPerLocation_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`
  ];

  const tsi = selected_location_peril
    // ensure perilLimitValue is not null
    ? (perilLimitValue !== null ? perilLimitValue : source_tsi)
    : source_tsi;

  return { tsi, perilLimitValue };
};

