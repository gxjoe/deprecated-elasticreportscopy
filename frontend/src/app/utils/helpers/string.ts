import { capitalize } from "src/app/utils/helpers/helpers";

export function dotNotationToHumanReadable(input: string): string {
  const words = input.replace(/([a-z])([A-Z])/g, '$1 $2').split(/\.|_/);

  const transformedWords = words.map((word) => {
    return capitalize(word);
  });

  return transformedWords.join(' ');
}

export function splitWordsByCasing(input) {
  return input.replace(/([a-z])([A-Z])/g, '$1 $2');
}