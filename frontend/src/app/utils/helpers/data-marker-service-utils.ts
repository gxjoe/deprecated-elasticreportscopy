import { dynamicSort } from 'src/app/charts/utils';
import { AddressesDoc, LocationAssessment } from 'src/app/interfaces/elastic';

/**
 * When drawing an exportable polygon around points in the Score Matrix charts, it's useful
 * to extract the selected location peril assessment to the front of all assessments
 * because selecting a location peril is a prerequisite for running these charts in the first place.
 * Consolidated as part of UDGB-20.
 * @param hit
 * @returns
 */
export const extractSelectedLocationPerilAssessment = (hit: AddressesDoc) => {
    const applicableAssessment =
        hit.address.locationProperty.locationAssessments.find((assessment) => {
            return assessment.peril === (window as any).selected_location_peril;
        });

    // we won't need the peril because the CSV column covering this assessment's props
    // will include it in its name

    return {
        riskClass: applicableAssessment.riskClass,
        level: applicableAssessment.level,
        score: applicableAssessment.score,
        asIfScore: applicableAssessment.asIfScore,
    } as Omit<LocationAssessment, 'peril' | 'id' | 'adjustedScore'>;
};

/**
 * Location assessments A) aren't ordered and B) don't contain a fixed number of objects
 * because each location could have be covered by an arbitrary # of perils.
 * Consequently, when dumping all assessments in a CSV, the whole thing is unreadable and incomparable: https://prnt.sc/20292oz.
 * So, let's figure out the "longest" assessment, fill the current hit's assessments one by one,
 * and provide placeholders for those that aren't provided so that the result looks like this: https://prnt.sc/202an7f
 * @param targetHit
 * @param allHits
 * @returns
 */
export const unifyAndSortLocationPerilAssessments = (
    targetHit: AddressesDoc,
    allHits: AddressesDoc[]
): AddressesDoc['address']['locationProperty']['locationAssessments'] => {
    // the assessment containing the largest number of perils
    // i.e. the longest `locationAssessments` array.
    const mostComprehensiveAssessment = allHits
        .map((h) => h.address.locationProperty.locationAssessments)
        .sort((a1, a2) => a2.length - a1.length)[0];

    // alpha-sort for easier lookups
    const sortedMostComprehensiveAssessments = mostComprehensiveAssessment.sort(
        dynamicSort('peril')
    );

    // there'll probably be fewer assessments than what's available in `sortedMostComprehensiveAssessment`
    const targetHitAssessments =
        targetHit.address.locationProperty.locationAssessments;

    return sortedMostComprehensiveAssessments.map((assessment) => {
        // if the target assessment won't be available, make sure to fill the appropriate columns with a placeholder
        const placeholderAssessment: Omit<
            LocationAssessment,
            'id' | 'adjustedScore'
        > = {
            peril: assessment.peril,
            riskClass: null,
            level: null,
            score: null,
            asIfScore: null,
        };

        const found = targetHitAssessments.find(
            (a) => a.peril === assessment.peril
        );

        if (found) {
            placeholderAssessment.riskClass = found.riskClass;
            placeholderAssessment.level = found.level;
            placeholderAssessment.score = found.score;
            placeholderAssessment.asIfScore = found.asIfScore;
            return placeholderAssessment as LocationAssessment;
        }

        return placeholderAssessment as LocationAssessment;
    });
};
