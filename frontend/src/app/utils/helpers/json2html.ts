import { sortObjKeysAlphabetically } from './helpers';
import { JSONPrettifier } from './highlightJSON';
import { environment } from 'src/environments/environment';

export function json2table(json) {
  let body = '';

  Object.entries(json).map(([key, val]) => {
    body += `
                <tr>
                    <th>${key}</th>
                    <td>${val}</td>
                </tr>
            `;
  });

  return `<div class="json-table"><table style="width:100%">
            ${body}
        </table></div>`;
}

export function JSONstringify(key, json) {
  if (!environment.log_queries) {
    return json;
  }
  if (typeof json !== 'string') {
    json = JSON.stringify(json, undefined, '\t');
  }

  const arr = [],
    _string = 'color:green',
    _number = 'color:darkorange',
    _boolean = 'color:blue',
    _null = 'color:magenta',
    _key = 'color:red';

  json = json.replace(
    /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
    function (match) {
      let style = _number;
      if (/^"/.test(match)) {
        if (/:$/.test(match)) {
          style = _key;
        } else {
          style = _string;
        }
      } else if (/true|false/.test(match)) {
        style = _boolean;
      } else if (/null/.test(match)) {
        style = _null;
      }
      arr.push(style);
      arr.push('');
      return '%c' + match + '%c';
    }
  );

  arr.unshift(json);

  console.log.apply(console, arr);
}

export const prepareOfferJSONEntry = content => {
  return {
    'LoB ID': content.properties.ids.lob,
    'Offer ID': content.properties.ids.offer,
    ...sortObjKeysAlphabetically(content.properties.address_data),
    Address: new JSONPrettifier({ ...content.properties.other_data.address })
      .outputHTML,
    LoB: new JSONPrettifier({ ...content.properties.other_data.LOB }).outputHTML
  };
};

export const prepareClientJSONEntry = content => {
  return {
    Offer: new JSONPrettifier({ ...content.properties.other_data.Offer })
      .outputHTML,
    LoB: new JSONPrettifier({ ...content.properties.other_data.LOB }).outputHTML
  };
};

export const transformJSON2html = (
  content: Object,
  preparation_fn: Function
) => {
  const prepared_json = preparation_fn.apply(null, [content]);
  return json2table(prepared_json);
};
