export class JSONPrettifier {
  constructor(private content) {}

  highlightSyntax(json) {
    if (typeof json !== 'string') {
      json = JSON.stringify(json, undefined, 2);
    }

    json = json
      .replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;');
    return json.replace(
      /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
      function(match) {
        let cls = 'number';
        if (/^"/.test(match)) {
          if (/:$/.test(match)) {
            cls = 'key';
          } else {
            cls = 'string';
          }
        } else if (/true|false/.test(match)) {
          cls = 'boolean';
        } else if (/null/.test(match)) {
          cls = 'null';
        }

        // match the quotes & substrings the key-value pairs are wrapped within
        // "city": "Wien",
        // ^    ^^^^    ^
        const outstripped_match = match.replace(
          /(^[\"]{1,2})|([\"]{1}$)|(\":\s?\"?)/gm,
          function(partial_match) {
            return `<span class='op-10'>${partial_match}</span>`;
          }
        );

        return '<span class="' + cls + '">' + outstripped_match + '</span>';
      }
    );
  }

  get outputHTML() {
    // prettify spacing
    const pretty = JSON.stringify(this.content, null, 2);
    // syntaxhighlight the pretty print version
    return this.highlightSyntax(pretty);
  }
}
