export const roundUp = (num: number, decimals: number = 4) => {
  const precision = Math.pow(10, decimals);
  return Math.ceil(num * precision) / precision;
};

export const roundUpToNearestN = (inp, base_of_ten = false) => {
  if (!base_of_ten) {
    const n = ('' + inp).length - 1;
    return Math.ceil(inp / 10 ** n) * 10 ** n;
  }
  return Math.pow(10, Math.floor(inp).toString().length);
};
