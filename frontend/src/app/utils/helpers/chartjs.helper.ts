import { sortArrOfStringsAlphabetically, isFalsey } from './helpers';
import { getRandomInt, colorSchemeVividDomainAsRgbArray, palette_base } from 'src/app/charts/common/constants';
import { isNumber, lineString, simplify } from '@turf/turf';
import regression from 'regression';
import { dynamicSort } from 'src/app/charts/utils';

export const registerAfterRenderPlugin = () => {
  // borrowed from https://github.com/chartjs/Chart.js/issues/4098

  const exists =
    (window as any).Chart.plugins.getAll().filter(plug => {
      return plug.id === 'afterrender';
    }).length > 0;

  if (exists) {
    return;
  }

  const plugin = {
    id: 'afterrender',

    afterRender: function (chart, options) {
      console.info('after render in helper');
      chart.update({
        animation: {
          onComplete: function () { }
        }
      });
    }
  };

  (window as any).Chart.plugins.register(plugin);
};

export const _getValueAtIndexOrDefault = (value, index, defaultValue) => {
  if (value === undefined || value === null) {
    return defaultValue;
  }

  if (Array.isArray(value)) {
    return index < value.length ? value[index] : defaultValue;
  }

  return value;
};

interface IPairOfValuesWithHit
  extends Array<{
    x: number;
    y: number;
    hit?: any;
  }> { }

interface IScatterPlotDataset extends Array<{ x: number; y: number }> { }

export interface IAnnotationTrendlines
  extends Array<{
    label: string;
    data: IScatterPlotDataset;
    customAnnotations: any;
  }> { }

export const generateYearlyTrendlineDatasetsForScatterScoreMatrices = (
  pairs_of_values: IPairOfValuesWithHit,
  simplify = false
): IAnnotationTrendlines => {
  const grouped_by_years: {
    [year: string]: IPairOfValuesWithHit;
  } = {};

  pairs_of_values.map(pair => {
    try {
      // year coming in as "2018/07/02 08:58:24"
      const year = pair.hit.offer.createdDate.split('/')[0];
      if (year in grouped_by_years) {
        grouped_by_years[year].push(pair);
      } else {
        grouped_by_years[year] = [pair];
      }
    } catch (err) { }
  });

  return sortArrOfStringsAlphabetically(Object.keys(grouped_by_years))
    .filter(year => grouped_by_years[year].length > 1) // prevent error `coordinates must be an array of two or more positions`
    .map((year: string, index: number) => {
      const leastSquares = findLineByLeastSquares(grouped_by_years[year], simplify);
      const sorted_y_values = leastSquares.map(pair => pair.y);

      const lowestYval = Math.min.apply(null, sorted_y_values);
      const highestYval = Math.max.apply(null, sorted_y_values);

      return {
        label: year,
        data: leastSquares,
        customAnnotations: {
          drawTime: 'afterDatasetsDraw',
          id: 'hline' + index,
          type: 'line',
          mode: 'horizontal',
          scaleID: 'normal_y_axis',
          value: lowestYval,
          endValue: highestYval,
          borderColor: palette_base[getRandomInt(1, palette_base.length - 1)],
          borderWidth: 3,
          label: {
            backgroundColor: 'rgba(40,40,40,0.6)',
            content: year,
            enabled: true
          },
          onClick: function (e) {
            // The annotation is is bound to the `this` variable
            console.log('Annotation', e.type, this);
          }
        }
      };
    });
};

/**
 * modified from https://dracoblue.net/dev/linear-least-squares-in-javascript/
 * @param pairs_of_values
 */
export const findLineByLeastSquares = (
  pairs_of_values: IPairOfValuesWithHit,
  simplify = false
): IScatterPlotDataset => {
  const values_x: Array<number> = [];
  const values_y: Array<number> = [];

  const combined = [];

  pairs_of_values.map(pair => {
    if (isNumber(pair.x) && isNumber(pair.y)) {
      values_x.push(pair.x);
      values_y.push(pair.y);
    }
  });

  pairs_of_values
    .filter(pair => {
      return isNumber(pair.x) && isNumber(pair.y);
    })
    .sort(dynamicSort('-x'))
    .map(pair => {
      combined.push([pair.x, pair.y]);
    });

  const reg_res = regression.linear(combined, { precision: 10 });
  if (reg_res && reg_res.points.length) {
    const points = reg_res.points.map(group => {
      return {
        x: group[0],
        y: group[1]
      };
    });

    return simplify ? simplifyLineUsingTurf(points) : points;
  }

  let sum_x = 0;
  let sum_y = 0;
  let sum_xy = 0;
  let sum_xx = 0;
  let count = 0;

  /*
   * We'll use those variables for faster read/write access.
   */
  let x = 0;
  let y = 0;
  const values_length = values_x.length;

  if (values_length !== values_y.length) {
    throw new Error('The parameters values_x and values_y need to have same size!');
  }

  /*
   * Nothing to do.
   */
  if (values_length === 0) {
    return [];
  }

  /*
   * Calculate the sum for each of the parts necessary.
   */
  for (let v = 0; v < values_length; v++) {
    x = values_x[v];
    y = values_y[v];
    sum_x += x;
    sum_y += y;
    sum_xx += x * x;
    sum_xy += x * y;
    count++;
  }

  /*
   * Calculate m and b for the formular:
   * y = x * m + b
   */
  const m = (count * sum_xy - sum_x * sum_y) / (count * sum_xx - sum_x * sum_x);
  const b = sum_y / count - (m * sum_x) / count;

  /*
   * We will make the x and y result line now
   */
  const result_values_x = [];
  const result_values_y = [];

  for (let v = 0; v < values_length; v++) {
    x = values_x[v];
    y = x * m + b;
    result_values_x.push(x);
    result_values_y.push(y);
  }

  const result: IScatterPlotDataset = [];

  result_values_x.map((_x: number, index) => {
    result.push({
      x: _x,
      y: result_values_y[index]
    });
  });

  return simplify ? simplifyLineUsingTurf(result) : result;
};

const simplifyLineUsingTurf = (line_coords: IScatterPlotDataset): IScatterPlotDataset => {
  const options = { tolerance: 0.01, highQuality: true };
  const filtered_coords = line_coords.filter(pair => !isNaN(pair.x) && !isNaN(pair.y)).map(pair => [pair.y, pair.x]);
  if (!filtered_coords.length) {
    return [{ x: 0, y: 0 }, { x: 0, y: 0 }];
  }

  const polyline = lineString(filtered_coords);

  try {
    const simplified = simplify(polyline, options);
    return simplified.geometry.coordinates.map(coords => {
      return {
        x: coords[1],
        y: coords[0]
      };
    });
  } catch (err) {
    console.warn('simplifyLineUsingTurf failed', { err, polyline, filtered_coords, line_coords });
    return polyline.geometry.coordinates.map(([y, x]) => {
      return {
        x, y
      };
    });
  }
};
