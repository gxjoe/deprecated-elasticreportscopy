/// <reference types="@types/googlemaps" />

import * as DeckGoogleMaps from '@deck.gl/google-maps';
import { HexagonLayer } from '@deck.gl/aggregation-layers';

const DeckOverlay = DeckGoogleMaps.GoogleMapsOverlay;

export enum DECK_OVERLAYS {
  TSI_HEXAGON = 'TSI_HEXAGON'
}

const uiConfig = (overlay_name: DECK_OVERLAYS) => {
  const hexagonColorRange = [
    [1, 152, 189],
    [73, 227, 206],
    [216, 254, 181],
    [254, 237, 177],
    [254, 173, 84],
    [209, 55, 78]
  ];

  switch (overlay_name) {
    case DECK_OVERLAYS.TSI_HEXAGON:
      return {
        // colorRange: hexagonColorRange,
        coverage: 1,
        upperPercentile: 90,
        radius: 1e3
        // material: new PhongMaterial({
        //   ambient: 0.64,
        //   diffuse: 0.6,
        //   shininess: 32,
        //   specularColor: [51, 51, 51]
        // })
      };
  }
};

class OverlayStore {
  private store: {
    [oname: string]: typeof DeckOverlay;
  } = {};

  add(oname: string, overlay: typeof DeckOverlay) {
    this.remove(oname);
    this.store[oname] = overlay;
  }

  remove(oname: string) {
    if (oname in this.store) {
      this.store[oname].setMap(null);
      this.store[oname].finalize();
    }
  }
}

const store = new OverlayStore();

function getMean(points, accessor_fn) {
  return (
    points.reduce((sum, p) => (sum += accessor_fn(p)), 0) / points.reduce((a, b) => accessor_fn(a) + accessor_fn(b))
  );
}

const accessTSI = point => {
  if (!point || !point.properties) {
    return 0;
  }
  return point.properties['lob.totalSumInsured_EUR_AdjustedForShare'] || 0;
};

export const buildDeckOverlay = (overlay_name, data) => {
  let overlay;

  switch (overlay_name) {
    case DECK_OVERLAYS.TSI_HEXAGON:
      // Create overlay instance
      overlay = new DeckOverlay({
        layers: [
          new HexagonLayer({
            id: 'heatmap',

            data,
            elevationRange: [0, Infinity],
            elevationScale: data && data.length ? 50 : 0,
            extruded: true,
            getPosition: d => {
              return d.geometry.coordinates;
            },
            onHover: ({ object, x, y }) => {
              console.info({
                object,
                x,
                y
              });
              //   const tooltip = `${object.centroid.join(', ')}\nCount: ${object.points.length}`;
              //   console.warn(tooltip);
              /* Update tooltip
                   http://deck.gl/#/documentation/developer-guide/adding-interactivity?section=example-display-a-tooltip-for-hovered-object
                */
            },
            getElevationValue: function (points) {
              return points.reduce((max, p) => (accessTSI(p) > max ? accessTSI(p) : max), -Infinity);
            },
            getFillColor: d => [48, 128, (accessTSI(d) / 10e9) * 255, 255],

            opacity: 1,
            pickable: true,
            transitions: {
              elevationScale: 3000
            },
            ...uiConfig(overlay_name)
          })
        ]
      });
      break;
  }

  if (overlay) {
    store.add(overlay_name, overlay);
  }

  return overlay;
};
