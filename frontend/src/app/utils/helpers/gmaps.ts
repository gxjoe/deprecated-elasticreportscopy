import { point } from '@turf/helpers';
import circle from '@turf/circle'
import pointsWithinPolygon from '@turf/points-within-polygon'
import { nFormatter } from './helpers';
import { invertColor } from 'src/app/charts/common/constants';

export function encodeSVG(data) {
  const symbols = /[\r\n%#()<>?\[\\\]^`{|}]/g;

  // Use single quotes instead of double to avoid encoding.

  data = data.replace(/'/g, '"');

  data = data.replace(/>\s{1,}</g, '><');
  data = data.replace(/\s{2,}/g, ' ');

  return data.replace(symbols, encodeURIComponent);
}

export const generateSvgIcon = (
  color,
  opacity,
  count,
  include_red_point = false,
  scaling_factor = 1,
  manual_r?
) => {
  // shoutout https://stackoverflow.com/a/10477334/8160318
  // const r = 7;

  const scaled_count = scaling_factor * count;
  let r: number;

  if (!manual_r) {
    const _r = Math.min(Math.log(Math.sqrt(count)) + 7, 15);
    if (scaled_count <= 50e3) {
      r = 10;
    } else if (scaled_count > 50e3 && scaled_count <= 500e3) {
      r = 12;
    } else if (scaled_count > 500e3 && scaled_count <= 10e6) {
      r = 14;
    } else if (scaled_count > 10e6 && scaled_count <= 50e6) {
      r = 17;
    } else {
      r = 19;
    }
  } else {
    r = manual_r;
  }

  const svg_path_no_text = {
    path: `
    M h h
    m -r, 0
    a r,r 0 1,0 sq, 0
    a r,r 0 1,0 -sq, 0
`
      .replace(/\r?\n|\r/g, '')
      .replace(/[h]/gm, String(r / 2))
      .replace(/[r]/gm, String(r))
      .replace(/(sq)/gm, String(r * 2)),
    fillColor: color,
    fillOpacity: 0.85,
    anchor: new google.maps.Point(r / 2, r / 2),
    strokeColor: '#EEEEEE',
    strokeWeight: 0.75,
    scale: 1 // Math.pow(count, 2)
  };

  const offset_radius = r + 6;

  const url_svg_with_text = {
    anchor: new google.maps.Point(offset_radius, offset_radius),
    url: encodeSVG(`data:image/svg+xml;utf-8,
          <svg width="${offset_radius * 2}px" height="${offset_radius *
      2}px" xmlns="http://www.w3.org/2000/svg"
            xmlns:xlink="http://www.w3.org/1999/xlink"
            xml:space="preserve"
            style="image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
            viewBox="0 0 ${offset_radius * 2}px ${offset_radius * 2}px">
              <g>
                <circle style="fill:${color};
                        stroke:#282828;
                        stroke-width:1px;"
                        cx="50%"
                        cy="50%"
                        r="${r + 3}px">
                </circle>
                <text alignment-baseline="middle"
                      x="50%"
                      y="53%"
                      text-anchor="middle"
                      font-family="sans-serif"
                      fill="${invertColor(color, true)}"
                      font-size="12px">
                      ${nFormatter(count, 0, true).replace(/\s/g, '')}
                </text>
                ${include_red_point
        ? `
                <circle style="fill:${invertColor(color, true)};
                        stroke:${color};
                        stroke-width:1px;"
                        cx="65%"
                        cy="15%"
                        r="4px"></circle>
                        `
        : ''
      }
              </g>
        </svg>
        `)
  };

  return url_svg_with_text;
};

export const polyLineTransparentStrokeOptions = {
  strokeOpacity: 0.3,
  strokeWeight: 1.5
};

export const polyLineFullStrokeOptions = {
  strokeOpacity: 1,
  strokeWeight: 1.5
};

export const generatePolylineOptions = color => {
  return {
    geodesic: true,
    strokeColor: color,
    ...polyLineTransparentStrokeOptions
  };
};

export const generatePolyline = (point_collection, color) => {
  const line = new google.maps.Polyline({
    path: point_collection.map(point => {
      return { lat: point[1], lng: point[0] };
    }),
    geodesic: false,
    strokeColor: color,
    ...polyLineTransparentStrokeOptions
  });

  google.maps.event.addListener(line, 'mouseover', function () {
    line.setOptions({ ...polyLineFullStrokeOptions });
  });

  google.maps.event.addListener(line, 'mouseout', function () {
    line.setOptions({ ...polyLineTransparentStrokeOptions });
  });

  return line;
};

export const isPointInCircle = (
  check_point: google.maps.LatLng,
  center_point: google.maps.LatLng,
  radius_in_m: number
) => {
  const polygonish_circle = circle(
    point([center_point.lng(), center_point.lat()]),
    radius_in_m,
    {
      steps: 50,
      units: 'meters'
    }
  );

  return (
    pointsWithinPolygon(
      point([check_point.lng(), check_point.lat()]),
      polygonish_circle
    ).features.length > 0
  );
};

export const isPointInPolygon = (
  polygon: google.maps.Polygon,
  point: google.maps.LatLng
): boolean => {
  return google.maps.geometry.poly.containsLocation(point, polygon);
};

export const mapDrawingContainsLatLng = (
  mapDrawing: {
    [data: string]: google.maps.Polygon | google.maps.Circle | any;
  },
  point: google.maps.LatLng
) => {
  if ((mapDrawing.type as string) === 'polygon') {
    return isPointInPolygon(mapDrawing.data, point);
  } else {
    return isPointInCircle(
      point,
      mapDrawing.data.getCenter(),
      mapDrawing.data.getRadius()
    );
  }
};
