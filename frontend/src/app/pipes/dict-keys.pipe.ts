import { Pipe, PipeTransform } from '@angular/core';
import { isObject } from '../utils/helpers/helpers';

@Pipe({
  name: 'dictKeys'
})
export class DictKeysPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (isObject(value)) {
      return Object.keys(value);
    }
    return value;
  }

}
