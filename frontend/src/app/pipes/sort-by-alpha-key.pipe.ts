import { Pipe, PipeTransform } from '@angular/core';
import { sortObjKeysAlphabetically } from '../utils/helpers/helpers';
import { JSONPrettifier } from '../utils/helpers/highlightJSON';

@Pipe({
  name: 'sortByAlphaKey'
})
export class SortByAlphaKeyPipe implements PipeTransform {
  transform(
    value: any,
    extract_key_as_first?: any,
    apply_es_mapping_transformations?: boolean,
    pprint_as_json?: boolean
  ): any {
    const ordered = {};
    if (extract_key_as_first) {
      try {
        ordered[extract_key_as_first] = value[extract_key_as_first];
        if (extract_key_as_first in value) {
          delete value[extract_key_as_first];
        }
      } catch (err) {
        console.warn(err);
      }
    }

    if (pprint_as_json) {
      return new JSONPrettifier(
        Object.assign(ordered, sortObjKeysAlphabetically(value, apply_es_mapping_transformations))
      ).outputHTML;
    }

    return Object.assign(ordered, sortObjKeysAlphabetically(value, apply_es_mapping_transformations));
  }
}
