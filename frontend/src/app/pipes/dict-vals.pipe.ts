import { Pipe, PipeTransform } from '@angular/core';
import { isObject } from '../utils/helpers/helpers';

@Pipe({
  name: 'dictVals'
})
export class DictValsPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (isObject(value)) {
      return Object.values(value);
    }
    return value;
  }

}
