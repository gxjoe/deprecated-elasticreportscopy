import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'enumerate'
})
export class EnumeratePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.map((item, index) => {
      return {
        item: item,
        index: index
      }
    })
  }

}
