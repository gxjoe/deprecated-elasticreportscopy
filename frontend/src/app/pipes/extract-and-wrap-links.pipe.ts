import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'extractAndWrapLinks'
})
export class ExtractAndWrapLinksPipe implements PipeTransform {

  /**
   * Transforms the value `abd www.a.com` to `abd <a href="...">...</a>`
   * @param value
   */
  transform(value: string): string {
    return value.replace(/((http|ftp|https):\/\/)?([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?/gm,
     `<a href="\$&" target="_blank">\$&</a>`
     );
  }

}
