export interface OnIncomingData {
  handleIncomingData(data: any): void;
}
