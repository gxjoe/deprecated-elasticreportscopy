export interface ImapDrawing {
  type: 'polygon' | 'circle' | null;
  path: any;
}
