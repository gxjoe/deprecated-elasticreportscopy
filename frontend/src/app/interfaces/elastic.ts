import { LISTS_OF_HISTOGRAM_CATEGORIES } from '../charts/histogram-table-chart/histogramListsAndCategories';

export interface LobDoc {
  lob: Lob;
  offer: Offer;
  id: number;
  addresses: Address[];
}

export interface AddressesDoc {
  lob: Lob;
  offer: Offer;
  id: number;
  address: Address;
}

export interface Address {
  ratingValues: any[];
  street: string;
  sumInsured_EUR_AdjustedForShare: number;
  premiumBasisPerLocation_UAH: number;
  id: number;
  premiumBasisPerLocation_UAH_AdjustedForShare: number;
  city: string;
  sumInsured_UAH: number;
  coordinatesVerified: boolean;
  coordinates: string;
  tsiPerLocation_UAH: number;
  coordinatesStatus: string;
  sumInsured_UAH_AdjustedForShare: number;
  tsiPerLocation_UAH_AdjustedForShare: number;
  zipCode: string;
  premiumBasisPerLocation_EUR: number;
  tsiPerLocation_EUR: number;
  houseNumber: string;
  tsiPerLocation_EUR_AdjustedForShare: number;
  locationProperty: LocationProperty;
  sumInsured_EUR: number;
  country: string;
  region: string;
  premiumBasisPerLocation_EUR_AdjustedForShare: number;
}

export interface LocationProperty {
  EML_EUR: number;
  numberOfStories: number;
  EML_EUR_AdjustedForShare: number;
  EML_UAH: number;
  basementFloors: number;
  PML_EUR_AdjustedForShare: number;
  globalLocationDataCreatedBy: null;
  locationAssessments: LocationAssessment[];
  shapeOfBuilding: string;
  locationAssessmentsLevel3: LocationAssessmentsLevel3;
  buildingType: string;
  numberOfBuildings: number;
  PML_UAH_AdjustedForShare: number;
  id: number;
  yearOfConstruction: number;
  roofGeometry: string;
  PML_UAH: number;
  EML_UAH_AdjustedForShare: number;
  PML_EUR: number;
  constructionMaterial: string;
  roofSystem: string;
}

export interface LocationAssessment {
  peril: PerilName;
  level: number;
  riskClass: number;
  score: number;
  adjustedScore: number;
  id: number;
  asIfScore?: number;
}

export type PerilName =
  typeof LISTS_OF_HISTOGRAM_CATEGORIES.listOfPerilNames[number];

export type LocationAssessmentsLevel3 = {
  [key in PerilName]: PerilAssessment;
};

export interface PerilAssessment {
  keyFactors: KeyFactors;
  totalScore: number;
  peril: PerilName;
  riskClass: number;
}

export type KeyFactorType = 'potential' | 'probability' | 'prevention';
export type KeyFactors = {
  [key in KeyFactorType]: SomeKeyFactor;
};

export interface SomeKeyFactor {
  asIfScore: number;
  averageScore: number;
  weight: number;
  keyCriteria: KeyCriterion;
  keyCriteriaAsList: KeyCriterionAssessment[];
  score: number;
  type: string;
}

export interface KeyCriterion {
  [key: string]: KeyCriterionAssessment;
}

export interface KeyCriterionAssessment {
  asIfScore: number;
  name: string;
  weight: number;
  text: null | string;
  positiveAspects: ActualAssessmentRecommendation[];
  negativeAspects: NegativeAspect[];
  recommendations: AssessmentRecommendation[];
  score: number;
}

export interface NegativeAspect extends AssessmentRecommendation {
  negativeAspect: string;
  negativeAspect_label: string;
}

export interface ActualAssessmentRecommendation {
  validTo: string | null;
  validFrom: string;
  name: string;
  name_label?: string;
  aspect?: string;
  aspect_label?: string;
  description: Description | null;
}

export interface Description {
  [key: string]: string;
}

export interface AssessmentRecommendation extends ActualAssessmentRecommendation {
  scenario: null | string;
  recommendationSequence: Date;
  priority: Priority;
  recommendation: string | null;
  statusFeedback: StatusFeedback;
}

export enum Priority {
  must = 'must',
  important = 'important',
  advisory = 'advisory',
}

export enum StatusFeedback {
  new = 'new',
  inProgress = 'inProgress',
  inDiscussion = 'inDiscussion',
  declined = 'declined',
  clientFeedback = 'clientFeedback',
  visited = 'visited',
}

export interface Lob {
  rating: Rating;
  endDate: string;
  share: number;
  currency: string;
  premium_EUR: number;
  rating_UAH_adjustedPremiumsRatio_AdjustedForShare: number;
  id: number;
  reasonForDeclining: string;
  premiumPrevious_EUR: number;
  typeOfBusiness: string[];
  newPolicyNumber: string;
  rating_UAH_adjustedPremiumsRatio: number;
  totalSumInsured_EUR_AdjustedForShare: number;
  riskClass: number;
  localUnderwriters: LocalUnderwriter[];
  status: string;
  reasonForDecline: string;
  totalSumInsured_UAH: number;
  totalSumInsured_EUR: number;
  uniqaLeading: boolean;
  exchangeRate: number;
  premiumPrevious_UAH_AdjustedForShare: number;
  policyIssuance: string;
  premium_UAH: number;
  premiumPrevious_EUR_AdjustedForShare: number;
  lineOfBusiness: string;
  premiumPrevious_UAH: number;
  rating_EUR_adjustedPremiumsRatio_AdjustedForShare: number;
  rating_EUR_adjustedPremiumsRatio: number;
  premium_EUR_AdjustedForShare: number;
  totalSumInsured_UAH_AdjustedForShare: number;
  salesPartner: any[];
  accountStatus: string;
  risks: RiskClass[];
  uniqaPML_EUR: number;
  premium_UAH_AdjustedForShare: number;
  uniqaPML_UAH: number;
  beginDate: string;
  uniqaPML_UAH_AdjustedForShare: number;
}

export interface LocalUnderwriter {
  username: string;
  city: null | string;
  roles: string[];
  countryCode: string;
  address: null | string;
  department: string;
  organization: null;
  fullName: string;
  id: number;
}

export interface Rating {
  status: string;
  superScoredTotalSumInsured_EUR_AdjustedForShare: number;
  totalSumInsured_UAH: number;
  totalSumInsured_EUR: number;
  superScore: number;
  brokerCommission: number;
  totalPremium_EUR_AdjustedForShare: number;
  superScoredTotalSumInsured_UAH_AdjustedForShare: number;
  mTotalPremium_EUR: number;
  versionNumber: number;
  id: number;
  mTotalPremium_UAH_AdjustedForShare: number;
  mTotalPremium_EUR_AdjustedForShare: number;
  totalPremium_UAH_AdjustedForShare: number;
  totalPremium_UAH: number;
  totalSumInsured_EUR_AdjustedForShare: number;
  totalPremium_EUR: number;
  mTotalPremium_UAH: number;
  ratingToolPerils: { [key in PerilName]: RatingToolPeril };
  personalDeviationAuthority: number;
  totalSumInsured_UAH_AdjustedForShare: number;
}

export interface RatingToolPeril {
  personalDeviationAuthority: number;
  mTotalPremium_EUR: number;
  avgRiskClass: number;
  deductibleRisks: DeductibleRiskElement[];
  id: number;
  premiumBasis_EUR: number;
  premiumBasis_UAH_AdjustedForShare: number;
  totalSI_EUR_AdjustedForShare: number;
  mTotalPremium_UAH_AdjustedForShare: number;
  totalPremium_EUR: number;
  mTotalPremium_UAH: number;
  totalPremium_UAH_AdjustedForShare: number;
  avgAsIfScore: number;
  deductibleValue_EUR: number;
  premiumBasis_EUR_AdjustedForShare: number;
  totalSI_EUR: number;
  deductibleValue_EUR_AdjustedForShare: number;
  deductibleValue_UAH: number;
  totalSI_UAH: number;
  peril: PerilName;
  avgScore: number;
  totalPremium_EUR_AdjustedForShare: number;
  mTotalPremium_EUR_AdjustedForShare: number;
  premiumBasis_UAH: number;
  totalPremium_UAH: number;
  deductibleType: DeductibleType;
  limitsAsList: Array<{
    [key in `limitValue_${string}_AdjustedForShare`]: number
  }>;
  limits: {
    'Property Damage': {
      [key in `limitValue_${string}_AdjustedForShare`]: number
    }
  };
}

export enum DeductibleRiskElement {
  PD = 'PD',
}

export enum DeductibleType {
  FixedAmount = 'fixed_amount',
}

export interface RiskClass {
  premiumBasis_EUR: number;
  risk: DeductibleRiskElement;
  sumInsured_EUR: number;
  perils: Peril[];
  sumInsured_UAH: number;
  premiumExclTax_EUR: number;
  premiumBasis_UAH_AdjustedForShare: number;
  sumInsured_UAH_AdjustedForShare: number;
  premiumExclTax_UAH: number;
  insuredObjects: { [key: string]: InsuredObject };
  sumInsured_EUR_AdjustedForShare: number;
  premiumExclTax_UAH_AdjustedForShare: number;
  premiumBasis_EUR_AdjustedForShare: number;
  premiumBasis_UAH: number;
  premiumExclTax_EUR_AdjustedForShare: number;
  id: number;
}

export interface InsuredObject {
  value_EUR: number | null;
  value_UAH: number | null;
  sumInsured_EUR: number | null;
  sumInsured_UAH: number | null;
  annualValue_EUR: number | null;
  valuationBasis: null | string;
  annualValue_UAH: number | null;
  type: string;
  indemnityBasis: null | string;
}

export interface Peril {
  riskDescription: string;
  sumInsured_EUR: null;
  sumInsured_UAH: null;
  limitPerEvent_UAH: null;
  limitInAggreagate_EUR: null;
  limitPerEvent_EUR: null;
  id: number;
  limitInAggreagate_UAH: null;
}

export interface Offer {
  riskDescription: string;
  averageScore: number;
  createdDate: string;
  number: string;
  regionalDirectorate: string;
  salesPartner: SalesPartner[];
  publicTender: boolean;
  uicbInvolved: boolean;
  id: number;
  subjectiveAssessment: number;
  regionalDirectorate2: string;
  draft: boolean;
  coinsured: any[];
  cbOrSme: string;
  internationalProgram: boolean;
  crossBorderBusiness: boolean;
  createdBy: string;
  updatedBy: string;
  salesManagers: LocalUnderwriter[];
  issuanceDate: null;
  distributionChannel: string;
  industry: string[];
  uicbConfirmNeeded: boolean;
  responseTimes: ResponseTimes;
  updatedDate: string;
  client: Client;
  premiumInEur: number;
  requestFromCountry: string;
  internalInfo: InternalInfo[];
}

export interface Client {
  city: string;
  premium: null;
  name: string;
  GVpremium: null;
  country: string;
  region: string;
  revenueInEuro: number;
  zipCode: string;
  coordinates: string;
  nacePrimaryCodes: string[];
  tradeRegisterNumber: null;
  street: string;
  houseNumber: null;
  id: number;
  naceSecondaryCodes: string[];
  esg: Esg;
  co2: Co2
}

export interface Co2 {
  valuationId:         string;
  emission:            number;
  emissionUnit:        string;
  carbonIntensity:     number;
  carbonIntensityUnit: string;
}

export interface Esg {
  valuationId: string;
  criteria:    Criterion[];
}

export interface Criterion {
  shortName: string;
  longName:  string;
  areaName:  AreaName;
  areaCode:  AreaCode;
  themeCode: string;
  themeName: string;
  riskCode:  string;
  riskName:  RiskName;
}

export enum AreaCode {
  Environmental = "ENVIRONMENTAL",
  Governance = "GOVERNANCE",
  Social = "SOCIAL",
}

export enum AreaName {
  Environmental = "Environmental",
  Governance = "Governance",
  Social = "Social",
}

export enum RiskName {
  NotApplicable = "Not applicable",
  PotentialElevatedRisk = "Potential elevated risk",
  PotentialHighOrDirectRisk = "Potential high or direct risk",
  PotentialRisk = "Potential risk",
}

export interface InternalInfo {
  offerLobId: number;
  created: string;
  offerId: number;
  state: string;
  manualStateChangeDate: null | string;
  createdBy: string;
  action: null | string;
  responseTimeInSec: number | null;
  id: number;
  previousState: null | string;
}

export interface ResponseTimes {
  vbgvkm01: number;
}

export interface SalesPartner {
  agentId: number;
  name: string;
  agentNumber: string;
}
