import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { RelativeDatepickerComponent, ItsValues } from './relative-datepicker/relative-datepicker.component';
import { GmapComponent } from './gmap/gmap.component';

import { CheckboxComponent } from './checkbox/checkbox.component';
import { Router, NavigationEnd } from '@angular/router';

import { AuthService } from './services/auth.service';
import { DATEPICKERS_TO_FIELDS } from './elastic/queries';

import packageJson from '../../package.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  public version: string = packageJson.version;

  constructor(private router: Router, private auth: AuthService) {
    if ((window).location.href.includes('gx')) {
      document.body.classList.add('is_demo');
    }

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.flushInnerComponents();
      }
    });

    (window as any).app = this;
    (window as any).selected_peril = null;
    (window as any).selected_location_peril = null;
  }

  public filterComponents = [];
  public textCardComponents = [];

  public OfferDistributionHeatmapComponent: GmapComponent;
  public ClientDistributionHeatmapComponent: GmapComponent;
  public ExperimentalHeatmapComponent: GmapComponent;

  public startDateComponent: RelativeDatepickerComponent;
  public endDateComponent: RelativeDatepickerComponent;
  public interactiveSearchCheckboxComponent: CheckboxComponent;

  private innerComponentsIDsList = [];
  private innerComponents = [];

  ngOnInit() {
    document.body.setAttribute("data-release", this.version);
  }

  setComponent(klass: string, component: any) {
    if (!this.innerComponentsIDsList.includes(klass)) {
      this.innerComponentsIDsList.push(klass);
    }
    const klass_pair = `${klass}Component`;
    if (!this[klass_pair]) {
      this[klass_pair] = component;
      this.innerComponents.push(component);
    }
  }

  unsetComponent(klass: string) {
    if (this.innerComponentsIDsList.includes(klass)) {
      this.innerComponentsIDsList = this.innerComponentsIDsList.filter(it => it.klass !== klass);
    }
    delete this[klass + 'Component'];
    try {
      this.innerComponents.splice(this.innerComponents.findIndex(comp => comp.klass === klass), 1);
    } catch (err) { }
  }

  getComponentByKlass(klass) {
    if (!klass.includes('Component')) {
      klass = `${klass}Component`;
    }

    return this[klass];
  }

  flushInnerComponents() {
    this.innerComponentsIDsList.map(ckey => {
      delete this[`${ckey}Component`];
      this[`${ckey}Component`] = null;
    });

    this.textCardComponents = [];
    this.filterComponents = [];
  }

  public getMins1YsoleActiveDatePickerComponent(): {
    error: string,
    picker_fields: typeof DATEPICKERS_TO_FIELDS[keyof typeof DATEPICKERS_TO_FIELDS],
    minus1yearValues: ItsValues
  } {
    const date_components = this.innerComponents.filter(comp => comp.klass.includes('Datepicker')) as RelativeDatepickerComponent[];
    let applicable_components = date_components.filter(comp => {
      const { start, end } = comp.tsValues;
      return start && end;
    });

    if (applicable_components.length === 2) {
      return {
        error: 'At most ONE date filter is permitted.',
        picker_fields: null,
        minus1yearValues: null
      };
    }

    if (!applicable_components.length) {
      // default to offer created, however empty -- the `minus1year` delegator
      // on the relative picker component takes this possibility into account
      applicable_components = [this['OfferCreatedDateDatepickerComponent']];
    }

    const target_component = applicable_components[0];

    return {
      error: null,
      picker_fields: DATEPICKERS_TO_FIELDS[target_component.klass],
      minus1yearValues: target_component.minus1year()
    };
  }
}
