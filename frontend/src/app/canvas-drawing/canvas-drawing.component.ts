import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { CanvasDrawingService } from '../services/canvas-drawing.service';
import { DivDownloaderService } from '../services/div-downloader.service';

@Component({
  selector: 'app-canvas-drawing',
  templateUrl: './canvas-drawing.component.html',
  styleUrls: ['./canvas-drawing.component.scss']
})
export class CanvasDrawingComponent implements OnInit {
  @Input() klass: string;
  @Input() canvas_to_draw: HTMLCanvasElement;
  @Input() clientReportCsv: string;

  @ViewChild('downloadClientReportCsv', { static: false }) downloadClientReportCsv: any;

  canvas_copy: HTMLCanvasElement;

  constructor(
    private drawingService: CanvasDrawingService,
    private divService: DivDownloaderService
  ) { }

  ngOnInit() { }

  begin() {
    this.canvas_copy = this.drawingService.begin(
      this.canvas_to_draw,
      this.klass
    );
  }

  get canClosePath() {
    return this.drawingService.canClosePath();
  }

  undo() {
    this.drawingService.undo();
  }

  trash() {
    this.drawingService.clear_canvas();
  }

  done() {
    this.drawingService.close();
  }

  downloadClientReport() {
    this.divService.downloadRegularCSV(
      this.downloadClientReportCsv.elementRef.nativeElement,
      'Client Report'
    );
  }
}
