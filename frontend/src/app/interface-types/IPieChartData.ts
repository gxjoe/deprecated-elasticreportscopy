export interface IPieChartData {
  labels: Array<string>;
  datasets: Array<{
    data: Array<number[]> | number[];
  }>;
}
