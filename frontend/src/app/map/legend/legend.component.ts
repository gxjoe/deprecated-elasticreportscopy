import {
    ChangeDetectionStrategy,
    Component,
    Input,
    ViewEncapsulation,
} from "@angular/core";
import { findLayerNameByLabel } from "src/app/geam-layers/constants";
import {
    CustomLayerColorScheme,
    LayerColorScheme,
} from "src/app/geam-layers/geam2-constants";

type LayerColorSchemeKey = keyof typeof LayerColorScheme;
type CustomLayerColorSchemeKey = keyof typeof CustomLayerColorScheme;

type Prop =
    | {
        schemeName: LayerColorSchemeKey;
    }
    | {
        customSchemeName: CustomLayerColorSchemeKey;
    }
    | {
        pngBlobSrc: string;
        label: LayerColorSchemeKey;
    };

function TerrorismScheme() {
    return `
        ${[5, 4, 3, 2, 0]
            .map((val) => {
                return `
            <div
                style="background: ${CustomLayerColorScheme["terrorism(AON)"][val]}"
            >
                <p>${val}</p>
            </div>
            `;
            })
            .join(" ")}  
        `;
}

function CustomSchemeComponent(keyword: keyof typeof CustomLayerColorScheme) {
    return `
        ${Object.entries(CustomLayerColorScheme[keyword])
            .map(([key, value]) => {
                return `
            <div style="background: ${value}">
                <p>${key.slice(0, 10)}</p>
            </div>
            `;
            })
            .join(" ")}`;
}

function ViolenceScheme() {
    return `

        <div style="background: ${CustomLayerColorScheme["politicalViolence(AON)"].Severe}">
            <p>Severe</p>
        </div>

        <div style="background: ${CustomLayerColorScheme["politicalViolence(AON)"].High}">
            <p>High</p>
        </div>

        <div style="background: ${CustomLayerColorScheme["politicalViolence(AON)"].Medium}">
            <p>Medium</p>
        </div>

        <div style="background: ${CustomLayerColorScheme["politicalViolence(AON)"].Low}">
          <p>Low</p>
        </div>

        <div style="background: ${CustomLayerColorScheme["politicalViolence(AON)"].Negligble}">
            <p>Negligible</p>
        </div>

        <div style="background: ${CustomLayerColorScheme["politicalViolence(AON)"]["No Risk"]}">
            <p>No Risk</p>
        </div>
        `;
}

function PopulationScheme() {
    return `

        <div style="background: ${CustomLayerColorScheme["populationDensity(NASA)"][6]}">
            <p><= 2.566.708</p>
        </div>

        <div style="background: ${CustomLayerColorScheme["populationDensity(NASA)"][5]}">
            <p><= 3.895,392</p>
        </div>

        <div style="background: ${CustomLayerColorScheme["populationDensity(NASA)"][4]}">
            <p><= 1.463,3706</p>
        </div>

        <div style="background: ${CustomLayerColorScheme["populationDensity(NASA)"][4]}">
            <p><= 1.463,3706</p>
        </div>

        <div style="background: ${CustomLayerColorScheme["populationDensity(NASA)"][3]}">
            <p><= 549.7402</p>
        </div>

        <div style="background: ${CustomLayerColorScheme["populationDensity(NASA)"][2]}">
            <p><= 199.16481</p>
        </div>

        <div style="background: ${CustomLayerColorScheme["populationDensity(NASA)"][1]}">
            <p><= 74.819664</p>
        </div>

        <div style="background: ${CustomLayerColorScheme["populationDensity(NASA)"][0]}">
            <p><= 28.107285</p>
        </div>
        `;
}

function CustomLayerScheme(customSchemeName: CustomLayerColorSchemeKey) {
    if (customSchemeName === "terrorism(AON)") {
        return TerrorismScheme();
    }
    if (customSchemeName === "politicalViolence(AON)") {
        return ViolenceScheme();
    }
    if (
        customSchemeName === "historicalCyclones" ||
        customSchemeName === "europeEarthquake(EFEHR)" ||
        customSchemeName === "CEELandslides(RELIA)" ||
        customSchemeName === "CEELandslides(SUS)" ||
        customSchemeName === "earthquakeEuropeMidEast" ||
        customSchemeName === "zeus2017(MedgustsAON)"
    ) {
        return CustomSchemeComponent(customSchemeName);
    }
    return PopulationScheme();
}

@Component({
    selector: "app-map-legend",
    templateUrl: "./legend.component.html",
    styleUrls: ["./legend.component.scss"],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LegendComponent {
    @Input() customSchemeName?: CustomLayerColorSchemeKey;
    @Input() schemeName?: LayerColorSchemeKey;
    @Input() pngBlobSrc?: string;
    @Input() label?: string;

    getTranslatedLayerName(label: string) {
        return findLayerNameByLabel(label);
    }

    getStyles(): Partial<CSSStyleDeclaration> {
        return {
            width:
                this.customSchemeName === "populationDensity(NASA)" ||
                    this.customSchemeName === "historicalCyclones" ||
                    this.customSchemeName === "europeEarthquake(EFEHR)" ||
                    this.customSchemeName === "CEELandslides(RELIA)" ||
                    this.customSchemeName === "CEELandslides(SUS)" ||
                    this.customSchemeName === "earthquakeEuropeMidEast" ||
                    this.customSchemeName === "zeus2017(MedgustsAON)" ||
                    this.pngBlobSrc
                    ? "120px"
                    : "96px",
        };
    }

    getLayerScheme() {
        return `
            ${LayerColorScheme[this.schemeName]
                .map((color, index) => {
                    return `
                    <div style="background: ${color}">
                        <p>${index + 1}</p>
                    </div>
                `;
                })
                .join("")}
        `;
    }

    getCustomLayerScheme() {
        if (!this.customSchemeName) {
            return;
        }
        return CustomLayerScheme(this.customSchemeName);
    }
}
