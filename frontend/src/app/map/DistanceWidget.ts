import { RadiusWidget } from "./RadiusWidget";

/**
 * A distance widget that will display a circle that can be resized and will
 * provide the radius in km.
 *
 * @param {google.maps.Map} map The map on which to attach the distance widget.
 *
 * @constructor
 */
export function DistanceWidget(map: google.maps.Map) {
  this.set("map", map);
  this.set("position", map.getCenter());

  var marker = new google.maps.Marker({
    draggable: true,
    icon: {
      url:
        "https://maps.gstatic.com/intl/en_us/mapfiles/markers2/measle_blue.png",
      size: new google.maps.Size(7, 7),
      anchor: new google.maps.Point(4, 4)
    },
    title: "Move me!"
  } as any);

  this.set("marker", marker);

  // Bind the marker map property to the DistanceWidget map property
  marker.bindTo("map", this);

  // Bind the marker position property to the DistanceWidget position
  // property
  marker.bindTo("position", this);

  // Create a new radius widget
  var radiusWidget = new RadiusWidget(map);

  this.set("radiusWidget", radiusWidget);

  // Bind the radiusWidget map to the DistanceWidget map
  radiusWidget.bindTo("map", this);

  // Bind the radiusWidget center to the DistanceWidget position
  radiusWidget.bindTo("center", this, "position");

  // Bind to the radiusWidgets' distance property
  this.bindTo("distance", radiusWidget);

  // Bind to the radiusWidgets' bounds property
  this.bindTo("bounds", radiusWidget);
}
DistanceWidget.prototype = new google.maps.MVCObject();

(DistanceWidget.prototype as any).unset = function() {
  this.get("marker").setMap(null);
  this.get("radiusWidget").unset();
};
