import {
  Component,
  ViewChild,
  ElementRef,
  Input,
  ViewEncapsulation,
  Output,
  EventEmitter,
  OnChanges,
} from '@angular/core';
import { GoogleMap } from '@angular/google-maps';
import { ElasticService } from 'src/app/services/elastic.service';
import { MAP_KLASSES } from 'src/app/elastic/queries';
import { SearchResponse } from 'elasticsearch';
import { AddressesDoc } from '../../interfaces/elastic';
import {
  clusterByDistance,
  IEnrichedPoint,
  geometryToLatLngBounds,
  CLUSTERING_ALGORITHMS,
  CLUSTERING_MODES,
} from './utils';

import { GeoJsonLayer, IconLayer } from '@deck.gl/layers';
import { GoogleMapsOverlay as DeckOverlay } from '@deck.gl/google-maps';


import {
  CURRENCY_PREFIX_UTF,
  nFormatter,
  uniq,
  validLocationPeril,
} from 'src/app/utils/helpers/helpers';
import { ValueOfSlider } from 'src/app/slider/slider.component';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { roundUp } from 'src/app/utils/helpers/numerical';
import { encodeSVG } from 'src/app/utils/helpers/gmaps';
import { prettyPercent } from 'src/app/charts/utils';
import { GoogleMapsService } from 'src/app/services/gmaps-service.service';
import { TypedSimpleChanges } from 'src/app/utils/typescript';
import { Feature, FeatureCollection, MultiPolygon, Polygon } from '@turf/turf';
import { centerOfMass, area, destination } from '@turf/turf';

interface HotspotDiv {
  index: number;
  tsi_short: string;
  countries: string;
  bounds: google.maps.LatLngBounds;
  count: number;
  area_covered?: string;
  selected?: boolean;
  percentage_of_those_with_peril_limit?: string;
}

export type Hotspots = Array<HotspotDiv>;

@Component({
  selector: 'app-hotspot',
  templateUrl: './hotspot.component.html',
  styleUrls: ['./hotspot.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HotspotComponent implements OnChanges {
  clustering_algorithms = Object.keys(CLUSTERING_ALGORITHMS).map((algo) => {
    return {
      name: algo,
      hint: ((_algo) => {
        switch (_algo) {
          case CLUSTERING_ALGORITHMS.GEAM:
            return 'Identical to the Hotspot Function you may know from GEAM — https://prnt.sc/10sna9d';
          case CLUSTERING_ALGORITHMS.RADIAL:
            return 'Using k-dimensional trees to represent the points and then applying a nearest-neighbor search — https://en.wikipedia.org/wiki/K-d_tree';
          case CLUSTERING_ALGORITHMS.DBSCAN:
            return 'Using density-based spatial clustering — https://en.wikipedia.org/wiki/DBSCAN';
          case CLUSTERING_ALGORITHMS.K_MEANS:
            return 'Using k-means clustering; leveraging the least squared Euclidian distance — https://en.wikipedia.org/wiki/K-means_clustering';
        }
      })(algo),
    };
  });

  _radius = '1000';
  _algorithm: CLUSTERING_ALGORITHMS = CLUSTERING_ALGORITHMS.GEAM;
  _tsiRange: ValueOfSlider;
  _perilLimitRange: ValueOfSlider;

  _mode: CLUSTERING_MODES = CLUSTERING_MODES.TSI;
  _CLUSTERING_MODES = CLUSTERING_MODES;

  calculating = false;

  @ViewChild('radiusInput', { static: false }) radiusInput: ElementRef;
  @Input() agmMap: GoogleMap;
  @Input() refreshMapBtn: HTMLButtonElement;

  @Input() onlyShowFocusedHotspot: boolean;

  @Output() hotspotDivsChanged: EventEmitter<Hotspots> =
    new EventEmitter<Hotspots>();

  deck: DeckOverlay;
  epigraph_polygons: FeatureCollection<
    Polygon | MultiPolygon
  >;

  hotspotDivs: Hotspots = [];

  negativeFeedback = 'No hotspots were identified.';
  showNegativeFeedback = false;

  constructor(
    private es: ElasticService,
    private notifService: NzNotificationService,
    private gmapsService: GoogleMapsService
  ) { }

  get tabsetSelectedIndex() {
    return this._mode === CLUSTERING_MODES.TSI
      ? 0
      : CLUSTERING_MODES.PERIL_LIMIT
        ? 1
        : 0;
  }

  onHotspotPopoverVisibleChange($event: boolean) {
    if ($event === true) {
      const algoFromLS = localStorage.getItem('_hotspotAlgorithm');
      if (algoFromLS) {
        this._algorithm = CLUSTERING_ALGORITHMS[algoFromLS];
      }

      const modeFromLS = localStorage.getItem('_hotspotMode');
      if (modeFromLS) {
        this._mode = CLUSTERING_MODES[modeFromLS];
      }
    }
  }

  defaults(mode: CLUSTERING_MODES) {
    switch (mode) {
      case CLUSTERING_MODES.TSI:
        return typeof this._tsiRange !== 'undefined'
          ? (this._tsiRange as number[]).join(',')
          : '10000000,500000000';
      case CLUSTERING_MODES.PERIL_LIMIT:
        return typeof this._perilLimitRange !== 'undefined'
          ? (this._perilLimitRange as number[]).join(',')
          : '10000000,500000000';
    }
  }

  onChange(value: string): void {
    this._radius = value.replace(/[^\d+]/g, '');
    if (this._radius.startsWith('0')) {
      this._radius = this._radius.substr(1);
    }
    this.radiusInput.nativeElement.value = this._radius;
  }

  ngOnChanges(changes: TypedSimpleChanges<HotspotComponent>): void {
    if (!changes.onlyShowFocusedHotspot?.firstChange) {
      this.renderDeck(this.epigraph_polygons);
    }
  }

  /**
   * Whether to disable inputs & the submit btn or not
   */
  get isDisabled() {
    switch (this._mode) {
      case CLUSTERING_MODES.TSI:
        // always let thru
        return false;
      case CLUSTERING_MODES.PERIL_LIMIT:
        return !this.validLocationPeril;
    }
  }

  get validLocationPeril() {
    return validLocationPeril();
  }

  tabChanged(mode: CLUSTERING_MODES) {
    this._mode = mode;

    localStorage.setItem('_hotspotMode', this._mode);

    // fresh start
    this.unsetDeck();
    this.unsetHotspotDivs();
  }

  updateTSIrange(minMax: ValueOfSlider) {
    this._tsiRange = minMax;
  }

  updatePerilLimitRange(minMax: ValueOfSlider) {
    this._perilLimitRange = minMax;
  }

  updateSelectedAlgorithm(newOne: CLUSTERING_ALGORITHMS) {
    this._algorithm = newOne;
    localStorage.setItem('_hotspotAlgorithm', this._algorithm);
  }

  get radius(): number {
    const parsed = parseInt(this._radius, 10);
    if (isNaN(parsed)) {
      return 0;
    }

    return parsed;
  }

  get stats() {
    return this.hotspotDivs && this.hotspotDivs.length
      ? ` (${this.hotspotDivs.length})`
      : '';
  }

  isHotspotSelectedByIndex(index: number) {
    const by_index = this.hotspotDivs[index];
    return by_index.selected === true;
  }

  unsetDeck() {
    try {
      this.deck.setMap(null);
      this.deck = null;

      // prevent multiple deckgl-overlay canvases from existing in the dom
      // which *might* result in "Context lost - multiple Deckgl instances running"
      Array.from(document.querySelectorAll("[id^=deckgl-overlay]")).map((cvs) =>
          cvs.parentNode.removeChild(cvs)
      );
    } catch (err) { }
  }

  unsetHotspotDivs() {
    this.hotspotDivs = [];

    this.gmapsService.hotspotModeActive$.next(false);
  }

  renderHotspotDivs(
    hotspots: FeatureCollection<
      Polygon | MultiPolygon,
      {
        [name: string]: any;
      }
    >
  ) {
    this.unsetHotspotDivs();

    hotspots.features.map((hotspot, index) => {
      const count = (hotspot.properties.original_points as IEnrichedPoint[])
        .length;
      const sum = hotspot.properties.tsi_sum;
      const tsi_short = `${CURRENCY_PREFIX_UTF()}${nFormatter(sum, 1, true)}`;
      const countries = uniq(
        (hotspot.properties.original_points as IEnrichedPoint[]).map(
          (point) => point.properties.props.address.country
        )
      )
        .sort()
        .join(', ');

      const count_with_peril_limit_value_applied = (
        hotspot.properties.original_points as IEnrichedPoint[]
      ).filter((point) => point.properties.peril_limit_value_applied).length;
      const percentage_of_those_with_peril_limit = prettyPercent(
        count_with_peril_limit_value_applied,
        count,
        true
      );

      const area_ = area(hotspot);

      this.hotspotDivs.push({
        index,
        countries,
        tsi_short,
        bounds: geometryToLatLngBounds(
          hotspot as Feature<Polygon>,
          this.radius
        ),
        count: count,
        area_covered:
          area_ >= 1e6
            ? `${roundUp(area_ / 1e6, 4)} km²`
            : `${roundUp(area_, 4)} m²`,

        percentage_of_those_with_peril_limit:
          this._mode === CLUSTERING_MODES.PERIL_LIMIT
            ? percentage_of_those_with_peril_limit
            : null,
      });
    });

    this.gmapsService.hotspotModeActive$.next(Boolean(this.hotspotDivs.length));
    this.hotspotDivsChanged.emit(this.hotspotDivs);
  }

  goToHotspot(index: number) {
    this.hotspotDivs[index].selected = true;
    const target = this.hotspotDivs[index];

    if (target) {
      this.renderDeck(this.epigraph_polygons);

      google.maps.event.addListenerOnce(
        this.es.activeMapComponent.map,
        'idle',
        () => {
          document
            .querySelector('#refreshMapButton')
            .dispatchEvent(new Event('click'));
        }
      );

      this.es.activeMapComponent.map.fitBounds(target.bounds);
    }
  }

  go() {
    this.calculating = true;
    this.showNegativeFeedback = false;

    this.es
      .search(MAP_KLASSES.OfferHotspotMap, true)
      .then((resp: SearchResponse<AddressesDoc>) => {
        clusterByDistance(
          resp,
          this.radius,
          this._mode === CLUSTERING_MODES.TSI
            ? this._tsiRange
            : this._perilLimitRange,
          this._algorithm,
          this.validLocationPeril
        )
          .then((epigraph_polygons) => {
            this.epigraph_polygons = epigraph_polygons;

            this.unsetDeck();
            this.renderHotspotDivs(epigraph_polygons);

            if (!epigraph_polygons.features.length) {
              this.unsetAll();
              return;
            }

            this.renderDeck(epigraph_polygons);
          })
          .catch((err) => {
            console.error(err);
            this.unsetAll();
          });
      })
      .catch((err) => {
        console.error(err);
        this.unsetAll();
      });
  }

  renderDeck(
    epigraph_polygons: FeatureCollection<
      Polygon | MultiPolygon
    >
  ) {
    const self = this;

    const vertices = [];
    const centers_of_mass = [];
    const clustering_origins = [];

    let minTSI = +Infinity;
    let maxTSI = 0;

    epigraph_polygons.features.map((polygon, index) => {
      const sum = polygon.properties.tsi_sum;

      if (sum < minTSI) {
        minTSI = sum;
      } else if (sum > maxTSI) {
        maxTSI = sum;
      }

      (polygon.properties.original_points as IEnrichedPoint[]).map((point) => {
        vertices.push({
          coordinates: point.geometry.coordinates,
          hotspot_div: this.hotspotDivs[index],
          point: point,
        });
      });

      centers_of_mass.push({
        coordinates: centerOfMass(polygon).geometry.coordinates,
        hotspot_div: this.hotspotDivs[index],
      });

      if (this._algorithm === CLUSTERING_ALGORITHMS.RADIAL) {
        (polygon.properties.original_points as IEnrichedPoint[])
          .slice(0, 1)
          .map((point) => {
            clustering_origins.push({
              coordinates: point.geometry.coordinates,
            });
          });
      }
    });

    // make address ids available so that filtering of addresses can happen
    this.es.filterByAddressIDs.next(
      vertices.map((vertex) => {
        return (
          vertex.point as IEnrichedPoint
        ).properties.props.address.id.toString();
      })
    );

    const circleIconRadius = 31;

    const onHotspotClick = ({
      object,
    }:
      | {
        object: {
          hotspot_div: HotspotDiv;
          coordinates: IEnrichedPoint['geometry']['coordinates'];
        };
      }
      | any) => {
      const indexOf = epigraph_polygons.features.indexOf(object);

      self.hotspotDivs.forEach((div) => (div.selected = false));
      self.hotspotDivs[indexOf].selected = true;

      const poly_div = self.hotspotDivs[indexOf];

      self.notifService.remove();
      self.notifService
        .warning(
          `Hotspot #${epigraph_polygons.features.indexOf(object)} • ${poly_div.tsi_short
          } • ${poly_div.count} addresses`,
          `Area covered: ${poly_div.area_covered}`,
          {
            nzPauseOnHover: true,
            nzDuration: 10e3,
          }
        )
        .onClick.subscribe(() => { });

      self.renderDeck(epigraph_polygons);
    };

    const layers = [
      // https://deck.gl/docs/api-reference/layers/geojson-layer
      new GeoJsonLayer({
        id: 'geojson',
        data: epigraph_polygons.features,
        opacity: 0.8,
        stroked: true,
        filled: true,
        extruded: false,
        wireframe: true,
        getElevation: (f: any) => Math.sqrt(f.properties.tsi_sum) * 10,
        getFillColor: (_: any, payload: any) => {
          const isSelected = this.isHotspotSelectedByIndex(payload.index);
          const isHidden = this.onlyShowFocusedHotspot && !isSelected;

          if (isHidden) {
            return [0, 0, 0, 0];
          }

          return isSelected ? [251, 51, 51, 120] : [40, 40, 40, 40];
        },
        highlightColor: () => [251, 51, 51, 40],

        getLineColor: (_) => [251, 51, 51, 180],
        getLineWidth: (_: any, payload: any) => {
          const isSelected = this.isHotspotSelectedByIndex(payload.index);
          const isHidden = this.onlyShowFocusedHotspot && !isSelected;

          if (isHidden) {
            return 0;
          }

          return 5;
        },

        pickable: true,
        autoHighlight: true,
        onClick: onHotspotClick,
      }),
      // no need to display the cluster members when in geam
      ![CLUSTERING_ALGORITHMS.GEAM, CLUSTERING_ALGORITHMS.RADIAL].includes(
        this._algorithm
      )
        ? new IconLayer({
          id: 'hotspot-members-icons',
          data: vertices,
          pickable: true,
          wrapLongitude: true,
          getPosition: (d: any) => d.coordinates,
          // TODO onClick?
          getIcon: (_) => {
            return {
              url: 'http://maps.google.com/mapfiles/ms/micons/blue.png',

              width: 32,
              height: 32,
              anchorY: 32,
            };
          },
          sizeUnits: 'meters',
          sizeMinPixels: 32,
        })
        : null,
      // https://deck.gl/docs/api-reference/layers/icon-layer
      new IconLayer({
        id: 'cluster-identifiers',
        data: epigraph_polygons.features,
        pickable: true,
        wrapLongitude: true,
        // place the cirlce icon top center by using bearing 0 from the cluster center
        getPosition: (feature: any) => {
          return destination(
            // center was already calculated when preparing `epigraph_polygons`
            feature.properties.center,
            this.radius,
            0,
            { units: 'meters' }
          ).geometry.coordinates;
        },
        onClick: onHotspotClick,
        getIcon: (_: any, payload: { data: any[]; index: number }) => {
          const isSelected = this.isHotspotSelectedByIndex(payload.index);
          const isHidden = this.onlyShowFocusedHotspot && !isSelected;

          const svg = `
          <svg width="${circleIconRadius * 2}px"
                height="${circleIconRadius * 2}px"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                xml:space="preserve"
                viewBox="0 0 ${circleIconRadius * 2} ${circleIconRadius * 2}">
              <rect width="${circleIconRadius * 2}px"
                      height="${circleIconRadius * 2}px"
                      stroke-width="3"
                      stroke="#FB3333"
                      fill="${isSelected ? '#FB3333' : '#FFFFFF'}"
                      rx="12"
                      r="30px"></rect>
              <text font-size="22px"
                    text-anchor="middle"
                    font-weight="bold"
                    x="50%"
                    y="53%"
                    fill="${isSelected ? '#FFFFFF' : '#FB3333'}"
                    dominant-baseline="middle"
                    font-family="sans-serif">&#35;${payload.index}</text>
            </svg>
          `;
          const encodedSvg = encodeSVG(`data:image/svg+xml;utf-8,${svg}`);

          return {
            url: isHidden
              ? // empty strings are falsey -- that's why we're using whitespace here
              // to hide individual icons
              ' '
              : encodedSvg,
            width: circleIconRadius * 2,
            height: circleIconRadius * 2,
            // center the icon *just* at the polygon rim
            anchorY: circleIconRadius,
          };
        },
        sizeUnits: 'meters',
        sizeMinPixels: circleIconRadius * 2,
      }),
    ];

    this.unsetDeck();
    this.deck = new DeckOverlay({
      // semi-random ID of the actual canvas
      // copy-pasted from gmaps-service
      id: `deckgl-overlay-${(Math.random() * 10e5).toFixed(0)}`,
      layers: layers,
      onAfterRender: () => { },
    });

    this.deck.setMap(this.es.activeMapComponent.map);

    this.calculating = false;
    this.showNegativeFeedback = false;
  }

  unsetAll() {
    this.unsetDeck();
    this.unsetHotspotDivs();

    this.calculating = false;
    this.showNegativeFeedback = true;
  }
}
