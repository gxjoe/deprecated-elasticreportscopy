import { SearchResponse } from "elasticsearch";
import { AddressesDoc } from "../../interfaces/elastic";
import { getPerilSpecificLocationTSI } from "src/app/utils/helpers/helpers";
import { Dbscan } from "@turf/clusters-dbscan";
import { transformJSON2html } from "src/app/utils/helpers/json2html";
import { ValueOfSlider } from "src/app/slider/slider.component";
import KDBush from "kdbush";
import { getRandomInt } from "src/app/charts/common/constants";
import {
  Feature,
  FeatureCollection,
  Geometry,
  MultiPolygon,
  Point,
  Polygon,
  buffer,
  center,
  circle,
  clone,
  clustersKmeans,
  clustersDbscan,
  concave,
  convex,
  degrees2radians,
  distance,
  featureCollection,
  point,
} from "@turf/turf";

export interface IEnrichedPoint extends Point {
  geometry: Geometry;
  properties?: {
    offer_number: string;
    tsi: number;
    // for peril limit hotspots
    peril_limit_value_applied?: boolean;

    props: AddressesDoc;

    // shared clustering
    cluster?: number;

    // RADIAL only
    radial_id?: number;

    // DBSCAN only
    dbscan?: Dbscan;

    // kmeans only
    centroid?: [number, number];
  };
}

export enum CLUSTERING_ALGORITHMS {
  GEAM = "GEAM",
  RADIAL = "RADIAL",
  DBSCAN = "DBSCAN",
  K_MEANS = "K_MEANS",
}

export enum CLUSTERING_MODES {
  TSI = "TSI",
  PERIL_LIMIT = "PERIL_LIMIT",
}

interface ResultOfClustering {
  type: "FeatureCollection";
  features: IEnrichedPoint[];
}

interface ReducedClusters {
  [key: string]: IEnrichedPoint[];
}

const locationHitsToFeatureCollection = (
  search_response: SearchResponse<AddressesDoc>,
  selected_peril: string | undefined
): FeatureCollection<IEnrichedPoint> => {
  return featureCollection(
    search_response.hits.hits
      .filter(
        (hit) =>
          hit._source.address.coordinates &&
          hit._source.address.coordinates.length
      )
      .map((hit) => {
        const source = hit._source;
        const coords = source.address.coordinates as string;

        const geojson_coords = coords.split(",").map(parseFloat).reverse();

        const { perilLimitValue, tsi } = getPerilSpecificLocationTSI(
          source,
          selected_peril
        );

        return point(geojson_coords, {
          offer_number: source.offer.number,
          tsi: tsi,
          peril_limit_value_applied: selected_peril && perilLimitValue !== null,
          props: {
            ...source,
            // the clusters need an ID in order to be correctly identified & deduplicated
            // fixes https://gxperts.atlassian.net/browse/UMGB-17
            id: source.address.id,
          },
        }) as any;
      })
  );
};

const pointsToBuffers = (collection: FeatureCollection, radius: number) => {
  const cloned = clone(collection);
  cloned.features = cloned.features.map((pointFeature) =>
    buffer(pointFeature, radius, { units: "meters" })
  );
  return cloned;
};

const latLongToXY = (latLong: number[]): number[] => {
  const r = 6378000; // meters
  const lambda = latLong[0];
  const phi = latLong[1];

  const cos_phi = Math.cos(degrees2radians(phi));

  return [r * degrees2radians(lambda) * cos_phi, r * degrees2radians(phi)];
};

const clusterByTrivialRadialIntersection = (
  point_collection: FeatureCollection<IEnrichedPoint>,
  radius: number
): ReducedClusters => {
  const clusters: ReducedClusters = {};
  const length = point_collection.features.length;

  if (!length) {
    return clusters;
  }

  type _point = (typeof point_collection.features)[number];
  type _foundFeature = _point & { connections?: _point[] };
  let foundFeatures: Array<_foundFeature> = [];

  for (let outerIndex = 0; outerIndex < length; outerIndex++) {
    for (let innerIndex = 0; innerIndex < length; innerIndex++) {
      // only calculate upper right half of the distance matrix, excluding the diagonal
      if (!(outerIndex < innerIndex)) {
        continue;
      }

      const outerFeature: _foundFeature = point_collection.features[outerIndex];
      const innerFeature: _foundFeature = point_collection.features[innerIndex];
      const geo_distance = distance(
        outerFeature.geometry,
        innerFeature.geometry,
        { units: "meters" }
      );

      if (
        // at least 1m from one another --> no same-point locations
        geo_distance < 1 ||
        // stay within the radius
        geo_distance > radius
      ) {
        continue;
      }

      if (!outerFeature.connections) {
        outerFeature.connections = [];
      }
      outerFeature.connections.push(innerFeature);

      if (!innerFeature.connections) {
        innerFeature.connections = [];
      }
      innerFeature.connections.push(outerFeature);

      foundFeatures.push(outerFeature);
      foundFeatures.push(innerFeature);
    }
  }

  // remove duplicates
  foundFeatures = foundFeatures.filter((value, index, self) => {
    return self.indexOf(value) === index;
  });

  // sort features by the # of their cluster neighbors
  foundFeatures.sort(function (feature1, feature2) {
    return feature2.connections.length - feature1.connections.length;
  });

  foundFeatures.map((cluster) => {
    // id is the address id
    clusters[cluster.properties.props.id] = [
      // include self
      cluster as any,
      // plus all the connections (cluster members)
      ...cluster.connections,
    ];
  });

  const standardizeIds = (_cluster: (typeof clusters)[number]) => {
    return _cluster
      .map((connection) => connection.properties.props.id)
      .sort()
      .join("|");
  };

  const clustersToIds = Object.keys(clusters).map((address_id) => {
    return {
      address_id,
      connection_ids: standardizeIds(clusters[address_id]),
    };
  });

  // #1=[addr1, addr2] vs #2=[addr2, addr1] are essentially the same cluster
  // so make sure only one of them is retained
  Object.keys(clusters).filter((address_id) => {
    const current_ids = standardizeIds(clusters[address_id]);

    // i.e. not a single cluster containing the same location IDs could be found
    const is_mirrored_index = clustersToIds
      // exclude self
      .filter(({ address_id: _addr_id }) => address_id !== _addr_id)
      .findIndex(({ connection_ids }) => connection_ids === current_ids);

    if (is_mirrored_index !== -1) {
      // ditch if mirrored
      delete clusters[address_id];

      // remove from clustersToIds b/c otherwise both mirrored
      // occurrences would've flushed each other
      clustersToIds.splice(is_mirrored_index, 1);
    }
  });

  return clusters;
};

const clusterByRadialComparison = (
  point_collection: FeatureCollection<IEnrichedPoint>,
  radius: number
): ReducedClusters => {
  const tree = new KDBush(
    point_collection.features
      .slice()
      .map((point) => point.geometry.coordinates),
    (p) => latLongToXY(p)[0],
    (p) => latLongToXY(p)[1],
    64,
    Float32Array
  );

  const non_singular_clusters_map: ReducedClusters = {};
  const uniqueness_test = {};

  point_collection.features.map((feature, index) => {
    const [x, y] = latLongToXY(feature.geometry.coordinates);

    uniqueness_test[index] = [];

    const items_within = tree.within(x, y, radius);

    if (items_within.length > 3) {
      non_singular_clusters_map[
        getRandomInt(0, point_collection.features.length)
      ] = []
        .concat(feature) // append self
        .concat(
          // plus all other features that the KDbush tree identified as nearby
          point_collection.features.filter((_, inner_index) =>
            items_within.includes(inner_index)
          )
        );
      uniqueness_test[index] += items_within;
    }
  });

  return non_singular_clusters_map;
};

export const clusterByDistance = (
  search_response: SearchResponse<AddressesDoc> | any,
  radius: number,
  tsi_range: ValueOfSlider,
  algorithm: CLUSTERING_ALGORITHMS,
  selected_peril: string | undefined
): Promise<FeatureCollection<Polygon | MultiPolygon>> => {
  return new Promise((resolve, reject) => {
    let point_collection: FeatureCollection<IEnrichedPoint>;
    point_collection = locationHitsToFeatureCollection(
      search_response,
      selected_peril
    );

    if (!point_collection.features.length) {
      reject([]);
      return;
    }

    // const buffer_collection = pointsToBuffers(point_collection, radius);
    let resultOfClustering: ResultOfClustering;
    let reducedClusters: ReducedClusters;
    let _reducedClusters: ReducedClusters;

    switch (algorithm) {
      case CLUSTERING_ALGORITHMS.GEAM:
        _reducedClusters = clusterByTrivialRadialIntersection(
          point_collection,
          radius
        );
        break;
      case CLUSTERING_ALGORITHMS.RADIAL:
        _reducedClusters = clusterByRadialComparison(point_collection, radius);
        break;
      case CLUSTERING_ALGORITHMS.DBSCAN:
        resultOfClustering = clustersDbscan(point_collection, radius, {
          units: "meters",
          minPoints: 2,
          mutate: true,
        }) as any;
        break;
      case CLUSTERING_ALGORITHMS.K_MEANS:
        resultOfClustering = clustersKmeans(point_collection, {
          mutate: true,
        }) as any;
        break;
    }

    if (!_reducedClusters) {
      resultOfClustering.features = resultOfClustering.features.filter(
        (feature) =>
          feature.properties.dbscan === "core" ||
          Boolean(feature.properties.centroid)
      );

      reducedClusters = resultOfClustering.features.reduce(
        (product, feature_of_cluster) => {
          const clusterIndex = feature_of_cluster.properties.cluster;
          if (!product[clusterIndex]) {
            product[clusterIndex] = [feature_of_cluster];
          } else {
            product[clusterIndex].push(feature_of_cluster);
          }
          return product;
        },
        {}
      );
    } else {
      reducedClusters = _reducedClusters;
    }

    type Epigraph = Feature<Polygon | MultiPolygon, any>;

    const epigraph_polygons = Object.values(reducedClusters)
      .map((cluster_points) => {
        let epigraph: Epigraph = null;

        if (algorithm === CLUSTERING_ALGORITHMS.GEAM) {
          // create a buffered circle around the first location
          // -- i.e. the `outerFeature` this cluster was based on
          epigraph = circle(cluster_points[0], radius, {
            units: "meters",
            steps: 64,
          });
        } else {
          // try with a concave tranformation
          epigraph = concave(featureCollection(cluster_points as any));

          // if no concave tranformation could be performed, default to convex
          if (!epigraph) {
            epigraph = convex(featureCollection(cluster_points as any));
          }
        }

        if (epigraph) {
          epigraph.properties = {
            original_points: cluster_points,
            tsi_sum: cluster_points
              .map((inner) => inner.properties.tsi)
              .sumByReduce(),
            center: center(epigraph),
          };
        }

        return epigraph;
      })
      .filter((epigraph) => Boolean(epigraph)) // filter out non-convex & non-concave sets
      .filter((epigraph) => {
        const tsi_sum = epigraph.properties.tsi_sum;
        return tsi_sum >= tsi_range[0] && tsi_sum <= tsi_range[1];
      })
      .sort(
        (a, b) =>
          -1 *
          (a.properties.tsi_sum < b.properties.tsi_sum
            ? -1
            : a.properties.tsi_sum > b.properties.tsi_sum
              ? 1
              : 0)
      );

    resolve(featureCollection(epigraph_polygons));
  });
};

export function setTooltip(object, x: number, y: number) {
  const el = document.getElementById("tooltip");
  if (object) {
    const innerHtml = transformJSON2html(object, (obj) => obj.properties);
    const current = JSON.stringify(el.innerHTML);
    const neww = JSON.stringify(innerHtml);
    console.warn({ current, neww });

    if (JSON.stringify(el.innerHTML) === JSON.stringify(innerHtml)) {
      return;
    }

    el.innerHTML = innerHtml;
    el.style.position = "absolute";
    el.style.display = "block";
    el.style.left = x + "px";
    el.style.top = y + "px";
  } else {
    el.style.display = "none";
  }
}

export const geometryToLatLngBounds = (
  feature: Feature<Polygon>,
  buffer_?: number
): google.maps.LatLngBounds => {
  const bounds = new google.maps.LatLngBounds();

  if (buffer_) {
    feature = buffer(clone(feature), 500, { units: "meters" });
  }

  feature.geometry.coordinates[0].map((coord) => {
    const a = coord[1];
    const b = coord[0];
    bounds.extend(new google.maps.LatLng(a, b));
  });

  return bounds;
};
