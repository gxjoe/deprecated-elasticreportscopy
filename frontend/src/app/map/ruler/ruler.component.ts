import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { GoogleMap } from '@angular/google-maps';
import { ElasticService } from 'src/app/services/elastic.service';
import { NzPopoverComponent } from 'ng-zorro-antd/popover';
import { DistanceWidget } from '../DistanceWidget';

@Component({
  selector: 'app-ruler',
  templateUrl: './ruler.component.html',
  styleUrls: ['./ruler.component.scss']
})
export class RulerComponent implements OnInit {
  @Input() agmMap: GoogleMap;
  @ViewChild('rulerTrigger', { static: false }) rulerTrigger: NzPopoverComponent;

  visible_popover = false;

  ruler_name = 'ONE_RULE';
  GMRuler: any;

  distanceWidget: google.maps.MVCObject;

  constructor(private es: ElasticService) { }

  ngOnInit() { }

  startCircularInspection() {
    this.distanceWidget = new DistanceWidget(this.es.activeMapComponent.map);
  }

  stopCircularInspection() {
    (this.distanceWidget as any).unset();
    this.distanceWidget = null;
  }

  start() {
    if (!this.GMRuler) {
      this.GMRuler = (window as any).gmruler;
    }

    this.GMRuler.init(this.es.activeMapComponent.map);

    this.GMRuler.add(this.ruler_name, {
      distanceUnit: 'km',
      strokeColor: '#40a9ff',
      strokeWidth: 3
    });

    this.visible_popover = false;
  }

  stop() {
    this.GMRuler.remove(this.ruler_name);
    this.GMRuler = null;
  }
}
