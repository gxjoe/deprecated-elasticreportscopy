import { roundUp } from '../utils/helpers/numerical';
import { encodeSVG } from '../utils/helpers/gmaps';

/**
 * A radius widget that add a circle to a map and centers on a marker.
 *
 * @constructor
 */
export function RadiusWidget(map: google.maps.Map) {
  const circle = new google.maps.Circle({
    strokeWeight: 2,
    draggable: true
  });

  const ne = map.getBounds().getNorthEast();
  const sw = map.getBounds().getSouthWest();

  const diagonal_distance =
    google.maps.geometry.spherical.computeDistanceBetween(ne, sw) / 8;

  // Set the distance property value
  this.set('distance', diagonal_distance);

  // Bind the RadiusWidget bounds property to the circle bounds property.
  this.bindTo('bounds', circle);

  // Bind the circle center to the RadiusWidget center property
  circle.bindTo('center', this);

  // Bind the circle map to the RadiusWidget map
  circle.bindTo('map', this);

  // Bind the circle radius property to the RadiusWidget radius property
  circle.bindTo('radius', this);

  this.set('circle', circle);

  this.addSizer_(this.get('distance'));
}
RadiusWidget.prototype = new google.maps.MVCObject();

/**
 * Update the radius when the distance has changed.
 */
(RadiusWidget.prototype as any).distance_changed = function() {
  this.set('radius', this.get('distance'));
};
/**
 * Add the sizer marker to the map.
 *
 * @private
 */

const generateMarkerSpecs = (radius: number) => {
  let prettified_radius;
  if (radius < 1000) {
    prettified_radius = `${roundUp(radius as number, 4)} m`;
  } else {
    prettified_radius = `${roundUp((radius as number) / 1e3, 4)} km`;
  }

  return {
    anchor: new google.maps.Point(50, 10),
    url: encodeSVG(
      `data:image/svg+xml;utf-8,
    <svg width="100"
        height="30"
        xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        xml:space="preserve"
        style="image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd">
      <rect rx="5px" x="0"
            y="0"
            width="100"
            height="20"
            fill="#666666" />
      <text font-size="14px"
            text-anchor="middle"
            x="50%"
            y="35%"
            fill="#FFFFFF"
            dominant-baseline="middle"
            font-family="sans-serif">${prettified_radius}</text>
    </svg>
  `
    )
  };
};

(RadiusWidget.prototype as any).addSizer_ = function(radius) {
  const sizer = new google.maps.Marker({
    draggable: true,
    icon: generateMarkerSpecs(radius),
    title: 'Drag me!'
  } as any);

  this.set('sizer', sizer);

  sizer.bindTo('map', this);
  sizer.bindTo('position', this, 'sizer_position');

  const me = this;
  google.maps.event.addListener(sizer, 'drag', function() {
    // Set the circle distance (radius)
    me.setDistance();
  });
};

(RadiusWidget.prototype as any).updateSizerWithPrettyRadius = function(
  new_radius_in_m: number
) {
  (this.get('sizer') as google.maps.Marker).setIcon(
    generateMarkerSpecs(new_radius_in_m)
  );
};

/**
 * Update the center of the circle and position the sizer back on the line.
 *
 * Position is bound to the DistanceWidget so this is expected to change when
 * the position of the distance widget is changed.
 */
(RadiusWidget.prototype as any).center_changed = function() {
  const bounds = this.get('bounds');

  // Bounds might not always be set so check that it exists first.
  if (bounds) {
    const lng = bounds.getNorthEast().lng();

    // Put the sizer at center, right on the circle.
    const position = new google.maps.LatLng(this.get('center').lat(), lng);
    this.set('sizer_position', position);
  }
};

/**
 * Calculates the distance between two latlng locations in km.
 * @see http://www.movable-type.co.uk/scripts/latlong.html
 *
 * @param {google.maps.LatLng} p1 The first lat lng point.
 * @param {google.maps.LatLng} p2 The second lat lng point.
 * @return {number} The distance between the two points in km.
 * @private
 */
(RadiusWidget.prototype as any).distanceBetweenPoints_ = function(p1, p2) {
  if (!p1 || !p2) {
    return 0;
  }

  const R = 6371000; // Radius of the Earth in km
  const dLat = ((p2.lat() - p1.lat()) * Math.PI) / 180;
  const dLon = ((p2.lng() - p1.lng()) * Math.PI) / 180;
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos((p1.lat() * Math.PI) / 180) *
      Math.cos((p2.lat() * Math.PI) / 180) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c;
  return d;
};

/**
 * Set the distance of the circle based on the position of the sizer.
 */
(RadiusWidget.prototype as any).setDistance = function() {
  // As the sizer is being dragged, its position changes.  Because the
  // RadiusWidget's sizer_position is bound to the sizer's position, it will
  // change as well.
  const pos = this.get('sizer_position');
  const center = this.get('center');
  const distance = this.distanceBetweenPoints_(center, pos);

  // Set the distance property for any objects that are bound to it
  this.set('distance', distance);
  this.updateSizerWithPrettyRadius(distance);
};

(RadiusWidget.prototype as any).unset = function() {
  this.get('sizer').setMap(null);
  this.get('circle').setMap(null);
};
