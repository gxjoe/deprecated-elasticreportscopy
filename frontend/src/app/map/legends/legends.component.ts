import * as R from "ramda";

import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ViewEncapsulation,
} from "@angular/core";
import { GoogleMapsService } from "src/app/services/gmaps-service.service";
import { distinctUntilChanged } from "rxjs";

@Component({
    selector: "app-map-legends",
    templateUrl: "./legends.component.html",
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LegendsComponent {
    constructor(
        public mapService: GoogleMapsService,
        private cd: ChangeDetectorRef
    ) {
        this.mapService.selectedLayersLabels$
            .pipe(distinctUntilChanged())
            .subscribe(async () => {
                // this is needed to async fetch CatNet layers
                // and push it to a BehSubject which is then subscribed to in the component.
                // If this were copy-pasted from GEAM, it would cause infinite loops
                // due to the template checking async pipes all the time.
                // Suppose this is more angular-like and cleaner.
                // Note to self: this took 4 days to figure out. Go figure (:
                await this.mapService.getAllCatnetPngs()
            });
    }
}
