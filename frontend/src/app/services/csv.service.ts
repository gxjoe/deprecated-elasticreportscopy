import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import {
  CSV_SEPARATOR,
  ITableReadyContentRow,
} from '../charts/histogram-table-chart/common';
import {
  CURRENCY_PREFIX_ISO,
  CURRENCY_PREFIX_UTF,
  extractNumberFromString,
  isNumeric,
  isString,
  mapOfUTFcurrenciesToISO,
} from '../utils/helpers/helpers';
import { roundUp } from '../utils/helpers/numerical';

/**
 * Ensure proper CSV values based on column names (keys)
 * @param key
 * @param value
 * @param rawCellValues
 * @returns
 */
const csvReplacer = (key: string, value: any, rawCellValues = false) => {
  if (
    value === null ||
    typeof value === 'undefined' ||
    (typeof value === 'string' && !value.length)
  ) {
    // nothing to do here
    return '';
  }

  if (!rawCellValues) {
    // noop
    return value;
  }

  if (typeof value === 'string') {
    value = value
      // replace quotes and commas
      .replace(/[\\\"\"\'\,\n\r]/gm, '')
      // replace hashtags because they seem to break CSVs
      // see https://gxperts.atlassian.net/browse/UDGB-57
      // and https://stackoverflow.com/questions/63924635/js-built-csv-breaking-after-in-excel
      .replace(/#/g, '%23')
      .trim();
  }

  // Check if we're dealing w/ currency-like fields
  // and if yes, round them up to integers as per UMGB-8
  // The commas in Intl-specific CSV exports often get mistaken for CSV separators
  // so let's make sure we're dealing w/ integers. Plus, people running the CSV exports
  // are interested in big bucks, not cents.
  if (
    (isNumeric(value) &&
      key?.match(
        /(pml|eml|premium|tsi|sum|limit|turnover|shipment|rating_|adjusted|score)/gi
      )) ||
    // Some column names may indeed include reserved words but their cells may not!
    // Make sure we're only rounding up the cell values by checking if they contain currency symbols
    (isString(value) &&
      [CURRENCY_PREFIX_ISO(), CURRENCY_PREFIX_UTF()].some((symbol) =>
        (value as string).includes(symbol)
      ))
  ) {
    value = roundUp(extractNumberFromString(value) || 0, 0);
  }

  return value;
};

/**
 * If a call value contains a currency symbol but we strip those away during the `csvReplacer` process,
 * we'll have to append that symbol to the column name to keep things consistent.
 * @param originalColumnName
 * @param originalRows
 * @returns
 */
const guessColumnSuffix = (
  originalColumnName: string,
  originalRows: Parameters<CsvService['_convert']>[0]
): string => {
  const columnCellsContent = originalRows.map((row) => {
    return row instanceof Map
      ? row.get(originalColumnName)
      : row[originalColumnName];
  });

  let suffix = '';

  const currencyISO = CURRENCY_PREFIX_ISO();
  const currencyUTF = CURRENCY_PREFIX_UTF();

  for (const cell of columnCellsContent) {
    const value = `${cell}`;
    if (value.includes(currencyISO)) {
      if (!suffix) {
        suffix = currencyISO;
        // bail out as soon as we've found it
        break;
      }
    }

    if (value.includes(currencyUTF)) {
      if (!suffix) {
        // Excel hates UTF so let's convert back to ISO (€ -> EUR)
        suffix = mapOfUTFcurrenciesToISO[currencyUTF];
        // bail out as soon as we've found it
        break;
      }
    }
  }

  return suffix;
};

interface NestedJsonToCsvOpts {
  doGuessCurrencySuffix?: boolean;
  columnsWithOrderPriority?: string[]
}

@Injectable({
  providedIn: 'root',
})
export class CsvService {
  constructor() { }

  _current_csv_value_shareable = new Subject<string>();
  _underlying_data_shareable = new Subject<string>();

  private _flatten(data) {
    let result = {};
    function recurse(cur, prop) {
      if (Object(cur) !== cur) {
        result[prop] = cur;
      } else if (Array.isArray(cur)) {
        const l = cur.length;
        for (let i = 0; i < l; i++) {
          recurse(cur[i], prop + '[' + i + ']');
        }
        if (l === 0) {
          result[prop] = [];
        }
      } else {
        let isEmpty = true;
        for (const p in cur) {
          isEmpty = false;
          recurse(cur[p], prop ? prop + '.' + p : p);
        }
        if (isEmpty && prop) {
          result[prop] = {};
        }
      }
    }
    recurse(data, '');

    return result;
  }

  /**
   * Perform conversion and normalization
   * @param data
   * @param rawCellValues
   * @returns
   */
  private _convert(
    data: Array<{ [key: string]: any } | Map<string, any>>,
    rawCellValues = false,
    opts: NestedJsonToCsvOpts = { doGuessCurrencySuffix: false },
    headers: string[] = []
  ) {
    if (!data || !data[0]) {
      return '';
    }

    const header = headers.length > 0 ? headers : data
      .map((row) => (row instanceof Map ? [...row.keys()] : Object.keys(row)))
      .sort((a, b) => {
        const result = a.length < b.length ? -1 : a.length > b.length ? 1 : 0;
        // desc
        return result * -1;
      })[0];

    const headerItemsWithSuffix: { [originalColumnName: string]: string } = {};

    const csv = data.map((row) =>
      header
        .map((columnName) => {
          const replaced = csvReplacer(
            columnName,
            row instanceof Map ? row.get(columnName) : row[columnName],
            rawCellValues
          );

          if (!rawCellValues) {
            return replaced;
          }

          // `guessColumnSuffix`, however optimized, is an expensive operation
          // so only guess it for reasonably slim datasets.
          // Otherwise issues like UDGB-31 will keep coming up.
          // See performance flowchart: https://prnt.sc/26kqe86
          if (opts.doGuessCurrencySuffix && data.length <= 1000) {
            const suffix = guessColumnSuffix(columnName, data);
            if (suffix) {
              headerItemsWithSuffix[columnName] = `${columnName} (${suffix})`;
            }
          }

          // stringifying in case we're dealing w/ JSON objects as values
          return JSON.stringify(replaced);
        })
        .join(CSV_SEPARATOR)
    );

    if (rawCellValues) {
      // enrich header members w/ the guessed suffix after the removal of any non-numeric
      // characters in the cells.
      // Finalizes UMGB-13
      if (Object.keys(headerItemsWithSuffix).length) {
        for (const [oldColumnName, suffixedColumnName] of Object.entries(
          headerItemsWithSuffix
        )) {
          header.splice(header.indexOf(oldColumnName), 1, suffixedColumnName);
        }
      }
    }

    csv.unshift(header.join(CSV_SEPARATOR));

    return csv.join('\n');
  }

  chartjsDatasetToCSV(
    labels: string[],
    datasets: Array<{ data: Array<number>; label: string }>,
    opts: NestedJsonToCsvOpts = { doGuessCurrencySuffix: true }
  ) {
    const prepared = [];

    labels.map((label, label_index) => {
      const inner_vals = {};
      datasets.map((dataset) => {
        const col_name = dataset.label.replace(/ \| just_values/, '');
        const val = dataset.data[label_index];

        inner_vals[col_name] = val;
      });

      prepared.push({
        label,
        ...inner_vals,
      });
    });

    return this._convert(prepared, undefined, opts);
  }

  /**
   * The ER tables are generated via the header an an array of row maps.
   * When users choose to download the CSV, the CSV has already been pre-generated
   * and stored as `csvInput` in the histogram-chart component.
   * Thus, the CSV maps the underlying table 1-to-1 (and vice versa).
   * However, prettified currency cells do not necessarily need to be prettified in the CSV too!
   * It's more maintainable to export raw, unformatted numbers w/ the currency symbol appended to the column name.
   * All of this will be performed in `_convert` and `csvReplacer` but first, the `arrayOfRowMaps`
   * needs to be converted to an array of key-value pairs.
   * Fixes UMGB-13 and UDGB-23.
   * @param header
   * @param arrayOfRowMaps
   * @param rawCellValues
   * @returns
   */
  rowMapsToCSV(
    header: Array<string>,
    arrayOfRowMaps: Array<ITableReadyContentRow>,
    rawCellValues = false
  ) {
    const listOfRows: Array<object> = [];
    const firstColumnName = header[0];

    arrayOfRowMaps.map((rowItem) => {
      const csvRow: Map<string, string | number> = new Map();

      // each `rowItem` contains a key-value pair of `firstColumnValue: {[otherColumnsName]: otherColumnsValue}`
      // so we first need to extract the first cell (0th CSV column) to prevent column shifting
      const firstColumnValue = Object.keys(rowItem)[0];

      csvRow.set(firstColumnName, firstColumnValue);

      // iterate the map itself, extract the key-value pairs, and set them onto the row
      rowItem[firstColumnValue].forEach(
        (columnValue: string | number, columnName) => {
          csvRow.set(columnName, columnValue);
        }
      );

      listOfRows.push(csvRow);
    });

    return this._convert(listOfRows, rawCellValues);
  }

  nestedJSONtoCSV = (
    array_of_items: Array<object>,
    opts: NestedJsonToCsvOpts = { doGuessCurrencySuffix: true },
    headers: string[] = []
  ) => {
    const flattened = array_of_items.map((item) => this._flatten(item));
    return this._convert(flattened, true, opts, this.getSortedHeaders(headers, opts.columnsWithOrderPriority));
  }

  private getSortedHeaders(headers: string[], columnsWithOrderPriority: string[]) {
    if (!columnsWithOrderPriority) {
      return headers;
    }

    const orderPriorityKeys = columnsWithOrderPriority.filter((key) => headers.includes(key))
    const restKeys = headers.filter((key) => !columnsWithOrderPriority.includes(key))
    return [...orderPriorityKeys, ...restKeys];
  }
}
