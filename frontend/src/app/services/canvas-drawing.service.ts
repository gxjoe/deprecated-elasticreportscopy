/*
   jPolygon - a ligthweigth javascript library to draw polygons over HTML5 canvas images.
   Project URL: http://www.matteomattei.com/projects/jpolygon
   Original Author: Matteo Mattei <matteo.mattei@gmail.com>
   Version: 1.0
   License: MIT License
*/

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CanvasDrawingService {
  constructor() {}

  private klass: string;
  private perimeter = new Array();
  private complete = false;
  private canvas: HTMLCanvasElement;
  private ctx;
  private coordinates: Array<{ x: number; y: number }> = [];

  public drawnCoordsObservable: Subject<{
    klass: string;
    coordinates: Array<{ x: number; y: number }>;
  }> = new Subject();

  public setCanvas(_c: HTMLCanvasElement) {
    this.canvas = _c;
  }

  public getCoords() {
    this.drawnCoordsObservable.next({
      klass: this.klass,
      coordinates: this.coordinates
    });
    return this.coordinates;
  }

  private line_intersects(p0, p1, p2, p3) {
    let s1_x, s1_y, s2_x, s2_y;
    s1_x = p1['x'] - p0['x'];
    s1_y = p1['y'] - p0['y'];
    s2_x = p3['x'] - p2['x'];
    s2_y = p3['y'] - p2['y'];

    let s, t;
    s =
      (-s1_y * (p0['x'] - p2['x']) + s1_x * (p0['y'] - p2['y'])) /
      (-s2_x * s1_y + s1_x * s2_y);
    t =
      (s2_x * (p0['y'] - p2['y']) - s2_y * (p0['x'] - p2['x'])) /
      (-s2_x * s1_y + s1_x * s2_y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1) {
      // Collision detected
      return true;
    }
    return false; // No collision
  }

  private point(x: number, y: number) {
    this.ctx.fillStyle = 'black';
    this.ctx.strokeStyle = 'black';
    this.ctx.fillRect(x - 4, y - 4, 8, 8);
    this.ctx.moveTo(x, y);
  }

  private draw(end) {
    if (!this.ctx) {
      this.ctx = this.canvas.getContext('2d');
    }

    this.ctx.lineWidth = 1;
    this.ctx.strokeStyle = 'white';
    this.ctx.lineCap = 'square';
    this.ctx.beginPath();

    for (let i = 0; i < this.perimeter.length; i++) {
      if (i === 0) {
        this.ctx.moveTo(this.perimeter[i]['x'], this.perimeter[i]['y']);
      } else {
        this.ctx.lineTo(this.perimeter[i]['x'], this.perimeter[i]['y']);
      }
      this.point(this.perimeter[i]['x'], this.perimeter[i]['y']);
    }
    if (end) {
      this.ctx.lineTo(this.perimeter[0]['x'], this.perimeter[0]['y']);
      this.ctx.closePath();
      this.ctx.strokeStyle = 'black';
      this.ctx.fillStyle = 'rgb(171, 171, 171)';
      this.ctx.fill();
      this.complete = true;
      this.perimeter.push({
        x: this.perimeter[0]['x'],
        y: this.perimeter[0]['y']
      });
    }
    this.ctx.stroke();
    this.coordinates = this.perimeter;
  }

  private check_intersect(x, y) {
    if (this.perimeter.length < 4) {
      return false;
    }
    const p0 = new Array();
    const p1 = new Array();
    const p2 = new Array();
    const p3 = new Array();

    p2['x'] = this.perimeter[this.perimeter.length - 1]['x'];
    p2['y'] = this.perimeter[this.perimeter.length - 1]['y'];
    p3['x'] = x;
    p3['y'] = y;

    for (let i = 0; i < this.perimeter.length - 1; i++) {
      p0['x'] = this.perimeter[i]['x'];
      p0['y'] = this.perimeter[i]['y'];
      p1['x'] = this.perimeter[i + 1]['x'];
      p1['y'] = this.perimeter[i + 1]['y'];
      if (p1['x'] === p2['x'] && p1['y'] === p2['y']) {
        continue;
      }
      if (p0['x'] === p3['x'] && p0['y'] === p3['y']) {
        continue;
      }
      if (this.line_intersects(p0, p1, p2, p3) === true) {
        return true;
      }
    }
    return false;
  }

  private start(with_draw = false) {
    this.ctx = this.canvas.getContext('2d');
    if (with_draw === true) {
      this.draw(false);
    }
  }

  private point_it(event) {
    if (this.complete) {
      // alert('Polygon already created');
      return false;
    }
    let rect, x, y;

    if (event.ctrlKey || event.which === 3 || event.button === 2) {
      if (this.perimeter.length === 2) {
        alert('You need at least three points for a polygon');
        return false;
      }

      x = this.perimeter[0]['x'];
      y = this.perimeter[0]['y'];

      if (this.check_intersect(x, y)) {
        alert('The line you are drawing cannot intersect another line');
        return false;
      }

      this.draw(true);
      // alert('Polygon closed');
      event.preventDefault();
      return false;
    } else {
      // correct for improperly sized canvas on Windows
      this.canvas.height = this.canvas.scrollHeight;
      this.canvas.width = this.canvas.scrollWidth;

      rect = this.canvas.getBoundingClientRect();

      x = event.clientX - rect.left;
      y = event.clientY - rect.top;
      if (
        this.perimeter.length > 0 &&
        x === this.perimeter[this.perimeter.length - 1]['x'] &&
        y === this.perimeter[this.perimeter.length - 1]['y']
      ) {
        // same point - double click
        return false;
      }
      if (this.check_intersect(x, y)) {
        alert(
          'The line you are attempting to draw will intersect another line'
        );
        return false;
      }

      this.perimeter.push({ x: x, y: y });
      this.draw(false);

      return false;
    }
  }

  public close() {
    const self = this;
    self.draw(true);

    self.canvas.onclick = null;
    self.canvas.style.setProperty('cursor', 'default');
    self.getCoords();
  }

  private removeZombieCanvases() {
    const elements = document.querySelectorAll(
      `app-histogram-table-chart[klass*=${this.klass}] canvas.for_drawing, ` +
        `app-line-chart[klass*=${this.klass}] canvas.for_drawing`
    );
    [].forEach.call(elements, el => el.remove());
  }

  public canClosePath() {
    return (
      this.canvas &&
      this.ctx &&
      this.coordinates &&
      this.coordinates.length >= 3
    );
  }

  public begin(_canvas: HTMLCanvasElement, klass: string) {
    const self = this;

    self.klass = klass;

    this.removeZombieCanvases();

    const new_canvas = _canvas.cloneNode() as HTMLCanvasElement;
    _canvas.parentElement.appendChild(new_canvas);
    new_canvas
      .getContext('2d')
      .clearRect(0, 0, new_canvas.width, new_canvas.height);

    this.canvas = new_canvas;
    this.canvas.addEventListener('click', function(evt) {
      self.point_it(evt);
    });
    self.canvas.classList.add('for_drawing');
    self.canvas.style.setProperty('cursor', 'crosshair');

    this.start(true);
    return new_canvas;
  }

  public undo() {
    this.ctx = undefined;
    this.perimeter.pop();
    this.complete = false;
    this.coordinates = [];
    this.start(true);
  }

  public clear_canvas() {
    this.ctx = undefined;
    this.removeZombieCanvases();

    this.perimeter = new Array();
    this.complete = false;
    this.coordinates = [];
    this.start(true);

    this.getCoords();
  }
}
