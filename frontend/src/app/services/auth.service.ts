
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, Subject } from 'rxjs';


import { getCookie, setCookie, eraseCookie } from '../elastic/constants';
import * as ObjTree from 'objtree';
import {
  findByKeyInNestedObject,
  nestedBoolMustOfBoolShoulds,
  emptyBoolMustMatch
} from '../elastic/utils';

export interface SuccessfullyParsedXMLResponse {
  country_codes: Array<string>;
  usernames: Array<string>;
  sections: Array<string>;
  user: string;
  userType_based_queries: any;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }

  public authProcessing: Subject<boolean> = new Subject<boolean>();

  private xmlObjTree: any;

  private getXMLcore(): {
    allCountries?: string;
    config?: {
      sections: {
        section: Array<string>;
      };
      userType: string;
      usersAllowed?: {
        userAllowed: Array<string>;
      };
    };
    countries: {
      country: Array<string>;
    };
    username: string;
  } {
    return findByKeyInNestedObject(this.xmlObjTree, 'return').return;
  }

  private parseAllowedSections() {
    let to_return = this.getXMLcore().config.sections.section;

    if (!Array.isArray(to_return)) {
      to_return = [to_return];
    }

    return new Array(...new Set(to_return));
  }

  private parseAllowedUsers() {
    if (!this.getXMLcore().config.usersAllowed) {
      return [];
    }

    let to_return = this.getXMLcore().config.usersAllowed.userAllowed;
    if (!Array.isArray(to_return)) {
      to_return = [to_return];
    }

    return new Array(...new Set(to_return));
  }

  private parseCountryCodes() {
    let to_return =
      this.getXMLcore().allCountries === 'true'
        ? ['ALL']
        : this.getXMLcore().countries.country;
    if (!Array.isArray(to_return)) {
      to_return = [to_return];
    }

    return new Array(...new Set(to_return));
  }

  private parseUser() {
    return this.getXMLcore().username;
  }

  private parseUsernames() {
    return this.parseAllowedUsers();
  }

  private parseUserTypeBasedQueries() {
    const user_type = this.getXMLcore().config.userType;
    const country_codes = this.parseCountryCodes();
    const allowed_users = [
      ...new Set([].concat(this.parseAllowedUsers()).concat(this.parseUser()))
    ];

    let list_of_query_restrictions = [];

    if (country_codes.length) {
      if (!country_codes.includes('ALL')) {
        list_of_query_restrictions = [].concat(
          nestedBoolMustOfBoolShoulds(
            'offer',
            'offer.requestFromCountry',
            country_codes
          )
        );
      } else {
        list_of_query_restrictions = [].concat(
          nestedBoolMustOfBoolShoulds(
            'offer',
            'offer.requestFromCountry',
            country_codes.filter(country => country !== 'ALL')
          )
        );
      }
    }

    switch (user_type) {
      case 'UCB':
      // country manager's countries are already in the query restrictions!
      case 'COUNTRY_MANAGER':
        break;
      case 'UNDERWRITER_TEAM_LEADER':
      case 'UNDERWRITER_LOCAL':
        list_of_query_restrictions.push(
          nestedBoolMustOfBoolShoulds(
            'lob.localUnderwriters',
            'lob.localUnderwriters.username',
            allowed_users
          )
        );
        break;
      case 'RISK_ENGINEER_TEAM_LEADER':
      case 'RISK_ENGINEER_LOCAL':
        list_of_query_restrictions.push(
          nestedBoolMustOfBoolShoulds(
            'lob.addresses.locationProperty',
            'lob.addresses.locationProperty.globalLocationDataCreatedBy',
            allowed_users
          )
        );
        break;
      case 'SALES_MANAGER_TEAM_LEADER':
      case 'SALES_MANAGER_LOCAL':
        list_of_query_restrictions.push(
          nestedBoolMustOfBoolShoulds(
            'offer.salesManagers',
            'offer.salesManagers.username',
            allowed_users
          )
        );
        break;
    }

    return list_of_query_restrictions;
  }

  public parseXmlAuthResponse(): SuccessfullyParsedXMLResponse {
    let parsed: SuccessfullyParsedXMLResponse;

    if (getCookie('x-cbnboost-restrictions')) {
      parsed = JSON.parse(getCookie('x-cbnboost-restrictions'));
    } else {
      parsed = {
        usernames: this.parseUsernames(),
        country_codes: this.parseCountryCodes(),
        sections: this.parseAllowedSections(),
        user: this.parseUser(),
        userType_based_queries: this.parseUserTypeBasedQueries()
      };
    }

    // console.warn({ parsed, tree: this.xmlObjTree });

    window['CBNBOOST_QUERY_RESTRICTIONS'] = parsed;
    return parsed;
  }

  doCheckAuth(): Promise<SuccessfullyParsedXMLResponse> {
    // const parsed = {
    //   usernames: [],
    //   country_codes: ['ALL'],
    //   sections: ['EUM', 'MAP', 'PPM', 'REM', 'SEM', 'ESG'],
    //   user: 'vbe0739',
    //   userType_based_queries: [{ bool: { should: [] } }]
    // };
    // setCookie('x-cbnboost-auth', '1');
    // eraseCookie('x-cbnboost-restrictions');
    // setCookie('x-cbnboost-restrictions', JSON.stringify(parsed));
    // window['CBNBOOST_QUERY_RESTRICTIONS'] = parsed;
    // this.authProcessing.next(false);
    // return new Promise(re => re(parsed));

    if (!environment.cbn_authentication.host) {
      return new Promise(r => {
        const xml_mock = `<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ns2:elasticReportDataResponse xmlns:ns2="http://ws.cbn.insdata.sk/services/cbn"><return><allCountries>true</allCountries><config><sections><section>EUM</section><section>MAP</section><section>PPM</section><section>REM</section><section>SEM</section><section>ESG</section></sections><userType>UCB</userType><usersAllowed/></config><countries><country>ALL</country></countries><username>vbe0739</username></return></ns2:elasticReportDataResponse></soap:Body></soap:Envelope>`;
        this.xmlObjTree = new ObjTree().parseXML(xml_mock);

        this.authProcessing.next(false);
        r(this.parseXmlAuthResponse());
      });
    }

    let token: string;

    const from_url = new URL(window.location.href).searchParams.get('token');
    const from_cookie = getCookie('x-cbnboost-token');

    if (from_url) {
      this.logout();
      eraseCookie('x-cbnboost-token');
    }

    token = from_url || from_cookie;

    if (window.location.href.includes('appspot')) {
      // token = token || '5h4UqrDoB5uI47IESaBDQShA4Npyfz6Q';
    } else {
      token = token || environment.defaultCbnToken;
    }

    return new Promise((resolve, reject) => {
      if (!token) {
        this.authProcessing.next(false);
        console.error(`No Token has been provided. Please provide one...`);
        this.flushDocument();
        reject();
        return;
      }

      this.authProcessing.next(true);

      if (this.isAuthenticated) {
        this.authProcessing.next(false);
        resolve(this.parseXmlAuthResponse());
        return;
      }

      setCookie('x-cbnboost-token', token);

      this.bounceToken(token).subscribe(
        response => {
          if (response === true) {
            this.authProcessing.next(false);
            console.debug(`successfully parsed token ${token}`, response);
            resolve(this.parseXmlAuthResponse());
          } else {
            console.error(
              `The provided Token could not be verified. Please try another one.`
            );
            this.authProcessing.next(true);
            console.error(`${response}`);
            reject();
          }
        },
        err => {
          console.error(err);
          this.authProcessing.next(true);
          this.flushDocument();
          reject(err);
        }
      );
    });
  }

  bounceToken(token?: string): Observable<boolean> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/xml',
      Accept: '*/*, text/xml'
    });

    return this.http
      .post(
        environment.cbn_authentication.host,
        `
      <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cbn="http://ws.cbn.insdata.sk/services/cbn">
        <s:Header/>
        <s:Body>
          <cbn:elasticReportData>
            <token>${token}</token>
          </cbn:elasticReportData>
        </s:Body>
      </s:Envelope>
    `,
        { headers: headers, responseType: 'text' }
      ).pipe(
        map((res: string) => {
          this.xmlObjTree = new ObjTree().parseXML(res);

          const parsed = this.parseXmlAuthResponse();
          const success = parsed.userType_based_queries.length > 0;

          console.debug(parsed, this.xmlObjTree);

          if (success) {
            setCookie('x-cbnboost-auth', '1');
            eraseCookie('x-cbnboost-restrictions');
            setCookie('x-cbnboost-restrictions', JSON.stringify(parsed));
            window['CBNBOOST_QUERY_RESTRICTIONS'] = parsed;
          }

          return success;
        }));
  }

  flushDocument() {
    document.body.innerHTML = '<h2 style="padding:20px">Not allowed</h2>';
  }

  get isAuthenticated() {
    return getCookie('x-cbnboost-auth') && getCookie('x-cbnboost-restrictions');
  }

  logout() {
    eraseCookie('x-cbnboost-auth');
    eraseCookie('x-cbnboost-restrictions');
    delete window['CBNBOOST_QUERY_RESTRICTIONS'];
  }
}
