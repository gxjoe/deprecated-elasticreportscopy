import { Injectable } from '@angular/core';
import { prettyPercent, sumByMapReduce } from '../charts/utils';
import { pprintCurrency } from '../utils/helpers/helpers';
import * as Immutable from 'immutable';
import * as ChartDataLabels from 'chartjs-plugin-datalabels';
import * as Annotation from 'chartjs-plugin-annotation';

export enum onAnimationCompleteTechniques {
  noop = 0,
  draw_above_bar_chart = 1,
  just_values = 2
}

export enum annotationTechniques {
  noop = 0,
  hundred_percent_line = 1
}

export interface IChartOptions {
  percentual_second_y_axis?: boolean;
  customAnnotations?: any;
  fully_custom_y_axis?: any;
  type?: string;
  labels?: Array<string>;
  datasets?: Array<any>;
  labelCallback?: Function;
  annotationTechniques?: annotationTechniques;
  onAnimationComplete?: onAnimationCompleteTechniques;
  tickOptions?: object;
  legendOptions?: object;
  stackOptions?: {
    stacked: boolean;
  };
  options?: any;
  customColors?: Array<any>;
  customChartDataLabels?: any;
  xAxisOpts?: any;
  yAxisOpts?: {
    [key: string]: {
      display: boolean;
      labelString: string;
    };
  };
  onClick?: Function;
  plugins?: Array<any>;
}

enum FontsAndSizes {
  fontSize = 14,
  padding = 15,
  largeFontSize = 17,
  topPadding = 10
}

export const generateVericalAnnotationLabel = (
  id: any,
  value: any,
  content: any,
  intensity: 'strong' | 'mild',
  xScaleID?,
  yScaleID?
) => {
  xScaleID = xScaleID || 'x-axis-0';
  return {
    id: 'hline' + `${id}`,
    type: 'line',
    scaleID: xScaleID,
    mode: 'vertical',
    value: value,
    endValue: value,
    ...(intensity === 'strong'
      ? {
        borderColor: '#444',
        borderDash: [10, 20],
        borderWidth: 2
      }
      : {
        borderColor: '#888',
        borderDash: [5, 10],
        borderWidth: 1
      }),
    label: {
      enabled: true,
      fontColor: '#000',
      content: `${content}`,
      position: 'top',
      cornerRadius: 3,
      fontSize: 12,
      ...(intensity === 'strong'
        ? {
          backgroundColor: '#DDD',
          yAdjust: 5
        }
        : {
          backgroundColor: '#EEE',
          yAdjust: 10
        })
    }
  };
};

export const weeklyHistogramXaxisOpts = () => {
  return {
    scaleLabel: {
      display: true,
      labelString: (window as any).dateRangeMagnitude === 'month' ? 'Week starting with day X' : 'Day X'
    }
  };
};

export const _labelBarChartWithValues = function () {
  // `this` is canvas, not `service`

  const chartInstance = (this as any).chart,
    ctx = chartInstance.ctx;

  ctx.font = (window as any).Chart.helpers.fontString(
    FontsAndSizes.fontSize,
    (window as any).Chart.defaults.global.defaultFontStyle,
    (window as any).Chart.defaults.global.defaultFontFamily
  );
  ctx.textAlign = 'center';
  ctx.textBaseline = 'bottom';
  ctx.textColor = '#000000';

  const use_percent = !(this as any).data.datasets[0].label.includes(
    'just_values'
  );

  (this as any).data.datasets.forEach(function (dataset, i) {
    const meta = chartInstance.controller.getDatasetMeta(i);
    meta.data.forEach(function (bar, index) {
      let data = dataset.data[index];
      data = use_percent
        ? parseFloat(data) <= 100
          ? prettyPercent(data, 100)
          : pprintCurrency(data)
        : data;
      if (chartInstance.config.type === 'horizontalBar') {
        ctx.fillText(data, bar._model.x + 10, bar._model.y + 10);
        ctx.fillStyle = '#000000';
      } else {
        ctx.fillText(data, bar._model.x, bar._model.y - 5);
        ctx.fillStyle = '#000000';
      }
    });
  });
};

@Injectable({
  providedIn: 'root'
})
export class ChartjsService {
  private _animation_options = {
    duration: 0
  };

  private _layout_options = {
    padding: {
      top: FontsAndSizes.padding
    }
  };

  private _legend_options = {
    labels: {
      usePointStyle: false,
      fontSize: FontsAndSizes.fontSize,
      padding: FontsAndSizes.padding
    }
  };

  private _scales_options = Immutable.fromJS({
    yAxes: [
      {
        id: 'normal_y_axis',
        ticks: {
          beginAtZero: true
        }
      }
    ]
  });

  private _hundertPercentLine = {
    annotations: [
      {
        drawTime: 'afterDatasetsDraw',
        id: 'hline',
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        value: 100,
        borderColor: 'black',
        borderWidth: 3,
        label: {
          backgroundColor: 'red',
          content: '100%',
          enabled: true
        }
      }
    ]
  };

  private _label_callback = function (item, data) {
    const datasetLabel = data.datasets[item.datasetIndex].label || '';
    return datasetLabel + ': ' + item.xLabel;
  };

  private _noop = () => { };

  constructor() {
    this.unregisterChartDataLabels();
  }

  unregisterChartDataLabels() {
    this._windowChart.plugins.unregister(ChartDataLabels);
  }

  registerChartDataLabels() {
    this._windowChart.plugins.register(ChartDataLabels);
  }

  get _windowChart() {
    return (window as any).Chart;
  }

  constructPieChart(
    nativeElement: HTMLCanvasElement,
    __options: IChartOptions
  ) {
    if (__options.customChartDataLabels) {
      this.registerChartDataLabels();
    } else {
      this.unregisterChartDataLabels();
    }

    const context = nativeElement.getContext('2d');
    context.clearRect(0, 0, nativeElement.width, nativeElement.height);

    return new this._windowChart(nativeElement, {
      type: __options.type,
      data: {
        labels: __options.labels,
        datasets: __options.datasets
      },
      options: {
        legend: __options.legendOptions || this._legend_options,
        layout: this._layout_options,
        animation: this._animation_options,
        responsive: false,
        maintainAspectRatio: false,
        tooltips: {
          enabled: true,
          callbacks: {
            label: __options.labelCallback
          },
          bodyFontSize: FontsAndSizes.fontSize
        }
      }
    });
  }

  constructNormalizedHorizontalBarChart(
    nativeElement: HTMLCanvasElement,
    __options: IChartOptions
  ) {
    this.constructNormalizedVerticalBarChart(
      nativeElement,
      __options,
      'horizontalBar'
    );
  }

  constructNormalizedVerticalBarChart(
    nativeElement: HTMLCanvasElement,
    __options: IChartOptions,
    type = 'bar'
  ) {
    if (__options.customChartDataLabels) {
      this.registerChartDataLabels();
    } else {
      this.unregisterChartDataLabels();
    }

    if (
      __options &&
      (__options.customAnnotations ||
        __options.annotationTechniques !== annotationTechniques.noop)
    ) {
      if (!(window as any).Chart.Annotation) {
        (window as any).Chart.Annotation = Annotation;
      }
    }

    const stackedSettings = this._scales_options.toJSON();
    const _constructStackedSettings = (
      should_stack = false,
      input_options: any = {}
    ) => {
      (stackedSettings.xAxes as any) = [
        {
          minRotation: 190,
          barPercentage: input_options.percentual_second_y_axis ? 0.4 : 0.9,
          ...(__options.type === 'scatter'
            ? {
              ...__options.xAxisOpts
            }
            : {
              // Show x-axis labels for ALL non-scatter plot bar charts
              // https://gxperts.atlassian.net/browse/UMGB-29
              // https://github.com/chartjs/Chart.js/issues/2801
              ticks: {
                autoSkip: false,
                stepSize: 1,
              }
            })
        }
      ];

      if (typeof input_options.tickOptions.max === 'undefined') {
        input_options.tickOptions.max = Math.max.apply(
          null,
          __options.datasets
            .filter(dset => dset.label && !dset.label.startsWith('Number of'))
            .map(dset => Math.max.apply(null, dset.data))
        );
      }

      input_options.tickOptions.max = input_options.tickOptions.max * 1.2;

      // const tmp = roundUpToNearestN(input_options.tickOptions.max * 1.1, false); // always make it 20% taller to get fit tooltips onto the canvas

      // if (String(tmp).length - String(input_options.tickOptions.max).length >= 3) {
      //   input_options.tickOptions.max *= 1.1;
      // } else {
      //   input_options.tickOptions.max = tmp;
      // }

      (stackedSettings.yAxes as any)[0] = {
        id: 'normal_y_axis',
        stacked: should_stack,
        ticks: {
          ...stackedSettings.yAxes[0].ticks,
          ...input_options.tickOptions
        },
        ...{ ...(input_options.yAxisOpts || {}) }
      };
      if (input_options.percentual_second_y_axis) {
        (stackedSettings.yAxes as any).push({
          position: 'right',
          id: 'percentual_y_axis',
          gridLines: {
            display: false
          },
          ticks: {
            beginAtZero: true,
            max: 120,
            callback: function (value) {
              return `${value} %`;
            }
          }
        });
      }

      if (input_options.fully_custom_y_axis) {
        // if (input_options.fully_custom_y_axis.ticks) {
        //  input_options.fully_custom_y_axis.ticks.max *= 1.2;
        // }
        (stackedSettings.yAxes as any).push({
          ...input_options.fully_custom_y_axis
        });
      }

      return stackedSettings;
    };

    const context = nativeElement.getContext('2d');
    context.clearRect(0, 0, nativeElement.width, nativeElement.height);

    const opts = {
      plugins: {
        afterrender: {},
        annotation: __options.customAnnotations
          ? __options.customAnnotations
          : {},
        ...{ ...(__options.plugins ? __options.plugins : {}) }
      },
      type: type,
      data: {
        labels: __options.labels,
        datasets: __options.datasets
      },
      options: {
        plugins: {
          datalabels: __options.customChartDataLabels || {},
          annotation: __options.customAnnotations
            ? __options.customAnnotations
            : {}
        },
        onClick: __options.onClick || (() => { }),
        legend:
          __options.legendOptions === null
            ? {
              display: false
            }
            : { ...this._legend_options, ...__options.legendOptions },
        layout: this._layout_options,
        responsive: false,
        maintainAspectRatio: false,
        animation: {
          onComplete: function (e) {
            const should_draw =
              __options.onAnimationComplete !==
              onAnimationCompleteTechniques.noop;
            if (!should_draw) {
              this.options.animation.onComplete = () => ''; // disable after first render
              return;
            }

            _labelBarChartWithValues.call(this);
          },
          ...this._animation_options
        },
        tooltips: {
          enabled: __options.labelCallback ? true : false,
          yAlign: 'bottom',
          callbacks: {
            title: () => '',
            label: __options.labelCallback
              ? __options.labelCallback
              : this._noop
          },
          // custom: __options.labelCallback,
          bodyFontSize: FontsAndSizes.largeFontSize,
          xPadding: FontsAndSizes.padding,
          yPadding: FontsAndSizes.padding,
          displayColors: false,
          position: 'average'
        },
        scales: _constructStackedSettings(
          __options.stackOptions.stacked,
          __options
        ),
        annotation:
          __options.annotationTechniques ===
            annotationTechniques.hundred_percent_line
            ? this._hundertPercentLine
            : __options.customAnnotations
              ? __options.customAnnotations
              : {},
        onHover: function () { },
        ...(__options?.options ? __options.options : {}),
      }
    };

    const chart = new this._windowChart(nativeElement, Object.assign({}, opts));

    if (__options.percentual_second_y_axis) {
      chart.boxes[3].chart.options.elements = {
        line: {
          tension: 0
        }
      };
      chart.boxes[3].chart.update();
    }

    if (__options.fully_custom_y_axis) {
      chart.boxes[chart.boxes.length - 1].chart.options.elements = {
        line: {
          tension: 0,
          showLine: false
        }
      };
      chart.boxes[chart.boxes.length - 1].chart.update();
    }

    return chart;
  }

  constructLineChart(
    nativeElement: HTMLCanvasElement,
    __options: IChartOptions
  ) {
    if (__options.customChartDataLabels) {
      this.registerChartDataLabels();
    } else {
      this.unregisterChartDataLabels();
    }

    if (
      __options &&
      (__options.customAnnotations ||
        __options.annotationTechniques !== annotationTechniques.noop)
    ) {
      if (!(window as any).Chart.Annotation) {
        (window as any).Chart.Annotation = Annotation;
      }
    }

    const stackedSettings = this._scales_options.toJSON();
    const _constructStackedSettings = (should_stack = false) => {
      (stackedSettings.xAxes as any) = [
        {
          autoSkip: false,
          minRotation: 190,
          ...__options.xAxisOpts
        }
      ];
      (stackedSettings.yAxes as any)[0] = {
        stacked: should_stack,
        ticks: {
          ...stackedSettings.yAxes[0].ticks,
          ...__options.tickOptions
        },
        ...__options.yAxisOpts
      };

      if (__options.fully_custom_y_axis) {
        __options.fully_custom_y_axis.ticks.max = parseInt(
          '' + __options.fully_custom_y_axis.ticks.max * 1.2,
          10
        );
        (stackedSettings.yAxes as any).push({
          ...__options.fully_custom_y_axis,
          ...{
            scaleLabel: {
              display: true,
              labelString: __options.fully_custom_y_axis.labelString || ''
            }
          }
        });
      }
      return stackedSettings;
    };

    const context = nativeElement.getContext('2d');
    context.clearRect(0, 0, nativeElement.width, nativeElement.height);

    const a = {
      plugins: {
        afterrender: {},
        annotation: __options.customAnnotations
          ? __options.customAnnotations
          : {}
      },
      type: 'line',
      data: {
        labels: __options.labels,
        datasets: __options.datasets
      },
      options: {
        plugins: {
          annotation: __options.customAnnotations
            ? __options.customAnnotations
            : {}
        },
        elements: {
          line: {
            tension: 0 // disables bezier curves
          }
        },
        legend: __options.legendOptions === null ? false : this._legend_options,
        layout: this._layout_options,
        animation: {
          onComplete: function (e) {
            const should_draw =
              __options.onAnimationComplete !==
              onAnimationCompleteTechniques.noop;
            if (!should_draw) {
              this.options.animation.onComplete = null; // disable after first render
              return;
            }

            _labelBarChartWithValues.call(this);
          },
          ...this._animation_options
        },
        tooltips: {
          enabled: __options.labelCallback ? true : false,
          callbacks: {
            label: __options.labelCallback
              ? __options.labelCallback
              : this._noop
          },
          // custom: __options.labelCallback,
          bodyFontSize: FontsAndSizes.fontSize
        },
        scales: _constructStackedSettings(__options.stackOptions.stacked),
        annotation:
          __options.annotationTechniques ===
            annotationTechniques.hundred_percent_line
            ? this._hundertPercentLine
            : __options.customAnnotations
              ? __options.customAnnotations
              : {},
        onHover: function () { }
      }
    };

    const chart = new this._windowChart(nativeElement, a);

    return chart;
  }
}
