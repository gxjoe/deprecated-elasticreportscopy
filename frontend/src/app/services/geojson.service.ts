import { Injectable } from '@angular/core';

import {
  polyLineTransparentStrokeOptions,
  polyLineFullStrokeOptions
} from '../utils/helpers/gmaps';
import { ElasticService } from './elastic.service';
import { Feature, FeatureCollection, Point, clone, featureCollection, point, pointsWithinPolygon, polygon } from '@turf/turf';

interface IStringToPolylineMap {
  [id: string]: google.maps.Polyline;
}

interface IGroupedPolylines {
  active: Array<string>;
  all: IStringToPolylineMap;
}

@Injectable({
  providedIn: 'root'
})
export class GeojsonService {
  constructor(private esService: ElasticService) { }

  private polylines: IGroupedPolylines = {
    active: [],
    all: {}
  };

  get all() {
    return this.polylines.all;
  }

  setActivePolylines(ids: Array<any>) {
    this.hideAllPolylines(ids);
    Object.keys(this.polylines.all).map(key => {
      if (ids.includes(key)) {
        this.polylines.all[key].setVisible(true);
        this.polylines.all[key].setOptions({
          ...polyLineFullStrokeOptions
        });
      }
    });
    this.polylines.active = ids.slice();
  }

  unsetActivePolylines() {
    this.showAllPolylines();
  }

  private hideAllPolylines(except_ids: Array<any> = [], remove = false) {
    Object.keys(this.polylines.all).map(key => {
      if (!except_ids.includes(key)) {
        this.polylines.all[key].setVisible(false);
        this.polylines.all[key].setOptions({
          ...polyLineTransparentStrokeOptions
        });
        if (remove) {
          this.polylines.all[key].setMap(null);
          delete this.polylines.all[key];
        }
      }
    });

    if (remove) {
      this.esService.activeMapComponent.map.data.setMap(null);
    }
  }

  private showAllPolylines() {
    Object.keys(this.polylines.all).map(key => {
      this.polylines.all[key].setVisible(true);
      this.polylines.all[key].setOptions({
        ...polyLineTransparentStrokeOptions
      });
    });
  }

  public setPolylines(polylines: IStringToPolylineMap) {
    Object.entries(polylines).forEach(([id, line]) => {
      line.setMap(this.esService.activeMapComponent.map);
      this.polylines.all[id] = line;
    });
  }

  public unsetPolylines() {
    this.hideAllPolylines([], true);
  }

  mergeCollections(...args): FeatureCollection {
    return featureCollection(
      [].concat.apply([], [...args.map(a => a.features)])
    );
  }

  mergeArrays(...args) {
    return [].concat.apply([], [...args]);
  }

  createCanvasPointCollection(
    items: Array<{
      x: number;
      y: number;
      hit: any;
    }>
  ): FeatureCollection<Point> {
    const collection: Array<Feature> = [];

    items.map(item => {
      // wrap in try/catch due to UDGB-44
      try {
        collection.push(point([item.y, item.x], { ...item.hit }));
      } catch (err) {
        console.warn('createCanvasPointCollection err', { err });
      }
    });

    return featureCollection(collection as any);
  }

  filterCanvasPointsByBoundary(
    collection: FeatureCollection<Point>,
    boundary: Array<{ x: number; y: number }>,
    // UMGB-7 added the possibility to filter collection
    // members according to their score assessment level (for now)
    collectionMembersFilteringFn?: Function
  ): FeatureCollection<Point> {
    const poly = polygon([
      boundary.map(point => {
        return [point.y, point.x];
      })
    ]);

    // don't touch the original collection -- work w/ a copy
    const collectionCopy: typeof collection = clone(collection);
    collectionCopy.features = collectionCopy.features
      .filter(
        typeof collectionMembersFilteringFn !== 'undefined'
          // do filter the collection if a filtering fn has been provided
          ? collectionMembersFilteringFn as any
          : _ => true
      );

    return pointsWithinPolygon(collectionCopy, poly);
  }
}
