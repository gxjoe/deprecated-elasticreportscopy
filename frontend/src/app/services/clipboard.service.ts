import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClipboardService {
  constructor() {}

  setFromInputId(id) {
    /* Get the text field */
    const copyText = document.getElementById(id) as HTMLInputElement;

    /* Select the text field */
    copyText.select();

    /* Copy the text inside the text field */
    document.execCommand('copy');

    /* Alert the copied text */
    // alert(copyText.value);

    (navigator as any).clipboard.writeText(copyText.value).then(
      function() {
        /* clipboard successfully set */
        console.info('set clipboard');
      },
      function() {
        /* clipboard write failed */
        console.info('could not set clipboard');
      }
    );
  }
}
