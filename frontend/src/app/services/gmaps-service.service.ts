import { Injectable } from "@angular/core";
import { findLayerByLabel } from "../geam-layers/constants";
import proj4 from "proj4";
import { BehaviorSubject, Observable, catchError, map, of } from "rxjs";
import * as R from "ramda";
import { environment } from "src/environments/environment";
import { json2table } from "../utils/helpers/json2html";
import { HttpClient } from "@angular/common/http";
import {
  BigQueryCircleTypeColumns,
  BigQueryIconTypeColumns,
  BigQueryLineTypeColumns,
  BigQueryNormalTypeColumns,
  CustomLayerColorScheme,
  Layer,
  LayerColorScheme,
  METEORITE_RATIO_DIVIDER,
  WhiteColorWithCustomLayerOpacity,
  WhiteColorWithFullOpacity,
  addCustomLayerOpacity,
  fetchProxiedImageBlob,
  getLayerFeatureColor,
} from "../geam-layers/geam2-constants";
import { Geometry } from "geojson";

import { CartoLayer, MAP_TYPES } from "@deck.gl/carto/typed";
import { TileLayer } from "@deck.gl/geo-layers/typed";
import { GoogleMapsOverlay } from "@deck.gl/google-maps/typed";
import { BitmapLayer, GeoJsonLayer } from "@deck.gl/layers/typed";
import { PerilSource } from "../geam-layers/perils";
import { PickingInfo } from "@deck.gl/core/typed";
import {
  prepareLiveUaMapDeckglProps,
  processLiveUaMap,
} from "../geam-layers/post-processing-functions";
import { filterObjectPropertiesByKeyRegexes } from "../charts/histogram-table-chart/common";

type DisplayedDeckLayer = CartoLayer | TileLayer | GeoJsonLayer | BitmapLayer;

@Injectable({
  providedIn: "root",
})
export class GoogleMapsService {
  apiLoaded: Observable<boolean>;

  constructor(private http: HttpClient) {
    this.apiLoaded = this.http
      .jsonp(
        `https://maps.googleapis.com/maps/api/js?key=${environment.gmaps_api_key}&libraries=visualization,geometry,drawing,places&v=quarterly`,
        "callback"
      )
      .pipe(
        map(() => true),
        catchError(() => of(false))
      );
  }

  infowindow = new google.maps.InfoWindow();
  private native_map_: google.maps.Map;

  containerLoadingObservable: BehaviorSubject<boolean> = new BehaviorSubject(
    false
  );

  public hotspotModeActive$: BehaviorSubject<boolean> = new BehaviorSubject(
    false
  );

  private deck: GoogleMapsOverlay;

  service: google.maps.places.PlacesService;

  public selectedLayersLabels$: BehaviorSubject<string[]> = new BehaviorSubject(
    []
  );

  // State-keeping for catnet legends - similar to the useEffect this was imported from.
  // Check `handleCatnetLegendRequest` in the Geam2 repo if needed.
  private catnetPngBlobs$: BehaviorSubject<{ label: string; src: Blob }[]> =
    new BehaviorSubject([]);
  getCatnetPngBlobs() {
    return this.catnetPngBlobs$.asObservable();
  }

  setMap(native_map: google.maps.Map) {
    this.native_map_ = native_map;
  }

  get map() {
    return this.native_map_;
  }

  getMap() {
    return this.native_map_;
  }

  addDataLayer(layer: Layer) {
    const self = this;
  }

  getBBox(coord, zoom, srs) {
    const zpow = Math.pow(2, zoom);
    const llPoint = new google.maps.Point(
      (coord.x * 256) / zpow,
      ((coord.y + 1) * 256) / zpow
    );
    const urPoint = new google.maps.Point(
      ((coord.x + 1) * 256) / zpow,
      (coord.y * 256) / zpow
    );
    const projection = this.map.getProjection();
    const ll = projection.fromPointToLatLng(llPoint);
    const ur = projection.fromPointToLatLng(urPoint);

    if (srs === "EPSG:4326") {
      return ll.lng() + "," + ll.lat() + "," + ur.lng() + "," + ur.lat();
    } else if (srs === "EPSG:900913" || srs === "EPSG:3857") {
      const llProj = proj4("WGS84", "GOOGLE", [ll.lng(), ll.lat()]);
      const urProj = proj4("WGS84", "GOOGLE", [ur.lng(), ur.lat()]);
      return llProj[0] + "," + llProj[1] + "," + urProj[0] + "," + urProj[1];
    } else {
      console.error("Please add the new projection to wmsLayers.js");
    }
  }

  setRailroadsStyle() {
    this.map.setMapTypeId(
      environment.gmaps_settings.mapStyledArrNamewithRailroads
    );
  }

  unsetRailroadsStyle() {
    this.map.setMapTypeId(environment.gmaps_settings.mapStyledArrName);
  }

  /*
  DECK.GL PREPARATION
  */

  setDeckLayers(...layers: DisplayedDeckLayer[]) {
    this.containerLoadingObservable.next(true);

    if (this.deck) {
      this.deck?.setMap(null);
      this.deck = null;

      // prevent multiple deckgl-overlay canvases from existing in the dom
      // which *might* result in "Context lost - multiple Deckgl instances running"
      Array.from(document.querySelectorAll("[id^=deckgl-overlay]")).map((cvs) =>
        cvs.parentNode.removeChild(cvs)
      );
    }

    this.deck = new GoogleMapsOverlay({
      // semi-random ID of the actual canvas even
      id: `deckgl-overlay-${(Math.random() * 10e5).toFixed(0)}`,
      layers,
      getCursor: ({ isHovering }) => {
        if (isHovering) {
          this.map.setOptions({ draggableCursor: "pointer" });
        } else {
          this.map.setOptions({ draggableCursor: "" });
        }

        return "";
      },
    });

    (window as any).displayedLayers = layers;
    this.deck.setMap(this.map);

    this.containerLoadingObservable.next(false);
  }

  private prepareLayerForBigQuery(layer: Layer) {
    const { lookupSource, lookupSourceTileSet, label } = layer;
    if (!lookupSource && !lookupSourceTileSet) {
      console.error(`Missing lookup source for layer ${label}`);
      return;
    }
    const isTileSet = Boolean(lookupSourceTileSet);
    // TODO: the abstraction with the following types is not ideal.. however
    //  for the sake of time and implementation, not reworking for now
    const isCircleType =
      label === "dams" ||
      label === "powerPlants" ||
      label === "ports" ||
      label === "urbanAreas" ||
      label === "tsunamiEvents(2000BC-2015)" ||
      label === "meteoriteStrikes" ||
      label === "earthquakeEuropeMidEast" ||
      label === "terrorismEvents(1975-2015)";
    const isIconType = label === "airports";
    const isLineType =
      label === "oilGasPipelines" ||
      label === "historicalCyclones" ||
      label === "historicalHailstormsUSA";
    const isNormalLayer = !isCircleType && !isIconType && !isIconType;

    const newBigQueryLayer = new CartoLayer({
      id: label,
      pickable: true,
      type: isTileSet ? MAP_TYPES.TILESET : MAP_TYPES.TABLE,
      connection: "bq-layers",
      data: lookupSourceTileSet || lookupSource,
      pointRadiusUnits: "pixels",
      lineWidthUnits: "pixels",
      onClick: (pickingInfoEvent) => {
        this.handleInfoWindow(layer, pickingInfoEvent);
      },
      getLineWidth: 0,
      ...(!isTileSet && { geoColumn: "the_geom" }),
      ...(isCircleType && {
        pointType: "circle",
        getLineWidth: 1,
        getLineColor: WhiteColorWithFullOpacity,
        getFillColor: ({
          properties,
        }: GeoJSON.Feature<null, BigQueryCircleTypeColumns>) => {
          if (label === "earthquakeEuropeMidEast" && "mw" in properties) {
            const numMw = Number(properties.mw);
            if (numMw <= 1) {
              return addCustomLayerOpacity([255, 255, 204]);
            }
            if (numMw <= 2) {
              return addCustomLayerOpacity([255, 237, 160]);
            }
            if (numMw <= 3) {
              return addCustomLayerOpacity([254, 217, 118]);
            }
            if (numMw <= 4) {
              return addCustomLayerOpacity([254, 178, 76]);
            }
            if (numMw <= 5) {
              return addCustomLayerOpacity([253, 141, 60]);
            }
            if (numMw <= 6) {
              return addCustomLayerOpacity([252, 78, 42]);
            }
            if (numMw <= 7) {
              return addCustomLayerOpacity([227, 26, 28]);
            }
            if (numMw <= 8) {
              return addCustomLayerOpacity([189, 0, 38]);
            }
            if (numMw <= 9) {
              return addCustomLayerOpacity([128, 0, 38]);
            }
            return addCustomLayerOpacity([255, 255, 255]);
          }
          if (label === "terrorismEvents(1975-2015)") {
            return addCustomLayerOpacity([180, 9, 3]);
          }
          if (label === "dams") {
            return addCustomLayerOpacity([62, 123, 182]);
          }
          if (label === "powerPlants") {
            return addCustomLayerOpacity([180, 9, 3]);
          }
          if (label === "ports") {
            return addCustomLayerOpacity([255, 102, 0]);
          }
          if (label === "urbanAreas") {
            return addCustomLayerOpacity([107, 15, 178]);
          }
          if (label === "tsunamiEvents(2000BC-2015)") {
            return addCustomLayerOpacity([33, 103, 171]);
          }
          if (label === "meteoriteStrikes") {
            return [255, 92, 0, 150];
          }
          return WhiteColorWithCustomLayerOpacity;
        },
        getPointRadius: ({
          properties,
        }: {
          properties: BigQueryCircleTypeColumns;
        }) => {
          if (label === "terrorismEvents(1975-2015)") {
            return 5.5;
          }
          if (
            label === "dams" ||
            label === "powerPlants" ||
            label === "ports"
          ) {
            return 10;
          }
          if (
            label === "tsunamiEvents(2000BC-2015)" &&
            "primary_magnitude" in properties
          ) {
            const { primary_magnitude: primaryMagnitude } = properties;
            if (primaryMagnitude <= 4.5) {
              return 1.0;
            }
            if (primaryMagnitude <= 5.1) {
              return 2.0;
            }
            if (primaryMagnitude <= 5.6) {
              return 3.0;
            }
            if (primaryMagnitude <= 6.1) {
              return 4.0;
            }
            if (primaryMagnitude <= 6.65) {
              return 5.0;
            }
            if (primaryMagnitude <= 7.2) {
              return 6.0;
            }
            if (primaryMagnitude <= 7.7) {
              return 7.0;
            }
            if (primaryMagnitude <= 8.2) {
              return 8.0;
            }
            if (primaryMagnitude <= 8.7) {
              return 9.0;
            }
            if (primaryMagnitude <= 9.5) {
              return 10.0;
            }
          }
          if (label === "urbanAreas" && "pop2015" in properties) {
            const { pop2015 } = properties;
            if (pop2015 <= 4275) {
              return 7;
            }
            if (pop2015 <= 7810) {
              return 9;
            }
            if (pop2015 <= 11345) {
              return 11;
            }
            if (pop2015 <= 14880) {
              return 13;
            }
            if (pop2015 <= 18415) {
              return 15;
            }
            if (pop2015 <= 21950) {
              return 17;
            }
            if (pop2015 <= 25485) {
              return 19;
            }
            if (pop2015 <= 29020) {
              return 21;
            }
            if (pop2015 <= 32555) {
              return 23;
            }
            if (pop2015 <= 36090) {
              return 25;
            }
          }
          if (label === "meteoriteStrikes" && "mass_g" in properties) {
            const { mass_g: massG } = properties;
            if (massG <= 10.6) {
              return 10 / METEORITE_RATIO_DIVIDER;
            }
            if (massG <= 25.16) {
              return 11.7 / METEORITE_RATIO_DIVIDER;
            }
            if (massG <= 48.6) {
              return 13.3 / METEORITE_RATIO_DIVIDER;
            }
            if (massG <= 86.1) {
              return 15 / METEORITE_RATIO_DIVIDER;
            }
            if (massG <= 149.2) {
              return 16.7 / METEORITE_RATIO_DIVIDER;
            }
            if (massG <= 267.1) {
              return 18.3 / METEORITE_RATIO_DIVIDER;
            }
            if (massG <= 518.5) {
              return 20.0 / METEORITE_RATIO_DIVIDER;
            }
            if (massG <= 1198.7) {
              return 21.7 / METEORITE_RATIO_DIVIDER;
            }
            if (massG <= 4416) {
              return 23.3 / METEORITE_RATIO_DIVIDER;
            }
            if (massG <= 60000000) {
              return 25 / METEORITE_RATIO_DIVIDER;
            }
          }
          if (label === "earthquakeEuropeMidEast" && "mw" in properties) {
            const numMw = Number(properties.mw);
            if (numMw <= 1) {
              return 1;
            }
            if (numMw <= 2) {
              return 2;
            }
            if (numMw <= 3) {
              return 3;
            }
            if (numMw <= 4) {
              return 4;
            }
            if (numMw <= 5) {
              return 5;
            }
            if (numMw <= 6) {
              return 6;
            }
            if (numMw <= 7) {
              return 7;
            }
            if (numMw <= 8) {
              return 8;
            }
            if (numMw <= 9) {
              return 9;
            }
            return 0;
          }
          return 1;
        },
      }),
      ...(isLineType && {
        pointType: "circle",
        getLineWidth: ({
          properties,
        }: GeoJSON.Feature<null, BigQueryLineTypeColumns>) => {
          if ("wind" in properties) {
            return 1;
          }
          return 2;
        },
        getLineColor: ({
          properties,
        }: GeoJSON.Feature<null, BigQueryLineTypeColumns>) => {
          if ("kind" in properties) {
            if (properties.kind === "oil") {
              return addCustomLayerOpacity([0, 0, 0]);
            }
            if (properties.kind === "natural_gas") {
              return addCustomLayerOpacity([255, 41, 0]);
            }
          }
          if ("wind" in properties) {
            if (properties.wind <= 12.1285714285714) {
              return addCustomLayerOpacity([255, 255, 178]);
            }
            if (properties.wind <= 24.2571428571428) {
              return addCustomLayerOpacity([254, 217, 118]);
            }
            if (properties.wind <= 36.3857142857142) {
              return addCustomLayerOpacity([254, 178, 76]);
            }
            if (properties.wind <= 48.5142857142856) {
              return addCustomLayerOpacity([253, 141, 60]);
            }
            if (properties.wind <= 60.642857142857) {
              return addCustomLayerOpacity([252, 78, 42]);
            }
            if (properties.wind <= 72.7714285714284) {
              return addCustomLayerOpacity([227, 26, 28]);
            }
            if (properties.wind <= 84.9) {
              return addCustomLayerOpacity([177, 0, 38]);
            }
          }
          if ("size" in properties) {
            return [33, 103, 171, 180];
          }
          return addCustomLayerOpacity([0, 0, 0]);
        },
      }),
      ...(isIconType && {
        pointType: "icon",
        getIconSize: 16,
        sizeUnits: "pixels",
        getIcon: ({
          properties: { type },
        }: GeoJSON.Feature<null, BigQueryIconTypeColumns>) => {
          const fileName = (() => {
            if (type === "closed") {
              return "closed.png";
            }
            if (type === "heliport") {
              return "heliport.png";
            }
            if (type === "balloonport") {
              return "balloonport.png";
            }
            if (type === "large_airport") {
              return "large_airport.png";
            }
            if (type === "seaplane_base") {
              return "seaplane_base.png";
            }
            if (type === "small_airport") {
              return "small_airport.png";
            }
            if (type === "medium_airport") {
              return "medium_airport.png";
            }
            return "heliport.png";
          })();
          return {
            url: `/assets/images/icons/airports/${fileName}`,
            height: 16,
            width: 16,
          };
        },
      }),
      ...(isNormalLayer && {
        ...(label === "historicalFloodRegions" && {
          getLineColor: WhiteColorWithFullOpacity,
          getLineWidth: 0.5,
        }),
        ...(label === "natura2000" && {
          getLineColor: [0, 0, 255, 204],
          getLineWidth: 0.5,
        }),
        getFillColor: (
          feature: GeoJSON.Feature<Geometry, BigQueryNormalTypeColumns>
        ) => {
          if (label === "historicalFloodRegions") {
            return [62, 123, 182, 80];
          }
          if (label === "natura2000") {
            return [0, 0, 255, 200];
          }
          const { properties } = feature;
          if (label === "zeus2017(MedgustsAON)" && "max_gust" in properties) {
            const { max_gust: maxGust } = properties;
            if (maxGust <= 9.4) {
              return addCustomLayerOpacity([255, 255, 204]);
            }
            if (maxGust <= 15.7) {
              return addCustomLayerOpacity([199, 233, 180]);
            }
            if (maxGust <= 22) {
              return addCustomLayerOpacity([127, 205, 187]);
            }
            if (maxGust <= 28.3) {
              return addCustomLayerOpacity([65, 182, 196]);
            }
            if (maxGust <= 34.6) {
              return addCustomLayerOpacity([29, 145, 192]);
            }
            if (maxGust <= 41.1) {
              return addCustomLayerOpacity([34, 94, 168]);
            }
            if (maxGust <= 54.6) {
              return addCustomLayerOpacity([12, 44, 132]);
            }
            return addCustomLayerOpacity([255, 255, 204]);
          }
          if (label === "europeEarthquake(EFEHR)" && "hpvalue" in properties) {
            if (properties.hpvalue <= 0.0063760627) {
              return addCustomLayerOpacity([255, 255, 178]);
            }
            if (properties.hpvalue <= 0.0116276848) {
              return addCustomLayerOpacity([254, 217, 118]);
            }
            if (properties.hpvalue <= 0.0224213916) {
              return addCustomLayerOpacity([254, 178, 76]);
            }
            if (properties.hpvalue <= 0.0439071796) {
              return addCustomLayerOpacity([253, 141, 60]);
            }
            if (properties.hpvalue <= 0.0974637082) {
              return addCustomLayerOpacity([252, 78, 42]);
            }
            if (properties.hpvalue <= 0.2144764231) {
              return addCustomLayerOpacity([227, 26, 28]);
            }
            if (properties.hpvalue <= 0.6929666625) {
              return addCustomLayerOpacity([177, 0, 38]);
            }
            return addCustomLayerOpacity([255, 255, 178]);
          }

          if (label === "CEELandslides(RELIA)" && "relia" in properties) {
            if (properties.relia <= 1) {
              return addCustomLayerOpacity([26, 152, 80]);
            }
            if (properties.relia <= 2) {
              return addCustomLayerOpacity([140, 206, 138]);
            }
            // commented lines are duplicates in old styles
            // if (properties.relia <= 2) {
            //   return addCustomLayerOpacity([210, 236, 180])
            // }
            if (properties.relia <= 3) {
              return addCustomLayerOpacity([255, 242, 204]);
            }
            // if (properties.relia <= 3) {
            //   return addCustomLayerOpacity([254, 214, 176])
            // }
            if (properties.relia <= 99) {
              return addCustomLayerOpacity([247, 146, 114]);
            }
            // if (properties.relia <= 99) {
            //   return addCustomLayerOpacity([215, 48, 39])
            // }
            return addCustomLayerOpacity([26, 152, 80]);
          }

          if (label === "CEELandslides(SUS)" && "sus" in properties) {
            if (properties.sus <= 2) {
              return addCustomLayerOpacity([255, 255, 178]);
            }
            // commented lines are duplicates in old styles
            // if (properties.sus <= 2) {
            //   return addCustomLayerOpacity([254, 217, 118])
            // }
            if (properties.sus <= 3) {
              return addCustomLayerOpacity([254, 178, 76]);
            }
            // if (properties.sus <= 3) {
            //   return addCustomLayerOpacity([253, 141, 60])
            // }
            if (properties.sus <= 4) {
              return addCustomLayerOpacity([252, 78, 42]);
            }
            // if (properties.sus <= 4) {
            //   return addCustomLayerOpacity([227, 26, 28])
            // }
            if (properties.sus <= 5) {
              return addCustomLayerOpacity([177, 0, 38]);
            }
            return addCustomLayerOpacity([255, 255, 178]);
          }

          return getLayerFeatureColor({ layer, feature });
        },
      }),
      // https://deck.gl/docs/developer-guide/performance#use-updatetriggers
      updateTriggers: {},
    });

    return newBigQueryLayer;
  }

  private prepareLayerForCatnet(layer: Layer) {
    if (!layer.tilesSource || !layer.overviewSource) {
      console.error(
        `Missing tiles or overview source for layer ${layer.label}`
      );
      return;
    }
    if (
      !environment.gmaps_settings.catnet.CATNET_BASE_API_URL ||
      !environment.gmaps_settings.catnet.CATNET_SUBSCRIPTION_KEY
    ) {
      console.error(
        "CATNET_BASE_API_URL or CATNET_SUBSCRIPTION_KEY is missing from environment"
      );
      return;
    }

    const newCatnetLayer = new TileLayer({
      id: layer.label,
      minZoom: 2,
      maxZoom: 18,
      // Since these OSM tiles support HTTP/2, we can make many concurrent requests
      // and we aren't limited by the browser to a certain number per domain.
      maxRequests: 20,
      tileSize: 256,
      opacity: 0.8,
      renderSubLayers: (props) => {
        // typeguard
        if ("west" in props.tile.bbox) {
          const { west, south, east, north } = props.tile.bbox;

          // https://stackoverflow.com/q/77267050/8160318
          return new BitmapLayer(props as any, {
            image: URL.createObjectURL(props.data),
            transparentColor: [255, 255, 255, 0],
            bounds: [west, south, east, north],
          });
        }
        return null;
      },
      getTileData: async ({ bbox, signal }) => {
        // typeguard
        if ("west" in bbox) {
          const { west, south, north, east } = bbox;

          const params = [
            "?Request=GetMap",
            "&Service=WMS",
            "&Version=1.3.0",
            "&crs=EPSG:4326",
            "&width=256",
            "&height=256",
            "&dpi=300",
            "&format=image/png",
            "&transparent=TRUE",
            `&BBOX=${[west, south, east, north].join(",")}`,
          ];
          const zoom = this.map.getZoom() || 10;
          const tilesSourceOrOverviewSource =
            zoom < 9 ? layer.tilesSource! : layer.overviewSource!;

          const targetUrl = [
            environment.gmaps_settings.catnet.CATNET_BASE_API_URL,
            tilesSourceOrOverviewSource,
            "/httpauth",
            params.join(""),
          ].join("");

          const blob = await fetchProxiedImageBlob(`${targetUrl}`, {
            signal,
          });

          if (signal?.aborted || !blob) {
            return null;
          }

          return blob;
        }
        return null;
      },
    });

    return newCatnetLayer;
  }

  private prepareLayerForGdacs(layer: Layer) {
    // convert back to keys that GEAM can process via graphql
    const LookupSourceMapping = {
      gdacsFlood: "flood",
      gdacsEarthquakes: "earthquakes",
      gdacsTropicalCyclones: "tropicalCyclones",
      gdacsVolcanicEruptions: "volcanicEruptions",
      gdacsRain: "",
      gdacsClouds: "",
    };

    const { label } = layer;

    const geamSource =
      LookupSourceMapping[label as keyof typeof LookupSourceMapping];
    if (!geamSource) {
      throw new Error("Supplied layer is not supported.");
    }

    const fetchData = async () => {
      const response = await fetch(
        `${environment.gmaps_settings.GEAM_PROXY_URL_BASE}/api/graphql`,
        {
          method: "POST",
          body: JSON.stringify({
            operationName: "GdacsLayerByLabel",
            variables: { input: { label: geamSource } },
            query:
              "query GdacsLayerByLabel($input: GdacsLayerByLabelInput!) {\n  gdacsLayerByLabel(input: $input)\n}",
          }),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      const json = await response.json();
      const data = json.data.gdacsLayerByLabel;
      return JSON.parse(data);
    };

    const newGdacsLayer = new GeoJsonLayer({
      id: layer.label,
      pickable: true,
      data: fetchData(),
      pointRadiusMinPixels: 1,
      lineWidthMinPixels: 1,
      onClick: (pickingInfoEvent) => {
        this.handleInfoWindow(layer, pickingInfoEvent);
      },
      getFillColor: (feature: GeoJSON.Feature) =>
        getLayerFeatureColor({ layer, feature }),
    });

    return newGdacsLayer;
  }

  private prepareLayerForColumbiaWms(layer: Layer) {
    if (!layer.tilesSource) {
      console.error(`Missing tiles source for layer ${layer.label}`);
      return;
    }

    if (!environment.gmaps_settings.COLUMBIA_WMS_BASE_URL) {
      console.error("COLUMBIA_WMS_BASE_URL is missing from environment");
      return;
    }

    const newColumbiaWmsLayer = new TileLayer({
      id: layer.label,
      minZoom: 2,
      maxZoom: 18,
      maxRequests: 20,
      tileSize: 256,
      opacity: 0.8,
      getTileData: async ({ bbox }) => {
        // typeguard
        if ("west" in bbox) {
          const { west, north, east, south } = bbox;

          // re-project gmaps bounds to "EPSG:3857" so PNGs are correctly projected
          const [w, s] = proj4("WGS84", "EPSG:3857", [west, south]);
          const [e, n] = proj4("WGS84", "EPSG:3857", [east, north]);

          const params = [
            `?LAYERS=${layer.tilesSource!}`,
            "&SERVICE=WMS",
            "&REQUEST=GetMap",
            "&VERSION=1.3.0",
            "&FORMAT=image/png",
            "&TRANSPARENT=TRUE",
            "&SRS=EPSG:3857",
            "&WIDTH=256",
            "&HEIGHT=256",
            `&BBOX=${[w, s, e, n].join(",")}`,
          ];

          const blob = await fetchProxiedImageBlob(
            `${environment.gmaps_settings.COLUMBIA_WMS_BASE_URL!}${params.join(
              ""
            )}`
          );

          if (!blob) {
            return null;
          }

          return blob;
        }
        return null;
      },
      renderSubLayers: (props) => {
        // typeguard
        if ("west" in props.tile.bbox) {
          const { west, south, east, north } = props.tile.bbox;

          // https://stackoverflow.com/q/77267050/8160318
          return new BitmapLayer(props as any, {
            image: URL.createObjectURL(props.data),
            transparentColor: [255, 255, 255, 0],
            bounds: [west, south, east, north],
          });
        }
        return null;
      },
    });

    return newColumbiaWmsLayer;
  }

  private prepareCustomJSONLayer(layer: Layer) {
    const newGeoJSONLayer = new GeoJsonLayer({
      id: layer.label,
      data: layer.geoJSON,
      dataTransform: (data): any => {
        switch (layer.perilSource) {
          case PerilSource.Ukraine: {
            return processLiveUaMap(data as any);
          }
          default:
            console.error("Parsing not implemented!", { layer, data });
        }

        return [];
      },

      ...(() => {
        switch (layer.perilSource) {
          case PerilSource.Ukraine:
            return prepareLiveUaMapDeckglProps((pi: PickingInfo) =>
              this.handleInfoWindow(layer, pi)
            );
          default:
            // noop
            return {};
        }
      })(),
    });

    return newGeoJSONLayer;
  }

  /**
   * Decide based on layer type/source how to prepare Deck.
   * @param layer
   * @returns
   */
  private generateLayer(layer: Layer): DisplayedDeckLayer {
    const { perilSource } = layer;

    switch (perilSource) {
      case PerilSource.BQ:
        return this.prepareLayerForBigQuery(layer);
      case PerilSource.Catnet:
        return this.prepareLayerForCatnet(layer);
      case PerilSource.Gdacs:
        return this.prepareLayerForGdacs(layer);
      case PerilSource.ColumbiaWms:
        return this.prepareLayerForColumbiaWms(layer);
      case PerilSource.Ukraine:
        return this.prepareCustomJSONLayer(layer);
    }
  }

  /***
   * Iterates all available layers & creates the corresponding Decks.
   * Choosing this approach of re-generating layers each time because
   * it's less time consuming than managing Deck state on the service level.
   * That said, gotta regenerate them because Deck cannot reinstantiate already
   * created layers...
   */
  generateLayers(layers: Layer[]) {
    const preparedLayers = layers.map((layer) => this.generateLayer(layer));
    this.setDeckLayers(...preparedLayers);
    this.selectedLayersLabels$.next(layers.slice().map((l) => l.label));
  }

  /*
  PRESENTATION
  */

  private getfullLayers() {
    return this.selectedLayersLabels$
      .getValue()
      .map((label) => findLayerByLabel(label));
  }

  /**
   * For use in HTML of `legends.component.ts`
   * @returns
   */
  public getLayerColorCards() {
    const naturalHazardAndManualLayersLegends = R.uniqBy(
      ({ colorScheme }) => colorScheme,
      this.getfullLayers()
        ?.filter(
          (layer) =>
            layer.isManual === true ||
            (layer.subPeril &&
              layer.subPeril in LayerColorScheme &&
              !(layer.label in CustomLayerColorScheme) &&
              layer.perilSource !== PerilSource.Catnet)
        )
        .map((layer) => ({
          label: layer.label,
          colorScheme: layer.isManual ? "ManualLayer" : layer.subPeril!,
        }))
    );

    const customLayerKeys = Object.keys(CustomLayerColorScheme);
    const customLayerLegends = this.getfullLayers()
      .filter((layer) => customLayerKeys.includes(layer.label))
      .map(({ label }) => label);

    return {
      naturalHazardAndManualLayersLegends,
      customLayerLegends,
    };
  }

  private prepareCatnetPngLegendUrl(serviceName: string) {
    if (
      !environment.gmaps_settings.catnet.CATNET_SUBSCRIPTION_KEY ||
      !environment.gmaps_settings.catnet.CATNET_BASE_API_URL
    ) {
      console.error(
        "CATNET_SUBSCRIPTION_KEY or CATNET_BASE_API_URL is missing from environment"
      );
      return null;
    }
    const params = [
      "httpauth",
      "?Request=GetLegendGraphic",
      "&Service=WMS",
      "&version=1.3.0",
      "&format=image/png",
    ].join("");

    return `${environment.gmaps_settings.catnet.CATNET_BASE_API_URL}${serviceName}/${params}`;
  }

  private async getCatnetPngLegendByBlobSrc(pngUrl: string) {
    const blob = await fetchProxiedImageBlob(pngUrl);
    return URL.createObjectURL(blob);
  }

  /**
   * Triggered asynchronously from `legends.component`
   * to repopulate `catnetPngBlobs$`
   */
  async getAllCatnetPngs() {
    const catnetLayers = this.getfullLayers().filter(
      (l) => l.perilSource === PerilSource.Catnet
    );
    const catnetPngBlobs = [];

    for (const layer of catnetLayers) {
      const srcUrl = this.prepareCatnetPngLegendUrl(layer.tilesSource);
      const blob = await this.getCatnetPngLegendByBlobSrc(srcUrl);

      catnetPngBlobs.push({
        label: layer.label,
        src: blob,
      });
    }

    this.catnetPngBlobs$.next(catnetPngBlobs);
  }

  private prepareBQlayerInfoWindowContent(pickingInfoEvent: PickingInfo) {
    const object = pickingInfoEvent.object as {
      properties: { [key: string]: string };
    };

    const sortedEntries = Object.entries(object.properties).sort(
      ([, valueA], [, valueB]) => {
        if (valueA === "null" && valueB !== "null") {
          return 1;
        }
        if (valueA !== "null" && valueB === "null") {
          return -1;
        }
        return 0;
      }
    );

    return json2table(Object.fromEntries(sortedEntries));
  }

  private prepareGenericInfoWindowContent(pickingInfoEvent: PickingInfo) {
    const object = pickingInfoEvent.object as {
      properties: { [key: string]: string };
    };

    return json2table(object.properties);
  }

  private handleInfoWindow(layer: Layer, pickingInfoEvent: PickingInfo) {
    if (!pickingInfoEvent) {
      return;
    }

    if (!pickingInfoEvent.coordinate) {
      console.error("No coordinate found in BQ layer picking info", {
        pickingInfoEvent,
        layer,
      });
      return null;
    }

    let infowindowContent = "";
    switch (layer.perilSource) {
      case PerilSource.BQ:
        infowindowContent =
          this.prepareBQlayerInfoWindowContent(pickingInfoEvent);
        break;
      case PerilSource.Ukraine:
        infowindowContent = this.prepareGenericInfoWindowContent(
          (() => {
            const originalPickingInfoEvt = pickingInfoEvent;
            // only keep the relevant properties
            originalPickingInfoEvt.object.properties =
              filterObjectPropertiesByKeyRegexes(
                originalPickingInfoEvt.object.properties,
                [/(datetime|location|message|link)/]
              );
            return originalPickingInfoEvt;
          })()
        );
        break;
      default:
        infowindowContent =
          this.prepareGenericInfoWindowContent(pickingInfoEvent);
        break;
    }

    if (!infowindowContent?.length) {
      return;
    }

    this.infowindow.setContent(`
      <div style='width:340px; text-align: left;'>
      ${infowindowContent}
      </div>
      `);

    const [lng, lat] = pickingInfoEvent.coordinate;

    this.infowindow.setOptions({
      position: {
        lat,
        lng,
      },
      pixelOffset: new google.maps.Size(0, -12),
    });

    this.infowindow.open(this.map);
  }
}
