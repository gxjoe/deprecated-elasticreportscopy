import { HttpClient } from "@angular/common/http"
import { Injectable } from "@angular/core"
import { environment } from '../../environments/environment'
import { CURRENCY_PREFIX_ISO, uniq } from "src/app/utils/helpers/helpers"
import { map, of, tap } from "rxjs"
import {LISTS_OF_HISTOGRAM_CATEGORIES} from "../charts/histogram-table-chart/histogramListsAndCategories";

type Mapping = Record<string, any>

@Injectable({
  providedIn: 'root'
})
export class ElasticMappingService {

  private host = environment.elasticsearch.host

  private mapping: Mapping
  private allProps: string[]

  constructor(
    private http: HttpClient,
  ) {
  }

  fetchMapping() {
    return this.mapping && of(this.mapping) || this.http.get(`${this.host}/prod_data/_mapping`)
      .pipe(
        map((value: { prod_data: { mappings: { entry: Mapping } } }) => value.prod_data.mappings.entry)
      )
  }

  getMappingProps(maxLevel: number) {
    return this.fetchMapping()
      .pipe(
        map((mapping) => this.listProps(mapping, null)),
        tap((props) => this.allProps = props),
        tap(console.log),
        map((props) => uniq(this.filterPropsBelowLevel(props, maxLevel))),
        tap(console.log),
      )
  }

  getChildrenFromAsteriskProp(asteriskProp: string) {
    return this.allProps.filter((prop) => !prop.includes('*') && prop.startsWith(asteriskProp.slice(0, asteriskProp.length - 2)))
  }

  private filterPropsBelowLevel(props: string[], level: number) {

    return props.reduce((acc, curr) => {
      const parts = curr.split(".")
      if (parts.length > level) {
        return [...acc, `${parts.slice(0, level).join(".")}.*`]
      }

      return [...acc, curr]
    }, [])
  }

  private listProps(mapping: Mapping, parentKey: string | null): string[] {
    let propertiesList: string[] = []

    const currency = CURRENCY_PREFIX_ISO()

    if ('properties' in mapping) {
      const keys = Object.keys(mapping.properties)
        .filter((key) => !key.includes('responseTimes'))
        .filter((key) => {
          const parts = key.split("_")
          const curr = parts.find((p) => LISTS_OF_HISTOGRAM_CATEGORIES.listOfCurrencies.includes(p))
          return !curr || curr === currency
        })
      const mappedKeys = keys.map((key) => parentKey ? `${parentKey}.${key}` : key)


      return [...mappedKeys.filter((key) => key !== 'lob' && key !== 'offer'), ...keys.flatMap((key, idx) => this.listProps(mapping.properties[key], mappedKeys[idx]))]
    }

    return propertiesList
  }
}