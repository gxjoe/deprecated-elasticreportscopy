/// <reference types="@types/googlemaps" />

import { Injectable } from '@angular/core';
import {
  buildQueries,
  VISUALIZATION_TYPES,
  PRECISION_RATIOS,
  ES_CLUSTERING_PRECISION,
  DATE_FORMATS,
  HEATMAP_VS_GEO_FIELD_SOURCES,
  DATEPICKERS_TO_FIELDS,
} from '../elastic/queries';
import {
  googleMapsBoundsToESBounds,
  mutateObjectProperty,
  constructNestedGeoQuery,
  findPath,
  deleteByPath,
  constructNestedCustomBoundaryQuery,
} from '../elastic/utils';
import { AppComponent } from '../app.component';
import { RelativeDatepickerComponent } from '../relative-datepicker/relative-datepicker.component';
import {
  ElasticFilterSubtypes,
  ElasticQueryClauses,
} from '../elastic/constants';
import IFilterComponentQueryMeta from '../utils/higher-order/IFilterComponentQueryMeta';
import * as objectPath from 'object-path';
import * as Immutable from 'immutable';
import { everyIsNull } from '../utils/helpers/helpers';
import { ImapDrawing } from '../interfaces/ImapDrawing';
import {
  boolQuery,
  existsQuery,
  nestedQuery,
  requestBodySearch,
  termsQuery,
} from 'elastic-builder';
import { LOGICAL_FILTER_MULTISELECT_BEHAVIOR } from '../multiselect/enums';

const extractNestedPath = (elastic_variable: string) => {
  // extract the path, e.g.:
  //  'offer.crossBorderBusiness'
  //   ^^^^^
  // and
  //  'lob.localUnderwriters.fullName'
  //   ^^^^^^^^^^^^^^^^^^^^^
  const p = elastic_variable.replace('.keyword', '').split('.');
  p.splice(-1, 1);
  return p.join('.');
};

@Injectable({
  providedIn: 'root',
})
export class QueryService {
  private query_name;
  private map: google.maps.Map;
  private gmap_klass: string;
  private app_: any;

  construct_filter_subquery(queryMeta: IFilterComponentQueryMeta) {
    if (!queryMeta.values.length) {
      return null;
    }

    // get rid of empty strings and such
    queryMeta.values = queryMeta.values.filter((val: any) => `${val}`.length);

    if (!queryMeta?.values?.length) {
      return null;
    }

    let base;
    let list_of_values = [];

    if (
      !queryMeta.clause ||
      queryMeta.clause === ElasticQueryClauses.filter ||
      queryMeta.operators
    ) {
      switch (queryMeta.subtype) {
        case ElasticFilterSubtypes.range:
          base = {};
          base[queryMeta.subtype] = {};

          objectPath.set(
            base,
            `${queryMeta.subtype}.${queryMeta.elastic_variable}`,
            {}
          );

          queryMeta.values.map((value, index) => {
            // value might be null --> don't assign value
            if (value) {
              list_of_values.push(value);
              objectPath.set(
                base,
                `${queryMeta.subtype}.${queryMeta.elastic_variable}.${queryMeta.operators[index]}`,
                value
              );
            }
          });
          break;
      }
    } else {
      switch (queryMeta.clause) {
        case ElasticQueryClauses.must: {
          const rbs = requestBodySearch();
          const boolQ = boolQuery();

          const { elastic_variable, values, nested, klass } = queryMeta;

          const termsQ = termsQuery(elastic_variable, values);
          const path = extractNestedPath(elastic_variable);

          list_of_values = values.slice();

          if (nested) {
            boolQ.must(nestedQuery().path(path).query(termsQ));
          } else {
            boolQ.must(termsQ);
          }

          const relatedLogicalFilter = (window as any)[
            `${klass}_LogicalFilter`
          ];
          if (relatedLogicalFilter?.length) {
            // UDGB-48
            switch (relatedLogicalFilter) {
              case LOGICAL_FILTER_MULTISELECT_BEHAVIOR.INCLUDE:
              default:
                rbs.query(boolQ);
                break;
              case LOGICAL_FILTER_MULTISELECT_BEHAVIOR.EXCLUDE:
                rbs.query(boolQuery().mustNot(boolQ));
                break;
              case LOGICAL_FILTER_MULTISELECT_BEHAVIOR.EXCLUDE_IF_EXISTS: {
                if (nested) {
                  rbs.query(
                    boolQuery().must([
                      nestedQuery()
                        .path(path)
                        .query(existsQuery(elastic_variable)),
                      boolQuery().mustNot(boolQ),
                    ])
                  );
                } else {
                  rbs.query(
                    boolQuery().must([
                      existsQuery(elastic_variable),
                      boolQuery().mustNot(boolQ),
                    ])
                  );
                }
              }
            }
          } else {
            rbs.query(boolQ);
          }

          base = rbs.toJSON()['query'];
          break;
        }
      }
    }

    if (everyIsNull(list_of_values)) {
      return null;
    }
    return base;
  }

  apply_date_subqueries(app_component: AppComponent, mutation_target) {
    Object.keys(DATEPICKERS_TO_FIELDS).map((klass) => {
      const _component = app_component.getComponentByKlass(
        klass
      ) as RelativeDatepickerComponent;
      const _tsValues = _component.tsValues;
      const _datepickerParams = DATEPICKERS_TO_FIELDS[klass];

      if (_component) {
        if (_tsValues.start || _tsValues.end) {
          if (!_datepickerParams.field) {
            Object.keys(_datepickerParams).map((dparamKey) => {
              const params = _datepickerParams[dparamKey];
              const value =
                dparamKey === 'gte' ? _tsValues.start : _tsValues.end;
              mutateObjectProperty(
                params.field,
                {
                  format: DATE_FORMATS.ES_DATE_HUMAN_READABLE,
                  [dparamKey]: value,
                },
                mutation_target
              );
            });
          } else {
            mutateObjectProperty(
              _datepickerParams.field,
              {
                format: DATE_FORMATS.ES_DATE_HUMAN_READABLE,
                gte: _tsValues.start,
                lte: _tsValues.end,
              },
              mutation_target
            );
          }
        } else {
          if (_datepickerParams.accessor) {
            let path = findPath(_datepickerParams.accessor, mutation_target);
            if (path) {
              if (!_datepickerParams.nested) {
                path = path.split('.range')[0];
              } else {
                path = path.split('.nested')[0];
              }
              deleteByPath(mutation_target, path);
            }
          } else {
            Object.keys(_datepickerParams).map((dkey) => {
              const params = _datepickerParams[dkey];
              let path = findPath(params.accessor, mutation_target);
              if (path) {
                if (!params.nested) {
                  path = path.split('.range')[0];
                } else {
                  path = path.split('.nested')[0];
                }
                deleteByPath(mutation_target, path);
              }
            });
          }
        }
      }
    });
  }

  construct(
    query_name: string,
    map_component: any,
    app: AppComponent,
    disregard_map?: Boolean,
    filter_meta_queries?: Array<IFilterComponentQueryMeta>,
    query_itself_?: any,
    customBoundary?: ImapDrawing
  ) {
    if (query_name.toLowerCase().includes('map')) {
      disregard_map = false;
    }

    const is_msearch = query_name === 'AllOffers';
    let query_itself;
    console.info(query_itself_);

    try {
      query_itself = Immutable.fromJS(query_itself_).toJSON();
    } catch (err) {
      query_itself = Immutable.fromJS(
        buildQueries(query_name, app)[query_name]
      ).toJSON();
    }

    if (is_msearch) {
      query_itself = query_itself.m_search_query_lines[0].query;
    }

    if (!disregard_map) {
      if (!map_component || !map_component.map) {
        try {
          map_component =
            app.OfferDistributionHeatmapComponent.es.activeMapComponent;
        } catch (exception) {
          console.error('qi', query_name);
          console.warn(exception);
          disregard_map = true;
        }
      }
    }

    this.query_name = query_name;

    if (!disregard_map) {
      this.map = map_component.map;
      this.gmap_klass = map_component.klass;
    }

    this.app_ = app;

    let is_query_function = false;
    let query, nested_query, geo_path, geo_field;

    if ('fn' in query_itself) {
      query = query_itself.base; // placeholder
      is_query_function = true;
    } else {
      query = Object.assign({}, query_itself);
    }

    if (!this.gmap_klass) {
      disregard_map = true;
    }

    if (!disregard_map) {
      const { path, field } = HEATMAP_VS_GEO_FIELD_SOURCES[this.gmap_klass];

      if (!customBoundary) {
        nested_query = constructNestedGeoQuery(path, field);
      }

      geo_field = field;
      geo_path = path;
    }

    if ((!disregard_map || customBoundary) && geo_path && geo_field) {
      if (
        !this.map ||
        !this.map.getBounds() ||
        this.map.getBounds().getNorthEast().lat() ===
        this.map.getBounds().getSouthWest().lat()
      ) {
        return {};
      }

      if (!query.query) {
        query.query = {
          bool: {
            filter: [],
          },
        };
      }

      let precision = this.map.getZoom() - PRECISION_RATIOS[this.gmap_klass];
      if (precision < ES_CLUSTERING_PRECISION.min) {
        precision = ES_CLUSTERING_PRECISION.min + 1;
      }

      if (precision > ES_CLUSTERING_PRECISION.max) {
        precision = ES_CLUSTERING_PRECISION.max - 1;
      }

      if (!customBoundary) {
        const q = Object.assign({}, nested_query);

        q.nested.query.geo_bounding_box[geo_field] = googleMapsBoundsToESBounds(
          this.map.getBounds()
        ).coordinates;

        // setObjectPropertyByPath('aggs.filter_agg.filter', q, query);
        query.query.bool.filter = query.query.bool.filter.concat(q);

        mutateObjectProperty('precision', precision, query);
        mutateObjectProperty(
          'geo_bounding_box',
          q.nested.query.geo_bounding_box,
          query
        );
      } else {
        const custom_boundary_nested_query = constructNestedCustomBoundaryQuery(
          customBoundary,
          geo_path,
          geo_field
        );

        query.query.bool.filter = query.query.bool.filter.concat(
          custom_boundary_nested_query
        );

        mutateObjectProperty('precision', precision, query);

        // replace filter with empty clause because the geo filter is already under main query
        objectPath.set(
          query,
          'aggs.filter_agg.aggs.aggs.filter',
          custom_boundary_nested_query
        );
      }
      // query.aggs.filter_agg.filter = q;
    } else {
      if (!query.query) {
        query['query'] = { bool: { filter: [] } };
      }
    }

    if (is_msearch) {
      this.apply_date_subqueries(this.app_, query);
    } else {
      const query_payload = query.query;
      // don't apply date mutations to the aggs, only the `query` part!
      this.apply_date_subqueries(this.app_, query_payload);
      query.query = query_payload;
    }

    if (filter_meta_queries) {
      filter_meta_queries.map((meta_query) => {
        if (meta_query.finished_query) {
          query.query.bool.filter.push(meta_query.finished_query);
        } else {
          const clause = meta_query.clause;

          const real_filter_query = this.construct_filter_subquery(meta_query);
          // console.debug('real_filter_query', real_filter_query);

          if (
            is_query_function &&
            meta_query.values.length &&
            meta_query.elastic_variable === query_itself.elastic_variable
          ) {
            query = query_itself.fn(meta_query.values);
          }

          if (real_filter_query !== null) {
            if (!query.query) {
              query['query'] = {
                bool: {
                  must: [],
                },
              };
            }
            if (Array.isArray(real_filter_query)) {
              // concat because some queries might come in as arrays of "filters"
              query.query.bool[clause] = (query.query.bool.must || []).concat(
                real_filter_query
              );
            } else {
              query.query.bool[clause].push(real_filter_query);
            }
          }
        }
      });
    }

    if (is_msearch) {
      const internal_query = query.query;

      const lob_based = query_itself_.m_search_query_lines[0];
      const address_based = query_itself_.m_search_query_lines[1];

      lob_based.query.query = JSON.parse(
        JSON.stringify(internal_query).replace(/address./g, 'lob.addresses.')
      );
      address_based.query.query = JSON.parse(
        JSON.stringify(internal_query).replace(/lob.addresses/g, 'address')
      );

      query = [lob_based, address_based];
    }

    console.debug({
      q: query,
      n: this.query_name,
    });

    return {
      query: query,
      type: VISUALIZATION_TYPES[this.query_name],
    };
  }
}
