import { Injectable } from '@angular/core';
import { ElasticService } from './elastic.service';
import { ImapDrawing } from '../interfaces/ImapDrawing';
import { roundUp } from '../utils/helpers/numerical';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { encodeSVG } from '../utils/helpers/gmaps';

@Injectable({
  providedIn: 'root'
})
export class MapdrawingService {
  constructor(private es: ElasticService) {
    this.radiusChangedSubject$
      .pipe(filter(olay => Boolean(olay)))
      .subscribe(overlay_event => {
        const { center, radius } = this.extractCircleData(
          overlay_event.overlay,
          true
        );

        this.removeCircleMarkers();

        let prettified_radius;
        if (radius < 1000) {
          prettified_radius = `${roundUp(radius as number, 4)} m`;
        } else {
          prettified_radius = `${roundUp((radius as number) / 1e3, 4)} km`;
        }

        this.radiusCenter = new google.maps.Marker({
          map: this._map,
          position: center
        });

        this.radiusInformer = new google.maps.Marker({
          map: this._map,
          position: google.maps.geometry.spherical.computeOffset(
            new google.maps.LatLng(center.lat, center.lng),
            radius as number,
            90
          ),
          icon: {
            anchor: new google.maps.Point(50, 10),
            url: encodeSVG(
              `data:image/svg+xml;utf-8,
            <svg width="100"
                height="30"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                xml:space="preserve"
                style="image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd">
              <rect rx="5px" x="0"
                    y="0"
                    width="100"
                    height="20"
                    fill="#666666" />
              <text font-size="14px"
                    text-anchor="middle"
                    x="50%"
                    y="35%"
                    fill="#FFFFFF"
                    dominant-baseline="middle"
                    font-family="sans-serif">${prettified_radius}</text>
            </svg>
          `
            )
          }
        });
      });
  }

  private drawing_state: 'circle' | 'polygon' | '' = '';
  private drawingManager: google.maps.drawing.DrawingManager;

  private is_in_drawing_mode: Boolean;

  private circle_btn: HTMLButtonElement;
  private polygon_btn: HTMLButtonElement;
  private trash_btn: HTMLButtonElement;
  private done_btn: HTMLButtonElement;
  private _map: google.maps.Map;

  private map_drawing_overlays = [];

  private _interaction_buttons: Array<HTMLButtonElement> = [];

  private _r = /circle|done|trash|polygon/;

  private radiusCenter: google.maps.Marker;
  private radiusInformer: google.maps.Marker;

  radiusChangedSubject$: BehaviorSubject<
    google.maps.drawing.OverlayCompleteEvent
  > = new BehaviorSubject(null);

  handleMapDrawings(
    circle_btn: HTMLButtonElement,
    polygon_btn: HTMLButtonElement,
    trash_btn: HTMLButtonElement,
    done_btn: HTMLButtonElement,
    _map: google.maps.Map
  ) {
    const self = this;

    self.drawing_state = '';
    self.circle_btn = circle_btn;
    self.polygon_btn = polygon_btn;
    self.trash_btn = trash_btn;
    self.done_btn = done_btn;

    self._map = _map;

    self._interaction_buttons = Array.from(arguments).filter(arg => !arg.zoom); // filter out the map
    self._interaction_buttons.map(btn => {
      const clone = btn.cloneNode(true) as HTMLButtonElement;

      if (!btn.parentNode) {
      } else {
        btn.parentNode.replaceChild(clone, btn); // get rid of existing click evts completely

        const relevant_class = clone.className.match(self._r)[0];
        self[`${relevant_class}_btn`] = clone;
      }
    });

    self.done_btn.addEventListener('click', _ => {
      self.is_in_drawing_mode = false;

      resetMapCursor();

      const polygons = self.map_drawing_overlays.filter(function (item) {
        return item.type === 'polygon';
      });

      const circles = self.map_drawing_overlays.filter(function (item) {
        return item.type === 'circle';
      });

      if (polygons.length || circles.length) {
        const type = polygons.length ? 'polygon' : 'circle';
        const customBoundary: ImapDrawing = {
          type: type,
          path:
            type === 'polygon'
              ? self.extractPolygonPoints(polygons[0].data)
              : self.extractCircleData(circles[0].data)
        };
        self.es.customBoundary = customBoundary;
        self.es.customDrawing = polygons[0] || circles[0];
        self.es.search(
          (this.es.activeMapComponent && this.es.activeMapComponent.klass) ||
          null
        );
      }

      self.resetDrawingManager();
      setActiveDrawingButton(null);
    });

    self.polygon_btn.addEventListener('click', _ => {
      resetDrawingState();
      self.trash_btn.dispatchEvent(new Event('click'));

      setCrosshairsCursor();
      self.removeCustomDrawings();
      setActiveDrawingButton('polygon');

      // map_dom.removeClass('will-set-points').addClass('will-set-points');

      self.drawingManager = new google.maps.drawing.DrawingManager({
        map: self._map,
        drawingMode: google.maps.drawing.OverlayType.POLYGON,
        drawingControl: false,
        polygonOptions: {
          ...environment.gmaps_settings.drawingColors
        }
      });

      try {
        google.maps.event.addListener(
          self.drawingManager,
          'overlaycomplete',
          overlayCompleteHandler
        );
      } catch (err) {
        self.resetDrawingManager();
        return;
      }

      function overlayCompleteHandler(item) {
        // _app.$map_dom.removeClass('will-set-points');
        self.map_drawing_overlays.push({
          type: 'polygon',
          data: item.overlay
        });
        self.drawingManager.setMap(null);
        self.drawingManager = null;
      }
    });

    self.circle_btn.addEventListener('click', _ => {
      resetDrawingState();
      self.trash_btn.dispatchEvent(new Event('click'));

      setCrosshairsCursor();
      self.removeCustomDrawings();
      setActiveDrawingButton('circle');

      // map_dom.removeClass('will-set-points').addClass('will-set-points');

      self.drawingManager = new google.maps.drawing.DrawingManager({
        map: self._map,
        drawingMode: google.maps.drawing.OverlayType.CIRCLE,
        drawingControl: false,
        circleOptions: {
          ...environment.gmaps_settings.drawingColors
          // TODO
          // draggable: true,
          // editable: true
        }
      });

      const click_listener = google.maps.event.addListener(
        self._map,
        'click',
        function () {
          // map_dom.removeClass('will-set-points').addClass('will-set-points');
        }
      );

      try {
        google.maps.event.addListener(
          self.drawingManager,
          'overlaycomplete',
          overlayCompleteHandler
        );
      } catch (err) {
        console.error({ err });
        click_listener.remove();
        self.resetDrawingManager();
        return;
      }

      function overlayCompleteHandler(
        item: google.maps.drawing.OverlayCompleteEvent
      ) {
        // _app.$map_dom.removeClass('will-set-points');
        // google.maps.event.removeListener(click_listener);

        self.map_drawing_overlays.push({
          type: 'circle',
          data: item.overlay
        });

        self.drawingManager.setMap(null);
        self.drawingManager = null;

        self.radiusChangedSubject$.next(item);
      }
    });

    self.trash_btn.addEventListener('click', _ => {
      setActiveDrawingButton('trash');

      resetMapCursor();
      self.removeCustomDrawings();
      self.resetDrawingManager();
      self.is_in_drawing_mode = false;

      if (!self.drawing_state) {
        return;
      }

      switch (self.drawing_state) {
        case 'polygon':
          self.polygon_btn.dispatchEvent(new Event('click'));
          break;
        case 'circle':
          self.circle_btn.dispatchEvent(new Event('click'));
          break;
      }
    });

    function setCrosshairsCursor() {
      self._map.setOptions({
        draggableCursor: 'crosshair'
      });
    }

    function resetMapCursor() {
      self._map.setOptions({
        draggableCursor: null
      });

      // removeClass('will-set-points');
    }

    function resetDrawingState() {
      self.drawing_state = null;
    }

    function setActiveDrawingButton(kind: string | any) {
      Array.from(document.querySelectorAll(`.drawing button`)).forEach(elem =>
        elem.classList.remove('active')
      );

      if (!kind) {
        return;
      }
      self.drawing_state = kind;
      document.querySelector(`.drawing .${kind}`).classList.add('active');
    }
  }

  removeCustomDrawings() {
    this.map_drawing_overlays.map((olay, i) => {
      olay.data.setMap(null);
      this.map_drawing_overlays.splice(i, 1);
    });

    this.map_drawing_overlays = [];
    this.es.customBoundary = null;

    this.removeCircleMarkers();

    this.resetDrawingManager();
  }

  removeCircleMarkers() {
    if (this.radiusInformer) {
      this.radiusInformer.setMap(null);
    }

    if (this.radiusCenter) {
      this.radiusCenter.setMap(null);
    }
  }

  resetDrawingManager() {
    if (this.drawingManager) {
      this.drawingManager.setMap(null);
      this.drawingManager = null;
    }
  }

  extractPolygonPoints(data: google.maps.Polygon) {
    const MVCarray = data.getPath().getArray();

    const to_return = MVCarray.map(point => {
      return `(${roundUp(point.lat())},${roundUp(point.lng())})`;
    });
    // first and last must be same
    return to_return.concat(to_return[0]).join(',');
  }

  extractCircleData(
    data,
    as_number?
  ): {
    center: google.maps.LatLngLiteral;
    radius: string | number;
  } {
    return {
      center: { lat: data.center.lat(), lng: data.center.lng() },
      radius: as_number ? data.radius : data.radius + 'm'
    };
  }
}
