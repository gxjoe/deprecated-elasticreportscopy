import { Injectable, QueryList } from '@angular/core';
import { NzCollapsePanelComponent } from 'ng-zorro-antd/collapse';

@Injectable({
  providedIn: 'root'
})
export class UiInteractionService {

  constructor() { }

  closeAllCollapsePanels(panels: QueryList<NzCollapsePanelComponent>) {
    panels.forEach(panel => {
      panel.nzActive = false;
      panel.markForCheck();
    });
  }
}
