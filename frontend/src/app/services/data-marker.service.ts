import { Injectable } from '@angular/core';

import * as objectPath from 'object-path';
import { ElasticService } from './elastic.service';
import {
  extractNumberFromString,
  sortArrOfStringsAlphabetically,
  extractFirstValueFromDict,
  CURRENCY_PREFIX_UTF,
  CURRENCY_PREFIX_ISO,
} from '../utils/helpers/helpers';
import { dynamicSort, sortArrayByKey } from '../charts/utils';
import { mapDrawingContainsLatLng } from '../utils/helpers/gmaps';
import {
  CHART_FUNCT,
  SUM_SIGMA_UTF,
  IPieChartDataset,
} from '../charts/histogram-table-chart/common';
import {
  extractSelectedLocationPerilAssessment,
} from '../utils/helpers/data-marker-service-utils';
import { AddressesDoc } from '../interfaces/elastic';
import { FeatureCollection, Point } from '@turf/turf';

interface IStringToDataMarkerMap {
  [id: string]: google.maps.Marker;
}

interface IGroupedDataMarkers {
  active: Array<string>;
  all: IStringToDataMarkerMap;
  grouped_by: {
    offer: IStringToDataMarkerMap;
    lob: IStringToDataMarkerMap;
  };
  original_geojson: {};
  TSI_vs_LoB?: {};
}

@Injectable({
  providedIn: 'root',
})
export class DataMarkerService {
  constructor(private esService: ElasticService) { }

  private dataMarkers: IGroupedDataMarkers = {
    active: [],
    all: {},
    grouped_by: {
      offer: {},
      lob: {},
    },
    original_geojson: {},
    TSI_vs_LoB: {},
  };

  private spiderifier;
  private mapClickUnspiderifierListener: google.maps.MapsEventListener;

  get all() {
    return this.dataMarkers.all;
  }

  get byLob() {
    return this.dataMarkers.grouped_by.lob;
  }

  toGeoJSON() {
    return this.dataMarkers.original_geojson;
  }

  private __addToGroup(kind, id, content) {
    if (!this.dataMarkers.grouped_by[kind][id]) {
      this.dataMarkers.grouped_by[kind][id] = [content];
    } else {
      this.dataMarkers.grouped_by[kind][id].push(content);
    }
  }

  private _sumUpTSIs(line_of_business, tsi) {
    const pure = extractNumberFromString(tsi);
    if (!Object.keys(this.dataMarkers.TSI_vs_LoB).includes(line_of_business)) {
      this.dataMarkers.TSI_vs_LoB[line_of_business] = { count: 0, sum: 0 };
    }

    this.dataMarkers.TSI_vs_LoB[line_of_business].count += 1;
    this.dataMarkers.TSI_vs_LoB[line_of_business].sum += pure;
  }

  private _prepareLoBvsTSIforTable() {
    const header = sortArrOfStringsAlphabetically(
      Object.keys(this.dataMarkers.TSI_vs_LoB)
    );
    const rows: Array<{ [id: string]: Map<string, any> }> = [];

    let piechartDataset: Array<IPieChartDataset> = [];

    const count_col_name = 'Address Count';
    const sigma = `${SUM_SIGMA_UTF} TSI`;

    header.map((lob) => {
      const _map = new Map<string, any>();
      _map.set(count_col_name, this.dataMarkers.TSI_vs_LoB[lob].count);
      _map.set(
        sigma,
        CHART_FUNCT.readable_large_currency(
          this.dataMarkers.TSI_vs_LoB[lob].sum
        )
      );

      rows.push({ [`${lob}`]: _map });
    });

    piechartDataset = Array.from(
      rows.map((row) => {
        return {
          count: extractFirstValueFromDict(row).get(sigma),
          category: Object.keys(row)[0],
          _occurrence: extractFirstValueFromDict(row).get(count_col_name),
          _count: extractNumberFromString(
            extractFirstValueFromDict(row).get(sigma)
          ),
        };
      })
    ).sort(dynamicSort('-_count'));

    const totals = { sum: 0, count: 0 };
    piechartDataset.map((item) => {
      totals.sum += item._count;
      totals.count += item._occurrence;
    });

    rows.push({
      [`${SUM_SIGMA_UTF}`]: new Map<string, any>([
        [count_col_name, totals.count],
        [sigma, CHART_FUNCT.readable_large_currency(totals.sum)],
      ]),
    });

    return {
      header: ['LoB', count_col_name, sigma],
      rows: rows,
      piechartDataset: piechartDataset,
    };
  }

  private addMarker(feature) {
    return new google.maps.Marker({
      icon: feature.properties.icon,
      position: feature.properties.position,
      meta: {
        icon: feature.properties.icon,
        icon_plus_sign: feature.properties.meta.icon,
      },
    } as any);
  }

  unsetTSIvsLoB() {
    this.dataMarkers.TSI_vs_LoB = {};
  }

  setActiveDataMarkers(ids: Array<any>) {
    this.hideAllDataMarkers(ids);
    Object.keys(this.dataMarkers.grouped_by.lob).map((key) => {
      if (ids.includes(key)) {
        (this.dataMarkers.grouped_by.lob[key] as any).map((geo_item) => {
          this.all[geo_item.properties.ids.address].setVisible(true);
        });
      }
    });
    this.dataMarkers.active = ids.slice();
  }

  unsetActiveDataMarkers() {
    this.showAllDataMarkers();
  }

  getByPathAndId(path, id) {
    return objectPath.get(this.dataMarkers, `${path}.${id}`);
  }

  private hideAllDataMarkers(except_ids: Array<string> = [], remove = false) {
    const all_keys = Object.keys(this.dataMarkers.all);
    for (let ind = 0; ind < all_keys.length; ind++) {
      const key = all_keys[ind];
      if (except_ids.includes(key)) {
        continue;
      }
      this.dataMarkers.all[key].setVisible(false);
      if (remove) {
        this.dataMarkers.all[key].setMap(null);
        delete this.dataMarkers.all[key];
      }
    }

    if (remove) {
      this.dataMarkers.grouped_by.lob = {};
      this.dataMarkers.grouped_by.offer = {};
    }
  }

  private showAllDataMarkers() {
    Object.keys(this.dataMarkers.all).map((key) => {
      this.dataMarkers.all[key].setVisible(true);
    });
  }

  public setDataMarkers(collection: FeatureCollection) {
    const self = this;
    const Spiderifier = (window as any).OverlappingMarkerSpiderfier;

    if (this.spiderifier) {
      this.spiderifier.forgetAllMarkers();
    }

    if (this.mapClickUnspiderifierListener) {
      this.mapClickUnspiderifierListener.remove();
    }

    this.spiderifier = new Spiderifier(this.esService.activeMapComponent.map, {
      markersWontMove: true,
      markersWontHide: true,
      ignoreMapClick: true,
      keepSpiderfied: true,
      piralFootSeparation: 20,
      spiralLengthFactor: 20,
      spiralLengthStart: 20,
      nearbyDistance: 1,
      basicFormatEvents: false,
    });

    this.spiderifier.addListener('format', function (marker, status) {
      if (status === Spiderifier.markerStatus.SPIDERFIABLE) {
        marker.setOptions({ icon: marker.meta.icon_plus_sign });
      } else {
        marker.setOptions({ icon: marker.meta.icon });
      }
    });

    this.mapClickUnspiderifierListener = google.maps.event.addListener(
      this.esService.activeMapComponent.map,
      'click',
      function () {
        self.spiderifier.unspiderfy();
      }
    );

    collection.features.map((feat) => {
      const id = feat.properties.ids.address || feat.properties.ids.offer;

      if (
        this.esService.activeMapComponent.map
          .getBounds()
          .contains(feat.properties.position)
      ) {
        let can_proceed = true;
        if (this.esService.customBoundary && this.esService.customDrawing) {
          const coords = (
            (feat.geometry as any).coordinates as Array<number>
          ).reverse();
          const latLng = new google.maps.LatLng(coords[0], coords[1]);
          can_proceed = mapDrawingContainsLatLng(
            this.esService.customDrawing,
            latLng
          );
        }

        if (
          !can_proceed ||
          !feat.properties.other_data.LOB ||
          this.dataMarkers.all[id]
        ) {
        } else {
          if (feat.properties.other_data.address) {
            this._sumUpTSIs(
              feat.properties.other_data.LOB.LineOfBusiness,
              feat.properties.other_data.address[
              `SI per Loc ${CURRENCY_PREFIX_UTF()} (share)`
              ]
            );
          }

          const marker = this.addMarker(feat);
          this.dataMarkers.all[id] = marker;

          this.spiderifier.addMarker(marker, function () { });

          this.dataMarkers.original_geojson[id] = feat;

          this.__addToGroup('lob', feat.properties.ids.lob, feat);
          this.__addToGroup('offer', feat.properties.ids.offer, feat);
        }
      }
    });

    return this._prepareLoBvsTSIforTable();
  }

  public prepareScoreMatrixTable(
    collection: FeatureCollection<Point>
  ) {
    this.dataMarkers.TSI_vs_LoB = {};

    collection.features.map((feature) => {
      this._sumUpTSIs(
        feature.properties.hit.lob.lineOfBusiness,
        feature.properties.hit.address[
        `tsiPerLocation_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`
        ]
      );
    });

    return this._prepareLoBvsTSIforTable();
  }

  public prepareClientReportInsideScatterPlotPolygon(
    collection: FeatureCollection<Point>
  ) {
    const mapping = [];

    const allHits: AddressesDoc[] = collection.features.map(
      (f) => f.properties.hit
    );

    allHits.map((hit) => {
      mapping.push({
        'Offer ID': hit.offer.number,
        'LoB ID': hit.lob.id,
        'LoB Status': hit.lob.status,
        'Client Name': hit.offer.client.name,
        'Address': `${hit.address.country} – ${hit.address.street} ${hit.address.houseNumber || ''}, ${hit.address.zipCode || ''} ${hit.address.city || ''}`,
        'Type of Business': hit.lob.typeOfBusiness,
        'TSI Per Location':
          hit.address[
          `tsiPerLocation_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`
          ],
        // more columns requested within https://gxperts.atlassian.net/browse/UMGB-7
        [`PML Per Location Adjusted For Share`]:
          hit.address.locationProperty[
          `PML_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`
          ] || 0, // UDGB-54 default to 0 to indicate it's missing
        [`Location Assessment (${(window as any).selected_location_peril})`]:
          extractSelectedLocationPerilAssessment(hit),

        // All location assessments are useless here, esp. when a single location peril
        // is selected.

        // Discontinuing as per UDGB-31 because the processing times for 1K+ rows is not acceptable.

        // 'All Location Assessments': unifyAndSortLocationPerilAssessments(
        //   hit,
        //   allHits
        // ),
      });
    });

    return sortArrayByKey(mapping, 'TSI Per Location', true);
  }

  public prepareClientReportInsideEUM_PremiumSILoB_Polygon(
    collection: FeatureCollection<Point>
  ) {
    const mapping = [];

    collection.features.map((feature) => {
      const { hit } = feature.properties.meta;

      mapping.push({
        ...hit,
      });
    });

    return mapping.sort(
      dynamicSort(
        `lob.totalSumInsured_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`
      )
    );
  }

  public unsetDataMarkers() {
    this.unsetActiveDataMarkers();
    this.unsetTSIvsLoB();
    this.hideAllDataMarkers([], true);
  }
}
