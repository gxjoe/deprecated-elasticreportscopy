
import { switchMap, distinctUntilChanged, debounceTime } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject, identity } from 'rxjs';
import {
  MAP_KLASSES,
  buildQueries,
  isQuerySkippable,
  VIEW_VS_QUERIES_ON_LOAD
} from '../elastic/queries';
import {
  hashObject,
  getHashedKey,
  constructNestedFilter
} from '../elastic/utils';
import { QueryService } from './query.service';
import { AppComponent } from '../app.component';
import { GmapComponent } from '../gmap/gmap.component';
import { FilterComponent } from '../utils/higher-order/FilterComponent';
import { SearchResponse } from 'elasticsearch';
import { InfowindowService } from './infowindow.service';

import { ImapDrawing } from '../interfaces/ImapDrawing';

import {
  indexNameChooser,
  currencyResolver,
  authQueryFilter
} from '../elastic/constants';
import { COMPONENT_KEYS_TO_ALWAYS_UPDATE } from '../multiselect/constants';
import { MultiselectComponent } from '../multiselect/multiselect.component';
import { AuthService } from './auth.service';
import { CURRENCY_PREFIX_ISO, extractNumberFromString, isBetween } from '../utils/helpers/helpers';
import { AddressesDoc, LocationAssessment } from '../interfaces/elastic';

@Injectable({
  providedIn: 'root'
})
export class ElasticService {
  private host: string;
  private username: string;
  private password: string;
  private headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Accept: '*/*, application/json'
    // Authorization: `Basic ${this._getBearer()}`
  };

  private QUERIES;

  public percentualProgress = new Subject<number>();
  public filtersHaveChanged = new Subject<string>();

  public observable_subject = new Subject<string>();
  private query_subject = new Subject<string>();

  public executing: Boolean;
  public executing_charts: Boolean;

  public activeMapComponent: GmapComponent;

  public customBoundary: ImapDrawing;
  public customDrawing: {
    [data: string]: google.maps.Circle | google.maps.Polygon;
  };

  private isStalledDueToAuth = true;

  queriesArray: Array<{
    key: string;
    query: any;
    true_query?: any;
  }> = [];

  shouldFilterByAddressIDs: BehaviorSubject<boolean> = new BehaviorSubject(
    false
  );
  filterByAddressIDs: BehaviorSubject<string[]> = new BehaviorSubject([]);

  constructor(
    private http: HttpClient,
    private queryService: QueryService,
    public app_component: AppComponent,
    private infowin: InfowindowService,
    private auth: AuthService
  ) {
    Object.assign(this, environment.elasticsearch);

    this.auth.authProcessing.subscribe(observer => {
      this.isStalledDueToAuth = observer;
    });
  }

  _forceFlushExecutionMonitors() {
    this.executing = false;
    this.executing_charts = false;
  }

  _lsGet = (key, is_hashed = false) => {
    // this.localStorageService.(webStorage as any).getItem(key);
    if (!is_hashed) {
      key = getHashedKey(JSON.stringify(key));
    }

    return window.localStorage.getItem(key);
  }

  _lsSet = (_hash, obj) => {
    // this.localStorageService.set(_hash, JSON.stringify(obj));
    _hash = getHashedKey(_hash);
    try {
      window.localStorage.setItem(_hash, JSON.stringify(obj));
    } catch (err) {
      if (('' + err).indexOf('exceeded') !== -1) {
        window.localStorage.clear();
      }
    }
  }

  _getBearer() {
    return btoa(`${this.username}:${this.password}`);
  }

  _raw_search(
    query_terms: Observable<string>,
    debounceMs = 2,
    construct,
    args
  ) {
    return query_terms.pipe(
      debounceTime(debounceMs),
      distinctUntilChanged(),
      switchMap(_ => construct(args)));
  }

  _constructRequest(
    method: 'options' | 'get' | 'post',
    body?: any,
    url_path_suffix: string = '',
    query_name?: string
  ): Observable<Object> {
    let responseObservable;
    let host_;

    let tmp_body;
    if (typeof body === 'string') {
      tmp_body = JSON.parse(body);
    } else {
      tmp_body = Object.assign({}, body);
    }

    const is_msearch = query_name === 'AllOffers' && Array.isArray(tmp_body);
    let query, index_name;

    this.query_subject.next(hashObject(new Date().toISOString()));

    if (!is_msearch) {
      ({ query, index_name } = indexNameChooser(query_name, body));
      if (query) {
        body = query;
      }

      host_ = `${this.host}/${index_name}${url_path_suffix || ''}/_search`;
    } else {
      if (typeof body === 'string') {
        body = JSON.parse(body);
      }
      body =
        [].concat
          .apply(
            [],
            [
              ...body.map(bitem => {
                const actual_query = bitem.query;
                if (!actual_query.query.bool.must) {
                  actual_query.query.bool.must = [authQueryFilter(bitem.index)];
                } else {
                  actual_query.query.bool.must = []
                    .concat(actual_query.query.bool.must)
                    .concat(authQueryFilter(bitem.index));
                }

                return [
                  {
                    index: bitem.index
                  },
                  {
                    ...actual_query
                  }
                ];
              })
            ]
          )
          .map(a => JSON.stringify(a))
          .join('\n') + '\n'; // msearch must be terminated by a new line
      host_ = `${this.host}/_msearch`;
    }

    switch (method) {
      case 'options':
        responseObservable = this.http.options(host_, {
          headers: this.headers
        });
        break;
      case 'get':
        responseObservable = this.http.get(host_, {
          headers: this.headers
        });

        break;
      case 'post':
        const headers = is_msearch
          ? { 'Content-Type': 'application/x-ndjson' }
          : this.headers;
        let fallback: Observable<any>;
        const hashed_query = getHashedKey(body);

        console.debug({
          hash: hashed_query,
          body: body.aggs
        });

        if (environment.memcached_wrapper.endpoint) {
          fallback = this.http.post(
            environment.memcached_wrapper.endpoint,
            (body = {
              query_key: hashed_query,
              url: host_,
              headers: headers,
              payload: body,
              expire: 60 * 60
            }),
            {
              headers: this.headers
            }
          );
        } else {
          fallback = this.http.post(host_, body, {
            headers: headers
          });
        }

        const cached = false; // this._lsGet(getHashedKey(body.payload), true);
        if (cached) {
          responseObservable = Observable.create(function (observer) {
            observer.next(cached);
            observer.complete();
          });
        } else {
          responseObservable = fallback;
        }

        const index = this.queriesArray.findIndex(
          predicate => predicate.key === query_name
        );
        if (index !== -1) {
          this.queriesArray[index].true_query = body.payload;
        }
    }

    return responseObservable;
  }

  OPTIONS() {
    return this._constructRequest('options');
  }

  GET(
    body?: Object,
    url_path_suffix?: string,
    query_name?: string
  ): Observable<any> {
    return this._constructRequest('get', body, url_path_suffix, query_name);
  }

  POST(
    body?: Object,
    url_path_suffix?: string,
    query_name?: string
  ): Observable<any> {
    return this._constructRequest('post', body, url_path_suffix, query_name);
  }

  readyForRefresh() {
    this.filtersHaveChanged.next('next');
  }

  ping() {
    // const ping_ = this.GET();
  }

  search(
    refresh_component_by_klass?: string,
    dont_override_size?: boolean,
    size_override?: number
  ): Promise<any> {
    if (this.isStalledDueToAuth) {
      setTimeout(() => {
        this.search(refresh_component_by_klass);
      }, 5e2);
      return;
    }

    console.debug('usernames and countries', this.auth.parseXmlAuthResponse());

    // this.percentualProgress.next(0);
    this.observable_subject.next(Math.random() + '');

    const doNotPipeDistinctUntilChanged = refresh_component_by_klass
      && typeof refresh_component_by_klass === 'string'
      // Map requests, esp those run for the hotspots, likely won't change because
      // the filtering & clustering is done in the FE.
      // Thus, if we're dealing w/ map ES requests, don't pipe distinctUntilChanged
      && refresh_component_by_klass.toLowerCase().includes('map');

    return new Promise((resolve, reject) => {
      this.observable_subject
        .pipe(doNotPipeDistinctUntilChanged
          // Map requests, esp those run for the hotspots, likely won't change because
          // the filtering & clustering is done in the FE.
          // This, if we're dealing w/ map ES requests, don't pipe distinctUntilChanged
          ? identity
          : distinctUntilChanged())
        .pipe(debounceTime(50))
        .subscribe(__ => {
          // if (!this.executing || !this.executing_charts) {
          // resolve();
          // }
        });

      const filterComponentMetaQueries = this.app_component.filterComponents.map(
        (component: FilterComponent) => {
          return component.filterQueryBuilder();
        }
      );

      const is_interactive = !(this.app_component
        .interactiveSearchCheckboxComponent
        ? !this.app_component.interactiveSearchCheckboxComponent.checked
        : true);

      console.time('build queries');
      this.QUERIES = buildQueries(refresh_component_by_klass, this.app_component);
      console.timeEnd('build queries');

      const executeMaps = (filters?: any[]) => {
        if (
          this.executing ||
          this.infowin.is_info_window_open ||
          !this.activeMapComponent
        ) {
          this.executing = false;
          return;
        }

        this.executing = true;
        let query_obj;

        const map_queries = buildQueries(this.activeMapComponent.klass);

        switch (
        refresh_component_by_klass === MAP_KLASSES.OfferHotspotMap
          ? refresh_component_by_klass
          : this.activeMapComponent.klass
        ) {
          case MAP_KLASSES.heatmap:
            query_obj = this.queryService.construct(
              'OfferDistributionHeatmap',
              this.activeMapComponent,
              this.app_component,
              false,
              filters,
              map_queries['OfferDistributionHeatmap'],
              this.customBoundary
            );
            break;
          case MAP_KLASSES.ExperimentalHeatmap:
          case MAP_KLASSES.OfferDistribution:
            query_obj = this.queryService.construct(
              'OfferDistributionHeatmap',
              this.app_component.OfferDistributionHeatmapComponent,
              this.app_component,
              false,
              filters,
              map_queries['OfferDistributionHeatmap'],
              this.customBoundary
            );

            if (
              this.shouldFilterByAddressIDs.value &&
              this.filterByAddressIDs.value.length
            ) {
              query_obj.query.query.bool.must.push({
                bool: {
                  should: this.filterByAddressIDs.value.map(address_id => {
                    return {
                      nested: constructNestedFilter(
                        'address',
                        'address.id',
                        address_id
                      )
                    };
                  })
                }
              });
            }

            break;
          case MAP_KLASSES.ClientDistribution:
            query_obj = this.queryService.construct(
              'ClientDistributionHeatmap',
              this.app_component.ClientDistributionHeatmapComponent,
              this.app_component,
              false,
              filters,
              map_queries['ClientDistributionHeatmap'],
              this.customBoundary
            );
            break;
          case MAP_KLASSES.clustermap:
            query_obj = this.queryService.construct(
              'OfferDistributionClustermap',
              this.app_component.OfferDistributionHeatmapComponent,
              this.app_component,
              false,
              filters,
              map_queries['OfferDistributionClustermap'],
              this.customBoundary
            );
            break;
          case MAP_KLASSES.OfferHotspotMap:
            query_obj = this.queryService.construct(
              MAP_KLASSES.OfferHotspotMap,
              this.activeMapComponent,
              this.app_component,
              false,
              filters,
              buildQueries(MAP_KLASSES.OfferHotspotMap)[
              MAP_KLASSES.OfferHotspotMap
              ],
              this.customBoundary
            );
            break;
        }

        const { query, type } = query_obj;
        if (!query || !type) {
          console.debug('no query or type', query, type);
          this.executing = false;
          return;
        }

        const use_aggs =
          this.activeMapComponent.map.getZoom() <=
          environment.gmaps_settings.zoom_to_show_individual_markers;

        if (!dont_override_size) {
          query.size = use_aggs ? 0 : 2e3;
        }

        query.aggregations = use_aggs ? query.aggregations : {};

        this.POST(query, null, refresh_component_by_klass).subscribe(
          (resp: any) => {
            try {
              resp = resp.json();
            } catch (err) { }

            if (resp.response) {
              resp = resp.response;
            }

            resp = resp as SearchResponse<any>;

            this.executing = false;
            this.percentualProgress.next(100);

            switch (
            refresh_component_by_klass === MAP_KLASSES.OfferHotspotMap
              ? refresh_component_by_klass
              : this.activeMapComponent.klass
            ) {
              case MAP_KLASSES.heatmap:
                this.activeMapComponent.handleHeatmap(resp);
                break;
              case MAP_KLASSES.OfferDistribution:
                this.app_component.OfferDistributionHeatmapComponent.handleHeatmap(
                  resp
                );
                break;
              case MAP_KLASSES.ClientDistribution:
                this.app_component.ClientDistributionHeatmapComponent.handleHeatmap(
                  resp
                );
                break;
              case MAP_KLASSES.ExperimentalHeatmap:
                const mock = this.http
                  .get('http://localhost:8080/uniqa')
                  .subscribe(geojson => {
                    console.info(geojson);
                    this.app_component.ExperimentalHeatmapComponent.handleHeatmap(
                      geojson as any
                    );
                  });
                break;
              case MAP_KLASSES.OfferHotspotMap:
                resolve(resp);
            }
          },
          err => {
            switch (this.activeMapComponent.klass) {
              case MAP_KLASSES.ExperimentalHeatmap:
                const mock = this.http
                  .get('http://localhost:8080/uniqa')
                  .subscribe((geojson: any) => {
                    const parsed = JSON.parse(geojson.data.data).features;
                    this.app_component.ExperimentalHeatmapComponent.handleHeatmap(
                      {
                        took: 1,
                        timed_out: false,
                        _shards: {
                          total: 1,
                          successful: 1,
                          failed: 1,
                          skipped: 1
                        },
                        hits: {
                          hits: parsed as any,
                          total: 0,
                          max_score: 1
                        },
                        aggregations: {}
                      }
                    );
                  });
                break;
            }
            reject(err);
          }
        );
      };

      const executeCharts = (
        filters?: any[],
        refresh_component_by_klass_?: string
      ) => {
        if (this.executing_charts) {
          // return;
        }

        this.queriesArray = [];

        let resolveCount = 0;
        const disregard_maps = !is_interactive;
        this.executing_charts = true;
        let query_obj;

        const applicableKeys = Object.keys(this.QUERIES).filter(key => {
          if (key.includes('map')) {
            return false;
          }

          const KeyedComponent = this.app_component.getComponentByKlass(key);

          if (!KeyedComponent && !size_override) {
            return false;
          }

          if (!refresh_component_by_klass_ && isQuerySkippable(key)) {
            return false;
          }

          return true;
        });

        applicableKeys.map((key, kid) => {
          const query_itself = this.QUERIES[key];
          try {
            this.app_component.getComponentByKlass(key).isLoading = true;
          } catch (err) { }

          console.time(`construct ${key}`);
          query_obj = this.queryService.construct(
            key,
            null,
            this.app_component,
            disregard_maps,
            filters,
            query_itself
          );
          console.timeEnd(`construct ${key}`);

          const { query, type } = query_obj;

          if (!query || !type) {
            console.debug('no query or type', key);
            this.executing_charts = false;
          }

          if (size_override) {
            query.size = size_override;
          }

          this.queriesArray.push({
            key: key,
            query: query
          });
        });

        this.queriesArray.map((__, index) => {
          const { key, query: query_itself } = this.queriesArray[index];
          const query: any = currencyResolver(query_itself, true); // do all necessary replacements

          if (this.queriesArray.length === 1) {
            this.percentualProgress.next(50);
          }

          const { index_name: targetEsIndex } = indexNameChooser(refresh_component_by_klass, query);

          this.POST(query, null, key).subscribe(
            raw_response => {
              const { true_query } = this.queriesArray[index];

              let response: SearchResponse<any>;
              if (raw_response.response) {
                response = raw_response.response;
              } else {
                response = raw_response;
              }

              console.debug({
                true_query: getHashedKey(true_query)
                // rkey: raw_response.query_key
              });

              // this._lsSet(true_query, response);

              const KeyedComponent = this.app_component.getComponentByKlass(
                key
              );

              if (KeyedComponent) {
                // console.warn({
                //   key,
                //   query
                // });

                if (response.hits && typeof response.hits.total !== 'number') {
                  // ES 7.x fix
                  response.hits.total = (response.hits.total as any).value;
                }

                if (targetEsIndex === environment.elasticsearch.address_based_index_name) {
                  // address response postprocessing introduced in UDGB-18
                  response = this.postProcessResponse<AddressesDoc>(response);
                }

                KeyedComponent.handleIncomingData(response, {
                  app: this.app_component,
                  keyed_component: KeyedComponent,
                  query: query
                });

                KeyedComponent.isLoading = false;

                [...new Set(this.app_component.textCardComponents)]
                  .filter(comp => comp.klass === key)
                  .map(component => {
                    if (query && component) {
                      console.debug({
                        key: key,
                        resp: response,
                        q: JSON.parse(query),
                        comp: component
                      });
                      component.handleIncomingData(key, response, query);
                      component.isLoading = false;
                      try {
                        component.cd.markForCheck();
                        component.cd.detectChanges();
                      } catch (err) {
                        console.warn(err);
                      }
                    }
                  });
                console.debug('');
              } else {
                console.warn('no keyed component', {
                  key,
                  app: this.app_component
                });
              }

              resolveCount++;
              this.percentualProgress.next(
                (resolveCount / this.queriesArray.length) * 100 || 100
              );

              if (resolveCount === this.queriesArray.length) {
                this.executing_charts = false;

                if (this.queriesArray.length === 1) {
                  console.info('Will resolve single', this.queriesArray);
                  resolve({ key, response, query });
                }

                // if "fetch total counts" button initiated this search
                if (
                  Object.keys(VIEW_VS_QUERIES_ON_LOAD).includes(
                    refresh_component_by_klass
                  )
                ) {
                  this.refreshComputationHeavyMultiselects(
                    // find the main bool filters of AllOffers and apply them to multiselect aggs so they correspond to allOffers selection
                    this.queriesArray.find(pair => pair.key === 'AllOffers')
                      .query[0].query.query.bool
                  );
                }
              }
            },
            err => {
              // TODO show error
              const KeyedComponent = this.app_component.getComponentByKlass(
                key
              );
              KeyedComponent.isLoading = false;

              console.error(`failed fetching ${this.queriesArray[index].key}`);
              alert(`
              • Are all necessary filters for this table selected?
              • What happens when you refresh the whole page and repeat your steps?
              • Does the site "${this.host}" load when you access it?

              Otherwise contact your favorite administrator and tell them that "fetching ${this.queriesArray[index].key} on host ${this.host} failed ".
              `);

              resolveCount++;
              this.percentualProgress.next(
                (resolveCount / this.queriesArray.length) * 100 || 100
              );

              if (resolveCount === this.queriesArray.length) {
                this.executing_charts = false;
              }

              if (this.queriesArray.length === 1) {
                reject(err);
              }
            }
          );
        });
      };

      executeMaps(filterComponentMetaQueries);
      executeCharts(filterComponentMetaQueries, refresh_component_by_klass);
    });
  }

  /**
   * As per UDGB-18, it's required to filter the hits manually
   * because queries on nested paths like `address.locationProperty.locationAssessments.score`
   * will NOT filter these nested arrays by default -- only the top level `hits` are affected (filtered away).
   * We *could* use `inner_hits` for this but it's more straightforward to just manually filter away what needs to be filtered away
   * @param response
   * @returns
   */
  postProcessResponse<T extends AddressesDoc>(response: SearchResponse<T>): SearchResponse<T> {
    const responseHits = response?.hits?.hits;
    let i = responseHits?.length;
    if (!i) {
      // don't bother
      return response;
    }

    const AddressUniqaPMLRange = (window as any).AddressUniqaPMLRange;
    const AddressLocationAssessmentScoreRange = (window as any).AddressLocationAssessmentScoreRange;

    // looping backwards to avoid original array shifts in the opposite direction
    // https://stackoverflow.com/a/16217435/8160318
    while (i--) {
      const hit = responseHits[i];
      const locationProperty = hit._source?.address?.locationProperty;
      if (!locationProperty) {
        continue;
      }

      if (AddressUniqaPMLRange?.length) {
        const pml = extractNumberFromString(locationProperty[`PML_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`]);
        const [min, max]: [number, number] = AddressUniqaPMLRange;
        if (isNaN(pml) || !isBetween(pml, min, max)) {
          // ditch this hit completely because it doesn't match the address PML range
          responseHits.splice(i, 1);
          // 'continuing' because the next filter will have no hit to work with!
          continue;
        }
      }

      if (AddressLocationAssessmentScoreRange?.length) {
        const assessments = locationProperty?.locationAssessments;
        if (!assessments || !Array.isArray(assessments)) {
          // not including locationAssessments disqualifies the hit straightaway
          responseHits.splice(i, 1);
          continue;
        }

        const [min, max]: [number, number] = AddressLocationAssessmentScoreRange;

        const filteredAssessments: LocationAssessment[] = [];

        for (const assessment of assessments) {
          const { score } = assessment;
          if (!isNaN(score) && isBetween(score, min, max)) {
            filteredAssessments.push(assessment);
          }
        }

        // replace the assessments
        responseHits[i]._source.address.locationProperty.locationAssessments = filteredAssessments;
      }
    }

    response.hits.hits = responseHits;

    return response;
  }

  refreshComputationHeavyMultiselects(bool_to_apply: Array<any>) {
    /**
     * Refresh all other filters based on the selection change of these
     */
    this.app_component.filterComponents
      // skip own refreshing
      .filter(fcomponent =>
        COMPONENT_KEYS_TO_ALWAYS_UPDATE().includes(fcomponent.klass)
      )
      .map((fcompo: MultiselectComponent, index) => {
        console.time(fcompo.klass);
        // options are concatenated so reset to empty
        fcompo.listOfOptions = [];

        // apply current selection to the query filters
        let filter_subqueries = this.queryService.construct_filter_subquery(
          fcompo.filterQueryBuilder()
        );

        if (!filter_subqueries) {
          filter_subqueries = {
            bool: { ...bool_to_apply }
          };
        }

        fcompo.prefetchOptions_(filter_subqueries);
        fcompo.preload(
          ((index + 1) / COMPONENT_KEYS_TO_ALWAYS_UPDATE().length) * 100,
          {
            force_real_options: true
          }
        );
        console.timeEnd(fcompo.klass);
      });
  }
}
