import { Injectable } from '@angular/core';
import { GoogleMapsService } from './gmaps-service.service';

@Injectable({
  providedIn: 'root'
})
export class InfowindowService {
  constructor(private mapService: GoogleMapsService) {}

  get map() {
    return this.mapService.getMap();
  }

  info_windows = [];
  private _is_open = false;
  public currently_open_window;

  get is_info_window_open(): boolean {
    return this._is_open;
  }

  setContentAndShow(content, instance, onCloseCallback?) {
    const self = this;
    const infowindow = new google.maps.InfoWindow();
    this.info_windows.push(infowindow);

    infowindow.setContent(content);
    this.closeInfoWindows();

    this._is_open = true;
    this.currently_open_window = infowindow;

    infowindow.open(this.map, instance);
    google.maps.event.addListener(infowindow, 'closeclick', function() {
      self._is_open = false;
      if (onCloseCallback) {
        onCloseCallback();
      }
    });

    google.maps.event.addListenerOnce(this.map, 'click', function() {
      self.closeInfoWindows();
      if (onCloseCallback) {
        onCloseCallback();
      }
    });
  }

  closeInfoWindows() {
    this.info_windows.map((iw: google.maps.InfoWindow) => {
      try {
        iw.close();
      } catch (err) {
        console.error(err);
      }
    });

    this._is_open = false;
  }
}
