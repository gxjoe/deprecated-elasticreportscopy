import { Injectable } from '@angular/core';
import domtoimage from 'dom-to-image';
import { LISTS_OF_HISTOGRAM_CATEGORIES } from '../charts/histogram-table-chart/histogramListsAndCategories';

@Injectable({
  providedIn: 'root',
})
export class DivDownloaderService {
  constructor() { }

  private clean_up_csv(csv_input: string, do_replace_utf_currencies = false) {
    if (!csv_input.match(/^data:text\/csv/i)) {
      csv_input = 'data:text/csv;charset=utf-8,' + csv_input;

      if (do_replace_utf_currencies) {
        const all_currencies =
          LISTS_OF_HISTOGRAM_CATEGORIES.mapOfISOcurreciesToUTF;

        Object.keys(all_currencies).map((iso_val, index) => {
          csv_input = csv_input.replace(
            new RegExp(iso_val, 'gm'),
            all_currencies[iso_val]
          );
        });
      }
    }

    // strip out all html tags
    return csv_input.replace(/<[^>]*>/g, '');
  }

  createLinkElement(filename: string, extension: string, dataurl: string) {
    const link = document.createElement('a');
    link.download = `${filename}.${extension}`;
    link.href = dataurl;

    (document as any).body.append(link);

    return link;
  }

  downloadAsJPG(referringNode: Element, filename: string) {
    return domtoimage.toPng(referringNode).then((dataurl) => {
      const link = this.createLinkElement(filename, 'png', dataurl);
      link.click();
      link.remove();
    });
  }

  downloadNestedCSV(
    referringNode: Element,
    filename: string,
    csv_value?: string,
    rawCsvInput = false
  ) {
    if (referringNode && !csv_value) {
      let corresponding_input =
        referringNode.querySelector(`input[type=hidden].${rawCsvInput ? `rawCsvInput` : `prettifiedCsvInput`}`);

      // no raw nor prettified CSV input found; try the regular hidden input type
      // fixes https://gxperts.atlassian.net/browse/UDGB-50
      if (!corresponding_input) {
        corresponding_input = referringNode.querySelector(`input[type=hidden]`);
      }

      if (!corresponding_input) {
        return;
      }

      csv_value = corresponding_input.getAttribute('value');
    }

    csv_value = this.clean_up_csv(csv_value);
    csv_value = encodeURI(csv_value);

    const link = this.createLinkElement(filename, 'csv', csv_value);

    link.click();
  }

  downloadRegularCSV(referringNode: Element, filename: string) {
    let csv_value = referringNode.getAttribute('csv');

    if (!csv_value) {
      alert('No CSV found.');
      return;
    }

    csv_value = this.clean_up_csv(csv_value);
    csv_value = encodeURI(csv_value);

    const link = this.createLinkElement(filename, 'csv', csv_value);

    link.click();
  }
}
