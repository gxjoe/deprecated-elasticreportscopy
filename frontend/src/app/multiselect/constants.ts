import { mutateObjectProperty, constructNestedExistsQuery } from '../elastic/utils';
import * as Immutable from 'immutable';
import {
  LISTS_OF_HISTOGRAM_CATEGORIES,
} from '../charts/histogram-table-chart/histogramListsAndCategories';
import { constructPerilPath } from '../charts/histogram-table-chart/constants';
import componentNameVsVariableNameMapping from '../utils/constants/componentNameVsVariableNameMapping';
import {
  arrayToObject,
  sortArrOfStringsAlphabetically
} from '../utils/helpers/helpers';
import { AGG_VALUE_FORMATTERS, AGG_VALUE_FORMATTERS_ENUM } from '../charts/histogram-table-chart/queryBuilder';
import { KeyFactorType } from '../interfaces/elastic';
import { LOGICAL_FILTER_MULTISELECT_BEHAVIOR, NULL_ASSESMENT_SCORES_BEHAVIOR } from './enums';

const BASE_OPTIONS = Immutable.fromJS({
  request_body: {
    size: 0,
    aggs: {
      termsAgg: {
        terms: {
          size: 100000,
          order: {
            _count: 'desc'
          },
          field: ''
        }
      }
    }
  }
});

const BASE_OPTIONS_FOR_NESTED_AGGREGATION = Immutable.fromJS({
  request_body: {
    size: 0,
    aggs: {
      termsAgg: {
        nested: {
          path: ''
        },
        aggs: {
          termsAgg: {
            terms: {
              size: 100000,
              order: {
                _key: 'asc' // alphabetical sort
              },
              field: ''
            }
          }
        }
      }
    }
  }
});

const replaceNestedField = field_val => {
  const cp = Object.assign({}, BASE_OPTIONS.toJSON());
  mutateObjectProperty('field', field_val, cp);
  return cp;
};

const replaceMultipleNestedFieldsInNestedAggregation = (
  path,
  field_val,
  include_source?,
  customQuery?,
  customAggs?
) => {
  const cp = Object.assign({}, BASE_OPTIONS_FOR_NESTED_AGGREGATION.toJSON());

  if (!customAggs) {
    mutateObjectProperty('field', field_val, cp);
    mutateObjectProperty('path', path, cp);
  }

  if (customQuery) {
    cp.request_body.query = customQuery;
  }

  if (include_source) {
    cp.request_body.aggs.termsAgg.aggs[
      'include_source'
    ] = includeInnerTopHitsSuffixer(include_source.includes);
  }

  if (customAggs) {
    cp.request_body.aggs = customAggs;
  }

  return cp;
};

const includeInnerTopHitsSuffixer = (includes = []) => {
  return {
    top_hits: {
      size: 1e5,
      _source: {
        includes: includes
      }
    }
  };
};

const aggregateTypeOfBusinessBasedOnIndustryCategory = parent => {
  const industries = LISTS_OF_HISTOGRAM_CATEGORIES.listOfIndustryCategories.slice();

  industries.map(indu => {
    parent.request_body.aggs[`conditional|${indu}`] = {
      filter: {
        term: {
          industry: indu
        }
      },
      aggs: {
        aggs: {
          nested: {
            path: 'lob'
          },
          aggs: {
            aggs: {
              terms: {
                field: 'lob.typeOfBusiness',
                size: 10000,
                order: {
                  _key: 'asc'
                }
              }
            }
          }
        }
      }
    };
  });

  return parent;
};

/**
 * Construct recommendation name & negative aspects aggregations,
 * the results of which whill be used to populate `window.mappers`.
 * Loads of REM-tables are based on the results of these.
 * @param key_factor
 * @returns
 */
const keyCriteriasQueryFn = (key_factor: KeyFactorType) => {
  const location_peril = (window as any).selected_location_peril;

  if (!location_peril) {
    throw new Error('Location Peril does not exist');
  }

  const field_base = `lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor}`;

  return replaceMultipleNestedFieldsInNestedAggregation('', '', null, null, {
    termsAgg: {
      nested: {
        path: `${field_base}.keyCriteriaAsList`
      },
      aggs: {
        termsAgg: {
          terms: {
            field: `${field_base}.keyCriteriaAsList.name.keyword`,
            size: 200
          },
          aggs: {
            terms: {
              nested: {
                path: `${field_base}.keyCriteriaAsList.recommendations`
              },
              aggs: {
                terms: {
                  terms: {
                    field: `${field_base}.keyCriteriaAsList.recommendations.name.keyword`,
                    // 2K should be enough -- while the recommendations are highly individual,
                    // their names are rather standardized. The `description` field on the same level
                    // tends be more customized.
                    size: 2e3
                  }
                }
              }
            },
            negativeAspects: {
              nested: {
                path: `${field_base}.keyCriteriaAsList.negativeAspects.recommendations`
              },
              aggs: {
                negativeAspects: {
                  terms: {
                    field: `${field_base}.keyCriteriaAsList.negativeAspects.recommendations.negativeAspect.keyword`,
                    // same principle here w/ 2K as above
                    size: 2e3
                  }
                }
              }
            }
          }
        }
      }
    }
  });
};

const mapAndCombineToOptions = (arr: Array<string>) => {
  return arr.map(item => {
    return { label: item, value: item };
  });
};

/**
 * Extracts the risk classes into a html-ready `customHtmlLabelSuffix`.
 * @param tobs
 * @returns
 */
export const prettifyTypeOfBusinessRiskClasses = (tobs: string[]) => {
  return sortArrOfStringsAlphabetically(tobs).map(
    (tob: string) => {
      const original = tob;
      const normalized = AGG_VALUE_FORMATTERS[AGG_VALUE_FORMATTERS_ENUM.NORMALIZE_TYPE_OF_BUSINESS](tob);

      const r = /( risk \d)/gi;
      const risk = normalized.match(r)?.[0];
      const risk_class_int = parseInt(risk?.match(/\d/)[0], 10);

      return {
        label: normalized,
        value: original,
        customHtmlLabelSuffix:
          `<span class="with_pretty_risk_class">
            ${normalized}
            ${risk
            ? `<span class="risk_class_${risk_class_int}">
                  <span class="num_itself">${risk_class_int}</span>
                </span>`
            : ``
          }
          </span>`
      };
    }
  );
};
export const MULTISELECT_HARDCODED_OPTIONS = {
  LocationAssessmentLevel3_KeyFactorKind_Multiselect: mapAndCombineToOptions([
    'probability',
    'prevention',
    'potential'
  ]),
  LocationAssessmentLevel3_IgnoreNullScores_Multiselect: mapAndCombineToOptions([
    NULL_ASSESMENT_SCORES_BEHAVIOR.KEEP_EMPTY,
    NULL_ASSESMENT_SCORES_BEHAVIOR.EXCLUDE_EMPTY,
    NULL_ASSESMENT_SCORES_BEHAVIOR.ONLY_EMPTY,
  ]),
  International_Business_Multiselect_LogicalFilter: mapAndCombineToOptions([
    LOGICAL_FILTER_MULTISELECT_BEHAVIOR.INCLUDE,
    LOGICAL_FILTER_MULTISELECT_BEHAVIOR.EXCLUDE_IF_EXISTS,
    LOGICAL_FILTER_MULTISELECT_BEHAVIOR.EXCLUDE,
  ]),
  ESGCriterion_Multiselect: mapAndCombineToOptions(LISTS_OF_HISTOGRAM_CATEGORIES.listOfEsgCriterias),
  ESGRiskCode_Multiselect: mapAndCombineToOptions([
    "0", "1", "2", "3"
  ])
};

export const MULTISELECT_OPTIONS = {
  CB_or_SMB_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer',
    'offer.cbOrSme'
  ),
  Cross_Border_Businesss_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer',
    'offer.crossBorderBusiness'
  ),
  LoB_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'lob',
    'lob.lineOfBusiness'
  ),
  EUM_ResponsibleLocalUnderwriterMultiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'lob.localUnderwriters',
    'lob.localUnderwriters.fullName',
    null,
    null,
    {
      termsAgg: {
        nested: {
          path: 'lob.localUnderwriters'
        },
        aggs: {
          termsAgg: {
            terms: {
              size: 100000,
              order: {
                _key: 'asc'
              },
              field: 'lob.localUnderwriters.fullName'
            },
            aggs: {
              uname: {
                terms: {
                  field: 'lob.localUnderwriters.username',
                  size: 1
                }
              }
            }
          }
        }
      }
    }
  ),
  DistributionChannel_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer',
    'offer.distributionChannel'
  ),
  OfferStatus_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'lob',
    'lob.status'
  ),
  AccountStatus_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'lob',
    'lob.accountStatus'
  ),
  RiskClass_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer',
    'offer.Risk Class'
  ),
  Peril_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'lob.rating.ratingToolPerils',
    'lob.rating.ratingToolPerils.peril'
  ),
  PerilRiskClass_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'lob.rating.ratingToolPerils',
    'lob.rating.ratingToolPerils.peril'
  ),
  LocationPeril_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'address.locationProperty.locationAssessments',
    'address.locationProperty.locationAssessments.peril'
  ),
  LocationPerilRiskClass_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    `address.locationProperty.locationAssessments.${constructPerilPath('')}`,
    'address.locationProperty.locationAssessments.riskClass'
  ),

  RegionOfOrigin_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer',
    'offer.regionalDirectorate'
  ),
  SourceSystem_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer',
    'offer.sourceSystem.keyword'
  ),
  Region_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer',
    'offer.region'
  ),
  Country_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer',
    'offer.requestFromCountry'
  ),
  City_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer',
    'offer.city'
  ),
  Creator_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer',
    'offer.Creator'
  ),
  NaceCodesStandalone_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer.client',
    'offer.client.nacePrimaryCodes',
    null,
    null,
    {
      termsAgg: {
        nested: {
          path: 'offer'
        },
        aggs: {
          termsAgg: {
            terms: {
              field: 'offer.industry',
              size: 100
            },
            aggs: {
              termsAgg: {
                nested: {
                  path: 'offer.client'
                },
                aggs: {
                  termsAgg: {
                    terms: {
                      size: 1000,
                      field: 'offer.client.nacePrimaryCodes'
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  ),
  LoBRisk_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'lob.risks',
    'lob.risks.risk',
    null,
    null,
    {
      termsAgg: {
        nested: {
          path: 'lob.risks'
        },
        aggs: {
          termsAgg: {
            terms: {
              field: 'lob.risks.risk',
              size: 10e3
            }
          }
        }
      }
    }
  ),
  LoB_Peril_RiskDescription_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'lob.risks.perils',
    'lob.risks.perils.riskDescription',
    null,
    null,
    {
      termsAgg: {
        nested: {
          path: 'lob.risks.perils'
        },
        aggs: {
          termsAgg: {
            terms: {
              field: 'lob.risks.perils.riskDescription',
              size: 10e3
            }
          }
        }
      }
    }
  ),
  Industry_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer',
    'offer.industry'
  ),
  International_Business_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer',
    'offer.internationalProgram'
  ),
  TypeOfBusiness_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'lob',
    'lob.typeOfBusiness',
    null,
    null,
    {
      termsAgg: {
        nested: {
          path: 'offer'
        },
        aggs: {
          termsAgg: {
            terms: {
              field: 'offer.industry',
              size: 100
            },
            aggs: {
              termsAgg: {
                nested: {
                  path: 'lob'
                },
                aggs: {
                  termsAgg: {
                    terms: {
                      size: 1000,
                      field: 'lob.typeOfBusiness'
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  ),
  SalesManager_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer.salesManagers',
    'offer.salesManagers.fullName',
    null,
    null,
    {
      termsAgg: {
        nested: {
          path: 'offer.salesManagers'
        },
        aggs: {
          termsAgg: {
            terms: {
              field: 'offer.salesManagers.fullName',
              size: 10e3
            }
          }
        }
      }
    }
  ),
  SalesPartner_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'lob.salesPartner',
    'lob.salesPartner.name',
    null,
    null,
    {
      termsAgg: {
        nested: {
          path: 'lob.salesPartner'
        },
        aggs: {
          termsAgg: {
            terms: {
              field: 'lob.salesPartner.name',
              size: 500
            }
          }
        }
      }
    }
  ),
  Public_Tender_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer',
    'offer.publicTender'
  ),
  RiskEngineer_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'address.locationProperty',
    'address.locationProperty.globalLocationDataCreatedByFullName.keyword',
    {
      includes: [
        'address.locationProperty.globalLocationDataCreatedByFullName',
        'address.locationProperty.globalLocationDataCreatedBy'
      ]
    },
    {
      bool: {
        must: [
          {
            bool: {
              filter: [
                constructNestedExistsQuery(
                  'address.locationProperty',
                  'address.locationProperty.globalLocationDataCreatedByFullName'
                )
              ]
            }
          }
        ]
      }
    }
  ),
  UCB_Confirmation_needed_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer',
    'offer.uicbConfirmNeeded'
  ),
  UCB_Involved_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer',
    'offer.uicbInvolved'
  ),
  UNIQA_Leading_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'lob',
    'lob.uniqaLeading'
  ),

  // Building & Materials
  BuildingType_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'address.locationProperty',
    'address.locationProperty.buildingType'
  ),
  RoofGeometry_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'address.locationProperty',
    'address.locationProperty.roofGeometry'
  ),
  RoofSystem_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'address.locationProperty',
    'address.locationProperty.roofSystem'
  ),
  ConstructionMaterial_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'address.locationProperty',
    'address.locationProperty.constructionMaterial'
  ),
  ShapeOfBuilding_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'address.locationProperty',
    'address.locationProperty.shapeOfBuilding'
  ),

  // for Window Aggs Only
  RecommendationKinds_forGlobalWindowAggsOnly_Multiselect: () => {
    const location_peril = (window as any).selected_location_peril;
    const selected_key_factor_kind = (window as any).selected_key_factor_kind;

    if (!location_peril) {
      throw new Error('location peril does not exist');
    }

    return replaceMultipleNestedFieldsInNestedAggregation('', '', null, null, {
      termsAgg_probability_merge: {
        nested: {
          path: `lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.probability.keyCriteriaAsList.recommendations`
        },
        aggs: {
          termsAgg: {
            terms: {
              field: `lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.probability.keyCriteriaAsList.recommendations.name.keyword`,
              size: 10e3
            }
          }
        }
      },
      termsAgg2_prevention_merge: {
        nested: {
          path: `lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.prevention.keyCriteriaAsList.recommendations`
        },
        aggs: {
          termsAgg: {
            terms: {
              field: `lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.prevention.keyCriteriaAsList.recommendations.name.keyword`,
              size: 10e3
            }
          }
        }
      },
      termsAgg3_potential_merge: {
        nested: {
          path: `lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.potential.keyCriteriaAsList.recommendations`
        },
        aggs: {
          termsAgg: {
            terms: {
              field: `lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.potential.keyCriteriaAsList.recommendations.name.keyword`,
              size: 10e3
            }
          }
        }
      }
    });
  },
  RecommendationKindsNegativeConditions_forGlobalWindowAggsOnly_Multiselect: () => {
    const location_peril = (window as any).selected_location_peril;

    if (!location_peril) {
      throw new Error('location peril does not exist');
    }

    return replaceMultipleNestedFieldsInNestedAggregation('', '', null, null, {
      termsAgg: {
        nested: {
          path: `lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyCriteria.negativeAspects.recommendations`
        },
        aggs: {
          termsAgg: {
            terms: {
              field: `lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyCriteria.negativeAspects.recommendations.negativeAspect.keyword`,
              size: 10e3
            }
          }
        }
      }
    });
  },
  ProbabilityKeyCriterias_forGlobalWindowAggsOnly_Multiselect: [
    keyCriteriasQueryFn,
    'probability'
  ],
  PotentialKeyCriterias_forGlobalWindowAggsOnly_Multiselect: [
    keyCriteriasQueryFn,
    'potential'
  ],
  PreventionKeyCriterias_forGlobalWindowAggsOnly_Multiselect: [
    keyCriteriasQueryFn,
    'prevention'
  ],

  AssessmentRecommendations_Multiselect: () => {
    const location_peril = (window as any).selected_location_peril;
    const key_factor_kind = (window as any).selected_key_factor_kind;
    const key_criterion = (window as any).selected_key_criterion;

    if (!location_peril || !key_factor_kind || !key_criterion) {
      throw new Error('!location_peril || !key_factor_kind || !key_criterion');
    }

    return replaceMultipleNestedFieldsInNestedAggregation(
      null,
      null,
      null,
      null,
      {
        termsAgg: {
          filter: {
            nested: {
              path: `lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteria.${key_criterion}`,
              query: {
                term: {
                  [`lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteria.${key_criterion}.name.keyword`]: {
                    value: key_criterion
                  }
                }
              }
            }
          },
          aggs: {
            termsAgg: {
              nested: {
                path: `lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteria.${key_criterion}.recommendations`
              },
              aggs: {
                termsAgg: {
                  terms: {
                    field: `lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteria.${key_criterion}.recommendations.name.keyword`,
                    size: 1e3
                  }
                }
              }
            }
          }
        }
      }
    );
  },

  NegativeAspects_forGlobalWindowAggsOnly_Multiselect: () => {
    const location_peril = (window as any).selected_location_peril;
    const key_factor_kind = (window as any).selected_key_factor_kind;
    const key_criterion = (window as any).selected_key_criterion;
    const applicable_recommendations = (window as any)
      .applicable_assessment_recommendations;

    if (
      !location_peril ||
      !key_factor_kind ||
      !key_criterion ||
      !applicable_recommendations ||
      !applicable_recommendations.length
    ) {
      throw new Error(
        '!location_peril || !key_factor_kind || !key_criterion || !applicable_recommendations'
      );
    }

    return replaceMultipleNestedFieldsInNestedAggregation(
      null,
      null,
      null,
      null,
      {
        termsAgg: {
          filter: {
            nested: {
              path: `lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteria.${key_criterion}`,
              query: {
                term: {
                  [`lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteria.${key_criterion}.name.keyword`]: {
                    value: key_criterion
                  }
                }
              }
            }
          },
          aggs: {
            termsAgg: {
              nested: {
                path: `lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteria.${key_criterion}.negativeAspects.recommendations`
              },
              aggs: {
                termsAgg: {
                  terms: {
                    field: `lob.addresses.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteria.${key_criterion}.negativeAspects.recommendations.negativeAspect.keyword`,
                    size: 1e3
                  }
                }
              }
            }
          }
        }
      }
    );
  },

  TypeOfBusiness_SemiStatic_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'lob',
    'lob.typeOfBusiness'
  ),

  IndustryVsTypeOfBusiness_forGlobalWindowAggsOnly_Multiselect: () => {
    return replaceMultipleNestedFieldsInNestedAggregation(
      null,
      null,
      null,
      null,
      {
        termsAgg: {
          filter: {
            bool: {
              must: [
                // no "empty" industries -- believe or not, there are some...
                {
                  nested: {
                    path: 'offer',
                    query: {
                      bool: {
                        must_not: [
                          {
                            term: {
                              'offer.industry': ''
                            }
                          }
                        ]
                      }
                    }
                  }
                }
              ]
            }
          },
          aggs: {
            termsAgg: {
              nested: {
                path: 'offer'
              },
              aggs: {
                termsAgg: {
                  terms: {
                    field: 'offer.industry',
                    size: 1e3,
                    order: {
                      _term: 'asc' // [01.., 02.., 03..]
                    }
                  },
                  aggs: {
                    backToTop: {
                      reverse_nested: {},
                      aggs: {
                        termsAgg: {
                          nested: {
                            path: 'lob'
                          },
                          aggs: {
                            termsAgg: {
                              terms: {
                                field: 'lob.typeOfBusiness',
                                size: 1000,
                                order: {
                                  _term: 'asc'
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    );
  },
  NACECodes_forGlobalWindowAggsOnly_Multiselect: replaceMultipleNestedFieldsInNestedAggregation(
    'offer.client',
    'offer.client.nacePrimaryCodes'
  )
};

export const tidyUpBuckets = bucks => bucks.filter(b => b.key.length > 0);

export const MULTISELET_BUCKETS_AND_INNERHITS_CACHERS = {
  EUM_ResponsibleLocalUnderwriterMultiselect: buckets => {
    (window as any).mappers = (window as any).mappers || {};
    const mapping = {};
    const reverse_mapping = {};

    buckets.map(hit => {
      const _hit = {
        username: hit.uname.buckets[0].key,
        fullName: hit.key
      };

      if (!(_hit.fullName in mapping)) {
        mapping[_hit.fullName] = _hit.username;
      }

      if (!(_hit.username in reverse_mapping)) {
        reverse_mapping[_hit.username] = _hit.fullName;
      }
    });
    (window as any).mappers['Underwriters'] = mapping;
    (window as any).mappers['UnderwritersUsernames'] = reverse_mapping;
  },
  LoB_Multiselect: inner_hits => {
    const mapping = {};
    (window as any).mappers = (window as any).mappers || {};

    // inner_hits.hits.hits.map(hit => {
    //   const s = hit._source;
    //   if (!(s.lineOfBusiness in mapping)) {
    //     mapping[s.lineOfBusiness] = s.lineOfBusiness;
    //   }
    // });
    tidyUpBuckets(inner_hits).map(buck => {
      const s = buck.key;
      if (!(s in mapping)) {
        mapping[s] = s;
      }
    });
    (window as any).mappers['LoBs'] = mapping;
  },
  Industry_Multiselect: buckets => {
    const mapping = {};
    (window as any).mappers = (window as any).mappers || {};
    tidyUpBuckets(buckets).map(buck => {
      const s = buck.key;
      if (!(s in mapping)) {
        mapping[s] = s;
      }
    });
    (window as any).mappers['industry'] = mapping;
  },
  SalesPartner_Multiselect: buckets => {
    const mapping = {};
    (window as any).mappers = (window as any).mappers || {};
    tidyUpBuckets(buckets).map(buck => {
      const s = buck.key;
      if (!(s in mapping)) {
        mapping[s] = s;
      }
    });
    (window as any).mappers['SalesPartner'] = mapping;
  },
  RiskEngineer_Multiselect: inner_hits => {
    (window as any).mappers = (window as any).mappers || {};
    const mapping = {};
    const reverse_mapping = {};

    inner_hits.hits.hits.map(hit => {
      const s = hit._source;
      if (!(s.globalLocationDataCreatedByFullName in mapping)) {
        mapping[s.globalLocationDataCreatedByFullName] = s.globalLocationDataCreatedBy;
      }

      if (!(s.globalLocationDataCreatedBy in reverse_mapping)) {
        reverse_mapping[s.globalLocationDataCreatedBy] = s.globalLocationDataCreatedByFullName;
      }
    });
    (window as any).mappers['RiskEngineers'] = mapping;
    (window as any).mappers['RiskEngineersUsernames'] = reverse_mapping;
  },
  RegionOfOrigin_Multiselect: buckets => {
    const mapping = {};
    (window as any).mappers = (window as any).mappers || {};
    tidyUpBuckets(buckets).map(buck => {
      const s = buck.key;
      if (!(s in mapping)) {
        mapping[s] = s;
      }
    });
    (window as any).mappers['RegionOfOrigin'] = mapping;
  },
  AssessmentRecommendations_Multiselect: (buckets, app_component) => {
    app_component.getComponentByKlass(
      'AssessmentRecommendations_Multiselect'
    ).listOfOptions = mapAndCombineToOptions(buckets.map(b => b.key));
  },
  ConstructionMaterial_Multiselect: buckets => {
    const mapping = {};
    (window as any).mappers = (window as any).mappers || {};
    tidyUpBuckets(buckets).map(buck => {
      const s = buck.key;
      if (!(s in mapping)) {
        mapping[s] = s;
      }
    });
    (window as any).mappers['ConstructionMaterials'] = mapping;
  },

  NegativeAspects_forGlobalWindowAggsOnly_Multiselect: buckets => {
    (window as any).list_of_negative_aspects = buckets.map(b => b.key);
  },

  IndustryVsTypeOfBusiness_forGlobalWindowAggsOnly_Multiselect: (buckets: Array<any>) => {
    (window as any).mappers = (window as any).mappers || {};
    (window as any).mappers.industries_vs_typesOfBusinesses = buckets?.map(bucket => {
      return {
        industry: bucket.key,
        typesOfBusiness: tidyUpBuckets(bucket.backToTop?.termsAgg?.termsAgg?.buckets)?.map(b => b.key)
      };
    });
  },

  ...arrayToObject(
    Object.keys(componentNameVsVariableNameMapping)
      .filter(
        key =>
          key.includes('forGlobalWindowAggsOnly') &&
          !key.includes('NegativeAspects') &&
          !key.includes('IndustryVsTypeOfBusiness')
      )
      .map(key => {
        return {
          key: key,
          func: (buckets, app_component) => {
            const mapping = {};
            (window as any).mappers = (window as any).mappers || {};

            if (buckets.length && buckets[0].criterion) {
              (window as any).mappers[key.split('_')[0]] = buckets;
              return;
            }

            tidyUpBuckets(buckets).map(buck => {
              const s = buck.key;
              if (!(s in mapping)) {
                mapping[s] = s;
              }
            });
            (window as any).mappers[key.split('_')[0]] = Object.keys(mapping);

            const selected_key_factor_kind = (window as any)
              .selected_key_factor_kind;
            if (!selected_key_factor_kind) {
              return;
            }

            const extracted_key = key
              .split('_')[0]
              .split(/(?=[A-Z])/)
              .filter(s => s.length)[0]
              .toLowerCase();

            if (selected_key_factor_kind === extracted_key) {
              app_component.getComponentByKlass(
                'KeyCriterion_Multiselect'
              ).listOfOptions = mapAndCombineToOptions(Object.keys(mapping));
            }
          }
        };
      }),
    'key',
    'func'
  )
};

export const COMPONENT_KEYS_TO_ALWAYS_UPDATE = (current_route?: string) => {
  if (!current_route) {
    current_route = window.location.pathname;
  }

  let base_arr = [
    'NaceCodesStandalone_Multiselect',
    'RegionOfOrigin_Multiselect',
    'SalesPartner_Multiselect',
    'SalesManager_Multiselect'
    // 'TypeOfBusiness_Multiselect'
  ];

  if (current_route && current_route.includes('risk')) {
    base_arr = []
      .concat(base_arr)
      .concat([
        'AssessmentRecommendations_Multiselect',
        ...Object.keys(componentNameVsVariableNameMapping).filter(key =>
          key.includes('forGlobalWindowAggsOnly')
        )
      ]);
  } else if (current_route?.includes('product_portfolio')) {
    base_arr = []
      .concat(base_arr)
      // introduced in UDGB-17 -- the *NoCanvas* charts' row names
      // should always be kept up-to-date
      .concat(['IndustryVsTypeOfBusiness_forGlobalWindowAggsOnly_Multiselect'])
      .concat(['NACECodes_forGlobalWindowAggsOnly_Multiselect']);
  }

  return base_arr;
};
