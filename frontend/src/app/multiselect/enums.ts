// For whatever reasons, some locations may contain location peril assessments
// but those may not be scored. The user (more spec. the risk engineer) may
// want to filter such addresses out. Vice versa, they may want to *exclusively*
// include only those and get a picture of what such entities look like.
// Implements UDGB-34.
export enum NULL_ASSESMENT_SCORES_BEHAVIOR {
    KEEP_EMPTY = 'Keep empty (default)',
    EXCLUDE_EMPTY = 'Exclude empty',
    ONLY_EMPTY = 'Only empty',
}

export enum LOGICAL_FILTER_MULTISELECT_BEHAVIOR {
    INCLUDE = 'Include (default)',
    EXCLUDE = 'Exclude',
    EXCLUDE_IF_EXISTS = 'Exclude only if exists',
}
