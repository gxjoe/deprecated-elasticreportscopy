import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  OnDestroy,
  ChangeDetectionStrategy,
} from '@angular/core';
import { ElasticService } from '../services/elastic.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  MULTISELECT_OPTIONS,
  MULTISELET_BUCKETS_AND_INNERHITS_CACHERS,
  COMPONENT_KEYS_TO_ALWAYS_UPDATE,
  MULTISELECT_HARDCODED_OPTIONS,
  tidyUpBuckets,
  prettifyTypeOfBusinessRiskClasses,
} from './constants';
import { FilterComponent } from '../utils/higher-order/FilterComponent';
import IFilterComponentQueryMeta from '../utils/higher-order/IFilterComponentQueryMeta';
import componentNameVsVariableNameMapping from '../utils/constants/componentNameVsVariableNameMapping';
import {
  ElasticFilterSubtypes,
  ElasticQueryClauses,
} from '../elastic/constants';
import { SearchResponse } from 'elasticsearch';
import { regexMatches, mergeDeep, isFunction } from '../utils/helpers/helpers';
import {
  findByKeyInNestedObject,
  constructCombinedLocationPerilQuery,
} from '../elastic/utils';
import { QueryService } from '../services/query.service';
import { LISTS_OF_HISTOGRAM_CATEGORIES } from '../charts/histogram-table-chart/histogramListsAndCategories';
import {
  constructPerilPath,
  createRangeParametersFromRiskClassInput,
} from '../charts/histogram-table-chart/constants';
import { AuthService } from '../services/auth.service';
import { NULL_ASSESMENT_SCORES_BEHAVIOR } from './enums';

interface ILabelValuePair {
  label: string;
  value: string;
}

interface IGroupedOption {
  group_name: string;
  options: ILabelValuePair[];
}

export interface IArrayOfOptions
  extends Array<ILabelValuePair & IGroupedOption> { }

@Component({
  selector: 'app-multiselect',
  templateUrl: './multiselect.component.html',
  styleUrls: ['./multiselect.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiselectComponent
  extends FilterComponent
  implements OnInit, OnDestroy {
  @Input()
  multiple;
  @Input()
  placeholder;
  @Input()
  klass: string;
  @Input()
  heading;
  isMultiple: Boolean;

  @Input() isNew: Boolean;

  @Input() defaultSelection?: any[] = [];

  listOfOptions: IArrayOfOptions = [];
  virtualListOfOptions: IArrayOfOptions = [];

  listOfSelectedValues = this.defaultSelection.slice();

  selectedUser;
  isLoading = false;
  prefetchOptions: Observable<ILabelValuePair[]>;

  private elastic_variable: string;
  private currentIndex = 0;

  /* AllChecked handlers */
  allColumnsChecked = false;
  indeterminateColumns = false;

  updateAllCheckedValues(): void {
    this.indeterminateColumns = false;
    if (this.allColumnsChecked) {
      this.listOfSelectedValues = this.listOfOptions.map((opt) => opt.value);
    } else {
      this.listOfSelectedValues = [];
    }

    this.onChange(null);
  }

  get valueOf() {
    return this.listOfSelectedValues;
  }

  get isGrouped() {
    return (
      (this.listOfOptions.length &&
        (this.listOfOptions[0] as IGroupedOption).group_name) ||
      (this.virtualListOfOptions.length &&
        (this.virtualListOfOptions[0] as IGroupedOption).group_name)
    );
  }

  constructor(
    private es: ElasticService,
    private qs: QueryService,
    private auth: AuthService
  ) {
    super();
  }

  next20(): IArrayOfOptions {
    if (this.listOfOptions.length < this.currentIndex) {
      return [];
    }

    const next = [...this.virtualListOfOptions].slice(
      this.currentIndex,
      this.currentIndex + 20
    );
    this.currentIndex += 20;

    return next;
  }

  loadMore($event): void {
    this.listOfOptions = [...this.listOfOptions, ...this.next20()];
  }

  prefetchOptions_(query_filter?) {
    let request_body_itself;

    if (this.klass in MULTISELECT_HARDCODED_OPTIONS) {
      this.isLoading = false;
      if (isFunction(MULTISELECT_HARDCODED_OPTIONS[this.klass])) {
        // pass. Will be applied directly in onChange of the corresponding linked multiselect
      } else {
        this.listOfOptions = MULTISELECT_HARDCODED_OPTIONS[this.klass];
      }
      return;
    }

    try {
      if (isFunction(MULTISELECT_OPTIONS[this.klass])) {
        request_body_itself = MULTISELECT_OPTIONS[this.klass]().request_body;
      } else if (Array.isArray(MULTISELECT_OPTIONS[this.klass])) {
        // the first param is a function, the rest are params so apply a sliced param array to the fn
        request_body_itself = MULTISELECT_OPTIONS[this.klass][0].apply(
          null,
          MULTISELECT_OPTIONS[this.klass].slice(1)
        ).request_body;
      } else {
        request_body_itself = MULTISELECT_OPTIONS[this.klass].request_body;
      }
    } catch (err) {
      console.debug(err);
      return;
    }

    const request_body = mergeDeep(request_body_itself, true, {
      query: {
        bool: {
          filter: query_filter || [],
        },
      },
    });

    this.prefetchOptions = this.es
      ._constructRequest('post', request_body)
      .pipe(
        map((res: any | { response: SearchResponse<any> }) => {
          if (res.response) {
            res = res.response;
          }
          // save the inner hits or terms buckets because they will be used as reference for querying, say,
          // underwriters listed by username vs full name
          if (
            res.aggregations.termsAgg &&
            res.aggregations.termsAgg.include_source
          ) {
            MULTISELET_BUCKETS_AND_INNERHITS_CACHERS[this.klass](
              res.aggregations.termsAgg.include_source
            );
          } else if (this.klass in MULTISELET_BUCKETS_AND_INNERHITS_CACHERS) {
            let buckets = [];

            if (
              Object.keys(res.aggregations).some((k) => k.includes('merge'))
            ) {
              Object.keys(res.aggregations).map((subagg_key) => {
                buckets.push({
                  criterion: subagg_key.split('_')[1],
                  buckets: tidyUpBuckets(
                    res.aggregations[subagg_key].termsAgg.buckets
                  ).map((b) => b.key),
                });
              });
            }

            if (this.klass.includes('KeyCriterias')) {
              (<any>window).mappers[this.klass.split('_')[0] + '_raw'] =
                res.aggregations.termsAgg.termsAgg.buckets.map((bucket) => {
                  return {
                    criterion: bucket.key,
                    recommendations: bucket.terms.terms.buckets.map(
                      (b) => b.key
                    ),
                    negativeAspects:
                      bucket.negativeAspects.negativeAspects.buckets.map(
                        (b) => b.key
                      ),
                  };
                });

              // console.warn(this.klass.split('_')[0] + '_raw', (<any>window).mappers[this.klass.split('_')[0] + '_raw']);
            }

            if (!buckets.length) {
              try {
                buckets = findByKeyInNestedObject(
                  res.aggregations,
                  'buckets'
                ).buckets;
              } catch (err) {
                console.warn({
                  err: err,
                  klass: this.klass,
                  buckets: res.aggregations,
                  res,
                });
                buckets = [];
              }
            }

            MULTISELET_BUCKETS_AND_INNERHITS_CACHERS[this.klass](
              buckets,
              this.es.app_component
            );
          }

          if (
            this.klass === 'Peril_Multiselect' ||
            this.klass === 'LocationPeril_Multiselect'
          ) {
            // UDGB-14 Let's see if we can replace the hard-coded
            // list of peril names w/ results from the corresponding ES aggs
            if (res?.aggregations?.termsAgg?.termsAgg?.buckets?.length) {
              LISTS_OF_HISTOGRAM_CATEGORIES.listOfPerilNames =
                res?.aggregations?.termsAgg?.termsAgg?.buckets?.map(
                  (bucket) => bucket.key
                );
            }

            return LISTS_OF_HISTOGRAM_CATEGORIES.listOfPerilNames.map((v) => {
              return {
                label: v,
                key: v,
              };
            });
          }

          if (
            this.klass === 'PerilRiskClass_Multiselect' ||
            this.klass === 'LocationPerilRiskClass_Multiselect'
          ) {
            return LISTS_OF_HISTOGRAM_CATEGORIES.listOfPerilRiskClasses.map(
              (v) => {
                return {
                  label: v,
                  key: v,
                };
              }
            );
          }

          console.debug(request_body, this.klass, res);

          if (res.aggregations.termsAgg) {
            if (res.aggregations.termsAgg.termsAgg) {
              return res.aggregations.termsAgg.termsAgg.buckets;
            } else {
              return res.aggregations.termsAgg.buckets;
            }
          } else {
            return [];
          }
        })
      )
      .pipe(
        map((list: any) => {
          if (!list) {
            return [];
          }

          if (
            this.klass === 'TypeOfBusiness_Multiselect' ||
            this.klass === 'NaceCodesStandalone_Multiselect'
          ) {
            return list.map((group) => {
              return {
                group_name: group.key,
                options: group.termsAgg.termsAgg.buckets.map((bucket) => {
                  return {
                    label: `${bucket.key}`,
                    value: `${bucket.key}`,
                  };
                }),
              };
            });
          }

          // UDGB-28
          if (this.klass === 'TypeOfBusiness_SemiStatic_Multiselect') {
            return prettifyTypeOfBusinessRiskClasses(
              list.map((group) => group.key)
            );
          }

          return list.map((item) => {
            return {
              label: `${item.key_as_string ? item.key_as_string : item.key}`,
              value: `${item.key_as_string ? item.key_as_string : item.key}`,
            };
          });
        })
      );
  }

  ngOnInit() {
    this.es.app_component.setComponent(this.klass, this);
    if (!(this.klass in MULTISELECT_HARDCODED_OPTIONS) || this.klass.includes('ESG')) {
      this.es.app_component.filterComponents.push(this);
    }

    this.isMultiple = this.multiple === 'true';

    if (this.klass in componentNameVsVariableNameMapping) {
      this.elastic_variable = componentNameVsVariableNameMapping[this.klass];
    }

    setTimeout(() => {
      this.prefetchOptions_();
      this.auth.doCheckAuth().then(() => this.preload());
    }, 3500);
  }

  preload(progress?: number, opts?: { force_real_options: boolean }): void {
    this.isLoading = true;

    if (!this.prefetchOptions) {
      this.isLoading = false;
      return;
    }

    this.prefetchOptions.subscribe((data) => {
      this.isLoading = false;
      if (typeof progress !== 'undefined') {
        this.es.percentualProgress.next(100 || progress);
      }

      if (!data || !data.length) {
        return;
      }

      if (
        !COMPONENT_KEYS_TO_ALWAYS_UPDATE().includes(this.klass) ||
        (opts && opts.force_real_options)
      ) {
        this.listOfOptions = <IArrayOfOptions>[...data];
      } else {
        this.virtualListOfOptions = <IArrayOfOptions>[...data];
      }
    });
  }

  filterQueryBuilder = (): IFilterComponentQueryMeta => {
    const b = document.querySelector('body') as HTMLBodyElement;

    let build: IFilterComponentQueryMeta;
    const empty_build: IFilterComponentQueryMeta = {
      klass: this.klass,
      elastic_variable: this.elastic_variable,
      subtype: ElasticFilterSubtypes.term,
      operators: null,
      values: [],
      clause: ElasticQueryClauses.must,
      nested: this.elastic_variable.includes('.'),
      finished_query: null,
    };

    build = Object.assign({}, empty_build);

    if (!this.valueOf || !this.valueOf.length) {
      if (this.klass === 'Peril_Multiselect') {
        Array.from(b.classList).forEach((v) => {
          if (regexMatches(/peril_/g, v)) {
            b.classList.remove(v);
          }
        });
      }

      if (this.klass === 'LocationAssessmentLevel3_KeyFactorKind_Multiselect') {
        if (!build.values.length) {
          (window as any).selected_key_factor_kind = null;
        }
      }

      if (
        this.klass === 'LocationAssessmentLevel3_IgnoreNullScores_Multiselect'
      ) {
        if (!build.values.length) {
          (window as any).ignore_null_assessment_scores =
            NULL_ASSESMENT_SCORES_BEHAVIOR.KEEP_EMPTY;
        }
      }

      if (this.klass.endsWith('LogicalFilter')) {
        if (!build.values.length) {
          delete (window as any)[this.klass];
        }
      }

      if (this.klass === 'ESGRiskCode_Multiselect')
        (window as any).selected_esg_riskCode = null;

      if (this.klass === 'ESGCriterion_Multiselect')
        (window as any).selected_esg_criterion = null;

      return build;
    }

    build.values = Array.isArray(this.valueOf) ? this.valueOf : [this.valueOf];

    if (this.klass === 'Peril_Multiselect') {
      Array.from(b.classList).forEach((v) => {
        if (regexMatches(/peril_/g, v)) {
          b.classList.remove(v);
        }
      });
      if (build.values.length) {
        b.classList.add(`peril_${build.values[0]}`);
        (window as any).selected_peril = build.values[0];
      } else {
        (window as any).selected_peril = null;
        return empty_build;
      }

      // build the finished query because the Peril_Multiselect is a single-select
      // bo bool:should are needed
      build = empty_build;
      build.finished_query = {
        nested: {
          path: constructPerilPath(),
          query: {
            bool: {
              must: [
                {
                  term: {
                    [constructPerilPath('peril')]: (window as any)
                      .selected_peril,
                  },
                },
              ],
            },
          },
        },
      };

      return build;
    }

    if (this.klass === 'PerilRiskClass_Multiselect') {
      const sorted_values = build.values.sort().map(parseFloat);

      build = empty_build;
      build.finished_query = {
        bool: {
          should: sorted_values.map((build_val) => {
            return {
              nested: {
                path: constructPerilPath(),
                query: {
                  bool: {
                    must: [
                      {
                        term: {
                          [constructPerilPath('peril')]: (window as any)
                            .selected_peril,
                        },
                      },
                      {
                        range: {
                          [constructPerilPath('avgRiskClass')]:
                            createRangeParametersFromRiskClassInput(build_val),
                        },
                      },
                    ],
                  },
                },
              },
            };
          }),
          minimum_should_match: 1,
        },
      };

      return build;
    }

    if (this.klass === 'ESGCriterion_Multiselect') {
      if (build.values.length) {
        (window as any).selected_esg_criterion = build.values[0];
        build = empty_build;
        build.finished_query = {
          nested: {
            path: "offer.client.esg.criteria",
            query: {
              bool: {
                should: (window as any).selected_esg_riskCode ? (window as any).selected_esg_riskCode.map((riskCode) => ([
                  {
                    bool: {
                      must: [
                        {
                          term: {
                            "offer.client.esg.criteria.shortName.keyword": {
                              value: (window as any).selected_esg_criterion
                            }
                          }
                        },
                        {
                          term: {
                            "offer.client.esg.criteria.riskCode.keyword": {
                              value: riskCode
                            }
                          }
                        }
                      ]
                    }
                  }
                ])) : [
                  {
                    term: {
                      "offer.client.esg.criteria.shortName.keyword": {
                        value: (window as any).selected_esg_criterion
                      }
                    }
                  }],
              }
            }
          }
        }
      } else {
        (window as any).selected_esg_criterion = null;
      }

      return empty_build;
    }
    if (this.klass === 'ESGRiskCode_Multiselect') {
      if (build.values.length) {
        console.log({ vals: build.values });
        (window as any).selected_esg_riskCode = build.values;
        build = empty_build;
        build.finished_query = {
          nested: {
            path: "offer.client.esg.criteria",
            query: {
              bool: {
                should: (window as any).selected_esg_riskCode.map((riskCode) => ({
                  bool: {
                    must: [
                      ...((window as any).selected_esg_criterion ? [{
                        term: {
                          "offer.client.esg.criteria.shortName.keyword": {
                            value: (window as any).selected_esg_criterion
                          }
                        }
                      }] : []),
                      {
                        term: {
                          "offer.client.esg.criteria.riskCode.keyword": {
                            value: riskCode
                          }
                        }
                      }
                    ]
                  }
                }))
              }
            }
          }
        }
      } else {
        (window as any).selected_esg_riskCode = null;
      }

      return empty_build;
    }

    if (this.klass === 'LocationPeril_Multiselect') {
      Array.from(b.classList).forEach((v) => {
        if (regexMatches(/location_peril_/g, v)) {
          b.classList.remove(v);
        }
      });
      if (build.values.length) {
        b.classList.add(`location_peril_${build.values[0]}`);
        (window as any).selected_location_peril = build.values[0];
      } else {
        (window as any).selected_location_peril = null;
        return empty_build;
      }

      // build the finished query because the Peril_Multiselect is a single-select
      // bo bool:should are needed
      build = empty_build;
      build.finished_query = constructCombinedLocationPerilQuery();
    }

    if (this.klass === 'LocationPerilRiskClass_Multiselect') {
      const sorted_values = build.values.sort().map(parseFloat);

      if (sorted_values.length) {
        (window as any).selected_location_risk_classes = sorted_values;
      } else {
        (window as any).selected_location_risk_classes = [];
      }

      build = empty_build;
      build.finished_query = constructCombinedLocationPerilQuery();

      return build;
    }

    if (
      this.klass === 'LocationAssessmentLevel3_IgnoreNullScores_Multiselect'
    ) {
      if (build.values.length) {
        (window as any).ignore_null_assessment_scores = build.values[0];
      } else {
        (window as any).ignore_null_assessment_scores =
          NULL_ASSESMENT_SCORES_BEHAVIOR.KEEP_EMPTY;
      }
    }

    if (this.klass === 'LocationAssessmentLevel3_KeyFactorKind_Multiselect') {
      if (build.values.length) {
        (window as any).selected_key_factor_kind = build.values[0];
      } else {
        (window as any).selected_key_factor_kind = null;
        // this.es.app_component.getComponentByKlass('KeyCriterion_Multiselect').listOfOptions = [];
        // this.es.app_component.getComponentByKlass('AssessmentRecommendations_Multiselect').listOfOptions = [];
      }

      return empty_build;
    }

    if (this.klass === 'KeyCriterion_Multiselect') {
      if (build.values.length) {
        (window as any).selected_key_criterion = build.values[0];
      } else {
        (window as any).selected_key_criterion = null;
        // this.es.app_component.getComponentByKlass('AssessmentRecommendations_Multiselect').listOfOptions = [];
      }

      return empty_build;
    }

    if (this.klass === 'AssessmentRecommendations_Multiselect') {
      if (build.values.length) {
        (window as any).applicable_assessment_recommendations = build.values;
      } else {
        (window as any).applicable_assessment_recommendations = [];
      }

      return empty_build;
    }

    // UDGB-48
    if (this.klass.endsWith('LogicalFilter')) {
      (window as any)[this.klass] = build.values[0];
    }

    return build;
  }

  private _regexSanitizer = (s: string): string => {
    return s;
    // return s.replace(/[-[\]\{\}()*+?.,\\^$|#]/g, '\\$&');
  }

  private _regexBuilder = (s: string): RegExp => {
    return new RegExp(
      s
        .split(' ')
        .map((part) => `(${part})`)
        .join('|'),
      'giu'
    );
  }

  onSearch($event: string | null) {
    if (!COMPONENT_KEYS_TO_ALWAYS_UPDATE().includes(this.klass)) {
      return;
    }

    this.listOfOptions = [];
    let filtered: IArrayOfOptions = [];

    if (!$event) {
      return this.listOfOptions;
    }

    $event = $event.trim();

    if (this.isGrouped) {
      filtered = <IArrayOfOptions>this.virtualListOfOptions.map((group) => {
        return {
          group_name: group.group_name,
          options: group.options.filter(
            (pair) => pair.value.toLowerCase().includes($event.toLowerCase())
            // $event.toLowerCase().match(this._regexBuilder(this._regexSanitizer(pair.value)))
          ),
        };
      });
    } else {
      filtered = this.virtualListOfOptions.filter((pair) => {
        return pair.value.toLowerCase().includes($event.toLowerCase());
        // return $event.toLowerCase().match(this._regexBuilder(this._regexSanitizer(pair.value)));
      });
    }

    this.listOfOptions = filtered;
  }

  onChange($event) {
    if (
      this.klass === 'Peril_Multiselect' ||
      this.klass in MULTISELECT_HARDCODED_OPTIONS
    ) {
      this.filterQueryBuilder();
    }

    if (this.klass === 'LocationPerilRiskClass_Multiselect') {
      const sorted_values = this.valueOf.sort().map(parseFloat);

      if (!sorted_values.length) {
        (window as any).selected_location_risk_classes = [];
      }
    }

    this.es.readyForRefresh();

    if (!this.listOfSelectedValues || this.listOfSelectedValues.length) {
      this.indeterminateColumns = false;
      // this.allColumnsChecked = false;
      return;
    }

    if (
      JSON.stringify(this.listOfSelectedValues) !==
      JSON.stringify(this.listOfOptions)
    ) {
      this.indeterminateColumns = true;
      // this.allColumnsChecked = false;
      return;
    }

    this.indeterminateColumns = false;
    // this.allColumnsChecked = true;
  }

  ngOnDestroy() {
    switch (this.klass) {
      case 'LocationPeril_Multiselect':
        (window as any).selected_location_peril = null;
        break;
      case 'LocatLocationPerilRiskClass_MultiselectionPeril_Multiselect':
        (window as any).selected_location_risk_classes = null;
        break;
      case 'Peril_Multiselect':
        (window as any).selected_peril = null;
        break;
      case 'LocationAssessmentLevel3_KeyFactorKind_Multiselect':
        (window as any).selected_key_factor_kind = null;
        break;
      case 'KeyCriterion_Multiselect':
        (window as any).selected_key_factor_kind = null;
        break;
      case 'AssessmentRecommendations_Multiselect':
        (window as any).applicable_assessment_recommendations = [];
        break;
      case 'ESGRiskCode_Multiselect':
        (window as any).selected_esg_riskCode = null;
        break;
      case 'ESGCriterion_Multiselect':
        (window as any).selected_esg_criterion = null;
        break;
    }
  }
}
