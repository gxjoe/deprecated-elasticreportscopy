import {
  Component,
  OnInit,
  Input,
  ChangeDetectorRef,
  OnChanges,
  ViewChild,
  ElementRef,
  ChangeDetectionStrategy,
  ViewEncapsulation
} from '@angular/core';
import { ElasticService } from '../services/elastic.service';
import { OnIncomingData } from '../interfaces/on-incoming-data';
import { TEXT_CARD_OPTIONS } from './constants';
import { IOnLoadingSpinner } from '../interfaces/on-loading-spinner';
import { CsvService } from '../services/csv.service';
import { DivDownloaderService } from '../services/div-downloader.service';

@Component({
  selector: 'app-text-card',
  templateUrl: './text-card.component.html',
  styleUrls: ['./text-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
  encapsulation: ViewEncapsulation.None
})
export class TextCardComponent
  implements OnInit, OnIncomingData, IOnLoadingSpinner {
  @Input()
  klass;
  @Input()
  hasOwnQuery;

  @Input()
  heading;

  @ViewChild('download_csv', { static: false }) download_csv: ElementRef;

  columns = [];
  isLoading = true;
  teaser: '';
  popover_visible: boolean;

  source_hits = [];
  lob_count = 0;

  constructor(
    private es: ElasticService,
    public cd: ChangeDetectorRef,
    private csvs: CsvService,
    private div: DivDownloaderService
  ) { }

  ngOnInit() {
    this.es.app_component.setComponent(this.klass, this);
    if (this.hasOwnQuery !== 'true') {
      this.es.app_component.textCardComponents.push(this);
    }

    Object.assign(this, TEXT_CARD_OPTIONS[this.klass]);
    this.handleIncomingData = TEXT_CARD_OPTIONS[this.klass].handleIncomingData;
  }

  popoverToggle() {
    this.popover_visible = !this.popover_visible;
  }

  handleIncomingData() { }

  downloadAllHitsAsCsv() {
    let button: HTMLButtonElement = this.download_csv.nativeElement;
    if (!button) {
      button = document.querySelector('#download_csv');
    }

    const csv_rows = this.csvs.nestedJSONtoCSV(
      JSON.parse(button.getAttribute('source_hits'))
    );
    this.div.downloadNestedCSV(
      this.download_csv.nativeElement,
      'All LoBs',
      csv_rows
    );
  }
}
