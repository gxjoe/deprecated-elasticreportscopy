import {
  findByKeyInNestedObject,
  extractMedianAgg,
  extractDistinctTokensCountFromAgg
} from '../elastic/utils';
import {
  pprintCurrency,
  highlightJSON,
  nFormatter,
  numberWithCommas
} from '../utils/helpers/helpers';
import { SUM_SIGMA_UTF } from '../charts/histogram-table-chart/common';
import { fromUnixToFormat } from '../charts/utils';
import pluralize from 'pluralize';
import { SearchResponse } from 'elasticsearch';

export const HIGHLIGHTED_JSON_HITS_LIMIT = 1;

export const TEXT_CARD_OPTIONS = {
  AllOffers: {
    columns: [
      {
        description: 'Total Counts',
        content: '...'
      }
    ],
    handleIncomingData(key, inputData, query) {
      if (key !== 'AllOffers') {
        return false;
      }

      const lob_based: SearchResponse<any> = inputData.responses[0];
      const address_based: SearchResponse<any> = inputData.responses[1];

      if (lob_based.hits && typeof lob_based.hits.total !== 'number') {
        lob_based.hits.total = (lob_based.hits.total as any).value;
      }

      if (address_based.hits && typeof address_based.hits.total !== 'number') {
        address_based.hits.total = (address_based.hits.total as any).value;
      }

      if (
        lob_based.hits.total > HIGHLIGHTED_JSON_HITS_LIMIT ||
        // dont highlight offers JSON unless an underwriter has been selected
        !JSON.stringify(query).includes('lob.localUnderwriters')
      ) {
        this.columns[0].json_content = undefined;
      } else {
        this.columns[0].json_content = highlightJSON(
          lob_based.hits.hits.map(item => item._source),
          'id',
          false
        ).html;
      }

      let offer_count, lob_count, address_count;

      offer_count = extractDistinctTokensCountFromAgg(
        lob_based.aggregations,
        'unique_offers'
      );
      lob_count = extractDistinctTokensCountFromAgg(
        lob_based.aggregations,
        'unique_lobs'
      );
      address_count = extractDistinctTokensCountFromAgg(
        address_based.aggregations,
        'unique_addresses_address_based'
      );

      this.columns[0].content = `
      ${numberWithCommas(offer_count)} ${pluralize('Offer', offer_count)}<br>
      ${numberWithCommas(lob_count)} ${pluralize('LoB', lob_count)}<br>
      ${numberWithCommas(address_count)} ${pluralize('Address', address_count)}
      `;

      this.lob_count = lob_count;

      // const source_hits = lob_based.hits.hits.map(item => item._source);
      // this.source_hits = JSON.stringify(source_hits);
    }
  },
  MinMaxAvgTotalPremiumTextChart: {
    columns: [
      {
        description: 'Min',
        content: 0
      },
      {
        description: 'Max',
        content: 0
      },
      {
        description: 'Avg',
        content: 0
      },
      {
        description: 'Median',
        content: 0
      },
      {
        description: SUM_SIGMA_UTF,
        content: 0
      }
    ],
    handleIncomingData(key, inputData, query) {
      if (key !== 'MinMaxAvgTotalPremiumTextChart') {
        return;
      }

      const bucket = findByKeyInNestedObject(inputData, 'aggregations')[
        'aggregations'
      ]['filter_agg'].filter_agg;
      const median = extractMedianAgg(inputData);

      this.columns = [
        {
          description: this.columns[0].description,
          content: pprintCurrency(bucket.min)
        },
        {
          description: this.columns[1].description,
          content: pprintCurrency(bucket.max)
        },
        {
          description: this.columns[2].description,
          content: pprintCurrency(bucket.avg)
        },
        {
          description: this.columns[3].description,
          content: pprintCurrency(median)
        },
        {
          description: this.columns[4].description,
          content: pprintCurrency(bucket.sum)
        }
      ];
    }
  },
  MinMaxAvgUniqaSharePremiumTextChart: {
    columns: [
      {
        description: 'Min',
        content: 0
      },
      {
        description: 'Max',
        content: 0
      },
      {
        description: 'Avg',
        content: 0
      },
      {
        description: 'Median',
        content: 0
      },
      {
        description: SUM_SIGMA_UTF,
        content: 0
      }
    ],
    handleIncomingData(key, inputData, query) {
      if (key !== 'MinMaxAvgUniqaSharePremiumTextChart') {
        return;
      }

      const bucket = findByKeyInNestedObject(inputData, 'aggregations')[
        'aggregations'
      ]['filter_agg'].filter_agg;
      const median = extractMedianAgg(inputData);

      this.columns = [
        {
          description: this.columns[0].description,
          content: pprintCurrency(bucket.min)
        },
        {
          description: this.columns[1].description,
          content: pprintCurrency(bucket.max)
        },
        {
          description: this.columns[2].description,
          content: pprintCurrency(bucket.avg)
        },
        {
          description: this.columns[3].description,
          content: pprintCurrency(median)
        },
        {
          description: this.columns[4].description,
          content: pprintCurrency(bucket.sum)
        }
      ];
    }
  },
  MinMaxAvgTotalSumInsuredTextChart: {
    columns: [
      {
        description: 'Min',
        content: 0
      },
      {
        description: 'Max',
        content: 0
      },
      {
        description: 'Avg',
        content: 0
      },
      {
        description: 'Median',
        content: 0
      },
      {
        description: SUM_SIGMA_UTF,
        content: 0
      }
    ],
    handleIncomingData(key, inputData) {
      if (
        typeof 'key' !== 'string' ||
        key !== 'MinMaxAvgTotalSumInsuredTextChart'
      ) {
        return;
      }

      const bucket = findByKeyInNestedObject(inputData, 'aggregations')[
        'aggregations'
      ]['filter_agg'].filter_agg;
      const median = extractMedianAgg(inputData);

      this.columns = [
        {
          description: this.columns[0].description,
          content: pprintCurrency(bucket.min)
        },
        {
          description: this.columns[1].description,
          content: pprintCurrency(bucket.max)
        },
        {
          description: this.columns[2].description,
          content: pprintCurrency(bucket.avg)
        },
        {
          description: this.columns[3].description,
          content: pprintCurrency(median)
        },
        {
          description: this.columns[4].description,
          content: pprintCurrency(bucket.sum)
        }
      ];
    }
  },
  MinMaxOfferCreatedTextChart: {
    columns: [
      {
        description: 'Offers <b>created</b> from',
        content: '-'
      },
      {
        description: 'To',
        content: '-'
      }
    ],
    handleIncomingData(key, inputData, query) {
      if (typeof 'key' !== 'string' || key !== 'MinMaxOfferCreatedTextChart') {
        return;
      }

      const bucket = findByKeyInNestedObject(inputData, 'aggregations')[
        'aggregations'
      ]['filter_agg'].filter_agg;
      const DATE_FORMATS = {
        from_elastic: 'YYYY/MM/DD HH:mm:ss',
        moment: 'D. MMM YYYY'
      };

      this.columns = [
        {
          description: this.columns[0].description,
          content: fromUnixToFormat(
            bucket.min_as_string,
            DATE_FORMATS.moment,
            DATE_FORMATS.from_elastic
          )
        },
        {
          description: this.columns[1].description,
          content: fromUnixToFormat(
            bucket.max_as_string,
            DATE_FORMATS.moment,
            DATE_FORMATS.from_elastic
          )
        }
      ];
      this.cd.detectChanges();
    }
  },
  MinMaxOfferUpdatedTextChart: {
    columns: [
      {
        description: 'Offers <b>modified</b> from',
        content: '-'
      },
      {
        description: 'To',
        content: '-'
      }
    ],
    handleIncomingData(key, inputData, query) {
      if (typeof 'key' !== 'string' || key !== 'MinMaxOfferUpdatedTextChart') {
        return;
      }

      const bucket = findByKeyInNestedObject(inputData, 'aggregations')[
        'aggregations'
      ]['filter_agg'].filter_agg;
      const DATE_FORMATS = {
        from_elastic: 'YYYY/MM/DD HH:mm:ss',
        moment: 'D. MMM YYYY'
      };

      this.columns = [
        {
          description: this.columns[0].description,
          content: fromUnixToFormat(
            bucket.min_as_string,
            DATE_FORMATS.moment,
            DATE_FORMATS.from_elastic
          )
        },
        {
          description: this.columns[1].description,
          content: fromUnixToFormat(
            bucket.max_as_string,
            DATE_FORMATS.moment,
            DATE_FORMATS.from_elastic
          )
        }
      ];
      this.cd.detectChanges();
    }
  },
  ResponsibleLocalUnderwriter: {
    columns: [
      {
        description: 'Responsible Local Underwriter',
        content: ''
      }
    ],
    handleIncomingData(key, inputData) {
      this.columns[0].json_content = undefined;
      this.columns[0].teaser = undefined;
      this.columns[0].content = '';

      /*
      if (key !== 'ResponsibleLocalUnderwriter') {
        return;
      }

      if (!inputData.hits.hits.length) {
        this.columns[0].json_content = undefined;
        this.columns[0].teaser = undefined;
        this.columns[0].content = '';
        return;
      }
      let hit = findByKeyInNestedObject(inputData.hits, 'hits').hits[0];
      hit = hit._source.lobs[0].localUnderwriters[0];

      const highlight_dict = highlightJSON(hit, 'fullName', false);

      this.columns[0].teaser = highlight_dict.main_val;
      this.columns[0].content = hit;
      this.columns[0].json_content = hit;
      */
    }
  }
};
