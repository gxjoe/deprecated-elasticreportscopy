import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import { ElasticService } from '../services/elastic.service';

import * as moment from 'moment';
import componentNameVsVariableNameMapping from '../utils/constants/componentNameVsVariableNameMapping';
import { FilterComponent } from '../utils/higher-order/FilterComponent';
import {
  ElasticFilterSubtypes,
  ElasticRangeOperators,
  ElasticQueryClauses,
} from '../elastic/constants';
import IFilterComponentQueryMeta from '../utils/higher-order/IFilterComponentQueryMeta';
import {
  pprintCurrency,
  extractNumberFromString,
  pprintLargePrice,
} from '../utils/helpers/helpers';
import {
  constructNestedExistsQuery,
  constructNestedRangeQuery,
} from '../elastic/utils';
import { extract_base_path } from '../charts/histogram-table-chart/constants';

export type ValueOfSlider = number | number[];

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SliderComponent
  extends FilterComponent
  implements OnInit, OnDestroy {
  @Input() klass: string;
  @Input() min: number;
  @Input() max: number;
  @Input() kind: 'single' | 'double';
  @Input() suffix?: string;
  @Input() unit?: 'currency';
  @Input() heading: string;
  @Input() step: number;
  @Input() isDisabled?: boolean;

  @Input() standalone = false;
  @Input() defaults: ValueOfSlider;

  @Output() emitChange: EventEmitter<ValueOfSlider> = new EventEmitter();

  public elastic_variable: string;
  private valueOf_: ValueOfSlider;

  private _ngRangeModel: Array<number>;
  private _ngRangeModelMin: number;
  private _ngRangeModelMax: number;

  abbreviatedCurrencyFormatter = (value: number) => {
    // return as $ 52 Mio
    return `${pprintLargePrice(value)}`;
  }

  constructor(private es: ElasticService) {
    super();
  }

  get as_currency() {
    return this.unit === 'currency';
  }

  get isSingle() {
    return this.kind === 'single';
  }

  get valueOf() {
    return Array.isArray(this.valueOf_) ? this.valueOf_ : [this.valueOf_];
  }

  private get shouldStoreInWindow() {
    return [
      'AddressUniqaPMLRange',
      'AddressLocationAssessmentScoreRange',
    ].includes(this.klass);
  }

  private doStoreInWindow(val: ValueOfSlider | null) {
    if (this.shouldStoreInWindow) {
      (window as any)[this.klass] = val;
    }
  }

  ngOnInit() {
    if (!this.standalone) {
      this.es.app_component.setComponent(this.klass, this);
      this.es.app_component.filterComponents.push(this);
    }

    if (!this.isSingle) {
      if (this.suffix === 'year') {
        const now = parseInt(moment().format('YYYY'), 10);
        this.min = now - 10;
        this.max = now;
      } else {
        this.min = !isNaN(this.min) ? parseFloat(`${this.min}`) : null;
        this.max = !isNaN(this.max) ? parseFloat(`${this.max}`) : Infinity;

        if (this.defaults) {
          if (typeof this.defaults === 'string') {
            this.defaults = (this.defaults as string)
              .split(',')
              .map(parseFloat);
          }
          this._ngRangeModelMin = this.defaults[0];
          this._ngRangeModelMax = this.defaults[1];
        } else {
          this._ngRangeModelMin = this.min;
          this._ngRangeModelMax = this.max;
        }

        this._ngRangeModel = [this._ngRangeModelMin, this._ngRangeModelMax];
      }

      this.valueOf_ = this._ngRangeModel;
    } else {
      this.min = parseFloat(`${this.min}`);
      this.max = parseFloat(`${this.max}`);

      this.valueOf_ = this.min;
    }

    this.tipFormatter = (value) => {
      switch (true) {
        case this.as_currency:
          // return as $ 52,000
          if (value <= 10e6) {
            return `${pprintCurrency(value)}`;
          }
          // return as $ 52 Mio
          return this.abbreviatedCurrencyFormatter(value);
        default:
          return `${value}`;
      }
    };

    if (this.klass in componentNameVsVariableNameMapping) {
      this.elastic_variable = componentNameVsVariableNameMapping[this.klass];
    }

    this.emitChange.emit(this.valueOf_);
  }

  tipFormatter = (value) => value;

  filterQueryBuilder = (): IFilterComponentQueryMeta => {
    const build: IFilterComponentQueryMeta = {
      klass: this.klass,
      elastic_variable: this.elastic_variable,
      subtype: ElasticFilterSubtypes.range,
      operators: [],
      values: [],
      clause: ElasticQueryClauses.must,
      nested: this.elastic_variable.includes('.'),
      finished_query: null,
    };

    if (!this.valueOf || !this.valueOf.length || this.isDisabled) {
      this.doStoreInWindow(null);
      return build;
    }

    build.values = this.valueOf;

    if (this.isSingle) {
      build.operators = [ElasticRangeOperators.gte];
      this.doStoreInWindow(build.values);
      return build;
    }

    build.operators = [ElasticRangeOperators.gte, ElasticRangeOperators.lte];
    if (
      [
        'TotalPremium',
        'TotalSumInsured',
        'TurnoverRange',
        'LobUniqaPMLRange',
        'AddressUniqaPMLRange',
        'AddressLocationAssessmentScoreRange',
        'NumberOfBuildings',
        'NumberOfStories',
        'NumberOfBasementFloors',
        'YearOfConstruction',
      ].includes(this.klass)
    ) {
      const nestedPath = extract_base_path(this.elastic_variable);
      const nestedField = this.elastic_variable;

      const [min, max] = [build.values[0] || 0, build.values[1]];

      const rangeBaseQuery = constructNestedRangeQuery(
        nestedPath,
        nestedField,
        {
          gte: min,
          lte: max,
        }
      );

      if (min > 0) {
        // if the minimum is larger than 0, the user is intentionally "filtering out"
        // the less-than documents.
        build.finished_query = rangeBaseQuery;
      } else {
        // if the min is indeed zero, the user is just attempting to perform a "broad" filter
        // but the implication is, any documents that don't have the target field (`this.elastic_variable`)
        // will be automatically filtered out.
        // As such, this bool-should of a not-exists + range query takes care of both edge cases.
        // Fixes UDGB-21.
        build.finished_query = {
          bool: {
            should: [
              {
                bool: {
                  must_not: [
                    constructNestedExistsQuery(nestedPath, nestedField),
                  ],
                },
              },
              rangeBaseQuery,
            ],
          },
        };
      }
    }

    this.doStoreInWindow(build.values);
    return build;
  }

  onChange() {
    this.valueOf_ = this._ngRangeModel;
    if (!this.standalone) {
      this.es.readyForRefresh();
    }

    this.emitChange.emit(this.valueOf_);
  }

  onModelChange($event: Array<number>) {
    this._ngRangeModelMin = $event[0];
    this._ngRangeModelMax = $event[1];
    this.onChange();
  }

  onMinChange($event: number) {
    this._ngRangeModel = [$event, this._ngRangeModel[1]];
    this.onChange();
  }

  onMaxChange($event: number) {
    this._ngRangeModel = [this._ngRangeModel[0], $event];
    this.onChange();
  }

  private formatInputAsIdentity(v: any) {
    return v;
  }

  private formatInputValAsCurrency(val: any) {
    return pprintCurrency(val);
  }

  chooseInputValFormatter() {
    switch (true) {
      case this.as_currency:
        return this.formatInputValAsCurrency;
    }
    return this.formatInputAsIdentity;
  }

  parserInputVal(val: any) {
    return extractNumberFromString(val);
  }

  ngOnDestroy() {
    this.doStoreInWindow(null);
  }
}
