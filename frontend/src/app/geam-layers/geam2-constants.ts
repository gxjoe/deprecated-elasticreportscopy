// Migrated over from https://github.com/sudolabs-io/geam/blob/main/src/constants/layers.ts
// Mostly 1-1 to ensure interoperability
// except for `LayerTranslations` which are powered by `next-translate`
// but here, it's a JSON config.

import * as R from "ramda";
import fromHexToRgba, { RgbaTuple } from "hex-rgb";
import { PerilProvider, PerilSource, SubPerilName } from "./perils";
import { environment } from "src/environments/environment";

export const LAYER_COLOR_OPACITY = 180;
export const CUSTOM_LAYER_STYLE_COLOR_OPACITY = 200;
const WhiteColorInRgbTuple = [255, 255, 255];
export const WhiteColorWithFullOpacity = R.append(255, WhiteColorInRgbTuple);
export const addCustomLayerOpacity = R.append(CUSTOM_LAYER_STYLE_COLOR_OPACITY);
export const WhiteColorWithCustomLayerOpacity =
    addCustomLayerOpacity(WhiteColorInRgbTuple);

export const LAYER_LOAD_WAIT_LIMIT = 200;
export const METEORITE_RATIO_DIVIDER = 1.7;

type GetLayerFeatureColorProps = {
    layer: Layer;
    feature: GeoJSON.Feature;
};

const INTENSITY_OFFSET_IN_ARRAY_FORMAT = 1;

const getLayerIntensityInColorScheme = ({
    feature,
}: {
    feature: GeoJSON.Feature;
}) =>
    R.clamp(1, 10, Math.round(feature?.properties?.intensity as number) || 1) -
    INTENSITY_OFFSET_IN_ARRAY_FORMAT;

const getLayerFeatureHexColor = ({
    layer,
    feature,
}: GetLayerFeatureColorProps): string => {
    // color for natural hazards layers
    if (layer.subPeril && layer.subPeril in LayerColorScheme) {
        const intensity = getLayerIntensityInColorScheme({ feature });
        const key = layer.subPeril as keyof typeof LayerColorScheme;
        return LayerColorScheme[key][intensity];
    }

    // color for manual layers
    if (layer.isManual) {
        const intensity = getLayerIntensityInColorScheme({ feature });
        return LayerColorScheme.ManualLayer[intensity];
    }

    // color for custom layers
    if (layer.label in CustomLayerColorScheme) {
        if (layer.label === "terrorism(AON)") {
            const colorKey: 0 | 2 | 3 | 4 | 5 = feature?.properties?.prmrating ?? 0;
            return CustomLayerColorScheme[layer.label][colorKey];
        }
        if (layer.label === "politicalViolence(AON)") {
            const colorKey: keyof (typeof CustomLayerColorScheme)["politicalViolence(AON)"] =
                feature?.properties?.riskcategory ?? "No Risk";
            return CustomLayerColorScheme[layer.label][colorKey];
        }
        if (layer.label === "historicalCyclones") {
            const colorKey: keyof (typeof CustomLayerColorScheme)["historicalCyclones"] =
                feature?.properties?.wind;
            return CustomLayerColorScheme[layer.label][colorKey];
        }
        if (layer.label === "populationDensity(NASA)") {
            const density: number = feature?.properties?.valuenum ?? 0;
            const colorIndex =
                (density <= 28.107285 && "0") ||
                (density <= 74.819664 && 1) ||
                (density <= 199.16481 && 2) ||
                (density <= 549.7402 && 3) ||
                (density <= 1463.3706 && 4) ||
                (density <= 3895.392 && 5) ||
                (density <= 2566708 && 6) ||
                6;
            return CustomLayerColorScheme["populationDensity(NASA)"][colorIndex];
        }
        console.error(
            "getLayerFeatureColor: Supplied layer is custom but there is no handler specified for it"
        );
    }

    // TODO: figure out what should be the default color if we don't have it specified
    return "#fff";
};

export const getLayerFeatureColor = ({
    layer,
    feature,
}: GetLayerFeatureColorProps) => {
    const hexColor = getLayerFeatureHexColor({ layer, feature });
    const rgbaColor = fromHexToRgba(hexColor, { format: "array" });
    const colorWithLayerOpacity = R.update(
        3,
        LAYER_COLOR_OPACITY,
        rgbaColor
    ) as RgbaTuple;
    return colorWithLayerOpacity;
};

// [] property of every key is color scheme, where index 0 is lowest
// and 9 is highest intensity color for corresponding riskClass of layer (1 to 10)
export const LayerColorScheme = {
    // Tsunami ---------------
    [SubPerilName.Tsunami]: [
        "#eff6ff",
        "#dbeafe",
        "#bfdbfe",
        "#93c5fd",
        "#60a5fa",
        "#3b82f6",
        "#2563eb",
        "#1d4ed8",
        "#1e40af",
        "#1e3a8a",
    ],
    // Storm ---------------
    [SubPerilName.StormFlood]: [
        "#f0f9ff",
        "#e0f2fe",
        "#bae6fd",
        "#7dd3fc",
        "#38bdf8",
        "#0ea5e9",
        "#0284c7",
        "#0369a1",
        "#075985",
        "#0c4a6e",
    ],
    [SubPerilName.WindSpeed]: [
        "#f0f9ff",
        "#e0f2fe",
        "#bae6fd",
        "#7dd3fc",
        "#38bdf8",
        "#0ea5e9",
        "#0284c7",
        "#0369a1",
        "#075985",
        "#0c4a6e",
    ],
    [SubPerilName.Tornado]: [
        "#f0f9ff",
        "#e0f2fe",
        "#bae6fd",
        "#7dd3fc",
        "#38bdf8",
        "#0ea5e9",
        "#0284c7",
        "#0369a1",
        "#075985",
        "#0c4a6e",
    ],
    // Flood ---------------
    [SubPerilName.RiverFlood]: [
        "#ecfeff",
        "#cffafe",
        "#a5f3fc",
        "#67e8f9",
        "#22d3ee",
        "#06b6d4",
        "#0891b2",
        "#0e7490",
        "#155e75",
        "#164e63",
    ],
    // Coastal Flood ---------------
    [SubPerilName.CoastalFlood]: [
        "#F3F0FF",
        "#E5DBFF",
        "#D0BFFF",
        "#B197FC",
        "#9775FA",
        "#845EF7",
        "#7950F2",
        "#7048E8",
        "#6741D9",
        "#5F3DC4",
    ],
    // Hailstorm ---------------
    [SubPerilName.Hail]: [
        "#f8fafc",
        "#f1f5f9",
        "#e2e8f0",
        "#cbd5e1",
        "#94a3b8",
        "#64748b",
        "#475569",
        "#334155",
        "#1e293b",
        "#0f172a",
    ],
    // SnowLevel ---------------
    [SubPerilName.StandaloneAvalanche]: [
        "#f9fafb",
        "#f3f4f6",
        "#e5e7eb",
        "#d1d5db",
        "#9ca3af",
        "#6b7280",
        "#4b5563",
        "#374151",
        "#1f2937",
        "#111827",
    ],
    [SubPerilName.StandaloneSnowLoad]: [
        "#f9fafb",
        "#f3f4f6",
        "#e5e7eb",
        "#d1d5db",
        "#9ca3af",
        "#6b7280",
        "#4b5563",
        "#374151",
        "#1f2937",
        "#111827",
    ],
    // Earthquake ---------------
    [SubPerilName.Earthquake]: [
        "#fafaf9",
        "#f5f5f4",
        "#e7e5e4",
        "#d6d3d1",
        "#a8a29e",
        "#78716c",
        "#57534e",
        "#44403c",
        "#292524",
        "#1c1917",
    ],
    // Lightning ---------------
    [SubPerilName.Lightning]: [
        "#fefce8",
        "#fef9c3",
        "#fef08a",
        "#fde047",
        "#facc15",
        "#eab308",
        "#ca8a04",
        "#a16207",
        "#854d0e",
        "#713f12",
    ],
    // Wildfire ---------------
    [SubPerilName.Fire]: [
        "#fef2f2",
        "#fee2e2",
        "#fecaca",
        "#fca5a5",
        "#f87171",
        "#ef4444",
        "#dc2626",
        "#b91c1c",
        "#991b1b",
        "#7f1d1d",
    ],
    // VolcanicEruption ---------------
    [SubPerilName.Volcano]: [
        "#fff7ed",
        "#ffedd5",
        "#fed7aa",
        "#fdba74",
        "#fb923c",
        "#f97316",
        "#ea580c",
        "#c2410c",
        "#9a3412",
        "#7c2d12",
    ],
    // Landslide ---------------
    [SubPerilName.Landslide]: [
        "#f0fdf4",
        "#dcfce7",
        "#bbf7d0",
        "#86efac",
        "#4ade80",
        "#22c55e",
        "#16a34a",
        "#15803d",
        "#166534",
        "#14532d",
    ],
    // ManualLayers ---------------
    ManualLayer: [
        "#fff7ed",
        "#ffedd5",
        "#fed7aa",
        "#fdba74",
        "#fb923c",
        "#f97316",
        "#ea580c",
        "#c2410c",
        "#9a3412",
        "#7c2d12",
    ],
};

// Custom schemes for a few extra layers
export const CustomLayerColorScheme = {
    "terrorism(AON)": {
        0: "#A6CEE3",
        2: "#1F78B4",
        3: "#B2DF8A",
        4: "#33A02C",
        5: "#FB9A99",
    },
    "politicalViolence(AON)": {
        "No Risk": "#FFFFFF",
        Negligble: "#b2df8a", // typo is on purpose
        Low: "#229A00",
        Medium: "#5CA2D1",
        High: "#F84F40",
        Severe: "#B81609",
    },
    "populationDensity(NASA)": [
        "#FFFFB2",
        "#FED976",
        "#FEB24C",
        "#FD8D3C",
        "#FC4E2A",
        "#E31A1C",
        "#B10026",
    ],
    historicalCyclones: {
        "<= 12.1285714285714": "#FFFFB2",
        "<= 24.2571428571428": "#FED9B2",
        "<= 36.3857142857142": "#FEB24C",
        "<= 48.5142857142856": "#FD8D3C",
        "<= 60.642857142857": "#FC4E2A",
        "<= 72.7714285714284": "#E31A1C",
        "<= 84.9": "#B10026",
    },
    "europeEarthquake(EFEHR)": {
        "<= 0.0063760627": "#FFFFB2",
        "<= 0.0116276848": "#FED976",
        "<= 0.0224213916": "#FEB24C",
        "<= 0.0439071796": "#FD8D3C",
        "<= 0.0974637082": "#FC4E2A",
        "<= 0.2144764231": "#E31A1C",
        "<= 0.6929666625": "#B10026",
    },
    "CEELandslides(RELIA)": {
        "<= 1": "#1a9850",
        "<= 2": "#8cce8a",
        // commented lines are duplicates in old styles
        // '<= 2': '#d2ecb4',
        "<= 3": "#fff2cc",
        // '<= 3': '#fed6b0',
        "<= 99": "#f79272",
        // '<= 99': '#d73027',
    },
    "CEELandslides(SUS)": {
        "<= 2": "#FFFFB2",
        // commented lines are duplicates in old styles
        // '<= 2': '#FED976',
        "<= 3": "#FEB24C",
        // '<= 3': '#FD8D3C',
        "<= 4": "#FC4E2A",
        // '<= 4': '#E31A1C',
        "<= 5": "#B10026",
    },
    earthquakeEuropeMidEast: {
        "<= 1": "#ffffcc",
        "<= 2": "#ffeda0",
        "<= 3": "#fed976",
        "<= 4": "#feb24c",
        "<= 5": "#fd8d3c",
        "<= 6": "#fc4e2a",
        "<= 7": "#e31a1c",
        "<= 8": "#bd0026",
        "<= 9": "#800026",
    },
    "zeus2017(MedgustsAON)": {
        "<= 9.4": "#FFFFCC",
        "<= 15.7": "#C7E9B4",
        "<= 22": "#7FCDBB",
        "<= 28.3": "#41B6C4",
        "<= 34.6": "#1D91C0",
        "<= 41.1": "#225EA8",
        "<= 54.6": "#0C2C84",
    },
};

export interface Layer {
    label: string;
    symbol?: string;
    subPeril?: SubPerilName;
    perilProvider?: PerilProvider;
    perilSource?: PerilSource;
    tilesSource?: string;
    overviewSource?: string;
    lookupSource?: string;
    lookupSourceTileSet?: string;
    infoContent?: string;
    isVisible?: boolean;
    isManual?: boolean;
    geoJSON?: any;
}

export interface Category {
    label: string;
    extras?: string;
    symbol?: string;
    subCategories?: Category[];
    layers?: Layer[];
}

type BigQueryOilAndNaturalGasColumns = {
    cartodb_id: number;
    name: string;
    kind: "oil" | "natural_gas";
};

type BigQueryHistoricalCyclonesColumns = {
    // other columns are missing, there are more but these suffice for our functionality
    cartodb_id: number;
    wind: number;
};

type BigQueryHistoricalHailColumns = {
    // other columns are missing, there are more but these suffice for our functionality
    cartodb_id: number;
    size: number;
};

type BigQueryTsunamiEventsFrom2000Bc = {
    // other columns are missing, there are more but these suffice for our functionality
    cartodb_id: number;
    primary_magnitude: number;
};

type BigQueryHistoricalMeteoritesColumns = {
    // other columns are missing, there are more but these suffice for our functionality
    cartodb_id: number;
    mass_g: number;
};

type BigQueryUrbanAreasColumns = {
    // other columns are missing, there are more but these suffice for our functionality
    cartodb_id: number;
    pop2015: number;
};

export type BigQueryAirportsColumns = {
    // other columns are missing, there are more but these suffice for our functionality
    cartodb_id: number;
    type:
    | "closed"
    | "heliport"
    | "balloonport"
    | "large_airport"
    | "seaplane_base"
    | "small_airport"
    | "medium_airport";
};

type BigQueryEfehrEarthquakeColumns = {
    cartodb_id: number;
    hpvalue: number;
};

type BigQueryCeeLandslidesSusColumns = {
    cartodb_id: number;
    sus: number;
};

type BigQueryCeeLandslidesReliaColumns = {
    cartodb_id: number;
    relia: number;
};

type BigQueryEarthquakeEuropeMiddleEastColumns = {
    cartodb_id: number;
    mw: string; // needs to be parsed to number
};

type BigQueryWindstormsZeusColumns = {
    cartodb_id: number;
    max_gust: number;
};

export type BigQueryLineTypeColumns =
    | BigQueryOilAndNaturalGasColumns
    | BigQueryHistoricalCyclonesColumns
    | BigQueryHistoricalHailColumns;
export type BigQueryCircleTypeColumns =
    | BigQueryUrbanAreasColumns
    | BigQueryTsunamiEventsFrom2000Bc
    | BigQueryHistoricalMeteoritesColumns
    | BigQueryEarthquakeEuropeMiddleEastColumns;
export type BigQueryIconTypeColumns = BigQueryAirportsColumns;
export type BigQueryNormalTypeColumns =
    | BigQueryEfehrEarthquakeColumns
    | BigQueryCeeLandslidesSusColumns
    | BigQueryCeeLandslidesReliaColumns
    | BigQueryWindstormsZeusColumns;

export const LayerCategories: Category[] = [
    {
        label: "naturalHazards",
        subCategories: [
            {
                label: "flood",
                symbol: "💦",
                layers: [
                    {
                        label: "albaniaRiverFlood(AON)",
                        symbol: "🇦🇱",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_albania_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_albania_aon_tileset",
                    },
                    {
                        label: "austriaRiverFlood(HORA)",
                        symbol: "🇦🇹",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Hora,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_austria_hora",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_austria_hora_tileset",
                    },
                    {
                        label: "austriaRiverFlood(AON)",
                        symbol: "🇦🇹",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_austria_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_austria_aon_tileset",
                    },
                    {
                        label: "bosniaRiverFlood(AON)",
                        symbol: "🇧🇦",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_bosnia_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_bosnia_aon_tileset",
                    },
                    {
                        label: "bulgariaRiverFlood(AON)",
                        symbol: "🇧🇬",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_bulgaria_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_bulgaria_aon_tileset",
                    },
                    {
                        label: "czechiaRiverFlood(AON)",
                        symbol: "🇨🇿",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_czechia_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_czechia_aon_tileset",
                    },
                    {
                        label: "croatiaRiverFlood(AON)",
                        symbol: "🇭🇷",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_croatia_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_croatia_aon_tileset",
                    },
                    {
                        label: "germanyRiverFlood(AON)",
                        symbol: "🇩🇪",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_germany_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_germany_aon_tileset",
                    },
                    {
                        label: "hungaryRiverFlood(AON)",
                        symbol: "🇭🇺",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_hungary_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_hungary_aon_tileset",
                    },
                    {
                        label: "italyRiverFlood(AON)",
                        symbol: "🇮🇹",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_italy_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_italy_aon_tileset",
                    },
                    {
                        label: "kosovoRiverFlood(AON)",
                        symbol: "🇽🇰",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_kosovo_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_kosovo_aon_tileset",
                    },
                    {
                        label: "macedoniaRiverFlood(AON)",
                        symbol: "🇲🇰",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_macedonia_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_macedonia_aon_tileset",
                    },
                    {
                        label: "montenegroRiverFlood(AON)",
                        symbol: "🇭🇷",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_montenegro_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_montenegro_aon_tileset",
                    },
                    {
                        label: "polandRiverFlood(AON)",
                        symbol: "🇵🇱",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_poland_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_poland_aon_tileset",
                    },
                    {
                        label: "polandRiverFlood(ATENA)",
                        symbol: "🇵🇱",
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_poland_atena",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_poland_atena_tileset",
                        perilSource: PerilSource.BQ,
                        infoContent: `Zone 1 → our/SwissRe risk class 3
              Zone 2 → our risk class 4
              Zone 3 → our risk class 7
              Zone 4 → our risk class 9`,
                    },
                    {
                        label: "romaniaRiverFlood(AON)",
                        symbol: "🇷🇴",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_romania_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_romania_aon_tileset",
                    },
                    {
                        label: "slovakiaRiverFlood(AON)",
                        symbol: "🇸🇰",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_slovakia_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_slovakia_aon_tileset",
                        infoContent:
                            "AON Impact Forecasting Slovakia Flood Hazard Dec 2017",
                    },
                    {
                        label: "serbiaRiverFlood(AON)",
                        symbol: "🇷🇸",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_serbia_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_serbia_aon_tileset",
                    },
                    {
                        label: "switzerlandLiechtensteinRiverFlood(AON)",
                        symbol: "🇨🇭 🇱🇮",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource:
                            "uniqa-geam.naturalHazards.riverflood_switzerland_liechtenstein_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_switzerland_liechtenstein_aon_tileset",
                    },
                    {
                        label: "ukraineRiverFlood(AON)",
                        symbol: "🇺🇦",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.riverflood_ukraine_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.riverflood_ukraine_aon_tileset",
                    },
                    {
                        label: "riverFlood(CN)",
                        subPeril: SubPerilName.RiverFlood,
                        perilProvider: PerilProvider.Catnet,
                        perilSource: PerilSource.Catnet,
                        lookupSource: "RiskLookUpRiverFlood",
                        tilesSource: "River_Flood_WMS",
                        overviewSource: "River_Flood_Overview_WMS",
                    },
                    {
                        label: "pluvialFlood(CN)",
                        subPeril: SubPerilName.StormFlood,
                        perilProvider: PerilProvider.Catnet,
                        perilSource: PerilSource.Catnet,
                        lookupSource: "RiskLookUpPluvial",
                        tilesSource: "Pluvial_WMS",
                        overviewSource: "Pluvial_Overview_WMS",
                    },
                    {
                        label: "stormSurge(CN)",
                        subPeril: SubPerilName.CoastalFlood,
                        perilProvider: PerilProvider.Catnet,
                        perilSource: PerilSource.Catnet,
                        lookupSource: "RiskLookUpStormSurge",
                        tilesSource: "Storm_Surge_WMS",
                        overviewSource: "Storm_Surge_Overview_WMS",
                    },
                    {
                        label: "coastalFlood(CN)",
                        subPeril: SubPerilName.CoastalFlood,
                        perilProvider: PerilProvider.Catnet,
                        perilSource: PerilSource.Catnet,
                        lookupSource: "RiskLookUpCoastalFlood",
                        tilesSource: "Coastal_Flood_WMS",
                        overviewSource: "Coastal_Flood_Overview_WMS",
                    },
                ],
            },
            {
                label: "earthquake",
                symbol: "🌍",
                layers: [
                    {
                        label: "austriaEarthquake(HORA)",
                        symbol: "🇦🇹",
                        subPeril: SubPerilName.Earthquake,
                        perilProvider: PerilProvider.Hora,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.earthquake_austria_hora",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.earthquake_austria_hora_tileset",
                    },
                    {
                        label: "europeEarthquake(EFEHR)",
                        symbol: "🇪🇺",
                        subPeril: SubPerilName.Earthquake,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.earthquake_europe_efehr",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.earthquake_europe_efehr_tileset",
                    },
                    {
                        label: "europeEarthquake(SHARE)",
                        symbol: "🇪🇺",
                        subPeril: SubPerilName.Earthquake,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.earthquake_europe_share",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.earthquake_europe_share_tileset",
                    },
                    {
                        label: "CEEEarthquake(AON)",
                        subPeril: SubPerilName.Earthquake,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.earthquake_cee_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.earthquake_cee_aon_tileset",
                    },
                    {
                        label: "earthquake(CN)",
                        subPeril: SubPerilName.Earthquake,
                        perilProvider: PerilProvider.Catnet,
                        perilSource: PerilSource.Catnet,
                        lookupSource: "RiskLookUpSeismicHazard",
                        tilesSource: "Seismic_Hazard_WMS",
                        overviewSource: "Seismic_Hazard_Overview_WMS",
                        infoContent:
                            "© 2015 Swiss Re. All rights reserved. The seismic hazard is represented as the pseudo spectral acceleration in units of g at a period of 0.3s for a return period of 475 years. Swiss Re proprietary models SHARE http://www.efehr.org:8080/jetspeed/portal/hazard.psml USGS http://earthquake.usgs.gov/hazards/products/; GSHAP www.seismo.ethz.ch/GSHAP/index.html",
                    },
                ],
            },
            {
                label: "tsunami",
                symbol: "🌊",
                layers: [
                    {
                        label: "tsunami(CN)",
                        subPeril: SubPerilName.Tsunami,
                        perilProvider: PerilProvider.Catnet,
                        perilSource: PerilSource.Catnet,
                        lookupSource: "RiskLookUpTsunami",
                        tilesSource: "Tsunami_WMS",
                        overviewSource: "Tsunami_Overview_WMS",
                        infoContent:
                            "© 2015 Swiss Re. All rights reserved. Swiss Re tsunami hazard zones are available for Southeast Asia, parts of Oceania & the West Coast of the Americas and have been calculated based on available maximum wave height fields of the NCTR Propagation Database by the NOAA Center for Tsunami Research. Swiss Re proprietary models; NCTR Propagation Database by the NOAA Center for Tsunami Research http://nctr.pmel.noaa.gov/propagationdatabase.html Historic earthquake catalogues (NEIC, Centennial); 90 m resolution SRTM DTED1 digital elevation model; SRTM Water Body Data Set",
                    },
                ],
            },
            {
                label: "storm",
                symbol: "⛈️",
                layers: [
                    {
                        label: "windSpeed(CN)",
                        subPeril: SubPerilName.WindSpeed,
                        perilProvider: PerilProvider.Catnet,
                        perilSource: PerilSource.Catnet,
                        lookupSource: "RiskLookUpWindSpeed",
                        tilesSource: "Wind_Speed_WMS",
                        overviewSource: "Windspeed_Overview_WMS",
                        infoContent:
                            "© 2015 Swiss Re. All rights reserved. The wind speed data shows the 3 seconds peak gust with a return period of 50 years. Hazard module of Swiss Re's proprietary wind loss models; Global reanalysis dataset '20th century reanalysis project' designed by the Physical Sciences Division of the Earth System Laboratory of NOAA",
                    },
                    {
                        label: "austriaWindstorm(HORA)",
                        symbol: "🇦🇹",
                        subPeril: SubPerilName.WindSpeed,
                        perilProvider: PerilProvider.Hora,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.windspeed_austria_hora",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.windspeed_austria_hora_tileset",
                    },
                    {
                        label: "CEEWindstorm(AON)",
                        subPeril: SubPerilName.WindSpeed,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.windstorm_cee_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.windstorm_cee_aon_tileset",
                    },
                    {
                        label: "tornadoes(CN)",
                        subPeril: SubPerilName.Tornado,
                        perilProvider: PerilProvider.Catnet,
                        perilSource: PerilSource.Catnet,
                        lookupSource: "RiskLookUpTornadoes",
                        tilesSource: "Tornadoes_WMS",
                        overviewSource: "Tornadoes_Overview_WMS",
                        infoContent:
                            "© 2015 Swiss Re. All rights reserved. Swiss Re tsunami hazard zones are available for Southeast Asia, parts of Oceania & the West Coast of the Americas and have been calculated based on available maximum wave height fields of the NCTR Propagation Database by the NOAA Center for Tsunami Research. Swiss Re proprietary models; NCTR Propagation Database by the NOAA Center for Tsunami Research http://nctr.pmel.noaa.gov/propagationdatabase.html Historic earthquake catalogues (NEIC, Centennial); 90 m resolution SRTM DTED1 digital elevation model; SRTM Water Body Data Set",
                    },
                ],
            },
            {
                label: "hailstorm",
                symbol: "❄️",
                layers: [
                    {
                        label: "hailstorms(CN)",
                        subPeril: SubPerilName.WindSpeed,
                        perilProvider: PerilProvider.Catnet,
                        perilSource: PerilSource.Catnet,
                        lookupSource: "RiskLookUpHailstorms",
                        tilesSource: "Hailstorms_WMS",
                        overviewSource: "Hailstorms_Overview_WMS",
                        infoContent:
                            "© 2015 Swiss Re. All rights reserved. The wind speed data shows the 3 seconds peak gust with a return period of 50 years. Hazard module of Swiss Re's proprietary wind loss models; Global reanalysis dataset '20th century reanalysis project' designed by the Physical Sciences Division of the Earth System Laboratory of NOAA",
                    },
                    {
                        label: "austriaHailstorms(HORA)",
                        symbol: "🇦🇹",
                        subPeril: SubPerilName.Hail,
                        perilProvider: PerilProvider.Hora,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.hail_austria_hora",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.hail_austria_hora_tileset",
                    },
                    {
                        label: "europeHailstorms(AON)",
                        symbol: "🇪🇺",
                        subPeril: SubPerilName.Hail,
                        perilProvider: PerilProvider.Aon,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.hail_europe_aon",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.hail_europe_aon_tileset",
                    },
                ],
            },
            {
                label: "wildfire",
                symbol: "🌲",
                layers: [
                    {
                        label: "wildfires(CN)",
                        subPeril: SubPerilName.Fire,
                        perilProvider: PerilProvider.Catnet,
                        perilSource: PerilSource.Catnet,
                        lookupSource: "RiskLookUpWildfires",
                        tilesSource: "Wildfires_WMS",
                        overviewSource: "Wildfires_Overview_WMS",
                        infoContent:
                            "© 2015 Swiss Re. All rights reserved. Based on the assumption, that a very small percentage of all wildfires (those with high intensity > 600 MW) is responsible for a large part of the losses, the wildfire map shows the numbers of those fires in the period November 2000 – December 2011. MODIS 'MCD14ML' dataset http://modis.gsfc.nasa.gov/data/dataprod/nontech/ MOD14.php; Global GRID of 720 x 1440 cells, provided by the University of Maryland",
                    },
                ],
            },
            {
                label: "landslide",
                symbol: "🌫️",
                layers: [
                    {
                        label: "austriaLandslides(HORA)",
                        symbol: "🇦🇹",
                        subPeril: SubPerilName.Landslide,
                        perilProvider: PerilProvider.Hora,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.landslide_austria_hora",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.landslide_austria_hora_tileset",
                    },
                    {
                        label: "CEELandslides(NASA)",
                        perilSource: PerilSource.ColumbiaWms,
                        tilesSource: "ndh:ndh-landslide-hazard-distribution",
                    },
                    {
                        label: "CEELandslides(RELIA)",
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.landslide_cee_relia",
                    },
                    {
                        label: "CEELandslides(SUS)",
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.landslide_cee_sus",
                    },
                ],
            },
            {
                label: "snowLevel",
                symbol: "☃️",
                layers: [
                    {
                        label: "austriaAvalanches(HORA)",
                        symbol: "🇦🇹",
                        subPeril: SubPerilName.StandaloneAvalanche,
                        perilProvider: PerilProvider.Hora,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.avalanche_austria_hora",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.avalanche_austria_hora_tileset",
                    },
                    {
                        label: "austriaSnowLoad(HORA)",
                        symbol: "🇦🇹",
                        subPeril: SubPerilName.StandaloneSnowLoad,
                        perilProvider: PerilProvider.Hora,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.snowload_austria_hora",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.snowload_austria_hora_tileset",
                    },
                ],
            },
            {
                label: "lightning",
                symbol: "⚡",
                layers: [
                    {
                        label: "austriaLightning(HORA)",
                        symbol: "🇦🇹",
                        subPeril: SubPerilName.Lightning,
                        perilProvider: PerilProvider.Hora,
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.naturalHazards.lightning_austria_hora",
                        lookupSourceTileSet:
                            "uniqa-geam.naturalHazards__tilesets.lightning_austria_hora_tileset",
                    },
                    {
                        label: "lightning(CN)",
                        subPeril: SubPerilName.Lightning,
                        perilProvider: PerilProvider.Catnet,
                        perilSource: PerilSource.Catnet,
                        lookupSource: "RiskLookUpLightning",
                        tilesSource: "Lightning_WMS",
                        overviewSource: "Lightning_Overview_WMS",
                    },
                ],
            },
            {
                label: "volcanicEruption",
                symbol: "🌋",
                layers: [
                    {
                        label: "volcanoes(CN)",
                        subPeril: SubPerilName.Volcano,
                        perilProvider: PerilProvider.Catnet,
                        perilSource: PerilSource.Catnet,
                        lookupSource: "RiskLookUpVolcano",
                        tilesSource: "Volcano_WMS",
                        overviewSource: "Volcano_Overview_WMS",
                    },
                ],
            },
        ],
    },
    {
        label: "manmadeHazards",
        layers: [
            {
                label: "terrorism(AON)",
                perilSource: PerilSource.BQ,
                lookupSource: "uniqa-geam.manmadeHazards.terrorism_aon",
            },
            {
                label: "politicalViolence(AON)",
                perilSource: PerilSource.BQ,
                lookupSource: "uniqa-geam.manmadeHazards.political_violence_aon",
            },
        ],
    },
    {
        label: "insuranceEconomics",
        layers: [
            {
                label: "populationDensity(NASA)",
                perilSource: PerilSource.BQ,
                lookupSource:
                    "uniqa-geam.insuranceAndEconomics.population_density_nasa",
            },
            {
                label: "urbanAreas",
                perilSource: PerilSource.BQ,
                lookupSource:
                    "uniqa-geam.insuranceAndEconomics.urban_areas_various_sources",
            },
            /* TODO: Google POIs are not being done now
                  {
                    label: 'fireStations',
                    symbol: '🚒',
                  },
                  {
                    label: 'policeStations',
                    symbol: '👮‍♀️',
                  },
                  {
                    label: 'hospitals',
                    symbol: '🚑',
                  },
                  {
                    label: 'railroads',
                    symbol: '🚂',
                  },
                  */
            {
                label: "airports",
                symbol: "✈️",
                perilSource: PerilSource.BQ,
                lookupSource:
                    "uniqa-geam.insuranceAndEconomics.airports_various_sources",
            },
            {
                label: "oilGasPipelines",
                symbol: "🛢️",
                perilSource: PerilSource.BQ,
                lookupSource:
                    "uniqa-geam.insuranceAndEconomics.oil_and_natural_gas_infrastructure_various_sources",
            },
            {
                label: "powerPlants",
                symbol: "🔋",
                perilSource: PerilSource.BQ,
                lookupSource:
                    "uniqa-geam.insuranceAndEconomics.power_plants_various_sources",
            },
            {
                label: "dams",
                symbol: "💧",
                perilSource: PerilSource.BQ,
                lookupSource:
                    "uniqa-geam.insuranceAndEconomics.dams_europe_various_sources",
            },
            {
                label: "ports",
                symbol: "⚓",
                perilSource: PerilSource.BQ,
                lookupSource: "uniqa-geam.insuranceAndEconomics.ports_various_sources",
            },
        ],
    },
    {
        label: "additionalMaps",
        subCategories: [
            {
                label: "natura2000",
                layers: [
                    {
                        label: "natura2000",
                        symbol: "🇪🇺",
                        perilSource: PerilSource.BQ,
                        lookupSource: "uniqa-geam.additionalMaps.eu_natura_2000",
                        lookupSourceTileSet:
                            "uniqa-geam.additionalMaps__tilesets.eu_natura_2000_tileset",
                        infoContent:
                            "Natura 2000 is an ecological network composed of sites designated under the Birds Directive and the Habitats Directive. The aim of Natura 2000 is to assure the long-term survival of Europe's most valuable and threatened species and habitats and to promote sustainable land management.",
                    },
                ],
            },
            {
                label: "historicalEvents",
                subCategories: [
                    {
                        label: "historicalEventsFlood",
                        symbol: "💦",
                        layers: [
                            {
                                label: "historizedRiverFlood(CN)",
                                perilProvider: PerilProvider.Catnet,
                                perilSource: PerilSource.Catnet,
                                lookupSource: "RiskLookUpHistorizedRiverFlood",
                                tilesSource: "Historized_River_Flood_WMS",
                                overviewSource: "Historized_River_Flood_Overview_WMS",
                            },
                            {
                                label: "historicalFloodRegions",
                                perilSource: PerilSource.BQ,
                                lookupSource:
                                    "uniqa-geam.additionalMaps.historical_flood_regions_various_sources",
                            },
                        ],
                    },
                    {
                        label: "historicalEventsEarthquake",
                        symbol: "🌍",
                        layers: [
                            {
                                label: "earthquakeEuropeMidEast",
                                perilSource: PerilSource.BQ,
                                lookupSource:
                                    "uniqa-geam.additionalMaps.earthquake_europe_middle_east_various_sources",
                            },
                        ],
                    },
                    {
                        label: "historicalEventsTsunami",
                        symbol: "🌊",
                        layers: [
                            {
                                label: "historizedTsunami(CN)",
                                perilProvider: PerilProvider.Catnet,
                                perilSource: PerilSource.Catnet,
                                tilesSource: "Historized_Tsunami_WMS",
                                overviewSource: "Historized_Tsunami_Overview_WMS",
                            },
                            {
                                label: "tsunamiEvents(2000BC-2015)",
                                perilSource: PerilSource.BQ,
                                lookupSource:
                                    "uniqa-geam.additionalMaps.tsunami_events_from_2000bc_various_sources",
                            },
                        ],
                    },
                    {
                        label: "historicalEventsCyclones",
                        symbol: "🌪️",
                        layers: [
                            {
                                label: "historicalCyclones",
                                perilSource: PerilSource.BQ,
                                lookupSource:
                                    "uniqa-geam.additionalMaps.historical_cyclones_tracks",
                                lookupSourceTileSet:
                                    "uniqa-geam.additionalMaps__tilesets.historical_cyclones_tracks_tileset",
                            },
                        ],
                    },
                    {
                        label: "historicalEventsHailstorm",
                        symbol: "❄️",
                        layers: [
                            {
                                label: "historicalHailstormsUSA",
                                perilSource: PerilSource.BQ,
                                lookupSource: "uniqa-geam.additionalMaps.historical_hail_usa",
                            },
                        ],
                    },
                    {
                        label: "historicalEventsWindstorms",
                        symbol: "⛈️",
                        layers: [
                            {
                                label: "zeus2017(MedgustsAON)",
                                perilSource: PerilSource.BQ,
                                lookupSource:
                                    "uniqa-geam.additionalMaps.windstorm_zeus_footprint_medgusts_aon",
                            },
                            {
                                label: "herwart2017",
                                perilSource: PerilSource.BQ,
                                lookupSource:
                                    "uniqa-geam.additionalMaps.windstorm_herwart_footprint",
                            },
                            {
                                label: "ophelia2017",
                                perilSource: PerilSource.BQ,
                                lookupSource:
                                    "uniqa-geam.additionalMaps.windstorm_ophelia_footprint",
                            },
                            {
                                label: "eberhard2019",
                                perilSource: PerilSource.BQ,
                                lookupSource:
                                    "uniqa-geam.additionalMaps.windstorm_eberhard_footprint",
                            },
                            {
                                label: "ciaraSabine2020",
                                perilSource: PerilSource.BQ,
                                lookupSource:
                                    "uniqa-geam.additionalMaps.windstorm_ciara_sabine_footprint",
                            },
                            {
                                label: "dennis2020",
                                perilSource: PerilSource.BQ,
                                lookupSource:
                                    "uniqa-geam.additionalMaps.windstorm_dennis_footprint",
                            },
                        ],
                    },
                    {
                        label: "historicalEventsMeteorites",
                        symbol: "🎇",
                        layers: [
                            {
                                label: "meteoriteStrikes",
                                perilSource: PerilSource.BQ,
                                lookupSource:
                                    "uniqa-geam.additionalMaps.historical_meteorites_various_sources",
                            },
                        ],
                    },
                    {
                        label: "historicalEventsTerrorism",
                        symbol: "💣",
                        layers: [
                            {
                                label: "terrorismEvents(1975-2015)",
                                perilSource: PerilSource.BQ,
                                lookupSource:
                                    "uniqa-geam.additionalMaps.terrorism_events_various_sources",
                            },
                        ],
                    },
                ],
            },
            {
                label: "alertsUNEU",
                subCategories: [
                    {
                        label: "GDACSOrg",
                        layers: [
                            {
                                label: "gdacsFlood",
                                symbol: "💦",
                                subPeril: SubPerilName.RiverFlood,
                                perilSource: PerilSource.Gdacs,
                                lookupSource: "https://www.gdacs.org/xml/gdacsFL.geojson",
                            },
                            {
                                label: "gdacsEarthquakes",
                                symbol: "🌍",
                                subPeril: SubPerilName.Earthquake,
                                perilSource: PerilSource.Gdacs,
                                lookupSource: "https://www.gdacs.org/xml/gdacsEQ.geojson",
                            },
                            {
                                label: "gdacsTropicalCyclones",
                                symbol: "🌪️",
                                subPeril: SubPerilName.WindSpeed,
                                perilSource: PerilSource.Gdacs,
                                lookupSource: "https://www.gdacs.org/xml/gdacsTC.geojson",
                            },
                            {
                                label: "gdacsVolcanicEruptions",
                                symbol: "🌋",
                                subPeril: SubPerilName.Volcano,
                                perilSource: PerilSource.Gdacs,
                                lookupSource: "https://www.gdacs.org/xml/gdacsVO.geojson",
                            },
                            /* Not using for now - probably deprecated by gdacs
                                          {
                                            label: 'gdacsRain',
                                            symbol: '☔',
                                            perilSource: PerilSource.Gdacs,
                                            lookupSource: '',
                                          },
                                          {
                                            label: 'gdacsClouds',
                                            symbol: '☁️',
                                            perilSource: PerilSource.Gdacs,
                                            lookupSource: '',
                                          },
                                          */
                        ],
                    },
                ],
            },
        ],
    },
    {
        label: "emergencies",
        extras: '<span class="developing-crisis-span"></span>',
        subCategories: [
            {
                label: "russiaUkraine",
                subCategories: [
                    {
                        label: "liveMap",
                        layers: [
                            {
                                label: "liveUaMap",
                                subPeril: SubPerilName.Explosion,
                                perilSource: PerilSource.Ukraine,
                                geoJSON: "https://europe-central2-uniqa-geam.cloudfunctions.net/ukraineWarGeojsonLayer"
                            },
                        ],
                    },
                ],
            },
        ],
    },
];

// used in `getLabel` alongside `objectPath`
export const LayerTranslations = {
    naturalHazards: {
        label: "Natural Hazards",
        subCategories: {
            flood: {
                label: "Flood",
                layers: {
                    "albaniaRiverFlood(AON)": "Albania River Flood (AON)",
                    "austriaRiverFlood(HORA)": "Austria River Flood (HORA)",
                    "austriaRiverFlood(AON)": "Austria River Flood (AON)",
                    "bosniaRiverFlood(AON)": "Bosnia River Flood (AON)",
                    "bulgariaRiverFlood(AON)": "Bulgaria River Flood (AON)",
                    "czechiaRiverFlood(AON)": "Czechia River Flood (AON)",
                    "croatiaRiverFlood(AON)": "Croatia River Flood (AON)",
                    "germanyRiverFlood(AON)": "Germany River Flood (AON)",
                    "hungaryRiverFlood(AON)": "Hungary River Flood (AON)",
                    "italyRiverFlood(AON)": "Italy River Flood (AON)",
                    "kosovoRiverFlood(AON)": "Kosovo River Flood (AON)",
                    "macedoniaRiverFlood(AON)": "Macedonia River Flood (AON)",
                    "montenegroRiverFlood(AON)": "Montenegro River Flood (AON)",
                    "polandRiverFlood(AON)": "Poland River Flood (AON)",
                    "polandRiverFlood(ATENA)": "Poland River Flood (ATENA)",
                    "romaniaRiverFlood(AON)": "Romania River Flood (AON)",
                    "slovakiaRiverFlood(AON)": "Slovakia River Flood (AON)",
                    "serbiaRiverFlood(AON)": "Serbia River Flood (AON)",
                    "switzerlandLiechtensteinRiverFlood(AON)":
                        "Switzerland/Liechtenstein River Flood (AON)",
                    "ukraineRiverFlood(AON)": "Ukraine River Flood (AON)",
                    "riverFlood(CN)": "River Flood (CN)",
                    "pluvialFlood(CN)": "Pluvial Flood (CN)",
                    "stormSurge(CN)": "Storm Surge (CN)",
                    "coastalFlood(CN)": "Coastal Flood (CN)",
                },
            },
            earthquake: {
                label: "Earthquake",
                layers: {
                    "austriaEarthquake(HORA)": "Austria Earthquake (HORA)",
                    "europeEarthquake(EFEHR)": "Europe Earthquake (EFEHR)",
                    "europeEarthquake(SHARE)": "Europe Earthquake (SHARE)",
                    "CEEEarthquake(AON)": "CEE Earthquake (AON)",
                    "earthquake(CN)": "Earthquake (CN)",
                },
            },
            tsunami: {
                label: "Tsunami",
                layers: {
                    "tsunami(CN)": "Tsunami (CN)",
                },
            },
            storm: {
                label: "Storm",
                layers: {
                    "windSpeed(CN)": "Wind Speed (CN)",
                    "austriaWindstorm(HORA)": "Austria Windstorm (HORA)",
                    "CEEWindstorm(AON)": "CEE Windstorm (AON)",
                    "tornadoes(CN)": "Tornadoes (CN)",
                },
            },
            hailstorm: {
                label: "Hailstorm",
                layers: {
                    "hailstorms(CN)": "Hailstorms (CN)",
                    "austriaHailstorms(HORA)": "Austria Hailstorms (HORA)",
                    "europeHailstorms(AON)": "Europe Hailstorms (AON)",
                },
            },
            wildfire: {
                label: "Wildfire",
                layers: {
                    "wildfires(CN)": "Wildfires (CN)",
                },
            },
            landslide: {
                label: "Landslide",
                layers: {
                    "austriaLandslides(HORA)": "Austria Landslides (HORA)",
                    "CEELandslides(NASA)": "CEE Landslides (NASA)",
                    "CEELandslides(RELIA)": "CEE Landslides (RELIA)",
                    "CEELandslides(SUS)": "CEE Landslides (SUS)",
                },
            },
            snowLevel: {
                label: "Snow Level",
                layers: {
                    "austriaAvalanches(HORA)": "Austria Avalanches (HORA)",
                    "austriaSnowLoad(HORA)": "Austria Snow Load (HORA)",
                },
            },
            lightning: {
                label: "Lightning",
                layers: {
                    "austriaLightning(HORA)": "Austria Lightning (HORA)",
                    "lightning(CN)": "Lightning (CN)",
                },
            },
            volcanicEruption: {
                label: "Volcanic Eruption",
                layers: {
                    "volcanoes(CN)": "Volcanoes (CN)",
                },
            },
        },
    },
    manmadeHazards: {
        label: "Manmade Hazards",
        layers: {
            "terrorism(AON)": "Terrorism (AON)",
            "politicalViolence(AON)": "Political violence (AON)",
        },
    },
    insuranceEconomics: {
        label: "Insurance & Economics",
        layers: {
            "populationDensity(NASA)": "Population density (NASA)",
            urbanAreas: "Urban areas",
            fireStations: "Fire stations",
            policeStations: "Police stations",
            hospitals: "Hospitals",
            railroads: "Railroads",
            airports: "Airports",
            oilGasPipelines: "Oil & gas pipelines",
            powerPlants: "Power plants",
            dams: "Dams",
            ports: "Ports",
        },
    },
    additionalMaps: {
        label: "Additional Maps",
        subCategories: {
            natura2000: {
                label: "Natura 2000",
                layers: {
                    natura2000: "Natura 2000",
                },
            },
            historicalEvents: {
                label: "Historical Events",
                subCategories: {
                    historicalEventsFlood: {
                        label: "Flood",
                        layers: {
                            "historizedRiverFlood(CN)": "Historized River Flood (CN)",
                            historicalFloodRegions: "Historical flood regions",
                        },
                    },
                    historicalEventsEarthquake: {
                        label: "Earthquake",
                        layers: {
                            earthquakeEuropeMidEast: "Earthquake Europe/MidEast",
                        },
                    },
                    historicalEventsTsunami: {
                        label: "Tsunami",
                        layers: {
                            "historizedTsunami(CN)": "Historized Tsunami (CN)",
                            "tsunamiEvents(2000BC-2015)": "Tsunami events (2000BC - 2015)",
                        },
                    },
                    historicalEventsCyclones: {
                        label: "Cyclones",
                        layers: {
                            historicalCyclones: "Historical cyclones",
                        },
                    },
                    historicalEventsHailstorm: {
                        label: "Hailstorm",
                        layers: {
                            historicalHailstormsUSA: "Historical hailstorms USA",
                        },
                    },
                    historicalEventsWindstorms: {
                        label: "Windstorms",
                        layers: {
                            "zeus2017(MedgustsAON)": "Zeus 2017 (Medgusts AON)",
                            herwart2017: "Herwart 2017",
                            ophelia2017: "Ophelia 2017",
                            eberhard2019: "Eberhard 2019",
                            ciaraSabine2020: "Ciara/Sabine 2020",
                            dennis2020: "Dennis 2020",
                        },
                    },
                    historicalEventsMeteorites: {
                        label: "Meteorites",
                        layers: {
                            meteoriteStrikes: "Meteorite strikes",
                        },
                    },
                    historicalEventsTerrorism: {
                        label: "Terrorism",
                        layers: {
                            "terrorismEvents(1975-2015)": "Terrorism events (1975 - 2015)",
                        },
                    },
                },
            },
            alertsUNEU: {
                label: "Alerts UN & EU",
                subCategories: {
                    GDACSOrg: {
                        label: "GDACS.org",
                        layers: {
                            gdacsFlood: "Flood",
                            gdacsEarthquakes: "Earthquakes",
                            gdacsTropicalCyclones: "Tropical cyclones",
                            gdacsVolcanicEruptions: "Volcanic eruptions",
                            gdacsRain: "Rain",
                            gdacsClouds: "Clouds",
                        },
                    },
                },
            },
        },
    },
    emergencies: {
        label: 'Emergencies',
        subCategories: {
            russiaUkraine: {
                label: "Russo-Ukrainian War",
                subCategories: {
                    liveMap: {
                        label: "Live Map",
                        layers: {
                            liveUaMap: "Live UA Map 🇺🇦"
                        }
                    }
                }
            }
        }
    }
};

/**
 * NOT WORKING UNTIL CORS IS FIXED
 * Access to fetch at 'https://geam-v2.uniqa-cluster.eu/api/img-proxy/http...' from origin 'http://localhost:4200' has been
 * blocked by CORS policy: Response to preflight request doesn't pass access control check: No 'Access-Control-Allow-Origin' header is present on the requested resource.
 * If an opaque response serves your needs, set the request's mode to 'no-cors' to fetch the resource with CORS disabled.
 * @param targetUrl
 * @param fetchArgs
 * @returns
 */
export const fetchProxiedImageBlob = async (
    targetUrl: string,
    fetchArgs?: Record<string, any>
): Promise<Blob> => {
    // the target URL has to be encoded b/c it includes URL query params
    // which would otherwise interfere w/ the next api route's parser
    const targetEncodedUrl = encodeURIComponent(targetUrl);

    const r = await fetch(
        `${environment.gmaps_settings.GEAM_PROXY_URL_BASE}/api/img-proxy/${targetEncodedUrl}`,
        {
            headers: {
                ...fetchArgs?.headers,
                ...(targetUrl.includes("swissre") ? {
                    "ocp-apim-subscription-key":
                        environment.gmaps_settings.catnet.CATNET_SUBSCRIPTION_KEY,
                } : {}),
                Accept: "image/png",
            },
            signal: fetchArgs?.signal
        },
    );
    return await r?.blob()
};

export async function fetchCatnetImageBlob(url: string) {
    console.debug(
        "Will attempt to run the following URL thru 'fetchCatnetImageBlob'",
        {
            url,
        }
    );

    try {
        const resp = await fetch(url, {
            method: "GET",
            headers: {
                "Ocp-Apim-Subscription-Key":
                    environment.gmaps_settings.catnet.CATNET_SUBSCRIPTION_KEY,
                Accept: "image/*",
            },
            // // https://stackoverflow.com/a/51638033/8160318
            // responseType: 'arraybuffer',
        });

        return resp;
    } catch (err) {
        console.warn("img-proxy error in fetchImageBlob", { url, err });
        return undefined;
    }
}
