import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewEncapsulation,
} from "@angular/core";
import {
  MAP_LAYERS_FOR_ANT_DESIGN,
  findLayerByLabel,
} from "./constants";
import { GoogleMapsService } from "../services/gmaps-service.service";
import { Layer } from "./geam2-constants";

import { setDefaultCredentials } from "@deck.gl/carto/typed";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-geam-layers",
  templateUrl: "./geam-layers.component.html",
  styleUrls: ["./geam-layers.component.scss"],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GeamLayersComponent implements OnInit {
  constructor(private mapService: GoogleMapsService) {
    setDefaultCredentials({
      accessToken: environment.gmaps_settings.carto.CARTO_ACCESS_TOKEN,
      apiBaseUrl: environment.gmaps_settings.carto.CARTO_API_BASE_URL,
    });
  }

  visible = true;

  nodes = MAP_LAYERS_FOR_ANT_DESIGN.slice();

  private selected_layer_labels = [];

  get currentLength() {
    const len = this.selected_layer_labels.length;
    if (!len) {
      return "";
    }

    return `&nbsp;<strong>(${len})</strong>`;
  }

  /**
   * Mirroring the ng model of selected checkboxes
   * @param layer 
   * @returns Boolean
   */
  isAmongChecked(layer: Layer) {
    return this.selected_layer_labels.includes(layer.label);
  }

  toggleLayer(visible: boolean, layer: Layer) {
    const { label } = layer;

    if (!visible) {
      this.selected_layer_labels = this.selected_layer_labels.filter(
        (val) => val !== label
      );
      this.mapService.generateLayers(this.getFullSelectedLayers());
      return;
    }

    // sanity check
    if (this.selected_layer_labels.includes(label)) {
      return;
    }

    this.selected_layer_labels.push(label);
    this.mapService.generateLayers(this.getFullSelectedLayers());
  }

  /**
   * Iterates selected layer checkbox labels and returns the full `Layer`s
   * @returns Layer[]
   */
  public getFullSelectedLayers() {
    return this.selected_layer_labels.map((label) => findLayerByLabel(label));
  }

  ngOnInit() { }
}
