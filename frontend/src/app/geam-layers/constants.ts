import { Layer, LayerCategories, LayerTranslations } from "./geam2-constants";
import objectPath from "object-path";
import objectScan from "object-scan";

export const MAP_LAYERS_FOR_ANT_DESIGN = ((): {
  parent: any;
  children: {
    submenu: any;
    extras?: string;
    children: {
      value?: string;
      label?: string;
      layer?: Layer;
      // in case of (deeply) nested layers
      submenu?: any;
      children?: {
        value: string;
        label: string;
        layer: Layer;
      }[];
    }[];
  }[];
}[] => {
  const getLabel = ({
    categoryPath,
    layer,
    symbol,
  }: {
    categoryPath: string[];
    layer?: Layer;
    symbol?: string;
  }) => {
    let translationPath = `${categoryPath.join(".subCategories.")}`;
    translationPath += layer ? `.layers.${layer.label}` : `.label`;

    const label = objectPath.get(LayerTranslations, translationPath);
    const labelSymbol = symbol || layer?.symbol;

    return labelSymbol ? `${labelSymbol} ${label}` : label;
  };

  return LayerCategories.map((parent) => {
    return {
      extras: parent?.extras,
      parent: getLabel({
        categoryPath: [parent.label],
        symbol: parent.symbol,
      }),
      children: parent.subCategories
        ? parent.subCategories.map((subcategory) => {
          return {
            submenu: getLabel({
              categoryPath: [parent.label, subcategory.label],
              symbol: subcategory.symbol,
            }),
            children: subcategory.layers
              ? subcategory.layers.map((layer) => {
                const label = getLabel({
                  categoryPath: [parent.label, subcategory.label],
                  symbol: layer.symbol,
                  layer,
                });
                return {
                  value: label,
                  label,
                  layer,
                };
              })
              : subcategory.subCategories.map((subSubCategory) => {
                return {
                  submenu: getLabel({
                    categoryPath: [
                      parent.label,
                      subcategory.label,
                      subSubCategory.label,
                    ],
                    symbol: subSubCategory.symbol,
                  }),
                  children: subSubCategory.layers?.map((layer) => {
                    const label = getLabel({
                      categoryPath: [
                        parent.label,
                        subcategory.label,
                        subSubCategory.label,
                      ],
                      symbol: layer.symbol,
                      layer,
                    });
                    return {
                      value: label,
                      label,
                      layer,
                    };
                  }),
                };
              }),
          };
        })
        : [
          {
            // empty parent by design
            submenu: "",
            children: parent.layers?.map((layer) => {
              const label = getLabel({
                categoryPath: [parent.label],
                symbol: layer.symbol,
                layer,
              });

              return {
                value: label,
                label,
                layer,
              };
            }),
          },
        ],
    };
  });
})();

export function findLayerNameByLabel(label: string) {
  let foundLayerName: string;

  objectScan(["**"], {
    rtn: "key",
    filterFn: (key) => {
      let innerVal = key.property;
      if (innerVal === label) {
        foundLayerName = key.value;
        return true; // Stop searching once found
      }

      innerVal = key.property;
      if (innerVal === label) {
        foundLayerName = key.value;
        return true; // Stop searching once found
      }
      return false;
    },
  })(LayerTranslations);

  return foundLayerName;
}

export function findLayerByLabel(label: string) {
  let foundLayer: Layer;

  objectScan(["**"], {
    rtn: "key",
    filterFn: (key) => {
      const innerVal = key.value;
      if (innerVal === label) {
        foundLayer = key.parent;
        return true; // Stop searching once found
      }
      return false;
    },
  })(LayerCategories);

  // objectScan isn't perfect and e.g. natura2000 must be caught like this
  if (Array.isArray((foundLayer as any)?.layers)) {
    return (foundLayer as any).layers[0];
  }

  return foundLayer;
}

export const findLayerByInnerKey = (presentedLayerName: string): Layer => {
  function findLabelByPresentedLayerName(value: string) {
    let foundKey = null;

    // strip emojis which aren't present in the config of a `Layer`'s label
    value = value.replace(/\p{Emoji_Presentation}/gu, "").trim();

    objectScan(["**"], {
      rtn: "key",
      filterFn: (key: any, v: any) => {
        const innerVal = key.value;
        if (v === value || innerVal === value) {
          foundKey = key.property;
          return true; // Stop searching once found
        }
        return false;
      },
    })(LayerTranslations);

    return foundKey;
  }

  const label = findLabelByPresentedLayerName(presentedLayerName);
  const layer = findLayerByLabel(label);
  return layer;
};
