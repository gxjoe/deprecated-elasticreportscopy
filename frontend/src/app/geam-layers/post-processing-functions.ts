import {
  FeatureCollection,
  MultiPolygon,
  Point,
  featureCollection,
} from "@turf/turf";
import {
  GeoJsonLayerProps,
  IconLayerProps,
  PathLayerProps,
  SolidPolygonLayerProps,
} from "@deck.gl/layers/typed";
import { Feature, Geometry } from "geojson";
import { Color } from "@deck.gl/core/typed";
import { hexToRgb_, hexToRgba } from "../charts/common/constants";
export interface LiveUaMapFeatureMetadata {
  datetime: string;
  isRecent: boolean;
  icon: string;

  location: string;
  message: string;
  link: string;
}

type FeatureLiveUaMapFeatureMetadata = Feature<
  Geometry,
  LiveUaMapFeatureMetadata
>;

export interface ApiResponse {
  venues: FeatureCollection<Point, LiveUaMapFeatureMetadata>;
  areas: FeatureCollection<MultiPolygon>;
}

/**
 * Process the response from the `ukraineWarGeojsonLayer` cloud function.
 * Added in UDGB-38
 * @param data
 * @returns
 */
export const processLiveUaMap = (data: ApiResponse) => {
  const { venues, areas } = data;

  return featureCollection([].concat(venues.features).concat(areas.features));
};

export const prepareLiveUaMapDeckglProps = (
  handleInfoWindow: Function
): Partial<GeoJsonLayerProps> => {
  return {
    pickable: true,
    autoHighlight: true,

    iconSizeUnits: "pixels",
    getIconSize: 36,
    pointType: "icon",

    onClick: (pickingInfo) => {
      handleInfoWindow(pickingInfo);
    },

    // https://deck.gl/docs/api-reference/layers/geojson-layer#sub-layers
    // These props are part of the config of the CompositeLayer
    // and allow us to individually style polygons & point (icon) features.
    // Truly a life saver because otherwise, we'd have to split this geojson layer
    // into multiple separate
    _subLayerProps: {
      // POLYGONS
      "polygons-fill": {
        // https://deck.gl/docs/api-reference/layers/solid-polygon-layer
        pickable: false,
        opacity: 0.2,
        getFillColor: (_: FeatureLiveUaMapFeatureMetadata) => {
          return hexToRgb_("#FF0000") as Color;
        },
      } as Partial<SolidPolygonLayerProps>,
      "polygons-stroke": {
        // https://deck.gl/docs/api-reference/layers/path-layer
        pickable: false,
        opacity: 0.6,
        getColor: (_: FeatureLiveUaMapFeatureMetadata) => {
          return hexToRgb_("#FF0000") as Color;
        },
      } as Partial<PathLayerProps>,

      // POINTS (ICONS)
      "points-icon": {
        // https://deck.gl/docs/api-reference/layers/icon-layer
        getIcon: (f: FeatureLiveUaMapFeatureMetadata) => {
          const { icon, isRecent } = f.properties;
          const iconName = icon.split("/").pop();

          // a 1-to-1 mirror stored in the `uniqa-geam` project's GCS
          const gcsUrl = `https://storage.googleapis.com/live-ua-map-icons/${iconName}`;
          const iconUrl = isRecent
            ? // pass thru this proxy anyways to circumvent CORS errors (don't ask me, it's weird)
            `https://europe-central2-uniqa-geam.cloudfunctions.net/geamPngOpacityReducer?url=${gcsUrl}&opacity=1`
            : // reduce opacity of icon if it's not recent
            `https://europe-central2-uniqa-geam.cloudfunctions.net/geamPngOpacityReducer?url=${gcsUrl}&opacity=0.5`;

          return {
            url: iconUrl,
            height: 38,
            width: 38,
          };
        },
      } as Partial<IconLayerProps>,
    },
  };
};
