export enum PerilName {
    Flexa = "flexa",
    OtherNatCat = "otherNatCat", // camel case for historical reasons
    Flood = "flood",
    MaliciousDamage = "malicious_damage",
    Storm = "storm",
    StrikeRiotsCivilCommotions = "srcc_-_strike,_riots,_civil_commotions",
    Earthquake = "earthquake",
    VehicleImpactSmokeSonicBoom = "vehicle_impact,_smoke,_sonic_boom",
    Hail = "hail",
    // Missing from sheet spec for now
    StandaloneAvalanche = "avalanche",
    SnowLoad = "snowLoad",
    // Missing from sheet spec for now
    StandaloneLandslide = "landslide",
}

export enum SubPerilName {
    // flexa
    Fire = "fire",
    Lightning = "lightning",
    Explosion = "explosion",
    Aircraft = "aircraft",
    // otherNatCat
    SnowPressure = "snowPressure",
    Landslide = "landslide",
    Avalanche = "avalanche",
    Volcano = "volcano",
    // flood
    RiverFlood = "riverFlood",
    StormFlood = "stormFlood",
    CoastalFlood = "coastalFlood",

    // maliciousDamage
    MaliciousDamage = "maliciousDamage",

    // storm
    WindSpeed = "windSpeed",
    Tornado = "tornado",

    // strikeRiotsCivilCommotions
    Strikes = "strikes",
    Riots = "riots",
    CivilCommotion = "civilCommotion",

    // earthquake
    Earthquake = "earthquake",
    Tsunami = "tsunami",

    // vehicleImpactSmokeSonicBoom
    VehicleImpact = "vehicleImpact",
    Smoke = "smoke",
    SupersonicBoom = "supersonicBoom",

    // hail
    Hail = "hail",

    // standaloneAvalanche
    StandaloneAvalanche = "avalanche",

    // snowLoad
    StandaloneSnowLoad = "snowLoad",

    // standaloneLandslide
    StandaloneLandslide = "landslide",
}

export const SubPerilNamesInArray = Object.values(SubPerilName);

export interface PerilData {
    peril: PerilName;
    riskClass: number;
    provider: string;
    subPerilData: SubPerilData[];
}

export enum PerilProvider {
    CbnManual = "cbnmanual", // originally set in CBN
    GeamManual = "geammanual", // manually set in GEAM

    // bigquery lookups
    Aon = "aon",
    Share = "share", // earthquake only - mostly West & CEE Europe
    Hora = "hora",

    // separate commercial api lookups
    Catnet = "catnet",
}

export enum PerilSource {
    BQ = "bq",
    Catnet = "catnet",
    Gdacs = "gdacs",
    ColumbiaWms = "columbiaWms",
    Ukraine = "ukraine"
}

export interface SubPerilData {
    subPeril: SubPerilName;
    riskClass: number;
    provider: PerilProvider;
}

export const PerilsNamesToTitles = {
    [PerilName.Flexa]: "perilsTable.perils.flexa",
    [PerilName.OtherNatCat]: "perilsTable.perils.onc",
    [PerilName.Storm]: "perilsTable.perils.storm",
    [PerilName.Earthquake]: "perilsTable.perils.earthquake",
    [PerilName.Flood]: "perilsTable.perils.flood",
    [PerilName.Hail]: "perilsTable.perils.hail",
};

export const SubperilsNamesToTitles = {
    [SubPerilName.Fire]: "perilsTable.subPerils.fire",
    [SubPerilName.Lightning]: "perilsTable.subPerils.lightning",
    [SubPerilName.Explosion]: "perilsTable.subPerils.explosion",
    [SubPerilName.Aircraft]: "perilsTable.subPerils.aircraft",
    [SubPerilName.WindSpeed]: "perilsTable.subPerils.windSpeed",
    [SubPerilName.Tornado]: "perilsTable.subPerils.tornado",
    [SubPerilName.RiverFlood]: "perilsTable.subPerils.riverFlood",
    [SubPerilName.StormFlood]: "perilsTable.subPerils.stormFlood",
    [SubPerilName.CoastalFlood]: "perilsTable.subPerils.coastalFlood",
    [SubPerilName.SnowPressure]: "perilsTable.subPerils.snowPressure",
    [SubPerilName.Landslide]: "perilsTable.subPerils.landslide",
    [SubPerilName.Avalanche]: "perilsTable.subPerils.avalanche",
    [SubPerilName.Volcano]: "perilsTable.subPerils.volcano",
    [SubPerilName.Earthquake]: "perilsTable.subPerils.earthquake",
    [SubPerilName.Tsunami]: "perilsTable.subPerils.tsunami",
};

export const EditableSubperils = [
    SubPerilName.RiverFlood,
    SubPerilName.StormFlood,
    SubPerilName.CoastalFlood,
    SubPerilName.Earthquake,
    SubPerilName.Tsunami,
    PerilName.Hail,
];

export const EditablePerils = [PerilName.Hail];

// TODO: find out what `overLimitMinValue` must be used and in which cases
export const OVER_LIMIT_MIN_VALUE = 5;
