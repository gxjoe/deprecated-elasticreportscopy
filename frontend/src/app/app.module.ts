import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routes.module';

@NgModule({
  declarations: [AppComponent],
  imports: [AppRoutingModule],
  providers: [AppRoutingModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
