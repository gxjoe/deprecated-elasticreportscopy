import moment from 'moment';
import { DATE_FORMATS } from '../elastic/queries';
import { arrayToObject } from '../utils/helpers/helpers';

export const klassToHeading = {
  OfferCreatedDateDatepicker: 'Offer Created Date',
  OfferIssuanceDateDatepicker: 'Offer Issuance Date',
  PolicyIssuanceDateDatepicker: 'Policy Issuance Date',
  IsActiveDateDatepicker: 'Policy Active Date',
  InceptionDateDatepicker: 'Policy Start (Inception) Date',
  IsExpiringDateDatepicker: 'Policy End (Expiration) Date',
};

export const RELATIVE_DATEPICKER_OPTIONS = {
  custom: {
    XXX: {}
  }
};

export const jsDateToMoment = (date: Date) => {
  return moment(date);
};

export const lastNyears = (limit: number, return_moment_only = false) => {
  const this_year = moment().year();
  const range = [].rangeOfNumbers(this_year - limit, this_year, 1);

  if (return_moment_only) {
    return range.map(firstOfYearToMoment);
  }

  return arrayToObject(
    range.map(year => {
      return {
        name: `Whole ${year}`,
        dates: [
          firstOfYearToMoment(year).toDate(),
          moment(`${year}-12-31`).toDate()
        ]
      };
    }),
    'name',
    'dates'
  );
};

export const currentYearsQuarters = () => {
  const first_of_jan = moment()
    .startOf('year')
    .startOf('day');

  const last_of_dec = moment()
    .endOf('year')
    .endOf('day');

  const {
    quarters: quarters_days_without_first_of_jan
  } = calculateYearAndQuarterDates([first_of_jan, last_of_dec]);
  const this_year = moment().year();

  return arrayToObject(
    []
      .concat([first_of_jan.clone()])
      .concat(quarters_days_without_first_of_jan)
      .map((quarter: moment.Moment, index: number) => {
        return {
          name: `Q${index + 1} ${this_year}`,
          dates: [
            quarter.toDate(),
            quarter
              .clone()
              .add(3, 'months')
              .endOf('day')
              .subtract(1, 'day')
              .toDate()
          ]
        };
      }),
    'name',
    'dates'
  );
};

export const calculateYearAndQuarterDates = (
  list_of_dates: moment.Moment[]
): {
  years: moment.Moment[];
  quarters: moment.Moment[];
} => {
  if (!list_of_dates.length) {
    return {
      years: [],
      quarters: []
    };
  }

  const first = list_of_dates[0];
  const last = list_of_dates[list_of_dates.length - 1];

  const beginnings_of_quarters: moment.Moment[] = [];

  [].rangeOfNumbers(first.year(), last.year(), 1).map(year => {
    const first_of_jan = firstOfYearToMoment(year);
    beginnings_of_quarters.push(first_of_jan.clone().add(3, 'months'));
    beginnings_of_quarters.push(first_of_jan.clone().add(6, 'months'));
    beginnings_of_quarters.push(first_of_jan.clone().add(9, 'months'));
  });

  const year_beginnings = list_of_dates
    .map(mdate => mdate.year())
    .makeDistinct()
    .map(firstOfYearToMoment)
    .filter(first_of_jan => first_of_jan.isBetween(first, last, 'day', '[]'));

  const quarters = beginnings_of_quarters.filter(quarter_day =>
    quarter_day.isBetween(first, last, 'day', '(]')
  );

  return {
    years: year_beginnings,
    quarters: quarters
  };
};

export const monthYearPairToFirstOfMonthMoment = (
  month_year_pair: string
): moment.Moment => {
  return moment(month_year_pair, DATE_FORMATS.HUMAN_READABLE);
};

export const firstOfYearToMoment = (year: number): moment.Moment => {
  return moment(`${year}-01-01`, 'YYYY-MM-DD');
};

export const humanReadableToMoment = (date_str: string) => {
  return moment(date_str, [DATE_FORMATS.HUMAN_READABLE]);
};

export const elasticSearchDateToFormat = (
  date: string,
  format?: any
): number | moment.Moment | string => {
  const m = moment(date, DATE_FORMATS.MOMENT_DATE_HUMAN_READABLE);
  if (!format) {
    return m.unix();
  }

  if (format === 'as_moment') {
    return m;
  }

  return m.format(format);
};

export const momentToEsFormat = (date: moment.Moment) => {
  return date.format(DATE_FORMATS.MOMENT_DATE_HUMAN_READABLE);
};
