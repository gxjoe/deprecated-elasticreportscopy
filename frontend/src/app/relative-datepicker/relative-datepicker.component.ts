import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import * as moment from 'moment';
import { ElasticService } from '../services/elastic.service';
import {
  klassToHeading,
  jsDateToMoment,
  momentToEsFormat,
  lastNyears,
  currentYearsQuarters,
} from './constants';
import { arrayToObject } from '../utils/helpers/helpers';
import { DATE_FORMATS } from '../elastic/queries';

export interface ItsValues {
  start: string;
  end: string;
  format?: string;
  comparison?: {
    start: string;
    end: string;
    format?: string
  };
}

@Component({
  selector: 'app-relative-datepicker',
  templateUrl: './relative-datepicker.component.html',
  styleUrls: ['./relative-datepicker.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RelativeDatepickerComponent implements OnInit {
  @Input()
  klass;

  public valueOf: Number;
  public tsValues: ItsValues = {
    start: null,
    end: null
  };

  klassToHeading = klassToHeading;

  relativeRanges = {
    ...this.currentAndPastShortlinks,
    ...this.thisYearsQuarters,
    ...this.last3years,
  };

  constructor(private es: ElasticService) { }

  ngOnInit() {
    this.es.app_component.setComponent(this.klass, this);
    // this.setTsValues(this.relativeRanges['Last 20 Years']);
  }

  private get now() {
    return moment().toDate();
  }

  private get now_() {
    return moment();
  }

  private setTsValues(result: Date[], return_only = false): ItsValues {
    const s = result[0];
    const e = result[1];

    const start = s ? momentToEsFormat(jsDateToMoment(s).startOf('day')) : null;
    const end = e ? momentToEsFormat(jsDateToMoment(e).endOf('day')) : null;

    if (return_only) {
      return {
        start,
        end,
        format: DATE_FORMATS.ES_DATE_HUMAN_READABLE
      };
    }

    this.tsValues = {
      start,
      end,
    };

    return this.tsValues;
  }

  private get last3years(): { [name: string]: [Date, Date] } {
    return lastNyears(2, false);
  }

  private get thisYearsQuarters(): { [name: string]: [Date, Date] } {
    return currentYearsQuarters();
  }

  private get allMonthsOfCurrentYear(): { [name: string]: [Date, Date] } {
    const current_year = moment().get('year');
    return arrayToObject(
      [].rangeOfNumbers(1, 12, 1).map((month) => {
        const month_begin = moment(`${current_year}/${month}/1`, 'YYYY/M/D');
        const month_end = month_begin.clone().endOf('month');

        return {
          name: `${month_begin.format('MMM')} ${current_year}`,
          dates: [month_begin.toDate(), month_end.toDate()],
        };
      }),
      'name',
      'dates'
    );
  }

  private get allMonthsOfPreviousYear(): { [name: string]: [Date, Date] } {
    const last_year = moment().subtract(1, 'year').get('year');
    return arrayToObject(
      [].rangeOfNumbers(1, 12, 1).map((month) => {
        const month_begin = moment(`${last_year}/${month}/1`, 'YYYY/M/D');
        const month_end = month_begin.clone().endOf('month');

        return {
          name: `${month_begin.format('MMM')} ${last_year}`,
          dates: [month_begin.toDate(), month_end.toDate()],
        };
      }),
      'name',
      'dates'
    );
  }

  private get currentAndPastShortlinks(): { [name: string]: [Date, Date] } {
    return {
      'This week': [
        moment().startOf('week').startOf('day').toDate(),
        moment().endOf('week').endOf('day').toDate(),
      ],
      'Last week': [
        moment().subtract(1, 'week').startOf('week').startOf('day').toDate(),
        moment().subtract(1, 'week').endOf('week').endOf('day').toDate(),
      ],
      'This month': [
        moment().startOf('month').startOf('day').toDate(),
        moment().endOf('month').endOf('day').toDate(),
      ],
      'Last month': [
        moment().subtract(1, 'month').startOf('month').startOf('day').toDate(),
        moment().subtract(1, 'month').endOf('month').endOf('day').toDate(),
      ],
    };
  }

  onChange(result: Date[]): void {
    this.setTsValues(result);
    this.es.readyForRefresh();
  }

  public minus1year(): ItsValues {
    let comparison: ItsValues;

    if (!this.tsValues.start) {
      comparison = this.setTsValues(
        [
          moment().startOf('year').subtract(1, 'year').startOf('day').toDate(),
          // not end of `year` but `day` to keep things consistend
          moment().subtract(1, 'year').endOf('day').toDate(),
        ], true);

      const now = this.setTsValues(
        [
          moment().startOf('year').startOf('day').toDate(),
          // not end of `year` but `day` to keep things consistend
          moment().endOf('day').toDate(),
        ], true);

      return {
        start: now.start,
        end: now.end,
        format: DATE_FORMATS.ES_DATE_HUMAN_READABLE,
        comparison: {
          ...comparison
        }
      };
    }

    comparison = this.setTsValues(
      [
        moment(this.tsValues.start, DATE_FORMATS.ES_DATE_HUMAN_READABLE)
          .clone()
          .subtract(1, 'year')
          .startOf('day')
          .toDate(),
        moment(this.tsValues.end, DATE_FORMATS.ES_DATE_HUMAN_READABLE)
          .clone()
          .subtract(1, 'year')
          .endOf('day')
          .toDate()
      ],
      true
    );

    return {
      start: this.tsValues.start,
      end: this.tsValues.end,
      format: DATE_FORMATS.ES_DATE_HUMAN_READABLE,
      comparison: {
        start: comparison.start,
        end: comparison.end,
        format: DATE_FORMATS.ES_DATE_HUMAN_READABLE
      }
    };

  }
}
