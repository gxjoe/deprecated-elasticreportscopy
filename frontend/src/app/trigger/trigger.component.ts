import { Component, OnInit } from '@angular/core';
import { ElasticService } from '../services/elastic.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-trigger',
  templateUrl: './trigger.component.html',
  styleUrls: ['./trigger.component.scss']
})
export class TriggerComponent implements OnInit {
  constructor(private es: ElasticService, private route: ActivatedRoute) { }

  isRefreshable = false;
  hint = 'Refresh counts & dropdowns';

  ngOnInit() {
    this.es.filtersHaveChanged.subscribe(_ => {
      this.isRefreshable = true;
    });
  }

  onClick() {
    this.isRefreshable = false;
    this.es.search(this.route.snapshot.url[0].path);
  }
}
