import { OnInit, Input, AfterViewInit, ViewChild, Directive } from '@angular/core';
import { OnIncomingData } from '../../interfaces/on-incoming-data';

@Directive()
export class CustomChartDirective
  implements OnInit, OnIncomingData, AfterViewInit {
  @Input()
  klass: string;
  totalCount: Number;
  data;

  @ViewChild('canvas', { static: false })
  canvas;

  get nbHits() {
    return this.totalCount;
  }

  constructor() {
  }

  ngOnInit() { }

  handleIncomingData() { }

  ngAfterViewInit() {
    if (!this.canvas || !this.canvas.nativeElement) {
      return;
    }

    const getPaddingLeft = el =>
      parseInt(
        (getComputedStyle(el) as any).paddingLeft.match(/\d+/g).map(Number)[0],
        10
      );
    const getForceHeight = el => parseInt(el.getAttribute('force_height'), 10);

    const _el = this.canvas.nativeElement.parentNode.closest('.ant-card-body');
    const rect = _el.getBoundingClientRect();

    const shall_return = this.canvas.nativeElement.classList.contains(
      'no_adjustments'
    );
    if (shall_return) {
      return false;
    }

    const half_height_technique = this.canvas.nativeElement.classList.contains(
      'technique-half-height'
    );

    const parentCard = this.canvas.nativeElement.closest('._nz-card');

    // fit canvas content to parent container
    this.canvas.nativeElement.width = rect.width - getPaddingLeft(_el) * 2;
    this.canvas.nativeElement.height = rect.height + getPaddingLeft(_el) * 2;

    if (parentCard && parentCard.classList.contains('split-chart-screen')) {
      this.canvas.nativeElement.width =
        rect.width / 2 - getPaddingLeft(_el) * 2;
    }

    if (parentCard && parentCard.getAttribute('force_height')) {
      this.canvas.nativeElement.height = getForceHeight(parentCard);
    }

    this.canvas.nativeElement.height = Math.max(
      this.canvas.nativeElement.height,
      350
    );

    if (half_height_technique) {
      this.canvas.nativeElement.height /= 2;
      this.canvas.nativeElement.width = Math.min(
        this.canvas.nativeElement.width,
        350
      );
    }
  }
}
