export const pretty_colors = [
  '#5e2ca5',
  '#a463f2',
  '#d5008f',
  '#ff41b4',
  '#ff80cc',
  '#ffa3d7',
  '#137752',
  '#19a974',
  '#9eebcf',
  '#001b44',
  '#00449e',
  '#357edd',
  '#96ccff',
  '#e7040f',
  '#ff4136',
  '#ff725c',
  '#ff6300',
  '#ffb700',
  '#ffde37'
];

export const semiRandomIndex = (base_id, len) => {
  // const int = parseInt(base_id.slice(-2), 10) % (len - 1);
  const int = getRandomInt(0, len);
  return int <= len - 1 ? int : 1;
};

/**
 * Pick a pretty color and ensure that all offer's markers will be the same color
 * because no randomization is applied.
 * @param offerId
 * @returns
 */
export const getOfferColor = (offerId: string) => {
  const lastOfferDigit = parseInt(offerId.slice(-1), 10);
  // the last digit will be a number bet. 0 - 9 and so ~50% of these numbers
  // will be divisible by two
  const shallMultiplyByTwo = parseInt(offerId, 10) % 2 === 0;

  return pretty_colors[
    // it's safe to multiply by two because the result will be at most 18
    lastOfferDigit * (
      shallMultiplyByTwo ? 2 : 1
    )
  ];
};

let ctx;
const generate_palette = function (from, to, numberOfShades) {
  // returns an array of CSS color strings
  // @from: CSS color string of first color
  // @to: CSS color string of last color
  // @numberOfShades: number of shades to be returned (from and end included)
  // generate a canvas context and store it in cache
  ctx = ctx || document.createElement('canvas').getContext('2d');
  // set our canvas dimensions according to the number of shades required
  const w = (ctx.canvas.width = numberOfShades || 10);
  ctx.canvas.height = 1;
  // create a linear gradient
  // (to keep 'from' and 'to' values, we set its x to 1 and width to width -1)
  const grad = ctx.createLinearGradient(1, 0, w - 1, 0);
  grad.addColorStop(0, from || 'white');
  grad.addColorStop(1, to || 'black');
  ctx.fillStyle = grad;
  ctx.fillRect(0, 0, w, 1); // draw it
  const data = ctx.getImageData(0, 0, w, 1); // get the pixels info ([r, g, b, a, r, g...])
  const colors = [];
  data.data.forEach(function (comp, i) {
    if (i % 4 === 0) {
      // map each pixel in its own array
      colors.push([]);
    }
    if (i % 4 === 3) {
      // alpha
      comp /= 255;
    }
    colors[colors.length - 1].push(comp);
  });
  return colors.map(function (c) {
    // return a CSS computed value
    ctx.fillStyle = 'rgba(' + c.join() + ')';
    return ctx.fillStyle;
  });
};

export function makeid(length) {
  let result = '';
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

export const random_hex_color = () => {
  return pretty_colors[getRandomInt(0, pretty_colors.length - 1)];
  /*
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
  */
};

export const random_hex_color_similar_to = color => {
  let p = 1,
    temp,
    result = '#';
  const random = Math.random();

  while (p < color.length) {
    temp = parseInt(color.slice(p, (p += 2)), 16);
    temp += Math.floor((255 - temp) * random);
    result += temp.toString(16).padStart(2, '0');
  }
  return result;
};

const hashCode = str => {
  // java String#hashCode
  let hash = 0;
  for (let i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  return hash;
};

const intToRGB = i => {
  const c = (i & 0x00ffffff).toString(16).toUpperCase();

  return '#' + ('00000'.substring(0, 6 - c.length) + c);
};

export const hex_from_string_hash = str => {
  return intToRGB(hashCode(str));
};

export const palette_base = [
  '#647c8a',
  '#3f51b5',
  '#2196f3',
  '#00b862',
  '#afdf0a',
  '#a7b61a',
  '#f3e562',
  '#ff9800',
  '#ff5722',
  '#ff4514'
  /*...Array.apply(null, new Array(50)).map((_, ind) => {
    return "#4285f4";
  })*/
];

export const palette_base_human_readable = {
  red: '#ff4514',
  green: '#00b862',
  orange: '#ff9800'
};

export const padZero = (str, len?) => {
  len = len || 2;
  const zeros = new Array(len).join('0');
  return (zeros + str).slice(-len);
};

export const invertColor = (hex: string, bw) => {
  const orig = hex;
  if (hex.includes('rgb')) {
    const parts = hex
      .replace(/[^0-9\s,]/g, '')
      .split(/,[\s]?/g)
      .map(parseFloat);
    hex = rgbToHex_(parts[0], parts[1], parts[2]);
  }
  if (hex.indexOf('#') === 0) {
    hex = hex.slice(1);
  }
  // convert 3-digit hex to 6-digits.
  if (hex.length === 3) {
    hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
  }
  if (hex.length !== 6) {
    throw new Error('Invalid HEX color.');
  }
  let r: any = parseInt(hex.slice(0, 2), 16),
    g: any = parseInt(hex.slice(2, 4), 16),
    b: any = parseInt(hex.slice(4, 6), 16);
  if (bw) {
    // http://stackoverflow.com/a/3943023/112731
    return r * 0.299 + g * 0.587 + b * 0.114 > 186 ? '#000000' : '#FFFFFF';
  }
  // invert color components
  r = (255 - r).toString(16);
  g = (255 - g).toString(16);
  b = (255 - b).toString(16);
  // pad each with zeros and return
  return '#' + padZero(r) + padZero(g) + padZero(b);
};

export const colorSchemeVividDomain = [].concat(palette_base).concat(
  Array.apply(null, new Array(200)).map((_, ind) => {
    return random_hex_color_similar_to(
      palette_base[getRandomInt(0, palette_base.length - 1)]
    );
  })
  // generate_palette(palette_base[2], palette_base[palette_base.length - 7], 100)
);

export const rgbToHex_ = (r, g, b) => {
  return '#' + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
};

export function hexToRgb_(hex: string, asObj = false) {
  hex = hex.toLowerCase();
  return (function (res) {
    return res == null
      ? null
      : (function (parts) {
        return !asObj ? parts : [parts[0], parts[1], parts[2]];
      })(
        res.slice(1, 4).map(function (val) {
          return parseInt(val, 16);
        })
      );
  })(/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex));
}

export const hexToRgba = (hex, opacity) => {
  return `rgba(${hexToRgb_(hex, false).join(', ')}, ${opacity})`;
};

export const colorSchemeVividDomainAsRgbArray = (opacity = 1) =>
  colorSchemeVividDomain.map(
    color => `rgba(${hexToRgb_(color, false).join(', ')}, ${opacity})`
  );

export const hex_with_opacity = (hex, opacity) => {
  return `rgba(${hexToRgb_(hex, false).join(', ')}, ${opacity})`;
};

export const fitCanvasToContainer = (
  __canvas: HTMLCanvasElement,
  klass?: string,
  chart_type?: string
) => {
  // Make it visually fill the positioned parent
  __canvas.classList.add('has-content');

  __canvas.style.width = '100%';
  __canvas.style.height = '100%';

  // ...then set the internal size to match
  __canvas.width = __canvas.offsetWidth;

  const limit_total_height =
    (klass && klass.includes('address_based')) ||
    klass.match(/_histogram_chart$/) ||
    chart_type === 'pie';

  const height_ratio = limit_total_height ? 0.35 : 1;

  __canvas.classList.add(chart_type);

  const height_to_set = __canvas.offsetHeight * height_ratio;

  if (!limit_total_height) {
    __canvas.height = __canvas.offsetHeight * height_ratio;
  } else {
    __canvas.height = height_to_set > 450 ? 450 : height_to_set;
    console.info('limit_total_height', __canvas.height);
  }
};

export const SEUM_COLORS = {
  Property: ['#FFCDD2', '#E57373', '#F44336', '#D32F2F', '#B71C1C'],
  Motor: ['#F5F5F5', '#E0E0E0', '#9E9E9E', '#616161', '#212121'],
  Engineering: ['##FFF9C4', '#FFF176', '#FFEB3B', '#FBC02D', '#DBA623'],
  Marine: ['#BBDEFB', '#64B5F6', '#2196F3', '#1976D2', '#0D47A1'],
  Liability: ['#E1BEE7', '#BA68C8', '#9C27B0', '#7B1FA2', '#4A148C'],
  Financial: ['#B2EBF2', '#4DD0E1', '#00BCD4', '#0097A7', '#006064'],
  EB: ['#C8E6C9', '#81C784', '#4CAF50', '#388E3C', '#1B5E20'],
  Terrorism: ['#FFE0B2', '#FFB74D', '#FF9800', '#F57C00', '#E65100'],
  Retail: ['#C5CAE9', '#7986CB', '#3F51B5', '#303F9F', '#1A237E']
};

export const RISK_PROFILE_COLORS = {
  Percentages: [
    [hex_with_opacity('#00b050', 0.2), 'less than 10%'],
    [hex_with_opacity('#ffd900', 0.2), 'between 11% and 19%'],
    [hex_with_opacity('#ff0000', 0.2), 'more than 20%']
  ],
  'Average Rate / Combined Rate %': [
    [hex_with_opacity('#00b050', 1), 'more than 10%'],
    [hex_with_opacity('#ffd900', 1), 'between +/- 10%'],
    [hex_with_opacity('#ff0000', 1), 'less than -10%']
  ]
};

export const lobVsColor = (q: string, use_softer = false) => {
  if (!q) {
    return '';
  }
  const is_lob_like = Object.keys(SEUM_COLORS).some((k: string) =>
    q.includes(k)
  );
  if (is_lob_like) {
    const colors =
      SEUM_COLORS[Object.keys(SEUM_COLORS).filter(okey => q.includes(okey))[0]];
    if (!use_softer) {
      return colors[3];
    }

    return colors[1];
  }

  return '';
};

export const shortenedLabelsVsLegendMapKeysVsColors = (
  shortened_labels: Array<string>,
  legend_map: {
    [id: string]: {
      key: string;
      count: string;
      percentage: string;
    };
  },
  index = 0
) => {
  return shortened_labels.map(label => {
    const lob = legend_map[label].key;
    return lobVsColor(lob, index !== 0);
  });
};

export const correspodingColor = (line_of_b: string, riskClass: number) => {
  if (!line_of_b) {
    console.error('no line_of_b', {
      line_of_b,
      riskClass
    });
    return null;
  }

  let colors = [];

  // index starts at 0
  riskClass -= 1;

  switch (true) {
    case !line_of_b:
      return null;
    case line_of_b.includes('Property'):
      colors = SEUM_COLORS.Property;
      break;
    case line_of_b.includes('Motor'):
      colors = SEUM_COLORS.Motor;
      break;
    case line_of_b.includes('Engineering'):
      colors = SEUM_COLORS.Engineering;
      break;
    case line_of_b.includes('Marine'):
      colors = SEUM_COLORS.Marine;
      break;
    case line_of_b.includes('Liability'):
      colors = SEUM_COLORS.Liability;
      break;
    case line_of_b.includes('Financial'):
      colors = SEUM_COLORS.Financial;
      break;
    case line_of_b.includes('EB'):
      colors = SEUM_COLORS.EB;
      break;
    case line_of_b.includes('Terrorism'):
      colors = SEUM_COLORS.Terrorism;
      break;
    case line_of_b.includes('Retail'):
      colors = SEUM_COLORS.Retail;
      break;
  }

  if (colors.length) {
    return colors[riskClass];
  }
  return null;
};

export function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}

export const riskScoreBucketColors = () => {
  const pretty_colors_ascending = [
    '#00b862',
    '#afdf0a',
    '#f3e562',
    '#ff9800',
    '#ff4514'
  ];
  return [
    pretty_colors_ascending,
    pretty_colors_ascending.map(rgb => hexToRgba(rgb, 0.5)),
    pretty_colors_ascending.map(rgb => hexToRgba(rgb, 0.3))
  ];
};

export const duplicateArrayItems = arr => {
  return arr.reduce(function (res, current) {
    return res.concat([current, current]);
  }, []);
};
