import {
  Component,
  OnInit,
  ViewEncapsulation,
  ViewChild,
  Input,
  AfterViewInit
} from '@angular/core';
import { OnIncomingData } from '../../interfaces/on-incoming-data';
import { ElasticService } from '../../services/elastic.service';
import { VERTICAL_BAR_CHART_OPTIONS } from './constants';
import { CustomChartDirective } from '../common/chart';
import {
  colorSchemeVividDomainAsRgbArray,
  lobVsColor
} from '../common/constants';
import { ChartjsService } from '../../services/chartjs.service';

@Component({
  selector: 'app-horizontal-bar-chart',
  templateUrl: './horizontal-bar-chart.component.html',
  styleUrls: ['./horizontal-bar-chart.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HorizontalBarChart extends CustomChartDirective
  implements OnInit, OnIncomingData {
  @ViewChild('canvas', { static: false })
  canvas;

  @Input()
  canvasw;
  @Input()
  canvash;

  isNormalized: Boolean = false;
  isStacked: Boolean = false;

  _colors = colorSchemeVividDomainAsRgbArray();
  _softer_colors = colorSchemeVividDomainAsRgbArray(0.4);
  _datasets;
  _labels;
  _counts;
  totalCount;

  legendMap: object;

  private _drawnChart;

  height;
  width;

  csvInput?: string;

  public destroyCanvasChart() {
    if (this._drawnChart) {
      this._drawnChart.destroy();
    }

    if (this.legendMap) {
      this.legendMap = undefined;
    }
  }

  constructor(
    public es: ElasticService,
    private chart_service: ChartjsService
  ) {
    super();
  }

  ngOnInit() {
    this.es.app_component.setComponent(this.klass, this);
    Object.assign(this, VERTICAL_BAR_CHART_OPTIONS.custom[this.klass]);

    this.isNormalized = this.klass.toLowerCase().indexOf('normalized') !== -1;
    this.isStacked = this.klass.toLowerCase().indexOf('stacked') !== -1;

    // console.info(this.isNormalized, this.isStacked)
  }

  handleOnIncoming(datasets, labels, options) {
    if (this._drawnChart) {
      this._drawnChart.destroy();
    }

    this._drawnChart = this.chart_service.constructNormalizedHorizontalBarChart(
      this.canvas.nativeElement,
      {
        type: '',
        labels: labels,
        datasets: datasets.map((dataset, index) => {
          const lob_vs_color = lobVsColor(labels[0], index !== 0);

          if (
            typeof lob_vs_color === 'string' ||
            typeof lob_vs_color === 'undefined'
          ) {
            return {
              // if a lob color could not be established, fallback to gray
              backgroundColor: labels.map(label =>
                typeof lobVsColor(label, index !== 0) !== 'undefined'
                  ? lobVsColor(label, index !== 0)
                  : index === 0
                    ? '#444'
                    : '#444'
              ),
              ...dataset
            };
          }

          return {
            backgroundColor: options.customColors
              ? options.customColors[index]
              : index === 0
                ? this._colors
                : this._softer_colors,
            ...dataset
          };
        }),
        ...options
      }
    );
  }
}
