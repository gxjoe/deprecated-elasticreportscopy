import { findByKeyInNestedObject } from '../../elastic/utils';
import {
  percentageOfBuckets,
  appendToOrCreateObjectKey,
  groupedBucketsToPseudoBuckets,
  dynamicSort,
  stripHtml,
  sumByMapReduce
} from '../utils';
import * as _ from 'lodash';
import { onAnimationCompleteTechniques } from '../../services/chartjs.service';
import { nthLetterOfAlphabet, jsonToKeyValueTable } from '../../utils/helpers/helpers';
import { roundUpToNearestN } from '../../utils/helpers/numerical';

export const VERTICAL_BAR_CHART_OPTIONS = {
  custom: {
    OffersAggregatedByPerils: {
      legend: true,
      legendTitle: 'Offers By Perils',
      handleIncomingData(inputData) {
        const hits = findByKeyInNestedObject(inputData, 'hits')['hits'];
        let buckets = inputData.aggregations.filter_agg.filter_agg.buckets;
        const bucket_count = sumByMapReduce(buckets);

        this.totalCount = hits.total;

        const legendMap = {};
        const labels = [];
        const datasets = [
          {
            label: 'All | just_values',
            data: [],
            stack: 1
          }
        ];

        const max_arr = [];

        const grouped_by_value_count = {};

        buckets.map((item, ind) => {
          let k = item.key;
          if (!k) {
            k = 'n/a';
          }
          appendToOrCreateObjectKey(grouped_by_value_count, '' + item.doc_count, k);
        });

        buckets = groupedBucketsToPseudoBuckets(grouped_by_value_count);

        buckets.sort(dynamicSort('-doc_count')).map((item, ind) => {
          datasets[0].data.push(item.doc_count);
          const letter = nthLetterOfAlphabet(ind).toUpperCase();
          labels.push(letter);
          legendMap[letter] = {
            key: `${item.key}`,
            count: `${item.doc_count}`,
            percentage: `${percentageOfBuckets(item.doc_count, bucket_count)}`
          };
          max_arr.push(item.doc_count);
        });

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: null,
          tickOptions: {
            callback: function(value, ___, __) {
              return `${value}`;
            },
            max: roundUpToNearestN(Math.max.apply(null, max_arr))
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.noop,
          stackOptions: {
            stacked: true
          },
          labelCallback: function(tooltipItem, data) {
            return `${stripHtml(legendMap[tooltipItem.yLabel])}`;
          }
        });
        this.legendMap = jsonToKeyValueTable(legendMap);
      }
    }
  }
};
