import * as moment from 'moment';
import * as moment_duration from 'moment-duration-format';
import { isObject, extractNumberFromString } from '../utils/helpers/helpers';
import { roundUp } from '../utils/helpers/numerical';
import * as objectPath from 'object-path';

moment_duration(moment);

const REGEXES = {
  html: new RegExp('<(?:.|\n)*?>', 'gm'),
  spaces: new RegExp('(&nbsp;)+', 'gm'),
  trailing_bar: /\s?\|\s?$/gim
};
export const SLICE_LIMIT = 10;

export const formatLabel = (label: any): string => {
  if (label instanceof Date) {
    label = label.toLocaleDateString();
  } else {
    label = label.toLocaleString();
  }

  return label;
};

export const prettyPercent = (nomi, denomi, round?): string => {
  nomi = parseFloat(nomi);
  denomi = parseFloat(denomi);

  // division by zero
  if (isNaN(nomi / denomi)) {
    return '0%';
  }

  const perc = ((nomi / denomi) * 1e4) / 1e2;

  if (typeof round === 'undefined') {
    return `${roundUp(perc, 4)}%`;
  }

  return Math.round(perc) + '%';
};

export const prettyPercentToFloat = (percent): number => {
  const num = extractNumberFromString(percent);
  console.warn(num, percent);
  return num / 100;
};

export const percentageOfBuckets = (nomi, denomi, trailing_description?) => {
  return `<span class="explainer">${prettyPercent(nomi, denomi, false)}${trailing_description ? ' ' + trailing_description : ''
    }</span>`;
};

export const sortArrayByKey = (array, key, reverse?) => {
  return array.sort(function (a, b) {
    const x = a[key];
    const y = b[key];
    if (reverse) {
      return x > y ? -1 : x < y ? 1 : 0;
    }
    return x < y ? -1 : x > y ? 1 : 0;
  });
};

export const fromUnixToFormat = (to_parse: any, as_format?: string, from_format?: string) => {
  let mom;
  if (!from_format) {
    mom = moment(to_parse);
  } else {
    mom = moment(to_parse, from_format);
  }

  return mom.format(as_format || 'DD.MM.YYYY');
};

export const appendToOrCreateObjectKey = (obj, key, val) => {
  if (key in obj) {
    obj[key].push(val);
    obj[key] = obj[key].sort();
  } else {
    obj[key] = [val];
  }
};

export const groupedBucketsToPseudoBuckets = obj => {
  return Object.keys(obj).map(key => {
    return {
      key: obj[key].join('<br>'),
      doc_count: parseInt(key, 10)
    };
  });
};

export const dynamicSort = property => {
  let sortOrder = 1;
  if (property[0] === '-') {
    sortOrder = -1;
    property = property.substr(1);
  }
  return function (a, b) {
    const a_property = objectPath.get(a, property);
    const b_property = objectPath.get(b, property);
    const result = a_property < b_property ? -1 : a_property > b_property ? 1 : 0;
    return result * sortOrder;
  };
};

export const stripHtml = txt => {
  if (isObject(txt)) {
    // if input is a dict of dicts, split them into "lines"
    if (Object.keys(txt).length === 2) {
      let tmp = '';
      Object.values(txt).map(v => {
        tmp += `${v['key']} (${v['count']}) ${v['percentage']}<br>`;
      });
      txt = tmp;
    } else {
      txt = `${txt.key} ${txt.count ? '(' + txt.count + ')' : ''} ${txt.percentage}`;
    }
  }
  return txt
    .replace(REGEXES.html, ' | ')
    .replace(REGEXES.spaces, ' | ')
    .replace(REGEXES.trailing_bar, ' ');
};

export const sumByMapReduce = buckets => {
  return buckets.map(b => b.doc_count).sumByReduce();
};

export const humanReadableTimeFrame = (input, format = 'minutes') => {
  const now = moment();
  const end = moment().add(input, format);
  const as_moment = moment.duration(end.diff(now)) as any;
  let humanized = '';

  switch (format) {
    case 'minutes':
      if (as_moment.asDays() <= 1) {
        humanized = `${as_moment.format('h [hrs], m [min]')}`;
      } else if (as_moment.asMonths() < 1) {
        humanized = `${as_moment.format('d [d], h [hrs]')}`;
      } else if (as_moment.asMonths() <= 12) {
        humanized = `${as_moment.format('M [mo], d [d]')}`;
      } else {
        humanized = `${as_moment.format('y [y], M [m]')}`;
      }
      break;
  }

  return humanized;
};
