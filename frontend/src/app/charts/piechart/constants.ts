import { SLICE_LIMIT, sortArrayByKey } from '../utils';
import { findByKeyInNestedObject } from '../../elastic/utils';

export const PIE_CHART_OPTIONS = {
  custom: {
    TotalNoOfOffersPerLoB: {
      legendTitle: 'LoB',
      handleIncomingData(inputData) {
        const hits = findByKeyInNestedObject(inputData, 'hits')['hits'];
        this.totalCount = hits.total;

        const buckets = sortArrayByKey(findByKeyInNestedObject(inputData, 'buckets')['buckets'], 'key');
        const self = this;

        self._labels = [];
        self._datasets = [{ data: [] }];

        buckets.slice(0, SLICE_LIMIT).map(item => {
          self._labels.push(item.key);
          self._datasets[0].data.push(item.doc_count);
        });

        this.handleOnIncoming(this._datasets, this._labels);
      }
    },
    TotalPremiumPerLoB: {
      legend: true,
      legendTitle: 'LoB',
      handleIncomingData(inputData) {
        const self = this;
        self.totalCount = 0;

        const buckets = sortArrayByKey(findByKeyInNestedObject(inputData, 'buckets')['buckets'], 'key');

        self._labels = [];
        self._datasets = [{ data: [] }];

        buckets.slice(0, SLICE_LIMIT).map(item => {
          self._labels.push(item.key);
          self._datasets[0].data.push(item.total_premium.value);
          self.totalCount += item.total_premium.value;
        });

        this.handleOnIncoming(this._datasets, this._labels);
      }
    }
  }
};
