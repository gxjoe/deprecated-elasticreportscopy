import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { OnIncomingData } from '../../interfaces/on-incoming-data';
import { ElasticService } from '../../services/elastic.service';
import { PIE_CHART_OPTIONS } from './constants';
import { CustomChartDirective } from '../common/chart';
import { colorSchemeVividDomainAsRgbArray } from '../common/constants';
import { prettyPercent } from '../utils';
import { ChartjsService } from '../../services/chartjs.service';

@Component({
  selector: 'app-piechart',
  templateUrl: './piechart.component.html',
  styleUrls: ['./piechart.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PiechartComponent extends CustomChartDirective implements OnInit, OnIncomingData {
  @ViewChild('canvas', { static: false })
  canvas;
  _colors = colorSchemeVividDomainAsRgbArray();
  _datasets;
  _labels;
  totalCount;

  _drawnChart;

  constructor(public es: ElasticService, private chart_service: ChartjsService) {
    super();
  }

  ngOnInit() {
    this.es.app_component.setComponent(this.klass, this);
    Object.assign(this, PIE_CHART_OPTIONS.custom[this.klass]);
  }

  handleOnIncoming(datasets, labels) {
    const self = this;

    if (this._drawnChart) {
      this._drawnChart.destroy();
    }
    this._drawnChart = this.chart_service.constructPieChart(this.canvas.nativeElement, {
      type: 'pie',
      labels: labels,
      datasets: datasets.map(dataset => {
        return { backgroundColor: this._colors, ...dataset };
      }),
      labelCallback: function (tooltipItem, data) {
        let label = data.labels[tooltipItem.index] || '';

        const value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

        if (label) {
          label += ': ';
        }
        label += prettyPercent(value, self.totalCount, false);
        return label;
      }
    });
  }

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
}
