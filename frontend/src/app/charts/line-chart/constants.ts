import { findByKeyInNestedObject } from '../../elastic/utils';
import {
  hexToRgba,
  palette_base,
  correspodingColor,
  makeid
} from '../common/constants';
import { sortArrayByKey, prettyPercent } from '../utils';
import * as _ from 'lodash';
import {
  onAnimationCompleteTechniques,
  annotationTechniques,
  weeklyHistogramXaxisOpts,
  generateVericalAnnotationLabel
} from '../../services/chartjs.service';
import {
  CURRENCY_PREFIX_ISO,
  pprintCurrency,
  nFormatter,
  CURRENCY_PREFIX_UTF,
  premiumDeviation,
  isFalsey
} from '../../utils/helpers/helpers';
import {
  CUSTOM_Y_AXIS_ID,
  secondYaxisDenotingCount
} from '../histogram-table-chart/rem_options';
import { roundUp } from 'src/app/utils/helpers/numerical';
import { generateYearlyTrendlineDatasetsForScatterScoreMatrices } from 'src/app/utils/helpers/chartjs.helper';
import { CHART_FUNCT } from '../histogram-table-chart/common';

import * as objPath from 'object-path';

import { draw as pattern_draw } from 'patternomaly';
import {
  elasticSearchDateToFormat,
  calculateYearAndQuarterDates
} from 'src/app/relative-datepicker/constants';
import { Moment } from 'moment';
import { DATE_FORMATS } from 'src/app/elastic/queries';
import { AddressesDoc, Lob } from 'src/app/interfaces/elastic';

const shadedPattern = (rgba_border_color: string, rgba_bg_color: string) => {
  return {
    borderWidth: 2,
    borderColor: rgba_border_color,
    backgroundColor: pattern_draw(
      'diagonal-right-left',
      'rgba(0,0,0,0)',
      rgba_bg_color,
      5
    ),
    pointRadius: 1,
    pointHoverRadius: 2
  };
};

export const LINE_CHART_OPTIONS = {
  custom: {
    OffersSuccessRatioAggregatedByDates: {
      legend: true,
      legendTitle: 'Offers By SuccessRatio',
      handleIncomingData(inputData) {
        const hits = findByKeyInNestedObject(inputData, 'hits')['hits'];
        const confirmed_buckets =
          inputData.aggregations.confirmed.buckets.all.development.development
            .buckets;
        const non_declined_buckets =
          inputData.aggregations.non_declined.buckets.all.development
            .development.buckets;

        const longer =
          confirmed_buckets.length > non_declined_buckets.length
            ? confirmed_buckets.slice()
            : non_declined_buckets.slice();

        this.totalCount = hits.total;

        const labels = [];
        const datasets = [
          {
            label: 'ratio',
            data: [],
            stack: 1,
            ...shadedPattern(
              hexToRgba(palette_base[3], 0.8),
              hexToRgba(palette_base[3], 0.8)
            )
          }
        ];

        longer.map(item => {
          const nominator = confirmed_buckets.find(
            x => x.key_as_string === item.key_as_string
          );
          const denominator = non_declined_buckets.find(
            x => x.key_as_string === item.key_as_string
          );

          if (nominator && denominator && denominator.doc_count > 0) {
            datasets[0].data.push(
              (nominator.doc_count / denominator.doc_count) * 100
            );
            labels.push(nominator.key_as_string);
          }
        });

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: annotationTechniques.hundred_percent_line,
          yAxisOpts: {
            scaleLabel: {
              display: true,
              labelString: 'Success Ratio in %'
            }
          },
          xAxisOpts: {
            ...weeklyHistogramXaxisOpts()
          },
          labelCallback: function (tooltipItem, data) {
            // const label = data.labels[tooltipItem.index + 1] || '';
            const value =
              data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

            return `${prettyPercent(value / 100, 1)}`;
          },
          tickOptions: {
            callback: function (value, ___, __) {
              return `${value}`;
            },
            max: 150
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.noop,
          stackOptions: {
            stacked: false
          }
        });
      }
    },
    OffersSuccessRatiovsLobsCountAggregatedByDates: {
      legend: true,
      legendTitle: 'Offers By SuccessRatio',
      handleIncomingData(inputData) {
        const hits = findByKeyInNestedObject(inputData, 'hits')['hits'];
        const confirmed_buckets =
          inputData.aggregations.confirmed.buckets.all.development.development
            .buckets;
        const non_declined_buckets =
          inputData.aggregations.non_declined.buckets.all.development
            .development.buckets;

        const longer =
          confirmed_buckets.length > non_declined_buckets.length
            ? confirmed_buckets.slice()
            : non_declined_buckets.slice();

        this.totalCount = hits.total;

        const labels = [];
        const datasets = [
          {
            label: 'offers_count',
            data: [],
            stack: 1,
            ...shadedPattern(
              hexToRgba(palette_base[2], 0.8),
              hexToRgba(palette_base[2], 0.8)
            ),

            yAxisID: CUSTOM_Y_AXIS_ID,
            type: 'line'
          },
          {
            label: 'ratio',
            data: [],
            stack: 1,
            ...shadedPattern(
              hexToRgba(palette_base[3], 0.8),
              hexToRgba(palette_base[3], 0)
            )
          }
        ];

        longer.map(item => {
          const nominator = confirmed_buckets.find(
            x => x.key_as_string === item.key_as_string
          );
          const denominator = non_declined_buckets.find(
            x => x.key_as_string === item.key_as_string
          );

          if (nominator && denominator && denominator.doc_count > 0) {
            datasets[1].data.push(
              (nominator.doc_count / denominator.doc_count) * 100
            );
            labels.push(nominator.key_as_string);

            datasets[0].data.push(
              denominator.offers_count.offers_count.offers_count.count
            );
          }
        });

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: annotationTechniques.hundred_percent_line,
          yAxisOpts: {
            scaleLabel: {
              display: true,
              labelString: 'Success Ratio in %'
            }
          },
          xAxisOpts: {
            ...weeklyHistogramXaxisOpts()
          },
          labelCallback: function (tooltipItem, data) {
            const success = data.datasets[1].data[tooltipItem.index];
            const offer_count = data.datasets[0].data[tooltipItem.index];
            const label = tooltipItem.xLabel;

            return `Success: ${prettyPercent(
              success / 100,
              1
            )} • Offers: ${offer_count} • ${(window as any).dateRangeMagnitude === 'month' ? 'Week' : 'Day'}: ${label}`;
          },
          tickOptions: {
            callback: function (value, ___, __) {
              return `${value}`;
            },
            max: 150
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.noop,
          stackOptions: {
            stacked: false
          },
          fully_custom_y_axis: {
            ...secondYaxisDenotingCount(
              Math.max.apply(null, [].concat(datasets[0].data)),
              Math.min.apply(null, [].concat(datasets[0].data)),
              `LoBs Count`
            )
          }
        });
      }
    },
    OffersSuccessRatioVsPremiumsAggregatedByDates: {
      legend: true,
      legendTitle: 'Offers By SuccessRatio',
      handleIncomingData(inputData) {
        const hits = findByKeyInNestedObject(inputData, 'hits')['hits'];
        const confirmed_buckets =
          inputData.aggregations.confirmed.buckets.all.development.development
            .buckets;
        const non_declined_buckets =
          inputData.aggregations.non_declined.buckets.all.development
            .development.buckets;

        const longer =
          confirmed_buckets.length > non_declined_buckets.length
            ? confirmed_buckets.slice()
            : non_declined_buckets.slice();

        this.totalCount = hits.total;

        const labels = [];
        const datasets = [
          {
            label: 'premium_sum',
            data: [],
            stack: 1,
            ...shadedPattern(
              hexToRgba(palette_base[2], 0.8),
              hexToRgba(palette_base[2], 0.8)
            ),

            yAxisID: CUSTOM_Y_AXIS_ID,
            type: 'line'
          },
          {
            label: 'ratio',
            data: [],
            stack: 1,
            ...shadedPattern(
              hexToRgba(palette_base[3], 0.8),
              hexToRgba(palette_base[3], 0)
            )
          }
        ];

        longer.map(item => {
          const nominator = confirmed_buckets.find(
            x => x.key_as_string === item.key_as_string
          );
          const denominator = non_declined_buckets.find(
            x => x.key_as_string === item.key_as_string
          );

          if (nominator && denominator && denominator.doc_count > 0) {
            datasets[1].data.push(
              (nominator.doc_count / denominator.doc_count) * 100
            );
            labels.push(nominator.key_as_string);

            datasets[0].data.push(
              denominator.premium_sum.premium_sum.premium_sum.sum
            );
          }
        });

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: annotationTechniques.hundred_percent_line,
          yAxisOpts: {
            scaleLabel: {
              display: true,
              labelString: 'Success Ratio in %'
            }
          },
          xAxisOpts: {
            ...weeklyHistogramXaxisOpts()
          },
          labelCallback: function (tooltipItem, data) {
            const success = data.datasets[1].data[tooltipItem.index];
            const premium_sum = data.datasets[0].data[tooltipItem.index];
            const label = tooltipItem.xLabel;

            return `Success: ${prettyPercent(
              success / 100,
              1
            )} • Premium Sum: ${CURRENCY_PREFIX_UTF()}${nFormatter(
              premium_sum,
              3,
              false
            )} • ${(window as any).dateRangeMagnitude === 'month' ? 'Week' : 'Day'}: ${label}`;
          },
          tickOptions: {
            callback: function (value, ___, __) {
              return `${value}`;
            },
            max: 150
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.noop,
          stackOptions: {
            stacked: false
          },
          fully_custom_y_axis: {
            ...secondYaxisDenotingCount(
              Math.max.apply(null, [].concat(datasets[0].data)),
              Math.min.apply(null, [].concat(datasets[0].data)),
              `Premium Sum in ${CURRENCY_PREFIX_UTF()}`,
              function (price_point, ___, __) {
                return nFormatter(price_point, 3, false);
              }
            )
          }
        });
      }
    },

    PolicyIssuedPremiumRatiosByTSI: {
      legend: true,
      legendTitle: 'Premium Ratios',
      handleIncomingData(inputData) {
        const data_hash = [];
        let sum_of_m = 0;
        let sum_of_t = 0;

        const buckets = inputData.hits.hits.map(source => {
          const hit = source._source;

          const mprem =
            hit.lob.rating[`mTotalPremium_${CURRENCY_PREFIX_ISO()}`];
          const tprem = hit.lob.rating[`totalPremium_${CURRENCY_PREFIX_ISO()}`];
          const tsi =
            hit.lob[
            `totalSumInsured_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`
            ] || hit.lob[`totalSumInsured_${CURRENCY_PREFIX_ISO()}`];
          //       ^^ might be 0 so take the non-adjusted value
          const tsi_pretty = `${CURRENCY_PREFIX_UTF()} ${nFormatter(
            tsi,
            1,
            false
          )}`;

          const ratio = premiumDeviation(mprem, tprem);
          const ratio_pretty = premiumDeviation(mprem, tprem, 4);
          const lob_id = parseInt(hit.lob.id, 10);
          const offer_id = hit.offer.number;

          sum_of_m += mprem;
          sum_of_t += tprem;

          data_hash.push({
            lob_id: `${lob_id}`,
            tsi: tsi,
            tsi_pretty: tsi_pretty,
            ratio: ratio,
            ratio_pretty: ratio_pretty,
            mprem: pprintCurrency(mprem),
            tprem: pprintCurrency(tprem),
            offer_id: offer_id
          });

          return {
            mprem_raw: mprem,
            tprem_raw: tprem,
            ratio: ratio,
            ratio_pretty: ratio_pretty,
            tsi: tsi,
            tsi_pretty: tsi_pretty,
            lob_id: lob_id,
            offer_id: offer_id
          };
        });

        const total_deviation = premiumDeviation(sum_of_m, sum_of_t, 4);

        const labels = [];
        const datasets = [
          {
            label: 'ratio',
            data: sortArrayByKey(buckets, 'tsi', false).map(item => {
              labels.push(item.tsi_pretty);
              return item.ratio;
            }),
            stack: 1,
            backgroundColor: hexToRgba(palette_base[6], 0.3)
          },
          {
            label: 'mprem',
            data: sortArrayByKey(buckets, 'tsi', false).map(item => {
              return item.mprem_raw;
            }),
            stack: 1,
            borderColor: hexToRgba(palette_base[2], 0.8),
            fill: false,

            yAxisID: CUSTOM_Y_AXIS_ID,
            type: 'line'
          },
          {
            label: 'tprem',
            data: sortArrayByKey(buckets, 'tsi', false).map(item => {
              return item.tprem_raw;
            }),
            stack: 1,
            fill: false,
            borderColor: hexToRgba(palette_base[3], 0.8),

            yAxisID: CUSTOM_Y_AXIS_ID,
            type: 'line'
          }
        ];

        this.handleOnIncoming(
          datasets,
          labels,
          {
            annotationTechniques: annotationTechniques.noop,
            yAxisOpts: {
              scaleLabel: {
                display: true,
                labelString: 'Deviation Ratio in %'
              }
            },
            xAxisOpts: {
              scaleLabel: {
                display: true,
                labelString: `TSI in ${CURRENCY_PREFIX_UTF()}`
              }
            },
            labelCallback: function (tooltipItem) {
              const target = data_hash.filter(
                item => item.tsi_pretty === tooltipItem.xLabel
              )[0];
              return `Offer: ${target.offer_id} • LoB: ${target.lob_id} • Ratio: ${target.ratio_pretty} • M: ${target.mprem} • T: ${target.tprem}`;
            },
            tickOptions: {
              callback: function (value, ___, __) {
                return `${value} %`;
              },
              max: Math.max.apply(null, datasets[0].data) * 1.2,
              min: Math.min.apply(null, datasets[0].data)
            },
            legendOptions: null,
            onAnimationComplete: onAnimationCompleteTechniques.noop,
            stackOptions: {
              stacked: false
            },
            fully_custom_y_axis: {
              ...secondYaxisDenotingCount(
                Math.max.apply(
                  null,
                  [].concat(datasets[1].data).concat(datasets[2].data)
                ),
                Math.min.apply(
                  null,
                  [].concat(datasets[1].data).concat(datasets[2].data)
                ),
                `Premiums in ${CURRENCY_PREFIX_UTF()}`
              )
            }
          },
          true
        );

        this.extraChartInformationHTML = `
          <div class="extra-table">
            <span class="bold">Total Deviation</span>
            <span>${total_deviation}</span>
          </div>
          <div class="extra-table">
            <span class="bold">Number of LoBs</span>
            <span>${labels.length}</span>
          </div>
        `;
      }
    },
    PolicyIssuedPremiumRatiosVsTime: {
      legend: true,
      legendTitle: 'Premium Ratios',
      handleIncomingData(inputData) {
        let labels = [];
        const data_hash = [];
        let sum_of_m = 0;
        let sum_of_t = 0;
        const list_of_dates: Moment[] = [];

        const customSorter = (a: any, b: any) => {
          const val_a = elasticSearchDateToFormat(
            objPath.get(a, '_source.offer.createdDate')
          );
          const val_b = elasticSearchDateToFormat(
            objPath.get(b, '_source.offer.createdDate')
          );

          if (val_a < val_b) {
            return -1;
          }
          if (val_a > val_b) {
            return 1;
          }
          return 0;
        };

        const hits = inputData.hits.hits
          .sort(customSorter)
          .map(source => source._source);

        hits.map(hit => {
          const _date = <Moment>(
            elasticSearchDateToFormat(hit.offer.createdDate, 'as_moment')
          );
          if (_date && _date.isValid()) {
            list_of_dates.push(_date);
          }
        });

        const {
          years: distinct_years,
          quarters: distinct_quarters
        } = calculateYearAndQuarterDates(list_of_dates);
        const annotation_flags: Moment[] = []
          .concat(distinct_years)
          .concat(distinct_quarters);

        let buckets = hits.map(hit => {
          const mprem =
            hit.lob.rating[`mTotalPremium_${CURRENCY_PREFIX_ISO()}`];
          const tprem = hit.lob.rating[`totalPremium_${CURRENCY_PREFIX_ISO()}`];
          const tsi =
            hit.lob[
            `totalSumInsured_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`
            ] || hit.lob[`totalSumInsured_${CURRENCY_PREFIX_ISO()}`];
          //       ^^ might be 0 so take the non-adjusted value
          const tsi_pretty = `${CURRENCY_PREFIX_UTF()} ${nFormatter(
            tsi,
            1,
            false
          )}`;

          const ratio = premiumDeviation(mprem, tprem);
          const ratio_pretty = premiumDeviation(mprem, tprem, 4);
          const lob_id = parseInt(hit.lob.id, 10);
          const offer_id = hit.offer.number;

          const moment_date = <Moment>(
            elasticSearchDateToFormat(hit.offer.createdDate, 'as_moment')
          );
          const date = moment_date.format(DATE_FORMATS.HUMAN_READABLE);

          sum_of_m += mprem;
          sum_of_t += tprem;

          data_hash.push({
            lob_id: `${lob_id}`,
            tsi: tsi,
            tsi_pretty: tsi_pretty,
            ratio: ratio,
            ratio_pretty: ratio_pretty,
            mprem: pprintCurrency(mprem),
            tprem: pprintCurrency(tprem),
            offer_id: offer_id,

            date: date
          });

          return {
            mprem_raw: mprem,
            tprem_raw: tprem,
            ratio: ratio,
            ratio_pretty: ratio_pretty,
            tsi: tsi,
            tsi_pretty: tsi_pretty,
            lob_id: lob_id,
            offer_id: offer_id,

            date: date,
            date_unix: moment_date.unix()
          };
        });

        annotation_flags.map(flag_date => {
          buckets.push({
            mprem_raw: 0,
            tprem_raw: 0,
            ratio: 0,
            ratio_pretty: 0,
            tsi: 0,
            tsi_pretty: 0,
            lob_id: '',
            offer_id: '',

            date: flag_date.format('DD.MM.YYYY'),
            date_unix: flag_date.unix()
          });
        });

        buckets = sortArrayByKey(buckets, 'date_unix', false);
        labels = buckets.map(bucket => bucket.date);

        const total_deviation = premiumDeviation(sum_of_m, sum_of_t, 4);

        const datasets = [
          {
            label: 'ratio',
            data: buckets.map(item => {
              // ;
              return item.ratio;
            }),
            stack: 1,
            backgroundColor: hexToRgba(palette_base[6], 0.3)
          },
          {
            label: 'mprem',
            data: buckets.map(item => {
              return item.mprem_raw;
            }),
            stack: 1,
            borderColor: hexToRgba(palette_base[2], 0.8),
            fill: false,

            yAxisID: CUSTOM_Y_AXIS_ID,
            type: 'line'
          },
          {
            label: 'tprem',
            data: buckets.map(item => {
              return item.tprem_raw;
            }),
            stack: 1,
            fill: false,
            borderColor: hexToRgba(palette_base[3], 0.8),

            yAxisID: CUSTOM_Y_AXIS_ID,
            type: 'line'
          },
          {
            data: list_of_dates.map(m => m.unix()),
            label: 'transparent',
            stack: 1,
            fill: false,
            xAxisID: 'x-axis-0'
          }
        ];

        this.handleOnIncoming(
          datasets,
          labels,
          {
            yAxisOpts: {
              scaleLabel: {
                display: true,
                labelString: 'Deviation Ratio in %'
              }
            },
            xAxisOpts: {
              gridLines: {
                display: false
              },
              id: 'x-axis-0',
              scaleLabel: {
                display: true,
                labelString: `Date Created`
              }
            },
            labelCallback: function (tooltipItem) {
              let target: any = data_hash.filter(
                item => item.date === tooltipItem.xLabel
              );

              if (!target.length) {
                return;
              }

              target = target[0];
              return `Offer: ${target.offer_id} • LoB: ${target.lob_id} • Ratio: ${target.ratio_pretty} • M: ${target.mprem} • T: ${target.tprem} • Date: ${target.date}`;
            },
            tickOptions: {
              callback: function (value, ___, __) {
                return `${value} %`;
              },
              max: Math.max.apply(null, datasets[0].data) * 1.2,
              min: Math.min.apply(null, datasets[0].data)
            },
            legendOptions: null,
            onAnimationComplete: onAnimationCompleteTechniques.noop,
            stackOptions: {
              stacked: false
            },
            fully_custom_y_axis: {
              ...secondYaxisDenotingCount(
                Math.max.apply(
                  null,
                  [].concat(datasets[1].data).concat(datasets[2].data)
                ),
                Math.min.apply(
                  null,
                  [].concat(datasets[1].data).concat(datasets[2].data)
                ),
                `Premiums in ${CURRENCY_PREFIX_UTF()}`
              )
            },
            customAnnotations: {
              annotations: []
                .concat(
                  distinct_years.map(year => {
                    return generateVericalAnnotationLabel(
                      makeid(5),
                      year.format(DATE_FORMATS.HUMAN_READABLE),
                      year.format('YYYY'),
                      'strong'
                    );
                  })
                )
                .concat(
                  distinct_quarters.map(quarter => {
                    return generateVericalAnnotationLabel(
                      makeid(5),
                      quarter.format(DATE_FORMATS.HUMAN_READABLE),
                      `Q${quarter.quarter()} '${quarter.format('YY')}`,
                      'mild'
                    );
                  })
                )
            }
          },
          true
        );

        this.extraChartInformationHTML = `
          <div class="extra-table">
            <span class="bold">Total Deviation</span>
            <span>${total_deviation}</span>
          </div>
          <div class="extra-table">
            <span class="bold">Number of LoBs</span>
            <span>${labels.length}</span>
          </div>
        `;
      }
    },

    PremiumVsSIvsLoBScatterPlot: {
      legend: true,
      legendTitle: 'PremiumVsSIvsLoBScatterPlot',
      handleIncomingData(inputData) {
        let data_hash: Array<{
          offer_id: string,
          lob_id: string,
          tsi: number,
          tsi_pretty: string,
          premium: number,
          premium_pretty: string,

          risk_class: number,
          lob: Lob,
          color: string,
          hit: AddressesDoc
        }> = [];
        const filtered_data_hash = [];
        let sum_of_prem = 0;
        let sum_of_si = 0;

        inputData.hits.hits.map(source => {
          const hit = source._source;

          // UMGB-21 take uniqa-share
          const premium = hit.lob[`premium_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`];
          const tsi =
            hit.lob[
            `totalSumInsured_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`
            ] || hit.lob[`totalSumInsured_${CURRENCY_PREFIX_ISO()}`];
          //       ^^ might be 0 so take the non-adjusted value

          const tsi_pretty = `${CURRENCY_PREFIX_UTF()} ${nFormatter(
            tsi,
            1,
            false
          )}`;

          const lob_id = parseInt(hit.lob.id, 10);
          const offer_id = hit.offer.number;

          const risk_class = parseInt(hit.lob.riskClass, 10);

          const to_push: typeof data_hash[number] = {
            offer_id: offer_id,
            lob_id: `${lob_id}`,
            tsi: tsi,
            tsi_pretty: tsi_pretty,
            premium: premium,
            premium_pretty: pprintCurrency(premium),

            risk_class: risk_class,
            lob: hit.lob.lineOfBusiness,
            color: correspodingColor(hit.lob.lineOfBusiness, risk_class),
            hit: hit
          };

          data_hash.push(to_push);

          if (!isFalsey(premium) && !isFalsey(tsi)) {
            sum_of_prem += premium;
            sum_of_si += tsi;
          }

          return to_push;
        });

        data_hash = data_hash.filter(bucket => {
          return (
            bucket.premium >= 0 &&
            bucket.tsi >= 0 &&
            bucket.risk_class >= 0 &&
            bucket.color
          );
        });

        const y_axis_values = [];
        const x_axis_values = [];

        const labels = [];
        const datasets = data_hash.map(bucket => {
          x_axis_values.push(bucket.premium);
          y_axis_values.push(bucket.tsi);

          const prepared_bucket = {
            x: bucket.premium,
            y: bucket.tsi,
            meta: {
              ...bucket
            }
          };

          filtered_data_hash.push({ ...prepared_bucket, hit: bucket.hit });

          return {
            data: [prepared_bucket],
            // label: `${bucket.lob_id}`,
            type: 'scatter',
            pointBorderWidth: 0.5,
            pointRadius: 5,
            borderColor: '#EEEEEE',
            backgroundColor: bucket.color
          };
        });

        const data_hash_lookup = (x: number, y: number) => {
          return data_hash.filter(item => {
            return item.premium === x && item.tsi === y;
          })[0];
        };

        const y_axis_max = Math.min.apply(null, [
          Math.max.apply(null, y_axis_values) * 1.2
        ]);
        const x_axis_min = Math.max.apply(null, [
          Math.min.apply(null, x_axis_values),
          0
        ]);

        const total_ratio = `${roundUp((sum_of_prem / sum_of_si) * 1e2, 4)} %`;

        const trendline_datasets = generateYearlyTrendlineDatasetsForScatterScoreMatrices(
          filtered_data_hash,
          false
        );

        const self = this;

        this.handleOnIncoming(
          datasets,
          labels,
          {
            type: 'scatter',
            annotationTechniques: annotationTechniques.noop,
            customAnnotations: {
              drawTime: 'afterDatasetsDraw', // (default)

              // Mouse events to enable on each annotation.
              // Should be an array of one or more browser-supported mouse events
              // See https://developer.mozilla.org/en-US/docs/Web/Events
              annotations: trendline_datasets.map(dset => {
                return dset.customAnnotations;
              })
            },
            yAxisOpts: {
              scaleLabel: {
                display: true,
                labelString: `TSI in ${CURRENCY_PREFIX_UTF()}`
              },
              ticks: {
                callback: function (value, ___, __) {
                  return `${nFormatter(value, 3, false)}`;
                },
                min: 0,
                max: y_axis_max
              }
            },
            xAxisOpts: {
              scaleLabel: {
                display: true,
                labelString: `Premium Uniqa Share in ${CURRENCY_PREFIX_UTF()}`
              },
              ticks: {
                min: x_axis_min,
                callback: function (value, ___, __) {
                  return `${nFormatter(value, 3, false)}`;
                },
                max: Math.max.apply(null, x_axis_values)
              }
            },
            onClick: (_, arr) => {
              if (!arr?.length) {
                return;
              }

              const elem = arr[0];
              const { _datasetIndex } = elem;

              // UDGB-51
              try {
                const target: typeof data_hash[number] = elem._chart.data.datasets[_datasetIndex].data[0].meta;
                const offerNumber = target.offer_id;

                navigator.clipboard.writeText(offerNumber).then(() => {
                  const msg = `Copied ${offerNumber} to clipboard!`;
                  self.notif.success(msg, '', { nzPlacement: 'bottomLeft' });
                });
              } catch (_) { }
            },
            labelCallback: function (tooltipItem, data) {
              return data.datasets
                .filter((___, i) => i === tooltipItem.datasetIndex)
                .map(__ => {
                  const hit = data_hash_lookup(
                    tooltipItem.xLabel,
                    tooltipItem.yLabel
                  );
                  if (!hit) {
                    return '';
                  }

                  return `Offer: ${hit.offer_id
                    } • Client: ${CHART_FUNCT.ellipsis(
                      hit.hit.offer.client.name
                    )} • Prem Uniqa Share: ${hit.premium_pretty} • TSI: ${hit.tsi_pretty
                    } • RC: ${hit.risk_class} • LoB: ${hit.lob}`;
                })
                .join('');
            },
            tickOptions: {
              max: y_axis_max
            },
            legendOptions: null,
            onAnimationComplete: onAnimationCompleteTechniques.noop,
            stackOptions: {
              stacked: false
            }
          },
          true
        );

        this.extraChartInformationHTML = `
          <div class="extra-table">
            <span class="bold">Total Prem vs SI ratio</span>
            <span>${total_ratio}</span>
          </div>
          <div class="extra-table">
            <span class="bold">Number of LoBs analyzed</span>
            <span>${filtered_data_hash.length}</span>
          </div>
        `;
      }
    },

    AggregatedSuperScorePremiums: {
      legend: true,
      legendTitle: 'AggregatedSuperScorePremiums',
      handleIncomingData(inputData) {
        let data_hash = [];
        const filtered_data_hash = [];

        inputData.hits.hits.map(source => {
          const hit = source._source;

          const premium = hit.lob[`premium_${CURRENCY_PREFIX_ISO()}`];
          const superScore = hit.lob.rating.superScore;

          const lob_id = parseInt(hit.lob.id, 10);
          const offer_id = hit.offer.number;

          const risk_class = parseInt(hit.lob.riskClass, 10);

          const to_push = {
            offer_id: offer_id,
            lob_id: `${lob_id}`,
            superScore,
            premium: premium,
            premium_pretty: pprintCurrency(premium),

            risk_class: risk_class,
            lob: hit.lob.lineOfBusiness,
            color: correspodingColor(hit.lob.lineOfBusiness, risk_class),
            hit: hit
          };

          data_hash.push(to_push);

          return to_push;
        });

        data_hash = data_hash
          .filter(bucket => {
            return (
              bucket.premium >= 0 &&
              bucket.superScore >= 0 &&
              bucket.risk_class >= 0 &&
              bucket.color
            );
          })
          .slice(0, 5e3);

        const y_axis_values = [];
        const x_axis_values = [];

        const labels = [];
        const datasets = data_hash.map(bucket => {
          x_axis_values.push(bucket.premium);
          y_axis_values.push(bucket.superScore);

          const prepared_bucket = {
            x: bucket.premium,
            y: bucket.superScore,
            meta: {
              ...bucket
            }
          };

          filtered_data_hash.push({ ...prepared_bucket, hit: bucket.hit });

          return {
            data: [prepared_bucket],
            // label: `${bucket.lob_id}`,
            type: 'scatter',
            pointBorderWidth: 0.5,
            pointRadius: 5,
            borderColor: '#EEEEEE',
            backgroundColor: bucket.color
          };
        });

        const data_hash_lookup = (x: number, y: number) => {
          return data_hash.filter(item => {
            return item.premium === x && item.superScore === y;
          })[0];
        };

        const y_axis_max = Math.min.apply(null, [
          Math.max.apply(null, y_axis_values) * 1.2
        ]);
        const x_axis_min = Math.max.apply(null, [
          Math.min.apply(null, x_axis_values),
          0
        ]);

        const trendline_datasets = generateYearlyTrendlineDatasetsForScatterScoreMatrices(
          filtered_data_hash,
          false
        );

        this.handleOnIncoming(
          datasets,
          labels,
          {
            type: 'scatter',
            annotationTechniques: annotationTechniques.noop,
            customAnnotations: {
              drawTime: 'afterDatasetsDraw', // (default)

              // Mouse events to enable on each annotation.
              // Should be an array of one or more browser-supported mouse events
              // See https://developer.mozilla.org/en-US/docs/Web/Events
              annotations: trendline_datasets.map(dset => {
                return dset.customAnnotations;
              })
            },
            yAxisOpts: {
              scaleLabel: {
                display: true,
                labelString: 'Rating SuperScore'
              },
              ticks: {
                callback: function (value, ___, __) {
                  return `${nFormatter(value, 3, false)}`;
                },
                min: 0,
                max: y_axis_max
              }
            },
            xAxisOpts: {
              scaleLabel: {
                display: true,
                labelString: `Premium in ${CURRENCY_PREFIX_UTF()}`
              },
              ticks: {
                min: x_axis_min,
                callback: function (value, ___, __) {
                  return `${nFormatter(value, 3, false)}`;
                },
                max: Math.max.apply(null, x_axis_values)
              }
            },
            labelCallback: function (tooltipItem, data) {
              return data.datasets
                .filter((___, i) => i === tooltipItem.datasetIndex)
                .map(__ => {
                  console.info(tooltipItem);
                  const hit = data_hash_lookup(
                    tooltipItem.xLabel,
                    tooltipItem.yLabel
                  );
                  if (!hit) {
                    return '';
                  }

                  return `Offer: ${hit.offer_id
                    } • Client: ${CHART_FUNCT.ellipsis(
                      hit.hit.offer.client.name
                    )} • Prem: ${hit.premium_pretty} • SuperScore: ${hit.superScore
                    } • RC: ${hit.risk_class} • LoB: ${hit.lob}`;
                })
                .join('');
            },
            tickOptions: {
              max: y_axis_max
            },
            legendOptions: null,
            onAnimationComplete: onAnimationCompleteTechniques.noop,
            stackOptions: {
              stacked: false
            }
          },
          true
        );
      }
    }
  }
};
