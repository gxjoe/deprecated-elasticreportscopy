import {
  Component,
  OnInit,
  ViewEncapsulation,
  ViewChild,
  Input
} from '@angular/core';
import { OnIncomingData } from '../../interfaces/on-incoming-data';
import { ElasticService } from '../../services/elastic.service';
import { LINE_CHART_OPTIONS } from './constants';
import { CustomChartDirective } from '../common/chart';
import { fitCanvasToContainer } from '../common/constants';
import { ChartjsService } from '../../services/chartjs.service';
import { CanvasDrawingService } from 'src/app/services/canvas-drawing.service';
import { GeojsonService } from 'src/app/services/geojson.service';
import { DataMarkerService } from 'src/app/services/data-marker.service';
import { CsvService } from 'src/app/services/csv.service';
import { filter } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LineChart extends CustomChartDirective implements OnInit, OnIncomingData {
  @ViewChild('canvas', { static: false })
  canvas;

  @Input()
  canvasw;
  @Input()
  canvash;

  isNormalized: Boolean = false;
  isStacked: Boolean = false;
  isLoading = false;

  legendMap: object;
  chartType: string;

  private _drawnChart: any;
  private _opts = {
    datasets: []
  };

  height: string;
  width: string;

  extraChartInformationHTML: string;

  downloadableTableInput: string | null;

  constructor(
    public es: ElasticService,
    private chart_service: ChartjsService,
    private canvasDrawingService: CanvasDrawingService,
    private geoJSONService: GeojsonService,
    private dataMarkerService: DataMarkerService,
    private csvService: CsvService,
    // needed for UDGB-51
    private notif: NzNotificationService

  ) {
    super();

    this.height = `${this.canvash}px`;
    this.width = `${this.canvasw || 550}px`;
  }

  ngOnInit() {
    this.height = `${this.canvash}px`;
    this.width = `${this.canvasw || 550}px`;

    this.es.app_component.setComponent(this.klass, this);
    Object.assign(this, LINE_CHART_OPTIONS.custom[this.klass]);

    this.isNormalized = this.klass.toLowerCase().indexOf('normalized') !== -1;
    this.isStacked = this.klass.toLowerCase().indexOf('stacked') !== -1;

    // console.info(this.isNormalized, this.isStacked)
  }

  public destroyCanvasChart() {
    if (this._drawnChart) {
      this._drawnChart.destroy();
    }

    if (this.legendMap) {
      this.legendMap = undefined;
    }
  }

  handleOnIncoming(datasets, labels, options) {
    this.extraChartInformationHTML = '';

    if (this._drawnChart) {
      this._drawnChart.destroy();
    }

    this.downloadableTableInput = null;

    this.destroyCanvasChart();

    this.chartType = options.type;
    this._opts = {
      datasets
    };

    if (options.type === 'scatter') {
      this._drawnChart = this.chart_service.constructNormalizedVerticalBarChart(
        this.canvas.nativeElement,
        {
          type: 'scatter',
          labels,
          datasets,
          ...options
        },
        'scatter'
      );

      fitCanvasToContainer(this.canvas.nativeElement, this.klass);
      setTimeout(() => {
        this._drawnChart.resize();
        this._drawnChart.update();
      }, 1e3);

      this.handleDrawingOnChart(this.klass);

      return;
    }

    this._drawnChart = this.chart_service.constructLineChart(
      this.canvas.nativeElement,
      {
        type: options.type || '',
        labels: labels,
        datasets: datasets,
        ...options
      }
    );
  }

  handleDrawingOnChart(klass: string) {
    this.canvasDrawingService.drawnCoordsObservable
      .pipe(filter(observer => observer.klass === klass))
      .subscribe(observer => {
        if (
          observer &&
          Array.isArray(observer.coordinates) &&
          observer.coordinates.length
        ) {
          const poly_array = observer.coordinates.slice();

          const converted_from_meta_datasets: Array<{
            x: number;
            y: number;
            hit: any;
          }> = this.geoJSONService.mergeArrays(
            ...this._opts.datasets.map((_, dset_index) => {
              return this._drawnChart
                .getDatasetMeta(dset_index)
                .data.map((dpoint, dpoint_index) => {
                  return {
                    x: dpoint._model.x,
                    y: dpoint._model.y,
                    hit: this._opts.datasets[dset_index].data[dpoint_index]
                  };
                });
            })
          );

          const collection = this.geoJSONService.createCanvasPointCollection(
            converted_from_meta_datasets
          );
          const filtered = this.geoJSONService.filterCanvasPointsByBoundary(
            collection,
            poly_array
          );

          const client_report_array = this.dataMarkerService.prepareClientReportInsideEUM_PremiumSILoB_Polygon(
            filtered
          );

          const client_report_csv = this.csvService.nestedJSONtoCSV(
            client_report_array
          );

          this.downloadableTableInput = client_report_csv;
        } else {
          this.downloadableTableInput = null;
        }
      });
  }
}
