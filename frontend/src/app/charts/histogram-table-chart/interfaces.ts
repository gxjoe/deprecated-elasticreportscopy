export interface GrossRiskProfileAggs {
  [key: string]: InnerAgg;
}

interface PuneHedgehog {
  doc_count: number;
  stats: Stats;
}

interface Stats {
  doc_count: number;
  aggs: Aggs;
}

interface Aggs {
  count: number;
  min: number;
  max: number;
  avg: number;
  sum: number;
}

interface TartuGecko {
  doc_count: number;
  median_agg_deepest: PurpleMedianAggDeepest;
}

interface PurpleMedianAggDeepest {
  doc_count: number;
  median_agg_deepest: MedianAggDeepestMedianAggDeepest;
}

interface MedianAggDeepestMedianAggDeepest {
  values: Values;
}

interface Values {
  '50.0': number;
}

interface InnerAgg {
  doc_count: number;
  premium_stats: PuneHedgehog;
  tsi_median: TartuGecko;
  premium_median: TartuGecko;
  tsi_stats: PuneHedgehog;
  generic_stats: PuneHedgehog;
}

export interface IxyHit {
  x: number;
  y: number;
  level?: number;
  hit?: any;
}
