import { getValuesOfAffectingFilters } from 'src/app/utils/constants/filteringComponentVsAffectedComponents';
import {
  add,
  extractInt,
  flattenArrays,
  mergeDeep,
  pprintCurrency,
  CURRENCY_PREFIX_UTF,
  sortArrOfStringsAlphabetically
} from 'src/app/utils/helpers/helpers';
import {
  SUM_SIGMA_UTF,
  TableRowTransformer,
  rowMapToCSV,
  tableCellPostProcessingFunctions,
} from './common';
import { prettyPercent } from '../utils';
import { LISTS_OF_HISTOGRAM_CATEGORIES } from './histogramListsAndCategories';
import { GrossRiskProfileAggs } from './interfaces';
import { BASE_METADATA_ANNOTATED_HISTO_INCOMING_HANDLER } from './shared_metatada_annotated_options';
import { roundUp } from 'src/app/utils/helpers/numerical';
import { secondYaxisDenotingCount } from './rem_options';

export const PPM_HISTOGRAM_TABLE_CHART_OPTIONS = {
  // BEGIN Industry
  AggregatedTotalPremiumPerIndustry_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalPremiumPerIndustry_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalPremiumPerIndustry_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Premium / LoB',
        'sum'
      );

      const { tableContent, piechartContent } = tableReadyContent;

      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedLOBCountPerIndustry_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedLOBCountPerIndustry_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedLOBCountPerIndustry_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'LoB IDs',
        'count',
        'count'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedTotalSumInsuredPerIndustry_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalSumInsuredPerIndustry_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalSumInsuredPerIndustry_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'TSI / LoB',
        'sum'
      );
      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedPremiumsVsTotalSumInsuredPerIndustry_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedPremiumsVsTotalSumInsuredPerIndustry_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedPremiumsVsTotalSumInsuredPerIndustry_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Ratio / LoB',
        'sum|value'
      );
      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels: tmp_labels,
        datasets,
        legendMap,
        onIncomingOptions: meta_opts
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      const [line_datasets, labels] = Transformer.toSplitLineDatasets(
        rows,
        header,
        tmp_labels
      );

      const numberOfLobsDataset = line_datasets.slice().pop();

      let onIncomingOptions = meta_opts;

      onIncomingOptions = mergeDeep(meta_opts, true, {
        type: 'line',
        legendOptions: null,
        stackOptions: {
          stacked: false
        },
        labelCallback: function (tooltipItem, data) {
          const dsetIndex = tooltipItem.datasetIndex;
          const ratioDataset = data.datasets[dsetIndex];

          const lob = ratioDataset.label;

          const industry = tooltipItem.xLabel;

          const ratio = ratioDataset.data[tooltipItem.index];
          const numberOfLobs = numberOfLobsDataset.data[tooltipItem.index];

          if (lob === 'Number of LoBs') {
            return `${industry}: ${numberOfLobs} LoBs`;
          }

          return `${lob} -> ${industry}: Ratio of Sums = ${ratio}%; LoBs = ${numberOfLobs}`;
        },
        tickOptions: {
          min: 0,
          callback: function (value, ___, __) {
            // 15680.18784 -> 15,680 %
            return `${roundUp(value).toLocaleString()} %`;
          },
          max: Math.max.apply(
            null,
            flattenArrays(
              line_datasets
                .slice(0, line_datasets.length - 1)
                .map(dset => dset.data)
            )
          )
        },
        fully_custom_y_axis: {
          ...secondYaxisDenotingCount(
            Math.max.apply(null, [].concat(numberOfLobsDataset.data)) * 1.2,
            Math.min.apply(null, [].concat(numberOfLobsDataset.data)),
            `Number of LoBs`,
            (value) => `${roundUp(value).toLocaleString()} LoBs`
          )
        }
      });

      this.handleOnIncoming(line_datasets, labels, onIncomingOptions, true);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedLocationsCountPerIndustry_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedLocationsCountPerIndustry_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedLocationsCountPerIndustry_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Locations / Lob',
        'count',
        'count'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  // END Industry
  // BEGIN Business Activity
  PPM_GlobalStatsPerBusinessActivity_NoCanvas_histogram_chart:
    BASE_METADATA_ANNOTATED_HISTO_INCOMING_HANDLER(
      'PPM_GlobalStatsPerBusinessActivity_NoCanvas_histogram_chart'
    ),
  AggregatedTotalPremiumPerBusinessActivity_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalPremiumPerBusinessActivity_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalPremiumPerBusinessActivity_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Premium / LoB',
        'sum'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        this.displayWarningToSelectLoBAndIndustry();
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedTotalOffersPerBusinessActivity_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalOffersPerBusinessActivity_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalOffersPerBusinessActivity_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'LoB IDs',
        'count',
        'count'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        this.displayWarningToSelectLoBAndIndustry();
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedTotalSumInsuredPerBusinessActivity_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalSumInsuredPerBusinessActivity_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalSumInsuredPerBusinessActivity_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'TSI / LoB',
        'sum'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        this.displayWarningToSelectLoBAndIndustry();
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedLocationsPerBusinessActivity_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedLocationsPerBusinessActivity_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedLocationsPerBusinessActivity_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Locations / Lob',
        'count',
        'count'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        this.displayWarningToSelectLoBAndIndustry();
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedPremiumsVsTotalSumInsuredPerBusinessActivity_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedPremiumsVsTotalSumInsuredPerBusinessActivity_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedPremiumsVsTotalSumInsuredPerBusinessActivity_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Ratio / LoB',
        'sum|value'
      );
      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels: tmp_labels,
        datasets,
        legendMap,
        onIncomingOptions: meta_opts
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      const [line_datasets, labels] = Transformer.toSplitLineDatasets(
        rows,
        header,
        tmp_labels
      );

      const numberOfLobsDataset = line_datasets.slice().pop();

      let onIncomingOptions = meta_opts;

      onIncomingOptions = mergeDeep(meta_opts, true, {
        type: 'line',
        legendOptions: null,
        stackOptions: {
          stacked: false
        },
        labelCallback: function (tooltipItem, data) {
          const dsetIndex = tooltipItem.datasetIndex;
          const ratioDataset = data.datasets[dsetIndex];

          const lob = ratioDataset.label;

          const bizAct = tooltipItem.xLabel;

          const ratio = ratioDataset.data[tooltipItem.index];
          const numberOfLobs = numberOfLobsDataset.data[tooltipItem.index];

          if (lob === 'Number of LoBs') {
            return `${bizAct}: ${numberOfLobs} LoBs`;
          }

          return `${lob} -> ${bizAct}: Ratio of Sums = ${ratio}%; LoBs = ${numberOfLobs}`;
        },
        tickOptions: {
          min: 0,
          callback: function (value, ___, __) {
            // 15680.18784 -> 15,680 %
            return `${roundUp(value).toLocaleString()} %`;
          },
          max: Math.max.apply(
            null,
            flattenArrays(
              line_datasets
                .slice(0, line_datasets.length - 1)
                .map(dset => dset.data)
            )
          )
        },
        fully_custom_y_axis: {
          ...secondYaxisDenotingCount(
            Math.max.apply(null, [].concat(numberOfLobsDataset.data)) * 1.2,
            Math.min.apply(null, [].concat(numberOfLobsDataset.data)),
            `Number of LoBs`,
            (value) => `${roundUp(value).toLocaleString()} LoBs`
          )
        }
      });

      this.handleOnIncoming(line_datasets, labels, onIncomingOptions, true);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  // END Business Activity
  // BEGIN Nace Codes
  PPM_GlobalStatsPerNaceCode_NoCanvas_histogram_chart:
    BASE_METADATA_ANNOTATED_HISTO_INCOMING_HANDLER(
      'PPM_GlobalStatsPerNaceCode_NoCanvas_histogram_chart'
    ),
  AggregatedTotalPremiumPerNaceCodes_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalPremiumPerNaceCodes_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalPremiumPerNaceCodes_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Premium / LoB',
        'sum'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        this.displayWarningToSelectLoBAndIndustry();
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedTotalOffersPerNaceCodes_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalOffersPerNaceCodes_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalOffersPerNaceCodes_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'LoB IDs',
        'count',
        'count'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        this.displayWarningToSelectLoBAndIndustry();
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedTotalSumInsuredPerNaceCodes_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalSumInsuredPerNaceCodes_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalSumInsuredPerNaceCodes_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'TSI / LoB',
        'sum'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        this.displayWarningToSelectLoBAndIndustry();
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedLocationsPerNaceCodes_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedLocationsPerNaceCodes_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedLocationsPerNaceCodes_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Locations / Lob',
        'count',
        'count'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        this.displayWarningToSelectLoBAndIndustry();
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  // END Nace Codes
  // BEGIN Turnover
  AggregatedTurnoverVsPremium_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTurnoverVsPremium_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTurnoverVsPremium_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformNumericalHistogramResponseIntoTableRows(
        'Turnover / LoB',
        ' '
      );
      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        []
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedTurnoverVsTSI_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTurnoverVsTSI_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTurnoverVsTSI_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformNumericalHistogramResponseIntoTableRows(
        'Turnover / LoB',
        ' '
      );
      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        []
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedTurnoverVsCount_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTurnoverVsCount_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTurnoverVsCount_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformNumericalHistogramResponseIntoTableRows(
        'LoB IDs',
        ' ',
        'count',
        '',
        'numerical_stats.aggs.aggs.doc_count'
      );
      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        []
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  // END Turnover

  GrossRiskProfile_colored_histogram_chart: {
    legend: true,
    legendTitle: 'Premium Ratios',
    handleIncomingData(inputData) {
      const aggs = inputData.aggregations as GrossRiskProfileAggs;

      const Transformer = new TableRowTransformer(
        'GrossRiskProfile_colored_histogram_chart',
        inputData,
        []
      );

      const currency = CURRENCY_PREFIX_UTF();

      const header = [
        'Country', // 0
        'LoBs', // 1
        'LoB %', // 2
        `Total Premium ${currency}`, // 3
        'Premium %', // 4
        `Max. Premium ${currency}`, // 5
        `Avg. Premium ${currency}`, // 6
        `Median Premium ${currency}`, // 7
        `Total SI ${currency}`, // 8
        `SI %`, // 9
        `Max. SI ${currency}`, // 10
        `Avg. SI ${currency}`, // 11
        `Median SI ${currency}`, // 12
        'Average Rate %', // 13
        'Average Rate / Combined Rate %' // 14
      ];

      const all_countries = aggs['histograms_ALL'];
      const total_lob_count = all_countries.doc_count;
      const total_premium = all_countries.premium_stats.stats.aggs.sum;
      const total_si = all_countries.tsi_stats.stats.aggs.sum;
      const total_average_rate = total_premium / total_si;

      const rows = LISTS_OF_HISTOGRAM_CATEGORIES.countryNamesVsISO.map(
        ([iso, country_name]) => {
          const _map = new Map<string, any>();

          const extracted_country_row = aggs[`histograms_${iso}`];

          const lob_count = extracted_country_row.doc_count;

          const premium = extracted_country_row.premium_stats.stats.aggs.sum;
          const max_premium =
            extracted_country_row.premium_stats.stats.aggs.max;
          const avg_premium =
            extracted_country_row.premium_stats.stats.aggs.avg;
          const median_premium =
            extracted_country_row.premium_median.median_agg_deepest
              .median_agg_deepest.values['50.0'];

          const tsi = extracted_country_row.tsi_stats.stats.aggs.sum;
          const max_tsi = extracted_country_row.tsi_stats.stats.aggs.max;
          const avg_tsi = extracted_country_row.tsi_stats.stats.aggs.avg;
          const median_tsi =
            extracted_country_row.tsi_median.median_agg_deepest
              .median_agg_deepest.values['50.0'];
          const avg_rate = premium / tsi;

          header.map((column_name, index) => {
            switch (index) {
              case 1:
                _map.set(column_name, lob_count);
                break;
              case 2:
                if (iso === 'ALL') {
                  _map.set(column_name, '100 %');
                } else {
                  _map.set(
                    column_name,
                    prettyPercent(lob_count, total_lob_count, false)
                  );
                }
                break;
              case 3:
                _map.set(column_name, pprintCurrency(premium));
                break;
              case 4:
                if (iso === 'ALL') {
                  _map.set(column_name, '100 %');
                } else {
                  _map.set(
                    column_name,
                    prettyPercent(premium, total_premium, false)
                  );
                }
                break;
              case 5:
                _map.set(column_name, pprintCurrency(max_premium));
                break;
              case 6:
                _map.set(column_name, pprintCurrency(avg_premium));
                break;
              case 7:
                _map.set(column_name, pprintCurrency(median_premium));
                break;
              case 8:
                _map.set(column_name, pprintCurrency(tsi));
                break;
              case 9:
                if (iso === 'ALL') {
                  _map.set(column_name, '100 %');
                } else {
                  _map.set(column_name, prettyPercent(tsi, total_si, false));
                }
                break;
              case 10:
                _map.set(column_name, pprintCurrency(max_tsi));
                break;
              case 11:
                _map.set(column_name, pprintCurrency(avg_tsi));
                break;
              case 12:
                _map.set(column_name, pprintCurrency(median_tsi));
                break;
              case 13:
                if (iso === 'ALL') {
                  _map.set(
                    column_name,
                    prettyPercent(total_average_rate, 1, false)
                  );
                } else {
                  _map.set(column_name, prettyPercent(avg_rate, 1));
                }
                break;
              case 14:
                _map.set(
                  column_name,
                  prettyPercent(avg_rate / total_average_rate, 1)
                );

                break;
            }
          });

          return { [country_name]: _map };
        }
      );
      const piechartContent = [{ category: 'Sum', count: 0 }];

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },

  // BEGIN Region of Origin
  AggregatedTotalPremiumPerRegionOfOrigin_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalPremiumPerRegionOfOrigin_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalPremiumPerRegionOfOrigin_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Premium / LoB',
        'sum'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedLOBCountPerRegionOfOrigin_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedLOBCountPerRegionOfOrigin_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedLOBCountPerRegionOfOrigin_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'LoB IDs',
        'count',
        'count'
      );
      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent
          .filter(it => !it.category.includes('Total'))
          .map(i => i.count)
          .reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedTotalSumInsuredPerRegionOfOrigin_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalSumInsuredPerRegionOfOrigin_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalSumInsuredPerRegionOfOrigin_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'TSI / LoB',
        'sum'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent
          .filter(it => !it.category.includes('Total'))
          .map(i => i.count)
          .reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedLocationsCountPerRegionOfOrigin_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedLocationsCountPerRegionOfOrigin_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedLocationsCountPerRegionOfOrigin_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Locations / Lob',
        'count',
        'count'
      );
      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent
          .filter(it => !it.category.includes('Total'))
          .map(i => i.count)
          .reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  // END Region of Origin
  // BEGIN Sales Partner
  AggregatedTotalPremiumPerSalesPartner_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalPremiumPerSalesPartner_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalPremiumPerSalesPartner_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Premium / LoB',
        'sum'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedLOBCountPerSalesPartner_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedLOBCountPerSalesPartner_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedLOBCountPerSalesPartner_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'LoB IDs',
        'count',
        'count'
      );
      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent
          .filter(it => !it.category.includes('Total'))
          .map(i => i.count)
          .reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedTotalSumInsuredPerSalesPartner_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalSumInsuredPerSalesPartner_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalSumInsuredPerSalesPartner_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'TSI / LoB',
        'sum'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent
          .filter(it => !it.category.includes('Total'))
          .map(i => i.count)
          .reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedLocationsCountPerSalesPartner_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedLocationsCountPerSalesPartner_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedLocationsCountPerSalesPartner_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Locations / Lob',
        'count',
        'count'
      );
      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent
          .filter(it => !it.category.includes('Total'))
          .map(i => i.count)
          .reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  // END Sales Partner
  // BEGIN Avg Risk Class Perils
  AggregatedTotalPremiumPerAvgRiskClassPerPeril_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalPremiumPerAvgRiskClassPerPeril_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalPremiumPerAvgRiskClassPerPeril_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Premium / LoB',
        'sum'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedTotalSumInsuredPerAvgRiskClassPerPeril_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalSumInsuredPerAvgRiskClassPerPeril_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalSumInsuredPerAvgRiskClassPerPeril_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'TSI / LoB',
        'sum'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedLOBCountPerAvgRiskClassPerPeril_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedLOBCountPerAvgRiskClassPerPeril_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedLOBCountPerAvgRiskClassPerPeril_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'LoB IDs',
        'count',
        'count'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedGroupedPremiumPerAvgRiskClassPerPeril_LoBBased_histogram_chart: {
    legend: true,
    handleIncomingData(inputData) {
      const Transformer = new TableRowTransformer(
        'AggregatedGroupedPremiumPerAvgRiskClassPerPeril_LoBBased_histogram_chart',
        inputData
      );

      let my_rows: Array<{
        [name: string]: Map<string, any>
      }> = [];
      let my_headers = ['Risk Class', 'Peril-spc. Mpremiums', 'Peril-spc. Mpremiums %', 'LoB IDs', 'LoB IDs %', 'Peril-spc. SI', 'Peril-spc. SI %'];

      const totals = {
        m_premium_peril_specific_sum: 0,
        lob_count: 0,
        tsi_peril_specific_sum: 0
      };

      sortArrOfStringsAlphabetically(Object.keys(Transformer.resp.aggregations)).map((agg_key: string) => {
        const true_key = agg_key.split('histograms_')[1];
        const current_agg = Transformer.resp.aggregations[agg_key];
        const true_agg = current_agg[true_key];

        const m_premium_peril_specific_sum = true_agg.m_premium_peril_specific_sum.sum.value;
        const lob_count = true_agg.lob_count.cardinality.value;
        const tsi_peril_specific_sum = true_agg.tsi_peril_specific_sum.sum.value;

        totals.m_premium_peril_specific_sum += m_premium_peril_specific_sum;
        totals.lob_count += lob_count;
        totals.tsi_peril_specific_sum += tsi_peril_specific_sum;

        return agg_key;
      }).map((agg_key: string) => {
        const true_key = agg_key.split('histograms_')[1];
        const current_agg = Transformer.resp.aggregations[agg_key];
        const true_agg = current_agg[true_key];

        const rowMap: Map<string, any> = new Map();

        const m_premium_peril_specific_sum = true_agg.m_premium_peril_specific_sum.sum.value;
        const lob_count = true_agg.lob_count.cardinality.value;
        const tsi_peril_specific_sum = true_agg.tsi_peril_specific_sum.sum.value;

        rowMap.set('Peril-spc. Mpremiums', pprintCurrency(m_premium_peril_specific_sum));
        rowMap.set('Peril-spc. Mpremiums %', prettyPercent(m_premium_peril_specific_sum, totals.m_premium_peril_specific_sum));
        rowMap.set('LoB IDs', lob_count);
        rowMap.set('LoB IDs %', prettyPercent(lob_count, totals.lob_count));
        rowMap.set('Peril-spc. SI', pprintCurrency(tsi_peril_specific_sum));
        rowMap.set('Peril-spc. SI %', prettyPercent(tsi_peril_specific_sum, totals.tsi_peril_specific_sum));

        my_rows.push({
          [true_key]: rowMap
        });
      });

      const pieChartDataset = Transformer.sumUpColumnsOfMapRows(
        my_rows as any,
        [/.*/g]
      );

      const [headers, rows] = Transformer.appendCumulativeColumnSumsToRows(
        my_headers,
        my_rows,
        pieChartDataset,
        null,
        {
          total_keyword: 'Total'
        }
      );

      my_headers = headers;
      my_rows = rows;

      // const to_return = {
      //   tableContent: {
      //     header: my_headers,
      //     rows: my_rows
      //   },
      //   piechartContent: pieChartDataset
      // };

      my_rows.map((row_group, index) => {
        if (index === my_rows.length - 1) {
          const sum_map = row_group[SUM_SIGMA_UTF];

          sum_map.set('Peril-spc. Mpremiums', pprintCurrency(sum_map.get('Peril-spc. Mpremiums')));
          sum_map.set('Peril-spc. Mpremiums %', '100 %');
          sum_map.set('LoB IDs %', '100 %');
          sum_map.set('Peril-spc. SI', pprintCurrency(sum_map.get('Peril-spc. SI')));
          sum_map.set('Peril-spc. SI %', '100 %');

          my_rows[index] = { [SUM_SIGMA_UTF]: sum_map };
        }
      });

      this.csvInput = rowMapToCSV(my_headers, my_rows);
      this.rawCsvInput = rowMapToCSV(my_headers, my_rows, true);

      // const {
      //   labels,
      //   datasets,
      //   onIncomingOptions
      // } = Transformer.toPieChartJSArguments(
      //   to_return.piechartContent,
      //   to_return.piechartContent.map(i => i.count).reduce(add, 0),
      //   [],
      //   tableCellPostProcessingFunctions[Transformer.query_id]
      // );

      // this.handleOnIncoming(datasets, labels, onIncomingOptions);
    }
  },
  AggregatedGroupedPremiumPerAvgRiskClassPerPeril_location_based_histogram_chart: {
    legend: true,
    handleIncomingData(inputData) {
      const Transformer = new TableRowTransformer(
        'AggregatedGroupedPremiumPerAvgRiskClassPerPeril_location_based_histogram_chart',
        inputData
      );

      let my_rows: Array<{
        [name: string]: Map<string, any>
      }> = [];
      let my_headers = ['Risk Class', 'Peril-spc. Mpremiums', 'Peril-spc. Mpremiums %', 'Location IDs', 'Location IDs %', 'Peril-spc. SI', 'Peril-spc. SI %'];

      const totals = {
        m_premium_peril_specific_sum: 0,
        address_count: 0,
        tsi_peril_specific_sum: 0
      };

      sortArrOfStringsAlphabetically(Object.keys(Transformer.resp.aggregations)).map((agg_key: string) => {
        const true_key = agg_key.split('histograms_')[1];
        const current_agg = Transformer.resp.aggregations[agg_key];
        const true_agg = current_agg[true_key];

        const m_premium_peril_specific_sum = true_agg.m_premium_peril_specific_sum.sum.value;
        const address_count = true_agg.address_count.cardinality.value;
        const tsi_peril_specific_sum = true_agg.tsi_peril_specific_sum.sum.value;

        totals.m_premium_peril_specific_sum += m_premium_peril_specific_sum;
        totals.address_count += address_count;
        totals.tsi_peril_specific_sum += tsi_peril_specific_sum;

        return agg_key;
      }).map((agg_key: string) => {
        const true_key = agg_key.split('histograms_')[1];
        const current_agg = Transformer.resp.aggregations[agg_key];
        const true_agg = current_agg[true_key];

        const rowMap: Map<string, any> = new Map();

        const m_premium_peril_specific_sum = true_agg.m_premium_peril_specific_sum.sum.value;
        const address_count = true_agg.address_count.cardinality.value;
        const tsi_peril_specific_sum = true_agg.tsi_peril_specific_sum.sum.value;

        rowMap.set('Peril-spc. Mpremiums', pprintCurrency(m_premium_peril_specific_sum));
        rowMap.set('Peril-spc. Mpremiums %', prettyPercent(m_premium_peril_specific_sum, totals.m_premium_peril_specific_sum));
        rowMap.set('Location IDs', address_count);
        rowMap.set('Location IDs %', prettyPercent(address_count, totals.address_count));
        rowMap.set('Peril-spc. SI', pprintCurrency(tsi_peril_specific_sum));
        rowMap.set('Peril-spc. SI %', prettyPercent(tsi_peril_specific_sum, totals.tsi_peril_specific_sum));

        my_rows.push({
          [true_key]: rowMap
        });
      });

      const pieChartDataset = Transformer.sumUpColumnsOfMapRows(
        my_rows as any,
        [/.*/g]
      );

      const [headers, rows] = Transformer.appendCumulativeColumnSumsToRows(
        my_headers,
        my_rows,
        pieChartDataset,
        null,
        {
          total_keyword: 'Total'
        }
      );

      my_headers = headers;
      my_rows = rows;

      // const to_return = {
      //   tableContent: {
      //     header: my_headers,
      //     rows: my_rows
      //   },
      //   piechartContent: pieChartDataset
      // };

      my_rows.map((row_group, index) => {
        if (index === my_rows.length - 1) {
          const sum_map = row_group[SUM_SIGMA_UTF];

          sum_map.set('Peril-spc. Mpremiums', pprintCurrency(sum_map.get('Peril-spc. Mpremiums')));
          sum_map.set('Peril-spc. Mpremiums %', '100 %');
          sum_map.set('Location IDs %', '100 %');
          sum_map.set('Peril-spc. SI', pprintCurrency(sum_map.get('Peril-spc. SI')));
          sum_map.set('Peril-spc. SI %', '100 %');

          my_rows[index] = { [SUM_SIGMA_UTF]: sum_map };
        }
      });

      this.csvInput = rowMapToCSV(my_headers, my_rows);
      this.rawCsvInput = rowMapToCSV(my_headers, my_rows, true);

      // const {
      //   labels,
      //   datasets,
      //   onIncomingOptions
      // } = Transformer.toPieChartJSArguments(
      //   to_return.piechartContent,
      //   to_return.piechartContent.map(i => i.count).reduce(add, 0),
      //   [],
      //   tableCellPostProcessingFunctions[Transformer.query_id]
      // );

      // this.handleOnIncoming(datasets, labels, onIncomingOptions);
    }
  },

  // End Avg Risk Class Perils
  // BEGIN Numerical Histo Aggs (1, 10, 50, 200...)
  AggregatedSI_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedSI_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedSI_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformNumericalHistogramResponseIntoTableRows(
        'Sum Insured / LoB'
      );
      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent
          .filter(it => !it.category.includes(SUM_SIGMA_UTF))
          .map(i => i.count)
          .reduce(add, 0),
        []
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedPremium_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedPremium_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedPremium_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformNumericalHistogramResponseIntoTableRows(
        'Premiums / LoB'
      );
      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent
          .filter(it => !it.category.includes(SUM_SIGMA_UTF))
          .map(i => i.count)
          .reduce(add, 0),
        []
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },

  // BEGIN PDAs
  AggregatedAvgPDAPerAvgRiskClassPerPeril_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedAvgPDAPerAvgRiskClassPerPeril_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedAvgPDAPerAvgRiskClassPerPeril_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Policies / LoB',
        'avg'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedAvgPDAPerTSIPerPeril_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedAvgPDAPerTSIPerPeril_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedAvgPDAPerTSIPerPeril_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Policies / LoB',
        'avg'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },

  // BEGIN Broker Commission
  AggregatedBrokerCommission_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedBrokerCommission_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedBrokerCommission_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Commission / LoB',
        'avg'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      // no graph --> wrap it up.
      this.kanvas = null;
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },

  // BEGIN Per Peril
  AggregatedTotalMPremiumPerPeril_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalMPremiumPerPeril_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalMPremiumPerPeril_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Premium / LoB',
        'sum'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedTotalTPremiumPerPeril_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTotalTPremiumPerPeril_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTotalTPremiumPerPeril_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Premium / LoB',
        'sum'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedRiskScorePerAvgRiskClassPerPeril_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedRiskScorePerAvgRiskClassPerPeril_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedRiskScorePerAvgRiskClassPerPeril_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Avg Score / LoB',
        'avg'
      );

      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        [],
        tableCellPostProcessingFunctions[Transformer.query_id]
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedRiskScoreVsPeril_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedRiskScoreVsPeril_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedRiskScoreVsPeril_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformNumericalHistogramResponseIntoTableRows(
        'LoB IDs',
        ' ',
        'count',
        key => {
          const extract = extractInt(key);
          if (extract === 0) {
            return `Only `;
          } else {
            return `${extract - 20} - `;
          }
        }
      );
      const { tableContent, piechartContent } = tableReadyContent;
      const { header, rows } = tableContent;

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      const {
        labels,
        datasets,
        legendMap,
        onIncomingOptions
      } = Transformer.toPieChartJSArguments(
        piechartContent,
        piechartContent.map(i => i.count).reduce(add, 0),
        []
      );

      this.handleOnIncoming(datasets, labels, onIncomingOptions);
      // this.legendMap = jsonToKeyValueTable(legendMap);
    }
  },
  AggregatedInfoLevelVsRiskClassVsPeril_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedInfoLevelVsRiskClassVsPeril_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedInfoLevelVsRiskClassVsPeril_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Info Level / LoB',
        'avg'
      );

      const { tableContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      // no chart needed, carry on
      this.kanvas = null;
    }
  },
  // END Per Peril
  // BEGIN Superscore
  AggregatedSuperscoresPerLocationsPerIndustry_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedSuperscoresPerLocationsPerIndustry_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedSuperscoresPerLocationsPerIndustry_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Superscore / LoB',
        'avg'
      );

      const { tableContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      // no chart needed, carry on
      this.kanvas = null;
    }
  },
  AggregatedSuperscoresPerRequestFromCountry_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedSuperscoresPerRequestFromCountry_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedSuperscoresPerRequestFromCountry_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Superscore / LoB',
        'avg'
      );

      const { tableContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      // no chart needed, carry on
      this.kanvas = null;
    }
  },
  AggregatedSuperscoresPerBucketedTSI_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedSuperscoresPerBucketedTSI_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedSuperscoresPerBucketedTSI_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Superscore / LoB',
        'avg'
      );

      const { tableContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      // no chart needed, carry on
      this.kanvas = null;
    }
  },
  // END Superscore
  // BEGIN Insured Object
  AggregatedTSIPerInsuredObject_histogram_chart: {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const { app } = kwargs;

      const selectedValues = getValuesOfAffectingFilters(
        app,
        'AggregatedTSIPerInsuredObject_histogram_chart'
      );

      const Transformer = new TableRowTransformer(
        'AggregatedTSIPerInsuredObject_histogram_chart',
        inputData,
        selectedValues
      );

      const tableReadyContent = Transformer.transformCategorizedSpreadResponseIntoTableRows(
        'Insured Object / LoB',
        'sum'
      );

      const { tableContent } = tableReadyContent;
      const { header, rows } = tableContent;

      if (!header.length || !header[0]) {
        return;
      }

      this.csvInput = rowMapToCSV(header, rows);
      this.rawCsvInput = rowMapToCSV(header, rows, true);

      // no chart needed, carry on
      this.kanvas = null;
    }
  },
  // END Insured Object
  // BEGIN Construction Materials
  AggregatedTSIPerConstructionMaterials_location_based_histogram_chart: {
    legend: true,
    handleIncomingData(inputData) {
      const Transformer = new TableRowTransformer(
        'AggregatedTSIPerConstructionMaterials_location_based_histogram_chart',
        inputData
      );

      let my_rows: Array<{
        [name: string]: Map<string, any>
      }> = [];
      let my_headers = [
        'Construction Material',
        'Location Count',
        'Location Count %',
        'Agg Location SI',
        'Agg Location SI %'
      ];

      const totals = {
        tsi_per_location: 0,
        location_count: 0
      };

      sortArrOfStringsAlphabetically(Object.keys(Transformer.resp.aggregations)).map((agg_key: string) => {
        const true_key = agg_key.split('histograms_')[1];
        const current_agg = Transformer.resp.aggregations[agg_key];
        const true_agg = current_agg[true_key];

        const tsi_per_location = true_agg.tsi_per_location.sum.value;
        const location_count = true_agg.location_count.cardinality.value;

        totals.tsi_per_location += tsi_per_location;
        totals.location_count += location_count;

        return agg_key;
      }).map((agg_key: string) => {
        const true_key = agg_key.split('histograms_')[1];
        const current_agg = Transformer.resp.aggregations[agg_key];
        const true_agg = current_agg[true_key];

        const rowMap: Map<string, any> = new Map();

        const tsi_per_location = true_agg.tsi_per_location.sum.value;
        const location_count = true_agg.location_count.cardinality.value;

        rowMap.set('Location Count', location_count);
        rowMap.set('Location Count %', prettyPercent(location_count, totals.location_count));
        rowMap.set('Agg Location SI', pprintCurrency(tsi_per_location));
        rowMap.set('Agg Location SI %', prettyPercent(tsi_per_location, totals.tsi_per_location));

        my_rows.push({
          [true_key]: rowMap
        });
      });

      const pieChartDataset = Transformer.sumUpColumnsOfMapRows(
        my_rows as any,
        [/.*/g]
      );

      const [headers, rows] = Transformer.appendCumulativeColumnSumsToRows(
        my_headers,
        my_rows,
        pieChartDataset,
        null,
        {
          total_keyword: 'Total'
        }
      );

      my_headers = headers;
      my_rows = rows;

      // const to_return = {
      //   tableContent: {
      //     header: my_headers,
      //     rows: my_rows
      //   },
      //   piechartContent: pieChartDataset
      // };

      my_rows.map((row_group, index) => {
        if (index === my_rows.length - 1) {
          const sum_map = row_group[SUM_SIGMA_UTF];

          sum_map.set('Location Count %', '100 %');
          sum_map.set('Agg Location SI', pprintCurrency(sum_map.get('Agg Location SI')));
          sum_map.set('Agg Location SI %', '100 %');

          my_rows[index] = { [SUM_SIGMA_UTF]: sum_map };
        }
      });

      const csv = rowMapToCSV(my_headers, my_rows);
      this.csvInput = csv;

      // const {
      //   labels,
      //   datasets,
      //   onIncomingOptions
      // } = Transformer.toPieChartJSArguments(
      //   to_return.piechartContent,
      //   to_return.piechartContent.map(i => i.count).reduce(add, 0),
      //   [],
      //   tableCellPostProcessingFunctions[Transformer.query_id]
      // );

      // this.handleOnIncoming(datasets, labels, onIncomingOptions);
    }
  }
  // END Construction Materials
};
