import {
  Component,
  OnInit,
  ViewChild,
  Input,
  ViewEncapsulation,
  OnDestroy,
} from '@angular/core';
import {
  colorSchemeVividDomainAsRgbArray,
  fitCanvasToContainer,
  lobVsColor,
} from '../common/constants';
import {
  ChartjsService,
  onAnimationCompleteTechniques,
} from 'src/app/services/chartjs.service';
import { CustomChartDirective } from '../common/chart';
import {
  HISTOGRAM_TABLE_CHART_OPTIONS,
  assignTableCSSClasses,
} from './constants';
import { OnIncomingData } from 'src/app/interfaces/on-incoming-data';
import { ClipboardService } from 'src/app/services/clipboard.service';
import { highlightJSON, regexMatches } from 'src/app/utils/helpers/helpers';

import {
  getTranformFns,
  rowMapToCSV,
  CHART_FUNCT,
  chartIdToChartType,
  REM_AGGS_ONLY_KEYS,
} from './common';
import { GeojsonService } from 'src/app/services/geojson.service';
import { CanvasDrawingService } from 'src/app/services/canvas-drawing.service';
import { DataMarkerService } from 'src/app/services/data-marker.service';
import { CsvService } from 'src/app/services/csv.service';
import { filter } from 'rxjs/operators';
import { DivDownloaderService } from 'src/app/services/div-downloader.service';
import { IxyHit } from './interfaces';
import { Chart } from 'chart.js';
import { ElasticService } from 'src/app/services/elastic.service';

@Component({
  selector: 'app-histogram-table-chart',
  templateUrl: './histogram-table-chart.component.html',
  styleUrls: ['./histogram-table-chart.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HistogramTableChartComponent
  extends CustomChartDirective
  implements OnInit, OnIncomingData, OnDestroy {
  @ViewChild('canvas', { static: false })
  canvas;

  @Input()
  canvasw;
  @Input()
  canvash;

  @Input() emptyishInfoNotice;

  @Input() set _csvInput(value: string) {
    this.csvInput = value;
  }

  @Input() pageSize = 50

  @Input() set _underlyingCsvInput(value: string) {
    this.dataHashCSV = value;
  }

  _colors = colorSchemeVividDomainAsRgbArray();
  _softer_colors = colorSchemeVividDomainAsRgbArray(0.4);
  _datasets: Array<any>;
  _labels: string[];
  _counts: any;
  totalCount: number;

  _opts: any;

  legendMap: object;

  _drawnChart: Chart | any;
  yScalesTypeIsLinear = true;
  linearScalesSpec: Array<any> = [];

  height: number;
  width: number;

  query_id: string;
  originalQuery: any;

  displayWarning = false;

  isLoading = false;
  csvInput: string;
  rawCsvInput: string;
  underlyingCsvInput: string;

  jsonPanelInput: ReturnType<typeof highlightJSON> | null;
  chartType: ReturnType<typeof chartIdToChartType> & 'scatter';
  data_hash: Array<{ x: number; y: number; hit: any }>;

  additionalTableInput: string;
  downloadableTableInput: string;
  dataHashCSV: string;
  additionalDataHashCSV: string;

  level3only = false;
  hideTrendlines = false;

  kanvas = undefined; // canvas hider helper

  REM_AGGS_ONLY_KEYS = REM_AGGS_ONLY_KEYS;

  hidePagination: boolean

  public destroyCanvasChart() {
    if (this._drawnChart) {
      this._drawnChart.destroy();
    }

    if (this.legendMap) {
      this.legendMap = undefined;
    }
  }

  extraClassesToUse() {
    let klasses = 'fitWidth';

    if (
      this.klass !== 'AggregatedBrokerCommission_histogram_chart' &&
      !regexMatches(/PremiumsVsTotalSumInsured/, this.klass) && !this.klass.includes('ESG')
    ) {
      klasses += ' lastRowIsSummary';
    }

    if (this.klass.includes('Activity')) {
      klasses += ' broad-columns';
    }

    if (
      this.klass.includes('RiskClass') &&
      !this.klass.includes('PDA') &&
      !this.klass.includes('RiskScore') &&
      !this.klass.includes('InfoLevel') &&
      !this.klass.includes('AvgRiskClass')
    ) {
      klasses += ' last3RowsAreSummary';
    }

    klasses += ' ' + assignTableCSSClasses(this.klass);

    return klasses;
  }

  constructor(
    private es: ElasticService,
    private chart_service: ChartjsService,
    private clipser: ClipboardService,
    private csvService: CsvService,
    private dataMarkerService: DataMarkerService,
    private geoJSONService: GeojsonService,
    private canvasDrawingService: CanvasDrawingService,
    private div: DivDownloaderService
  ) {
    super();
    this.query_id = this.klass;
  }

  ngOnInit() {
    if (this.klass === 'noquery') {
      this.displayWarning = false;
    } else {
      this.es.app_component.setComponent(this.klass, this);
      Object.assign(this, HISTOGRAM_TABLE_CHART_OPTIONS.custom[this.klass]);
    }
  }

  ngOnDestroy() {
    // try {
    //   this.canvasDrawingService.drawnCoordsObservable.unsubscribe();
    // } catch (err) {}
  }

  displayWarningToSelectLoBAndIndustry() {
    this.destroyCanvasChart();
    this.displayWarning = true;
  }

  private QUERY_ID_VS_CHART_TYPE = (id: string) => {
    return chartIdToChartType(id);
  }

  handleOnIncoming(datasets, labels, options, straight_to_bar_chart?) {
    const self = this;

    if (!self.canvas) {
      return;
    }

    self.yScalesTypeIsLinear = true;

    self.jsonPanelInput = null;
    self.additionalTableInput = null;
    self.downloadableTableInput = null;

    this.destroyCanvasChart();
    this.displayWarning = false;

    const { chart_label_body: body_fn_ } = getTranformFns(this.klass);

    let type = this.QUERY_ID_VS_CHART_TYPE(this.klass);

    if (straight_to_bar_chart) {
      type = options.type || 'bar';
    } else {
      if (!type) {
        type = 'bar';
      }
      options.type = type;
    }

    const checkedOptions =
      type !== 'bar'
        ? options
        : {
          annotationTechniques: null,
          tickOptions: {
            callback: function (value, ___, __) {
              return `${value}`;
            },
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.noop,
          customAnnotations: options.customAnnotations,
          stackOptions: {
            stacked: true,
          },
          fully_custom_y_axis: options.fully_custom_y_axis,
          labelCallback: function (tooltipItem, data) {
            let left_label = data.datasets[tooltipItem.datasetIndex].label;
            if (
              typeof left_label === 'undefined' ||
              !left_label.trim().length
            ) {
              left_label = tooltipItem['xLabel'];
            }

            let final_val = body_fn_(tooltipItem['yLabel']);
            if (
              data.datasets[tooltipItem.datasetIndex].label.startsWith(
                'Number of'
              )
            ) {
              final_val = tooltipItem['yLabel'];
            }

            return `${left_label}: ${final_val}`;
          },
        };

    const opts = {
      type: type,
      labels: labels
        ? labels.map((label) =>
          datasets.length !== 1 ? CHART_FUNCT.ellipsis(label) : label
        )
        : [],
      datasets: datasets.map((dataset, index) => {
        const lob_vs_color =
          labels && labels.length && lobVsColor(labels[0], index !== 0);

        if (lob_vs_color) {
          return {
            backgroundColor: labels.map((label) =>
              lobVsColor(label, index !== 0)
            ),
            ...dataset,
          };
        }

        return {
          backgroundColor:
            dataset.backgroundColor ||
            (options.customColors
              ? options.customColors[index]
              : index === 0
                ? this._colors
                : this._softer_colors),
          ...dataset,
        };
      }),
      ...(straight_to_bar_chart ? options : checkedOptions),
      ...(options?.options ? options.options : {})
    };

    self.chartType = <typeof self.chartType>type;
    self._opts = Object.assign({}, opts);

    if (straight_to_bar_chart) {
      this._drawnChart = this.chart_service.constructNormalizedVerticalBarChart(
        this.canvas.nativeElement,
        {
          ...opts,
        },
        type
      );

      fitCanvasToContainer(self.canvas.nativeElement, self.klass);
      setTimeout(() => {
        self._drawnChart.resize();
        self._drawnChart.update();
      }, 1e3);

      this.handleDrawingOnChart(self.klass);
      return;
    }

    self.canvas.nativeElement.classList.add(type);

    if (type === 'bar') {
      this._drawnChart = this.chart_service.constructNormalizedVerticalBarChart(
        this.canvas.nativeElement,
        opts,
        type
      );
    } else {
      this._drawnChart = this.chart_service.constructPieChart(
        this.canvas.nativeElement,
        opts
      );
    }

    fitCanvasToContainer(self.canvas.nativeElement, self.klass, type);
    setTimeout(() => {
      self._drawnChart.resize();
      self._drawnChart.update();
    }, 1e3);

    // this.cd.detectChanges();
  }

  copyToClipBoard() {
    this.clipser.setFromInputId(`originalQuery${this.klass}`);
  }

  handleDrawingOnChart(klass: string) {
    this.canvasDrawingService.drawnCoordsObservable
      .pipe(filter((observer) => observer.klass === klass))
      .subscribe((observer) => {
        if (
          observer &&
          Array.isArray(observer.coordinates) &&
          observer.coordinates.length
        ) {
          const poly_array = observer.coordinates.slice();

          const converted_from_meta_datasets: Array<{
            x: number;
            y: number;
            hit: any;
          }> = this.geoJSONService.mergeArrays(
            ...this._opts.datasets.map((_, dset_index) => {
              return this._drawnChart
                .getDatasetMeta(dset_index)
                .data.map((dpoint, dpoint_index) => {
                  return {
                    x: dpoint._model.x,
                    y: dpoint._model.y,
                    hit: this._opts.datasets[dset_index].data[dpoint_index],
                  };
                });
            })
          );

          const collection = this.geoJSONService.createCanvasPointCollection(
            converted_from_meta_datasets
          );

          // UMGB-7 added the possibility to filter collection
          // members according to their score assessment level (for now)

          let collectionMembersFilteringFn: Function = () => true; // pass thru default

          if (
            [
              'AggregatedAddressesPerScorePerPML_address_based_histogram_chart',
              'AggregatedAddressesPerAsIfScorePerPML_address_based_histogram_chart',
              'AggregatedAddressesPerAsIfScorePerPML_EML_Ratio_address_based_histogram_chart',
            ].includes(this.klass)
          ) {
            type DatasetType = Chart['data']['datasets'][number];
            // easy access
            const datasets = (this._drawnChart as Chart).data.datasets;

            // access the `hidden` property of a dataset thru its `_meta` field
            // https://stackoverflow.com/a/41878442/8160318
            const isHidden = (dataset: DatasetType) =>
              (dataset as any)._meta[Object.keys((dataset as any)._meta)[0]]
                .hidden;

            // figure out the non-hidden datasets
            const shownDatasetNames = datasets
              ?.filter((dset) => !isHidden(dset))
              .map((dset) => dset.label);

            // filter out those datasets whose `name` isn't desired
            collectionMembersFilteringFn = (
              predicate: typeof collection['features'][number]
            ) =>
              shownDatasetNames.includes(
                `Level ${(predicate.properties as IxyHit).level}`
              );
          }

          const filtered = this.geoJSONService.filterCanvasPointsByBoundary(
            collection,
            poly_array,
            collectionMembersFilteringFn
          );

          const { header, rows } =
            this.dataMarkerService.prepareScoreMatrixTable(filtered);
          const client_report_array =
            this.dataMarkerService.prepareClientReportInsideScatterPlotPolygon(
              filtered
            );

          const table_csv = rowMapToCSV(header, rows);

          const client_report_csv =
            this.csvService.nestedJSONtoCSV(client_report_array, {
              // the currency guessing takes a sh!t ton of time so let's disable it here completely
              // fixes UDGB-31
              doGuessCurrencySuffix: false
            });

          this.additionalTableInput = table_csv;
          this.downloadableTableInput = client_report_csv;
        } else {
          this.additionalTableInput = null;
          this.downloadableTableInput = null;
        }
      });
  }

  downloadDatahashCsv() {
    const csv_rows = this.dataHashCSV;
    if (!csv_rows) {
      return;
    }

    this.div.downloadNestedCSV(
      null,
      `Underlying Data: ${this.klass}`,
      csv_rows
    );
  }

  downloadAdditionalDatahashCsv() {
    const csv_rows = this.additionalDataHashCSV;
    if (!csv_rows) {
      return;
    }

    this.div.downloadNestedCSV(
      null,
      `Additional Data: ${this.klass}`,
      csv_rows
    );
  }

  switchToAndFromLogarithmicYScales() {
    if (!this._drawnChart) {
      return;
    }

    try {
      if (this.yScalesTypeIsLinear === true) {
        // switch to linear

        if (!this.linearScalesSpec.length) {
          (this._drawnChart.options.scales.yAxes as Array<any>)
            .slice()
            .map((_, index) => {
              this._drawnChart.options.scales.yAxes[index].type = 'linear';
            });

          this.linearScalesSpec = this._drawnChart.options.scales.yAxes.slice();
        } else {
          this._drawnChart.options.scales.yAxes = this.linearScalesSpec.slice();
        }
      } else {
        (this._drawnChart.options.scales.yAxes as Array<any>)
          .slice()
          .map((_, index) => {
            this._drawnChart.options.scales.yAxes[index].ticks.autoSkip = true;
            this._drawnChart.options.scales.yAxes[
              index
            ].ticks.maxTicksLimit = 20;
            this._drawnChart.options.scales.yAxes[index].type = 'logarithmic';
          });
      }

      this._drawnChart.update();
    } catch (err) {
      console.warn({ err });
    }
  }
}
