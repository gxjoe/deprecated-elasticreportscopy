import * as _ from 'lodash';

import {
  pprintCurrency,
  nFormatter,
  regexMatches,
  extractNumberFromString,
  capitalize,
  concatMaps,
  sortArrayOfRiskClasses,
  sortArrayOfRisks,
  sortArrOfStringsAlphabetically,
  sortByExtractingNumbersFromStrings,
  extractFirstValueFromDict,
  add,
  extractInt,
  CURRENCY_PREFIX_UTF,
  premiumDeviation,
  isObject,
  flattenArrays,
  safeStringForRegex,
} from '../../utils/helpers/helpers';
import {
  prettyPercent,
  stripHtml,
  percentageOfBuckets,
  sortArrayByKey,
  dynamicSort,
} from '../utils';
import { roundUp } from 'src/app/utils/helpers/numerical';
import { _getValueAtIndexOrDefault } from 'src/app/utils/helpers/chartjs.helper';
import { SearchResponse } from 'elasticsearch';
import { onAnimationCompleteTechniques } from '../../services/chartjs.service';

import { findByKeyInNestedObject } from 'src/app/elastic/utils';
import {
  NaceCodes,
  LISTS_OF_HISTOGRAM_CATEGORIES,
} from './histogramListsAndCategories';
import { hexToRgba, lobVsColor } from '../common/constants';
import {
  IAggregationQueryBuilderResponse,
  AGG_VALUE_FORMATTERS,
} from './queryBuilder';
import * as objectPath from 'object-path';
import { CsvService } from 'src/app/services/csv.service';
import { ActualAssessmentRecommendation, Address, AssessmentRecommendation } from 'src/app/interfaces/elastic';

export const numericalRegex = new RegExp(/\d+/g);
export const strBeginsWithCurrencyLikeRegex = new RegExp(
  Object.keys(LISTS_OF_HISTOGRAM_CATEGORIES.mapOfISOcurreciesToUTF)
    .map((okey) => {
      return `(${okey}|${LISTS_OF_HISTOGRAM_CATEGORIES.mapOfISOcurreciesToUTF[okey]})s?(.+)`;
    })
    .join('|')
);
export const strBeginsLikePriceBucket = new RegExp(
  [
    0, 1e6, 5e6, 10e6, 20e6, 30e6, 50e6, 100e6, 200e6, 300e6, 500e6, 1e9, 2e9,
    3e9, 5e9, 10e9, 50e9,
  ]
    .map((si) => nFormatter(si, 1))
    .join('|')
);

export const strBeginsWithHistogramBucketingRegex =
  /((Only)?(\s?\d+\s?\-\s?\d+|\s?\d+)\s+?)|(Not Recognized)$/gim;
export const strEndsNumericallyRegex = new RegExp(/^[^less][^\d]+\d{1,}$/g);
export const strBeginsNumericallyRegex = new RegExp(/\d{1,3}\s?(...+)/g);
export const globalStatsNames = new RegExp(/^sum|count$/gi);
export const naceCodeAlphaNumInStr = new RegExp(/nacecodes/gi);
export const onlyNumericalRiskClass = new RegExp(/riskClass/i);
export const riskClassAbbreviaton = new RegExp(/RC\s\d{1,2}/g);
export const pdaInQuery = new RegExp(/PDA|personalDeviationAuthority/g);
export const TSIPerPerilInQuery = new RegExp(/SumInsured/g);
export const TSI_PDAInQuery = new RegExp(/PDAPerTSI/g);
export const brokerCommissionInQuery = /brokerCommission/gi;
export const infoLevelInQuery = /InfoLevel/g;
export const riskScoreInQuery = /riskScore/gim;
export const superScoreInQuery = /superScore/gi;
export const insuredObjectsInQuery = /insuredObject/gi;
export const histoNumStatsBucketName = {
  extractor: (name: string) =>
    new RegExp(/((less_\d+)|(_other_))(\|)(.+)/).exec(name)[1],
  matcher: (name: string) =>
    new RegExp(/((less_\d+)|(_other_))(\|)(.+)/).test(name),
};
export const dontUsePiechartRE = new RegExp(/^((?!PDA|RiskScore).)*$/g);
export const OTHER_BUCKET_NAME = 'Not Recognized';
export const otherBucketNameRegex = new RegExp(OTHER_BUCKET_NAME, 'gim');
export const wildcardRegex = new RegExp('');

export const includeMinMaxInRowMaps = new RegExp(dontUsePiechartRE); // more to come

export const numericalCategoryPrefacedWithLettersStandaloneRegex = new RegExp(
  /\N\s\d{4}/g
);

export const isTurnoverLike = (name: string) =>
  regexMatches(/turnover/i, name) && !regexMatches(/count/i, name);

export const CATEGORIZED_PIECHART_DATASET_MAX_LENGTH = 20;
export const CATEGORIZED_BARCHART_DATASET_MAX_LENGTH = 30;

export interface ITableReadyContentRow {
  [trueKey: string]: Map<string, any>;
}

export interface ITableReadyContent {
  header: Array<string>;
  rows: Array<ITableReadyContentRow>;
}

export interface IPieChartDataset {
  category: string;
  count: number;
  _occurrence?: number;
  _count?: any;
}

export interface ITableContentAndPieChartData {
  tableContent: ITableReadyContent;
  piechartContent: Array<IPieChartDataset>;
}

export interface ITableCellPostProcessingGroup {
  header: Function;
  body: Function;
  chart_label_header?: Function;
  chart_label_body?: Function;
  nonexistent?: boolean;
}

export interface ITablePostProcessingFunctions {
  [index: string]: {
    header: Function;
    body: Function;
    chart_label_header?: Function;
    chart_label_body?: Function;
  };
}

export const getTranformFns = (id): ITableCellPostProcessingGroup => {
  if (id in tableCellPostProcessingFunctions) {
    return tableCellPostProcessingFunctions[id];
  }
  return {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.identity,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.identity,
    nonexistent: true,
  };
};

export const SUM_SIGMA_UTF = '\u2211';
export const CSV_SEPARATOR = ';';

const MIDDLE_DOT_UTF = '\u00B7';

// There are certain keywords like construction materials
// of the form "Metal sheathing (PIR)"
// that should NOT have the parentheses replaced.
// On the other hand, variants occurring in aggregation
// names like "Premium 1) Ratio 2)" or "Sum Insured 12)"
// which were designed to help sort aggregation rows do
// need to be replaced when rendered in the Table component.
// The reasoning behind the regex is here: https://regex101.com/r/8qw0m2/1
export const COLUMN_NAME_CLEANSER_RE =
  /(^(([a-zA-Z-_\d]{1,2})\)\s?)+)|(((\s[a-zA-Z-_\d]{1,2})\))+)/g;

export const chartIdToChartType = (id: string): 'line' | 'bar' | 'pie' => {
  switch (id) {
    case 'AggregatedPremiumsVsTotalSumInsuredPerIndustry_histogram_chart':
    case 'AggregatedPremiumsVsTotalSumInsuredPerBusinessActivity_histogram_chart':
      return 'line';
    case 'AggregatedAvgPDAPerAvgRiskClassPerPeril_histogram_chart':
      return 'bar';
    case 'AggregatedTotalMPremiumPerPeril_histogram_chart':
    case 'AggregatedTotalTPremiumPerPeril_histogram_chart':
      return 'pie';
    default:
      return 'bar';
  }
};

export const csvToJSON = (csv, separator = CSV_SEPARATOR) => {
  const lines = csv.split('\n');

  const result = [];

  const headers = lines[0].split(separator);

  for (let i = 1; i < lines.length; i++) {
    const obj = {};
    const currentline = lines[i].split(separator);

    for (let j = 0; j < headers.length; j++) {
      obj[headers[j]] = currentline[j];
    }

    result.push(obj);
  }

  return { headers, result };
};

export const filterObjectPropertiesByKeyRegexes = (
  raw: Object,
  rexes: Array<RegExp>
): Object | any => {
  return Object.keys(raw)
    .filter((key) => rexes.some((rex) => regexMatches(rex, key)))
    .reduce((obj, key) => {
      obj[key] = raw[key];
      return obj;
    }, {});
};

export const destringifySemiNumericalCategoryName = (nc) => {
  if (regexMatches(numericalCategoryPrefacedWithLettersStandaloneRegex, nc)) {
    nc = nc.replace(/\D/g, '');
  }
  return nc;
};

export const columnNameMatchers = {
  AggregatedSI_histogram_chart: [
    strBeginsWithCurrencyLikeRegex,
    otherBucketNameRegex,
  ],
  AggregatedPremium_histogram_chart: [
    strBeginsWithCurrencyLikeRegex,
    otherBucketNameRegex,
  ],

  AggregatedTotalPremiumPerIndustry_histogram_chart: [
    globalStatsNames,
    strBeginsNumericallyRegex,
  ], // starts with category num, i.e. `02 Insurance...`
  AggregatedTotalSumInsuredPerIndustry_histogram_chart: [
    globalStatsNames,
    strBeginsNumericallyRegex,
  ],
  AggregatedPremiumsVsTotalSumInsuredPerIndustry_histogram_chart: [
    globalStatsNames,
    strBeginsNumericallyRegex,
  ],
  AggregatedLOBCountPerIndustry_histogram_chart: [
    globalStatsNames,
    strBeginsNumericallyRegex,
  ],
  AggregatedLocationsCountPerIndustry_histogram_chart: [
    globalStatsNames,
    strBeginsNumericallyRegex,
  ],

  PPM_GlobalStatsPerBusinessActivity_NoCanvas_histogram_chart: [
    globalStatsNames,
    strEndsNumericallyRegex,
  ],
  AggregatedTotalSumInsuredPerBusinessActivity_histogram_chart: [
    globalStatsNames,
    strEndsNumericallyRegex,
  ], // ends with number, i.e. `...risk 4`,
  AggregatedTotalOffersPerBusinessActivity_histogram_chart: [
    globalStatsNames,
    strEndsNumericallyRegex,
  ],
  AggregatedTotalPremiumPerBusinessActivity_histogram_chart: [
    globalStatsNames,
    strEndsNumericallyRegex,
  ],
  AggregatedLocationsPerBusinessActivity_histogram_chart: [
    globalStatsNames,
    strEndsNumericallyRegex,
  ],
  AggregatedPremiumsVsTotalSumInsuredPerBusinessActivity_histogram_chart: [
    globalStatsNames,
    strEndsNumericallyRegex,
  ],

  PPM_GlobalStatsPerNaceCode_NoCanvas_histogram_chart: [
    globalStatsNames,
    numericalCategoryPrefacedWithLettersStandaloneRegex,
  ],
  AggregatedTotalPremiumPerNaceCodes_histogram_chart: [
    globalStatsNames,
    numericalCategoryPrefacedWithLettersStandaloneRegex,
  ],
  AggregatedTotalOffersPerNaceCodes_histogram_chart: [
    globalStatsNames,
    numericalCategoryPrefacedWithLettersStandaloneRegex,
  ],
  AggregatedTotalSumInsuredPerNaceCodes_histogram_chart: [
    globalStatsNames,
    numericalCategoryPrefacedWithLettersStandaloneRegex,
  ],
  AggregatedLocationsPerNaceCodes_histogram_chart: [
    globalStatsNames,
    numericalCategoryPrefacedWithLettersStandaloneRegex,
  ],

  GrossRiskProfile_colored_histogram_chart: [
    globalStatsNames,
    wildcardRegex,
    otherBucketNameRegex,
  ],

  AggregatedTurnoverVsPremium_histogram_chart: [
    globalStatsNames,
    strBeginsWithCurrencyLikeRegex,
    otherBucketNameRegex,
  ],
  AggregatedTurnoverVsTSI_histogram_chart: [
    globalStatsNames,
    strBeginsWithCurrencyLikeRegex,
    otherBucketNameRegex,
  ],
  AggregatedTurnoverVsCount_histogram_chart: [
    globalStatsNames,
    strBeginsWithCurrencyLikeRegex,
    otherBucketNameRegex,
    strBeginsLikePriceBucket,
  ],

  AggregatedTotalPremiumPerRegionOfOrigin_histogram_chart: [
    globalStatsNames,
    wildcardRegex,
    otherBucketNameRegex,
  ],
  AggregatedLOBCountPerRegionOfOrigin_histogram_chart: [
    globalStatsNames,
    wildcardRegex,
    otherBucketNameRegex,
  ],
  AggregatedTotalSumInsuredPerRegionOfOrigin_histogram_chart: [
    globalStatsNames,
    wildcardRegex,
    otherBucketNameRegex,
  ],
  AggregatedLocationsCountPerRegionOfOrigin_histogram_chart: [
    globalStatsNames,
    wildcardRegex,
    otherBucketNameRegex,
  ],

  AggregatedTotalPremiumPerSalesPartner_histogram_chart: [
    globalStatsNames,
    wildcardRegex,
    otherBucketNameRegex,
  ],
  AggregatedLOBCountPerSalesPartner_histogram_chart: [
    globalStatsNames,
    wildcardRegex,
    otherBucketNameRegex,
  ],

  AggregatedTotalPremiumPerAvgRiskClassPerPeril_histogram_chart: [
    globalStatsNames,
    riskClassAbbreviaton,
  ],
  AggregatedTotalSumInsuredPerAvgRiskClassPerPeril_histogram_chart: [
    globalStatsNames,
    riskClassAbbreviaton,
  ],
  AggregatedLOBCountPerAvgRiskClassPerPeril_histogram_chart: [
    globalStatsNames,
    riskClassAbbreviaton,
  ],
  AggregatedGroupedPremiumPerAvgRiskClassPerPeril_LoBBased_histogram_chart: [
    globalStatsNames,
    riskClassAbbreviaton,
  ],
  AggregatedGroupedPremiumPerAvgRiskClassPerPeril_location_based_histogram_chart:
    [globalStatsNames, riskClassAbbreviaton],

  AggregatedAvgPDAPerAvgRiskClassPerPeril_histogram_chart: [
    globalStatsNames,
    riskClassAbbreviaton,
  ],
  AggregatedAvgPDAPerTSIPerPeril_histogram_chart: [
    globalStatsNames,
    wildcardRegex,
    otherBucketNameRegex,
    strBeginsWithCurrencyLikeRegex,
    /^0\s{1}$/,
  ],

  AggregatedBrokerCommission_histogram_chart: [globalStatsNames, wildcardRegex],

  AggregatedTotalMPremiumPerPeril_histogram_chart: [
    globalStatsNames,
    wildcardRegex,
  ],
  AggregatedTotalTPremiumPerPeril_histogram_chart: [
    globalStatsNames,
    wildcardRegex,
  ],

  AggregatedRiskScorePerAvgRiskClassPerPeril_histogram_chart: [
    globalStatsNames,
    riskClassAbbreviaton,
  ],
  AggregatedRiskScoreVsPeril_histogram_chart: [
    globalStatsNames,
    strBeginsWithHistogramBucketingRegex,
  ],

  AggregatedInfoLevelVsRiskClassVsPeril_histogram_chart: [
    globalStatsNames,
    new RegExp(SUM_SIGMA_UTF),
  ],

  AggregatedSuperscoresPerLocationsPerIndustry_histogram_chart: [
    globalStatsNames,
  ],
  AggregatedSuperscoresPerRequestFromCountry_histogram_chart: [
    globalStatsNames,
  ],
  AggregatedSuperscoresPerBucketedTSI_histogram_chart: [globalStatsNames],

  AggregatedTSIPerInsuredObject_histogram_chart: [
    globalStatsNames,
    new RegExp(SUM_SIGMA_UTF),
    /count/gi,
    wildcardRegex,
  ],

  // Construction Materials
  AggregatedTSIPerConstructionMaterials_location_based_histogram_chart: [
    globalStatsNames,
    wildcardRegex,
  ],

  //
  // RISK MANAGEMENT BEGIN
  //
  AggregatedAddressesVsIndustryCategory_histogram_chart: [
    globalStatsNames,
    /.../g,
  ],

  AggregatedAddressesPerIL_histogram_chart: [/.../g],
  AggregatedAddressesPerScore_histogram_chart: [/.../g],
  AggregatedAddressesPerPML_histogram_chart: [/.../g],
  AggregatedAddressesPerRC_histogram_chart: [/.../g],

  AggregatedAddressesPerRC_vsPML_address_based_histogram_chart: [/.../g],
  AggregatedAddressesPerRC_vsIL_address_based_histogram_chart: [/.../g],
  AggregatedAddressesPerRC_vsScore_address_based_histogram_chart: [/.../g],
  AggregatedAddressesPerRC_vsRecommendations_address_based_histogram_chart: [
    /.../g,
  ],

  AggregatedAddressesPerIC_vsRecommendations_address_based_histogram_chart: [
    /.../g,
  ],
  AggregatedAddressesPerIC_vsPML_address_based_histogram_chart: [/.../g],
  AggregatedAddressesPerIC_vsIL_address_based_histogram_chart: [/.../g],
  AggregatedAddressesPerIC_vsScore_address_based_histogram_chart: [/.../g],

  AggregatedAddressesPerScorePerPML_address_based_histogram_chart: [/.../g],
  AggregatedAddressesPerAsIfScorePerPML_address_based_histogram_chart: [/.../g],

  AggregatedAddressesPerAsIfScorePerPML_EML_Ratio_address_based_histogram_chart:
    [/.../g],

  AggregatedAddressesPerRiskScore_vsPremiums_address_based_histogram_chart: [
    /.../g,
  ],
  AggregatedAddressesPerRiskScore_vsPremiums_IndividualLocations_address_based_histogram_chart:
    [/.../g],

  AggregatedAddressesPerRecommendationKinds_vsPriority_address_based_histogram_chart:
    [/.../g],
  AggregatedAddressesPerRecommendationKinds_vsRecommendations_address_based_histogram_chart:
    [/.../g],
  AggregatedAddressesPerRecommendationKinds_vsLocationCounts_address_based_histogram_chart:
    [/.../g],
  AggregatedAddressesPerRecommendationKinds_vsNegativeCondition_address_based_histogram_chart:
    [/.../g],

  AggregatedAddressesPerEvaluations_vsProbabilityCriterias_address_based_histogram_chart:
    [/.../g],
  AggregatedAddressesPerEvaluations_vsPotentialCriterias_address_based_histogram_chart:
    [/.../g],
  AggregatedAddressesPerEvaluations_vsPreventionCriterias_address_based_histogram_chart:
    [/.../g],
  // RISK ENGINEERING END
  // RE INSURANCE BEGIN
  RIM_Property_AggregatedStatsByPML_address_based_histogram_chart: [/.../g],
  RIM_Property_AggregatedStatsByPMLminusTimeRange_AbsoluteValues_address_based_histogram_chart:
    [/.../g],
  RIM_Property_AggregatedStatsByPMLminusTimeRange_PercentualValues_address_based_histogram_chart:
    [/.../g],
  RIM_Property_AggregatedStatsByTSI_address_based_histogram_chart: [/.../g],
  RIM_Property_AggregatedStatsByTSIminusTimeRange_AbsoluteValues_address_based_histogram_chart:
    [/.../g],
  RIM_Property_AggregatedStatsByTSIminusTimeRange_PercentualValues_address_based_histogram_chart:
    [/.../g],
};

export const CHART_FUNCT = {
  identity: (el) => {
    return el;
    // if (regexMatches(numericalRegex, `${el}`)) {
    //   return parseFloat(`${el}`);
    // }
    // return el;
  },
  pprint_el: (el) => pprintCurrency(el),
  ellipsis: (el, len?) =>
    el && el.slice && el.length >= 15 ? `${el.slice(0, len || 15)}...` : el,
  readable_large_currency: (el, apply_currency?) =>
    (apply_currency ? CURRENCY_PREFIX_UTF() : '') + pprintCurrency(el),
  multiplyBy100: (el) => el * 1e2,
  extractFormattedNumberBucket: (el) =>
    regexMatches(/global_stats/, el)
      ? el
      : nFormatter(parseInt(el.split('less_')[1], 10), 1),

  extractFormattedNumberBucketWithCurrency: (el) =>
    regexMatches(/global_stats/, el)
      ? `${CURRENCY_PREFIX_UTF()} ${el}`
      : `${CURRENCY_PREFIX_UTF()} ${nFormatter(
        parseInt(el.split('less_')[1], 10),
        1
      )}`,

  toPrettyPercentX100: (el) => `${roundUp((el || 0) * 1e2, 4)} %`,
  prettyPercent: (el) => `${roundUp((el || 0) / 1e2, 4)} %`,

  // prettyPercent: el => prettyPercent(roundUp((el || 0) / 1e2) / 100, 1),
  onlyPrettyPercent: (el) =>
    isNaN(parseFloat(el)) ? 'n/a' : prettyPercent(parseFloat(el) / 100, 1),

  sixDigitRoundUp: (el) =>
    isNaN(parseFloat(el)) ? 'n/a' : roundUp(parseFloat(el), 6),

  nullIsZero: (el) => (el === null ? 0 : el),

  twoDigitRoundUp: (el) =>
    isNaN(parseFloat(el)) ? '' : roundUp(parseFloat(el), 2),
};

const CHART_FUNCT_IDENTITY_NULL_IS_ZERO = {
  header: CHART_FUNCT.identity,
  body: CHART_FUNCT.nullIsZero,
  chart_label_header: CHART_FUNCT.identity,
  chart_label_body: CHART_FUNCT.nullIsZero,
};

export const tableCellPostProcessingFunctions: ITablePostProcessingFunctions = {
  AggregatedTotalPremiumPerIndustry_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.pprint_el,
    chart_label_header: CHART_FUNCT.ellipsis,
    chart_label_body: CHART_FUNCT.readable_large_currency,
  },
  AggregatedTotalSumInsuredPerIndustry_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.pprint_el,
    chart_label_header: CHART_FUNCT.ellipsis,
    chart_label_body: CHART_FUNCT.readable_large_currency,
  },
  AggregatedPremiumsVsTotalSumInsuredPerIndustry_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.onlyPrettyPercent,
    chart_label_header: CHART_FUNCT.ellipsis,
    chart_label_body: CHART_FUNCT.onlyPrettyPercent,
  },
  // Biz Activity
  AggregatedTotalSumInsuredPerBusinessActivity_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.pprint_el,
    chart_label_header: CHART_FUNCT.ellipsis,
    chart_label_body: CHART_FUNCT.readable_large_currency,
  },
  AggregatedTotalOffersPerBusinessActivity_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.identity,
    chart_label_header: CHART_FUNCT.ellipsis,
    chart_label_body: CHART_FUNCT.identity,
  },
  AggregatedTotalPremiumPerBusinessActivity_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.pprint_el,
    chart_label_header: CHART_FUNCT.ellipsis,
    chart_label_body: CHART_FUNCT.readable_large_currency,
  },
  AggregatedLocationsPerBusinessActivity_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.identity,
    chart_label_header: CHART_FUNCT.ellipsis,
    chart_label_body: CHART_FUNCT.identity,
  },
  AggregatedPremiumsVsTotalSumInsuredPerBusinessActivity_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.onlyPrettyPercent,
    chart_label_header: CHART_FUNCT.ellipsis,
    chart_label_body: CHART_FUNCT.onlyPrettyPercent,
  },
  // Nace Codes
  AggregatedTotalPremiumPerNaceCodes_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.pprint_el,
    chart_label_header: (el) =>
      CHART_FUNCT.ellipsis(
        `${el} ${NaceCodes[destringifySemiNumericalCategoryName(el)]}`,
        30
      ),
    chart_label_body: CHART_FUNCT.readable_large_currency,
  },
  AggregatedTotalOffersPerNaceCodes_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.identity,
    chart_label_header: (el) =>
      CHART_FUNCT.ellipsis(
        `${el} ${NaceCodes[destringifySemiNumericalCategoryName(el)]}`,
        30
      ),
    chart_label_body: CHART_FUNCT.identity,
  },
  AggregatedTotalSumInsuredPerNaceCodes_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.pprint_el,
    chart_label_header: (el) =>
      CHART_FUNCT.ellipsis(
        `${el} ${NaceCodes[destringifySemiNumericalCategoryName(el)]}`,
        30
      ),
    chart_label_body: CHART_FUNCT.readable_large_currency,
  },
  AggregatedLocationsPerNaceCodes_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.identity,
    chart_label_header: (el) =>
      CHART_FUNCT.ellipsis(
        `${el} ${NaceCodes[destringifySemiNumericalCategoryName(el)]}`,
        30
      ),
    chart_label_body: CHART_FUNCT.identity,
  },

  // Gross Risk Profile
  GrossRiskProfile_colored_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.identity,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.identity,
  },

  // Region of Origin
  AggregatedTotalPremiumPerRegionOfOrigin_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.pprint_el,
    chart_label_header: CHART_FUNCT.ellipsis,
    chart_label_body: CHART_FUNCT.readable_large_currency,
  },
  AggregatedLOBCountPerRegionOfOrigin_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.identity,
    chart_label_header: CHART_FUNCT.ellipsis,
    chart_label_body: CHART_FUNCT.identity,
  },
  AggregatedTotalSumInsuredPerRegionOfOrigin_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.pprint_el,
    chart_label_header: CHART_FUNCT.ellipsis,
    chart_label_body: CHART_FUNCT.readable_large_currency,
  },
  AggregatedLocationsCountPerRegionOfOrigin_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.identity,
    chart_label_header: CHART_FUNCT.ellipsis,
    chart_label_body: CHART_FUNCT.identity,
  },
  // Sales Partners
  AggregatedTotalPremiumPerSalesPartner_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.pprint_el,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.readable_large_currency,
  },
  AggregatedLOBCountPerSalesPartner_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.identity,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.identity,
  },
  AggregatedTotalSumInsuredPerSalesPartner_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.pprint_el,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.readable_large_currency,
  },
  AggregatedLocationsCountPerSalesPartner_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.identity,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.identity,
  },
  // AVG Risk Class Perils
  AggregatedTotalPremiumPerAvgRiskClassPerPeril_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.pprint_el,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.readable_large_currency,
  },
  AggregatedTotalSumInsuredPerAvgRiskClassPerPeril_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.pprint_el,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.readable_large_currency,
  },
  AggregatedLOBCountPerAvgRiskClassPerPeril_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.identity,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.identity,
  },
  AggregatedGroupedPremiumPerAvgRiskClassPerPeril_LoBBased_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.identity,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.identity,
  },
  AggregatedGroupedPremiumPerAvgRiskClassPerPeril_location_based_histogram_chart:
  {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.identity,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.identity,
  },

  // Avg PDAs
  AggregatedAvgPDAPerAvgRiskClassPerPeril_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.toPrettyPercentX100,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.onlyPrettyPercent,
  },
  AggregatedAvgPDAPerTSIPerPeril_histogram_chart: {
    header: CHART_FUNCT.extractFormattedNumberBucketWithCurrency,
    body: CHART_FUNCT.toPrettyPercentX100,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.onlyPrettyPercent,
  },

  // Broker Commission
  AggregatedBrokerCommission_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.identity,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.identity,
  },

  // Per Peril
  AggregatedTotalMPremiumPerPeril_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.pprint_el,
    chart_label_header: CHART_FUNCT.ellipsis,
    chart_label_body: CHART_FUNCT.readable_large_currency,
  },
  AggregatedTotalTPremiumPerPeril_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.pprint_el,
    chart_label_header: CHART_FUNCT.ellipsis,
    chart_label_body: CHART_FUNCT.readable_large_currency,
  },

  AggregatedRiskScorePerAvgRiskClassPerPeril_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.sixDigitRoundUp,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.sixDigitRoundUp,
  },
  AggregatedRiskScoreVsPeril_histogram_chart: {
    header: CHART_FUNCT.identity,
    body: CHART_FUNCT.identity,
    chart_label_header: CHART_FUNCT.identity,
    chart_label_body: CHART_FUNCT.identity,
  },
  AggregatedInfoLevelVsRiskClassVsPeril_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },

  AggregatedSuperscoresPerLocationsPerIndustry_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },
  AggregatedSuperscoresPerRequestFromCountry_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },
  AggregatedSuperscoresPerBucketedTSI_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },

  AggregatedTSIPerInsuredObject_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },

  AggregatedTSIPerConstructionMaterials_location_based_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },

  //
  // RISK ENGINEERING BEGIN
  //
  AggregatedAddressesVsIndustryCategory_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },

  AggregatedAddressesPerIL_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },
  AggregatedAddressesPerScore_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },
  AggregatedAddressesPerPML_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },
  AggregatedAddressesPerRC_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },

  AggregatedAddressesPerRC_vsPML_address_based_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },
  AggregatedAddressesPerRC_vsIL_address_based_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },
  AggregatedAddressesPerRC_vsScore_address_based_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },
  AggregatedAddressesPerRC_vsRecommendations_address_based_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },

  AggregatedAddressesPerIC_vsRecommendations_address_based_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },
  AggregatedAddressesPerIC_vsPML_address_based_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },
  AggregatedAddressesPerIC_vsIL_address_based_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },
  AggregatedAddressesPerIC_vsScore_address_based_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },

  AggregatedAddressesPerScorePerPML_address_based_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },
  AggregatedAddressesPerAsIfScorePerPML_address_based_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },

  AggregatedAddressesPerAsIfScorePerPML_EML_Ratio_address_based_histogram_chart:
  {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },

  AggregatedAddressesPerRiskScore_vsPremiums_address_based_histogram_chart: {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },
  AggregatedAddressesPerRiskScore_vsPremiums_IndividualLocations_address_based_histogram_chart:
  {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },

  AggregatedAddressesPerRecommendationKinds_vsPriority_address_based_histogram_chart:
  {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },

  AggregatedAddressesPerRecommendationKinds_vsRecommendations_address_based_histogram_chart:
  {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },

  AggregatedAddressesPerEvaluations_vsProbabilityCriterias_address_based_histogram_chart:
  {
    ...CHART_FUNCT_IDENTITY_NULL_IS_ZERO,
  },
};

/**
 * Delegator to the CSV service.
 * @param header
 * @param arrayOfRowMaps
 * @param rawCellValues
 * @returns
 */
export const rowMapToCSV = (
  header: Array<string>,
  arrayOfRowMaps: Array<ITableReadyContentRow>,
  rawCellValues = false
) => {
  return new CsvService().rowMapsToCSV(header, arrayOfRowMaps, rawCellValues);
};

export const categorizedBucketsToSortedList = (
  buckets: object,
  column_name_prefix: string = '',
  query_id: string = '',
  last_key?
): Map<string, any> => {
  const categorizedKeysToValuesMap: Map<string, any> = new Map();
  let bucketKeys = Object.keys(buckets);

  /*
    keep 'other' buckets at the end of the map by extracting them and appending at the end
  */
  let others = null;
  if ('_other_' in buckets) {
    others = buckets['_other_'];
    delete buckets['_other_'];
    bucketKeys = Object.keys(buckets);
  }

  if (regexMatches(strEndsNumericallyRegex, bucketKeys[0])) {
    if (regexMatches(riskClassAbbreviaton, bucketKeys[0])) {
      bucketKeys = sortArrayOfRiskClasses(bucketKeys);
    } else {
      bucketKeys = sortArrayOfRisks(bucketKeys);
    }

    // bucketKeys = bucketKeys.sort(function(a, b) {
    //   if ('sum' in buckets[a]) {
    //     return buckets[b].sum.sum.value - buckets[a].sum.sum.value;
    //   }
    // });
  } else if (
    regexMatches(
      numericalCategoryPrefacedWithLettersStandaloneRegex,
      bucketKeys[0]
    )
  ) {
    bucketKeys = sortArrOfStringsAlphabetically(bucketKeys);
  } else if (regexMatches(pdaInQuery, query_id)) {
    bucketKeys = sortByExtractingNumbersFromStrings(bucketKeys);
  } else {
    bucketKeys = sortArrOfStringsAlphabetically(bucketKeys);
  }

  bucketKeys.map((bkey) => {
    let formatted_key = bkey;
    let val;

    if (last_key) {
      // !regexMatches(numericalCategoryPrefacedWithLettersStandaloneRegex, bkey)
      if (last_key === 'count') {
        if (
          query_id.toLowerCase().includes('persalespartner') ||
          query_id.includes('LOBCountPerAvgRiskClass')
        ) {
          val =
            (buckets[bkey].stats_operation &&
              (buckets[bkey].stats_operation.stats.count ||
                buckets[bkey].stats_operation.stats.value)) ||
            0;
        } else {
          if (
            query_id.toLowerCase().includes('locations') &&
            buckets[bkey].stats_operation &&
            typeof buckets[bkey].stats_operation.stats !== undefined
          ) {
            val = buckets[bkey].stats_operation.stats[last_key];
          } else {
            val = buckets[bkey].doc_count;
          }
        }
      } else {
        if (buckets[bkey].stats && buckets[bkey].stats.stats) {
          val = buckets[bkey].stats.stats[last_key];
        } else {
          val = 0;
        }
      }
    } else {
      try {
        val = findByKeyInNestedObject(buckets[bkey], 'value').value;
      } catch (err) {
        val = 0;
      }
    }

    if (query_id && query_id in tableCellPostProcessingFunctions) {
      const { header: header_fn, body: body_fn } =
        tableCellPostProcessingFunctions[query_id];
      formatted_key = header_fn(formatted_key);
      val = body_fn(val);
    }
    categorizedKeysToValuesMap.set(
      `${column_name_prefix}${formatted_key}`,
      val
    );
  });

  if (others) {
    categorizedKeysToValuesMap.set(OTHER_BUCKET_NAME, others.doc_count);
  }

  return categorizedKeysToValuesMap;
};

const getColumnIndex = (column_name: string, header: Array<string>) => {
  return header.indexOf(column_name);
};

const histogramBucketsToSortedList = (
  buckets: object,
  column_name_prefix: string | Function = '',
  column_name_suffix: string | Function = '',
  stats_operation = 'sum'
): Map<string, any> => {
  const numericalKeysToValuesMap: Map<string, any> = new Map();
  let bucketKeys = Object.keys(buckets);

  /*
    keep 'other' buckets at the end of the map by extracting them and appending at the end
  */
  let others = null;
  if ('_other_' in buckets) {
    others = buckets['_other_'];
    delete buckets['_other_'];
    bucketKeys = Object.keys(buckets);
  }

  if (regexMatches(strEndsNumericallyRegex, bucketKeys[0])) {
    bucketKeys = sortArrayOfRisks(bucketKeys);
  } else {
    bucketKeys = sortByExtractingNumbersFromStrings(bucketKeys);
  }

  bucketKeys.map((bkey) => {
    const formatted_key = nFormatter(parseInt(bkey.split('less_')[1], 10), 1);

    let prefix;
    if (typeof column_name_prefix === 'function') {
      prefix = column_name_prefix(formatted_key);
    } else {
      prefix = column_name_prefix;
    }

    if (column_name_suffix) {
      let suffix;
      if (typeof column_name_suffix === 'function') {
        suffix = column_name_suffix(formatted_key);
      } else {
        suffix = column_name_suffix;
      }

      numericalKeysToValuesMap.set(
        `${prefix}${formatted_key}${suffix}`,
        buckets[bkey][stats_operation]
      );
    } else {
      numericalKeysToValuesMap.set(
        `${prefix}${formatted_key}`,
        buckets[bkey].doc_count
      );
      numericalKeysToValuesMap.set(
        `${prefix}${formatted_key} ${SUM_SIGMA_UTF}${column_name_suffix}`,
        buckets[bkey][stats_operation]
      );
    }
  });

  if (others) {
    numericalKeysToValuesMap.set(OTHER_BUCKET_NAME, others.doc_count);
  }
  return numericalKeysToValuesMap;
};

const extractHistoStatsBucketsAndMerge = (
  whole_subagg,
  spread_buckets,
  operation_path?
) => {
  const filtered = {};
  Object.keys(whole_subagg).map((key) => {
    if (histoNumStatsBucketName.matcher(key)) {
      const __key__ = histoNumStatsBucketName.extractor(key);
      filtered[__key__] = {};

      filtered[__key__]['sum'] = CHART_FUNCT.readable_large_currency(
        findByKeyInNestedObject(whole_subagg[key], 'sum').sum
      );
      if (operation_path) {
        filtered[__key__]['count'] = _.get(
          whole_subagg[key],
          operation_path,
          0
        );
      } else {
        filtered[__key__]['count'] = spread_buckets[__key__].doc_count;
      }
    }
  });

  Object.keys(spread_buckets).map((k) => {
    if (filtered[k]) {
      spread_buckets[k]['sum'] = filtered[k]['sum'];
      spread_buckets[k]['count'] = filtered[k]['count'];
    }
  });
};

const ELEMENTARY_AGG_PARSER = (opts: RemAggResponseParsersOpts): Map<string, any> => {
  const { inner_aggs, rowMap, row_name_extraction } = opts;
  const is_count = row_name_extraction.includes('number');

  sortArrOfStringsAlphabetically(Object.keys(inner_aggs)).map((okey) => {
    let { [row_name_extraction]: stats } = inner_aggs[okey];

    if (stats) {
      if (stats.top && stats.top.hits.hits.length >= 0) {
        stats = (stats.top.hits.hits as Array<any>)
          .map((hit) => hit._source.id)
          .makeDistinct().length;
        console.info(`taking ${okey} distinct count of ${stats}`);
      } else {
        stats = stats.inside;
        if (is_count) {
          stats = stats.count || stats.value;
        } else {
          stats = stats.sum;
        }
      }

      const formatting_fn = is_count
        ? CHART_FUNCT.identity
        : CHART_FUNCT.readable_large_currency;
      rowMap.set(`${okey}`, formatting_fn(stats));
    }
  });

  rowMap.delete('Sum');
  rowMap.delete('Avg');

  return rowMap;
};

const ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS = (
  opts: RemAggResponseParsersOpts
): Map<string, any> => {
  const { query_id, inner_aggs, rowMap, row_name_extraction } = opts;

  let is_count =
    row_name_extraction.includes('number') ||
    !row_name_extraction.includes('accumulated');

  sortArrOfStringsAlphabetically(Object.keys(inner_aggs)).map((okey) => {
    let stats;

    if (
      query_id ===
      'AggregatedAddressesPerRiskScore_vsPremiums_address_based_histogram_chart' ||
      query_id ===
      'AggregatedAddressesPerRiskScore_vsPremiums_IndividualLocations_address_based_histogram_chart'
    ) {
      stats = inner_aggs[okey];
      okey = okey.match(/(.prem)/)[0];
    } else {
      ({ [row_name_extraction]: stats } = inner_aggs[okey]);
    }

    if (stats) {
      if (stats.top && stats.top.hits.hits.length >= 0) {
        stats = (stats.top.hits.hits as Array<any>)
          .map((hit) => hit._source.id)
          .makeDistinct().length;
        console.info(`taking ${okey} distinct count of ${stats}`);
      } else if (
        (query_id.includes('_vsRecommendations_') &&
          !(window as any).selected_key_factor_kind)
        // UDGB-52 include priority table too
        || (query_id.includes('PerRecommendationKinds_vsPriority'))
      ) {
        // extract prevention, potential & probability individually & sum them up
        // to forgo the need to have a pre-selected `Assessment Key Factor`
        // #3 in Sirucek's requirements:
        // https://docs.google.com/document/d/1qG6-ZqrgwymgbdDPew-UaUceQKflnU3XXzzitM_6Fus/edit

        let go_up_path_summarizing_aggs = {};
        try {
          go_up_path_summarizing_aggs = stats.go_up_path || stats[row_name_extraction]?.go_up_path || stats[row_name_extraction];
          if (typeof go_up_path_summarizing_aggs === 'undefined') {
            throw new TypeError('Cannot pass undefined onwards');
          }
        } catch (err) {
          console.debug({ err });
          try {
            go_up_path_summarizing_aggs = {};
            const candidate_key = Object.keys(stats).filter((k) =>
              regexMatches(
                new RegExp(safeStringForRegex(row_name_extraction)),
                k
              )
            )[0];
            console.debug({ candidate_key, row_name_extraction });
            go_up_path_summarizing_aggs = stats[candidate_key].go_up_path || {};
          } catch (err2) {
            console.error({ err2 });
          }
        }

        stats = Object.keys(go_up_path_summarizing_aggs)
          // one of the keys will be doc_count. Get rid of it
          .filter((key) => typeof go_up_path_summarizing_aggs[key] !== 'number')
          .map((key) => {
            let to_return;
            try {
              // there may not be any 'inside' object so test first
              to_return = findByKeyInNestedObject(
                go_up_path_summarizing_aggs[key],
                'inside'
              ).inside;

              if (typeof to_return.value === 'undefined' && to_return.hits || (
                // jump straight to inner hits calculation to circumvent
                // the `keyCriteriaAsList` array flattening issue which causes
                // the recommendations count to not represent reality -- https://prnt.sc/HQ4QxTymupsi
                // Fixes UDGB-52
                query_id.includes('PerRC_vsRecommendations')
                || query_id.includes('PerRecommendationKinds_vsPriority')
              )) {
                throw new Error('proceed to next catch');
              }

              return to_return.value;
            } catch (err) {
              console.debug({ err });

              // `inside` may not be a numeric stat but rather
              // a `top_hits` agg such as in the case of
              // `AggregatedAddressesPerRecommendationKinds_vsRecommendations_address_based_histogram_chart` and others
              try {
                let inside_top_hits = to_return;

                // Fixes UDGB-52 by adjusted where we should look for inner hits
                if (go_up_path_summarizing_aggs[key]?.inner_hits) {
                  inside_top_hits = go_up_path_summarizing_aggs[key]?.inner_hits;
                } else if (go_up_path_summarizing_aggs[key]?.hits) {
                  inside_top_hits = { hits: go_up_path_summarizing_aggs[key] };
                }

                const meta = inside_top_hits.meta;
                inside_top_hits = (inside_top_hits as SearchResponse<any>).hits
                  .hits as any[];

                if (inside_top_hits) {
                  switch (query_id) {
                    // Fixes UDGB-52
                    case 'AggregatedAddressesPerRC_vsRecommendations_address_based_histogram_chart': {
                      const recommendations = flattenArrays(
                        inside_top_hits.map(
                          // the hits will contain _source on the top level
                          (hit) => hit._source
                        )
                      ) as AssessmentRecommendation[];
                      const cleaned_status_feedback = `${okey.replace(
                        COLUMN_NAME_CLEANSER_RE,
                        ''
                      )}`;

                      return recommendations.filter((rec) => {
                        return (
                          rec.statusFeedback === cleaned_status_feedback
                        );
                      }).length;
                    }
                    case 'AggregatedAddressesPerRecommendationKinds_vsPriority_address_based_histogram_chart': {
                      const applicableRecomms: AssessmentRecommendation[] = [];
                      const applicablePriority = `${okey.replace(
                        COLUMN_NAME_CLEANSER_RE,
                        ''
                      )}`;

                      const addressHits = inside_top_hits.hits.map(hit => hit._source) as Pick<Address, 'locationProperty'>[];

                      addressHits.map((h) => {
                        const level3 = h.locationProperty.locationAssessmentsLevel3;

                        for (const peril in level3) {
                          const factors = level3[peril]['keyFactors'];

                          for (const f in factors) {
                            const criteria = factors[f]['keyCriteria'];

                            for (const c in criteria) {
                              const { recommendations: recomms } = criteria[c];

                              if (recomms?.length) {
                                for (const r of recomms) {

                                  const recommendation_name = row_name_extraction.split(' | ').pop();
                                  const { priority, name } = r as AssessmentRecommendation;

                                  if (priority === applicablePriority && name === recommendation_name) {
                                    applicableRecomms.push(r);
                                  }
                                }
                              }
                            }
                          }
                        }
                      });

                      return applicableRecomms.length;
                    }
                    case 'AggregatedAddressesPerRecommendationKinds_vsRecommendations_address_based_histogram_chart': {
                      const recommendations = flattenArrays(
                        inside_top_hits.map(
                          (hit) => hit._source.recommendations
                        )
                      ) as AssessmentRecommendation[];
                      const cleaned_status_feedback = `${okey.replace(
                        COLUMN_NAME_CLEANSER_RE,
                        ''
                      )}`;

                      return recommendations.filter((rec) => {
                        return (
                          rec.name ===
                          meta.assessment_recommendation &&
                          rec.statusFeedback === cleaned_status_feedback
                        );
                      }).length;
                    }
                  }
                } else {
                  return 0;
                }
              } catch (err2) {
                console.debug({ err2 });
                return 0;
              }
            }
          })
          .sumByReduce();
      } else {
        if (stats.inside) {
          stats = stats.inside;
        } else {
          stats = findByKeyInNestedObject(stats, 'inside').inside;
        }

        if (
          okey.includes('average') ||
          row_name_extraction.includes('average')
        ) {
          stats = roundUp(stats.avg || 0, 4);
        } else {
          // check for `undefined` instead of rudimentary falsy checks like `!stats.count`
          // because we might be dealing with zeroes which are indeed valid values!
          if (typeof stats.count === 'undefined' && typeof stats.sum === 'undefined') {
            is_count = !okey.includes('prem');
            stats = stats.value || 0;
          } else {
            if (is_count) {
              stats = stats.count;
            } else {
              stats = stats.sum;
            }
          }
        }
      }

      // UDGB-26 construct percentages
      if (query_id.includes('PerRC_vsIL')) {
        const applicableCount = CHART_FUNCT['twoDigitRoundUp'](stats);

        const rowTotal = inner_aggs?.['total_stats']?.doc_count
          // add fallback for the first (summary) row as per UDGB-40
          || rowMap.get('number_of_locations');

        if (rowTotal > 0 && applicableCount >= 0) {
          // UDGB-32 the percentages in the parentheses must relate to the applicable total
          stats = `${stats} (${prettyPercent(applicableCount, rowTotal, 0)})`;
        }
      }

      const formatting_fn = is_count
        ? CHART_FUNCT.identity
        : CHART_FUNCT.readable_large_currency;
      rowMap.set(`${okey}`, formatting_fn(stats));
    }
  });

  const total_stats = (<any>inner_aggs).total_stats;
  if (total_stats) {
    rowMap.set(opts.total_keyword, total_stats.totals.inside.count);
  }

  const avg_stats = (<any>inner_aggs).avg_stats;
  if (avg_stats) {
    rowMap.set(
      opts.avg_keyword,
      CHART_FUNCT['twoDigitRoundUp'](avg_stats.totals.inside.value)
    );
  }

  // UDGB-27 Sum up non-new recommendations and divide by count of new recommendations
  if (query_id.includes('PerRC_vsRecommendations')) {
    // Adjusted spec defined in UDGB-35
    const sumWithoutDeclinedAndNew = (() => {
      let sum = 0;
      for (const [colName, value] of rowMap.entries()) {
        // https://regex101.com/r/ijcJeZ/1
        if (/[A-Z]\) \d\) /.test(colName)
          && !colName.includes('declined')
          // UDGB-41 exclude 'new' recomms as well
          && !colName.includes('new')
        ) {
          sum += AGG_VALUE_FORMATTERS.IDENTITY_NAN_IS_ZERO(value);
        }
      }
      return sum;
    })();
    const sumIncludingDeclined = (() => {
      let sum = 0;
      for (const [colName, value] of rowMap.entries()) {
        // https://regex101.com/r/ijcJeZ/1
        if (/[A-Z]\) \d\) /.test(colName)) {
          sum += AGG_VALUE_FORMATTERS.IDENTITY_NAN_IS_ZERO(value);
        }
      }
      return sum;
    })();

    const ratio = prettyPercent(sumWithoutDeclinedAndNew, sumIncludingDeclined, false);
    rowMap.set('Ratio of non-declined vs all', ratio);
  }

  if (
    (query_id ===
      'AggregatedAddressesPerRiskScore_vsPremiums_address_based_histogram_chart' ||
      query_id ===
      'AggregatedAddressesPerRiskScore_vsPremiums_IndividualLocations_address_based_histogram_chart') &&
    rowMap.has('tprem') &&
    rowMap.has('mprem')
  ) {
    rowMap.set(
      'deviation',
      premiumDeviation(rowMap.get('mprem'), rowMap.get('tprem'), 4)
    );
  }

  rowMap.delete('Sum');
  rowMap.delete('Avg');

  return rowMap;
};

interface RemAggResponseParsersOpts {
  inner_aggs: object;
  full_aggs?: object;
  rowMap: Map<string, any>;
  row_name_extraction: string;
  query_id?: string;
  total_keyword?: string;
  avg_keyword?: string;
}

const REM_AGG_RESPONSE_PARSERS: {
  [string: string]: (opts: RemAggResponseParsersOpts) => Map<string, any>;
} = {
  AggregatedAddressesVsIndustryCategory_histogram_chart: ELEMENTARY_AGG_PARSER,

  AggregatedAddressesPerIL_histogram_chart: ELEMENTARY_AGG_PARSER,
  AggregatedAddressesPerScore_histogram_chart: ELEMENTARY_AGG_PARSER,
  AggregatedAddressesPerPML_histogram_chart: ELEMENTARY_AGG_PARSER,
  AggregatedAddressesPerRC_histogram_chart: ELEMENTARY_AGG_PARSER,

  AggregatedAddressesPerRC_vsPML_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,
  AggregatedAddressesPerRC_vsIL_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,
  AggregatedAddressesPerRC_vsScore_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,
  AggregatedAddressesPerRC_vsRecommendations_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,

  AggregatedAddressesPerIC_vsRecommendations_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,
  AggregatedAddressesPerIC_vsPML_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,
  AggregatedAddressesPerIC_vsIL_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,
  AggregatedAddressesPerIC_vsScore_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,

  AggregatedAddressesPerScorePerPML_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,
  AggregatedAddressesPerAsIfScorePerPML_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,

  AggregatedAddressesPerAsIfScorePerPML_EML_Ratio_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,

  AggregatedAddressesPerRiskScore_vsPremiums_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,
  AggregatedAddressesPerRiskScore_vsPremiums_IndividualLocations_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,

  AggregatedAddressesPerRecommendationKinds_vsPriority_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,
  AggregatedAddressesPerRecommendationKinds_vsRecommendations_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,
  AggregatedAddressesPerRecommendationKinds_vsLocationCounts_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,
  AggregatedAddressesPerRecommendationKinds_vsNegativeCondition_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,

  AggregatedAddressesPerEvaluations_vsProbabilityCriterias_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,
  AggregatedAddressesPerEvaluations_vsPotentialCriterias_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,
  AggregatedAddressesPerEvaluations_vsPreventionCriterias_address_based_histogram_chart:
    ELEMENTARY_AGG_PARSER_WITH_INSIDE_AGGS,
};

export const REM_AGGS_ONLY_KEYS = Object.keys(REM_AGG_RESPONSE_PARSERS);

export class TableRowTransformer {
  query_id: string;
  resp: SearchResponse<any>;
  selected_values: Array<string>;

  constructor(
    query_id: string,
    resp: SearchResponse<any> | IAggregationQueryBuilderResponse,
    selected_values?: Array<string>
  ) {
    this.query_id = query_id;
    this.resp = resp;
    this.selected_values = selected_values;

    if (this.selected_values && this.selected_values.length) {
      this.resp.aggregations = filterObjectPropertiesByKeyRegexes(
        this.resp.aggregations,
        this.selected_values.map((val) => new RegExp(val))
      );
    }
  }

  private get shouldFilterOutNullColumnSums() {
    return (
      !this.query_id.toLowerCase().includes('perperil') &&
      !this.query_id.toLowerCase().includes('vsperil') &&
      !this.query_id.toLowerCase().includes('minustimerange')
    );
  }

  appendCumulativeColumnSumsToRows(
    header: Array<any>,
    rows: Array<{ [id: string]: Map<string, any> }>,
    pieChartDataset: Array<IPieChartDataset>,
    sortByTotal?,
    opts?: {
      total_keyword: string;
      postProcessingFunctionsPerColumns?: {
        [column_header_name: string]: Function;
      };
    }
  ): [Array<any>, Array<any>] {
    const columnVsCount = [];
    const newHeader: Array<any> = []
      .concat(header[0])
      .concat(
        header.filter((h) =>
          [new RegExp(/^total$/i), globalStatsNames].some((rx) =>
            regexMatches(rx, h)
          )
        )
      );

    const origHeaderGeneric = newHeader.slice();
    const newRows: Array<{ [id: string]: Map<string, any> }> = [];

    const cumulativeRowMap: Map<string, any> = new Map();
    let transform_fn;

    pieChartDataset.map((data_group) => {
      ({ chart_label_body: transform_fn } = getTranformFns(this.query_id));

      const relevant_starting_index = getColumnIndex(
        data_group.category,
        header
      );
      if (!cumulativeRowMap.size) {
        header.slice(1, relevant_starting_index).map((column_name) => {
          cumulativeRowMap.set(column_name, '');
        });
      }
      columnVsCount.push({
        key: header[relevant_starting_index],
        count: data_group.count,
      });
      if (
        (relevant_starting_index !== -1 &&
          header[relevant_starting_index].includes(SUM_SIGMA_UTF)) ||
        isTurnoverLike(this.query_id)
      ) {
        transform_fn = (n) => CHART_FUNCT.readable_large_currency(n);
      }

      if (
        opts.postProcessingFunctionsPerColumns &&
        header[relevant_starting_index] in
        opts.postProcessingFunctionsPerColumns
      ) {
        transform_fn =
          opts.postProcessingFunctionsPerColumns[
          header[relevant_starting_index]
          ];
      }

      cumulativeRowMap.set(
        header[relevant_starting_index],
        transform_fn(data_group.count)
      );
    });

    const top25values = sortArrayByKey(columnVsCount, 'count', true)
      // .filter(item => !['Total', 'Min', 'Max', 'Avg'].includes(item.key))
      .filter((item) =>
        this.shouldFilterOutNullColumnSums ? true : item.count > 0
      )
      .slice(0, 30);

    // calculate sum of the total column (normally at index 1)
    let tmp_sum = 0;

    const runThroughTotalSorter = (
      origMap,
      okey: string
    ): { [id: string]: Map<string, any> } => {
      const mapDict_: { [id: string]: Map<string, any> } = {};
      ({ chart_label_body: transform_fn } = getTranformFns(this.query_id));

      mapDict_[okey] = <Map<string, any>>new Map();
      mapDict_[okey].set(opts.total_keyword, origMap.get(opts.total_keyword));

      top25values.map((entry: { key: string; count: number }) => {
        const column = entry.key;
        if (
          newHeader.length !== origHeaderGeneric.length + top25values.length &&
          !newHeader.includes(column)
        ) {
          newHeader.push(column);
        }
        if (column === opts.total_keyword) {
          transform_fn = CHART_FUNCT.identity;
        }
        mapDict_[okey].set(column, transform_fn(origMap.get(column)));
      });

      return mapDict_;
    };

    rows.map((row) => {
      const _map = <Map<string, any>>Object.values(row)[0];
      const total = extractNumberFromString(_map.get(opts.total_keyword));

      tmp_sum += total;

      if (sortByTotal) {
        newRows.push(runThroughTotalSorter(_map, Object.keys(row)[0]));
      }
    });

    ({ chart_label_body: transform_fn } = getTranformFns(this.query_id));
    // cumulativeRowMap.set('Total', transform_fn(tmp_sum));
    cumulativeRowMap.set(opts.total_keyword, tmp_sum);

    if (sortByTotal) {
      newRows.push(runThroughTotalSorter(cumulativeRowMap, `${SUM_SIGMA_UTF}`));
    }

    if (
      this.query_id !== 'AggregatedBrokerCommission_histogram_chart' &&
      !regexMatches(/NoCanvas/, this.query_id) &&
      !regexMatches(/PremiumsVsTotalSumInsured/, this.query_id)
    ) {
      rows.push({ [`${SUM_SIGMA_UTF}`]: cumulativeRowMap });
    }

    if (sortByTotal) {
      return [newHeader, newRows];
    }

    return [header, rows];
  }

  sumUpColumnsOfMapRows(
    row_map_arr: Array<Map<string, any>>,
    allowed_column_name_matchers?: Array<RegExp>
  ): Array<IPieChartDataset> {
    let summed_up_buckets: Array<IPieChartDataset> = [];
    let all_keys = [];
    let tmp_dict = null;

    row_map_arr.map((_row) => {
      const row = <Map<string, any>>extractFirstValueFromDict(_row);

      // initialze summation dict
      if (!tmp_dict) {
        all_keys = Array.from(row.keys()).filter((key) => {
          if (
            !(
              allowed_column_name_matchers &&
              allowed_column_name_matchers.length
            )
          ) {
            return true;
          }

          return allowed_column_name_matchers.some((rx) =>
            regexMatches(rx, key)
          );
        });
        tmp_dict = {};
        all_keys.map((key) => {
          tmp_dict[key] = [];
        });
      }

      row.forEach((v, k) => {
        if (
          allowed_column_name_matchers &&
          allowed_column_name_matchers.length &&
          !allowed_column_name_matchers.some((rx) => regexMatches(rx, k))
        ) {
        } else {
          if (k in tmp_dict) {
            tmp_dict[k].push(extractNumberFromString(v));
          }
        }
      });
    });

    for (const key of all_keys) {
      summed_up_buckets.push({
        category: key,
        count: tmp_dict[key].length === 0 ? 0 : tmp_dict[key].reduce(add, 0), // sum up extracted array content
      });
    }

    if (this.shouldFilterOutNullColumnSums) {
      summed_up_buckets = summed_up_buckets
        .filter((pair) => pair.count > 0)
        .sort(dynamicSort('-count'));
    }

    return summed_up_buckets;
  }

  applyRiskClassPostCalculations(
    rows: Array<any>,
    global_stats_operator: string,
    disallowed_matchers: Array<RegExp>
  ): Array<Map<string, any>> {
    const namedPerilsRowMap: Map<string, any> = new Map();
    const allRiskRowMap: Map<string, any> = new Map();

    const totalSum = extractNumberFromString(
      rows[rows.length - 1][SUM_SIGMA_UTF].get(
        capitalize(global_stats_operator)
      )
    );

    namedPerilsRowMap.set('Total', '');
    namedPerilsRowMap.set(capitalize(global_stats_operator), '');

    allRiskRowMap.set('Total', '');
    allRiskRowMap.set(capitalize(global_stats_operator), '');

    rows
      .filter((r) => Object.keys(r)[0] === SUM_SIGMA_UTF)
      .map((r) => {
        const rowMap: Map<string, any> = extractFirstValueFromDict(r);
        let sum_of_relative_risk_classes = 0;

        rowMap.forEach((map_count_value, map_key) => {
          if (disallowed_matchers.some((rx) => regexMatches(rx, map_key))) {
            // skip disallowed column names
          } else {
            const column_sum = extractNumberFromString(map_count_value);
            const pretty_percent = prettyPercent(column_sum, totalSum, false);

            const relative_risk_class =
              (extractNumberFromString(pretty_percent) / 1e2) *
              extractNumberFromString(map_key);

            namedPerilsRowMap.set(map_key, pretty_percent);
            allRiskRowMap.set(map_key, roundUp(relative_risk_class, 4));

            sum_of_relative_risk_classes += relative_risk_class;
          }
        });

        allRiskRowMap.set(
          capitalize(global_stats_operator),
          roundUp(sum_of_relative_risk_classes, 2)
        );
      });

    return [
      ...rows,
      {
        'Named Perils': namedPerilsRowMap,
      },
      {
        'All Risk': allRiskRowMap,
      },
    ];
  }

  transformNumericalHistogramResponseIntoTableRows(
    chart_name,
    column_name_suffix?: string | Function,
    operation = 'sum',
    column_name_prefix: string | Function = CURRENCY_PREFIX_UTF,
    operation_path?: string
  ): ITableContentAndPieChartData {
    const aggs = this.resp.aggregations;
    let rows = [];
    let header = [];

    sortArrOfStringsAlphabetically(Object.keys(aggs)).map((agg_key) => {
      const inner_aggs = aggs[agg_key];
      const spread_parent_reversal =
        inner_aggs.spread_parent.spread_parent_reversal ||
        inner_aggs.spread_parent;
      const spread_buckets =
        spread_parent_reversal.spread.buckets ||
        spread_parent_reversal.spread.at_doc_level.buckets;

      extractHistoStatsBucketsAndMerge(
        spread_parent_reversal,
        spread_buckets,
        operation_path
      );

      const applicable_count =
        typeof spread_parent_reversal.spread.doc_count !== 'undefined'
          ? spread_parent_reversal.spread.doc_count
          : inner_aggs.spread_parent.doc_count; // inner_aggs.generic_stats.numerical_stats.doc_count || inner_aggs.generic_stats.doc_count; // spread_parent_reversal.doc_count;

      const rowMap: Map<string, any> = new Map();

      const true_key = agg_key.split('histograms_')[1];
      if (
        this.selected_values.length &&
        !this.selected_values.includes(true_key)
      ) {
        // skip
      } else {
        const num_stats = inner_aggs.generic_stats.numerical_stats;
        // assignment without declaration https://stackoverflow.com/a/32138566/8160318

        let min__, max__, avg__;
        if (num_stats.aggs) {
          ({
            min: min__,
            max: max__,
            avg: avg__,
          } = inner_aggs.generic_stats.numerical_stats.aggs);
        } else {
          ({
            min: min__,
            max: max__,
            avg: avg__,
          } = inner_aggs.generic_stats.numerical_stats);
        }

        const { chart_label_body, nonexistent } = getTranformFns(this.query_id);
        let formatter;
        if (nonexistent) {
          formatter = pprintCurrency;
        } else {
          formatter = chart_label_body;
        }

        rowMap.set('Total', applicable_count);
        rowMap.set('Average', formatter(avg__));
        rowMap.set('Min', formatter(min__));
        rowMap.set('Max', formatter(max__));

        concatMaps(
          rowMap,
          histogramBucketsToSortedList(
            spread_buckets,
            column_name_prefix,
            column_name_suffix,
            operation
          )
        );

        if (!header.length) {
          header = [].concat(chart_name).concat(Array.from(rowMap.keys()));
        }

        rows.push({
          [true_key]: rowMap,
        });
      }
    });

    const pieChartDataset = this.sumUpColumnsOfMapRows(
      rows,
      columnNameMatchers[this.query_id]
    );

    [header, rows] = this.appendCumulativeColumnSumsToRows(
      header,
      rows,
      pieChartDataset,
      null,
      {
        total_keyword: 'Total',
      }
    );

    const to_return = {
      tableContent: {
        header: header,
        rows: rows,
      },
      piechartContent: pieChartDataset,
    };

    return to_return;
  }

  transformCategorizedSpreadResponseIntoTableRows(
    chart_name,
    global_stats_operator = '',
    last_key?,
    opts?: {
      total_keyword: string;
    }
  ): ITableContentAndPieChartData {
    let TOTAL_KEYWORD = 'Total';
    const AVG_KEYWORD = 'Average';

    if (this.query_id.includes('address_based')) {
      TOTAL_KEYWORD = 'Total Addresses';
    }

    if (opts && opts.total_keyword) {
      TOTAL_KEYWORD = opts.total_keyword;
    }

    let disallowed_matchers = [/^sum|count|total|min|max$/gi];
    const body_fn = getTranformFns(this.query_id).chart_label_body;

    const aggs = this.resp.aggregations;
    let rows = [];
    let header = [];

    sortArrOfStringsAlphabetically(Object.keys(aggs)).map((agg_key) => {
      const inner_aggs = aggs[agg_key];
      let spread_buckets = {};
      let applicable_count = 0;
      let global_stats = null;
      const orig_global_stats = Object.assign({}, inner_aggs.global_stats);

      if ('global_stats' in inner_aggs && global_stats_operator) {
        let multiple_operators = [];
        if (global_stats_operator.includes('|')) {
          multiple_operators = global_stats_operator.split('|');
        }

        if (!multiple_operators.length) {
          global_stats = findByKeyInNestedObject(
            inner_aggs.global_stats.stats_parent,
            global_stats_operator
          )[global_stats_operator];
        } else {
          for (let _i = 0; _i < multiple_operators.length; _i++) {
            try {
              global_stats = findByKeyInNestedObject(
                inner_aggs.global_stats.stats_parent,
                multiple_operators[_i]
              )[multiple_operators[_i]];
              global_stats_operator = multiple_operators[_i];
            } catch (ex) { }
          }
        }
        if (global_stats && this.query_id in tableCellPostProcessingFunctions) {
          const { chart_label_body: body_fn_ } = getTranformFns(this.query_id);
          global_stats = body_fn_(global_stats);
        }
        delete inner_aggs['global_stats'];
      }

      if (
        inner_aggs.spread_parent &&
        inner_aggs.spread_parent.spread_parent_reversal
      ) {
        spread_buckets =
          inner_aggs.spread_parent.spread_parent_reversal.spread.at_doc_level
            .buckets;
        applicable_count =
          inner_aggs.spread_parent.spread_parent_reversal.doc_count;
      } else {
        spread_buckets = inner_aggs;
        applicable_count = inner_aggs.doc_count;
      }

      const rowMap: Map<string, any> = new Map();

      let true_key = agg_key.split('histograms_')[1];
      if (
        this.selected_values.length &&
        !this.selected_values.includes(
          destringifySemiNumericalCategoryName(true_key)
        )
      ) {
        // skip
      } else {
        // get rid of doc_count because spread buckets should all be dictionaries
        delete spread_buckets['doc_count'];

        // rowMap.set('Total', global_stats ? global_stats : applicable_count);
        rowMap.set(
          TOTAL_KEYWORD,
          extractNumberFromString(
            applicable_count ? applicable_count : global_stats
          )
        );

        if (
          !regexMatches(includeMinMaxInRowMaps, this.query_id) &&
          orig_global_stats &&
          orig_global_stats.stats_parent
        ) {
          const { chart_label_body: body_fn__ } = getTranformFns(this.query_id);
          let {
            min: min_,
            max: max_,
            avg: avg_,
          } = orig_global_stats.stats_parent;

          if (
            body_fn__ !== CHART_FUNCT.identity &&
            body_fn__ !== CHART_FUNCT.sixDigitRoundUp
          ) {
            // probably coming in as percentages so convert
            min_ = min_ * 100;
            max_ = max_ * 100;
            avg_ = avg_ * 100;
          }

          rowMap.set('Min', body_fn__(min_));
          rowMap.set('Max', body_fn__(max_));

          if (
            this.query_id ===
            'AggregatedRiskScorePerAvgRiskClassPerPeril_histogram_chart'
          ) {
            rowMap.set('Math. Avg', body_fn__(avg_));
          } else {
            rowMap.set('Avg', body_fn__(avg_));
          }
        }

        if (
          global_stats_operator &&
          !(global_stats_operator === 'avg' && rowMap.has('Avg'))
        ) {
          rowMap.set(capitalize(global_stats_operator), global_stats);
        }

        if (regexMatches(brokerCommissionInQuery, this.query_id)) {
          const only_commission = inner_aggs.commission_stats.stats_parent;
          const commissioned_mprem_sum =
            inner_aggs.commissioned_mprem_stats.stats_parent.value;
          const only_mprem = inner_aggs.mprem_stats.stats_parent;

          const {
            min: commission_min,
            max: commission_max,
            avg: commission_avg,
            count: commission_count,
          } = only_commission;

          // const sum_of_commissioned_mprems = commissioned_mprem.sum;
          const sum_of_commissioned_mprems = commissioned_mprem_sum;
          const sum_of_mprems = only_mprem.sum;

          const avg_commission_of_mprems = prettyPercent(
            sum_of_commissioned_mprems || 0,
            sum_of_mprems || 1,
            false
          );

          rowMap.set('LoBs w/ existing commission', commission_count);
          rowMap.set('Weighted Average Commission', avg_commission_of_mprems);
          rowMap.set('Min Commission', prettyPercent(commission_min || 0, 100));
          rowMap.set('Max Commission', prettyPercent(commission_max || 0, 100));
          rowMap.set(
            'Average Commission',
            prettyPercent(commission_avg || 0, 100)
          );

          rowMap.delete('Avg');

          disallowed_matchers = [/count|avg/gi];
        } else if (regexMatches(superScoreInQuery, this.query_id)) {
          // don't need the avg
          rowMap.delete(capitalize(global_stats_operator));

          let sorted_ = [];
          if (this.query_id.includes('TSI')) {
            sorted_ = sortByExtractingNumbersFromStrings(
              Object.keys(spread_buckets)
            );
          } else {
            sorted_ = sortArrOfStringsAlphabetically(
              Object.keys(spread_buckets)
            );
          }

          sorted_.map((spread_bucket_key) => {
            const spread_bucket = spread_buckets[spread_bucket_key];

            if (spread_bucket_key.includes('less_')) {
              spread_bucket_key = `${CURRENCY_PREFIX_UTF()}${nFormatter(
                extractNumberFromString(spread_bucket_key),
                1
              )} ${MIDDLE_DOT_UTF}`;
            }

            const roundingFn = (el) =>
              typeof el === 'number' ? roundUp(el * 1e2, 4) : el;

            const supersocre_x_tsi_sum = CHART_FUNCT.nullIsZero(
              spread_bucket.superscore_x_tsi_stats.stats_parent.sum
            );
            const tsi_sum = CHART_FUNCT.nullIsZero(
              spread_bucket.tsi_stats.stats_parent.sum
            );

            let weighted_superscore;
            if (tsi_sum === 0) {
              weighted_superscore = ' - ';
            } else {
              weighted_superscore = supersocre_x_tsi_sum / tsi_sum;
            }

            rowMap.set(
              `${spread_bucket_key} weighted Superscore`,
              roundingFn(weighted_superscore)
            );
            rowMap.set(
              `${spread_bucket_key} ${SUM_SIGMA_UTF}TSI`,
              CHART_FUNCT.readable_large_currency(tsi_sum)
            );
          });
        } else if (regexMatches(infoLevelInQuery, this.query_id)) {
          sortArrOfStringsAlphabetically(Object.keys(inner_aggs))
            .filter((okey) => okey !== 'meta')
            .map((okey) => {
              const { unique_stats } = inner_aggs[okey];
              const tsi_sum = unique_stats.stats_parent.sum;
              const unique_address_count = unique_stats.stats_parent.count;

              if (okey === 'alpha_stats') {
                rowMap.set(
                  `All Locations ${MIDDLE_DOT_UTF} Count`,
                  unique_address_count
                );
                rowMap.set(
                  `All Locations ${MIDDLE_DOT_UTF} ${SUM_SIGMA_UTF}SI`,
                  CHART_FUNCT.readable_large_currency(tsi_sum)
                );
              } else {
                // const unique_address_count = sumByMapReduce(uniques.uniques_by_address_id.uniques_by_address_id_reversed.buckets.map(buck => {
                //   return { doc_count: buck.distinct_tokens.value };
                // }));

                rowMap.set(
                  `${okey} ${MIDDLE_DOT_UTF} Count`,
                  unique_address_count
                );
                rowMap.set(
                  `${okey} ${MIDDLE_DOT_UTF} ${SUM_SIGMA_UTF}SI`,
                  CHART_FUNCT.readable_large_currency(tsi_sum)
                );
              }
            });

          rowMap.delete('Avg');

          disallowed_matchers = [/count|avg/gi];
        } else if (regexMatches(insuredObjectsInQuery, this.query_id)) {
          sortArrOfStringsAlphabetically(Object.keys(inner_aggs))
            .filter((okey) => okey !== 'meta')
            .map((okey) => {
              const { unique_stats } = inner_aggs[okey];
              const tsi_sum = unique_stats.stats_parent.sum;
              const unique_address_count = unique_stats.stats_parent.count;

              rowMap.set(
                `${okey} ${MIDDLE_DOT_UTF} Count`,
                unique_address_count
              );
              rowMap.set(
                `${okey} ${MIDDLE_DOT_UTF} ${SUM_SIGMA_UTF}SI`,
                CHART_FUNCT.readable_large_currency(tsi_sum)
              );
            });

          rowMap.delete('Sum');
          rowMap.delete('Avg');

          disallowed_matchers = [/count|avg/gi];
        } else if (this.query_id in REM_AGG_RESPONSE_PARSERS) {
          REM_AGG_RESPONSE_PARSERS[this.query_id]({
            inner_aggs,
            full_aggs: aggs,
            rowMap,
            row_name_extraction: true_key,
            query_id: this.query_id,
            total_keyword: TOTAL_KEYWORD,
            avg_keyword: this.query_id.toLowerCase().includes('score')
              ? 'Avg Score'
              : AVG_KEYWORD,
          });
          true_key = true_key
            .split('_')
            .filter((colname) => {
              return !colname.match(COLUMN_NAME_CLEANSER_RE); // extract helpers like `'1)' number of locations`
            })
            .map((part) => {
              return isNaN(extractInt(part)) ||
                this.query_id.includes('RecommendationKinds')
                ? part
                : part.includes(')')
                  ? extractInt(part)
                  : part;
            }) // un-pad padded risk classes etc looking like 001, 002 ...
            .join(' ');
        } else {
          if (Object.keys(spread_buckets).length) {
            concatMaps(
              rowMap,
              categorizedBucketsToSortedList(
                spread_buckets,
                '',
                this.query_id,
                last_key
              )
            );
          }
        }

        if (Object.keys(spread_buckets).length) {
          if (!header.length) {
            header = [].concat(chart_name).concat(Array.from(rowMap.keys()));
          }

          rows.push({
            [true_key]: rowMap,
          });

          // horizontally sum only applicable counts/sums across the whole Map
          // should be <= Total for this particular row
          let count_sum = 0;

          rowMap.forEach((map_count_value, map_key) => {
            if (disallowed_matchers.some((rx) => regexMatches(rx, map_key))) {
              // skip disallowed column names
            } else {
              count_sum += extractNumberFromString(map_count_value);
            }
          });

          if (
            global_stats_operator &&
            regexMatches(includeMinMaxInRowMaps, this.query_id) &&
            !this.query_id.includes('LOBCount')
          ) {
            rowMap.set(capitalize(global_stats_operator), body_fn(count_sum));
          }
        }
      }
    });

    const pieChartDataset = this.sumUpColumnsOfMapRows(
      rows,
      columnNameMatchers[this.query_id]
    );

    [header, rows] = this.appendCumulativeColumnSumsToRows(
      header,
      rows,
      pieChartDataset,
      !regexMatches(onlyNumericalRiskClass, this.query_id) &&
      !regexMatches(pdaInQuery, this.query_id) &&
      !regexMatches(brokerCommissionInQuery, this.query_id) &&
      !regexMatches(riskScoreInQuery, this.query_id) &&
      !regexMatches(superScoreInQuery, this.query_id) &&
      !regexMatches(insuredObjectsInQuery, this.query_id) &&
      !regexMatches(/PremiumsVsTotalSumInsured/, this.query_id) &&
      !regexMatches(/NoCanvas/, this.query_id) &&
      !(this.query_id in REM_AGG_RESPONSE_PARSERS),
      {
        total_keyword: TOTAL_KEYWORD,
      }
    );

    if (
      regexMatches(onlyNumericalRiskClass, this.query_id) &&
      !regexMatches(pdaInQuery, this.query_id) &&
      !regexMatches(brokerCommissionInQuery, this.query_id) &&
      !regexMatches(riskScoreInQuery, this.query_id) &&
      !regexMatches(superScoreInQuery, this.query_id) &&
      !regexMatches(infoLevelInQuery, this.query_id) &&
      !regexMatches(insuredObjectsInQuery, this.query_id) &&
      !regexMatches(/NoCanvas/, this.query_id) &&
      !(this.query_id in REM_AGG_RESPONSE_PARSERS)
    ) {
      // rows = this.applyRiskClassPostCalculations(rows, global_stats_operator, disallowed_matchers);
    }

    const to_return = {
      tableContent: {
        header: header,
        rows: rows,
      },
      piechartContent: pieChartDataset,
    };

    return to_return;
  }

  transformQueryBuilderMetaAnnotatedResponseIntoTableRows(): ITableContentAndPieChartData {
    const aggs = (<IAggregationQueryBuilderResponse>this.resp).aggregations;
    let rows: { [key: string]: Map<string, any> }[] & any[] = [];
    let header: string[] = [];
    const postProcessingFunctionsPerColumns: {
      [column_header_name: string]: Function;
    } = {};

    Object.values(aggs)
      .sort(dynamicSort('meta.row_index'))
      .map((row: typeof aggs[keyof typeof aggs]) => {
        const rowMap: Map<string, any> = new Map();
        const { row_name, row_index } = row.meta;

        if (row_index === 0) {
          header.push(row.meta.category_column_name);
        }

        delete row['meta'];

        Object.values(row)
          .filter((potential_column) => isObject(potential_column))
          .sort(dynamicSort('meta.column.index'))
          .map((column_group) => {
            const { available_aggs, column, post_processing } =
              column_group.meta;

            const aggreg_name = column.presented_agg_name;
            const { accessor: path_accessor, formatter: formatter_fn_name } =
              post_processing[aggreg_name].value;

            const raw_column_value = objectPath.get(
              column_group,
              path_accessor
            );
            const post_processing_fn = AGG_VALUE_FORMATTERS[formatter_fn_name];
            const column_value = post_processing_fn(raw_column_value);

            rowMap.set(column.name, column_value);

            if (row_index === 0) {
              header.push(column.name);
              postProcessingFunctionsPerColumns[column.name] =
                post_processing_fn;
            }
          });

        rows.push({
          [row_name]: rowMap,
        });
      });

    const pieChartDataset = this.sumUpColumnsOfMapRows(
      rows,
      columnNameMatchers[this.query_id]
    );

    [header, rows] = this.appendCumulativeColumnSumsToRows(
      header,
      rows,
      pieChartDataset,
      false,
      {
        total_keyword: 'total',
        postProcessingFunctionsPerColumns: postProcessingFunctionsPerColumns,
      }
    );

    return {
      tableContent: {
        header: header,
        rows: rows,
      },
      piechartContent: pieChartDataset,
    };
  }

  toPieChartJSArguments(
    piecharData: Array<IPieChartDataset>,
    total_count: number,
    limit_by_selected_values?: Array<string>,
    labePostProcessingFns?: ITableCellPostProcessingGroup
  ) {
    const max_arr = [];
    const legendMap = {};
    const labels = [];
    let datasets = [
      {
        label: ' ',
        data: [],
        stack: 1,
      },
    ];

    const sum_already_in_piechart = piecharData.filter(
      (pd) => pd.category === 'Sum' || pd.category === 'Count'
    );
    if (sum_already_in_piechart.length) {
      total_count = extractNumberFromString(sum_already_in_piechart[0].count);
    }

    const use_barchart = chartIdToChartType(this.query_id) === 'bar';

    const allowed_length = use_barchart
      ? CATEGORIZED_BARCHART_DATASET_MAX_LENGTH
      : CATEGORIZED_PIECHART_DATASET_MAX_LENGTH;

    // limit piechart to 10
    (use_barchart
      ? piecharData
      : sortArrayByKey(piecharData, 'count', true)
    ).map((item, ind) => {
      if (
        (limit_by_selected_values.length &&
          !limit_by_selected_values.includes(item.category)) ||
        datasets[0].data.length >= allowed_length ||
        item.category.includes(SUM_SIGMA_UTF) ||
        regexMatches(/total|min|max|avg/gi, item.category) ||
        regexMatches(globalStatsNames, item.category)
      ) {
        // pass
      } else {
        const count = extractNumberFromString(item.count);
        total_count = extractNumberFromString(total_count);

        datasets[0].data.push(count);
        // const letter = nthLetterOfAlphabet(ind).toUpperCase();
        const letter = item.category;
        labels.push(letter);

        legendMap[letter] = {
          key: `${item.category}`,
          count: `${count}`,
          percentage: `${percentageOfBuckets(count, total_count)}`,
        };

        if (count !== 0) {
          max_arr.push(count);
        }
      }
    });

    const tickCallback = function (value, ___, __) {
      return `${value}`;
    };

    const labelCallback = function (tooltipItem, data) {
      const label_pointer = data.labels[tooltipItem.index];
      const accessor = Object.keys(legendMap).filter((okey) => {
        return okey.startsWith(label_pointer.replace(/\.{3}/g, ''));
      })[0];

      const { count, key, percentage } = legendMap[accessor];
      let prettifiedKey = key;
      let prettifiedCount = count;
      if (labePostProcessingFns) {
        const { chart_label_body, chart_label_header } = labePostProcessingFns;
        prettifiedKey = chart_label_header(prettifiedKey);
        prettifiedCount = chart_label_body(prettifiedCount);
      }

      return `${stripHtml({
        count: prettifiedCount,
        key: prettifiedKey,
        percentage: percentage,
      })}`;
    };

    // const dataset_comparator_arr = piecharData.map(point => this.query_id.toLowerCase().includes('vsperil') ? 1 : point.count !== 0);
    // const comparator_arr = piecharData.filter(point => this.query_id.toLowerCase().includes('vsperil') ? true : point.count !== 0).map(point => point.category);
    // labels = labels.filter(lab => comparator_arr.includes(lab));

    datasets = datasets.map((dset) => {
      return {
        data: dset.data, // .filter((__, ind) => dataset_comparator_arr[ind] !== false),
        label: dset.label,
        stack: dset.stack,
      };
    });

    const to_return = {
      labels: labels,
      datasets: datasets,
      legendMap: legendMap,
      onIncomingOptions: {
        annotationTechniques: null,
        tickOptions: {
          callback: tickCallback,
          min: Math.min.apply(null, max_arr),
          max: Math.max.apply(null, max_arr) * 1.2,
        },
        fully_custom_y_axis: null,
        legendOptions:
          chartIdToChartType(this.query_id) === 'bar'
            ? null
            : {
              display: true,
              position: 'left',
              labels: {
                usePointStyle: true,
                generateLabels: (chart) => {
                  // shoutout https://jsfiddle.net/lcustodio/97pLd0um/1/
                  chart.legend.afterFit = function () {
                    this.lineWidths = this.lineWidths
                      ? this.lineWidths.map(() => this.width - 12)
                      : [10];
                    this.options.labels.padding = 15;
                    this.options.labels.boxWidth = 60;
                  };

                  const data = chart.data;
                  // https://github.com/chartjs/Chart.js/blob/1ef9fbf7a65763c13fa4bdf42bf4c68da852b1db/src/controllers/controller.doughnut.js
                  if (data.labels.length && data.datasets.length) {
                    return data.labels.map((label, i) => {
                      const meta = chart.getDatasetMeta(0);
                      const ds = data.datasets[0];
                      const arc = meta.data[i];
                      const custom = (arc && arc.custom) || {};
                      const arcOpts = chart.options.elements.arc;
                      const fill = custom.backgroundColor
                        ? custom.backgroundColor
                        : _getValueAtIndexOrDefault(
                          ds.backgroundColor,
                          i,
                          arcOpts.backgroundColor
                        );
                      const stroke = custom.borderColor
                        ? custom.borderColor
                        : _getValueAtIndexOrDefault(
                          ds.borderColor,
                          i,
                          arcOpts.borderColor
                        );
                      const bw = custom.borderWidth
                        ? custom.borderWidth
                        : _getValueAtIndexOrDefault(
                          ds.borderWidth,
                          i,
                          arcOpts.borderWidth
                        );

                      return {
                        text: label,
                        fillStyle: fill,
                        strokeStyle: stroke,
                        lineWidth: bw,
                        hidden: isNaN(ds.data[i]) || meta.data[i].hidden,

                        // Extra data used for toggling the correct item
                        index: i,
                      };
                    });
                  }
                  return [];
                },
              },
            },
        onAnimationComplete: onAnimationCompleteTechniques.noop,
        stackOptions: {
          stacked: true,
        },
        labelCallback: labelCallback,
      },
    };

    return to_return;
  }

  toSplitLineDatasets(
    rows: Array<ITableReadyContentRow>,
    header: Array<string>,
    labels: Array<string>
  ) {
    const disallowed_matcher = /^((sum)|(count)|(total)|(min)|(max)|(value))$/i;
    const disallowed_indexes = header.slice(1).map((column_name) => {
      return disallowed_matcher.test(column_name) ? NaN : 1;
    });

    const shortened_labels = sortArrOfStringsAlphabetically(
      labels.map((l) => CHART_FUNCT.ellipsis(l))
    );

    let datasets = [];

    datasets = rows
      .filter((_r) => Object.keys(_r)[0] !== SUM_SIGMA_UTF)
      .map((_row, index) => {
        const name = Object.keys(_row)[0];
        const row: Map<string, any> = _row[name];

        return {
          label: name,
          data: new Array(...row.values())
            .filter((__, indx) => !isNaN(disallowed_indexes[indx]))
            .map((val) => extractNumberFromString(`${val}`)),
          stack: 1,
          fill: false,
          borderColor: lobVsColor(name),
          type: 'line',
          yAxisID: 'normal_y_axis',
        };
      });

    const LoBCount_Dataset: Array<{ category: string; count: number }> = [];

    Object.keys(this.resp.aggregations).map((agg_name) => {
      const inner_aggs = this.resp.aggregations[agg_name];
      Object.keys(inner_aggs).map((label_name) => {
        const actual_aggs = inner_aggs[label_name];
        const shortened_label_name = CHART_FUNCT.ellipsis(label_name);
        const count = actual_aggs.sum_operation
          ? actual_aggs.sum_operation.doc_count
          : 0;

        if (typeof count !== 'undefined' && count !== null) {
          const found_index = LoBCount_Dataset.findIndex(
            (obj) => obj.category === shortened_label_name
          );
          if (found_index !== -1) {
            LoBCount_Dataset[found_index].count += count;
          } else {
            LoBCount_Dataset.push({
              category: shortened_label_name,
              count: count,
            });
          }
        }
      });
    });

    datasets.push({
      label: 'Number of LoBs',
      data: LoBCount_Dataset.filter((pair) =>
        shortened_labels.includes(pair.category)
      )
        .sort(function (a, b) {
          return (
            shortened_labels.indexOf(a.category) -
            shortened_labels.indexOf(b.category)
          );
        })
        .map((val) => val.count),
      stack: 1,
      fill: false,
      borderColor: hexToRgba('#000000', 0.5),
      type: 'line',
      yAxisID: 'fully_custom_y_axis',
      showLine: true,
    });

    return [datasets, shortened_labels];
  }
}
