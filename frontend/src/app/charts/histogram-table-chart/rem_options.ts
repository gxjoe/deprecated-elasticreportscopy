import {
  TableRowTransformer,
  rowMapToCSV,
  csvToJSON,
  filterObjectPropertiesByKeyRegexes,
  CHART_FUNCT,
  COLUMN_NAME_CLEANSER_RE,
  SUM_SIGMA_UTF,
  REM_AGGS_ONLY_KEYS,
} from './common';
import {
  sortArrOfStringsAlphabetically,
  extractNumberFromString,
  nFormatter,
  arrayToObject,
  CURRENCY_PREFIX_UTF,
  CURRENCY_PREFIX_ISO,
  highlightJSON,
  isFalsey,
  sortObjectKeysByPropertyValue,
  regexMatches,
  extractFirstValueFromDict,
  premiumDeviation,
  flattenArrays,
} from 'src/app/utils/helpers/helpers';

import {
  onAnimationCompleteTechniques,
  IChartOptions,
} from 'src/app/services/chartjs.service';
import {
  colorSchemeVividDomainAsRgbArray,
  padZero,
  palette_base_human_readable,
  riskScoreBucketColors,
  duplicateArrayItems,
} from '../common/constants';
import { roundUpToNearestN, roundUp } from 'src/app/utils/helpers/numerical';
import { LISTS_OF_HISTOGRAM_CATEGORIES } from './histogramListsAndCategories';

import * as objectPath from 'object-path';
import { generateYearlyTrendlineDatasetsForScatterScoreMatrices } from 'src/app/utils/helpers/chartjs.helper';
import { CsvService } from 'src/app/services/csv.service';
import { mergeCriteriaSpecificAssessmentRecommendations } from 'src/app/elastic/utils';
import {
  AddressesDoc,
  KeyCriterion,
  LocationAssessment,
  PerilName,
} from 'src/app/interfaces/elastic';
import { dynamicSort } from '../utils';
import { IxyHit } from './interfaces';
import { HistogramTableChartComponent } from './histogram-table-chart.component';
import { SearchResponse } from 'elasticsearch';
import { flattenAssessmentsFromAddresses } from 'src/app/utils/section-based/rem/assessment-flattening';
import { KeyFactors } from '../../interfaces/elastic';

import uniqBy from 'lodash.uniqby';
import { getListOfSelectedLocationPerilRiskClasses } from './constants';

export const CUSTOM_Y_AXIS_ID = 'fully_custom_y_axis';

const isCount = (context) => {
  if (!context.dataset) {
    return context === CUSTOM_Y_AXIS_ID;
  }
  return context.dataset.yAxisID === CUSTOM_Y_AXIS_ID;
};

export const getBucketsFromGlobalMappers = (category): Array<string> => {
  const w = window as any;
  if (!w.mappers || !(category in w.mappers)) {
    return [];
  }

  return w.mappers[category];
};

const setOfColorsWithDecreasingIntensity = (
  opacity_levels: number,
  num_of_colors?: number
) => {
  const ratio = opacity_levels / 100;
  const arr_of_colors = [];

  [].rangeOfNumbers(1, 0.2, -ratio).map((opacity) => {
    arr_of_colors.push(colorSchemeVividDomainAsRgbArray(opacity));
  });

  return arr_of_colors;
};

export const secondYaxisDenotingCount = (
  right_max: number,
  right_min?: number,
  right_axis_label?: string,
  tick_callback?: Function
) => {
  right_max *= 1.2;

  return {
    position: 'right',
    id: CUSTOM_Y_AXIS_ID,
    stacked: false,
    gridLines: {
      display: false,
    },
    ticks: {
      suggestedMax: right_max,
      beginAtZero: right_min ? false : true,
      min: right_min || 0,
      max: right_max,
      callback: tick_callback
        ? tick_callback
        : function (value, ___, __) {
          return value;
        },
      stepSize: right_max > 10 ? undefined : 1,
    },
    labelString: right_axis_label,
  };
};

const promoteScatterplotDatasetToFront = (_data: Array<any>) => {
  const scatter = _data[_data.length - 1];
  const d = _data.slice();
  d.pop();

  d.unshift(scatter);

  return d;
};

interface IQueryDependentChartOptions {
  label_prefix: string;
  label_unused_char_extractor: RegExp;
  column_extractor_re: RegExp;
}

const fully_custom_rem_chart_opts: IQueryDependentChartOptions = {
  label_prefix: '',
  label_unused_char_extractor: new RegExp(''),
  column_extractor_re: new RegExp(''),
};

const REM_DIAGRAMS_CUSTOM_OPTS: {
  [id: string]: IQueryDependentChartOptions;
} = {
  AggregatedAddressesVsIndustryCategory_histogram_chart: {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: /^\d+/,
  },
  AggregatedAddressesPerIL_histogram_chart: {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: /infoLevel/,
  },
  AggregatedAddressesPerScore_histogram_chart: {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: /Score/,
  },
  AggregatedAddressesPerPML_histogram_chart: {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: /Uniqa PML/,
  },
  AggregatedAddressesPerRC_histogram_chart: {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: /Risk Class/,
  },

  AggregatedAddressesPerRC_vsIL_address_based_histogram_chart: {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: /infoLevel/,
  },
  AggregatedAddressesPerRC_vsPML_address_based_histogram_chart: {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: /Uniqa PML/,
  },
  AggregatedAddressesPerRC_vsScore_address_based_histogram_chart: {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: /Score/,
  },
  AggregatedAddressesPerRC_vsRecommendations_address_based_histogram_chart: {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: new RegExp(
      LISTS_OF_HISTOGRAM_CATEGORIES.listOfRecommendationStatusFeedbacks.join(
        '|'
      ),
      'g'
    ),
  },

  AggregatedAddressesPerIC_vsRecommendations_address_based_histogram_chart: {
    // label_prefix: 'IndCat ',
    // label_unused_char_extractor: /(?<=^.{3}).*/g,
    label_prefix: ' ',
    label_unused_char_extractor: new RegExp(''),
    column_extractor_re: new RegExp(
      LISTS_OF_HISTOGRAM_CATEGORIES.listOfRecommendationStatusFeedbacks.join(
        '|'
      ),
      'g'
    ),
  },
  AggregatedAddressesPerIC_vsPML_address_based_histogram_chart: {
    // label_prefix: 'IndCat ',
    // label_unused_char_extractor: /(?<=^.{3}).*/g,
    label_prefix: ' ',
    label_unused_char_extractor: new RegExp(''),
    column_extractor_re: /Uniqa PML/,
  },
  AggregatedAddressesPerIC_vsIL_address_based_histogram_chart: {
    // label_prefix: 'IndCat ',
    // label_unused_char_extractor: /(?<=^.{3}).*/g,
    label_prefix: ' ',
    label_unused_char_extractor: new RegExp(''),
    column_extractor_re: /infoLevel/,
  },
  AggregatedAddressesPerIC_vsScore_address_based_histogram_chart: {
    // label_prefix: 'IndCat ',
    // label_unused_char_extractor: /(?<=^.{3}).*/g,
    label_prefix: ' ',
    label_unused_char_extractor: new RegExp(''),
    column_extractor_re: /Score/,
  },

  AggregatedAddressesPerScorePerPML_address_based_histogram_chart: {
    ...fully_custom_rem_chart_opts,
  },
  AggregatedAddressesPerAsIfScorePerPML_address_based_histogram_chart: {
    ...fully_custom_rem_chart_opts,
  },
  AggregatedAddressesPerAsIfScorePerPML_EML_Ratio_address_based_histogram_chart:
  {
    ...fully_custom_rem_chart_opts,
  },

  AggregatedAddressesPerRiskScore_vsPremiums_address_based_histogram_chart: {
    label_prefix: ' ',
    label_unused_char_extractor: new RegExp(''),
    column_extractor_re: /.../gi,
  },
  AggregatedAddressesPerRiskScore_vsPremiums_IndividualLocations_address_based_histogram_chart:
  {
    label_prefix: ' ',
    label_unused_char_extractor: new RegExp(''),
    column_extractor_re: /.../gi,
  },

  AggregatedAddressesPerRecommendationKinds_vsPriority_address_based_histogram_chart:
  {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: new RegExp(''),
  },
  AggregatedAddressesPerRecommendationKinds_vsRecommendations_address_based_histogram_chart:
  {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: new RegExp(''),
  },
  AggregatedAddressesPerRecommendationKinds_vsLocationCounts_address_based_histogram_chart:
  {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: /.../gi,
  },
  AggregatedAddressesPerRecommendationKinds_vsNegativeCondition_address_based_histogram_chart:
  {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: /.../gi,
  },

  AggregatedAddressesPerEvaluations_vsProbabilityCriterias_address_based_histogram_chart:
  {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: /(?!^number[\s|_]of[\s|_].*|metric$)(^.*$)/gi,
  },
  AggregatedAddressesPerEvaluations_vsPotentialCriterias_address_based_histogram_chart:
  {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: /(?!^number[\s|_]of[\s|_].*|metric$)(^.*$)/gi,
  },
  AggregatedAddressesPerEvaluations_vsPreventionCriterias_address_based_histogram_chart:
  {
    label_prefix: '',
    label_unused_char_extractor: COLUMN_NAME_CLEANSER_RE,
    column_extractor_re: /(?!^number[\s|_]of[\s|_].*|metric$)(^.*$)/gi,
  },
};

const REM_DIAGRAM_RENDERERS = (
  self: HistogramTableChartComponent,
  query_id,
  jsonized_csv,
  opts?: IQueryDependentChartOptions,
  originalResponse?: any,
  total_keyword?: string
) => {
  if (!Object.keys(REM_DIAGRAMS_CUSTOM_OPTS).includes(query_id)) {
    return;
  }

  const points_info_level_1: Array<IxyHit> = [];
  const points_info_level_2: Array<IxyHit> = [];
  const points_info_level_3: Array<IxyHit> = [];

  const hoverActions = {
    pointHoverRadius: 5,
    pointHoverBackgroundColor: 'black',
  };

  const data_hash = [];
  const data_hash_lookup = (x: number, y: number) => {
    return data_hash.filter((item) => {
      return item.x === x && item.y === y;
    })[0];
  };

  let ID_MAP;
  let num_of_locations: any;

  let RIGHT_AXIS_MAX: number;
  let max_arr: Array<number>;

  let customColorSets: Array<any>;

  let merged_data: Array<{
    key: string;
    counts: Array<number>;
  }>;

  let datasets = [];
  let labels = [];
  let chart_options: IChartOptions;
  let premiumRows: Array<{
    column_name: string;
    data: any;
  }> = [];
  let mprem_data = [];
  let tprem_data = [];
  let deviation_data = [];

  let level3only: boolean;
  let hideTrendlines: boolean;

  switch (query_id) {
    case 'AggregatedAddressesVsIndustryCategory_histogram_chart':
    case 'AggregatedAddressesPerIL_histogram_chart':
    case 'AggregatedAddressesPerScore_histogram_chart':
    case 'AggregatedAddressesPerPML_histogram_chart':
    case 'AggregatedAddressesPerRC_histogram_chart':
      ID_MAP = {
        [`${total_keyword}`]: total_keyword.split('_').join(' '),
        accumulated_UNIQA_PML: 'accumulated UNIQA PML',
        accumulated_liability_sum_insured: 'accumulated liability sum insured',
        accumulated_premium_calculation_basis:
          'accumulated premium calculation basis',
      };

      num_of_locations = filterObjectPropertiesByKeyRegexes(
        jsonized_csv.result.filter(
          (obj) => obj.Metric === ID_MAP[total_keyword]
        )[0],
        [opts.column_extractor_re]
      );
      const accumulated_UNIQA_PML = filterObjectPropertiesByKeyRegexes(
        jsonized_csv.result.filter(
          (obj) => obj.Metric === ID_MAP.accumulated_UNIQA_PML
        )[0],
        [opts.column_extractor_re]
      );
      const accumulated_liability_sum_insured =
        filterObjectPropertiesByKeyRegexes(
          jsonized_csv.result.filter(
            (obj) => obj.Metric === ID_MAP.accumulated_liability_sum_insured
          )[0],
          [opts.column_extractor_re]
        );
      const accumulated_premium_calculation_basis =
        filterObjectPropertiesByKeyRegexes(
          jsonized_csv.result.filter(
            (obj) => obj.Metric === ID_MAP.accumulated_premium_calculation_basis
          )[0],
          [opts.column_extractor_re]
        );

      RIGHT_AXIS_MAX = Math.max.apply(null, Object.values(num_of_locations));

      merged_data = sortArrOfStringsAlphabetically(
        Object.keys(num_of_locations)
      ).map((okey) => {
        return {
          key: `${opts.label_prefix}${okey.replace(
            opts.label_unused_char_extractor,
            ''
          )}`,
          counts: [
            accumulated_UNIQA_PML[okey],
            accumulated_liability_sum_insured[okey],
            accumulated_premium_calculation_basis[okey],
            num_of_locations[okey],
          ].map((val) => extractNumberFromString(val)),
        };
      });

      const legendMap = {};
      labels = [];
      datasets = [
        {
          label: `${ID_MAP.accumulated_UNIQA_PML}`,
          data: [],
          stack: 1,
          yAxisID: 'normal_y_axis',
        },
        {
          label: `${ID_MAP.accumulated_liability_sum_insured}`,
          data: [],
          stack: 2,
          yAxisID: 'normal_y_axis',
        },
        {
          label: `${ID_MAP.accumulated_premium_calculation_basis}`,
          data: [],
          stack: 3,
          yAxisID: 'normal_y_axis',
        },
        {
          label: `${ID_MAP[total_keyword]}`,
          data: [],
          type: 'scatter',
          yAxisID: CUSTOM_Y_AXIS_ID,
          backgroundColor: '#000000',
          borderColor: '#000000',
          showLine: false,
        },
      ];

      max_arr = [];

      customColorSets =
        query_id === 'AggregatedAddressesPerScore_histogram_chart'
          ? riskScoreBucketColors()
          : setOfColorsWithDecreasingIntensity(3);

      merged_data.map((item) => {
        const label = `${item.key}`;

        datasets.map((dset, dindex) => {
          if (!dset.type) {
            dset.backgroundColor = customColorSets[dindex];
          }
          dset.data.push(item.counts[dindex]);
        });

        max_arr.push(Math.max.apply(null, item.counts));

        labels.push(
          `${opts.label_prefix} ${label.replace(
            opts.label_unused_char_extractor,
            ''
          )}`
        );
      });

      chart_options = {
        type: 'bar',
        fully_custom_y_axis: {
          ...secondYaxisDenotingCount(roundUpToNearestN(RIGHT_AXIS_MAX, true)),
        },
        annotationTechniques: null,
        tickOptions: {
          callback: function (value, ___, __) {
            return `${CURRENCY_PREFIX_UTF()} ${nFormatter(value, 3, false)}`;
          },
          max: roundUpToNearestN(Math.max.apply(null, max_arr), false),
        },

        legendOptions: null,
        onAnimationComplete: onAnimationCompleteTechniques.noop,
        stackOptions: {
          stacked: false,
        },
        labelCallback: function (tooltipItem, data) {
          return data.datasets
            .filter((_, i) => i === tooltipItem.datasetIndex)
            .map((dset) => {
              return `${dset.label}: ${!isCount(dset)
                ? nFormatter(dset.data[tooltipItem.index], 3, false)
                : CHART_FUNCT.readable_large_currency(
                  dset.data[tooltipItem.index],
                  true
                )
                }`;
            })
            .join('');
        },
      };

      datasets = promoteScatterplotDatasetToFront(datasets);
      break;
    case 'AggregatedAddressesPerRC_vsPML_address_based_histogram_chart':
    case 'AggregatedAddressesPerRC_vsIL_address_based_histogram_chart':
    case 'AggregatedAddressesPerRC_vsScore_address_based_histogram_chart':
    case 'AggregatedAddressesPerRC_vsRecommendations_address_based_histogram_chart':
      ID_MAP = {
        [`${total_keyword}`]: total_keyword.split('_').join(' '),
        ...arrayToObject(
          getListOfSelectedLocationPerilRiskClasses().map((step) => {
            return {
              k: `Risk Class ${padZero(step)}`,
              v: `Risk Class ${padZero(step)}`,
            };
          }),
          'k',
          'v'
        ),
        nonexistent_score: 'nonexistent_score',
      };

      num_of_locations = filterObjectPropertiesByKeyRegexes(
        jsonized_csv.result.filter(
          (obj) => obj.Metric === ID_MAP[total_keyword]
        )[0],
        [opts.column_extractor_re]
      );

      const riskClassRows: Array<{
        column_name: string;
        data: any;
      }> = Object.keys(ID_MAP)
        .filter((key) => key.includes('Risk'))
        .map((key) => {
          return filterObjectPropertiesByKeyRegexes(
            jsonized_csv.result.filter((obj) => obj.Metric === ID_MAP[key])[0],
            [opts.column_extractor_re]
          );
        })
        .map((extraction, index) => {
          return {
            column_name: `Risk Class ${padZero(index + 1)}`,
            data: extraction,
          };
        });

      labels = riskClassRows.map((r) => r.column_name);

      merged_data = sortArrOfStringsAlphabetically(
        Object.keys(num_of_locations)
      ).map((okey) => {
        return {
          key: `${opts.label_prefix}${okey.replace(
            opts.label_unused_char_extractor,
            ''
          )}`,
          counts: riskClassRows
            .map((row) => {
              return row.data[okey];
            })
            .map((val) =>
              // UDGB-26 edge case where the values include percentages
              // such as 165 (9%)
              // https://regex101.com/r/aGQgLt/1
              query_id.includes('PerRC_vsIL')
                ? Number(val?.match(/^\d{1,}/)?.[0])
                : // otherwise, simply extract the number from the string
                extractNumberFromString(val)
            ),
        };
      });

      customColorSets = setOfColorsWithDecreasingIntensity(10);

      max_arr = [];

      datasets = [
        ...merged_data.map((item, index) => {
          max_arr.push(Math.max.apply(null, item.counts));

          return {
            label: item.key,
            data: [...item.counts],
            stack: index + 1,
            yAxisID: 'normal_y_axis',
            backgroundColor: customColorSets[index],
          };
        }),
        {
          label: `${ID_MAP[total_keyword]}`,
          data: Object.keys(ID_MAP) // column based totals
            .filter((key) => key.includes('Risk'))
            .map((key) => {
              return filterObjectPropertiesByKeyRegexes(
                jsonized_csv.result.filter(
                  (obj) => obj.Metric === ID_MAP[key]
                )[0],
                [new RegExp(total_keyword)]
              )[total_keyword];
            })
            .map((v) => extractNumberFromString(v)),
          type: 'scatter',
          yAxisID: CUSTOM_Y_AXIS_ID,
          backgroundColor: '#000000',
          borderColor: '#000000',
          showLine: false,
        },
      ];

      RIGHT_AXIS_MAX = Math.max.apply(null, datasets[datasets.length - 1].data);

      chart_options = {
        type: 'bar',
        fully_custom_y_axis: {
          ...secondYaxisDenotingCount(roundUpToNearestN(RIGHT_AXIS_MAX, false)),
        },
        annotationTechniques: null,
        tickOptions: {
          callback: function (value, ___, __) {
            return value;
          },
          max: roundUpToNearestN(Math.max.apply(null, max_arr), false),
        },

        legendOptions: null,
        onAnimationComplete: onAnimationCompleteTechniques.noop,
        stackOptions: {
          stacked: false,
        },
        labelCallback: function (tooltipItem, data) {
          return data.datasets
            .filter((_, i) => i === tooltipItem.datasetIndex)
            .map((dset) => {
              return `${dset.label}: ${dset.data[tooltipItem.index]}`;
            })
            .join('');
        },
      };

      datasets = promoteScatterplotDatasetToFront(datasets);
      break;
    case 'AggregatedAddressesPerIC_vsRecommendations_address_based_histogram_chart':
    case 'AggregatedAddressesPerIC_vsPML_address_based_histogram_chart':
    case 'AggregatedAddressesPerIC_vsIL_address_based_histogram_chart':
    case 'AggregatedAddressesPerIC_vsScore_address_based_histogram_chart':
      ID_MAP = {
        [`${total_keyword}`]: total_keyword.split('_').join(' '),
        ...arrayToObject(
          LISTS_OF_HISTOGRAM_CATEGORIES.listOfIndustryCategories.map(
            (industry) => {
              return {
                k: industry,
                v: industry,
              };
            }
          ),
          'k',
          'v'
        ),
        nonexistent_score: 'nonexistent_score',
      };

      num_of_locations = filterObjectPropertiesByKeyRegexes(
        jsonized_csv.result.filter(
          (obj) => obj.Metric === ID_MAP[total_keyword]
        )[0],
        [opts.column_extractor_re]
      );

      const industryCategRows: Array<{
        column_name: string;
        data: any;
      }> = Object.keys(ID_MAP)
        .filter(
          (key) => !key.includes(total_keyword) && !key.includes('nonexistent')
        )
        .map((key) => {
          return {
            column_name: key,
            data: filterObjectPropertiesByKeyRegexes(
              jsonized_csv.result.filter(
                (obj) => obj.Metric === ID_MAP[key]
              )[0],
              [opts.column_extractor_re]
            ),
          };
        })
        .map((extraction) => {
          return {
            column_name: `${opts.label_prefix}${extraction.column_name.replace(
              opts.label_unused_char_extractor,
              ''
            )}`,
            data: extraction.data,
          };
        });

      labels = industryCategRows.map((r) => r.column_name);

      merged_data = sortArrOfStringsAlphabetically(
        Object.keys(num_of_locations)
      ).map((okey) => {
        return {
          key: okey.replace(COLUMN_NAME_CLEANSER_RE, ''),
          counts: industryCategRows
            .map((row) => {
              return row.data[okey];
            })
            .map((val) => extractNumberFromString(val)),
        };
      });

      customColorSets = setOfColorsWithDecreasingIntensity(19);

      max_arr = [];

      datasets = [
        ...merged_data.map((item, index) => {
          max_arr.push(Math.max.apply(null, item.counts));

          return {
            label: item.key,
            data: [...item.counts],
            stack: index + 1,
            yAxisID: 'normal_y_axis',
            backgroundColor: customColorSets[index],
          };
        }),
        {
          label: `${ID_MAP[total_keyword]}`,
          data: Object.keys(ID_MAP) // column based totals
            .filter(
              (key) => !key.includes('number') && !key.includes('nonexistent')
            )
            .map((key) => {
              return filterObjectPropertiesByKeyRegexes(
                jsonized_csv.result.filter(
                  (obj) => obj.Metric === ID_MAP[key]
                )[0],
                [new RegExp(total_keyword)]
              )[total_keyword];
            })
            .map((v) => extractNumberFromString(v)),
          type: 'scatter',
          yAxisID: CUSTOM_Y_AXIS_ID,
          backgroundColor: '#000000',
          borderColor: '#000000',
          showLine: false,
        },
      ];

      RIGHT_AXIS_MAX = Math.max.apply(null, datasets[datasets.length - 1].data);

      chart_options = {
        type: 'bar',
        fully_custom_y_axis: {
          ...secondYaxisDenotingCount(roundUpToNearestN(RIGHT_AXIS_MAX, false)),
        },
        annotationTechniques: null,
        tickOptions: {
          callback: function (value, ___, __) {
            return value;
          },
          max: roundUpToNearestN(Math.max.apply(null, max_arr), false),
        },

        legendOptions: null,
        onAnimationComplete: onAnimationCompleteTechniques.noop,
        stackOptions: {
          stacked: false,
        },
        labelCallback: function (tooltipItem, data) {
          return data.datasets
            .filter((_, i) => i === tooltipItem.datasetIndex)
            .map((dset) => {
              return `${dset.label}: ${dset.data[tooltipItem.index]}`;
            })
            .join('');
        },
      };

      datasets = promoteScatterplotDatasetToFront(datasets);
      break;

    case 'AggregatedAddressesPerScorePerPML_address_based_histogram_chart':
    case 'AggregatedAddressesPerAsIfScorePerPML_address_based_histogram_chart':
      labels = null; // ['Level 1', 'Level 2', 'Level 3'];

      level3only = self.level3only;
      hideTrendlines = self.hideTrendlines;

      (originalResponse as SearchResponse<AddressesDoc>).hits.hits.map((h) => {
        const hit = h._source;

        const peril = (window as any).selected_location_peril;

        let applicableAssessment: LocationAssessment | {} =
          hit.address.locationProperty?.locationAssessments?.find((lass) => {
            return peril === lass.peril;
          });

        if (!applicableAssessment) {
          console.warn('couldnt find assessment', {
            hit,
            locPeril: (window as any).selected_location_peril,
          });
          applicableAssessment = {};
        }

        let x: number = objectPath.get(
          hit,
          `address.locationProperty.PML_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`
        );

        let level: number, y: number;

        if (query_id.includes('AsIfScore')) {
          ({ asIfScore: y, level } = applicableAssessment as any);
        } else {
          ({ score: y, level } = applicableAssessment as any);
        }

        if (isFalsey(x) || isFalsey(y)) {
          console.warn('falsey x or y', { x, y, offerNum: hit.offer.number, addressId: hit.address.id, assessment: applicableAssessment, hit });
        }

        // UDGB-54 default to 0 to include non-availble PML & scores too
        x = x ? x : 0;
        y = y ? y : 0;
        level = level ? level : 1;

        switch (level) {
          case 1:
            if (level3only) {
              break;
            }
            points_info_level_1.push({
              x: x,
              y: y,
              level,
              hit: {
                ...hit,
              },
            });
            break;
          case 2:
            if (level3only) {
              break;
            }
            points_info_level_2.push({
              x: x,
              y: y,
              level,
              hit: {
                ...hit,
              },
            });
            break;
          case 3:
            points_info_level_3.push({
              x: x,
              y: y,
              level,
              hit: {
                ...hit,
              },
            });
            break;
        }

        // The matrix is no longer restricted by reasonable scores and PML
        // if (y <= 110 && x <= 150e6) { }

        if (level3only && level !== 3) {
        } else {
          data_hash.push({
            x: x,
            y: y,
            hit: {
              ...hit,
            },
          });
        }
      });

      self.data_hash = data_hash;
      // no data hash CSV because the reports should be generated via
      // the drawing functionality -- using the DataMarkerService.

      const trendline_datasets = hideTrendlines
        ? []
        : generateYearlyTrendlineDatasetsForScatterScoreMatrices(
          data_hash,
          true
        );

      datasets = [
        {
          data: points_info_level_1,
          label: 'Level 1',
          type: 'scatter',
          borderColor: palette_base_human_readable.red,
          backgroundColor: palette_base_human_readable.red,
          ...hoverActions,
        },
        {
          data: points_info_level_2,
          label: 'Level 2',
          type: 'scatter',
          borderColor: palette_base_human_readable.orange,
          backgroundColor: palette_base_human_readable.orange,
          ...hoverActions,
        },
        {
          data: points_info_level_3,
          label: 'Level 3',
          type: 'scatter',
          borderColor: palette_base_human_readable.green,
          backgroundColor: palette_base_human_readable.green,
          ...hoverActions,
        },
      ];

      chart_options = {
        type: 'scatter',
        annotationTechniques: null,
        ...(hideTrendlines
          ? // UDGB-19 do not display trendlines
          {}
          : {
            customAnnotations: {
              drawTime: 'afterDatasetsDraw', // (default)

              // Mouse events to enable on each annotation.
              // Should be an array of one or more browser-supported mouse events
              // See https://developer.mozilla.org/en-US/docs/Web/Events
              annotations: trendline_datasets.map((dset) => {
                return dset.customAnnotations;
              }),
            },
          }),
        xAxisOpts: {
          ticks: {
            min: 0,
            callback: function (value, ___, __) {
              return `${CURRENCY_PREFIX_UTF()} ${nFormatter(value, 3, false)}`;
            },
            stepSize: 10e6,
          },
          scaleLabel: {
            display: true,
            labelString: 'PML in Mio',
          },
        },
        tickOptions: {
          callback: function (value, ___, __) {
            return value;
          },
          max: 100, // roundUpToNearestN(Math.max.apply(null, points.map(p => p.y)), false)
        },

        legendOptions: {
          display: true,
          position: 'top',
        },
        onAnimationComplete: onAnimationCompleteTechniques.noop,
        stackOptions: {
          stacked: false,
        },
        labelCallback: function (tooltipItem, data) {
          return data.datasets
            .filter((_, i) => i === tooltipItem.datasetIndex)
            .map((_) => {
              const parent = data_hash_lookup(
                tooltipItem.xLabel,
                tooltipItem.yLabel
              );
              if (!parent || !parent.hit) {
                return '';
              }

              const hit = parent.hit;

              return `LoB #${parseInt(hit.lob.id, 10)}, Score ${tooltipItem.yLabel
                }, PML ${CURRENCY_PREFIX_UTF()} ${nFormatter(
                  tooltipItem.xLabel,
                  3,
                  false
                )}`;
            })
            .join('');
        },
        onClick: function (evt, arr) {
          // const activePoints = this.getElementsAtEvent(evt);
          // const el = activePoints[0];
          // alert(el);

          if (!arr || !arr.length) {
            return;
          }

          const element = arr[0];
          let target;
          switch (element._datasetIndex) {
            case 0:
              target = points_info_level_1[element._index];
              break;
            case 1:
              target = points_info_level_2[element._index];
              break;
            case 2:
              target = points_info_level_3[element._index];
              break;
          }
          if (typeof target === 'undefined') {
            return;
          }
          const hit = target.hit
            ? target
            : data_hash_lookup(target.x, target.y);
          self.jsonPanelInput = highlightJSON(hit.hit, '').html;
        },
      };

      break;
    case 'AggregatedAddressesPerAsIfScorePerPML_EML_Ratio_address_based_histogram_chart':
      labels = null;

      level3only = self.level3only;

      (originalResponse as SearchResponse<AddressesDoc>).hits.hits.map((h) => {
        const hit = h._source;

        if (
          hit.address.locationProperty &&
          hit.address.locationProperty.locationAssessments &&
          (window as any).selected_location_peril
        ) {
          const peril = (window as any).selected_location_peril;

          const applicableAssessment =
            hit.address.locationProperty.locationAssessments.filter((lass) => {
              return peril === lass.peril;
            });

          if (applicableAssessment.length) {
            const pml: number = objectPath.get(
              hit,
              `address.locationProperty.PML_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`
            );

            const eml: number = objectPath.get(
              hit,
              `address.locationProperty.EML_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`
            );

            const x: number = pml / eml;

            let level: number, y: number;

            if (query_id.includes('AsIfScore')) {
              ({ asIfScore: y, level } = applicableAssessment[0]);
            } else {
              ({ score: y, level } = applicableAssessment[0]);
            }

            if (!isFalsey(x) && !isFalsey(y)) {
              switch (level) {
                case 1:
                  if (level3only) {
                    break;
                  }
                  points_info_level_1.push({
                    x: x,
                    y: y,
                    hit: {
                      ...hit,
                    },
                  });
                  break;
                case 2:
                  if (level3only) {
                    break;
                  }
                  points_info_level_2.push({
                    x: x,
                    y: y,
                    hit: {
                      ...hit,
                    },
                  });
                  break;
                case 3:
                  points_info_level_3.push({
                    x: x,
                    y: y,
                    hit: {
                      ...hit,
                    },
                  });
                  break;
              }

              if (level3only && level !== 3) {
              } else {
                data_hash.push({
                  x: x,
                  y: y,
                  hit: {
                    ...hit,
                  },
                });
              }
            }
          }
        }
      });

      self.data_hash = data_hash;
      self.dataHashCSV = new CsvService().nestedJSONtoCSV(
        data_hash.map((item) => item.hit)
      );

      datasets = [
        {
          data: points_info_level_1,
          label: 'Level 1',
          type: 'scatter',
          borderColor: palette_base_human_readable.red,
          backgroundColor: palette_base_human_readable.red,
          ...hoverActions,
        },
        {
          data: points_info_level_2,
          label: 'Level 2',
          type: 'scatter',
          borderColor: palette_base_human_readable.orange,
          backgroundColor: palette_base_human_readable.orange,
          ...hoverActions,
        },
        {
          data: points_info_level_3,
          label: 'Level 3',
          type: 'scatter',
          borderColor: palette_base_human_readable.green,
          backgroundColor: palette_base_human_readable.green,
          ...hoverActions,
        },
      ];

      chart_options = {
        type: 'scatter',
        annotationTechniques: null,
        xAxisOpts: {
          ticks: {
            min: 0,
            max: 1,
            callback: function (value, ___, __) {
              return value;
            },
            stepSize: 0.1,
          },
          scaleLabel: {
            display: true,
            labelString: 'PML/EML Ratio',
          },
        },
        tickOptions: {
          callback: function (value, ___, __) {
            return value;
          },
          max: 100, // roundUpToNearestN(Math.max.apply(null, points.map(p => p.y)), false)
        },

        legendOptions: {
          display: true,
          position: 'top',
        },
        onAnimationComplete: onAnimationCompleteTechniques.noop,
        stackOptions: {
          stacked: false,
        },
        labelCallback: function (tooltipItem, data) {
          return data.datasets
            .filter((_, i) => i === tooltipItem.datasetIndex)
            .map((_) => {
              const parent = data_hash_lookup(
                tooltipItem.xLabel,
                tooltipItem.yLabel
              );
              if (!parent || !parent.hit) {
                return '';
              }

              const hit = parent.hit;

              return `LoB #${parseInt(hit.lob.id, 10)}, Score ${tooltipItem.yLabel
                }, Ratio ${tooltipItem.xLabel}`;
            })
            .join('');
        },
        onClick: function (evt, arr) {
          // const activePoints = this.getElementsAtEvent(evt);
          // const el = activePoints[0];
          // alert(el);

          if (!arr || !arr.length) {
            return;
          }

          const element = arr[0];
          let target;
          switch (element._datasetIndex) {
            case 0:
              target = points_info_level_1[element._index];
              break;
            case 1:
              target = points_info_level_2[element._index];
              break;
            case 2:
              target = points_info_level_3[element._index];
              break;
          }
          if (typeof target === 'undefined') {
            return;
          }
          const hit = target.hit
            ? target
            : data_hash_lookup(target.x, target.y);
          self.jsonPanelInput = highlightJSON(hit.hit, '').html;
        },
      };

      break;

    case 'AggregatedAddressesPerRiskScore_vsPremiums_address_based_histogram_chart':
      ID_MAP = {
        ...arrayToObject(
          [
            [0, 10],
            [10, 20],
            [20, 30],
            [30, 40],
            [40, 50],
            [50, 60],
            [60, 70],
            [70, 80],
            [80, 90],
            [90, 100],
          ].map((interval, index) => {
            const [gte, lte] = interval;
            return {
              key: `score ${gte}...${lte}`,
              val: `score ${gte}...${lte}`,
            };
          }),
          'key',
          'val'
        ),
      };

      premiumRows = Object.keys(ID_MAP).map((key) => {
        return {
          column_name: key,
          data: jsonized_csv.result.filter(
            (obj) => obj.Metric === ID_MAP[key]
          )[0],
        };
      });

      labels = premiumRows.map((r) => r.column_name);

      customColorSets = riskScoreBucketColors();

      mprem_data = [];
      tprem_data = [];
      deviation_data = [];

      premiumRows.map((row) => {
        mprem_data.push(row.data.mprem);
        tprem_data.push(row.data.tprem);
        deviation_data.push(row.data.deviation);
      });

      max_arr = []
        .concat(mprem_data.map(extractNumberFromString))
        .concat(tprem_data.map(extractNumberFromString));

      datasets = [
        {
          label: 'mprem',
          data: mprem_data.map(extractNumberFromString),
          stack: 1,
          yAxisID: 'normal_y_axis',
          backgroundColor: duplicateArrayItems(customColorSets[0]),
        },
        {
          label: 'tprem',
          data: tprem_data.map(extractNumberFromString),
          stack: 2,
          yAxisID: 'normal_y_axis',
          backgroundColor: duplicateArrayItems(customColorSets[1]),
        },
        // {
        //   label: 'deviation',
        //   data: deviation_data
        //     .map(extractNumberFromString)
        //     .map(num => (isNaN(num) ? 0 : num)),
        //   stack: 3,
        //   yAxisID: CUSTOM_Y_AXIS_ID,
        //   backgroundColor: '#CCC',
        //   fill: false,
        //   borderColor: '#CCC',
        //   type: 'line'
        // }
      ];

      chart_options = {
        type: 'bar',
        // fully_custom_y_axis: {
        //   // TODO Joe fix y axis max setting
        //   ...secondYaxisDenotingCount(
        //     Math.max.apply(null, deviation_data.map(extractNumberFromString)),
        //     Math.min.apply(null, deviation_data.map(extractNumberFromString)),
        //     'Deviation in %'
        //   )
        // },
        yAxisOpts: {
          scaleLabel: {
            display: true,
            labelString: 'Premium Amount',
          },
        },
        xAxisOpts: {
          scaleLabel: {
            display: true,
            labelString: 'Rating Tool Average Peril Score Buckets',
          },
        },
        annotationTechniques: null,
        tickOptions: {
          callback: function (value, ___, __) {
            return CHART_FUNCT.pprint_el(value);
          },
          max: roundUpToNearestN(Math.max.apply(null, max_arr), false),
        },

        legendOptions: null,
        onAnimationComplete: onAnimationCompleteTechniques.noop,
        stackOptions: {
          stacked: false,
        },
        labelCallback: function (tooltipItem, data) {
          return data.datasets
            .filter((_, i) => i === tooltipItem.datasetIndex)
            .map((dset) => {
              return `${dset.label}: ${dset.data[tooltipItem.index]}`;
            })
            .join('');
        },
      };
      break;
    case 'AggregatedAddressesPerRiskScore_vsPremiums_IndividualLocations_address_based_histogram_chart':
      ID_MAP = {
        ...arrayToObject(
          [
            [0, 20],
            [20, 40],
            [40, 60],
            [60, 80],
            [80, 100],
          ].map((interval, _) => {
            const [gte, lte] = interval;
            return {
              key: `score ${gte}...${lte}`,
              val: `score ${gte}...${lte}`,
            };
          }),
          'key',
          'val'
        ),
      };

      const hits = <AddressesDoc[]>(
        originalResponse.hits.hits.map((h) => h._source)
      );
      const hits_by_id = {};
      hits.map((h) => {
        hits_by_id[h.address.id] = h;
      });

      premiumRows = Object.keys(ID_MAP).map((key) => {
        return {
          column_name: key,
          data: jsonized_csv.result.filter(
            (obj) => obj.Metric === ID_MAP[key]
          )[0],
        };
      });

      labels = premiumRows.map((r) => r.column_name);

      customColorSets = riskScoreBucketColors();

      mprem_data = [];
      tprem_data = [];
      deviation_data = [];
      max_arr = [];

      const score_based_datasets = [[], [], [], [], []];

      hits.map((hit) => {
        try {
          // let score = hit.address.locationProperty.locationAssessments.score;
          let mprem = hit.lob.rating[`mTotalPremium_${CURRENCY_PREFIX_ISO()}`];
          let tprem = hit.lob.rating[`totalPremium_${CURRENCY_PREFIX_ISO()}`];
          let score = hit.lob.rating.superScore;

          // bulk cast to float
          [score, mprem, tprem] = [score, mprem, tprem].map(parseFloat);

          if ([score, mprem, tprem].some((num) => isNaN(num))) {
            console.error({
              score,
              mprem,
              tprem,
              hit,
            });
            throw new Error('Some of the attributes are NaN');
          }

          const deviation = premiumDeviation(mprem, tprem, 4);
          const numerical_deviation = <number>premiumDeviation(mprem, tprem);

          const base = {
            hit,
            score,
            mprem,
            tprem,
            deviation,
            numerical_deviation,
          };

          max_arr.push(numerical_deviation);

          switch (true) {
            case score >= 0 && score < 20:
              score_based_datasets[0].push(base);
              break;
            case score >= 20 && score < 40:
              score_based_datasets[1].push(base);
              break;
            case score >= 40 && score < 60:
              score_based_datasets[2].push(base);
              break;
            case score >= 60 && score < 80:
              score_based_datasets[3].push(base);
              break;
            case score >= 80:
              score_based_datasets[4].push(base);
              break;
          }
        } catch (err) {
          console.warn(err);
        }
      });

      deviation_data = flattenArrays(
        score_based_datasets.map((score_arr) =>
          score_arr.map((hit) => hit.numerical_deviation)
        )
      );

      datasets = [].concat(
        labels.map((label, index) => {
          return {
            label: label,
            data: score_based_datasets[index]
              .sort(dynamicSort('score'))
              .map((point) => {
                return {
                  x: point.score,
                  y: point.numerical_deviation,
                  hit: point,
                };
              }),
            type: 'scatter',
            yAxisID: 'normal_y_axis',
            stack: index + 1,
            backgroundColor: customColorSets[2][index],
            borderColor: customColorSets[0][index],
            showLine: true,
          };
        })
      );
      // .concat([
      //   {
      //     label: 'deviation',
      //     data: deviation_data,
      //     stack: 3,
      //     yAxisID: CUSTOM_Y_AXIS_ID,
      //     backgroundColor: '#CCC',
      //     fill: false,
      //     borderColor: '#CCC',
      //     type: 'line'
      //   }
      // ]);

      console.warn({
        datasets,
        max_arr,
      });

      chart_options = {
        type: 'scatter',
        yAxisOpts: {
          scaleLabel: {
            display: true,
            labelString: 'Deviation in %',
          },
        },
        xAxisOpts: {
          scaleLabel: {
            display: true,
            labelString: 'Rating Tool Average Peril Score Buckets',
          },
        },
        annotationTechniques: null,
        tickOptions: {
          callback: function (value, ___, __) {
            return `${roundUp(value, 4)} %`;
          },
          max: Math.max.apply(null, max_arr),
        },
        legendOptions: null,
        onAnimationComplete: onAnimationCompleteTechniques.noop,
        stackOptions: {
          stacked: false,
        },
        onClick: function (evt, arr) {
          // const activePoints = this.getElementsAtEvent(evt);
          // const el = activePoints[0];
          // alert(el);

          if (!arr || !arr.length) {
            return;
          }

          const element = arr[0];
          const target = datasets[element._datasetIndex].data[element._index];

          if (typeof target === 'undefined') {
            return;
          }

          const hit = target.hit.hit;
          delete target['hit']['hit'];
          const stats = target.hit;

          self.jsonPanelInput = highlightJSON(
            {
              info: stats,
              object: hit,
            },
            ''
          ).html;
        },
      };
      break;
    case 'AggregatedAddressesPerRecommendationKinds_vsPriority_address_based_histogram_chart':
    case 'AggregatedAddressesPerRecommendationKinds_vsRecommendations_address_based_histogram_chart':
      ID_MAP = {
        [`${total_keyword}`]: total_keyword.split('_').join(' '),
        ...arrayToObject(
          mergeCriteriaSpecificAssessmentRecommendations().map((r_kind) => {
            return {
              key: `${r_kind}`,
            };
          }),
          'key',
          'key'
        ),
      };

      if (!query_id.includes('vsPriority')) {
        num_of_locations = {
          Metric: total_keyword,
          number_of_locations: '-',
          ...arrayToObject(
            LISTS_OF_HISTOGRAM_CATEGORIES.listOfRecommendationStatusFeedbacks.map(
              (status, i) => {
                return {
                  k: `A) ${i}) ${status}`,
                  v: '-',
                };
              }
            ),
            'k',
            'v'
          ),
        };
      } else {
        num_of_locations = {
          Metric: total_keyword,
          number_of_locations: '-',
          'A) 0) must': '-',
          'A) 1) important': '-',
          'A) 2) advisory': '-',
        };
      }

      // filterObjectPropertiesByKeyRegexes(
      //   jsonized_csv.result.filter(obj => obj.Metric === ID_MAP[total_keyword])[0],
      //   [opts.column_extractor_re]
      // );

      const recommendationRows: Array<{
        column_name: string;
        data: any;
      }> = Object.keys(ID_MAP)
        .filter((key) => key !== total_keyword && key !== 'Metric')
        // due to the naming inconsistencies for metrics of the form
        // `probability | Explosion hazards | Adequate grounding, bounding and earthing`,
        // let's make sure we filter those out that aren't renderable -- i.e.
        // those that are found in `jsonized_csv` but not in `ID_MAP`.
        .filter(
          (key) =>
            jsonized_csv.result.filter((obj) => {
              return obj.Metric === ID_MAP[key];
            }).length
        )
        .map((key) => {
          return filterObjectPropertiesByKeyRegexes(
            jsonized_csv.result.filter((obj) => {
              return obj.Metric === ID_MAP[key];
            })[0],
            [opts.column_extractor_re]
          );
        })
        .map((extraction, index) => {
          return {
            column_name: `${opts.label_prefix}${extraction.Metric.replace(
              opts.label_unused_char_extractor,
              ''
            )}`,
            data: extraction,
          };
        });

      merged_data = sortArrOfStringsAlphabetically(
        Object.keys(num_of_locations)
      )
        .filter((key) => key !== total_keyword && key !== 'Metric')
        .map((okey) => {
          return {
            key: `${opts.label_prefix}${okey.replace(
              opts.label_unused_char_extractor,
              ''
            )}`,
            counts: recommendationRows
              .map((row) => {
                return row.data[okey];
              })
              .map((val) => extractNumberFromString(val)),
          };
        });

      customColorSets = setOfColorsWithDecreasingIntensity(6);

      max_arr = [];
      labels = [];
      const datapoints = [];

      merged_data
        .filter((item) => !item.key.includes('number of'))
        .map((item, index) => {
          max_arr.push(Math.max.apply(null, item.counts));

          labels.push(item.key);
          datapoints.push([...item.counts].sumByReduce());
        });

      datasets = [
        {
          data: datapoints,
          yAxisID: 'normal_y_axis',
          backgroundColor: customColorSets[4],
        },
      ];

      chart_options = {
        type: 'bar',
        annotationTechniques: null,
        tickOptions: {
          callback: function (value, ___, __) {
            return value;
          },
          max: Math.max.apply(null, datapoints) * 1.2,
        },

        legendOptions: null,
        onAnimationComplete: onAnimationCompleteTechniques.noop,
        stackOptions: {
          stacked: false,
        },
        labelCallback: function (tooltipItem, data) {
          return data.datasets
            .filter((_, i) => i === tooltipItem.datasetIndex)
            .map((dset) => {
              return `${tooltipItem.label}: ${dset.data[tooltipItem.index]}`;
            })
            .join('');
        },
      };
      break;
    case 'AggregatedAddressesPerEvaluations_vsProbabilityCriterias_address_based_histogram_chart':
    case 'AggregatedAddressesPerEvaluations_vsPotentialCriterias_address_based_histogram_chart':
    case 'AggregatedAddressesPerEvaluations_vsPreventionCriterias_address_based_histogram_chart':
      ID_MAP = {
        number_of_evaluations: 'number of evaluations',
        average_score: 'average score',
        average_weight: 'average weight',
      };

      const num_of_evaluations = filterObjectPropertiesByKeyRegexes(
        jsonized_csv.result.filter(
          (obj) => obj.Metric === ID_MAP.number_of_evaluations
        )[0],
        [opts.column_extractor_re]
      );
      const avg_score = filterObjectPropertiesByKeyRegexes(
        jsonized_csv.result.filter(
          (obj) => obj.Metric === ID_MAP.average_score
        )[0],
        [opts.column_extractor_re]
      );
      const avg_weight = filterObjectPropertiesByKeyRegexes(
        jsonized_csv.result.filter(
          (obj) => obj.Metric === ID_MAP.average_weight
        )[0],
        [opts.column_extractor_re]
      );

      RIGHT_AXIS_MAX = Math.max.apply(null, Object.values(num_of_evaluations));

      merged_data = sortArrOfStringsAlphabetically(
        Object.keys(num_of_evaluations)
      ).map((okey) => {
        return {
          key: `${opts.label_prefix}${okey.replace(
            opts.label_unused_char_extractor,
            ''
          )}`,
          counts: [
            avg_score[okey],
            avg_weight[okey],
            num_of_evaluations[okey],
          ].map((val) => extractNumberFromString(val)),
        };
      });

      labels = [];
      datasets = [
        {
          label: `${ID_MAP.average_score}`,
          data: [],
          stack: 1,
          yAxisID: 'normal_y_axis',
        },
        {
          label: `${ID_MAP.average_weight}`,
          data: [],
          stack: 2,
          yAxisID: 'normal_y_axis',
        },
        {
          label: `${ID_MAP.number_of_evaluations}`,
          data: [],
          type: 'scatter',
          yAxisID: CUSTOM_Y_AXIS_ID,
          backgroundColor: 'transparent',
          borderColor: '#000000',
          showLine: true,
        },
      ];

      max_arr = [];

      customColorSets = setOfColorsWithDecreasingIntensity(2);

      merged_data.map((item) => {
        const label = `${item.key}`;

        datasets.map((dset, dindex) => {
          if (!dset.type) {
            dset.backgroundColor = customColorSets[dindex];
          }
          dset.data.push(item.counts[dindex]);
        });

        if (item.key !== ID_MAP.num_of_evaluations) {
          max_arr.push(Math.max.apply(null, item.counts));
        }

        labels.push(
          `${opts.label_prefix} ${label.replace(
            opts.label_unused_char_extractor,
            ''
          )}`
        );
      });

      chart_options = {
        type: 'bar',
        fully_custom_y_axis: {
          ...secondYaxisDenotingCount(roundUpToNearestN(RIGHT_AXIS_MAX, true)),
        },
        annotationTechniques: null,
        tickOptions: {
          callback: function (value, ___, __) {
            return `${nFormatter(value, 3, false)}`;
          },
          max:
            Math.max.apply(
              null,
              [].concat(datasets[0].data).concat(datasets[1].data)
            ) * 1.2,
        },

        legendOptions: null,
        onAnimationComplete: onAnimationCompleteTechniques.noop,
        stackOptions: {
          stacked: false,
        },
        labelCallback: function (tooltipItem, data) {
          return data.datasets
            .filter((_, i) => i === tooltipItem.datasetIndex)
            .map((dset) => {
              return `${tooltipItem.xLabel}: ${nFormatter(
                dset.data[tooltipItem.index],
                3,
                false
              )}`;
            })
            .join('');
        },
      };

      datasets = promoteScatterplotDatasetToFront(datasets);

      break;

    case 'AggregatedAddressesPerRecommendationKinds_vsLocationCounts_address_based_histogram_chart':
    case 'AggregatedAddressesPerRecommendationKinds_vsNegativeCondition_address_based_histogram_chart':
      ID_MAP = {
        [`${total_keyword}`]: total_keyword.split('_').join(' '),
      };

      num_of_locations = filterObjectPropertiesByKeyRegexes(
        jsonized_csv.result.filter(
          (obj) => obj.Metric === ID_MAP[total_keyword]
        )[0],
        [opts.column_extractor_re]
      );

      const original_recommendations_aggs =
        originalResponse.aggregations['histograms_1)_number_of_locations'];

      const filtered_recommendations_aggs = {};
      for (const [key, value] of Object.entries(
        filterObjectPropertiesByKeyRegexes(
          original_recommendations_aggs,
          [/\b(?!meta)\b\S+/g] // everything except "meta"
        )
      )) {
        filtered_recommendations_aggs[key.trim()] = value;
      }

      merged_data = sortObjectKeysByPropertyValue(num_of_locations)
        .filter((okey) => okey !== total_keyword && okey !== 'Metric')
        .map((okey) => {
          return {
            key: `${opts.label_prefix}${okey.replace(
              opts.label_unused_char_extractor,
              ''
            )}`,
            counts: [num_of_locations[okey]].map((val) =>
              extractNumberFromString(val)
            ),
          };
        });

      labels = [];
      datasets = [
        {
          label: `${ID_MAP[total_keyword]}`,
          data: [],
          type: 'scatter',
          // yAxisID: CUSTOM_Y_AXIS_ID,
          backgroundColor: 'transparent',
          borderColor: '#000000',
          showLine: true,
        },
      ];

      // start off w/ 0 because applying Math.max on an empty array later on would equal -Infinity
      max_arr = [0];

      customColorSets = setOfColorsWithDecreasingIntensity(2);

      merged_data.map((item) => {
        const label = `${item.key}`;

        datasets.map((dset, dindex) => {
          if (!dset.type) {
            dset.backgroundColor = customColorSets[dindex];
          }
          dset.data.push(item.counts[dindex]);
        });

        max_arr.push(Math.max.apply(null, item.counts));

        labels.push(
          `${opts.label_prefix} ${label.replace(
            opts.label_unused_char_extractor,
            ''
          )}`
        );
      });



      RIGHT_AXIS_MAX = parseInt(
        `${Math.max.apply(null, [].concat(datasets[0].data)) * 1.2}`,
        10
      );

      chart_options = {
        type: 'bar',
        // fully_custom_y_axis: {
        //   ...secondYaxisDenotingCount(RIGHT_AXIS_MAX)
        // },
        annotationTechniques: null,
        tickOptions: {
          callback: function (value, ___, __) {
            return `${nFormatter(value, 3, false)}`;
          },
          max: RIGHT_AXIS_MAX,
          min: 0,
          stepSize: RIGHT_AXIS_MAX > 10 ? undefined : 1,
        },

        legendOptions: null,
        onAnimationComplete: onAnimationCompleteTechniques.noop,
        stackOptions: {
          stacked: false,
        },
        // raw options override per UDGB-30:
        // don't display the x-axis labels because they tend to be too long:
        options: {
          scales: {
            xAxes: [{
              ticks: {
                display: false
              }
            }],
            yAxes: [{
              ticks: {
                max: (
                  // add 1 to the max_arr to raise the y-axis limit so that
                  // the hovering label above the data point can remain visible
                  Math.max.apply(null, max_arr) + 1
                )
              }
            }]
          },
        },
        labelCallback: function (tooltipItem, data) {
          return data.datasets
            .filter((_, i) => i === tooltipItem.datasetIndex)
            .map((dset) => {
              return `${tooltipItem.xLabel} • ${dset.label}: ${nFormatter(
                dset.data[tooltipItem.index],
                3,
                false
              )}`;
            })
            .join('');
        },
        onClick: function (evt, arr) {
          if (!arr || !arr.length) {
            return;
          }

          try {
            const element = arr[0];
            const label = element._chart.config.data.labels[element._index];
            const json = filtered_recommendations_aggs[
              label.trim()
            ].inner_hits.hits.hits.map((h) => h._source);
            console.warn({ element, label, json });
            self.jsonPanelInput = highlightJSON(json, '').html;
          } catch (err) {
            console.warn({ err });
          }
        },
      };

      const flattenedArr = [];
      const innerHits: AddressesDoc[] = [];

      const targetPeril = (window as any).selected_location_peril as PerilName;

      const basedOn = query_id.toLowerCase().includes('negative')
        ? 'Negative Aspect'
        : 'Recommendation';

      Object.keys(filtered_recommendations_aggs).map(
        (recommendation_or_neg_aspect) => {
          filtered_recommendations_aggs[
            recommendation_or_neg_aspect
          ].inner_hits.hits.hits
            .map((h) => h._source)
            .map((inner_source: AddressesDoc) => {
              // unlinked copy
              innerHits.push(JSON.parse(JSON.stringify(inner_source)));

              let statusAndPrio = {};
              if (basedOn === 'Recommendation') {
                const [key_factor, key_criterion, recommendationName] =
                  recommendation_or_neg_aspect.split(' | ');

                try {
                  // find the respective recommendation based on the `recommendationName`
                  // extracted from the concatenated agg name
                  const targetRecommendation =
                    inner_source.address.locationProperty.locationAssessmentsLevel3[
                      targetPeril
                    ].keyFactors[key_factor as keyof KeyFactors].keyCriteria[
                      key_criterion as keyof KeyCriterion
                    ].recommendations.find((iteratedRecomm) => {
                      const baseline = recommendationName.trim().toLowerCase();
                      const candidate =
                        iteratedRecomm.name
                          .trim()
                          .toLowerCase();

                      return (
                        baseline === candidate ||
                        baseline.startsWith(candidate) ||
                        candidate.startsWith(baseline)
                      );
                    });

                  const { priority, statusFeedback } = targetRecommendation;

                  statusAndPrio = {
                    [`${basedOn} Priority`]: priority,
                    [`${basedOn} Status Feedback`]: statusFeedback,
                  };
                } catch (err) {
                  console.warn(
                    'Error trying to access the targetRecommendation',
                    { err }
                  );
                }
              }

              // extract the `totalScore` which needs to be shown in the table
              const { totalScore } =
                inner_source['address']['locationProperty'][
                'locationAssessmentsLevel3'
                ][targetPeril];

              // delete the contents of `locationAssessmentsLevel3.$peril` because it's hella long
              // due to `keyFactors.*.*.*...`
              delete inner_source['address']['locationProperty'][
                'locationAssessmentsLevel3'
              ][(window as any).selected_location_peril as PerilName];
              // put the `totalScore` back in
              inner_source['address']['locationProperty'][
                'locationAssessmentsLevel3'
              ][targetPeril] = { totalScore } as any;

              flattenedArr.push({
                ...inner_source,
                [`${basedOn} Name`]: recommendation_or_neg_aspect,
                ...statusAndPrio,
              });
            });
        }
      );

      self.dataHashCSV = new CsvService().nestedJSONtoCSV(flattenedArr);
      self.additionalDataHashCSV = new CsvService().nestedJSONtoCSV(
        flattenAssessmentsFromAddresses({
          // The inner hits aren't our typical, top-level `hits.hits`.
          // They're the potentially-duplicated inner hits from nested aggregations
          // so they need to be de-duplicated by the address ID
          // to prevent duplicate assessments dump needed for UDGB-29.
          innerHits: uniqBy<AddressesDoc>(innerHits, 'address.id'),
          basedOn:
            basedOn === 'Recommendation'
              ? 'recommendations'
              : 'negativeAspects',
          targetPeril,
        })
      );

      break;
  }

  self.handleOnIncoming(datasets, labels, chart_options, true);
  self.legendMap = null;
  // self.legendMap = legendMap;
};

const BASE_REM_HISTO_INCOMING_HANDLER = (query_id: string) => {
  return {
    legend: true,
    handleIncomingData(inputData, kwargs) {
      const Transformer = new TableRowTransformer(query_id, inputData, []);
      let total_keyword;
      switch (true) {
        case query_id.includes('address_based') &&
          !query_id.includes('Evaluations'):
          total_keyword = 'number_of_locations';
          break;
        case query_id.includes('Evaluations'):
          total_keyword = 'number_of_evaluations';
          break;
        default:
          total_keyword = 'number_of_lobs';
          break;
      }

      const tableReadyContent =
        Transformer.transformCategorizedSpreadResponseIntoTableRows(
          'Metric',
          REM_AGGS_ONLY_KEYS.includes(query_id)
            ? '' // empty as per UDGB-25 and UDGB-26 and UDGB-27
            : 'sum',
          null,
          {
            total_keyword: total_keyword,
          }
        );

      const { tableContent } = tableReadyContent;
      const { header } = tableContent;
      const { rows } = tableContent;

      if (!header.length || !header[0]) {
        return;
      }

      // get rid of the sigma sum row
      rows.splice(rows.length - 1, 1);

      const rows_with_sigma = rows.slice();

      if (regexMatches(/PerRecommendationKinds/, query_id) && rows.length) {
        const sigma_map = {
          [SUM_SIGMA_UTF]: new Map<string, number>(),
        };

        rows_with_sigma.map((row) => {
          const extract = extractFirstValueFromDict(row) as Map<string, number>;
          extract.forEach((val, key) => {
            if (sigma_map[SUM_SIGMA_UTF].has(key)) {
              sigma_map[SUM_SIGMA_UTF].set(
                key,
                sigma_map[SUM_SIGMA_UTF].get(key) + val
              );
            } else {
              sigma_map[SUM_SIGMA_UTF].set(key, val);
            }
          });
        });
        rows_with_sigma.push(sigma_map);
      }

      this.csvInput = rowMapToCSV(header, rows_with_sigma);
      this.rawCsvInput = rowMapToCSV(header, rows_with_sigma, true);

      REM_DIAGRAM_RENDERERS(
        this,
        query_id,
        csvToJSON(rowMapToCSV(header, rows)),
        REM_DIAGRAMS_CUSTOM_OPTS[query_id],
        inputData,
        total_keyword
      );
    },
  };
};

export const REM_HISTOGRAM_TABLE_CHART_OPTIONS = {
  AggregatedAddressesVsIndustryCategory_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesVsIndustryCategory_histogram_chart'
    ),

  AggregatedAddressesPerIL_histogram_chart: BASE_REM_HISTO_INCOMING_HANDLER(
    'AggregatedAddressesPerIL_histogram_chart'
  ),
  AggregatedAddressesPerScore_histogram_chart: BASE_REM_HISTO_INCOMING_HANDLER(
    'AggregatedAddressesPerScore_histogram_chart'
  ),
  AggregatedAddressesPerPML_histogram_chart: BASE_REM_HISTO_INCOMING_HANDLER(
    'AggregatedAddressesPerPML_histogram_chart'
  ),
  AggregatedAddressesPerRC_histogram_chart: BASE_REM_HISTO_INCOMING_HANDLER(
    'AggregatedAddressesPerRC_histogram_chart'
  ),

  AggregatedAddressesPerRC_vsPML_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerRC_vsPML_address_based_histogram_chart'
    ),
  AggregatedAddressesPerRC_vsIL_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerRC_vsIL_address_based_histogram_chart'
    ),
  AggregatedAddressesPerRC_vsScore_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerRC_vsScore_address_based_histogram_chart'
    ),
  AggregatedAddressesPerRC_vsRecommendations_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerRC_vsRecommendations_address_based_histogram_chart'
    ),

  AggregatedAddressesPerIC_vsRecommendations_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerIC_vsRecommendations_address_based_histogram_chart'
    ),
  AggregatedAddressesPerIC_vsPML_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerIC_vsPML_address_based_histogram_chart'
    ),
  AggregatedAddressesPerIC_vsIL_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerIC_vsIL_address_based_histogram_chart'
    ),
  AggregatedAddressesPerIC_vsScore_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerIC_vsScore_address_based_histogram_chart'
    ),

  AggregatedAddressesPerScorePerPML_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerScorePerPML_address_based_histogram_chart'
    ),
  AggregatedAddressesPerAsIfScorePerPML_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerAsIfScorePerPML_address_based_histogram_chart'
    ),
  AggregatedAddressesPerAsIfScorePerPML_EML_Ratio_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerAsIfScorePerPML_EML_Ratio_address_based_histogram_chart'
    ),

  AggregatedAddressesPerRiskScore_vsPremiums_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerRiskScore_vsPremiums_address_based_histogram_chart'
    ),
  AggregatedAddressesPerRiskScore_vsPremiums_IndividualLocations_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerRiskScore_vsPremiums_IndividualLocations_address_based_histogram_chart'
    ),

  AggregatedAddressesPerRecommendationKinds_vsPriority_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerRecommendationKinds_vsPriority_address_based_histogram_chart'
    ),
  AggregatedAddressesPerRecommendationKinds_vsRecommendations_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerRecommendationKinds_vsRecommendations_address_based_histogram_chart'
    ),
  AggregatedAddressesPerRecommendationKinds_vsLocationCounts_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerRecommendationKinds_vsLocationCounts_address_based_histogram_chart'
    ),
  AggregatedAddressesPerRecommendationKinds_vsNegativeCondition_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerRecommendationKinds_vsNegativeCondition_address_based_histogram_chart'
    ),

  AggregatedAddressesPerEvaluations_vsProbabilityCriterias_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerEvaluations_vsProbabilityCriterias_address_based_histogram_chart'
    ),
  AggregatedAddressesPerEvaluations_vsPotentialCriterias_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerEvaluations_vsPotentialCriterias_address_based_histogram_chart'
    ),
  AggregatedAddressesPerEvaluations_vsPreventionCriterias_address_based_histogram_chart:
    BASE_REM_HISTO_INCOMING_HANDLER(
      'AggregatedAddressesPerEvaluations_vsPreventionCriterias_address_based_histogram_chart'
    ),
};
