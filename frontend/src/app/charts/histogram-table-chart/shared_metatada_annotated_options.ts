import { add, extractFirstValueFromDict, regexMatches } from 'src/app/utils/helpers/helpers';
import { rowMapToCSV, SUM_SIGMA_UTF, tableCellPostProcessingFunctions, TableRowTransformer } from './common';
import { provideBusinessActivityBuckets } from './constants';
import { IAggregationQueryBuilderResponse } from './queryBuilder';

export const BASE_METADATA_ANNOTATED_HISTO_INCOMING_HANDLER = (query_id: string) => {
    return {
        legend: true,
        handleIncomingData(inputData: IAggregationQueryBuilderResponse, kwargs) {
            // no agg response means that the generated query was empty
            if (!Object.keys(inputData?.aggregations || {})?.length) {
                // let's investigate the reason... Are the requirements selected?
                if (!provideBusinessActivityBuckets(query_id)?.length) {
                    this.displayWarningToSelectLoBAndIndustry();
                    return;
                }
            }

            const Transformer = new TableRowTransformer(query_id, inputData, []);

            const tableReadyContent =
                Transformer.transformQueryBuilderMetaAnnotatedResponseIntoTableRows();

            const { tableContent, piechartContent } = tableReadyContent;
            const { header, rows } = tableContent;

            if (!header.length || !header[0]) {
                return;
            }

            // get rid of the sigma sum row
            // rows.splice(rows.length - 1, 1);

            const rows_with_sigma = rows.slice();

            if (regexMatches(/PerRecommendationKinds/, query_id) && rows.length) {
                const sigma_map = {
                    [SUM_SIGMA_UTF]: new Map<string, number>(),
                };

                rows_with_sigma.map((row) => {
                    const extract = extractFirstValueFromDict(row) as Map<string, number>;
                    extract.forEach((val, key) => {
                        if (sigma_map[SUM_SIGMA_UTF].has(key)) {
                            sigma_map[SUM_SIGMA_UTF].set(
                                key,
                                sigma_map[SUM_SIGMA_UTF].get(key) + val
                            );
                        } else {
                            sigma_map[SUM_SIGMA_UTF].set(key, val);
                        }
                    });
                });
                rows_with_sigma.push(sigma_map);
            }

            if (!header.length || !header[0]) {
                return;
            }

            this.csvInput = rowMapToCSV(header, rows);
            this.rawCsvInput = rowMapToCSV(header, rows, true);

            const { labels, datasets, legendMap, onIncomingOptions } =
                Transformer.toPieChartJSArguments(
                    piechartContent,
                    piechartContent.map((i) => i.count).reduce(add, 0),
                    [],
                    tableCellPostProcessingFunctions[Transformer.query_id]
                );
            this.handleOnIncoming(datasets, labels, onIncomingOptions);
        },
    };
};
