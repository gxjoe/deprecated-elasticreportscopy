import { SearchParams, SearchResponse } from 'elasticsearch';
import * as objectPath from 'object-path';
import { emptyBoolMustMatch } from 'src/app/elastic/utils';
import { capitalize, pprintCurrency, pprintFloat } from 'src/app/utils/helpers/helpers';
import { prettyPercent } from '../utils';
import escaperegexp from 'lodash.escaperegexp';
import { CSV_SEPARATOR } from './common';

export enum AGG_VALUE_FORMATTERS_ENUM {
    IDENTITY = 'IDENTITY',
    IDENTITY_NAN_IS_ZERO = 'IDENTITY_NAN_IS_ZERO',
    PPRINT_CURRENCY = 'PPRINT_CURRENCY',
    PRETTY_PERCENT = 'PRETTY_PERCENT',
    NORMALIZE_TYPE_OF_BUSINESS = 'NORMALIZE_TYPE_OF_BUSINESS',
    NORMALIZE_NACE_CODE = 'NORMALIZE_NACE_CODE',
    PRETTY_FLOAT = 'PRETTY_FLOAT'
}

export const AGG_VALUE_FORMATTERS: {
    [key in AGG_VALUE_FORMATTERS_ENUM]: Function;
} = {
    IDENTITY: (v: any) => v,
    IDENTITY_NAN_IS_ZERO: (v: any) => (isNaN(v) || v === null ? 0 : v),
    PPRINT_CURRENCY: (v: any) => pprintCurrency(v),
    PRETTY_PERCENT: (v: any) =>
        isNaN(parseFloat(v)) ? '-' : prettyPercent(parseFloat(v) / 100, 1),
    // transform strings of type
    // `offer.lob.category.activity.property.agriculture.risk.3` to
    // `Property Agriculture Risk 3`
    NORMALIZE_TYPE_OF_BUSINESS: (v: string) =>
        v
            .replace('offer.lob.category.activity', '')
            .split('.')
            .map(capitalize)
            .join(' ')
            .trim()
            // Some strings contain encoded html signs. Since the CSVs will be semicolon-separated,
            // we need to make sure semicolns in column values are escaped and/or otherwise handled.
            .replace('&amp;', '&') // ampersand is pretty easy
            // no idea what we might encounter further so let's replace the semicolon with a comma...
            .replace(CSV_SEPARATOR, ','),
    NORMALIZE_NACE_CODE: (v: string) =>
        v
            .trim()
            // Some strings contain encoded html signs. Since the CSVs will be semicolon-separated,
            // we need to make sure semicolns in column values are escaped and/or otherwise handled.
            .replace('&amp;', '&') // ampersand is pretty easy
            // no idea what we might encounter further so let's replace the semicolon with a comma...
            .replace(CSV_SEPARATOR, ','),
    PRETTY_FLOAT: (v: number) => pprintFloat(v)
};

export const stringPathAccessorResolvers = (path: string, obj: any) => {
    return objectPath.get(obj, path);
};

interface IAggRequest extends SearchParams {
    aggs: {
        [key: string]: any;
    };
}

interface IAnyKeyValPairObject {
    [key: string]: any;
}

interface IColumnMeta1toManyAggs {
    column: {
        index: number;
        name: string;
        presented_agg_name: string;
    };
    available_aggs?: string[];
    post_processing: {
        [key: string]: {
            value: {
                formatter: string | AGG_VALUE_FORMATTERS_ENUM;
                // Gotta use a split path for the accessor here because of special chars in its path (usually dots).
                // More examples here: https://github.com/mariocasciaro/object-path#usage
                accessor: string & string[];
                [key: string]: string;
            };
        };
    };
}

interface IRowQuery {
    disabled?: boolean;
    meta?:
    | {
        row_index: number;
        row_name: string;
        category_column_name: string;
    }
    | IColumnMeta1toManyAggs
    | IAnyKeyValPairObject;
    filter?: any;
    agg_name?: string;
    aggs_themselves?: {
        [key: string]: IRowQuery | IAnyKeyValPairObject;
    };
}

interface IAggregationQueryBuilderParams {
    row_names: Array<string>;
    row_queries: Array<IRowQuery>;
}

const prepareInnerAggregation = (
    row_query: IRowQuery | IAnyKeyValPairObject
):
    | {
        [key: string]: {
            filter?: any;
            meta?: any;
            aggs?: any;
        };
    }
    | IRowQuery => {
    if (!row_query.aggs_themselves) {
        return row_query;
    }

    const inner_aggs = {};

    for (const subagg_key in row_query.aggs_themselves) {
        const agg = row_query.aggs_themselves[subagg_key];
        const { filter, meta, aggs_themselves, agg_name, ...rest } = agg

        const prepared_inner_aggs = prepareInnerAggregation(agg);

        if (prepared_inner_aggs && aggs_themselves) {
            const inner_agg = {
                filter: filter ? filter : emptyBoolMustMatch(),
                meta: meta ? meta : {},
                aggs: prepared_inner_aggs,
                ...rest,
            };

            inner_aggs[agg_name] = inner_agg;
        } else {
            return prepared_inner_aggs;
        }
    }

    return inner_aggs;
};

export const constructAggregationQuery = (
    params: IAggregationQueryBuilderParams
): IAggRequest[] => {
    return params.row_names.map((row_name, index) => {
        const row_query = params.row_queries[index];

        return {
            aggs: {
                [`histograms_${row_name}`]: {
                    filter: row_query.filter ? row_query.filter : emptyBoolMustMatch(),
                    meta: row_query.meta ? row_query.meta : {},
                    aggs: prepareInnerAggregation(row_query),
                },
            },
        };
    });
};

export interface IAggregationQueryBuilderResponse extends SearchResponse<any> {
    aggregations: {
        [key: string]:
        | {
            doc_count?: number;
            meta?: {
                row_index?: number;
                row_name?: string;
                category_column_name?: string;
            };
        }
        | {
            [key: string]: {
                doc_count?: number;
                meta?: {
                    column?: {
                        index?: number;
                        name?: string;
                        presented_agg_name?: string;
                    };
                    available_aggs?: string[];
                    post_processing?: {
                        [key: string]: {
                            value?: {
                                accessor?: string;
                                formatter?: string;
                            };
                        };
                    };
                };
            } & {
                [key: string]: any;
            };
        };
    };
}

/**
 * Some RIM YoY tables CANNOT have any top-level
 * date range queries applied since the comparison queries
 * in the aggs need to filtered based on reasonable options such as LoB,
 * Peril Selections etc too. So the comparison, say 2020 vs 2019 works,
 * we need to make sure the top-level 2020 selection doesn't filter all
 * the 2019 docs! We could use `global: {}` in the aggs but that way
 * we'd lose the other filters too! So we nullify the date range
 * filters only and keep the rest intact.
 * @param query_name
 * @param search_payload
 */
export const nullifyGlobalDatetimeFiltersInTimeRangeQueries = (
    query_name: string | undefined,
    search_payload: string
): string => {
    if (
        !query_name ||
        (typeof query_name === 'string' && !query_name.includes('minusTimeRange'))
    ) {
        return search_payload;
    }

    const parsed_payload = JSON.parse(search_payload);

    // could be either of those paths, depending on other filters, so check both
    if (
        !parsed_payload?.query?.bool?.filter?.length &&
        parsed_payload?.query?.bool?.filter?.bool?.filter?.length
    ) {
        return search_payload;
    }

    (
        parsed_payload?.query?.bool?.filter ||
        parsed_payload?.query?.bool?.filter?.bool?.filter
    ).map((filter_group) => {
        const filter_group_json = JSON.stringify(filter_group);
        // explanation here https://prnt.sc/um182p
        if (filter_group_json.match(/(\w\..+(Date|Issuance))/g)) {
            // replace in-place
            search_payload = search_payload.replace(
                // include the voluntary comma at the end
                new RegExp(`${escaperegexp(filter_group_json)}\,?`),
                ''
            );
        }
    });

    return search_payload;
};
