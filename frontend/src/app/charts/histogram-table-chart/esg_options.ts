import { rowMapToCSV } from "src/app/charts/histogram-table-chart/common";
import { EsgCriteriaToId, LobAndIndustryBasedBusinessActivityBreakdown, NaceCodes } from "src/app/charts/histogram-table-chart/histogramListsAndCategories";
import { AGG_VALUE_FORMATTERS, AGG_VALUE_FORMATTERS_ENUM } from "src/app/charts/histogram-table-chart/queryBuilder";
import { prettyPercent } from "src/app/charts/utils";
import { getValuesOfAffectingFilters } from "src/app/utils/constants/filteringComponentVsAffectedComponents";
import { pprintCurrency, uniq } from "src/app/utils/helpers/helpers";
import * as _ from 'lodash';

const withoutDocCount = (entry: string) => entry !== 'doc_count'

type ColumnMetadata = {
  columnIndex: number;
  columnName: string;
  formatter: string;
}

type MetadataWithValue = {
  meta: ColumnMetadata;
  value: string | number;
}

function getObjectsWithMeta(obj: unknown): MetadataWithValue[] {
  const objectsWithMeta: MetadataWithValue[] = [];

  function recursiveSearch(object: any) {
    if (typeof object === "object") {
      if (object.hasOwnProperty("meta")) {
        objectsWithMeta.push(object);
      }
      for (const key in object) {
        if (object.hasOwnProperty(key)) {
          recursiveSearch(object[key]);
        }
      }
    }
  }

  recursiveSearch(obj);
  return objectsWithMeta;
}

export const ESG_HISTOGRAM_TABLE_CHART_OPTIONS = {
  ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndRiskCode_histogram_chart: {
    legend: false,
    handleIncomingData(inputData: any) {
      const headers: string[] = ['Criterion', 'Esg Id'];

      const criteria = Object.keys(inputData.aggregations).filter(withoutDocCount).sort(byEsgId)
      headers.push(...getObjectsWithMeta(inputData.aggregations[criteria[0]]).sort((a, b) => a.meta.columnIndex - b.meta.columnIndex).map(({ meta }) => meta.columnName))

      const rows = criteria.map((criterion) => {
        const objectsWithMeta = getObjectsWithMeta(inputData.aggregations[criterion]).sort((a, b) => a.meta.columnIndex - b.meta.columnIndex)
        const columnToValue = objectsWithMeta.reduce((acc, curr) => ({ ...acc, [curr.meta.columnName]: curr.value }), {})

        const totalSum = columnToValue['Risk 1 • Uniqa total premium'] + columnToValue['Risk 2 • Uniqa total premium'] + columnToValue['Risk 3 • Uniqa total premium']
        const totalClients = columnToValue['Risk 1 • No. of clients'] + columnToValue['Risk 2 • No. of clients'] + columnToValue['Risk 3 • No. of clients']
        const totalNaSum = columnToValue['Risk 0 • Uniqa total premium'] + columnToValue['Risk N/A • Uniqa total premium']
        const totalNaClients = columnToValue['Risk 0 • No. of clients'] + columnToValue['Risk N/A • No. of clients']

        const columnValues = [totalSum, totalClients, totalNaSum, totalNaClients]
        const doesAllSummaryValuesExist = columnValues.filter(Boolean).length === columnValues.length
        let summaryColumns: [string, string][] = []

        if (doesAllSummaryValuesExist) {
          summaryColumns = [
            ['Risk 1-3 • No. of clients', totalClients],
            ['Risk 1-3 • Uniqa total premium', pprintCurrency(totalSum)],
            ['Ratio N/A • No. Of Clients', prettyPercent(totalClients, totalNaClients)],
            ['Ratio N/A • Uniqa Total Premium', prettyPercent(totalSum, totalNaSum)]
          ]
        }

        const entries = objectsWithMeta.map(getCellValue)
        return ({
          [getCriterionCellValue(criterion)]: new Map([
            ["Esg Id", getCriterionId(criterion)],
            ...entries,
            ...summaryColumns
          ])
        })
      })

      this.csvInput = rowMapToCSV(headers, rows);
      this.rawCsvInput = rowMapToCSV(headers, rows, true);
    }
  },
  ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndNaceCode_histogram_chart: {
    legend: false,
    handleIncomingData(inputData: any, kwargs) {
      const { app } = kwargs;
      const selectedNaceCodes = getValuesOfAffectingFilters(
        app,
        'ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndNaceCode_histogram_chart'
      );

      const rows: Record<string, Map<string, number>> = {};
      const headers: string[] = [...['Criterion', 'Esg Id'], ...Object.keys(NaceCodes).filter((naceCode) => selectedNaceCodes.includes(naceCode)).reduce((acc, naceCode) => {
        return ([...acc, ...getNaceCodeHeaders(naceCode)])
      }, [])];

      const criteria = Object.keys(inputData.aggregations).sort(byEsgId);

      criteria.forEach(criterion => {
        const criteriaCellValue = getCriterionCellValue(criterion)
        rows[criteriaCellValue] = new Map();
        rows[criteriaCellValue].set('Esg Id', getCriterionId(criterion))

        const byNaceBuckets = inputData.aggregations[criterion].byNace.byNace.buckets

        byNaceBuckets.filter(({ key }) => !selectedNaceCodes?.length || selectedNaceCodes.includes(key)).forEach(naceBucket => {
          const naceCode = naceBucket.key;

          const sumValue = naceBucket.byNaceNested.byNaceSum.value;
          const clientCount = naceBucket.byNaceClient.value;
          const naceCodeHeaders = getNaceCodeHeaders(naceCode)
          const uniqaTotalPremiumHeader = naceCodeHeaders[0]
          const clientCountHeader = naceCodeHeaders[1]
          rows[criteriaCellValue].set(uniqaTotalPremiumHeader, pprintCurrency(sumValue))
          rows[criteriaCellValue].set(clientCountHeader, clientCount)

          if (!headers.includes(uniqaTotalPremiumHeader)) {
            headers.push(uniqaTotalPremiumHeader);
          }

          if (!headers.includes(clientCountHeader)) {
            headers.push(clientCountHeader);
          }
        });
      });

      // Add default values for missing pairs
      criteria.forEach(criterion => {
        const criterionCellValue = getCriterionCellValue(criterion)
        const missingPairs = headers.filter(header => header !== 'Criterion' && !rows[criterionCellValue].has(header));
        missingPairs.forEach(missingPair => {
          const format = missingPair.includes('Uniqa Total Premium') ? pprintCurrency : (val) => val
          rows[criterionCellValue].set(missingPair, format(0));
        });
      });

      const rowz = Object.keys(rows).map((key) => ({ [key]: new Map([...rows[key].entries()].sort()) }))
      this.csvInput = rowMapToCSV(headers.sort(), rowz);
      this.rawCsvInput = rowMapToCSV(headers.sort(), rowz, true);
    }
  },
  ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndOccupancy_histogram_chart: {
    legend: false,
    handleIncomingData(inputData: any, kwargs) {
      const rows: Record<string, Map<string, number>> = {};
      const {
        LoB_Multiselect: lobs,
        Industry_Multiselect: industries,
        TypeOfBusiness_SemiStatic_Multiselect: typeOfBusinesses
      } = getValuesOfAffectingFilters(
        kwargs.app,
        'ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndOccupancy_histogram_chart',
        true
      );

      const targetTypeOfBusinesses = getTargetTypeOfBusinesses(typeOfBusinesses, industries, lobs);

      const allTypeOfBusinesses = convertDictionaryToList(LobAndIndustryBasedBusinessActivityBreakdown)
        .filter((typeOfBusiness) => targetTypeOfBusinesses.length < 1 || targetTypeOfBusinesses.includes(typeOfBusiness))
      const headers: string[] = [...['Criterion', 'Esg Id'], ...allTypeOfBusinesses.reduce((acc, lob) => {
        return ([...acc, ...getOccupancyHeaders(lob)])
      }, [])];

      const criteria = Object.keys(inputData.aggregations).sort(byEsgId);

      criteria.forEach(criterion => {
        const criteriaCellValue = getCriterionCellValue(criterion)
        rows[criteriaCellValue] = new Map();
        rows[criteriaCellValue].set('Esg Id', getCriterionId(criterion))

        const byLobBuckets = inputData.aggregations[criterion].byLob.byTob.buckets

        byLobBuckets.filter(({ key }) => targetTypeOfBusinesses.length < 1 || targetTypeOfBusinesses.includes(key)).forEach(naceBucket => {
          const lob = naceBucket.key;

          const sumValue = naceBucket.byTobNested.byTobSum.value;
          const clientCount = naceBucket.byTobNestedClient.byTobClient.value;
          const lobHeaders = getOccupancyHeaders(lob)
          const uniqaTotalPremiumHeader = lobHeaders[0]
          const clientCountHeader = lobHeaders[1]
          rows[criteriaCellValue].set(uniqaTotalPremiumHeader, pprintCurrency(sumValue))
          rows[criteriaCellValue].set(clientCountHeader, clientCount)

          if (!headers.includes(uniqaTotalPremiumHeader)) {
            headers.push(uniqaTotalPremiumHeader);
          }

          if (!headers.includes(clientCountHeader)) {
            headers.push(clientCountHeader);
          }
        });
      });

      // Add default values for missing pairs
      criteria.forEach(criterion => {
        const criterionCellValue = getCriterionCellValue(criterion)
        const missingPairs = headers.filter(header => header !== 'Criterion' && !rows[criterionCellValue].has(header));
        missingPairs.forEach(missingPair => {
          const format = missingPair.includes('Uniqa Total Premium') ? pprintCurrency : (val) => val
          rows[criterionCellValue].set(missingPair, format(0));
        });
      });

      const rowz = Object.keys(rows).map((key) => ({ [key]: new Map([...rows[key].entries()].sort()) }))
      this.csvInput = rowMapToCSV(headers.sort(), rowz);
      this.rawCsvInput = rowMapToCSV(headers.sort(), rowz, true);
    }
  },
  ESG_AggregatedEmissionsPerOccupancy_histogram_chart: {
    legend: false,
    handleIncomingData(inputData: any, { app }) {
      const {
        LoB_Multiselect: lobs,
        Industry_Multiselect: industries,
        TypeOfBusiness_SemiStatic_Multiselect: typeOfBusinesses
      } = getValuesOfAffectingFilters(
        app,
        'ESG_AggregatedEmissionsPerOccupancy_histogram_chart',
        true
      );
      const targetTypeOfBusinesses = getTargetTypeOfBusinesses(typeOfBusinesses, industries, lobs);
      const headers: string[] = ['Criterion'];

      const buckets = inputData.aggregations.byTypeOfBusiness.byTypeOfBusinessInner.buckets.filter((typeOfBusiness) => targetTypeOfBusinesses.length < 1 || targetTypeOfBusinesses.includes(typeOfBusiness.key))
      headers.push(...getObjectsWithMeta(buckets[0]).sort((a, b) => a.meta.columnIndex - b.meta.columnIndex).map(({ meta }) => meta.columnName))
      headers.push('Emissions Ratio', 'Intensity Ratio')
      const rows = buckets.map((bucket) => {
        const objectsWithMeta = getObjectsWithMeta(bucket).sort((a, b) => a.meta.columnIndex - b.meta.columnIndex)

        const entries = objectsWithMeta.map(getCellValue)
        entries.forEach(([columnName, value]) => {
          const cleanNumber = Number(String(value).replace(",", ".").replace(/\s/g, ''))
          if (columnName.includes('Emissions')) {
            entries.push(['Emissions Ratio', prettyPercent(cleanNumber, inputData.aggregations.totals.inner.emissions.value)])
          } else if (columnName.includes('Carbon intensity')) {
            entries.push(['Intensity Ratio', prettyPercent(cleanNumber, inputData.aggregations.totals.inner.intensity.value)])
          }
        })
        return ({
          [`${bucket.key || '(No Occupancy)'}`]: new Map(entries)
        })
      })
      const allLobs = convertDictionaryToList(LobAndIndustryBasedBusinessActivityBreakdown)
      const missingRows = targetTypeOfBusinesses.length > 0 ? [] : getMissingRows(allLobs, rows, buckets)
      const sortedRowsByCriterion = [...rows, ...missingRows].sort((a, b) => Object.keys(a).pop().localeCompare(Object.keys(b).pop()))

      this.csvInput = rowMapToCSV(headers, sortedRowsByCriterion);
      this.rawCsvInput = rowMapToCSV(headers, sortedRowsByCriterion, true);
    }
  },
  ESG_AggregatedEmissionsPerNaceCode_histogram_chart: {
    legend: false,
    handleIncomingData(inputData: any, { app }) {
      const headers: string[] = ['Criterion'];
      const selectedNaceCodes = getValuesOfAffectingFilters(
        app,
        'ESG_AggregatedEmissionsPerNaceCode_histogram_chart'
      );
      const byNaceBuckets = inputData.aggregations.byNacePrimaryCodes.byNacePrimaryCodesInner.buckets.filter(({ key }) => !selectedNaceCodes?.length || selectedNaceCodes.includes(key))
      headers.push(...getObjectsWithMeta(byNaceBuckets[0]).sort((a, b) => a.meta.columnIndex - b.meta.columnIndex).map(({ meta }) => meta.columnName))
      headers.push('Emissions Ratio', 'Intensity Ratio')
      const rows = byNaceBuckets.map((bucket) => {
        const objectsWithMeta = getObjectsWithMeta(bucket).sort((a, b) => a.meta.columnIndex - b.meta.columnIndex)

        const entries = objectsWithMeta.map(getCellValue)
        entries.forEach(([columnName, value]) => {
          const cleanNumber = Number(String(value).replace(",", ".").replace(/\s/g, ''))
          if (columnName.includes('Emissions')) {
            entries.push(['Emissions Ratio', prettyPercent(cleanNumber, inputData.aggregations.totals.inner.emissions.value)])
          } else if (columnName.includes('Carbon intensity')) {
            entries.push(['Intensity Ratio', prettyPercent(cleanNumber, inputData.aggregations.totals.inner.intensity.value)])
          }
        })
        return ({
          [getNaceCode(bucket.key) ? `[${bucket.key}] ${getNaceCode(bucket.key)}` : '(Without Nace)']: new Map(entries)
        })
      })

      const sortedRowsByCriterion = [...rows].sort((a, b) => Object.keys(a).pop().localeCompare(Object.keys(b).pop()))

      this.csvInput = rowMapToCSV(headers, sortedRowsByCriterion);
      this.rawCsvInput = rowMapToCSV(headers, sortedRowsByCriterion, true);
    }
  },
}

/**
 * TypeOfBusiness, Industries or LoBs can be mapped to the typeOfBusiness. LoBs to
 * Industries and Industries to typeOfBusiness. We take into account only the most specific
 * filter active at the time. If '04 Wood' Industry and 'Furniture Risk 4' typeOfBusiness
 * is selected, we are taking into account only the latter one. '04 Wood' maps not only to
 * 'Furniture Risk 4' but also to e.g. 'Wood Risk 2'. If user selected a specific
 * typeOfBusiness, he isn't expecting to see other just because he selected
 * an Industry that is mapped to a different typeOfBusiness. Therefore, the more specific
 * filters are taken into account in favor of general ones.
 */
const getTargetTypeOfBusinesses = (typeOfBusinesses: string[], industries: string[], lobs: string[]): string[] => {
  let targetTypeOfBusinesses: string[] = [];

  // Return typeOfBusinesses if defined
  if (Array.isArray(typeOfBusinesses) && typeOfBusinesses.length > 0) {
    targetTypeOfBusinesses = typeOfBusinesses;
    //
  } else if (Array.isArray(industries) && industries.length > 0) {
    targetTypeOfBusinesses = industries.flatMap((industry) =>
      Object.values(LobAndIndustryBasedBusinessActivityBreakdown)
        .flatMap((val) => val[industry])
    ).reduce((acc, curr) => acc.includes(curr) ? acc : [...acc, curr], []);
  } else if (Array.isArray(lobs) && lobs.length > 0) {
    targetTypeOfBusinesses = lobs.flatMap((lob) => Object.entries<string[]>(LobAndIndustryBasedBusinessActivityBreakdown[lob])
      .flatMap(([_, val]) => val))
      .reduce((acc, curr) => acc.includes(curr) ? acc : [...acc, curr], []);
  } else {
    targetTypeOfBusinesses = [];
  }

  return targetTypeOfBusinesses;
};

function getNaceCodeHeaders(naceCode: string) {
  const convertedNaceCode = getNaceCodeOrDefault(naceCode)
  return [`[${naceCode}] ${convertedNaceCode} • Uniqa Total Premium`, `[${naceCode}] ${convertedNaceCode} • Nr. of Clients`]
}

function getNaceCode(naceCode: string) {
  if (!naceCode) {
    return ''
  }

  return (NaceCodes[naceCode] ?? naceCode).replaceAll(';', ',')
}

function getNaceCodeOrDefault(naceCode: string) {
  return getNaceCode(naceCode) || ' Without Nace'
}

function getOccupancyHeaders(occupancy: string) {
  const convertedOccupancy = _.capitalize(occupancy.replaceAll(/;/g, ',').replaceAll(/[()]/g, '')) || 'Without Occupancy';

  return [`[${convertedOccupancy}] • Uniqa Total Premium`, `[${convertedOccupancy}] • Nr. of Clients`]
}

function getMissingRows(allKeys: string[], rows: { [key: string]: Map<string, string> }[], buckets: object[]) {
  const firstRowMeta = getObjectsWithMeta(buckets[0]).sort((a, b) => a.meta.columnIndex - b.meta.columnIndex)
  const existingKeys = rows.map(Object.keys).reduce((acc, curr) => [...acc, ...curr], [])

  return uniq(allKeys.filter((key) => !existingKeys.includes(key))).map((key) => {
    const entries = firstRowMeta.map(({ meta }) => getCellValue({ meta, value: 0 }))
    entries.push(['Emissions Ratio', prettyPercent(0, 0)])
    entries.push(['Intensity Ratio', prettyPercent(0, 0)])

    return { [key]: new Map(entries) }
  })
}

function convertDictionaryToList(dictionary: Record<string, Record<string, string[]>>): string[] {
  let result: string[] = [];

  for (const topLevelProperty in dictionary) {
    const subDictionary = dictionary[topLevelProperty];

    for (const subProperty in subDictionary) {
      const subList = subDictionary[subProperty];
      result = result.concat(subList);
    }
  }

  return result;
}

function getCellValue({ meta, value }: MetadataWithValue) {
  const post_processing_fn = AGG_VALUE_FORMATTERS[meta.formatter || AGG_VALUE_FORMATTERS_ENUM.IDENTITY];
  const column_value = post_processing_fn(value) as string | number;
  return [meta.columnName, column_value] as const
}

function byEsgId(a: string, b: string) {
  return Object.keys(EsgCriteriaToId).indexOf(a) - Object.keys(EsgCriteriaToId).indexOf(b)
}

function getCriterionCellValue(criterion: string) {
  return `${criterion}`
}

function getCriterionId(criterion: string) {
  return EsgCriteriaToId[criterion]
}