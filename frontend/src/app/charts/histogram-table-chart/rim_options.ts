import { BASE_METADATA_ANNOTATED_HISTO_INCOMING_HANDLER } from './shared_metatada_annotated_options';

export const RIM_HISTOGRAM_TABLE_CHART_OPTIONS = {
    RIM_Property_AggregatedStatsByPML_address_based_histogram_chart:
        BASE_METADATA_ANNOTATED_HISTO_INCOMING_HANDLER(
            'RIM_Property_AggregatedStatsByPML_address_based_histogram_chart'
        ),
    RIM_Property_AggregatedStatsByPMLminusTimeRange_AbsoluteValues_address_based_histogram_chart:
        BASE_METADATA_ANNOTATED_HISTO_INCOMING_HANDLER(
            'RIM_Property_AggregatedStatsByPMLminusTimeRange_AbsoluteValues_address_based_histogram_chart'
        ),
    RIM_Property_AggregatedStatsByPMLminusTimeRange_PercentualValues_address_based_histogram_chart:
        BASE_METADATA_ANNOTATED_HISTO_INCOMING_HANDLER(
            'RIM_Property_AggregatedStatsByPMLminusTimeRange_PercentualValues_address_based_histogram_chart'
        ),
    RIM_Property_AggregatedStatsByTSI_address_based_histogram_chart:
        BASE_METADATA_ANNOTATED_HISTO_INCOMING_HANDLER(
            'RIM_Property_AggregatedStatsByTSI_address_based_histogram_chart'
        ),
    RIM_Property_AggregatedStatsByTSIminusTimeRange_AbsoluteValues_address_based_histogram_chart:
        BASE_METADATA_ANNOTATED_HISTO_INCOMING_HANDLER(
            'RIM_Property_AggregatedStatsByTSIminusTimeRange_AbsoluteValues_address_based_histogram_chart'
        ),
    RIM_Property_AggregatedStatsByTSIminusTimeRange_PercentualValues_address_based_histogram_chart:
        BASE_METADATA_ANNOTATED_HISTO_INCOMING_HANDLER(
            'RIM_Property_AggregatedStatsByTSIminusTimeRange_PercentualValues_address_based_histogram_chart'
        ),
};
