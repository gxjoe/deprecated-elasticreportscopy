import {
  sortArrOfStringsAlphabetically,
  regexMatches,
  uniq,
  extractInt,
  arrayToObject,
  pprintCurrency,
  flattenArrays,
  sortByExtractingNumbersFromStrings,
  safeStringForRegex,
} from '../../utils/helpers/helpers';

import { getValuesOfAffectingFilters } from '../../utils/constants/filteringComponentVsAffectedComponents';

import { _getValueAtIndexOrDefault } from 'src/app/utils/helpers/chartjs.helper';
import {
  LISTS_OF_HISTOGRAM_CATEGORIES,
  LobAndIndustryBasedBusinessActivityBreakdown,
  NaceCodes,
} from './histogramListsAndCategories';
import { PPM_HISTOGRAM_TABLE_CHART_OPTIONS } from './ppm_options';
import {
  REM_HISTOGRAM_TABLE_CHART_OPTIONS,
  getBucketsFromGlobalMappers,
} from './rem_options';
import {
  pdaInQuery,
  onlyNumericalRiskClass,
  riskClassAbbreviaton,
  TSIPerPerilInQuery,
  superScoreInQuery,
  infoLevelInQuery,
  numericalCategoryPrefacedWithLettersStandaloneRegex,
  destringifySemiNumericalCategoryName,
  filterObjectPropertiesByKeyRegexes,
  COLUMN_NAME_CLEANSER_RE,
} from './common';
import { LETTERS } from '../common/constants';
import {
  constructScriptedMetricNestedCardinalityQuery,
  constructNestedFilter,
  constructNestedExistsQuery,
  constructCombinedLocationPerilQuery,
  constructNestedTermQuery,
  constructScriptedMetricRatioOfSumsAggregation,
  emptyBoolMustMatch,
  constructNestedRangeQuery,
  constructNestedAssessmentTermQuery,
  constructNested_CriteriaSpecificAssessmentRecommendation_TermQuery,
  constructNested_CriteriaSpecific_RecommendationStatus_TermQuery,
  constructNested_CriteriaSpecific_RecommendationPriority_TermQuery,
  constructNestedAssessmentNegativeAspectTermQuery,
  mergeCriteriaSpecificAssessmentRecommendations,
  mergeCriteriaSpecificNegativeAspects,
  nestedStatsAggregation,
  nestedMedianAggregation,
  constructNestedRecommendationValueCountAggFactorAgnostic,
  constructSimulatedMultibucketBucketScript,
  constructNestedDateRangeQuery,
  constructBucketScriptScriptForPercentualComparison,
  nestedTermsAggregation,
  constructMedianAgg,
} from 'src/app/elastic/utils';
import { RIM_HISTOGRAM_TABLE_CHART_OPTIONS } from './rim_options';
import * as EB from 'elastic-builder'
import {
  constructAggregationQuery,
  AGG_VALUE_FORMATTERS_ENUM,
  AGG_VALUE_FORMATTERS,
} from './queryBuilder';
import { AppComponent } from 'src/app/app.component';
import { RelativeDatepickerComponent } from 'src/app/relative-datepicker/relative-datepicker.component';
import { NULL_ASSESMENT_SCORES_BEHAVIOR } from 'src/app/multiselect/enums';
import { PerilName } from 'src/app/interfaces/elastic';
import { ESG_HISTOGRAM_TABLE_CHART_OPTIONS } from 'src/app/charts/histogram-table-chart/esg_options';


const listOfLobs = LISTS_OF_HISTOGRAM_CATEGORIES.listOfLobs;
const listOfIndustryCategories =
  LISTS_OF_HISTOGRAM_CATEGORIES.listOfIndustryCategories;
const listOfRegionsOfOrigin =
  LISTS_OF_HISTOGRAM_CATEGORIES.listOfRegionalDirectorates;
const listOfRequestFromCountry =
  LISTS_OF_HISTOGRAM_CATEGORIES.listOfRequestFromCountry;
const listOfRecommendationStatusFeedbacks =
  LISTS_OF_HISTOGRAM_CATEGORIES.listOfRecommendationStatusFeedbacks;

// UDGB-52 - speed up query resolution by limiting the # of agg clauses
export const getListOfSelectedLocationPerilRiskClasses = (): number[] => (window as any).selected_location_risk_classes || [];

const getRangesFromSteps = (
  steps: Array<number>
): Array<{ min: number; max: number }> => {
  return steps.map((num, i) => {
    return { max: num, min: steps[i - 1] || 0 };
  });
};

export const numericalRanges = {
  SI: [
    0, 1e6, 5e6, 10e6, 20e6, 30e6, 50e6, 100e6, 200e6, 300e6, 500e6, 1e9, 2e9,
    3e9, 5e9, 10e9, 50e9,
  ],
  Premium: [
    0, 100, 500, 1e3, 3e3, 5e3, 10e3, 20e3, 1e4, 2e4, 3e4, 5e4, 1e5, 2.5e5, 5e5,
    1e6, 3e6, 5e6, 10e6,
  ],
  AvgRiskScores: [0, 20, 40, 60, 80, 100],
  ScoreIntervals: [
    [0, 20],
    [20, 40],
    [40, 60],
    [60, 80],
    [80, 100],
  ],
  ScoreIntervals10X: [
    [0, 10],
    [10, 20],
    [20, 30],
    [30, 40],
    [40, 50],
    [50, 60],
    [60, 70],
    [70, 80],
    [80, 90],
    [90, 100],
  ],
  UniqaPMLintervals: [
    [0, 20],
    [20, 50],
    [50, 80],
    [80, 140],
    [140, null],
  ],
  ListOfScores: [].rangeOfNumbers(1, 100, 1),
  // due to floating point imprecisions, use 0...10 / 1 instead of 0...1
  PML_EML_Ratio_Intervals: getRangesFromSteps(
    [].rangeOfNumbers(0, 10, 1).map((num) => num / 10)
  ).filter((entry) => entry.min !== entry.max),
  RIM_Property_PML_Intervals: [
    [0, 500e3],
    [500e3 + 1, 1e6],
    [1e6 + 1, 2e6],
    [2e6 + 1, 3e6],
    [3e6 + 1, 5e6],
    [5e6 + 1, 8e6],
    [8e6 + 1, 12e6],
    [12e6 + 1, 18e6],
    [18e6 + 1, 30e6],
    [30e6 + 1, 50e6],
    [50e6 + 1, 80e6],
    [80e6 + 1, 120e6],
    [120e6 + 1, 140e6],
    [140e6 + 1, Infinity],
  ],
  RIM_Property_TSI_Intervals: [
    [0, 500e3],
    [500e3 + 1, 1e6],
    [1e6 + 1, 2e6],
    [2e6 + 1, 3e6],
    [3e6 + 1, 5e6],
    [5e6 + 1, 8e6],
    [8e6 + 1, 12e6],
    [12e6 + 1, 18e6],
    [18e6 + 1, 30e6],
    [30e6 + 1, 50e6],
    [50e6 + 1, 80e6],
    [80e6 + 1, 120e6],
    [120e6 + 1, 140e6],
    [140e6 + 1, Infinity],
  ],
};

const stringifyNumericalNaceCode = (nc) => {
  return `${!nc.includes('N') ? 'N ' : ''}${nc}`;
};

export const destringifySemiNumericalRiskClassCategoryName = (nc) => {
  return nc.replace(/\R\C /g, '');
};

const createScoreRangeParameters = (end_score: number) => {
  const mn = 0;
  const mx = 100;

  if (end_score === mn) {
    return {
      gte: 0,
      lte: mn + 0.4,
    };
  } else if (end_score === mx) {
    return {
      gte: mx - 0.5,
      lte: mx,
    };
  }

  return {
    gte: end_score - 0.5,
    lt: end_score + 0.5,
  };
};

export const createRangeParametersFromRiskClassInput = (
  risk_class: number,
  mn = 1,
  mx = 10
) => {
  if (
    (window as any).selected_peril &&
    /flexa|burg/gim.test((window as any).selected_peril)
  ) {
    mx = 5;
  }
  if (risk_class === mn) {
    return {
      gt: 0,
      lte: risk_class + 0.4,
    };
  } else if (risk_class === mx) {
    return {
      gte: risk_class - 0.5,
      lte: risk_class,
    };
  }

  return {
    gte: risk_class - 0.5,
    lt: risk_class + 0.5,
  };
};

const createRangeParametersFromTSIInput = (
  field: string,
  tsi: number,
  steps: Array<number>,
  stats_operation: string
) => {
  const generated_steps = getRangesFromSteps(steps);
  const corresponding_interval = generated_steps.filter(
    (step) => step.max === tsi
  )[0];
  const { filter } = generateRangeFilterFromRange(
    corresponding_interval,
    field
  );
  const aggs = {
    [stats_operation]: {
      [stats_operation]: {
        field: constructPerilPath('personalDeviationAuthority'),
      },
    },
  };
  return {
    query_: filter,
    aggs_: aggs,
  };
};

export const extractRiskFromBusinessActivityName = (txt) => {
  return (
    txt.match(/risk \d/gim) &&
    txt
      .match(/risk \d/gim)[0]
      .split(' ')
      .join('')
  );
};

export const extractAlphaBracketsFromColName = (colname: string) => {
  const has = colname.match(COLUMN_NAME_CLEANSER_RE);
  if (!has) {
    return colname;
  }
  return colname.replace(COLUMN_NAME_CLEANSER_RE, '').trim();
};

export const assignTableCSSClasses = (query_id) => {
  if (
    regexMatches(superScoreInQuery, query_id) ||
    regexMatches(infoLevelInQuery, query_id)
  ) {
    return []
      .rangeOfNumbers(2, 30, 1)
      .filter((num) => num % 2 === 0)
      .map((col) => `child-${col}n-td-darker-border`)
      .join(' ');
  }

  if (
    [
      'AggregatedAddressesVsIndustryCategory_histogram_chart',
      'AggregatedAddressesPerIL_histogram_chart',
      'AggregatedAddressesPerScore_histogram_chart',
      'AggregatedAddressesPerRC_vsIL_address_based_histogram_chart',
      'AggregatedAddressesPerPML_histogram_chart',

      'AggregatedAddressesPerRC_histogram_chart',
      'AggregatedAddressesPerRC_vsScore_address_based_histogram_chart',
      'AggregatedAddressesPerRC_vsRecommendations_address_based_histogram_chart',

      'AggregatedAddressesPerIC_vsRecommendations_address_based_histogram_chart',
      'AggregatedAddressesPerIC_vsPML_address_based_histogram_chart',
      'AggregatedAddressesPerIC_vsIL_address_based_histogram_chart',
      'AggregatedAddressesPerIC_vsScore_address_based_histogram_chart',

      'AggregatedAddressesPerScorePerPML_address_based_histogram_chart',
      'AggregatedAddressesPerAsIfScorePerPML_address_based_histogram_chart',

      'AggregatedAddressesPerAsIfScorePerPML_EML_Ratio_address_based_histogram_chart',

      'AggregatedAddressesPerRiskScore_vsPremiums_address_based_histogram_chart',
      'AggregatedAddressesPerRiskScore_vsPremiums_IndividualLocations_address_based_histogram_chart',

      'AggregatedAddressesPerRecommendationKinds_vsPriority_address_based_histogram_chart',
      'AggregatedAddressesPerRecommendationKinds_vsRecommendations_address_based_histogram_chart',
      'AggregatedAddressesPerRecommendationKinds_vsLocationCounts_address_based_histogram_chart',
      'AggregatedAddressesPerRecommendationKinds_vsNegativeCondition_address_based_histogram_chart',

      'AggregatedAddressesPerEvaluations_vsProbabilityCriterias_address_based_histogram_chart',
      'AggregatedAddressesPerEvaluations_vsPotentialCriterias_address_based_histogram_chart',
      'AggregatedAddressesPerEvaluations_vsPreventionCriterias_address_based_histogram_chart',
      // RISK ENGINEERING END
    ].includes(query_id)
  ) {
    return `lastRowIsNotSummary notFitWidth child-2n-td-darker-border`;
  }

  if (
    [
      // Advanced PPM charts utilizing the metadata-annotated query builder
      'PPM_GlobalStatsPerBusinessActivity_NoCanvas_histogram_chart',
      'PPM_GlobalStatsPerNaceCode_NoCanvas_histogram_chart',

      // RE INSURANCE BEGIN
      'RIM_Property_AggregatedStatsByPML_address_based_histogram_chart',
      'RIM_Property_AggregatedStatsByPMLminusTimeRange_AbsoluteValues_address_based_histogram_chart',
      'RIM_Property_AggregatedStatsByPMLminusTimeRange_PercentualValues_address_based_histogram_chart',
      'RIM_Property_AggregatedStatsByTSI_address_based_histogram_chart',
      'RIM_Property_AggregatedStatsByTSIminusTimeRange_AbsoluteValues_address_based_histogram_chart',
      'RIM_Property_AggregatedStatsByTSIminusTimeRange_PercentualValues_address_based_histogram_chart',
    ].includes(query_id)
  ) {
    return `lastRowIsNotSummary notFitWidth`;
  }

  return '';
};

export const extractRiskFromRiskClassAndPeril = (klass) =>
  `risk_class_${klass.replace(/\D/g, '')}`;

const transformArrayOfAggsToObjofAggs = (
  main_query: any,
  queries: Array<any>
) => {
  queries.map((combo) => {
    Object.keys(combo.aggs).forEach((key) => main_query.aggs[key] = combo.aggs[key]);
  });
};

export const extract_base_path = (f) => {
  const splitf = f.split('.');
  if (splitf.length === 2) {
    return splitf[0];
  } else {
    return splitf.slice(0, splitf.length - 1).join('.');
  }
};

const pathsAreSame = (f1, f2) => {
  const p1 = extract_base_path(f1);
  const p2 = extract_base_path(f2);

  return p1 === p2;
};

export const constructPerilPath = (suffix?, peril_to_set?) => {
  const base = 'lob.rating.ratingToolPerils';
  const peril = (window as any).selected_peril;

  if (suffix) {
    suffix = `.${suffix}`;
  } else {
    suffix = '';
  }

  if (!peril_to_set) {
    if (!peril) {
      return `${base}${suffix}`;
    }

    return `${base}.${peril}${suffix}`;
  }

  if (!suffix) {
    return `${base}.${peril_to_set}`;
  }

  return `${base}.${peril_to_set}${suffix}`;
};

export const constructLocationPerilPath = (is_lob_based = false) => {
  if (is_lob_based) {
    return 'lob.addresses.locationProperty.locationAssessments';
  }
  return 'address.locationProperty.locationAssessments';
};

interface Range {
  gte?: number;
  gt?: number;
  lt?: number;
  lte?: number;
}

/**
 * Parses and formats the assessment score band
 * from the Numeric Range Sliders.
 * @returns
 */
const getAddressLocationAssessmentScoreRange = (): Range | undefined => {
  const storedAssessmentRange = (window as any)
    ?.AddressLocationAssessmentScoreRange;
  const hasAssessmentScoreRange =
    typeof storedAssessmentRange !== 'undefined' &&
    storedAssessmentRange !== null;
  return hasAssessmentScoreRange
    ? ({
      gte: storedAssessmentRange[0],
      lte: storedAssessmentRange[1],
    } as Range)
    : undefined;
};

export const constructNestedLocationAssessmentQueryString = (
  riskClass?: any,
  level?: any,
  score_range?: Range,
  scoreInt?: number,
  asIfScoreInt?: number,
  is_lob_based?: boolean,
  asIfScore_range?: Range
) => {
  const location_peril = (window as any).selected_location_peril;
  const peril_path = constructLocationPerilPath(Boolean(is_lob_based));

  // default to the score range from the Numeric Range Sliders
  if (typeof score_range === 'undefined') {
    score_range = getAddressLocationAssessmentScoreRange();
  }

  const keys = [
    `${peril_path}.peril`,
    `${peril_path}.riskClass`,
    `${peril_path}.level`,
    `${peril_path}.score`,
    `${peril_path}.score`,
    `${peril_path}.asIfScore`,
    `${peril_path}.asIfScore`,
  ];
  const values = [
    location_peril,
    riskClass,
    level,
    scoreInt,
    score_range,
    asIfScoreInt,
    asIfScore_range,
  ];

  const pairs = keys.map((key, index) => {
    return {
      key: key,
      val: values[index],
    };
  });

  return {
    nested: {
      path: peril_path,
      query: {
        bool: {
          must: [
            values.filter((_) => Boolean(_)).length
              ? {
                query_string: {
                  query: pairs
                    .filter((pair) => Boolean(pair.val))
                    .map((pair) => {
                      if (typeof pair.val === 'object') {
                        // construct a range query string
                        const { gt, gte, lt, lte } = pair.val as Range;

                        const isGte = typeof gte !== 'undefined';
                        const isLte = typeof lte !== 'undefined';

                        const from = isGte ? gte : gt;
                        const to = isLte ? lte : lt;

                        // inclusive ranges are [ ]
                        // exclusive ranges are { }
                        // https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html#_ranges

                        const brackets = {
                          left: isGte ? '[' : '{',
                          right: isLte ? ']' : '}',
                        };

                        return `${pair.key}:${brackets.left}${from} TO ${to}${brackets.right}`;
                      }
                      return `${pair.key}:${pair.val}`;
                    })
                    .concat(
                      // for whatever reasons, some locations may contain location peril assessments
                      // but those may not be scored. The user (more spec. the risk engineer) may
                      // want to filter such addresses out. Vice versa, they may want to *exclusively*
                      // include only those and get a picture of what such entities look like.
                      // Implements UDGB-34.
                      (window as any).ignore_null_assessment_scores
                        && (window as any).ignore_null_assessment_scores !== NULL_ASSESMENT_SCORES_BEHAVIOR.KEEP_EMPTY
                        ? [
                          (() => {
                            const path = `${peril_path}.score`;
                            // https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html#_field_names

                            const option = (window as any).ignore_null_assessment_scores;
                            if (option === NULL_ASSESMENT_SCORES_BEHAVIOR.EXCLUDE_EMPTY) {
                              // the score *must* exist if we're *not* ignoring null assessment scores
                              return `_exists_:${path}`;
                            }

                            // the user is specifically requesting addresses with *empty* assessment scores
                            return `NOT _exists_:${path}`;
                          })(),
                        ]
                        : []
                    )
                    .join(' AND '),
                },
              }
              : emptyBoolMustMatch(),
          ],
        },
      },
    },
  };
};

/**
 * As per UDGB-25, the avg scores need to reflect the currently selected
 * assessment score band from the Numeric Sliders.
 * As per UDGB-33, only the assessment score pertaining to the selected location peril
 * must be considered. Since the `locationAssessments` is a nested array of objects,
 * not restricting the other peril scores will skew the avg.
 * @param range
 * @returns
 */
export const constructScriptedAverageScoreQuery = (
  range: Range | undefined = getAddressLocationAssessmentScoreRange(),
  restrictToLocationPeril?: PerilName
) => {
  const avgAggContent = {
    script: {
      source: `
          def scorePath = 'address.locationProperty.locationAssessments.score';
          def score = doc[scorePath].size() != 0 ? doc[scorePath].value : -1;

          def locationPerilPath = 'address.locationProperty.locationAssessments.peril';
          def locationPeril = doc[locationPerilPath].size() != 0 ? doc[locationPerilPath].value : '';

          def rangeCondition = !params.applyRange || params.applyRange && score >= params.gte && score <= params.lte;
          def locationPerilCondition = params.restrictToLocationPeril == null || params.restrictToLocationPeril !== null && locationPeril == params.restrictToLocationPeril;

          if (rangeCondition && locationPerilCondition) {
            return score;
          }

          // gotta return 'null' and not '0' because
          // 0s will skew the avg
          return null;
      `,
      params: {
        ...range,
        applyRange: Boolean(range),
        restrictToLocationPeril
      },
    },
  };

  return {
    nested: {
      path: 'address.locationProperty.locationAssessments',
    },
    aggs: {
      inside: {
        avg: avgAggContent,
      },
    },
  };
};

export const constructSameLevelTermQueryForLocationPerilAndRiskClass = (
  peril,
  riskClass
) => { };

export const constructMustNotOfValidPDAs = () => {
  return {
    bool: {
      must_not: [
        {
          nested: {
            path: constructPerilPath(),
            query: {
              term: {
                [constructPerilPath(`mTotalPremium_$CURRENCY$`)]: 0,
              },
            },
          },
        },
        {
          nested: {
            path: constructPerilPath(),
            query: {
              term: {
                [constructPerilPath(`totalPremium_$CURRENCY$`)]: 0,
              },
            },
          },
        },
      ],
    },
  };
};

const constructHistogramBucketQuery = (path, field, gt, lte, is_last) => {
  let range_or_filter_query: any = {};
  if (gt + lte === 0) {
    range_or_filter_query = {
      bool: {
        should: [
          {
            range: {
              [field]: {
                lte: 0,
              },
            },
          },
          {
            bool: {
              must_not: [
                {
                  bool: {
                    filter: {
                      exists: {
                        field: field,
                      },
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    };
  } else {
    range_or_filter_query = {
      range: {
        [field]: {
          lte: lte,
          gt: gt,
        },
      },
    };
  }
  if (is_last) {
    // don't apply LTE -- just GT
    // range_or_filter_query = {
    //   [field]: {
    //     gt: gt
    //   }
    // };
  }

  return {
    nested: {
      path: path,
      query: range_or_filter_query,
    },
  };
};

const transformFunctionalPathsAndFields = (args, fn_params) => {
  args.forEach((__field, index) => {
    if (typeof __field === 'function') {
      return (args[index] = __field.apply(null, fn_params));
    }
    return args[index];
  });

  return args;
};

const constructCategorizedStatsBucketQuery = (
  path,
  field,
  stats_operation,
  category_field_is_nested,
  category_path,
  category_field,
  category_value,
  nested_filter
) => {
  const original_category_field = category_field;

  const potentially_functional_params = [
    path,
    field,
    category_path,
    category_field,
  ];
  [path, field, category_path, category_field] =
    transformFunctionalPathsAndFields(potentially_functional_params, [
      category_value,
    ]);

  if (
    regexMatches(
      numericalCategoryPrefacedWithLettersStandaloneRegex,
      category_value
    ) ||
    regexMatches(riskClassAbbreviaton, category_value)
  ) {
    category_value = destringifySemiNumericalRiskClassCategoryName(
      destringifySemiNumericalCategoryName(category_value)
    );
  }

  let category_field_query;
  let actual_agg_query = {
    [stats_operation]: {
      [stats_operation]: {
        field: field,
      },
    },
  };

  if (
    regexMatches(onlyNumericalRiskClass, original_category_field) ||
    regexMatches(riskClassAbbreviaton, original_category_field)
  ) {
    category_field_query = {
      range: {
        [category_field]: createRangeParametersFromRiskClassInput(
          extractInt(category_value)
        ),
      },
    };
  } else if (regexMatches(TSIPerPerilInQuery, original_category_field)) {
    const { query_, aggs_ } = createRangeParametersFromTSIInput(
      category_field,
      category_value,
      numericalRanges.SI,
      stats_operation
    );
    category_field_query = query_;
    actual_agg_query = aggs_;

    path = constructPerilPath();
  } else {
    category_field_query = {
      term: {
        [category_field]: category_value,
      },
    };
  }

  if (category_field_is_nested) {
    if (pathsAreSame(field, category_field)) {
      return {
        nested: {
          path: path,
        },
        aggs: {
          [`${stats_operation}_operation`]: {
            filter: {
              bool: {
                must: [category_field_query], // , nested_filter.nested.query]
              },
            },
            aggs: actual_agg_query,
          },
        },
      };
    } else {
      return {
        filter: {
          bool: {
            must: [
              {
                nested: {
                  path: category_path,
                  query: category_field_query,
                },
              },
              // {
              //   nested: {
              //     path: category_path,
              //     query: {
              //       bool: {
              //         filter: {
              //           exists: {
              //             field: category_field
              //           }
              //         }
              //       }
              //     }
              //   }
              // },

              // nested_filter
            ],
            // filter out rating tool entries w/ meaningless permiums => meaningless PDAs
            must_not: regexMatches(pdaInQuery, field)
              ? constructMustNotOfValidPDAs().bool.must_not
              : [],
          },
        },
        aggs: {
          [`${stats_operation}_operation`]: {
            nested: {
              path: path,
            },
            aggs: actual_agg_query,
          },
        },
      };
    }
  }

  return {
    filter: {
      term: {
        [category_field]: category_value,
      },
    },
    aggs: {
      [`${stats_operation}_operation`]: {
        nested: {
          path: path,
        },
        aggs: {
          [stats_operation]: {
            [stats_operation]: {
              field: field,
            },
          },
        },
      },
    },
  };
};

const constructCategorizedCountBucketQuery = (
  path,
  field,
  stats_operation,
  category_field_is_nested,
  category_path,
  category_field,
  category_value,
  nested_filter?
) => {
  if (category_field_is_nested) {
    if (pathsAreSame(field, category_field)) {
      return {
        nested: {
          path: path,
        },
        aggs: {
          [`${stats_operation}_operation`]: {
            filter: {
              bool: {
                must: [
                  {
                    term: {
                      [category_field]: category_value,
                    },
                  },
                  nested_filter,
                ],
              },
            },

            aggs: {
              [stats_operation]: {
                [stats_operation]: {
                  field: field,
                },
              },
            },
          },
        },
      };
    } else {
      return {
        nested: {
          path: path,
        },
        aggs: {
          [`${stats_operation}_operation`]: {
            filter: {
              bool: {
                must: [
                  {
                    nested: {
                      path: category_path,
                      query: {
                        term: {
                          [category_field]: category_value,
                        },
                      },
                    },
                  },
                  nested_filter,
                ],
              },
            },
            aggs: {
              [stats_operation]: {
                [stats_operation]: {
                  field: field,
                },
              },
            },
          },
        },
      };
    }
  }

  return {
    filter: {
      term: {
        [category_field]: category_value,
      },
    },
    aggs: {
      [`${stats_operation}_operation`]: {
        nested: {
          path: path,
        },
        aggs: {
          [stats_operation]: {
            [stats_operation]: {
              field: field,
            },
          },
        },
      },
    },
  };
};

const constructHistogramSpread = (bucket_prefix, steps, path, field) => {
  const array_of_buckets = [];
  const generated_steps = getRangesFromSteps(steps);

  generated_steps.map((range, ind) => {
    const { min: gt, max: lte } = range;
    array_of_buckets.push({
      bucket_name: `${bucket_prefix}${lte}`,
      query: constructHistogramBucketQuery(
        path,
        field,
        gt,
        lte,
        ind === generated_steps.length - 1
      ),
    });
  });
  return array_of_buckets;
};

const constructCategorizedSpread = (
  categories,
  path,
  field,
  stats_operation,
  category_field_is_nested,
  category_path,
  category_field,
  nested_filter?
) => {
  const array_of_buckets = [];

  categories.map((cat, ind) => {
    array_of_buckets.push({
      bucket_name: regexMatches(TSIPerPerilInQuery, category_field)
        ? `less_${cat}|num_stats`
        : cat.replace(/[\[\]"']/g, '_'),
      query: constructCategorizedStatsBucketQuery(
        path,
        field,
        stats_operation,
        category_field_is_nested,
        category_path,
        category_field,
        cat,
        { nested: nested_filter }
      ),
    });
  });
  return array_of_buckets;
};

const constructCategorizedCountSpread = (
  categories,
  path,
  field,
  stats_operation,
  category_field_is_nested,
  category_path,
  category_field,
  nested_filter?
) => {
  const array_of_buckets = [];

  categories.map((cat, ind) => {
    array_of_buckets.push({
      bucket_name: cat,
      query: constructCategorizedCountBucketQuery(
        path,
        field,
        stats_operation,
        category_field_is_nested,
        category_path,
        category_field,
        cat,
        nested_filter
      ),
    });
  });
  return array_of_buckets;
};

const generateRangeValuesFromList = (
  first: number,
  last: number | null,
  multiply_by_factor = 1
): {
  gte: number;
  lt?: number;
} => {
  if (last !== null) {
    return {
      gte: first * multiply_by_factor,
      lt: last * multiply_by_factor,
    };
  }
  return {
    gte: first * multiply_by_factor,
    lt: null,
  };
};

const generateRangeFilterFromRange = (range, numerical_field) => {
  let filter;
  const { min: gt, max: lte } = range;

  const is_nil = gt + lte === 0;
  if (is_nil) {
    filter = {
      bool: {
        should: [
          {
            range: {
              [numerical_field]: {
                lte: 0,
              },
            },
          },
          {
            bool: {
              must_not: [
                {
                  bool: {
                    filter: {
                      exists: {
                        field: numerical_field,
                      },
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    };
  } else {
    filter = {
      range: {
        [numerical_field]: {
          lte: lte,
          [gt === 0 ? 'gte' : 'gt']: gt,
        },
      },
    };
  }

  return {
    lte: lte,
    filter: filter,
  };
};

const constructAdditionalNumericalHistoStatsBuckets = (opts) => {
  const { numerical_path, numerical_field, steps, stats_path, stats_field } =
    opts;

  const array_of_buckets = [];
  const generated_steps = getRangesFromSteps(steps);

  generated_steps.map((range) => {
    let filter_query;

    const { lte, filter } = generateRangeFilterFromRange(
      range,
      numerical_field
    );

    if (stats_path && stats_path !== numerical_path) {
      filter_query = {
        nested: {
          path: numerical_path,
          query: filter,
        },
      };
    } else {
      filter_query = filter;
    }

    if (opts.wrap_in_empty_matchall) {
      array_of_buckets.push({
        bucket_name: `less_${lte}|num_stats`,
        query: {
          filter: emptyBoolMustMatch(),
          aggs: {
            numerical_stats: {
              filter: emptyBoolMustMatch(),
              aggs: {
                aggs: {
                  filter: filter_query,
                  aggs: {
                    aggs: {
                      nested: {
                        path: stats_path || numerical_path,
                      },
                      aggs: {
                        aggs: {
                          stats: {
                            field: stats_field || numerical_field,
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      });
    } else {
      array_of_buckets.push({
        bucket_name: `less_${lte}|num_stats`,
        query: {
          filter: emptyBoolMustMatch(),
          aggs: {
            numerical_stats: {
              filter: emptyBoolMustMatch(),
              aggs: {
                aggs: {
                  nested: {
                    path: stats_path || numerical_path,
                  },
                  aggs: {
                    aggs: {
                      filter: filter_query,
                      aggs: {
                        aggs: {
                          stats: {
                            field: stats_field || numerical_field,
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      });
    }
  });

  return array_of_buckets;
};

const constructSingleNumericalHistogramSpreadQuery = (agg_name, opts) => {
  const {
    filter_path: filter_path,
    filter_field: filter_field,
    filter_value: filter_value,
    numerical_path: numerical_path,
    numerical_field: numerical_field,
    stats_field: stats_field,
    stats_path: stats_path,
    steps: steps,
  } = opts;

  let num_stats = {};
  let value_count_stats = {};

  num_stats = {
    nested: {
      path: numerical_path,
    },
    aggs: {
      aggs: {
        stats: {
          field: numerical_field,
        },
      },
    },
  };

  value_count_stats = {
    nested: {
      path: numerical_path,
    },
    aggs: {
      aggs: {
        value_count: {
          field: numerical_field,
        },
      },
    },
  };

  return {
    aggs: {
      [`histograms_${agg_name}`]: {
        filter: emptyBoolMustMatch(),
        aggs: {
          generic_stats: {
            filter: {
              nested: constructNestedFilter(
                filter_path,
                filter_field,
                filter_value
              ),
            },
            aggs: {
              numerical_stats: num_stats,
              value_count_stats: value_count_stats,
            },
          },
          spread_parent: {
            filter: emptyBoolMustMatch(),
            aggs: {
              spread_parent_reversal: {
                filter: {
                  nested: constructNestedFilter(
                    filter_path,
                    filter_field,
                    filter_value
                  ),
                },
                aggs: {
                  spread: {
                    filter: emptyBoolMustMatch(),
                    aggs: {
                      at_doc_level: {
                        filters: {
                          other_bucket: true,
                          filters: {
                            ...arrayToObject(
                              constructHistogramSpread(
                                'less_',
                                steps,
                                numerical_path,
                                numerical_field
                              ),
                              'bucket_name',
                              'query'
                            ),
                          },
                        },
                      },
                    },
                  },
                  ...arrayToObject(
                    constructAdditionalNumericalHistoStatsBuckets({
                      numerical_path: stats_path || numerical_path,
                      numerical_field: stats_field || numerical_field,
                      steps: steps,
                    }),
                    'bucket_name',
                    'query'
                  ),
                },
              },
            },
          },
        },
      },
    },
  };
};

const constructSingleNumericalHistogramSpreadQueryWithStatsUnderDifferentPath =
  (agg_name, opts) => {
    const {
      filter_path: filter_path,
      filter_field: filter_field,
      filter_value: filter_value,
      numerical_path: numerical_path,
      numerical_field: numerical_field,
      stats_field: stats_field,
      stats_path: stats_path,
      steps: steps,
    } = opts;

    let num_stats = {};
    if (filter_path === numerical_path) {
      num_stats = {
        stats: {
          field: numerical_field,
        },
      };
    } else {
      num_stats = {
        nested: {
          path: numerical_path,
        },
        aggs: {
          aggs: {
            stats: {
              field: numerical_field,
            },
          },
        },
      };
    }

    return {
      aggs: {
        [`histograms_${agg_name}`]: {
          filter: {
            bool: {
              must: {
                ...constructNestedTermQuery(
                  filter_path,
                  filter_field,
                  filter_value
                ),
              },
            },
          },
          aggs: {
            generic_stats: {
              filter: emptyBoolMustMatch(),
              aggs: {
                numerical_stats: num_stats,
              },
            },
            spread_parent: {
              filter: emptyBoolMustMatch(),
              aggs: {
                spread: {
                  filters: {
                    other_bucket: true,
                    filters: {
                      ...arrayToObject(
                        constructHistogramSpread(
                          'less_',
                          steps,
                          numerical_path,
                          numerical_field
                        ),
                        'bucket_name',
                        'query'
                      ),
                    },
                  },
                },
                ...arrayToObject(
                  constructAdditionalNumericalHistoStatsBuckets({
                    numerical_path: numerical_path,
                    numerical_field: numerical_field,
                    stats_field: stats_field,
                    stats_path: stats_path,
                    steps: steps,
                    wrap_in_empty_matchall: true,
                  }),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        },
      },
    };
  };

const constructSingleCategorizedSpreadQuery = (agg_name, options) => {
  const {
    filter_path,
    filter_field,
    filter_value,

    numerical_path,
    numerical_field,

    steps,
    stats_operation,
    category_field_is_nested,
    category_path,
    category_field,
    reverse_nested,
  } = options;
  const nested_filter = constructNestedFilter(
    filter_path,
    filter_field,
    filter_value
  );

  if (reverse_nested) {
    return {
      aggs: {
        [`histograms_${agg_name}`]: {
          spread_parent: {
            filter: {
              bool: {
                must: [
                  {
                    nested: nested_filter,
                  },
                ],
                must_not: regexMatches(pdaInQuery, numerical_field)
                  ? constructMustNotOfValidPDAs().bool.must_not
                  : [],
              },
            },
            aggs: {
              spread: {
                nested: {
                  path: filter_path,
                },
                aggs: {
                  ...arrayToObject(
                    constructCategorizedSpread(
                      steps,
                      numerical_path,
                      numerical_field,
                      stats_operation,
                      category_field_is_nested,
                      category_path,
                      category_field,
                      nested_filter
                    ),
                    'bucket_name',
                    'query'
                  ),
                },
              },
            },
          },
        },
      },
    };
  }

  let global_stats: any = {
    global_stats: {
      nested: {
        path: numerical_path,
      },
      aggs: {
        stats_parent: {
          stats: {
            field: numerical_field,
          },
        },
      },
    },
  };

  // don't use global stats because there is no way to construct the paths
  // since `steps` has not yet been iterated.
  if (typeof numerical_path === 'function') {
    global_stats = {};
  }

  return {
    aggs: {
      [`histograms_${agg_name}`]: {
        filter: {
          bool: {
            must: [
              {
                nested: nested_filter,
              },
            ],
            must_not: regexMatches(pdaInQuery, numerical_field)
              ? constructMustNotOfValidPDAs().bool.must_not
              : [],
          },
        },
        aggs: {
          ...global_stats,
          ...arrayToObject(
            constructCategorizedSpread(
              steps,
              numerical_path,
              numerical_field,
              stats_operation,
              category_field_is_nested,
              category_path,
              category_field,
              nested_filter
            ),
            'bucket_name',
            'query'
          ),
        },
      },
    },
  };
};

const constructSingleCategorizedCountSpreadQuery = (agg_name, options) => {
  const {
    filter_path,
    filter_field,
    filter_value,

    numerical_path,
    numerical_field,

    steps,
    stats_operation,
    category_field_is_nested,
    category_path,
    category_field,
    reverse_nested,
  } = options;
  let nested_spread_filter;

  if (reverse_nested) {
    nested_spread_filter = constructNestedFilter(
      filter_path,
      filter_field,
      filter_value
    );

    return {
      aggs: {
        [`histograms_${agg_name}`]: {
          spread_parent: {
            filter: {
              bool: {
                must: [
                  {
                    nested: nested_spread_filter,
                  },
                ],
                must_not: regexMatches(pdaInQuery, numerical_field)
                  ? constructMustNotOfValidPDAs().bool.must_not
                  : [],
              },
            },
            aggs: {
              spread: {
                nested: {
                  path: filter_path,
                },
                aggs: {
                  ...arrayToObject(
                    constructCategorizedCountSpread(
                      steps,
                      numerical_path,
                      numerical_field,
                      stats_operation,
                      category_field_is_nested,
                      category_path,
                      category_field,
                      nested_spread_filter
                    ),
                    'bucket_name',
                    'query'
                  ),
                },
              },
            },
          },
        },
      },
    };
  }

  nested_spread_filter = constructNestedFilter(
    filter_path,
    filter_field,
    filter_value
  );

  return {
    aggs: {
      [`histograms_${agg_name}`]: {
        filter: {
          bool: {
            must: [
              {
                nested: nested_spread_filter,
              },
            ],
            must_not: regexMatches(pdaInQuery, numerical_field)
              ? constructMustNotOfValidPDAs().bool.must_not
              : [],
          },
        },
        aggs: {
          global_stats: {
            nested: {
              path: numerical_path,
            },
            aggs: {
              stats_parent: {
                stats: {
                  field: numerical_field,
                },
              },
            },
          },
          ...arrayToObject(
            constructCategorizedSpread(
              steps,
              numerical_path,
              numerical_field,
              stats_operation,
              category_field_is_nested,
              category_path,
              category_field,
              nested_spread_filter
            ),
            'bucket_name',
            'query'
          ),
        },
      },
    },
  };
};

export const provideBusinessActivityBuckets = (klass: string) => {
  const { LoB_Multiselect: lobs, Industry_Multiselect: industries } =
    getValuesOfAffectingFilters((window as any).app, klass, true);

  if (!lobs.length || !industries.length) {
    return [];
  }

  let list_of_bucket_categories = [];
  lobs.map((lob) => {
    industries.map((indu) => {
      const to_append = LobAndIndustryBasedBusinessActivityBreakdown[lob][indu];
      if (to_append) {
        list_of_bucket_categories = list_of_bucket_categories.concat(to_append);
      }
    });
  });

  return sortArrOfStringsAlphabetically(uniq(list_of_bucket_categories));
};

const provideNaceCodesBuckets = (klass) => {
  const { NaceCodesStandalone_Multiselect: naces } =
    getValuesOfAffectingFilters((window as any).app, klass, true);

  if (!naces.length) {
    return sortArrOfStringsAlphabetically(Object.keys(NaceCodes)).map((nc) =>
      stringifyNumericalNaceCode(nc)
    );
  }

  return sortArrOfStringsAlphabetically(naces).map((nc) =>
    stringifyNumericalNaceCode(nc)
  );
};

const provideRegionalDirectorateBuckets = (klass) => {
  const { RegionOfOrigin_Multiselect: regions } = getValuesOfAffectingFilters(
    (window as any).app,
    klass,
    true
  );

  if (!regions.length) {
    return sortArrOfStringsAlphabetically(listOfRegionsOfOrigin).filter(
      (el) => el.length > 0
    );
  }

  return sortArrOfStringsAlphabetically(regions).filter((el) => el.length > 0);
};

const provideSalesPartnersBuckets = (klass) => {
  const { SalesPartner_Multiselect: salesPartners } =
    getValuesOfAffectingFilters((window as any).app, klass, true);

  if (!salesPartners.length) {
    return sortArrOfStringsAlphabetically(
      Object.keys((window as any).mappers.SalesPartner || {})
    )
      .filter((el) => el.length > 0)
      .slice(0, 1200);
  }

  return sortArrOfStringsAlphabetically(salesPartners)
    .filter((el) => el.length > 0)
    .slice(0, 1200);
};

const top_address_hits_agg = {
  top: {
    nested: {
      path: 'lob.addresses',
    },
    aggs: {
      top: {
        top_hits: {
          size: 100,
          _source: 'address.id',
        },
      },
    },
  },
};

let query_names, numerical_aggs;

const REM_base_metrics_exports = (): {
  query_names: object;
  numerical_aggs: object;
} => {
  const q_names = {
    '1)_number_of_lobs': '1)_number_of_lobs',
    '2)_accumulated_UNIQA_PML': '2)_accumulated_UNIQA_PML',
    '3)_accumulated_liability_sum_insured':
      '3)_accumulated_liability_sum_insured',
    '4)_accumulated_premium_calculation_basis':
      '4)_accumulated_premium_calculation_basis',
  };

  const n_aggs = {
    [q_names['1)_number_of_lobs']]: {
      // nested: {
      //   path: 'lob'
      // },
      // aggs: {
      //   inside: {
      //     stats: {
      //       field: 'lob.id'
      //     }
      //   },
      //   top: {
      //     top_hits: {
      //       size: 100,
      //       _source: 'lob.id'
      //     }
      //   }
      // }

      // nested: {
      //   path: 'lob.addresses'
      // },
      // aggs: {
      //   inside: {
      //     stats: {
      //       field: 'lob.addresses.id'
      //     }
      //   }
      // }

      ...constructScriptedMetricNestedCardinalityQuery('inside', 'lob', 'id'),
    },
    [q_names['2)_accumulated_UNIQA_PML']]: {
      nested: {
        path: 'lob',
      },
      aggs: {
        inside: {
          stats: {
            field: `lob.uniqaPML_$CURRENCY$`,
          },
        },
      },
    },
    [q_names['3)_accumulated_liability_sum_insured']]: {
      nested: {
        path: 'lob',
      },
      aggs: {
        inside: {
          stats: {
            field: 'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',
          },
        },
      },
    },
    [q_names['4)_accumulated_premium_calculation_basis']]: {
      nested: {
        path: 'lob.risks',
      },
      aggs: {
        inside: {
          stats: {
            field: 'lob.risks.premiumBasis_$CURRENCY$_AdjustedForShare',
          },
        },
      },
    },
  };

  return {
    query_names: q_names,
    numerical_aggs: n_aggs,
  };
};

const REM_evaluation_query_basis = (factor_kind, criterion_kind?) => {
  const loc_peril = (window as any).selected_location_peril;
  const paths_and_fields = {
    path: `address.locationProperty.locationAssessmentsLevel3.${loc_peril}.keyFactors.${factor_kind}.keyCriteria.${criterion_kind}`,
    name_field: `address.locationProperty.locationAssessmentsLevel3.${loc_peril}.keyFactors.${factor_kind}.keyCriteria.${criterion_kind}.name.keyword`,
    score_field: `address.locationProperty.locationAssessmentsLevel3.${loc_peril}.keyFactors.${factor_kind}.keyCriteria.${criterion_kind}.score`,
    weight_field: `address.locationProperty.locationAssessmentsLevel3.${loc_peril}.keyFactors.${factor_kind}.keyCriteria.${criterion_kind}.weight`,
  };

  const as_dict = {
    '1)_number_of_evaluations': '1)_number_of_evaluations',
    '2)_average_score': '2)_average_score',
    '3)_average_weight': '3)_average_weight',
  };

  return {
    path_and_field_KeyCriterion: paths_and_fields,
    query_names_as_dict: as_dict,
    numerical_aggs: () => {
      return {
        [as_dict['1)_number_of_evaluations']]: {
          nested: {
            path: paths_and_fields.path,
          },
          aggs: {
            inside: {
              value_count: {
                field: paths_and_fields.name_field,
              },
            },
          },
        },
        [as_dict['2)_average_score']]: {
          nested: {
            path: paths_and_fields.path,
          },
          aggs: {
            inside: {
              stats: {
                field: paths_and_fields.score_field,
              },
            },
          },
        },
        [as_dict['3)_average_weight']]: {
          nested: {
            path: paths_and_fields.path,
          },
          aggs: {
            inside: {
              stats: {
                field: paths_and_fields.weight_field,
              },
            },
          },
        },
      };
    },
  };
};

// const REM_base_metrics_filter_exists_queries = [
//   constructNestedExistsQuery('lob.addresses', 'lob.addresses.id'),
//   constructNestedExistsQuery('lob', 'lob.uniqaPML_$CURRENCY$'),
//   constructNestedExistsQuery(
//     'lob',
//     'lob.totalSumInsured_$CURRENCY$_AdjustedForShare'
//   ),
//   constructNestedExistsQuery('lob', 'lob.premium_$CURRENCY$'),
// ];

export const histogramQueryConstructors = (
  kind,
  date_field_queries = [],
  theAppComponent: AppComponent = null
) => {
  const q = {
    aggs: {},
    size: 0,
    _source: {},
    query: {},
  };
  let queries: Array<any>;
  let LoBs,
    include_source,
    path_and_field_KeyCriterion,
    query_names_as_dict,
    query_dict_addr_based,
    query_dict;

  type pickerMinus1yearReturnType = ReturnType<
    RelativeDatepickerComponent['minus1year']
  >;
  let baseline_start: pickerMinus1yearReturnType['start'],
    baseline_end: pickerMinus1yearReturnType['end'],
    baseline_format: pickerMinus1yearReturnType['format'],
    comparison: pickerMinus1yearReturnType['comparison'];
  let baselineQuery: ReturnType<typeof constructNestedDateRangeQuery>;
  let comparisonQuery: ReturnType<typeof constructNestedDateRangeQuery>;
  let minus1yearRangeSpecs: ReturnType<
    AppComponent['getMins1YsoleActiveDatePickerComponent']
  >;

  LoBs = listOfLobs;

  const constructYoYRangeComparisonQueries = (
    active_picker_component_specs: typeof minus1yearRangeSpecs
  ): {
    baselineQuery: typeof baselineQuery | any;
    comparisonQuery: typeof comparisonQuery | any;
  } => {
    const field_access_specs = active_picker_component_specs.picker_fields;

    ({
      start: baseline_start,
      end: baseline_end,
      format: baseline_format,
      comparison,
    } = active_picker_component_specs.minus1yearValues);

    if (field_access_specs.gte && field_access_specs.lte) {
      return {
        baselineQuery: {
          bool: {
            must: [
              constructNestedDateRangeQuery(
                field_access_specs.gte.path,
                field_access_specs.gte.field,
                {
                  // gte only!
                  gte: baseline_start,
                  format: baseline_format,
                }
              ),
              constructNestedDateRangeQuery(
                field_access_specs.lte.path,
                field_access_specs.lte.field,
                {
                  // lte only!
                  lte: baseline_end,
                  format: baseline_format,
                }
              ),
            ],
          },
        },
        comparisonQuery: {
          bool: {
            must: [
              constructNestedDateRangeQuery(
                field_access_specs.gte.path,
                field_access_specs.gte.field,
                {
                  // gte only!
                  gte: comparison.start,
                  format: comparison.format,
                }
              ),
              constructNestedDateRangeQuery(
                field_access_specs.lte.path,
                field_access_specs.lte.field,
                {
                  // lte only!
                  lte: comparison.end,
                  format: comparison.format,
                }
              ),
            ],
          },
        },
      };
    }

    return {
      baselineQuery: constructNestedDateRangeQuery(
        field_access_specs.path,
        field_access_specs.field,
        {
          gte: baseline_start,
          lte: baseline_end,
          format: baseline_format,
        }
      ),
      comparisonQuery: constructNestedDateRangeQuery(
        field_access_specs.path,
        field_access_specs.field,
        {
          gte: comparison.start,
          lte: comparison.end,
          format: comparison.format,
        }
      ),
    };
  };

  switch (kind) {
    case 'ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndRiskCode_histogram_chart': {
      const categories = (window as any).selected_esg_criterion && [(window as any).selected_esg_criterion] || LISTS_OF_HISTOGRAM_CATEGORIES.listOfEsgCriterias;
      const riskCodes = (window as any).selected_esg_riskCode || [0, 1, 2, 3]
      queries = categories.map(category => {
        const riskCodeAggregations = riskCodes.map((riskCode, index) =>
          EB.filterAggregation(`Risk Code ${riskCode}`,
            EB.nestedQuery(
              EB.boolQuery()
                .must([
                  EB.termQuery("offer.client.esg.criteria.riskCode.keyword", riskCode),
                  EB.termQuery("offer.client.esg.criteria.shortName.keyword", category)
                ]), "offer.client.esg.criteria"))
            .agg(EB.nestedAggregation("Risk_Code_Count", "offer.client")
              .agg(EB.cardinalityAggregation(`Risk Code ${riskCode} count`, 'offer.client.id')
                .field('offer.client.id')
                .meta({
                  columnName: `Risk ${riskCode} • No. of clients`,
                  columnIndex: (index * 2) + 3,
                  formatter: AGG_VALUE_FORMATTERS_ENUM.IDENTITY
                })
              )
            )
            .agg(EB.nestedAggregation("Risk_Code_Sum", "offer")
              .agg(EB.sumAggregation(`Risk Code ${riskCode} sum`, 'offer.premiumInEur')
                .field('offer.premiumInEur')
                .meta({
                  columnName: `Risk ${riskCode} • Uniqa total premium`,
                  columnIndex: (index * 2) + 4,
                  formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY
                })
              )
            )
        );

        const riskCodeNa = EB.filterAggregation(
          "risk_code_n_a",
          EB.boolQuery()
            .should([
              EB.boolQuery().mustNot(EB.nestedQuery(EB.existsQuery('offer.client.esg.criteria'), "offer.client.esg.criteria")),
              EB.boolQuery().mustNot(EB.nestedQuery(EB.existsQuery('offer.client.esg.valuationId'), "offer.client")),
              EB.boolQuery().mustNot(EB.nestedQuery(
                EB.termQuery(
                  'offer.client.esg.criteria.shortName.keyword',
                  category
                ), "offer.client.esg.criteria"))
            ]))
          .agg(EB.nestedAggregation("Risk_Code_Count", "offer.client")
            .agg(EB.cardinalityAggregation(`Risk Code N/A count`, 'offer.client.id')
              .field('offer.client.id')
              .meta({
                columnName: `Risk N/A • No. of clients`,
                columnIndex: 1,
                formatter: AGG_VALUE_FORMATTERS_ENUM.IDENTITY
              })
            )
          )
          .agg(EB.nestedAggregation("Risk_Code_Sum", "offer")
            .agg(EB.sumAggregation(`Risk Code N/A sum`, 'offer.premiumInEur')
              .field('offer.premiumInEur')
              .meta({
                columnName: `Risk N/A • Uniqa total premium`,
                columnIndex: 2,
                formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY
              })
            )
          )


        return {
          size: 0,
          aggs: EB.filterAggregation(category, EB.matchAllQuery())
            .aggs([...riskCodeAggregations, riskCodeNa]).toJSON()
        }
      })
      break;
    }
    case 'ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndNaceCode_histogram_chart': {
      const categories = (window as any).selected_esg_criterion && [(window as any).selected_esg_criterion] || LISTS_OF_HISTOGRAM_CATEGORIES.listOfEsgCriterias;
      const riskCodes = (window as any).selected_esg_riskCode || [1, 2, 3]
      queries = categories.map((criteria) => ({
        size: 0,
        aggs: EB.filterAggregation(criteria, EB.nestedQuery(EB.boolQuery().should(
          riskCodes.map((riskCode) => EB.boolQuery().must([
            EB.termQuery("offer.client.esg.criteria.shortName.keyword", criteria),
            EB.termQuery("offer.client.esg.criteria.riskCode.keyword", riskCode)
          ]))
        ).minimumShouldMatch(1), 'offer.client.esg.criteria'),).agg(EB.nestedAggregation("byNace", "offer.client")
          .agg(EB.termsAggregation("byNace", "offer.client.nacePrimaryCodes").size(10e5)
            .agg(EB.reverseNestedAggregation("byNaceNested", "offer")
              .agg(EB.sumAggregation("byNaceSum", "offer.premiumInEur"))
            ).agg(EB.cardinalityAggregation("byNaceClient", "offer.client.id")
            )

          )
        ).toJSON()
      }))

      break;
    }
    case 'ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndOccupancy_histogram_chart': {
      const categories = (window as any).selected_esg_criterion && [(window as any).selected_esg_criterion] || LISTS_OF_HISTOGRAM_CATEGORIES.listOfEsgCriterias;
      const riskCodes = (window as any).selected_esg_riskCode || [1, 2, 3]
      queries = categories.map((criteria) => ({
        size: 0,
        aggs: EB.filterAggregation(criteria, EB.nestedQuery(EB.boolQuery().should(
          riskCodes.map((riskCode) => EB.boolQuery().must([
            EB.termQuery("offer.client.esg.criteria.shortName.keyword", criteria),
            EB.termQuery("offer.client.esg.criteria.riskCode.keyword", riskCode)
          ]))
        ).minimumShouldMatch(1), 'offer.client.esg.criteria')).agg(EB.nestedAggregation("byLob", "lob")
          .agg(EB.termsAggregation("byTob", "lob.typeOfBusiness").size(10e5)
            .agg(EB.reverseNestedAggregation("byTobNested", "offer")
              .agg(EB.sumAggregation("byTobSum", "offer.premiumInEur"))
            ).agg(EB.reverseNestedAggregation("byTobNestedClient", "offer.client").agg(EB.cardinalityAggregation("byTobClient", "offer.client.id"))
            )

          )
        ).toJSON()
      }))

      break;
    }

    case 'ESG_AggregatedEmissionsPerOccupancy_histogram_chart':
      queries = [
        {
          "size": 0,
          "query": constructNestedExistsQuery("offer.client", "offer.client.co2"),
          "aggs": {
            ...EB.nestedAggregation('byTypeOfBusiness', 'lob')
              .agg(EB.termsAggregation('byTypeOfBusinessInner', 'lob.typeOfBusiness').size(10e5)
                .agg(EB.nestedAggregation('byIntensityTotalPremium', 'offer.client')
                  .agg(EB.reverseNestedAggregation('byIntensityTotalPremium', 'offer')
                    .agg(EB.sumAggregation('byIntensityTotalPremiumInner', 'offer.premiumInEur')
                      .meta({
                        columnIndex: 2,
                        columnName: 'Uniqa total premium',
                        formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY
                      }))
                  )
                  .agg(EB.cardinalityAggregation('byIntensityTotalClientsInner', 'offer.client.id')
                    .meta({
                      columnIndex: 1,
                      columnName: 'No. of Clients'
                    }))
                  .agg(EB.sumAggregation('byEmissionsInner', 'offer.client.co2.emission').missing("0")
                    .meta({
                      columnIndex: 3,
                      columnName: 'Emissions (tCO₂)',
                      formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_FLOAT
                    }))
                  .agg(EB.sumAggregation('byIntensityInner', 'offer.client.co2.carbonIntensity').missing("0")
                    .meta({
                      columnIndex: 4,
                      columnName: 'Carbon intensity (tCO₂ / €1Mio)',
                      formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_FLOAT
                    }))
                ))
              .toJSON(),
            ...EB.globalAggregation('totals')
              .agg(EB.nestedAggregation("inner", 'offer.client')
                .agg(EB.sumAggregation('emissions', 'offer.client.co2.emission').missing("0"))
                .agg(EB.sumAggregation('intensity', 'offer.client.co2.carbonIntensity').missing("0"))
              ).toJSON()
          }
        }
      ]
      break;
    case 'ESG_AggregatedEmissionsPerNaceCode_histogram_chart':
      queries = [
        {
          "size": 0,
          "query": constructNestedExistsQuery("offer.client", "offer.client.co2"),
          "aggs": {
            ...EB.nestedAggregation('byNacePrimaryCodes', 'offer.client')
              .agg(EB.termsAggregation('byNacePrimaryCodesInner', 'offer.client.nacePrimaryCodes').size(10e5)
                .agg(EB.reverseNestedAggregation('byIntensityTotalPremium', 'offer')
                  .agg(EB.sumAggregation('byIntensityTotalPremiumInner', 'offer.premiumInEur')
                    .meta({
                      columnIndex: 2,
                      columnName: 'Uniqa total premium',
                      formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY
                    }))
                )
                .agg(EB.cardinalityAggregation('byIntensityTotalClientsInner', 'offer.client.id')
                  .meta({
                    columnIndex: 1,
                    columnName: 'No. of Clients'
                  }))
                .agg(EB.sumAggregation('byEmissionsInner', 'offer.client.co2.emission').missing("0")
                  .meta({
                    columnIndex: 3,
                    columnName: 'Emissions (tCO₂)',
                    formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_FLOAT
                  }))
                .agg(EB.sumAggregation('byIntensityInner', 'offer.client.co2.carbonIntensity').missing("0")
                  .meta({
                    columnIndex: 4,
                    columnName: 'Carbon intensity (tCO₂ / €1Mio)',
                    formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_FLOAT
                  }))
              )
              .toJSON(),
            ...EB.globalAggregation('totals')
              .agg(EB.nestedAggregation("inner", 'offer.client')
                .agg(EB.sumAggregation('emissions', 'offer.client.co2.emission').missing("0"))
                .agg(EB.sumAggregation('intensity', 'offer.client.co2.carbonIntensity').missing("0"))
              ).toJSON()
          }
        }
      ]
      break;
    case 'AggregatedTotalPremiumPerIndustry_histogram_chart':
      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob',
          numerical_field: 'lob.premium_$CURRENCY$',

          steps: listOfIndustryCategories,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: 'offer',
          category_field: 'offer.industry',

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedLOBCountPerIndustry_histogram_chart':
      queries = LoBs.map((lob) => {
        return constructSingleCategorizedCountSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob',
          numerical_field: 'lob.id',

          steps: listOfIndustryCategories,
          stats_operation: 'stats',

          category_field_is_nested: true,
          category_path: 'offer',
          category_field: 'offer.industry',

          reverse_nested: false,
        });
      });

      break;

    case 'AggregatedTotalSumInsuredPerIndustry_histogram_chart':
      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob',
          numerical_field: 'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',

          steps: listOfIndustryCategories,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: 'offer',
          category_field: 'offer.industry',

          reverse_nested: false,
        });
      });

      break;

    case 'AggregatedPremiumsVsTotalSumInsuredPerIndustry_histogram_chart':
      queries = LoBs.map((lob) => {
        return {
          aggs: {
            [`histograms_${lob}`]: {
              filter: {
                bool: {
                  must: [
                    constructNestedTermQuery('lob', 'lob.lineOfBusiness', lob),
                  ],
                },
              },
              aggs: {
                global_stats: {
                  nested: {
                    path: 'lob',
                  },
                  aggs: {
                    stats_parent: {
                      ...constructScriptedMetricRatioOfSumsAggregation(
                        'lob.id',
                        'lob.premium_$CURRENCY$',
                        'lob.totalSumInsured_$CURRENCY$_AdjustedForShare'
                      ),
                    },
                  },
                },
                ...arrayToObject(
                  listOfIndustryCategories.map((industry) => {
                    return {
                      agg_name: industry,
                      agg_itself: {
                        filter: {
                          bool: {
                            must: [
                              constructNestedTermQuery(
                                'offer',
                                'offer.industry',
                                industry
                              ),
                              constructNestedTermQuery(
                                'lob',
                                'lob.lineOfBusiness',
                                lob
                              ),
                            ],
                          },
                        },
                        aggs: {
                          sum_operation: {
                            nested: {
                              path: 'lob',
                            },
                            aggs: {
                              sum: constructScriptedMetricRatioOfSumsAggregation(
                                'lob.id',
                                'lob.premium_$CURRENCY$',
                                'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',
                                true
                              ),
                            },
                          },
                        },
                      },
                    };
                  }),
                  'agg_name',
                  'agg_itself'
                ),
              },
            },
          },
        };
      });

      break;

    case 'AggregatedLocationsCountPerIndustry_histogram_chart':
      queries = LoBs.map((lob) => {
        return constructSingleCategorizedCountSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob.addresses',
          numerical_field: 'lob.addresses.id',

          steps: listOfIndustryCategories,
          stats_operation: 'stats',

          category_field_is_nested: true,
          category_path: 'offer',
          category_field: 'offer.industry',

          reverse_nested: false,
        });
      });

      break;
    // END Categorized Aggs (per Industry)
    // BEGIN Business Activity

    case 'PPM_GlobalStatsPerBusinessActivity_NoCanvas_histogram_chart':
      {
        const queryRows: Array<{
          rowName: string;
          industry: string;
          typeOfBusiness: string;
        }> = flattenArrays(
          (window as any).mappers?.industries_vs_typesOfBusinesses?.map(
            (group: { industry: string; typesOfBusiness: string[] }) => {
              const { industry, typesOfBusiness } = group;

              return typesOfBusiness.map((biz) => {
                return {
                  // the baseline is the industry; the subsequent column is the ToB
                  rowName: `${industry} : ${biz}`,
                  industry,
                  typeOfBusiness: biz,
                };
              });
            }
          )
        );

        queries = constructAggregationQuery({
          row_names: queryRows.map(({ rowName }) => rowName),
          row_queries: queryRows.map((group, row_index) => {
            const { industry, typeOfBusiness } = group;

            // covering both the industry & the ToB
            const row_query = {
              bool: {
                must: [
                  constructNestedTermQuery('offer', 'offer.industry', industry),
                  constructNestedTermQuery(
                    'lob',
                    'lob.typeOfBusiness',
                    typeOfBusiness
                  ),
                ],
              },
            };

            const rowName = queryRows[row_index].rowName;

            return {
              disabled: false,
              agg_name: rowName,
              meta: {
                row_index,
                row_name: industry,
                category_column_name: 'Industry',
              },
              filter: row_query,
              aggs_themselves: {
                'Type of Business': {
                  agg_name: 'Type of Business',
                  meta: {
                    column: {
                      index: 1,
                      name: 'Type of Business',
                      presented_agg_name: 'type_of_business',
                    },
                    post_processing: {
                      type_of_business: {
                        value: {
                          formatter:
                            AGG_VALUE_FORMATTERS_ENUM.NORMALIZE_TYPE_OF_BUSINESS,
                          // access the first bucket's key               ---- vvv
                          accessor:
                            'type_of_business.type_of_business.buckets.0.key',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      type_of_business: nestedTermsAggregation(
                        'type_of_business',
                        'lob',
                        'lob.typeOfBusiness',
                        {
                          // Make sure to only retain the row's ToB.
                          // Note that wrapping this in an array targets exact values
                          // as opposed to regex if left as a string.
                          // https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-terms-aggregation.html#_filtering_values_with_exact_values_2
                          include: [typeOfBusiness],
                        }
                      ),
                    },
                  },
                },
                'LoB Count': {
                  agg_name: 'Lob Count',
                  meta: {
                    column: {
                      index: 2,
                      name: 'LoB Count',
                      presented_agg_name: 'lob_count',
                    },
                    post_processing: {
                      lob_count: {
                        value: {
                          formatter:
                            AGG_VALUE_FORMATTERS_ENUM.IDENTITY_NAN_IS_ZERO,
                          accessor: 'lob_count.inside.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      lob_count: constructScriptedMetricNestedCardinalityQuery(
                        'inside',
                        'lob',
                        'id'
                      ),
                    },
                  },
                },
                'TSI Sum': {
                  agg_name: 'TSI Sum',
                  meta: {
                    column: {
                      index: 3,
                      name: 'TSI Sum',
                      presented_agg_name: 'total_sum_insured',
                    },
                    post_processing: {
                      total_sum_insured: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'total_sum_insured.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      total_sum_insured: nestedStatsAggregation(
                        'lob',
                        'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',
                        'sum'
                      ),
                    },
                  },
                },
                'TSI Avg': {
                  agg_name: 'TSI Avg',
                  meta: {
                    column: {
                      index: 4,
                      name: 'TSI Avg',
                      presented_agg_name: 'avg_sum_insured',
                    },
                    post_processing: {
                      avg_sum_insured: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'avg_sum_insured.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      avg_sum_insured: nestedStatsAggregation(
                        'lob',
                        'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',
                        'avg'
                      ),
                    },
                  },
                },
                'TSI Median': {
                  agg_name: 'TSI Median',
                  meta: {
                    column: {
                      index: 5,
                      name: 'TSI Median',
                      presented_agg_name: 'median_tsi',
                    },
                    post_processing: {
                      median_tsi: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          // Gotta use a split path for the accessor here because of the dot character in `50.0`.
                          // More examples here: https://github.com/mariocasciaro/object-path#usage
                          accessor: [
                            'median_tsi',
                            'median_agg',
                            'median_agg_deepest',
                            'values',
                            '50.0',
                          ],
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      median_tsi: constructMedianAgg(
                        'lob',
                        'lob.totalSumInsured_$CURRENCY$_AdjustedForShare'
                      ),
                    },
                  },
                },
                'Premium Sum': {
                  agg_name: 'Premium Sum',
                  meta: {
                    column: {
                      index: 6,
                      name: 'Premium Sum',
                      presented_agg_name: 'total_premium',
                    },
                    post_processing: {
                      total_premium: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'total_premium.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      total_premium: nestedStatsAggregation(
                        'lob',
                        'lob.premium_$CURRENCY$',
                        'sum'
                      ),
                    },
                  },
                },
                'Premium Avg': {
                  agg_name: 'Premium Avg',
                  meta: {
                    column: {
                      index: 7,
                      name: 'Premium Avg',
                      presented_agg_name: 'avg_premium',
                    },
                    post_processing: {
                      avg_premium: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'avg_premium.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      avg_premium: nestedStatsAggregation(
                        'lob',
                        'lob.premium_$CURRENCY$',
                        'avg'
                      ),
                    },
                  },
                },
                'Premium Median': {
                  agg_name: 'Premium Median',
                  meta: {
                    column: {
                      index: 8,
                      name: 'Premium Median',
                      presented_agg_name: 'median_premium',
                    },
                    post_processing: {
                      median_premium: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          // Gotta use a split path for the accessor here because of the dot character in `50.0`.
                          // More examples here: https://github.com/mariocasciaro/object-path#usage
                          accessor: [
                            'median_premium',
                            'median_agg',
                            'median_agg_deepest',
                            'values',
                            '50.0',
                          ],
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      median_premium: constructMedianAgg(
                        'lob',
                        'lob.premium_$CURRENCY$'
                      ),
                    },
                  },
                },
              },
            };
          }),
        });
      }
      break;
    case 'AggregatedTotalPremiumPerBusinessActivity_histogram_chart':
      const unique_buckets_AggregatedTotalPremiumPerBusinessActivity_histogram_chart =
        provideBusinessActivityBuckets(
          'AggregatedTotalPremiumPerBusinessActivity_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob',
          numerical_field: 'lob.premium_$CURRENCY$',

          steps:
            unique_buckets_AggregatedTotalPremiumPerBusinessActivity_histogram_chart,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: 'lob',
          category_field: 'lob.typeOfBusiness',

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedTotalOffersPerBusinessActivity_histogram_chart':
      const unique_buckets_AggregatedTotalOffersPerBusinessActivity_histogram_chart =
        provideBusinessActivityBuckets(
          'AggregatedTotalOffersPerBusinessActivity_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob',
          numerical_field: 'lob.id',

          steps:
            unique_buckets_AggregatedTotalOffersPerBusinessActivity_histogram_chart,
          stats_operation: 'stats',

          category_field_is_nested: true,
          category_path: 'lob',
          category_field: 'lob.typeOfBusiness',

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedTotalSumInsuredPerBusinessActivity_histogram_chart':
      const unique_buckets_AggregatedTotalSumInsuredPerBusinessActivity_histogram_chart =
        provideBusinessActivityBuckets(
          'AggregatedTotalSumInsuredPerBusinessActivity_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob',
          numerical_field: 'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',

          steps:
            unique_buckets_AggregatedTotalSumInsuredPerBusinessActivity_histogram_chart,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: 'lob',
          category_field: 'lob.typeOfBusiness',

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedLocationsPerBusinessActivity_histogram_chart':
      const unique_buckets_AggregatedLocationsPerBusinessActivity_histogram_chart =
        provideBusinessActivityBuckets(
          'AggregatedLocationsPerBusinessActivity_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob.addresses',
          numerical_field: 'lob.addresses.id',

          steps:
            unique_buckets_AggregatedLocationsPerBusinessActivity_histogram_chart,
          stats_operation: 'stats',

          category_field_is_nested: true,
          category_path: 'lob',
          category_field: 'lob.typeOfBusiness',

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedPremiumsVsTotalSumInsuredPerBusinessActivity_histogram_chart':
      const unique_buckets_AggregatedPremiumsVsTotalSumInsuredPerBusinessActivity_histogram_chart =
        provideBusinessActivityBuckets(
          'AggregatedPremiumsVsTotalSumInsuredPerBusinessActivity_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return {
          aggs: {
            [`histograms_${lob}`]: {
              filter: {
                bool: {
                  must: [
                    constructNestedTermQuery('lob', 'lob.lineOfBusiness', lob),
                  ],
                },
              },
              aggs: {
                global_stats: {
                  nested: {
                    path: 'lob',
                  },
                  aggs: {
                    stats_parent: {
                      ...constructScriptedMetricRatioOfSumsAggregation(
                        'lob.id',
                        'lob.premium_$CURRENCY$',
                        'lob.totalSumInsured_$CURRENCY$_AdjustedForShare'
                      ),
                    },
                  },
                },
                ...arrayToObject(
                  unique_buckets_AggregatedPremiumsVsTotalSumInsuredPerBusinessActivity_histogram_chart.map(
                    (b_activity) => {
                      return {
                        agg_name: b_activity,
                        agg_itself: {
                          filter: {
                            bool: {
                              must: [
                                constructNestedTermQuery(
                                  'lob',
                                  'lob.typeOfBusiness',
                                  b_activity
                                ),
                                constructNestedTermQuery(
                                  'lob',
                                  'lob.lineOfBusiness',
                                  lob
                                ),
                              ],
                            },
                          },
                          aggs: {
                            sum_operation: {
                              nested: {
                                path: 'lob',
                              },
                              aggs: {
                                sum: constructScriptedMetricRatioOfSumsAggregation(
                                  'lob.id',
                                  'lob.premium_$CURRENCY$',
                                  'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',
                                  true
                                ),
                              },
                            },
                          },
                        },
                      };
                    }
                  ),
                  'agg_name',
                  'agg_itself'
                ),
              },
            },
          },
        };
      });

      break;
    // END Business Activity
    // BEGIN Nace Codes
    case 'PPM_GlobalStatsPerNaceCode_NoCanvas_histogram_chart':
      {
        const queryRows: Array<{
          rowName: string;
          rawCode: string | null;
          humanReadableCode: string;
        }> = flattenArrays(
          sortByExtractingNumbersFromStrings(
            (window as any).mappers?.NACECodes || []
          )?.map((code: string) => {
            const destringifiedCode =
              destringifySemiNumericalCategoryName(code);

            return {
              rowName: code,
              rawCode: code,
              humanReadableCode:
                destringifiedCode in NaceCodes
                  ? NaceCodes[destringifiedCode]
                  : '[NOT IMPLEMENTED]',
            };
          })
        ).concat([
          {
            rowName: 'Empty',
            rawCode: null,
            humanReadableCode: 'Empty',
          },
        ]);

        queries = constructAggregationQuery({
          row_names: queryRows.map(({ rowName }) => rowName),
          row_queries: queryRows.map((group, row_index) => {
            const { rawCode: code, humanReadableCode, rowName } = group;

            // covering both defined & non-defined codes
            const row_query = {
              bool: {
                must: [
                  code
                    ? constructNestedTermQuery(
                      'offer.client',
                      'offer.client.nacePrimaryCodes',
                      code
                    )
                    : // cover empty / non-available nace codes too
                    {
                      bool: {
                        must_not: [
                          constructNestedExistsQuery(
                            'offer.client',
                            'offer.client.nacePrimaryCodes'
                          ),
                        ],
                      },
                    },
                ],
              },
            };

            return {
              disabled: false,
              agg_name: rowName,
              meta: {
                row_index,
                // The nace code map may include `;` and other problematic chars
                // so make sure the cell values are normalized
                row_name:
                  AGG_VALUE_FORMATTERS[
                    AGG_VALUE_FORMATTERS_ENUM.NORMALIZE_NACE_CODE
                  ](humanReadableCode),
                category_column_name: 'NACE Code Interpretation',
              },
              filter: row_query,
              aggs_themselves: {
                'NACE Primary Code': {
                  agg_name: 'NACE Primary Code',
                  meta: {
                    column: {
                      index: 1,
                      name: 'NACE Primary Code',
                      presented_agg_name: 'nace_code',
                    },
                    post_processing: {
                      nace_code: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.IDENTITY,
                          accessor: 'nace_code.nace_code.buckets.0.key',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      nace_code: nestedTermsAggregation(
                        'nace_code',
                        'offer.client',
                        'offer.client.nacePrimaryCodes',
                        code
                          ? {
                            // Make sure to only retain the row's NACE code.
                            // Note that wrapping this in an array targets exact values
                            // as opposed to regex if left as a string.
                            // https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-terms-aggregation.html#_filtering_values_with_exact_values_2
                            include: [code],
                          }
                          : // do NOT include any code if it's empty because ES will throw the error:
                          // `Array elements in include/exclude clauses should be string values`
                          {}
                      ),
                    },
                  },
                },
                'LoB Count': {
                  agg_name: 'Lob Count',
                  meta: {
                    column: {
                      index: 2,
                      name: 'LoB Count',
                      presented_agg_name: 'lob_count',
                    },
                    post_processing: {
                      lob_count: {
                        value: {
                          formatter:
                            AGG_VALUE_FORMATTERS_ENUM.IDENTITY_NAN_IS_ZERO,
                          accessor: 'lob_count.inside.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      lob_count: constructScriptedMetricNestedCardinalityQuery(
                        'inside',
                        'lob',
                        'id'
                      ),
                    },
                  },
                },
                'TSI Sum': {
                  agg_name: 'TSI Sum',
                  meta: {
                    column: {
                      index: 3,
                      name: 'TSI Sum',
                      presented_agg_name: 'total_sum_insured',
                    },
                    post_processing: {
                      total_sum_insured: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'total_sum_insured.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      total_sum_insured: nestedStatsAggregation(
                        'lob',
                        'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',
                        'sum'
                      ),
                    },
                  },
                },
                'TSI Avg': {
                  agg_name: 'TSI Avg',
                  meta: {
                    column: {
                      index: 4,
                      name: 'TSI Avg',
                      presented_agg_name: 'avg_sum_insured',
                    },
                    post_processing: {
                      avg_sum_insured: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'avg_sum_insured.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      avg_sum_insured: nestedStatsAggregation(
                        'lob',
                        'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',
                        'avg'
                      ),
                    },
                  },
                },
                'TSI Median': {
                  agg_name: 'TSI Median',
                  meta: {
                    column: {
                      index: 5,
                      name: 'TSI Median',
                      presented_agg_name: 'median_tsi',
                    },
                    post_processing: {
                      median_tsi: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          // Gotta use a split path for the accessor here because of the dot character in `50.0`.
                          // More examples here: https://github.com/mariocasciaro/object-path#usage
                          accessor: [
                            'median_tsi',
                            'median_agg',
                            'median_agg_deepest',
                            'values',
                            '50.0',
                          ],
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      median_tsi: constructMedianAgg(
                        'lob',
                        'lob.totalSumInsured_$CURRENCY$_AdjustedForShare'
                      ),
                    },
                  },
                },
                'Premium Sum': {
                  agg_name: 'Premium Sum',
                  meta: {
                    column: {
                      index: 6,
                      name: 'Premium Sum',
                      presented_agg_name: 'total_premium',
                    },
                    post_processing: {
                      total_premium: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'total_premium.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      total_premium: nestedStatsAggregation(
                        'lob',
                        'lob.premium_$CURRENCY$',
                        'sum'
                      ),
                    },
                  },
                },
                'Premium Avg': {
                  agg_name: 'Premium Avg',
                  meta: {
                    column: {
                      index: 7,
                      name: 'Premium Avg',
                      presented_agg_name: 'avg_premium',
                    },
                    post_processing: {
                      avg_premium: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'avg_premium.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      avg_premium: nestedStatsAggregation(
                        'lob',
                        'lob.premium_$CURRENCY$',
                        'avg'
                      ),
                    },
                  },
                },
                'Premium Median': {
                  agg_name: 'Premium Median',
                  meta: {
                    column: {
                      index: 8,
                      name: 'Premium Median',
                      presented_agg_name: 'median_premium',
                    },
                    post_processing: {
                      median_premium: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          // Gotta use a split path for the accessor here because of the dot character in `50.0`.
                          // More examples here: https://github.com/mariocasciaro/object-path#usage
                          accessor: [
                            'median_premium',
                            'median_agg',
                            'median_agg_deepest',
                            'values',
                            '50.0',
                          ],
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      median_premium: constructMedianAgg(
                        'lob',
                        'lob.premium_$CURRENCY$'
                      ),
                    },
                  },
                },
              },
            };
          }),
        });
      }
      break;
    case 'AggregatedTotalPremiumPerNaceCodes_histogram_chart':
      const unique_buckets_AggregatedTotalPremiumPerNaceCodes_histogram_chart =
        provideNaceCodesBuckets(
          'AggregatedTotalPremiumPerNaceCodes_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob',
          numerical_field: 'lob.premium_$CURRENCY$',

          steps:
            unique_buckets_AggregatedTotalPremiumPerNaceCodes_histogram_chart,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: 'offer.client',
          category_field: 'offer.client.nacePrimaryCodes',

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedTotalOffersPerNaceCodes_histogram_chart':
      const unique_buckets_AggregatedTotalOffersPerNaceCodes_histogram_chart =
        provideNaceCodesBuckets(
          'AggregatedTotalOffersPerNaceCodes_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob',
          numerical_field: 'lob.id',

          steps:
            unique_buckets_AggregatedTotalOffersPerNaceCodes_histogram_chart,
          stats_operation: 'stats',

          category_field_is_nested: true,
          category_path: 'offer.client',
          category_field: 'offer.client.nacePrimaryCodes',

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedTotalSumInsuredPerNaceCodes_histogram_chart':
      const unique_buckets_AggregatedTotalSumInsuredPerNaceCodes_histogram_chart =
        provideNaceCodesBuckets(
          'AggregatedTotalSumInsuredPerNaceCodes_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob',
          numerical_field: 'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',

          steps:
            unique_buckets_AggregatedTotalSumInsuredPerNaceCodes_histogram_chart,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: 'offer.client',
          category_field: 'offer.client.nacePrimaryCodes',

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedLocationsPerNaceCodes_histogram_chart':
      const unique_buckets_AggregatedLocationsPerNaceCodes_histogram_chart =
        provideNaceCodesBuckets(
          'AggregatedLocationsPerNaceCodes_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob.addresses',
          numerical_field: 'lob.addresses.id',

          steps: unique_buckets_AggregatedLocationsPerNaceCodes_histogram_chart,
          stats_operation: 'stats',

          category_field_is_nested: true,
          category_path: 'offer.client',
          category_field: 'offer.client.nacePrimaryCodes',

          reverse_nested: false,
        });
      });

      break;
    // END Nace Codes
    // BEGIN Turnover
    case 'AggregatedTurnoverVsPremium_histogram_chart':
      queries = LoBs.map((lob) => {
        return {
          aggs: {
            [`histograms_${lob}`]: {
              filter: {
                bool: {
                  must: [
                    constructNestedTermQuery('lob', 'lob.lineOfBusiness', lob),
                  ],
                },
              },
              aggs: {
                generic_stats: {
                  filter: emptyBoolMustMatch(),
                  aggs: {
                    numerical_stats: {
                      nested: {
                        path: 'offer.client',
                      },
                      aggs: {
                        aggs: {
                          stats: {
                            field: 'offer.client.revenueInEuro',
                          },
                        },
                      },
                    },
                  },
                },
                spread_parent: {
                  filter: emptyBoolMustMatch(),
                  aggs: {
                    spread: {
                      filter: emptyBoolMustMatch(),
                      aggs: {
                        at_doc_level: {
                          filters: {
                            other_bucket: true,
                            filters: {
                              ...arrayToObject(
                                constructHistogramSpread(
                                  'less_',
                                  numericalRanges.SI,
                                  'lob',
                                  'lob.premium_$CURRENCY$'
                                ),
                                'bucket_name',
                                'query'
                              ),
                            },
                          },
                        },
                      },
                    },
                    ...arrayToObject(
                      constructAdditionalNumericalHistoStatsBuckets({
                        numerical_path: 'offer.client',
                        numerical_field: 'offer.client.revenueInEuro',
                        stats_path: 'lob',
                        stats_field: 'lob.premium_$CURRENCY$',
                        steps: numericalRanges.SI,
                        wrap_in_empty_matchall: true,
                      }),
                      'bucket_name',
                      'query'
                    ),
                  },
                },
              },
            },
          },
        };
      });

      break;
    case 'AggregatedTurnoverVsTSI_histogram_chart':
      queries = LoBs.map((lob) => {
        return {
          aggs: {
            [`histograms_${lob}`]: {
              filter: {
                bool: {
                  must: [
                    constructNestedTermQuery('lob', 'lob.lineOfBusiness', lob),
                  ],
                },
              },
              aggs: {
                generic_stats: {
                  filter: emptyBoolMustMatch(),
                  aggs: {
                    numerical_stats: {
                      nested: {
                        path: 'offer.client',
                      },
                      aggs: {
                        aggs: {
                          stats: {
                            field: 'offer.client.revenueInEuro',
                          },
                        },
                      },
                    },
                  },
                },
                spread_parent: {
                  filter: emptyBoolMustMatch(),
                  aggs: {
                    spread: {
                      filter: emptyBoolMustMatch(),
                      aggs: {
                        at_doc_level: {
                          filters: {
                            other_bucket: true,
                            filters: {
                              ...arrayToObject(
                                constructHistogramSpread(
                                  'less_',
                                  numericalRanges.SI,
                                  'lob',
                                  'lob.totalSumInsured_$CURRENCY$_AdjustedForShare'
                                ),
                                'bucket_name',
                                'query'
                              ),
                            },
                          },
                        },
                      },
                    },
                    ...arrayToObject(
                      constructAdditionalNumericalHistoStatsBuckets({
                        numerical_path: 'offer.client',
                        numerical_field: 'offer.client.revenueInEuro',
                        stats_path: 'lob',
                        stats_field:
                          'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',
                        steps: numericalRanges.SI,
                        wrap_in_empty_matchall: true,
                      }),
                      'bucket_name',
                      'query'
                    ),
                  },
                },
              },
            },
          },
        };
      });

      break;
    case 'AggregatedTurnoverVsCount_histogram_chart':
      queries = LoBs.map((lob) => {
        return {
          aggs: {
            [`histograms_${lob}`]: {
              filter: {
                bool: {
                  must: [
                    constructNestedTermQuery('lob', 'lob.lineOfBusiness', lob),
                  ],
                },
              },
              aggs: {
                generic_stats: {
                  filter: emptyBoolMustMatch(),
                  aggs: {
                    numerical_stats: {
                      nested: {
                        path: 'offer.client',
                      },
                      aggs: {
                        aggs: {
                          stats: {
                            field: 'offer.client.revenueInEuro',
                          },
                        },
                      },
                    },
                  },
                },
                spread_parent: {
                  filter: emptyBoolMustMatch(),
                  aggs: {
                    spread: {
                      filter: emptyBoolMustMatch(),
                      aggs: {
                        at_doc_level: {
                          filters: {
                            other_bucket: true,
                            filters: {
                              ...arrayToObject(
                                constructHistogramSpread(
                                  'less_',
                                  numericalRanges.SI,
                                  'lob',
                                  'lob.id'
                                ),
                                'bucket_name',
                                'query'
                              ),
                            },
                          },
                        },
                      },
                    },
                    ...arrayToObject(
                      constructAdditionalNumericalHistoStatsBuckets({
                        numerical_path: 'offer.client',
                        numerical_field: 'offer.client.revenueInEuro',
                        stats_path: 'lob',
                        stats_field: 'lob.id',
                        steps: numericalRanges.SI,
                        wrap_in_empty_matchall: true,
                      }),
                      'bucket_name',
                      'query'
                    ),
                  },
                },
              },
            },
          },
        };
      });

      break;
    // END Turnover

    // BEGIN Gross Risk Profile
    case 'GrossRiskProfile_colored_histogram_chart':
      const gross_query_names_as_dict = {
        ...arrayToObject(
          LISTS_OF_HISTOGRAM_CATEGORIES.countryNamesVsISO.map(
            ([iso, country_name]) => {
              return {
                key: iso === 'ALL' ? country_name : `${country_name} (${iso})`,
                val: iso,
              };
            }
          ),
          'key',
          'val'
        ),
      };

      queries = Object.keys(gross_query_names_as_dict).map((step) => {
        const country_name = gross_query_names_as_dict[step];

        return {
          aggs: {
            [`histograms_${country_name}`]: {
              filter: {
                bool: {
                  must:
                    step === 'All Countries Combined'
                      ? []
                      : [
                        constructNestedTermQuery(
                          'offer',
                          'offer.requestFromCountry',
                          country_name
                        ),
                      ],
                },
              },
              aggs: {
                [`generic_stats`]: nestedStatsAggregation('lob', 'lob.id'),
                [`premium_stats`]: nestedStatsAggregation(
                  'lob',
                  'lob.premium_$CURRENCY$'
                ),
                [`premium_median`]: nestedMedianAggregation(
                  'lob',
                  'lob.premium_$CURRENCY$'
                ),
                [`tsi_stats`]: nestedStatsAggregation(
                  'lob',
                  'lob.totalSumInsured_$CURRENCY$_AdjustedForShare'
                ),
                [`tsi_median`]: nestedMedianAggregation(
                  'lob',
                  'lob.totalSumInsured_$CURRENCY$_AdjustedForShare'
                ),
              },
            },
          },
        };
      });
      break;

    // BEGIN Regional Directorate
    case 'AggregatedTotalPremiumPerRegionOfOrigin_histogram_chart':
      const unique_buckets_AggregatedTotalPremiumPerRegionOfOrigin_histogram_chart =
        provideRegionalDirectorateBuckets(
          'AggregatedTotalPremiumPerRegionOfOrigin_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob',
          numerical_field: 'lob.premium_$CURRENCY$',

          steps:
            unique_buckets_AggregatedTotalPremiumPerRegionOfOrigin_histogram_chart,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: 'offer',
          category_field: 'offer.regionalDirectorate',

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedLOBCountPerRegionOfOrigin_histogram_chart':
      const unique_buckets_AggregatedLOBCountPerRegionOfOrigin_histogram_chart =
        provideRegionalDirectorateBuckets(
          'AggregatedLOBCountPerRegionOfOrigin_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob',
          numerical_field: 'lob.id',

          steps:
            unique_buckets_AggregatedLOBCountPerRegionOfOrigin_histogram_chart,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: 'offer',
          category_field: 'offer.regionalDirectorate',

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedTotalSumInsuredPerRegionOfOrigin_histogram_chart':
      const unique_buckets_AggregatedTotalSumInsuredPerRegionOfOrigin_histogram_chart =
        provideRegionalDirectorateBuckets(
          'AggregatedTotalSumInsuredPerRegionOfOrigin_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob',
          numerical_field: 'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',

          steps:
            unique_buckets_AggregatedTotalSumInsuredPerRegionOfOrigin_histogram_chart,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: 'offer',
          category_field: 'offer.regionalDirectorate',

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedLocationsCountPerRegionOfOrigin_histogram_chart':
      const unique_buckets_AggregatedLocationsCountPerRegionOfOrigin_histogram_chart =
        provideRegionalDirectorateBuckets(
          'AggregatedLocationsCountPerRegionOfOrigin_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob.addresses',
          numerical_field: 'lob.addresses.id',

          steps:
            unique_buckets_AggregatedLocationsCountPerRegionOfOrigin_histogram_chart,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: 'offer',
          category_field: 'offer.regionalDirectorate',

          reverse_nested: false,
        });
      });
      break;
    // END Regional Directorate
    // BEGIN Sales Partner
    case 'AggregatedTotalPremiumPerSalesPartner_histogram_chart':
      const unique_buckets_AggregatedTotalPremiumPerSalesPartner_histogram_chart =
        provideSalesPartnersBuckets(
          'AggregatedTotalPremiumPerSalesPartner_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob',
          numerical_field: 'lob.premium_$CURRENCY$',

          steps:
            unique_buckets_AggregatedTotalPremiumPerSalesPartner_histogram_chart,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: 'lob.salesPartner',
          category_field: 'lob.salesPartner.name',

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedLOBCountPerSalesPartner_histogram_chart':
      const unique_buckets_AggregatedLOBCountPerSalesPartner_histogram_chart =
        provideSalesPartnersBuckets(
          'AggregatedLOBCountPerSalesPartner_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob',
          numerical_field: 'lob.id',

          steps:
            unique_buckets_AggregatedLOBCountPerSalesPartner_histogram_chart,
          stats_operation: 'stats',

          category_field_is_nested: true,
          category_path: 'lob.salesPartner',
          category_field: 'lob.salesPartner.name',

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedTotalSumInsuredPerSalesPartner_histogram_chart':
      const unique_buckets_AggregatedTotalSumInsuredPerSalesPartner_histogram_chart =
        provideSalesPartnersBuckets(
          'AggregatedTotalSumInsuredPerSalesPartner_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob',
          numerical_field: 'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',

          steps:
            unique_buckets_AggregatedTotalSumInsuredPerSalesPartner_histogram_chart,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: 'lob.salesPartner',
          category_field: 'lob.salesPartner.name',

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedLocationsCountPerSalesPartner_histogram_chart':
      const unique_buckets_AggregatedLocationsCountPerSalesPartner_histogram_chart =
        provideSalesPartnersBuckets(
          'AggregatedLocationsCountPerSalesPartner_histogram_chart'
        );

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: 'lob.addresses',
          numerical_field: 'lob.addresses.id',

          steps:
            unique_buckets_AggregatedLocationsCountPerSalesPartner_histogram_chart,
          stats_operation: 'stats',

          category_field_is_nested: true,
          category_path: 'lob.salesPartner',
          category_field: 'lob.salesPartner.name',

          reverse_nested: false,
        });
      });
      break;
    // END Sales Partner
    // BEGIN Avg Risk Class Perils
    case 'AggregatedTotalPremiumPerAvgRiskClassPerPeril_histogram_chart':
      const unique_buckets_AggregatedTotalPremiumPerAvgRiskClassPerPeril_histogram_chart =
        [].rangeOfNumbers(1, 10, 1).map((item) => `RC ${item}`);

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: constructPerilPath(),
          numerical_field: constructPerilPath(`mTotalPremium_$CURRENCY$`),

          steps:
            unique_buckets_AggregatedTotalPremiumPerAvgRiskClassPerPeril_histogram_chart,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: constructPerilPath(),
          category_field: constructPerilPath('avgRiskClass'),

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedTotalSumInsuredPerAvgRiskClassPerPeril_histogram_chart':
      const unique_buckets_AggregatedTotalSumInsuredPerAvgRiskClassPerPeril_histogram_chart =
        [].rangeOfNumbers(1, 10, 1).map((item) => `RC ${item}`);

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: constructPerilPath(),
          numerical_field: constructPerilPath(
            `totalSI_$CURRENCY$_AdjustedForShare`
          ),

          steps:
            unique_buckets_AggregatedTotalSumInsuredPerAvgRiskClassPerPeril_histogram_chart,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: constructPerilPath(),
          category_field: constructPerilPath('avgRiskClass'),

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedLOBCountPerAvgRiskClassPerPeril_histogram_chart':
      const unique_buckets_AggregatedLOBCountPerAvgRiskClassPerPeril_histogram_chart =
        [].rangeOfNumbers(1, 10, 1).map((item) => `RC ${item}`);

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: constructPerilPath(),
          numerical_field: constructPerilPath('id'),

          steps:
            unique_buckets_AggregatedLOBCountPerAvgRiskClassPerPeril_histogram_chart,
          stats_operation: 'stats',

          category_field_is_nested: true,
          category_path: constructPerilPath(),
          category_field: constructPerilPath('avgRiskClass'),

          reverse_nested: false,
        });
      });
      break;
    case 'AggregatedGroupedPremiumPerAvgRiskClassPerPeril_LoBBased_histogram_chart':
      query_dict = {
        ...arrayToObject(
          [].rangeOfNumbers(1, 10, 1).map((rc) => {
            return {
              key: `Risk_Class_${rc < 10 ? '0' + rc : rc}`, // "A" will be extracted later but we wanna keep the order of bucket keys
              val: `Risk_Class_${rc < 10 ? '0' + rc : rc}`,
            };
          }),
          'key',
          'val'
        ),
      };

      numerical_aggs = {
        ...arrayToObject(
          Object.keys(
            filterObjectPropertiesByKeyRegexes(query_dict, [/^[^1]/g])
          ).map((applicable_key) => {
            const riskClassInt = extractInt(applicable_key.match(/\d+$/)[0]);

            return {
              bucket_name: applicable_key,
              query: {
                filter: {
                  nested: {
                    path: `${constructPerilPath()}`,
                    query: {
                      bool: {
                        must: [
                          {
                            range: {
                              [`${constructPerilPath('avgRiskClass')}`]: {
                                ...createScoreRangeParameters(riskClassInt),
                              },
                            },
                          },
                        ],
                      },
                    },
                  },
                },
                aggs: {
                  lob_count: {
                    nested: {
                      path: 'lob',
                    },
                    aggs: {
                      cardinality: {
                        cardinality: {
                          field: 'lob.id',
                        },
                      },
                    },
                  },
                  m_premium_peril_specific_sum: {
                    nested: {
                      path: constructPerilPath(),
                    },
                    aggs: {
                      sum: {
                        sum: {
                          field: constructPerilPath('mTotalPremium_$CURRENCY$'),
                        },
                      },
                    },
                  },
                  tsi_peril_specific_sum: {
                    nested: {
                      path: constructPerilPath(),
                    },
                    aggs: {
                      sum: {
                        sum: {
                          field: constructPerilPath(
                            'totalSI_$CURRENCY$_AdjustedForShare'
                          ),
                        },
                      },
                    },
                  },
                },
              },
            };
          }),
          'bucket_name',
          'query'
        ),
      };

      queries = Object.keys(query_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [],
                },
              },
              aggs: {
                ...numerical_aggs,
              },
            },
          },
        };
      });

      break;
    case 'AggregatedGroupedPremiumPerAvgRiskClassPerPeril_location_based_histogram_chart':
      query_dict_addr_based = {
        ...arrayToObject(
          [].rangeOfNumbers(1, 10, 1).map((rc) => {
            return {
              key: `Risk_Class_${rc < 10 ? '0' + rc : rc}`, // "A" will be extracted later but we wanna keep the order of bucket keys
              val: `Risk_Class_${rc < 10 ? '0' + rc : rc}`,
            };
          }),
          'key',
          'val'
        ),
      };

      numerical_aggs = {
        ...arrayToObject(
          Object.keys(
            filterObjectPropertiesByKeyRegexes(query_dict_addr_based, [
              /^[^1]/g,
            ])
          ).map((applicable_key) => {
            const riskClassInt = extractInt(applicable_key.match(/\d+$/)[0]);

            return {
              bucket_name: applicable_key,
              query: {
                filter: {
                  nested: {
                    path: `${constructPerilPath()}`,
                    query: {
                      bool: {
                        must: [
                          {
                            range: {
                              [`${constructPerilPath('avgRiskClass')}`]: {
                                ...createScoreRangeParameters(riskClassInt),
                              },
                            },
                          },
                        ],
                      },
                    },
                  },
                },
                aggs: {
                  address_count: {
                    nested: {
                      path: 'lob.addresses',
                    },
                    aggs: {
                      cardinality: {
                        cardinality: {
                          field: 'lob.addresses.id',
                        },
                      },
                    },
                  },
                  m_premium_peril_specific_sum: {
                    nested: {
                      path: constructPerilPath(),
                    },
                    aggs: {
                      sum: {
                        sum: {
                          field: constructPerilPath('mTotalPremium_$CURRENCY$'),
                        },
                      },
                    },
                  },
                  tsi_peril_specific_sum: {
                    nested: {
                      path: constructPerilPath(),
                    },
                    aggs: {
                      sum: {
                        sum: {
                          field: constructPerilPath(
                            'totalSI_$CURRENCY$_AdjustedForShare'
                          ),
                        },
                      },
                    },
                  },
                },
              },
            };
          }),
          'bucket_name',
          'query'
        ),
      };

      queries = Object.keys(query_dict_addr_based).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [],
                },
              },
              aggs: {
                ...numerical_aggs,
              },
            },
          },
        };
      });

      break;
    // END Avg Risk Class Perils
    // BEGIN Avg PDAs
    case 'AggregatedAvgPDAPerAvgRiskClassPerPeril_histogram_chart':
      const unique_buckets_AggregatedPDAPerAvgRiskClassPerPeril_histogram_chart =
        [].rangeOfNumbers(1, 10, 1).map((item) => `RC ${item}`);

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: constructPerilPath(),
          numerical_field: constructPerilPath('personalDeviationAuthority'),

          steps:
            unique_buckets_AggregatedPDAPerAvgRiskClassPerPeril_histogram_chart,
          stats_operation: 'avg',

          category_field_is_nested: true,
          category_path: constructPerilPath(),
          category_field: constructPerilPath('avgRiskClass'),

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedAvgPDAPerTSIPerPeril_histogram_chart':
      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: constructPerilPath(),
          numerical_field: constructPerilPath('personalDeviationAuthority'),

          steps: numericalRanges.SI,
          stats_operation: 'avg',

          category_field_is_nested: true,
          category_path: 'lob',
          category_field: 'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',

          reverse_nested: false,
        });
      });

      break;
    // END Avg PDAs
    // BEGIN Broker Commission
    case 'AggregatedBrokerCommission_histogram_chart':
      queries = LoBs.map((lob) => {
        return {
          aggs: {
            [`histograms_${lob}`]: {
              filter: {
                bool: {
                  must: [
                    {
                      nested: {
                        path: 'lob',
                        query: {
                          bool: {
                            filter: [
                              {
                                term: {
                                  'lob.lineOfBusiness': {
                                    value: lob,
                                  },
                                },
                              },
                            ],
                          },
                        },
                      },
                    },
                  ],
                },
              },
              aggs: {
                commissioned_mprem_stats: {
                  nested: {
                    path: 'lob.rating',
                  },
                  aggs: {
                    stats_parent: {
                      scripted_metric: {
                        init_script: 'state.list = []',
                        map_script: `
                        if (doc.containsKey('lob.rating.brokerCommission')) {
                            state.list.add(
                              (doc['lob.rating.brokerCommission'].value / 100) * doc['lob.rating.mTotalPremium_$CURRENCY$'].value
                            );
                        }
                        `,
                        combine_script: `
                        def result = 0.0;
                        for (def val: state.list) {
                          result += val;
                        }
                        return result;
                        `,
                        reduce_script: 'return states[0];',
                      },
                    },
                  },
                },
                mprem_stats: {
                  nested: {
                    path: 'lob.rating',
                  },
                  aggs: {
                    stats_parent: {
                      stats: {
                        field: 'lob.rating.mTotalPremium_$CURRENCY$',
                      },
                    },
                  },
                },
                commission_stats: {
                  nested: {
                    path: 'lob.rating',
                  },
                  aggs: {
                    stats_parent: {
                      stats: {
                        field: 'lob.rating.brokerCommission',
                      },
                    },
                  },
                },
              },
            },
          },
        };
      });

      break;
    // END Broker Commission
    // BEGIN Per Peril
    case 'AggregatedTotalMPremiumPerPeril_histogram_chart':
      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: (step) => constructPerilPath(null, step),
          numerical_field: (step) =>
            constructPerilPath('mTotalPremium_$CURRENCY$', step),

          steps: LISTS_OF_HISTOGRAM_CATEGORIES.listOfPerilNames,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: (step) => constructPerilPath(null, step),
          category_field: (step) => constructPerilPath('peril', step),

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedTotalTPremiumPerPeril_histogram_chart':
      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: (step) => constructPerilPath(null, step),
          numerical_field: (step) =>
            constructPerilPath('totalPremium_$CURRENCY$', step),

          steps: LISTS_OF_HISTOGRAM_CATEGORIES.listOfPerilNames,
          stats_operation: 'sum',

          category_field_is_nested: true,
          category_path: (step) => constructPerilPath(null, step),
          category_field: (step) => constructPerilPath('peril', step),

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedRiskScorePerAvgRiskClassPerPeril_histogram_chart':
      const unique_buckets_AggregatedRiskScorePerAvgRiskClassPerPeril_histogram_chart =
        [].rangeOfNumbers(1, 10, 1).map((item) => `RC ${item}`);

      queries = LoBs.map((lob) => {
        return constructSingleCategorizedSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,

          numerical_path: constructPerilPath(),
          numerical_field: constructPerilPath('avgScore'),

          steps:
            unique_buckets_AggregatedRiskScorePerAvgRiskClassPerPeril_histogram_chart,
          stats_operation: 'avg',

          category_field_is_nested: true,
          category_path: constructPerilPath(),
          category_field: constructPerilPath('avgRiskClass'),

          reverse_nested: false,
        });
      });

      break;
    case 'AggregatedRiskScoreVsPeril_histogram_chart':
      queries = LoBs.map((lob) => {
        return constructSingleNumericalHistogramSpreadQueryWithStatsUnderDifferentPath(
          lob,
          {
            filter_path: 'lob',
            filter_field: 'lob.lineOfBusiness',
            filter_value: lob,

            numerical_path: constructPerilPath(),
            numerical_field: constructPerilPath('avgScore'),

            stats_path: 'lob',
            stats_field: 'lob.id',

            steps: numericalRanges.AvgRiskScores,
          }
        );
      });

      break;
    case 'AggregatedInfoLevelVsRiskClassVsPeril_histogram_chart':
      queries = LoBs.map((lob) => {
        return {
          aggs: {
            [`histograms_${lob}`]: {
              filter: {
                bool: {
                  must: [
                    {
                      nested: {
                        path: 'lob',
                        query: {
                          bool: {
                            filter: [
                              {
                                term: {
                                  'lob.lineOfBusiness': {
                                    value: lob,
                                  },
                                },
                              },
                            ],
                          },
                        },
                      },
                    },
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  [
                    <any>{
                      bucket_name: 'alpha_stats',
                      query: {
                        filter: {
                          match_all: {}, // add an empty filter to preserve agg accessor path link in generated aggs below
                        },
                        aggs: {
                          unique_stats: {
                            nested: {
                              path: 'address',
                            },
                            aggs: {
                              stats_parent: {
                                stats: {
                                  field:
                                    'address.sumInsured_$CURRENCY$_AdjustedForShare',
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  ].concat(
                    ['1', '2', '3', null].map((infoLevel) => {
                      let filter_query;
                      if (infoLevel) {
                        filter_query = {
                          // infoLevel will already be passed into constructCombinedLocationPerilQuery
                          ...emptyBoolMustMatch(),
                        };
                      } else {
                        filter_query = {
                          bool: {
                            must_not: {
                              exists: {
                                field:
                                  'address.locationProperty.locationAssessments.level',
                              },
                            },
                          },
                        };
                      }

                      return <any>{
                        bucket_name: infoLevel
                          ? `Info level ${infoLevel}`
                          : `No Info Level`,
                        query: {
                          filter: {
                            bool: {
                              must: [
                                {
                                  nested: {
                                    path: constructPerilPath(),
                                    query: [{ ...filter_query }],
                                  },
                                },
                                constructCombinedLocationPerilQuery(
                                  infoLevel,
                                  filter_query
                                ),
                              ],
                            },
                          },
                          aggs: {
                            unique_stats: {
                              nested: {
                                path: 'address',
                              },
                              aggs: {
                                stats_parent: {
                                  stats: {
                                    field: 'address.sumInsured_$CURRENCY$_AdjustedForShare',
                                  },
                                },
                              },
                            },
                            // uniques: {
                            //   nested: {
                            //     path: 'address'
                            //   },
                            //   aggs: {
                            //     uniques_by_address_id: {
                            //       reverse_nested: {
                            //       },
                            //       aggs: {
                            //         uniques_by_address_id_reversed: {
                            //           terms: {
                            //             field: 'address.id',
                            //             size: 90e5
                            //           },
                            //           aggs: {
                            //             distinct_tokens: {
                            //               cardinality: {
                            //                 field: 'address.id'
                            //               }
                            //             }
                            //           }
                            //         }
                            //       }
                            //     }
                            //   }
                            // }
                          },
                        },
                      };
                    })
                  ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;
    // END Per Peril
    // BEGIN Numerical Histo Aggs (1, 10, 50, 200...)
    case 'AggregatedSI_histogram_chart':
      queries = LoBs.map((lob) => {
        return constructSingleNumericalHistogramSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,
          numerical_path: 'lob',
          numerical_field: 'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',
          steps: numericalRanges.SI,
        });
      });

      break;

    case 'AggregatedPremium_histogram_chart':
      queries = LoBs.map((lob) => {
        return constructSingleNumericalHistogramSpreadQuery(lob, {
          filter_path: 'lob',
          filter_field: 'lob.lineOfBusiness',
          filter_value: lob,
          numerical_path: 'lob',
          numerical_field: 'lob.premium_$CURRENCY$',
          steps: numericalRanges.Premium,
        });
      });

      break;
    // END Numerical Histo Aggs
    // BEGIN Superscore
    case 'AggregatedSuperscoresPerLocationsPerIndustry_histogram_chart':
      queries = LoBs.map((lob) => {
        return {
          aggs: {
            [`histograms_${lob}`]: {
              filter: {
                bool: {
                  must: [
                    {
                      nested: {
                        path: 'lob',
                        query: {
                          bool: {
                            filter: [
                              {
                                term: {
                                  'lob.lineOfBusiness': {
                                    value: lob,
                                  },
                                },
                              },
                            ],
                          },
                        },
                      },
                    },
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  listOfIndustryCategories.map((industry) => {
                    return {
                      bucket_name: industry,
                      query: {
                        filter: {
                          nested: {
                            path: 'offer',
                            query: {
                              term: {
                                'offer.industry': industry,
                              },
                            },
                          },
                        },
                        aggs: {
                          superscore_x_tsi_stats: {
                            nested: {
                              path: 'lob.rating',
                            },
                            aggs: {
                              stats_parent: {
                                stats: {
                                  field:
                                    'lob.rating.superScoredTotalSumInsured_$CURRENCY$_AdjustedForShare',
                                },
                              },
                            },
                          },
                          tsi_stats: {
                            nested: {
                              path: 'lob.rating',
                            },
                            aggs: {
                              stats_parent: {
                                stats: {
                                  field:
                                    'lob.rating.totalSumInsured_$CURRENCY$_AdjustedForShare',
                                },
                              },
                            },
                          },
                          superscore_stats: {
                            nested: {
                              path: 'lob.rating',
                            },
                            aggs: {
                              stats_parent: {
                                stats: {
                                  field: 'lob.rating.superScore',
                                },
                              },
                            },
                          },
                        },
                      },
                    };
                  }),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });

      break;
    case 'AggregatedSuperscoresPerRequestFromCountry_histogram_chart':
      queries = LoBs.map((lob) => {
        return {
          aggs: {
            [`histograms_${lob}`]: {
              filter: {
                bool: {
                  must: [
                    {
                      nested: {
                        path: 'lob',
                        query: {
                          bool: {
                            filter: [
                              {
                                term: {
                                  'lob.lineOfBusiness': {
                                    value: lob,
                                  },
                                },
                              },
                            ],
                          },
                        },
                      },
                    },
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  listOfRequestFromCountry.map((country) => {
                    return {
                      bucket_name: country,
                      query: {
                        filter: {
                          nested: {
                            path: 'offer',
                            query: {
                              term: {
                                'offer.requestFromCountry': country,
                              },
                            },
                          },
                        },
                        aggs: {
                          superscore_x_tsi_stats: {
                            nested: {
                              path: 'lob.rating',
                            },
                            aggs: {
                              stats_parent: {
                                stats: {
                                  field:
                                    'lob.rating.superScoredTotalSumInsured_$CURRENCY$_AdjustedForShare',
                                },
                              },
                            },
                          },
                          tsi_stats: {
                            nested: {
                              path: 'lob.rating',
                            },
                            aggs: {
                              stats_parent: {
                                stats: {
                                  field:
                                    'lob.rating.totalSumInsured_$CURRENCY$_AdjustedForShare',
                                },
                              },
                            },
                          },
                          superscore_stats: {
                            nested: {
                              path: 'lob.rating',
                            },
                            aggs: {
                              stats_parent: {
                                stats: {
                                  field: 'lob.rating.superScore',
                                },
                              },
                            },
                          },
                        },
                      },
                    };
                  }),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });

      break;
    case 'AggregatedSuperscoresPerBucketedTSI_histogram_chart':
      queries = LoBs.map((lob) => {
        return {
          aggs: {
            [`histograms_${lob}`]: {
              filter: {
                bool: {
                  must: [
                    {
                      nested: {
                        path: 'lob',
                        query: {
                          bool: {
                            filter: [
                              {
                                term: {
                                  'lob.lineOfBusiness': {
                                    value: lob,
                                  },
                                },
                              },
                            ],
                          },
                        },
                      },
                    },
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  numericalRanges.SI.map((tsi) => {
                    const generated_steps = getRangesFromSteps(
                      numericalRanges.SI
                    );
                    const corresponding_interval = generated_steps.filter(
                      (step) => step.max === tsi
                    )[0];
                    const { filter } = generateRangeFilterFromRange(
                      corresponding_interval,
                      'lob.totalSumInsured_$CURRENCY$_AdjustedForShare'
                    );

                    return {
                      bucket_name: `less_${tsi}`,
                      query: {
                        filter: {
                          nested: {
                            path: 'lob',
                            query: {
                              ...filter,
                            },
                          },
                        },
                        aggs: {
                          superscore_x_tsi_stats: {
                            nested: {
                              path: 'lob.rating',
                            },
                            aggs: {
                              stats_parent: {
                                stats: {
                                  field:
                                    'lob.rating.superScoredTotalSumInsured_$CURRENCY$_AdjustedForShare',
                                },
                              },
                            },
                          },
                          tsi_stats: {
                            nested: {
                              path: 'lob.rating',
                            },
                            aggs: {
                              stats_parent: {
                                stats: {
                                  field:
                                    'lob.rating.totalSumInsured_$CURRENCY$_AdjustedForShare',
                                },
                              },
                            },
                          },
                          superscore_stats: {
                            nested: {
                              path: 'lob.rating',
                            },
                            aggs: {
                              stats_parent: {
                                stats: {
                                  field: 'lob.rating.superScore',
                                },
                              },
                            },
                          },
                        },
                      },
                    };
                  }),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });

      break;
    case 'AggregatedTSIPerInsuredObject_histogram_chart':
      queries = LoBs.map((lob) => {
        return {
          aggs: {
            [`histograms_${lob}`]: {
              filter: {
                bool: {
                  must: [
                    {
                      nested: {
                        path: 'lob',
                        query: {
                          bool: {
                            filter: [
                              {
                                term: {
                                  'lob.lineOfBusiness': {
                                    value: lob,
                                  },
                                },
                              },
                            ],
                          },
                        },
                      },
                    },
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  LISTS_OF_HISTOGRAM_CATEGORIES.listOfInsuredObjectTypes.map(
                    (insured_o_type) => {
                      const filter_query = {
                        bool: {
                          must: {
                            nested: {
                              path: `lob.risks.insuredObjects.${insured_o_type}`,
                              query: {
                                term: {
                                  [`lob.risks.insuredObjects.${insured_o_type}.type`]:
                                    insured_o_type,
                                },
                              },
                            },
                          },
                        },
                      };

                      return <any>{
                        bucket_name: insured_o_type,
                        query: {
                          filter: {
                            ...filter_query,
                          },
                          aggs: {
                            unique_stats: {
                              nested: {
                                path: `lob.risks.insuredObjects.${insured_o_type}`,
                              },
                              aggs: {
                                stats_parent: {
                                  stats: {
                                    field: `lob.risks.insuredObjects.${insured_o_type}.sumInsured_$CURRENCY$`,
                                  },
                                },
                              },
                            },
                          },
                        },
                      };
                    }
                  ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;

    case 'AggregatedTSIPerConstructionMaterials_location_based_histogram_chart':
      const tsi_per_const_mat_query_dict = {
        ...arrayToObject(
          Object.keys((window as any).mappers.ConstructionMaterials)
            .sort()
            .map((material) => {
              return {
                key: material,
                val: material,
              };
            }),
          'key',
          'val'
        ),
      };

      numerical_aggs = {
        ...arrayToObject(
          Object.keys(
            filterObjectPropertiesByKeyRegexes(tsi_per_const_mat_query_dict, [
              /^[^1]/g,
            ])
          ).map((material) => {
            return {
              bucket_name: material,
              query: {
                filter: {
                  ...constructNestedTermQuery(
                    'address.locationProperty',
                    'address.locationProperty.constructionMaterial',
                    material
                  ),
                },
                aggs: {
                  location_count: {
                    nested: {
                      path: 'address',
                    },
                    aggs: {
                      cardinality: {
                        cardinality: {
                          field: 'address.id',
                        },
                      },
                    },
                  },
                  tsi_per_location: {
                    nested: {
                      path: 'address',
                    },
                    aggs: {
                      sum: {
                        sum: {
                          field:
                            'address.sumInsured_$CURRENCY$_AdjustedForShare',
                        },
                      },
                    },
                  },
                },
              },
            };
          }),
          'bucket_name',
          'query'
        ),
      };

      queries = Object.keys(tsi_per_const_mat_query_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [],
                },
              },
              aggs: {
                ...numerical_aggs,
              },
            },
          },
        };
      });
      break;

    // RISK ENGINEERING BEGIN
    case 'AggregatedAddressesVsIndustryCategory_histogram_chart':
      ({ query_names, numerical_aggs } = REM_base_metrics_exports());

      queries = Object.keys(query_names).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [], // ...REM_base_metrics_filter_exists_queries]
                },
              },
              aggs: {
                ...arrayToObject(
                  LISTS_OF_HISTOGRAM_CATEGORIES.listOfIndustryCategories.map(
                    (industry_categ) => {
                      return <any>{
                        bucket_name: industry_categ,
                        query: {
                          filter: {
                            bool: {
                              must: [
                                {
                                  nested: {
                                    path: 'offer',
                                    query: {
                                      bool: {
                                        must: {
                                          term: {
                                            'offer.industry': industry_categ,
                                          },
                                        },
                                      },
                                    },
                                  },
                                },
                              ],
                            },
                          },
                          aggs: {
                            ...numerical_aggs,
                          },
                        },
                      };
                    }
                  ),

                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;

    case 'AggregatedAddressesPerIL_histogram_chart':
      ({ query_names, numerical_aggs } = REM_base_metrics_exports());

      queries = Object.keys(query_names).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: emptyBoolMustMatch(),
              aggs: {
                ...arrayToObject(
                  [1, 2, 3].map((infoLevel) => {
                    return <any>{
                      bucket_name: `A) infoLevel ${infoLevel}`,
                      query: {
                        filter: constructNestedLocationAssessmentQueryString(
                          undefined,
                          infoLevel,
                          undefined,
                          undefined,
                          undefined,
                          true
                        ),
                        // {
                        //   bool: {
                        //     must: [constructCombinedLocationPerilQuery(infoLevel)]
                        //   }
                        // },
                        aggs: {
                          ...numerical_aggs,
                        },
                      },
                    };
                  }),

                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;
    case 'AggregatedAddressesPerScore_histogram_chart':
      ({ query_names, numerical_aggs } = REM_base_metrics_exports());

      queries = Object.keys(query_names).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [
                    // ...REM_base_metrics_filter_exists_queries,
                    // constructNestedExistsQuery(
                    //   'lob.addresses.locationProperty.locationAssessments',
                    //   'lob.addresses.locationProperty.locationAssessments.score'
                    // )
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  numericalRanges.ScoreIntervals.map((interval) => {
                    const [gte, lte] = interval;
                    return <any>{
                      bucket_name: `A) Score ${gte}...${lte}`,
                      query: {
                        filter: {
                          bool: {
                            must: constructNestedRangeQuery(
                              `lob.rating.ratingToolPerils.${(window as any).selected_peril
                              }`,
                              `lob.rating.ratingToolPerils.${(window as any).selected_peril
                              }.avgScore`,
                              {
                                ...generateRangeValuesFromList(gte, lte),
                              }
                            ),
                          },
                        },
                        aggs: {
                          ...numerical_aggs,
                        },
                      },
                    };
                  }),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;
    case 'AggregatedAddressesPerPML_histogram_chart':
      ({ query_names, numerical_aggs } = REM_base_metrics_exports());

      queries = Object.keys(query_names).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [
                    // ...REM_base_metrics_filter_exists_queries,
                    // constructNestedExistsQuery('lob', 'lob.uniqaPML_$CURRENCY$')
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  numericalRanges.UniqaPMLintervals.map((interval, index) => {
                    const [gte, lte] = interval;

                    return <any>{
                      // user LETTERS[index] to keep order when sorting ES dict response
                      bucket_name: `A) Uniqa PML ${LETTERS[index]}) ${gte}...${lte || ''
                        } Mio.`,
                      query: {
                        filter: {
                          bool: {
                            must: [
                              {
                                nested: {
                                  path: 'lob',
                                  query: {
                                    bool: {
                                      must: {
                                        range: {
                                          'lob.uniqaPML_$CURRENCY$':
                                          {
                                            ...generateRangeValuesFromList(
                                              gte,
                                              lte,
                                              1e6
                                            ),
                                          },
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                            ],
                          },
                        },
                        aggs: {
                          ...numerical_aggs,
                        },
                      },
                    };
                  }),

                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;
    case 'AggregatedAddressesPerRC_histogram_chart':
      ({ query_names, numerical_aggs } = REM_base_metrics_exports());

      queries = Object.keys(query_names).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [
                    // ...REM_base_metrics_filter_exists_queries,
                    // constructNestedExistsQuery(
                    //   `${constructLocationPerilPath(true)}`,
                    //   `${constructLocationPerilPath(true)}.peril`
                    // )
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  [].rangeOfNumbers(1, 10, 1).map((riskClass, index) => {
                    return <any>{
                      // user LETTERS[index] to keep order when sorting ES dict response
                      bucket_name: `A) Risk Class ${riskClass
                        .toString()
                        .padStart(2, '0')}`,
                      query: {
                        filter: {
                          bool: {
                            must: constructNestedRangeQuery(
                              `lob.rating.ratingToolPerils.${(window as any).selected_peril
                              }`,
                              `lob.rating.ratingToolPerils.${(window as any).selected_peril
                              }.avgRiskClass`,
                              createRangeParametersFromRiskClassInput(
                                riskClass
                              ) as {
                                gte: number;
                                lte: number;
                                gt: number; // gt might also accur,
                                lt: number; // lt might also occur
                              }
                            ),
                          },
                        },

                        aggs: {
                          ...numerical_aggs,
                        },
                      },
                    };
                  }),

                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;

    case 'AggregatedAddressesPerRC_vsPML_address_based_histogram_chart':
      query_names_as_dict = {
        '1)_number_of_locations': '1)_number_of_locations',
        ...arrayToObject(
          getListOfSelectedLocationPerilRiskClasses().map((rc) => {
            return {
              key: `Risk_Class_${rc < 10 ? '0' + rc : rc}`, // "A" will be extracted later but we wanna keep the order of bucket keys
              val: `Risk_Class_${rc < 10 ? '0' + rc : rc}`,
            };
          }),
          'key',
          'val'
        ),
      };

      numerical_aggs = {
        [query_names_as_dict['1)_number_of_locations']]: {
          nested: {
            path: 'address',
          },
          aggs: {
            inside: {
              stats: {
                field: 'address.id',
              },
            },
          },
        },
        ...arrayToObject(
          Object.keys(
            filterObjectPropertiesByKeyRegexes(query_names_as_dict, [/^[^1]/g])
          ).map((applicable_key) => {
            const riskClassInt = extractInt(applicable_key.match(/\d+$/)[0]);

            return {
              bucket_name: applicable_key,
              query: {
                filter: {
                  nested: {
                    path: `${constructLocationPerilPath()}`,
                    query: {
                      bool: {
                        must: [
                          {
                            term: {
                              [`${constructLocationPerilPath()}.peril`]: (
                                window as any
                              ).selected_location_peril,
                            },
                          },
                          {
                            term: {
                              [`${constructLocationPerilPath()}.riskClass`]:
                                riskClassInt,
                            },
                          },
                        ],
                      },
                    },
                  },
                },
                aggs: {
                  [applicable_key]: {
                    nested: {
                      path: 'address',
                    },
                    aggs: {
                      inside: {
                        stats: {
                          field: 'address.id',
                        },
                      },
                    },
                  },
                },
              },
            };
          }),
          'bucket_name',
          'query'
        ),
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [
                    // constructNestedExistsQuery('address', 'address.id'),
                    // constructNestedExistsQuery(
                    //   `${constructLocationPerilPath()}`,
                    //   `${constructLocationPerilPath()}.peril`
                    // ),
                    // constructNestedExistsQuery(
                    //   'address.locationProperty',
                    //   'address.locationProperty.PML_$CURRENCY$_AdjustedForShare'
                    // )
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  []
                    .concat(
                      !step.match(/\d+$/)
                        ? {}
                        : <any>{
                          bucket_name: 'total_stats',
                          query: {
                            filter: {
                              bool: {
                                must: [
                                  {
                                    nested: {
                                      path: `${constructLocationPerilPath()}`,
                                      query: {
                                        bool: {
                                          must: [
                                            {
                                              term: {
                                                [`${constructLocationPerilPath()}.peril`]:
                                                  (window as any)
                                                    .selected_location_peril,
                                              },
                                            },
                                            {
                                              term: {
                                                [`${constructLocationPerilPath()}.riskClass`]:
                                                  extractInt(
                                                    step.match(/\d+$/)[0]
                                                  ),
                                              },
                                            },
                                          ],
                                        },
                                      },
                                    },
                                  },
                                ],
                              },
                            },
                            aggs: {
                              totals: {
                                nested: {
                                  path: 'address',
                                },
                                aggs: {
                                  inside: {
                                    stats: {
                                      field: 'address.id',
                                    },
                                  },
                                },
                              },
                            },
                          },
                        }
                    )
                    .concat(
                      numericalRanges.UniqaPMLintervals.map(
                        (interval, index) => {
                          const [gte, lte] = interval;
                          return <any>{
                            // user LETTERS[index] to keep order when sorting ES dict response
                            bucket_name: `A) Uniqa PML ${LETTERS[index]
                              }) ${gte}...${lte || ''} Mio.`,
                            query: {
                              filter: {
                                bool: {
                                  must: [
                                    {
                                      nested: {
                                        path: 'address.locationProperty',
                                        query: {
                                          bool: {
                                            must: {
                                              range: {
                                                'address.locationProperty.PML_$CURRENCY$_AdjustedForShare':
                                                {
                                                  ...generateRangeValuesFromList(
                                                    gte,
                                                    lte,
                                                    1e6
                                                  ),
                                                },
                                              },
                                            },
                                          },
                                        },
                                      },
                                    },
                                  ],
                                },
                              },
                              aggs: {
                                ...numerical_aggs,
                              },
                            },
                          };
                        }
                      )
                    ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;
    case 'AggregatedAddressesPerRC_vsIL_address_based_histogram_chart':
      query_names_as_dict = {
        '1)_number_of_locations': '1)_number_of_locations',
        ...arrayToObject(
          getListOfSelectedLocationPerilRiskClasses().map((rc) => {
            return {
              key: `Risk_Class_${rc < 10 ? '0' + rc : rc}`, // "A" will be extracted later but we wanna keep the order of bucket keys
              val: `Risk_Class_${rc < 10 ? '0' + rc : rc}`,
            };
          }),
          'key',
          'val'
        ),
      };

      numerical_aggs = {
        [query_names_as_dict['1)_number_of_locations']]: {
          nested: {
            path: 'address',
          },
          aggs: {
            inside: {
              stats: {
                field: 'address.id',
              },
            },
          },
        },
        ...arrayToObject(
          Object.keys(
            filterObjectPropertiesByKeyRegexes(query_names_as_dict, [/^[^1]/g])
          ).map((applicable_key) => {
            const riskClassInt = extractInt(applicable_key.match(/\d+$/)[0]);

            return {
              bucket_name: applicable_key,
              query: {
                filter:
                  constructNestedLocationAssessmentQueryString(riskClassInt),
                aggs: {
                  [applicable_key]: {
                    nested: {
                      path: 'address',
                    },
                    aggs: {
                      inside: {
                        stats: {
                          field: 'address.id',
                        },
                      },
                    },
                  },
                },
              },
            };
          }),
          'bucket_name',
          'query'
        ),
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [
                    // constructNestedExistsQuery('address', 'address.id'),
                    // constructNestedExistsQuery(
                    //   `${constructLocationPerilPath()}`,
                    //   `${constructLocationPerilPath()}.peril`
                    // ),
                    // constructNestedExistsQuery(
                    //   'address.locationProperty.locationAssessments',
                    //   'address.locationProperty.locationAssessments.level'
                    // )
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  []
                    .concat(
                      !step.match(/\d+$/)
                        ? {}
                        : <any>{
                          bucket_name: 'total_stats',
                          query: {
                            filter: {
                              bool: {
                                must: [
                                  constructNestedLocationAssessmentQueryString(
                                    step.match(/\d+$/)[0]
                                  ),
                                ],
                              },
                            },
                            aggs: {
                              totals: {
                                nested: {
                                  path: 'address',
                                },
                                aggs: {
                                  inside: {
                                    stats: {
                                      field: 'address.id',
                                    },
                                  },
                                },
                              },
                            },
                          },
                        }
                    )
                    .concat(
                      [1, 2, 3].map((infoLevel) => {
                        return <any>{
                          bucket_name: `A) infoLevel ${infoLevel}`,
                          query: {
                            filter: {
                              bool: {
                                must: [
                                  constructNestedLocationAssessmentQueryString(
                                    step.match(/\d+$/)
                                      ? step.match(/\d+$/)[0]
                                      : undefined,
                                    infoLevel
                                  ),
                                ],
                              },
                            },
                            aggs: {
                              ...numerical_aggs,
                            },
                          },
                        };
                      })
                    ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;
    case 'AggregatedAddressesPerRC_vsScore_address_based_histogram_chart':
      query_names_as_dict = {
        '1)_number_of_locations': '1)_number_of_locations',
        '2)_nonexistent_score': '2)_nonexistent_score',
        ...arrayToObject(
          getListOfSelectedLocationPerilRiskClasses().map((rc) => {
            return {
              key: `Risk_Class_${rc < 10 ? '0' + rc : rc}`, // "A" will be extracted later but we wanna keep the order of bucket keys
              val: `Risk_Class_${rc < 10 ? '0' + rc : rc}`,
            };
          }),
          'key',
          'val'
        ),
      };

      numerical_aggs = {
        [query_names_as_dict['1)_number_of_locations']]: {
          nested: {
            path: 'address',
          },
          aggs: {
            inside: {
              stats: {
                field: 'address.id',
              },
            },
          },
        },
        [query_names_as_dict['2)_nonexistent_score']]: {
          nested: {
            path: 'address',
          },
          aggs: {
            inside: {
              stats: {
                field: 'address.id',
              },
            },
          },
        },
        ...arrayToObject(
          Object.keys(
            filterObjectPropertiesByKeyRegexes(query_names_as_dict, [
              /^[^(1|2)]/g,
            ])
          ).map((applicable_key) => {
            const riskClassInt = extractInt(applicable_key.match(/\d+$/)[0]);

            return {
              bucket_name: applicable_key,
              query: {
                filter:
                  constructNestedLocationAssessmentQueryString(riskClassInt),
                aggs: {
                  [applicable_key]: {
                    nested: {
                      path: 'address',
                    },
                    aggs: {
                      inside: {
                        stats: {
                          field: 'address.id',
                        },
                      },
                    },
                  },
                },
              },
            };
          }),
          'bucket_name',
          'query'
        ),
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [
                    // constructNestedExistsQuery('address', 'address.id'),
                    // constructNestedExistsQuery(
                    //   `${constructLocationPerilPath()}`,
                    //   `${constructLocationPerilPath()}.peril`
                    // ),
                    // constructNestedExistsQuery(
                    //   'address.locationProperty.locationAssessments',
                    //   'address.locationProperty.locationAssessments.score'
                    // )
                  ].concat(
                    step.includes('nonexistent')
                      ? {
                        bool: {
                          must_not: constructNestedExistsQuery(
                            'address.locationProperty.locationAssessments',
                            'address.locationProperty.locationAssessments.score'
                          ),
                        },
                      }
                      : []
                  ),
                },
              },
              aggs: {
                ...arrayToObject(
                  []
                    .concat(
                      !step.match(/\d+$/)
                        ? {}
                        : <any>{
                          bucket_name: 'total_stats',
                          query: {
                            filter:
                              constructNestedLocationAssessmentQueryString(
                                extractInt(step.match(/\d+$/)[0])
                              ),
                            aggs: {
                              totals: {
                                nested: {
                                  path: 'address',
                                },
                                aggs: {
                                  inside: {
                                    stats: {
                                      field: 'address.id',
                                    },
                                  },
                                },
                              },
                            },
                          },
                        }
                    )
                    .concat(
                      // The avg stats apply to all rows, not just those with a defined risk class.
                      // That way, there's an avg score for the 'number of locations' row too.
                      // Finalizes UDGB-25.
                      <any>{
                        bucket_name: 'avg_stats',
                        query: {
                          filter: step.match(/\d+$/)
                            ? // risk class filter
                            constructNestedLocationAssessmentQueryString(
                              extractInt(step.match(/\d+$/)[0])
                            )
                            :
                            // default location assessment filter based on the peril/class/strategy dropdowns
                            // fixes UDGB-33
                            constructNestedLocationAssessmentQueryString(),
                          aggs: {
                            totals: constructScriptedAverageScoreQuery(undefined, (window as any).selected_location_peril),
                          },
                        },
                      }
                    )
                    .concat(
                      numericalRanges.ScoreIntervals.map((interval) => {
                        const [gte, lte] = interval;
                        return <any>{
                          bucket_name: `B) Score ${gte}...${lte}`,
                          query: {
                            filter: {
                              bool: {
                                must: [
                                  // for the range chosen in Numeric Range Sliders
                                  constructNestedLocationAssessmentQueryString(
                                    step.match(/\d+$/)
                                      ? extractInt(step.match(/\d+$/)[0])
                                      : undefined,
                                    undefined,
                                    undefined
                                  ),
                                  // for the applicable columnar range
                                  constructNestedLocationAssessmentQueryString(
                                    step.match(/\d+$/)
                                      ? extractInt(step.match(/\d+$/)[0])
                                      : undefined,
                                    undefined,
                                    {
                                      ...generateRangeValuesFromList(gte, lte),
                                    }
                                  ),
                                ],
                              },
                            },
                            aggs: {
                              ...numerical_aggs,
                            },
                          },
                        };
                      })
                    ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;

    case 'AggregatedAddressesPerRC_vsRecommendations_address_based_histogram_chart':
      query_names_as_dict = {
        '1)_number_of_locations': '1)_number_of_locations',
        ...arrayToObject(
          getListOfSelectedLocationPerilRiskClasses().map((rc) => {
            return {
              key: `Risk_Class_${rc < 10 ? '0' + rc : rc}`, // "A" will be extracted later but we wanna keep the order of bucket keys
              val: `Risk_Class_${rc < 10 ? '0' + rc : rc}`,
            };
          }),
          'key',
          'val'
        ),
      };

      numerical_aggs = {
        [query_names_as_dict['1)_number_of_locations']]:
          constructNestedRecommendationValueCountAggFactorAgnostic(undefined, false, undefined, true),
        ...arrayToObject(
          Object.keys(
            filterObjectPropertiesByKeyRegexes(query_names_as_dict, [/^[^1]/g])
          ).map((applicable_key) => {
            const riskClassInt = extractInt(applicable_key.match(/\d+$/)[0]);

            return {
              bucket_name: applicable_key,
              query: {
                filter:
                  constructNestedLocationAssessmentQueryString(riskClassInt),
                aggs: {
                  [applicable_key]:
                    constructNestedRecommendationValueCountAggFactorAgnostic(undefined, false, undefined, true),
                },
              },
            };
          }),
          'bucket_name',
          'query'
        ),
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        const riskClassInt = extractInt(step.match(/\d+$/)?.[0]);

        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [
                    // constructNestedExistsQuery('address', 'address.id'),
                    // constructNestedExistsQuery(
                    //   `${constructLocationPerilPath()}`,
                    //   `${constructLocationPerilPath()}.peril`
                    // ),
                    // constructNestedExistsQuery(
                    //   'address.locationProperty.locationAssessmentsLevel3.keyFactors.keyCriteria.recommendations',
                    //   'address.locationProperty.locationAssessmentsLevel3.keyFactors.keyCriteria.recommendations.statusFeedback'
                    // )
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  []
                    .concat(
                      !riskClassInt
                        ? {}
                        : <any>{
                          bucket_name: 'total_stats',
                          query: {
                            filter:
                              constructNestedLocationAssessmentQueryString(riskClassInt),
                            aggs: {
                              totals: {
                                nested: {
                                  path: 'address',
                                },
                                aggs: {
                                  inside: {
                                    stats: {
                                      field: 'address.id',
                                    },
                                  },
                                },
                              },
                            },
                          },
                        }
                    )
                    .concat(
                      listOfRecommendationStatusFeedbacks.map(
                        (statusKey, ind) => {
                          return <any>{
                            bucket_name: `A) ${ind}) ${statusKey}`,
                            query: {
                              filter: {
                                bool: {
                                  must: [
                                    constructNestedAssessmentTermQuery(
                                      statusKey
                                    ),
                                  ],
                                },
                              },
                              aggs: {
                                ...filterObjectPropertiesByKeyRegexes(numerical_aggs, [
                                  // UDGB-52 only include those aggs that match the currently iterated step
                                  // it their name
                                  new RegExp(safeStringForRegex(step))
                                ]),
                              },
                            },
                          };
                        }
                      )
                    ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;

    case 'AggregatedAddressesPerIC_vsRecommendations_address_based_histogram_chart':
      query_names_as_dict = {
        '1)_number_of_locations': '1)_number_of_locations',
        ...arrayToObject(
          listOfIndustryCategories.map((industry) => {
            return {
              key: industry,
              val: industry,
            };
          }),
          'key',
          'val'
        ),
      };

      numerical_aggs = () => {
        return {
          [query_names_as_dict['1)_number_of_locations']]:
            constructNestedRecommendationValueCountAggFactorAgnostic(),
          ...arrayToObject(
            Object.keys(
              filterObjectPropertiesByKeyRegexes(query_names_as_dict, [
                /\d+\s/g,
              ])
            ).map((applicable_key) => {
              return {
                bucket_name: applicable_key,
                query: {
                  filter: {
                    nested: {
                      path: `offer`,
                      query: {
                        bool: {
                          must: [
                            {
                              term: {
                                'offer.industry': applicable_key,
                              },
                            },
                          ],
                        },
                      },
                    },
                  },
                  aggs: {
                    [applicable_key]:
                      constructNestedRecommendationValueCountAggFactorAgnostic(),
                  },
                },
              };
            }),
            'bucket_name',
            'query'
          ),
        };
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [
                    // constructNestedExistsQuery('address', 'address.id'),
                    // constructNestedExistsQuery(`offer`, `offer.industry`),
                    // constructNestedExistsQuery(
                    //   'address.locationProperty.locationAssessmentsLevel3.keyFactors.keyCriteria.recommendations',
                    //   'address.locationProperty.locationAssessmentsLevel3.keyFactors.keyCriteria.recommendations.statusFeedback'
                    // )
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  []
                    .concat(
                      step.includes('number')
                        ? {}
                        : <any>{
                          bucket_name: 'total_stats',
                          query: {
                            filter: {
                              nested: {
                                path: `offer`,
                                query: {
                                  bool: {
                                    must: [
                                      {
                                        term: {
                                          'offer.industry': step,
                                        },
                                      },
                                    ],
                                  },
                                },
                              },
                            },
                            aggs: {
                              totals: {
                                nested: {
                                  path: 'address',
                                },
                                aggs: {
                                  inside: {
                                    stats: {
                                      field: 'address.id',
                                    },
                                  },
                                },
                              },
                            },
                          },
                        }
                    )
                    .concat(
                      listOfRecommendationStatusFeedbacks.map(
                        (statusKey, ind) => {
                          return <any>{
                            bucket_name: `A) ${ind}) ${statusKey}`,
                            query: {
                              filter: {
                                bool: {
                                  must: [
                                    constructNestedAssessmentTermQuery(
                                      statusKey
                                    ),
                                  ],
                                },
                              },
                              aggs: {
                                ...numerical_aggs(),
                              },
                            },
                          };
                        }
                      )
                    ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;
    case 'AggregatedAddressesPerIC_vsPML_address_based_histogram_chart':
      query_names_as_dict = {
        '1)_number_of_locations': '1)_number_of_locations',
        ...arrayToObject(
          listOfIndustryCategories.map((industry) => {
            return {
              key: industry,
              val: industry,
            };
          }),
          'key',
          'val'
        ),
      };

      numerical_aggs = {
        [query_names_as_dict['1)_number_of_locations']]: {
          nested: {
            path: 'address',
          },
          aggs: {
            inside: {
              stats: {
                field: 'address.id',
              },
            },
          },
        },
        ...arrayToObject(
          Object.keys(
            filterObjectPropertiesByKeyRegexes(query_names_as_dict, [/\d+\s/g])
          ).map((applicable_key) => {
            return {
              bucket_name: applicable_key,
              query: {
                filter: {
                  nested: {
                    path: `offer`,
                    query: {
                      bool: {
                        must: [
                          {
                            term: {
                              'offer.industry': applicable_key,
                            },
                          },
                        ],
                      },
                    },
                  },
                },
                aggs: {
                  [applicable_key]: {
                    nested: {
                      path: 'address',
                    },
                    aggs: {
                      inside: {
                        stats: {
                          field: 'address.id',
                        },
                      },
                    },
                  },
                },
              },
            };
          }),
          'bucket_name',
          'query'
        ),
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [
                    // constructNestedExistsQuery('address', 'address.id'),
                    // constructNestedExistsQuery(`offer`, `offer.industry`),
                    // constructNestedExistsQuery(
                    //   'address.locationProperty',
                    //   'address.locationProperty.PML_$CURRENCY$_AdjustedForShare'
                    // )
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  []
                    .concat(
                      step.includes('number')
                        ? {}
                        : <any>{
                          bucket_name: 'total_stats',
                          query: {
                            filter: {
                              nested: {
                                path: `offer`,
                                query: {
                                  bool: {
                                    must: [
                                      {
                                        term: {
                                          'offer.industry': step,
                                        },
                                      },
                                    ],
                                  },
                                },
                              },
                            },
                            aggs: {
                              totals: {
                                nested: {
                                  path: 'address',
                                },
                                aggs: {
                                  inside: {
                                    stats: {
                                      field: 'address.id',
                                    },
                                  },
                                },
                              },
                            },
                          },
                        }
                    )
                    .concat(
                      numericalRanges.UniqaPMLintervals.map(
                        (interval, index) => {
                          const [gte, lte] = interval;
                          return <any>{
                            // user LETTERS[index] to keep order when sorting ES dict response
                            bucket_name: `A) Uniqa PML ${LETTERS[index]
                              }) ${gte}...${lte || ''} Mio.`,
                            query: {
                              filter: {
                                bool: {
                                  must: [
                                    {
                                      nested: {
                                        path: 'address.locationProperty',
                                        query: {
                                          bool: {
                                            must: {
                                              range: {
                                                'address.locationProperty.PML_$CURRENCY$_AdjustedForShare':
                                                {
                                                  ...generateRangeValuesFromList(
                                                    gte,
                                                    lte,
                                                    1e6
                                                  ),
                                                },
                                              },
                                            },
                                          },
                                        },
                                      },
                                    },
                                  ],
                                },
                              },
                              aggs: {
                                ...numerical_aggs,
                              },
                            },
                          };
                        }
                      )
                    ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;

    case 'AggregatedAddressesPerIC_vsIL_address_based_histogram_chart':
      query_names_as_dict = {
        '1)_number_of_locations': '1)_number_of_locations',
        ...arrayToObject(
          listOfIndustryCategories.map((industry) => {
            return {
              key: industry,
              val: industry,
            };
          }),
          'key',
          'val'
        ),
      };

      numerical_aggs = {
        [query_names_as_dict['1)_number_of_locations']]: {
          nested: {
            path: 'address',
          },
          aggs: {
            inside: {
              stats: {
                field: 'address.id',
              },
            },
          },
        },
        ...arrayToObject(
          Object.keys(
            filterObjectPropertiesByKeyRegexes(query_names_as_dict, [/\d+\s/g])
          ).map((applicable_key) => {
            return {
              bucket_name: applicable_key,
              query: {
                filter: {
                  nested: {
                    path: `offer`,
                    query: {
                      bool: {
                        must: [
                          {
                            term: {
                              'offer.industry': applicable_key,
                            },
                          },
                        ],
                      },
                    },
                  },
                },
                aggs: {
                  [applicable_key]: {
                    nested: {
                      path: 'address',
                    },
                    aggs: {
                      inside: {
                        stats: {
                          field: 'address.id',
                        },
                      },
                    },
                  },
                },
              },
            };
          }),
          'bucket_name',
          'query'
        ),
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [
                    // constructNestedExistsQuery('address', 'address.id'),
                    // constructNestedExistsQuery(`offer`, `offer.industry`),
                    // constructNestedExistsQuery(
                    //   'address.locationProperty.locationAssessments',
                    //   'address.locationProperty.locationAssessments.level'
                    // )
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  []
                    .concat(
                      step.includes('number')
                        ? {}
                        : <any>{
                          bucket_name: 'total_stats',
                          query: {
                            filter: {
                              nested: {
                                path: `offer`,
                                query: {
                                  bool: {
                                    must: [
                                      {
                                        term: {
                                          'offer.industry': step,
                                        },
                                      },
                                    ],
                                  },
                                },
                              },
                            },
                            aggs: {
                              totals: {
                                nested: {
                                  path: 'address',
                                },
                                aggs: {
                                  inside: {
                                    stats: {
                                      field: 'address.id',
                                    },
                                  },
                                },
                              },
                            },
                          },
                        }
                    )
                    .concat(
                      [1, 2, 3].map((infoLevel) => {
                        return <any>{
                          bucket_name: `A) infoLevel ${infoLevel}`,
                          query: {
                            filter:
                              constructNestedLocationAssessmentQueryString(
                                undefined,
                                infoLevel
                              ),
                            aggs: {
                              ...numerical_aggs,
                            },
                          },
                        };
                      })
                    ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;
    case 'AggregatedAddressesPerIC_vsScore_address_based_histogram_chart':
      query_names_as_dict = {
        '1)_number_of_locations': '1)_number_of_locations',
        '2)_nonexistent_score': '2)_nonexistent_score',
        ...arrayToObject(
          listOfIndustryCategories.map((industry) => {
            return {
              key: industry,
              val: industry,
            };
          }),
          'key',
          'val'
        ),
      };

      numerical_aggs = {
        [query_names_as_dict['1)_number_of_locations']]: {
          nested: {
            path: 'address',
          },
          aggs: {
            inside: {
              stats: {
                field: 'address.id',
              },
            },
          },
        },
        [query_names_as_dict['2)_nonexistent_score']]: {
          nested: {
            path: 'address',
          },
          aggs: {
            inside: {
              stats: {
                field: 'address.id',
              },
            },
          },
        },
        ...arrayToObject(
          Object.keys(
            filterObjectPropertiesByKeyRegexes(query_names_as_dict, [/\d+\s/g])
          ).map((applicable_key) => {
            return {
              bucket_name: applicable_key,
              query: {
                filter: {
                  nested: {
                    path: `offer`,
                    query: {
                      bool: {
                        must: [
                          {
                            term: {
                              'offer.industry': applicable_key,
                            },
                          },
                        ],
                      },
                    },
                  },
                },
                aggs: {
                  [applicable_key]: {
                    nested: {
                      path: 'address',
                    },
                    aggs: {
                      inside: {
                        stats: {
                          field: 'address.id',
                        },
                      },
                    },
                  },
                },
              },
            };
          }),
          'bucket_name',
          'query'
        ),
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [
                    // constructNestedExistsQuery('address', 'address.id'),
                    // constructNestedExistsQuery(`offer`, `offer.industry`),
                    // constructNestedExistsQuery(
                    //   'address.locationProperty.locationAssessments',
                    //   'address.locationProperty.locationAssessments.score'
                    // )
                  ].concat(
                    step.includes('nonexistent')
                      ? {
                        bool: {
                          must_not: constructNestedExistsQuery(
                            'address.locationProperty.locationAssessments',
                            'address.locationProperty.locationAssessments.score'
                          ),
                        },
                      }
                      : []
                  ),
                },
              },
              aggs: {
                ...arrayToObject(
                  []
                    .concat(
                      step.includes('number') || step.includes('nonexistent')
                        ? {}
                        : <any>{
                          bucket_name: 'total_stats',
                          query: {
                            filter: {
                              nested: {
                                path: `offer`,
                                query: {
                                  bool: {
                                    must: [
                                      {
                                        term: {
                                          'offer.industry': step,
                                        },
                                      },
                                    ],
                                  },
                                },
                              },
                            },
                            aggs: {
                              totals: {
                                nested: {
                                  path: 'address',
                                },
                                aggs: {
                                  inside: {
                                    stats: {
                                      field: 'address.id',
                                    },
                                  },
                                },
                              },
                            },
                          },
                        }
                    )
                    .concat(
                      numericalRanges.ScoreIntervals.map((interval) => {
                        const [gte, lte] = interval;
                        return <any>{
                          bucket_name: `B) Score ${gte}...${lte}`,
                          query: {
                            filter: {
                              bool: {
                                must: [
                                  constructNestedLocationAssessmentQueryString(
                                    undefined,
                                    undefined,
                                    {
                                      ...generateRangeValuesFromList(gte, lte),
                                    }
                                  ),
                                ],
                              },
                            },
                            aggs: {
                              ...numerical_aggs,
                            },
                          },
                        };
                      })
                    ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;

    case 'AggregatedAddressesPerScorePerPML_address_based_histogram_chart':
      // The generated scores table doesn't help anyone so we're only aggregating the bare minimum
      // for this query to go thru and will hide the generated table in HTML.
      query_names_as_dict = {
        '1)_number_of_locations': '1)_number_of_locations',
      };

      numerical_aggs = {
        [query_names_as_dict['1)_number_of_locations']]: {
          nested: {
            path: 'address',
          },
          aggs: {
            inside: {
              stats: {
                field: 'address.id',
              },
            },
          },
        },
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [],
                },
              },
              aggs: {
                ...arrayToObject(
                  [].concat(
                    !step.match(/\d+$/)
                      ? {}
                      : <any>{
                        bucket_name: 'total_stats',
                        query: {
                          filter:
                            constructNestedLocationAssessmentQueryString(
                              undefined,
                              undefined,
                              query_names_as_dict[step]
                            ),

                          aggs: {
                            totals: {
                              nested: {
                                path: 'address',
                              },
                              aggs: {
                                inside: {
                                  stats: {
                                    field: 'address.id',
                                  },
                                },
                              },
                            },
                          },
                        },
                      }
                  ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });

      include_source = {
        includes: [
          'id',
          'offer.id',
          'offer.number',
          'lob.id',
          'lob.lineOfBusiness',
          'lob.localUnderwriters.fullName',
          'address.i*',
          'address.c*',
          'address.s*',
          'address.t*',
          'offer.industry',
          'offer.cr*',
          'offer.upd*',
          'offer.client.name',
          'lob.share',
          'lob.premium*',
          'lob.totalSumInsured*',
          'lob.typeOfBusiness',
          'lob.status',
          'address.locationProperty.PML_*',
          'address.locationProperty.locationAssessments.level',
          'address.locationProperty.locationAssessments.peril',
          'address.locationProperty.locationAssessments.riskClass',
          'address.locationProperty.locationAssessments.score',
          'address.locationProperty.locationAssessments.asIfScore',
        ],
        excludes: ['address.geoPoint', 'address.ratingValues'],
      };

      q.query = {
        bool: {
          filter: [
            // {
            //   nested: {
            //     path: 'address.locationProperty.locationAssessments',
            //     query: {
            //       exists: {
            //         field: 'address.locationProperty.locationAssessments.score',
            //       },
            //     },
            //   },
            // },
            ...date_field_queries,
          ],
        },
      };

      break;

    case 'AggregatedAddressesPerAsIfScorePerPML_address_based_histogram_chart':
      query_names_as_dict = {
        '1)_number_of_locations': '1)_number_of_locations',
        ...arrayToObject(
          numericalRanges.ListOfScores.map((rc) => {
            return {
              key: `Score_${rc.toString().padStart(3, '0')}`,
              val: createScoreRangeParameters(rc),
            };
          }),
          'key',
          'val'
        ),
      };

      numerical_aggs = {
        [query_names_as_dict['1)_number_of_locations']]: {
          nested: {
            path: 'address',
          },
          aggs: {
            inside: {
              stats: {
                field: 'address.id',
              },
            },
          },
        },
        ...arrayToObject(
          Object.keys(
            filterObjectPropertiesByKeyRegexes(query_names_as_dict, [/^[^1]/g])
          ).map((applicable_key) => {
            return {
              bucket_name: applicable_key,
              query: {
                filter: constructNestedLocationAssessmentQueryString(
                  undefined,
                  undefined,
                  undefined,
                  undefined,
                  undefined,
                  undefined,
                  query_names_as_dict[applicable_key]
                ),
                aggs: {
                  [applicable_key]: {
                    nested: {
                      path: 'address',
                    },
                    aggs: {
                      inside: {
                        stats: {
                          field: 'address.id',
                        },
                      },
                    },
                  },
                },
              },
            };
          }),
          'bucket_name',
          'query'
        ),
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [
                    // constructNestedExistsQuery('address', 'address.id'),
                    // constructNestedExistsQuery(
                    //   'address.locationProperty.locationAssessments',
                    //   'address.locationProperty.locationAssessments.asIfScore'
                    // ),
                    // constructNestedExistsQuery(
                    //   'address.locationProperty',
                    //   'address.locationProperty.PML_$CURRENCY$_AdjustedForShare'
                    // )
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  []
                    .concat(
                      !step.match(/\d+$/)
                        ? {}
                        : <any>{
                          bucket_name: 'total_stats',
                          query: {
                            filter:
                              constructNestedLocationAssessmentQueryString(
                                undefined,
                                undefined,
                                undefined,
                                undefined,
                                undefined,
                                undefined,
                                query_names_as_dict[step]
                              ),

                            aggs: {
                              totals: {
                                nested: {
                                  path: 'address',
                                },
                                aggs: {
                                  inside: {
                                    stats: {
                                      field: 'address.id',
                                    },
                                  },
                                },
                              },
                            },
                          },
                        }
                    )
                    .concat(
                      numericalRanges.UniqaPMLintervals.map(
                        (interval, index) => {
                          const [gte, lte] = interval;
                          return <any>{
                            // user LETTERS[index] to keep order when sorting ES dict response
                            bucket_name: `A) Uniqa PML ${LETTERS[index]
                              }) ${gte}...${lte || ''} Mio.`,
                            query: {
                              filter: {
                                bool: {
                                  must: [
                                    {
                                      nested: {
                                        path: 'address.locationProperty',
                                        query: {
                                          bool: {
                                            must: {
                                              range: {
                                                'address.locationProperty.PML_$CURRENCY$_AdjustedForShare':
                                                {
                                                  ...generateRangeValuesFromList(
                                                    gte,
                                                    lte,
                                                    1e6
                                                  ),
                                                },
                                              },
                                            },
                                          },
                                        },
                                      },
                                    },
                                  ],
                                },
                              },
                              aggs: {
                                ...filterObjectPropertiesByKeyRegexes(
                                  numerical_aggs,
                                  [
                                    /^\d\)/g,
                                    new RegExp(
                                      `Score_${(step.match(/\d+$/) || ['^d\\)'])[0]
                                      }`,
                                      'g'
                                    ),
                                  ]
                                ),
                              },
                            },
                          };
                        }
                      )
                    ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });

      include_source = {
        includes: [
          'id',
          'offer.id',
          'offer.number',
          'lob.id',
          'lob.lineOfBusiness',
          'lob.localUnderwriters.fullName',
          'address.i*',
          'address.c*',
          'address.s*',
          'address.t*',
          'offer.industry',
          'offer.cr*',
          'offer.upd*',
          'offer.client.name',
          'lob.share',
          'lob.premium*',
          'lob.totalSumInsured*',
          'lob.typeOfBusiness',
          'address.locationProperty.PML_*',
          'address.locationProperty.locationAssessments.level',
          'address.locationProperty.locationAssessments.peril',
          'address.locationProperty.locationAssessments.riskClass',
          'address.locationProperty.locationAssessments.score',
          'address.locationProperty.locationAssessments.asIfScore',
        ],
        excludes: ['address.geoPoint', 'address.ratingValues'],
      };

      q.query = {
        bool: {
          filter: [
            // {
            //   nested: {
            //     path: 'address.locationProperty.locationAssessments',
            //     query: {
            //       exists: {
            //         field:
            //           'address.locationProperty.locationAssessments.asIfScore',
            //       },
            //     },
            //   },
            // },
            ...date_field_queries,
          ],
        },
      };
      break;

    case 'AggregatedAddressesPerAsIfScorePerPML_EML_Ratio_address_based_histogram_chart':
      query_names_as_dict = {
        '1)_number_of_locations': '1)_number_of_locations',
        ...arrayToObject(
          numericalRanges.ListOfScores.map((rc) => {
            return {
              key: `Score_${rc.toString().padStart(3, '0')}`,
              val: createScoreRangeParameters(rc),
            };
          }),
          'key',
          'val'
        ),
      };

      numerical_aggs = {
        [query_names_as_dict['1)_number_of_locations']]: {
          nested: {
            path: 'address',
          },
          aggs: {
            inside: {
              stats: {
                field: 'address.id',
              },
            },
          },
        },
        ...arrayToObject(
          Object.keys(
            filterObjectPropertiesByKeyRegexes(query_names_as_dict, [/^[^1]/g])
          ).map((applicable_key) => {
            return {
              bucket_name: applicable_key,
              query: {
                filter: constructNestedLocationAssessmentQueryString(
                  undefined,
                  undefined,
                  undefined,
                  undefined,
                  undefined,
                  undefined,
                  query_names_as_dict[applicable_key]
                ),
                aggs: {
                  [applicable_key]: {
                    nested: {
                      path: 'address',
                    },
                    aggs: {
                      inside: {
                        stats: {
                          field: 'address.id',
                        },
                      },
                    },
                  },
                },
              },
            };
          }),
          'bucket_name',
          'query'
        ),
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [
                    // constructNestedExistsQuery('address', 'address.id'),
                    // constructNestedExistsQuery(
                    //   'address.locationProperty.locationAssessments',
                    //   'address.locationProperty.locationAssessments.asIfScore'
                    // ),
                    // constructNestedExistsQuery(
                    //   'address.locationProperty',
                    //   'address.locationProperty.PML_$CURRENCY$_AdjustedForShare'
                    // ),
                    // constructNestedExistsQuery(
                    //   'address.locationProperty',
                    //   'address.locationProperty.EML_$CURRENCY$_AdjustedForShare'
                    // )
                  ],
                },
              },
              aggs: {
                ...arrayToObject(
                  []
                    .concat(
                      !step.match(/\d+$/)
                        ? {}
                        : <any>{
                          bucket_name: 'total_stats',
                          query: {
                            filter:
                              constructNestedLocationAssessmentQueryString(
                                undefined,
                                undefined,
                                undefined,
                                undefined,
                                undefined,
                                undefined,
                                query_names_as_dict[step]
                              ),
                            aggs: {
                              totals: {
                                nested: {
                                  path: 'address',
                                },
                                aggs: {
                                  inside: {
                                    stats: {
                                      field: 'address.id',
                                    },
                                  },
                                },
                              },
                            },
                          },
                        }
                    )
                    .concat(
                      numericalRanges.PML_EML_Ratio_Intervals.map(
                        (interval, index) => {
                          const { min, max } = interval;
                          const is_end = max === 1;

                          return <any>{
                            // user LETTERS[index] to keep order when sorting ES dict response
                            bucket_name: `A) PML/EML ${LETTERS[index]}) ${max}`,

                            query: {
                              filter: {
                                bool: {
                                  must: [
                                    {
                                      nested: {
                                        path: 'address.locationProperty',
                                        query: {
                                          bool: {
                                            must: {
                                              script: {
                                                script: {
                                                  source: `
                                                    def ratio = (doc['address.locationProperty.PML_$CURRENCY$_AdjustedForShare'].value / doc['address.locationProperty.EML_$CURRENCY$_AdjustedForShare'].value);
                                                    return ratio >= ${min} && ratio <${is_end ? '=' : ''
                                                    }${max};
                                                  `,
                                                  lang: 'painless',
                                                },
                                              },
                                            },
                                          },
                                        },
                                      },
                                    },
                                  ],
                                },
                              },
                              aggs: {
                                ...filterObjectPropertiesByKeyRegexes(
                                  numerical_aggs,
                                  [
                                    /^\d\)/g,
                                    new RegExp(
                                      `Score_${(step.match(/\d+$/) || ['^d\\)'])[0]
                                      }`,
                                      'g'
                                    ),
                                  ]
                                ),
                              },
                            },
                          };
                        }
                      )
                    ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });

      include_source = {
        includes: [
          'id',
          'offer.id',
          'offer.number',
          'lob.id',
          'lob.lineOfBusiness',
          'lob.localUnderwriters.fullName',
          'address.i*',
          'address.c*',
          'address.s*',
          'address.t*',
          'offer.industry',
          'offer.cr*',
          'offer.upd*',
          'offer.client.name',
          'lob.share',
          'lob.premium*',
          'lob.totalSumInsured*',
          'lob.typeOfBusiness',
          'address.locationProperty.PML_*',
          'address.locationProperty.EML_*',
          'address.locationProperty.locationAssessments.level',
          'address.locationProperty.locationAssessments.peril',
          'address.locationProperty.locationAssessments.riskClass',
          'address.locationProperty.locationAssessments.score',
          'address.locationProperty.locationAssessments.asIfScore',
        ],
        excludes: ['address.geoPoint', 'address.ratingValues'],
      };

      q.query = {
        bool: {
          filter: [
            ...date_field_queries,
            {
              bool: {
                must: [
                  {
                    nested: {
                      path: 'address.locationProperty',
                      query: {
                        exists: {
                          field:
                            'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                        },
                      },
                    },
                  },
                ],
                must_not: [
                  {
                    nested: {
                      path: 'address.locationProperty',
                      query: {
                        term: {
                          'address.locationProperty.PML_$CURRENCY$_AdjustedForShare': 0,
                        },
                      },
                    },
                  },
                  {
                    nested: {
                      path: 'address.locationProperty',
                      query: {
                        term: {
                          'address.locationProperty.EML_$CURRENCY$_AdjustedForShare': 0,
                        },
                      },
                    },
                  },
                ],
              },
            },
          ],
        },
      };
      break;

    case 'AggregatedAddressesPerRiskScore_vsPremiums_address_based_histogram_chart':
      query_names_as_dict = {
        ...arrayToObject(
          numericalRanges.ScoreIntervals10X.map((interval, index) => {
            const [gte, lte] = interval;
            return {
              key: `score ${gte}...${lte}`,
              val: interval,
            };
          }),
          'key',
          'val'
        ),
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        const extract = /(\d+)...(\d+)/.exec(step);
        const [gte, lte] = [extract[1], extract[2]].map(extractInt);

        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [
                    // constructNestedExistsQuery('address', 'address.id'),
                    // constructNestedExistsQuery(
                    //   'address.locationProperty.locationAssessments',
                    //   'address.locationProperty.locationAssessments.score'
                    // ),
                    // constructNestedExistsQuery('lob.rating', 'lob.rating.mTotalPremium_$CURRENCY$'),
                    // constructNestedExistsQuery('lob.rating', 'lob.rating.totalPremium_$CURRENCY$'),
                    constructNestedRangeQuery(
                      `lob.rating.ratingToolPerils.${(window as any).selected_peril
                      }`,
                      `lob.rating.ratingToolPerils.${(window as any).selected_peril
                      }.avgScore`,
                      {
                        gte,
                        lte,
                      }
                    ),
                  ],
                },
              },
              aggs: {
                [`${step} mprem`]: {
                  nested: {
                    path: 'lob.rating',
                  },
                  aggs: {
                    inside: {
                      sum: {
                        field: 'lob.rating.mTotalPremium_$CURRENCY$',
                      },
                    },
                  },
                },
                [`${step} tprem`]: {
                  nested: {
                    path: 'lob.rating',
                  },
                  aggs: {
                    inside: {
                      sum: {
                        field: 'lob.rating.totalPremium_$CURRENCY$',
                      },
                    },
                  },
                },
              },
            },
          },
        };
      });
      break;

    case 'AggregatedAddressesPerRiskScore_vsPremiums_IndividualLocations_address_based_histogram_chart':
      query_names_as_dict = {
        ...arrayToObject(
          numericalRanges.ScoreIntervals.map((interval, index) => {
            const [gte, lte] = interval;
            return {
              key: `score ${gte}...${lte}`,
              val: interval,
            };
          }),
          'key',
          'val'
        ),
      };

      const arr_of_musts = [
        constructNestedExistsQuery(
          'lob.rating',
          'lob.rating.mTotalPremium_$CURRENCY$'
        ),
        constructNestedExistsQuery(
          'lob.rating',
          'lob.rating.totalPremium_$CURRENCY$'
        ),
      ];

      queries = Object.keys(query_names_as_dict).map((step) => {
        const extract = /(\d+)...(\d+)/.exec(step);
        const [gte, lte] = [extract[1], extract[2]].map(extractInt);

        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [
                    // constructNestedExistsQuery('address', 'address.id'),
                    // constructNestedExistsQuery(
                    //   'address.locationProperty.locationAssessments',
                    //   'address.locationProperty.locationAssessments.score'
                    // ),
                    // constructNestedExistsQuery('lob.rating', 'lob.rating.mTotalPremium_$CURRENCY$'),
                    // constructNestedExistsQuery('lob.rating', 'lob.rating.totalPremium_$CURRENCY$'),
                    ...arr_of_musts,
                    constructNestedRangeQuery(
                      `lob.rating.ratingToolPerils.${(window as any).selected_peril
                      }`,
                      `lob.rating.ratingToolPerils.${(window as any).selected_peril
                      }.avgScore`,
                      {
                        gte,
                        lte,
                      }
                    ),
                  ],
                },
              },
              aggs: {
                [`${step} mprem`]: {
                  nested: {
                    path: 'lob.rating',
                  },
                  aggs: {
                    inside: {
                      sum: {
                        field: 'lob.rating.mTotalPremium_$CURRENCY$',
                      },
                    },
                  },
                },
                [`${step} tprem`]: {
                  nested: {
                    path: 'lob.rating',
                  },
                  aggs: {
                    inside: {
                      sum: {
                        field: 'lob.rating.totalPremium_$CURRENCY$',
                      },
                    },
                  },
                },
              },
            },
          },
        };
      });

      q._source = {
        includes: [
          'id',
          'offer.id',
          'offer.number',
          'lob.id',
          'lob.lineOfBusiness',
          'lob.localUnderwriters.fullName',
          'address.i*',
          'address.c*',
          'address.s*',
          'address.t*',
          'offer.industry',
          'offer.cr*',
          'offer.upd*',
          'offer.client.name',
          'lob.share',
          'lob.premium*',
          'lob.totalSumInsured*',
          'address.locationProperty.locationAssessments.score',
          // superscore
          'lob.rating.superScore',
          // mprem
          'lob.rating.mTotalPremium_$CURRENCY$',
          // tprem
          'lob.rating.totalPremium_$CURRENCY$',
        ],
        excludes: ['address.geoPoint', 'address.ratingValues'],
      };

      q.query['bool'] = {
        must: arr_of_musts,
      };

      // TODO CHANGE TO 90e3
      q.size = 50e3;
      break;

    case 'AggregatedAddressesPerRecommendationKinds_vsPriority_address_based_histogram_chart':
      const merged = mergeCriteriaSpecificAssessmentRecommendations();

      query_names_as_dict = {
        '1)_number_of_locations': '1)_number_of_locations',
        ...arrayToObject(
          merged.map((r_kind) => {
            return {
              key: `${r_kind}`,
            };
          }),
          'key',
          'key'
        ),
      };

      numerical_aggs = (except?) => {
        return {
          [query_names_as_dict['1)_number_of_locations']]: {
            nested: {
              path: 'address',
            },
            aggs: {
              inside: {
                stats: {
                  field: 'address.id',
                },
              },
            },
          },
          ...arrayToObject(
            Object.keys(
              filterObjectPropertiesByKeyRegexes(query_names_as_dict, [
                /^[^1]/g,
              ])
            )
              .filter((akey) => (except ? akey === except : true))
              .map((applicable_key) => {
                const r_kind = applicable_key;
                const [key_factor, key_criterion, recommendation] =
                  r_kind.split(' | ');

                const step_query = {
                  ...constructNested_CriteriaSpecificAssessmentRecommendation_TermQuery(
                    key_factor,
                    key_criterion,
                    recommendation
                  ),
                };

                return {
                  bucket_name: applicable_key,
                  query: {
                    filter: step_query,
                    aggs: {
                      [applicable_key]: {
                        nested: {
                          path: 'address',
                        },
                        aggs: {
                          inside: {
                            stats: {
                              field: 'address.id',
                            },
                          },
                          // UDGB-52 use inner hits too to figure out the true priority counts
                          // since nested array aggs aren't reliable
                          inner_hits: {
                            top_hits: {
                              size: 1e3,
                              _source: [
                                `address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril}`,
                              ],
                            },
                          },
                        },
                      },
                    },
                  },
                };
              }),
            'bucket_name',
            'query'
          ),
        };
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        const r_kind =
          step !== query_names_as_dict['1)_number_of_locations'] && step;

        let key_factor, key_criterion, recommendation;

        let step_query;
        if (r_kind) {
          [key_factor, key_criterion, recommendation] = r_kind.split(' | ');
          step_query = {
            ...constructNested_CriteriaSpecificAssessmentRecommendation_TermQuery(
              key_factor,
              key_criterion,
              recommendation
            ),
          };
        } else {
          step_query = {
            match_all: {},
          };

          return {
            aggs: {
              [`histograms_${step}`]: {
                filter: {
                  bool: {
                    must: [step_query],
                  },
                },
              },
            },
          };
        }

        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [step_query],
                },
              },
              aggs: {
                ...arrayToObject(
                  LISTS_OF_HISTOGRAM_CATEGORIES.listOfRecommendationPriorities.map(
                    (priority, ind) => {
                      return <any>{
                        bucket_name: `A) ${ind}) ${priority}`,
                        query: {
                          filter: {
                            bool: {
                              must: [
                                step_query,
                                constructNested_CriteriaSpecific_RecommendationPriority_TermQuery(
                                  key_factor,
                                  key_criterion,
                                  priority
                                ),
                              ],
                            },
                          },
                          aggs: {
                            ...numerical_aggs(r_kind),
                          },
                        },
                      };
                    }
                  ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;

    case 'AggregatedAddressesPerRecommendationKinds_vsRecommendations_address_based_histogram_chart':
      const merged_recommendations =
        mergeCriteriaSpecificAssessmentRecommendations();

      query_names_as_dict = {
        '1)_number_of_locations': '1)_number_of_locations',
        ...arrayToObject(
          merged_recommendations.map((r_kind) => {
            return {
              key: `${r_kind}`,
            };
          }),
          'key',
          'key'
        ),
      };

      numerical_aggs = (except?) => {
        return {
          [query_names_as_dict['1)_number_of_locations']]:
            constructNestedRecommendationValueCountAggFactorAgnostic(),
          ...arrayToObject(
            Object.keys(
              filterObjectPropertiesByKeyRegexes(query_names_as_dict, [
                /^[^1]/g,
              ])
            )
              .filter((akey) => (except ? akey === except : true))
              .map((applicable_key) => {
                const r_kind = applicable_key;
                const [key_factor, key_criterion, recommendation] =
                  r_kind.split(' | ');

                const step_query = {
                  ...constructNested_CriteriaSpecificAssessmentRecommendation_TermQuery(
                    key_factor,
                    key_criterion,
                    recommendation
                  ),
                };

                return {
                  bucket_name: applicable_key,
                  query: {
                    filter: step_query,
                    aggs: {
                      [applicable_key]:
                        constructNestedRecommendationValueCountAggFactorAgnostic(
                          key_criterion,
                          true,
                          recommendation
                        ),
                    },
                  },
                };
              }),
            'bucket_name',
            'query'
          ),
        };
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        const r_kind =
          step !== query_names_as_dict['1)_number_of_locations'] && step;

        let key_factor, key_criterion, recommendation;

        let step_query;
        if (r_kind) {
          [key_factor, key_criterion, recommendation] = r_kind.split(' | ');
          step_query = {
            ...constructNested_CriteriaSpecificAssessmentRecommendation_TermQuery(
              key_factor,
              key_criterion,
              recommendation
            ),
          };
        } else {
          step_query = {
            match_all: {},
          };

          return {
            aggs: {
              [`histograms_${step}`]: {
                filter: {
                  bool: {
                    must: [step_query],
                  },
                },
              },
            },
          };
        }

        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [step_query],
                },
              },
              aggs: {
                ...arrayToObject(
                  []
                    .concat(
                      !step.match(/\d+$/)
                        ? {}
                        : <any>{
                          bucket_name: 'total_stats',
                          query: {
                            filter: step_query,
                            aggs: {
                              totals: {
                                nested: {
                                  path: 'address',
                                },
                                aggs: {
                                  inside: {
                                    stats: {
                                      field: 'address.id',
                                    },
                                  },
                                },
                              },
                            },
                          },
                        }
                    )
                    .concat(
                      listOfRecommendationStatusFeedbacks.map(
                        (statusKey, ind) => {
                          return <any>{
                            bucket_name: `A) ${ind}) ${statusKey}`,
                            query: {
                              filter: {
                                bool: {
                                  must: [
                                    step_query,
                                    constructNested_CriteriaSpecific_RecommendationStatus_TermQuery(
                                      key_factor,
                                      key_criterion,
                                      statusKey
                                    ),
                                  ],
                                },
                              },
                              aggs: {
                                ...numerical_aggs(r_kind),
                              },
                            },
                          };
                        }
                      )
                    ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;
    case 'AggregatedAddressesPerRecommendationKinds_vsLocationCounts_address_based_histogram_chart':
      query_names_as_dict = {
        '1)_number_of_locations': '1)_number_of_locations',
      };

      numerical_aggs = {
        [query_names_as_dict['1)_number_of_locations']]: {
          nested: {
            path: 'address',
          },
          aggs: {
            inside: {
              stats: {
                field: 'address.id',
              },
            },
          },
        },
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [],
                },
              },
              aggs: {
                ...arrayToObject(
                  mergeCriteriaSpecificAssessmentRecommendations().map(
                    (r_kind) => {
                      const [key_factor, key_criterion, recommendation] =
                        r_kind.split(' | ');
                      return <any>{
                        bucket_name: `${r_kind}`,
                        query: {
                          filter: {
                            bool: {
                              must: [
                                constructNested_CriteriaSpecificAssessmentRecommendation_TermQuery(
                                  key_factor,
                                  key_criterion,
                                  recommendation
                                ),
                              ],
                            },
                          },
                          aggs: {
                            ...numerical_aggs,
                            inner_hits: {
                              top_hits: {
                                size: 1e3,
                                _source: [
                                  'offer.number',
                                  'offer.client.name',
                                  'address.id',
                                  'address.city',
                                  'address.street',
                                  'address.country',
                                  'address.zipCode',
                                  'address.houseNumber',
                                  'address.coordinates',
                                  'lob.id',
                                  'lob.typeOfBusiness',
                                  'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',
                                  `lob.uniqaPML_$CURRENCY$`,
                                  `address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
                                  }`,
                                ],
                              },
                            },
                          },
                        },
                      };
                    }
                  ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;
    case 'AggregatedAddressesPerRecommendationKinds_vsNegativeCondition_address_based_histogram_chart':
      query_names_as_dict = {
        '1)_number_of_locations': '1)_number_of_locations',
      };

      numerical_aggs = {
        [query_names_as_dict['1)_number_of_locations']]: {
          nested: {
            path: 'address',
          },
          aggs: {
            inside: {
              stats: {
                field: 'address.id',
              },
            },
          },
        },
      };

      queries = Object.keys(query_names_as_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [],
                },
              },
              aggs: {
                ...arrayToObject(
                  mergeCriteriaSpecificNegativeAspects().map((aspect_group) => {
                    const [key_factor, key_criterion, aspect] =
                      aspect_group.split(' | ');

                    return <any>{
                      bucket_name: `${aspect_group}`,
                      query: {
                        filter: {
                          bool: {
                            must: [
                              constructNestedAssessmentNegativeAspectTermQuery(
                                key_factor,
                                key_criterion,
                                aspect
                              ),
                            ],
                          },
                        },
                        aggs: {
                          ...numerical_aggs,
                          inner_hits: {
                            top_hits: {
                              size: 500,
                              _source: [
                                'offer.number',
                                'offer.client.name',
                                'address.id',
                                'address.city',
                                'address.street',
                                'address.country',
                                'address.zipCode',
                                'address.houseNumber',
                                'address.coordinates',
                                'lob.id',
                                'lob.typeOfBusiness',
                                'lob.totalSumInsured_$CURRENCY$_AdjustedForShare',
                                `lob.uniqaPML_$CURRENCY$`,
                                `address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
                                }`,
                              ],
                            },
                          },
                        },
                      },
                    };
                  }),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;

    case 'AggregatedAddressesPerEvaluations_vsProbabilityCriterias_address_based_histogram_chart':
      ({ query_names_as_dict } = REM_evaluation_query_basis('probability'));

      queries = Object.keys(query_names_as_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [],
                },
              },
              aggs: {
                ...arrayToObject(
                  getBucketsFromGlobalMappers('ProbabilityKeyCriterias').map(
                    (criterion, ind) => {
                      // get the criterion-specific aggs & paths

                      ({ path_and_field_KeyCriterion, numerical_aggs } =
                        REM_evaluation_query_basis('probability', criterion));

                      return <any>{
                        bucket_name: `A) ${ind}) ${criterion}`,
                        query: {
                          filter: {
                            bool: {
                              must: [
                                constructNestedTermQuery(
                                  path_and_field_KeyCriterion.path,
                                  path_and_field_KeyCriterion.name_field,
                                  criterion
                                ),
                              ],
                            },
                          },
                          aggs: {
                            ...numerical_aggs(),
                          },
                        },
                      };
                    }
                  ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;
    case 'AggregatedAddressesPerEvaluations_vsPotentialCriterias_address_based_histogram_chart':
      ({ path_and_field_KeyCriterion, query_names_as_dict, numerical_aggs } =
        REM_evaluation_query_basis('potential'));

      queries = Object.keys(query_names_as_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [],
                },
              },
              aggs: {
                ...arrayToObject(
                  getBucketsFromGlobalMappers('PotentialKeyCriterias').map(
                    (criterion, ind) => {
                      ({ path_and_field_KeyCriterion, numerical_aggs } =
                        REM_evaluation_query_basis('potential', criterion));

                      return <any>{
                        bucket_name: `A) ${ind}) ${criterion}`,
                        query: {
                          filter: {
                            bool: {
                              must: [
                                constructNestedTermQuery(
                                  path_and_field_KeyCriterion.path,
                                  path_and_field_KeyCriterion.name_field,
                                  criterion
                                ),
                              ],
                            },
                          },
                          aggs: {
                            ...numerical_aggs(),
                          },
                        },
                      };
                    }
                  ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;
    case 'AggregatedAddressesPerEvaluations_vsPreventionCriterias_address_based_histogram_chart':
      ({ path_and_field_KeyCriterion, query_names_as_dict, numerical_aggs } =
        REM_evaluation_query_basis('prevention'));

      queries = Object.keys(query_names_as_dict).map((step) => {
        return {
          aggs: {
            [`histograms_${step}`]: {
              filter: {
                bool: {
                  must: [],
                },
              },
              aggs: {
                ...arrayToObject(
                  getBucketsFromGlobalMappers('PreventionKeyCriterias').map(
                    (criterion, ind) => {
                      ({ path_and_field_KeyCriterion, numerical_aggs } =
                        REM_evaluation_query_basis('prevention', criterion));

                      return <any>{
                        bucket_name: `A) ${ind}) ${criterion}`,
                        query: {
                          filter: {
                            bool: {
                              must: [
                                constructNestedTermQuery(
                                  path_and_field_KeyCriterion.path,
                                  path_and_field_KeyCriterion.name_field,
                                  criterion
                                ),
                              ],
                            },
                          },
                          aggs: {
                            ...numerical_aggs(),
                          },
                        },
                      };
                    }
                  ),
                  'bucket_name',
                  'query'
                ),
              },
            },
          },
        };
      });
      break;
    // RISK ENGINEERING END
    // RE INSURANCE BEGIN
    case 'RIM_Property_AggregatedStatsByPML_address_based_histogram_chart':
      query_names = numericalRanges.RIM_Property_PML_Intervals.map(
        ([gte, lte]) => {
          const prettify = (num: number) => pprintCurrency(num, null);
          return `${prettify(gte)}...${isFinite(lte) ? prettify(lte) : ''}`;
        }
      );

      queries = constructAggregationQuery({
        row_names: query_names,
        row_queries: numericalRanges.RIM_Property_PML_Intervals.map(
          ([gte, lte], row_index) => {
            return {
              disabled: false,
              agg_name: query_names[row_index],
              meta: {
                row_index,
                row_name: `<span class="align-right">${query_names[row_index]}</span>`,
                category_column_name: 'Range of PML',
              },
              filter: constructNestedRangeQuery(
                'address.locationProperty',
                'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                {
                  gte,
                  lte,
                }
              ),
              aggs_themselves: {
                'Number of Insureds/Risks': {
                  agg_name: 'Number of Insureds/Risks',
                  meta: {
                    column: {
                      index: 1,
                      name: 'Number of Insureds/Risks',
                      presented_agg_name: 'address_count',
                    },
                    post_processing: {
                      address_count: {
                        value: {
                          formatter:
                            AGG_VALUE_FORMATTERS_ENUM.IDENTITY_NAN_IS_ZERO,
                          accessor: 'address_count.inside.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      address_count:
                        constructScriptedMetricNestedCardinalityQuery(
                          'inside',
                          'address',
                          'id'
                        ),
                    },
                  },
                },
                'Aggregated Sum Insured': {
                  agg_name: 'Aggregated Sum Insured',
                  meta: {
                    column: {
                      index: 2,
                      name: 'Aggregated Sum Insured',
                      presented_agg_name: 'total_sum_insured',
                    },
                    post_processing: {
                      total_sum_insured: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'total_sum_insured.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      total_sum_insured: nestedStatsAggregation(
                        'address',
                        'address.sumInsured_$CURRENCY$_AdjustedForShare',
                        'sum'
                      ),
                    },
                  },
                },
                'Aggregated PML': {
                  agg_name: 'Aggregated PML',
                  meta: {
                    column: {
                      index: 3,
                      name: 'Aggregated PML',
                      presented_agg_name: 'total_pml',
                    },
                    post_processing: {
                      total_pml: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'total_pml.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      total_pml: nestedStatsAggregation(
                        'address.locationProperty',
                        'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                        'sum'
                      ),
                    },
                  },
                },
                'Aggregated Annual Premium Amount Written': {
                  agg_name: 'Aggregated Annual Premium Amount Written',
                  meta: {
                    column: {
                      index: 4,
                      name: 'Aggregated Annual Premium Amount Written',
                      presented_agg_name: 'total_premium',
                    },
                    post_processing: {
                      total_premium: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'total_premium.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      total_premium: nestedStatsAggregation(
                        'address',
                        'address.premiumBasisPerLocation_$CURRENCY$',
                        'sum'
                      ),
                    },
                  },
                },
                'Avg Sum Insured': {
                  agg_name: 'Avg Sum Insured',
                  meta: {
                    column: {
                      index: 5,
                      name: 'Avg Sum Insured',
                      presented_agg_name: 'avg_sum_insured',
                    },
                    post_processing: {
                      avg_sum_insured: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'avg_sum_insured.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      avg_sum_insured: nestedStatsAggregation(
                        'address',
                        'address.sumInsured_$CURRENCY$_AdjustedForShare',
                        'avg'
                      ),
                    },
                  },
                },
                'Avg PML': {
                  agg_name: 'Avg PML',
                  meta: {
                    column: {
                      index: 6,
                      name: 'Avg PML',
                      presented_agg_name: 'avg_pml',
                    },
                    post_processing: {
                      avg_pml: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'avg_pml.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      avg_pml: nestedStatsAggregation(
                        'address.locationProperty',
                        'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                        'avg'
                      ),
                    },
                  },
                },
              },
            };
          }
        ),
      });
      break;
    case 'RIM_Property_AggregatedStatsByPMLminusTimeRange_AbsoluteValues_address_based_histogram_chart':
      query_names = numericalRanges.RIM_Property_PML_Intervals.map(
        ([gte, lte]) => {
          const prettify = (num: number) => pprintCurrency(num, null);
          return `${prettify(gte)}...${isFinite(lte) ? prettify(lte) : ''}`;
        }
      );

      minus1yearRangeSpecs =
        theAppComponent.getMins1YsoleActiveDatePickerComponent();
      if (minus1yearRangeSpecs.error) {
        alert(minus1yearRangeSpecs.error);
        break;
      }

      ({ baselineQuery, comparisonQuery } =
        constructYoYRangeComparisonQueries(minus1yearRangeSpecs));

      queries = constructAggregationQuery({
        row_names: query_names,
        row_queries: numericalRanges.RIM_Property_PML_Intervals.map(
          ([gte, lte], row_index) => {
            return {
              disabled: false,
              agg_name: query_names[row_index],
              meta: {
                row_index,
                row_name: `<span class="align-right">${query_names[row_index]}</span>`,
                category_column_name: 'Range of PML',
              },
              filter: constructNestedRangeQuery(
                'address.locationProperty',
                'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                {
                  gte,
                  lte,
                }
              ),
              aggs_themselves: {
                'Number of Insureds/Risks': {
                  agg_name: 'Number of Insureds/Risks',
                  meta: {
                    column: {
                      index: 1,
                      name: 'Number of Insureds/Risks',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter:
                            AGG_VALUE_FORMATTERS_ENUM.IDENTITY_NAN_IS_ZERO,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        address_count: {
                          filter: baselineQuery,
                          aggs: {
                            inside:
                              constructScriptedMetricNestedCardinalityQuery(
                                'inside',
                                'address',
                                'id'
                              ),
                          },
                        },
                      },
                      {
                        address_count_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside:
                              constructScriptedMetricNestedCardinalityQuery(
                                'inside',
                                'address',
                                'id'
                              ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'address_count>inside>inside.value',
                              before:
                                'address_count_time_diff>inside>inside.value',
                            },
                            script: 'params.now - params.before',
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Aggregated Sum Insured': {
                  agg_name: 'Aggregated Sum Insured',
                  meta: {
                    column: {
                      index: 2,
                      name: 'Aggregated Sum Insured',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        total_sum_insured: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        total_sum_insured_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'total_sum_insured>inside>stats>aggs.value',
                              before:
                                'total_sum_insured_time_diff>inside>stats>aggs.value',
                            },
                            script: 'params.now - params.before',
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Aggregated PML': {
                  agg_name: 'Aggregated PML',
                  meta: {
                    column: {
                      index: 3,
                      name: 'Aggregated PML',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        total_pml: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        total_pml_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'total_pml>inside>stats>aggs.value',
                              before:
                                'total_pml_time_diff>inside>stats>aggs.value',
                            },
                            script: 'params.now - params.before',
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Aggregated Annual Premium Amount Written': {
                  agg_name: 'Aggregated Annual Premium Amount Written',
                  meta: {
                    column: {
                      index: 4,
                      name: 'Aggregated Annual Premium Amount Written',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        total_premium: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.premiumBasisPerLocation_$CURRENCY$',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        total_premium_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.premiumBasisPerLocation_$CURRENCY$',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'total_premium>inside>stats>aggs.value',
                              before:
                                'total_premium_time_diff>inside>stats>aggs.value',
                            },
                            script: 'params.now - params.before',
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Avg Sum Insured': {
                  agg_name: 'Avg Sum Insured',
                  meta: {
                    column: {
                      index: 5,
                      name: 'Avg Sum Insured',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        avg_sum_insured: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        avg_sum_insured_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'avg_sum_insured>inside>stats>aggs.value',
                              before:
                                'avg_sum_insured_time_diff>inside>stats>aggs.value',
                            },
                            script: 'params.now - params.before',
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Avg PML': {
                  agg_name: 'Avg PML',
                  meta: {
                    column: {
                      index: 6,
                      name: 'Avg PML',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        avg_pml: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        avg_pml_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'avg_pml>inside>stats>aggs.value',
                              before:
                                'avg_pml_time_diff>inside>stats>aggs.value',
                            },
                            script: 'params.now - params.before',
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
              },
            };
          }
        ),
      });
      break;
    case 'RIM_Property_AggregatedStatsByPMLminusTimeRange_PercentualValues_address_based_histogram_chart':
      query_names = numericalRanges.RIM_Property_PML_Intervals.map(
        ([gte, lte]) => {
          const prettify = (num: number) => pprintCurrency(num, null);
          return `${prettify(gte)}...${isFinite(lte) ? prettify(lte) : ''}`;
        }
      );

      minus1yearRangeSpecs =
        theAppComponent.getMins1YsoleActiveDatePickerComponent();
      if (minus1yearRangeSpecs.error) {
        alert(minus1yearRangeSpecs.error);
        break;
      }

      ({ baselineQuery, comparisonQuery } =
        constructYoYRangeComparisonQueries(minus1yearRangeSpecs));

      queries = constructAggregationQuery({
        row_names: query_names,
        row_queries: numericalRanges.RIM_Property_PML_Intervals.map(
          ([gte, lte], row_index) => {
            return {
              disabled: false,
              agg_name: query_names[row_index],
              meta: {
                row_index,
                row_name: `<span class="align-right">${query_names[row_index]}</span>`,
                category_column_name: 'Range of PML',
              },
              filter: constructNestedRangeQuery(
                'address.locationProperty',
                'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                {
                  gte,
                  lte,
                }
              ),
              aggs_themselves: {
                'Number of Insureds/Risks': {
                  agg_name: 'Number of Insureds/Risks',
                  meta: {
                    column: {
                      index: 1,
                      name: 'Number of Insureds/Risks',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_PERCENT,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        address_count: {
                          filter: baselineQuery,
                          aggs: {
                            inside:
                              constructScriptedMetricNestedCardinalityQuery(
                                'inside',
                                'address',
                                'id'
                              ),
                          },
                        },
                      },
                      {
                        address_count_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside:
                              constructScriptedMetricNestedCardinalityQuery(
                                'inside',
                                'address',
                                'id'
                              ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'address_count>inside>inside.value',
                              before:
                                'address_count_time_diff>inside>inside.value',
                            },
                            script:
                              constructBucketScriptScriptForPercentualComparison(),
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Aggregated Sum Insured': {
                  agg_name: 'Aggregated Sum Insured',
                  meta: {
                    column: {
                      index: 2,
                      name: 'Aggregated Sum Insured',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_PERCENT,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        total_sum_insured: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        total_sum_insured_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'total_sum_insured>inside>stats>aggs.value',
                              before:
                                'total_sum_insured_time_diff>inside>stats>aggs.value',
                            },
                            script:
                              constructBucketScriptScriptForPercentualComparison(),
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Aggregated PML': {
                  agg_name: 'Aggregated PML',
                  meta: {
                    column: {
                      index: 3,
                      name: 'Aggregated PML',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_PERCENT,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        total_pml: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        total_pml_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'total_pml>inside>stats>aggs.value',
                              before:
                                'total_pml_time_diff>inside>stats>aggs.value',
                            },
                            script:
                              constructBucketScriptScriptForPercentualComparison(),
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Aggregated Annual Premium Amount Written': {
                  agg_name: 'Aggregated Annual Premium Amount Written',
                  meta: {
                    column: {
                      index: 4,
                      name: 'Aggregated Annual Premium Amount Written',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_PERCENT,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        total_premium: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.premiumBasisPerLocation_$CURRENCY$',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        total_premium_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.premiumBasisPerLocation_$CURRENCY$',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'total_premium>inside>stats>aggs.value',
                              before:
                                'total_premium_time_diff>inside>stats>aggs.value',
                            },
                            script:
                              constructBucketScriptScriptForPercentualComparison(),
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Avg Sum Insured': {
                  agg_name: 'Avg Sum Insured',
                  meta: {
                    column: {
                      index: 5,
                      name: 'Avg Sum Insured',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_PERCENT,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        avg_sum_insured: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        avg_sum_insured_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'avg_sum_insured>inside>stats>aggs.value',
                              before:
                                'avg_sum_insured_time_diff>inside>stats>aggs.value',
                            },
                            script:
                              constructBucketScriptScriptForPercentualComparison(),
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Avg PML': {
                  agg_name: 'Avg PML',
                  meta: {
                    column: {
                      index: 6,
                      name: 'Avg PML',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_PERCENT,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        avg_pml: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        avg_pml_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'avg_pml>inside>stats>aggs.value',
                              before:
                                'avg_pml_time_diff>inside>stats>aggs.value',
                            },
                            script:
                              constructBucketScriptScriptForPercentualComparison(),
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
              },
            };
          }
        ),
      });
      break;
    case 'RIM_Property_AggregatedStatsByTSI_address_based_histogram_chart':
      query_names = numericalRanges.RIM_Property_TSI_Intervals.map(
        ([gte, lte]) => {
          const prettify = (num: number) => pprintCurrency(num, null);
          return `${prettify(gte)}...${isFinite(lte) ? prettify(lte) : ''}`;
        }
      );

      queries = constructAggregationQuery({
        row_names: query_names,
        row_queries: numericalRanges.RIM_Property_TSI_Intervals.map(
          ([gte, lte], row_index) => {
            return {
              disabled: false,
              agg_name: query_names[row_index],
              meta: {
                row_index,
                row_name: `<span class="align-right">${query_names[row_index]}</span>`,
                category_column_name: 'Range of TSI',
              },
              filter: constructNestedRangeQuery(
                'address',
                'address.sumInsured_$CURRENCY$_AdjustedForShare',
                {
                  gte,
                  lte,
                }
              ),
              aggs_themselves: {
                'Number of Insureds/Risks': {
                  agg_name: 'Number of Insureds/Risks',
                  meta: {
                    column: {
                      index: 1,
                      name: 'Number of Insureds/Risks',
                      presented_agg_name: 'address_count',
                    },
                    post_processing: {
                      address_count: {
                        value: {
                          formatter:
                            AGG_VALUE_FORMATTERS_ENUM.IDENTITY_NAN_IS_ZERO,
                          accessor: 'address_count.inside.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      address_count:
                        constructScriptedMetricNestedCardinalityQuery(
                          'inside',
                          'address',
                          'id'
                        ),
                    },
                  },
                },
                'Aggregated Sum Insured': {
                  agg_name: 'Aggregated Sum Insured',
                  meta: {
                    column: {
                      index: 2,
                      name: 'Aggregated Sum Insured',
                      presented_agg_name: 'total_sum_insured',
                    },
                    post_processing: {
                      total_sum_insured: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'total_sum_insured.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      total_sum_insured: nestedStatsAggregation(
                        'address',
                        'address.sumInsured_$CURRENCY$_AdjustedForShare',
                        'sum'
                      ),
                    },
                  },
                },
                'Aggregated PML': {
                  agg_name: 'Aggregated PML',
                  meta: {
                    column: {
                      index: 3,
                      name: 'Aggregated PML',
                      presented_agg_name: 'total_pml',
                    },
                    post_processing: {
                      total_pml: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'total_pml.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      total_pml: nestedStatsAggregation(
                        'address.locationProperty',
                        'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                        'sum'
                      ),
                    },
                  },
                },
                'Aggregated Annual Premium Amount Written': {
                  agg_name: 'Aggregated Annual Premium Amount Written',
                  meta: {
                    column: {
                      index: 4,
                      name: 'Aggregated Annual Premium Amount Written',
                      presented_agg_name: 'total_premium',
                    },
                    post_processing: {
                      total_premium: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'total_premium.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      total_premium: nestedStatsAggregation(
                        'address',
                        'address.premiumBasisPerLocation_$CURRENCY$',
                        'sum'
                      ),
                    },
                  },
                },
                'Avg Sum Insured': {
                  agg_name: 'Avg Sum Insured',
                  meta: {
                    column: {
                      index: 5,
                      name: 'Avg Sum Insured',
                      presented_agg_name: 'avg_sum_insured',
                    },
                    post_processing: {
                      avg_sum_insured: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'avg_sum_insured.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      avg_sum_insured: nestedStatsAggregation(
                        'address',
                        'address.sumInsured_$CURRENCY$_AdjustedForShare',
                        'avg'
                      ),
                    },
                  },
                },
                'Avg PML': {
                  agg_name: 'Avg PML',
                  meta: {
                    column: {
                      index: 6,
                      name: 'Avg PML',
                      presented_agg_name: 'avg_pml',
                    },
                    post_processing: {
                      avg_pml: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor: 'avg_pml.stats.aggs.value',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: {
                      avg_pml: nestedStatsAggregation(
                        'address.locationProperty',
                        'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                        'avg'
                      ),
                    },
                  },
                },
              },
            };
          }
        ),
      });
      break;
    case 'RIM_Property_AggregatedStatsByTSIminusTimeRange_AbsoluteValues_address_based_histogram_chart':
      query_names = numericalRanges.RIM_Property_TSI_Intervals.map(
        ([gte, lte]) => {
          const prettify = (num: number) => pprintCurrency(num, null);
          return `${prettify(gte)}...${isFinite(lte) ? prettify(lte) : ''}`;
        }
      );

      minus1yearRangeSpecs =
        theAppComponent.getMins1YsoleActiveDatePickerComponent();
      if (minus1yearRangeSpecs.error) {
        alert(minus1yearRangeSpecs.error);
        break;
      }

      ({ baselineQuery, comparisonQuery } =
        constructYoYRangeComparisonQueries(minus1yearRangeSpecs));

      queries = constructAggregationQuery({
        row_names: query_names,
        row_queries: numericalRanges.RIM_Property_PML_Intervals.map(
          ([gte, lte], row_index) => {
            return {
              disabled: false,
              agg_name: query_names[row_index],
              meta: {
                row_index,
                row_name: `<span class="align-right">${query_names[row_index]}</span>`,
                category_column_name: 'Range of TSI',
              },
              filter: constructNestedRangeQuery(
                'address',
                'address.sumInsured_$CURRENCY$_AdjustedForShare',
                {
                  gte,
                  lte,
                }
              ),
              aggs_themselves: {
                'Number of Insureds/Risks': {
                  agg_name: 'Number of Insureds/Risks',
                  meta: {
                    column: {
                      index: 1,
                      name: 'Number of Insureds/Risks',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter:
                            AGG_VALUE_FORMATTERS_ENUM.IDENTITY_NAN_IS_ZERO,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        address_count: {
                          filter: baselineQuery,
                          aggs: {
                            inside:
                              constructScriptedMetricNestedCardinalityQuery(
                                'inside',
                                'address',
                                'id'
                              ),
                          },
                        },
                      },
                      {
                        address_count_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside:
                              constructScriptedMetricNestedCardinalityQuery(
                                'inside',
                                'address',
                                'id'
                              ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'address_count>inside>inside.value',
                              before:
                                'address_count_time_diff>inside>inside.value',
                            },
                            script: 'params.now - params.before',
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Aggregated Sum Insured': {
                  agg_name: 'Aggregated Sum Insured',
                  meta: {
                    column: {
                      index: 2,
                      name: 'Aggregated Sum Insured',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        total_sum_insured: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        total_sum_insured_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'total_sum_insured>inside>stats>aggs.value',
                              before:
                                'total_sum_insured_time_diff>inside>stats>aggs.value',
                            },
                            script: 'params.now - params.before',
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Aggregated PML': {
                  agg_name: 'Aggregated PML',
                  meta: {
                    column: {
                      index: 3,
                      name: 'Aggregated PML',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        total_pml: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        total_pml_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'total_pml>inside>stats>aggs.value',
                              before:
                                'total_pml_time_diff>inside>stats>aggs.value',
                            },
                            script: 'params.now - params.before',
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Aggregated Annual Premium Amount Written': {
                  agg_name: 'Aggregated Annual Premium Amount Written',
                  meta: {
                    column: {
                      index: 4,
                      name: 'Aggregated Annual Premium Amount Written',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        total_premium: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.premiumBasisPerLocation_$CURRENCY$',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        total_premium_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.premiumBasisPerLocation_$CURRENCY$',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'total_premium>inside>stats>aggs.value',
                              before:
                                'total_premium_time_diff>inside>stats>aggs.value',
                            },
                            script: 'params.now - params.before',
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Avg Sum Insured': {
                  agg_name: 'Avg Sum Insured',
                  meta: {
                    column: {
                      index: 5,
                      name: 'Avg Sum Insured',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        avg_sum_insured: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        avg_sum_insured_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'avg_sum_insured>inside>stats>aggs.value',
                              before:
                                'avg_sum_insured_time_diff>inside>stats>aggs.value',
                            },
                            script: 'params.now - params.before',
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Avg PML': {
                  agg_name: 'Avg PML',
                  meta: {
                    column: {
                      index: 6,
                      name: 'Avg PML',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PPRINT_CURRENCY,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        avg_pml: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        avg_pml_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'avg_pml>inside>stats>aggs.value',
                              before:
                                'avg_pml_time_diff>inside>stats>aggs.value',
                            },
                            script: 'params.now - params.before',
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
              },
            };
          }
        ),
      });
      break;
    case 'RIM_Property_AggregatedStatsByTSIminusTimeRange_PercentualValues_address_based_histogram_chart':
      query_names = numericalRanges.RIM_Property_TSI_Intervals.map(
        ([gte, lte]) => {
          const prettify = (num: number) => pprintCurrency(num, null);
          return `${prettify(gte)}...${isFinite(lte) ? prettify(lte) : ''}`;
        }
      );

      minus1yearRangeSpecs =
        theAppComponent.getMins1YsoleActiveDatePickerComponent();
      if (minus1yearRangeSpecs.error) {
        alert(minus1yearRangeSpecs.error);
        break;
      }

      ({ baselineQuery, comparisonQuery } =
        constructYoYRangeComparisonQueries(minus1yearRangeSpecs));

      queries = constructAggregationQuery({
        row_names: query_names,
        row_queries: numericalRanges.RIM_Property_PML_Intervals.map(
          ([gte, lte], row_index) => {
            return {
              disabled: false,
              agg_name: query_names[row_index],
              meta: {
                row_index,
                row_name: `<span class="align-right">${query_names[row_index]}</span>`,
                category_column_name: 'Range of TSI',
              },
              filter: constructNestedRangeQuery(
                'address',
                'address.sumInsured_$CURRENCY$_AdjustedForShare',
                {
                  gte,
                  lte,
                }
              ),
              aggs_themselves: {
                'Number of Insureds/Risks': {
                  agg_name: 'Number of Insureds/Risks',
                  meta: {
                    column: {
                      index: 1,
                      name: 'Number of Insureds/Risks',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_PERCENT,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        address_count: {
                          filter: baselineQuery,
                          aggs: {
                            inside:
                              constructScriptedMetricNestedCardinalityQuery(
                                'inside',
                                'address',
                                'id'
                              ),
                          },
                        },
                      },
                      {
                        address_count_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside:
                              constructScriptedMetricNestedCardinalityQuery(
                                'inside',
                                'address',
                                'id'
                              ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'address_count>inside>inside.value',
                              before:
                                'address_count_time_diff>inside>inside.value',
                            },
                            script:
                              constructBucketScriptScriptForPercentualComparison(),
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Aggregated Sum Insured': {
                  agg_name: 'Aggregated Sum Insured',
                  meta: {
                    column: {
                      index: 2,
                      name: 'Aggregated Sum Insured',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_PERCENT,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        total_sum_insured: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        total_sum_insured_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'total_sum_insured>inside>stats>aggs.value',
                              before:
                                'total_sum_insured_time_diff>inside>stats>aggs.value',
                            },
                            script:
                              constructBucketScriptScriptForPercentualComparison(),
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Aggregated PML': {
                  agg_name: 'Aggregated PML',
                  meta: {
                    column: {
                      index: 3,
                      name: 'Aggregated PML',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_PERCENT,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        total_pml: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        total_pml_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'total_pml>inside>stats>aggs.value',
                              before:
                                'total_pml_time_diff>inside>stats>aggs.value',
                            },
                            script:
                              constructBucketScriptScriptForPercentualComparison(),
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Aggregated Annual Premium Amount Written': {
                  agg_name: 'Aggregated Annual Premium Amount Written',
                  meta: {
                    column: {
                      index: 4,
                      name: 'Aggregated Annual Premium Amount Written',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_PERCENT,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        total_premium: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.premiumBasisPerLocation_$CURRENCY$',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        total_premium_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.premiumBasisPerLocation_$CURRENCY$',
                              'sum'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'total_premium>inside>stats>aggs.value',
                              before:
                                'total_premium_time_diff>inside>stats>aggs.value',
                            },
                            script:
                              constructBucketScriptScriptForPercentualComparison(),
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Avg Sum Insured': {
                  agg_name: 'Avg Sum Insured',
                  meta: {
                    column: {
                      index: 5,
                      name: 'Avg Sum Insured',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_PERCENT,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        avg_sum_insured: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        avg_sum_insured_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address',
                              'address.sumInsured_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'avg_sum_insured>inside>stats>aggs.value',
                              before:
                                'avg_sum_insured_time_diff>inside>stats>aggs.value',
                            },
                            script:
                              constructBucketScriptScriptForPercentualComparison(),
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
                'Avg PML': {
                  agg_name: 'Avg PML',
                  meta: {
                    column: {
                      index: 6,
                      name: 'Avg PML',
                      presented_agg_name: 'multi_bucket_wrapper',
                    },
                    post_processing: {
                      multi_bucket_wrapper: {
                        value: {
                          formatter: AGG_VALUE_FORMATTERS_ENUM.PRETTY_PERCENT,
                          accessor:
                            'multi_bucket_wrapper.buckets.all.time_diff.value_as_string',
                        },
                      },
                    },
                  },
                  aggs_themselves: {
                    actual_aggs: constructSimulatedMultibucketBucketScript(
                      {
                        avg_pml: {
                          filter: baselineQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        avg_pml_time_diff: {
                          filter: comparisonQuery,
                          aggs: {
                            inside: nestedStatsAggregation(
                              'address.locationProperty',
                              'address.locationProperty.PML_$CURRENCY$_AdjustedForShare',
                              'avg'
                            ),
                          },
                        },
                      },
                      {
                        time_diff: {
                          bucket_script: {
                            buckets_path: {
                              now: 'avg_pml>inside>stats>aggs.value',
                              before:
                                'avg_pml_time_diff>inside>stats>aggs.value',
                            },
                            script:
                              constructBucketScriptScriptForPercentualComparison(),
                            format: '###.##',
                          },
                        },
                      }
                    ),
                  },
                },
              },
            };
          }
        ),
      });
      break;
  }

  transformArrayOfAggsToObjofAggs(q, queries);

  if (include_source) {
    q._source = include_source;
    // Certain charts like AggregatedAddressesPerScorePerPML_address_based_histogram_chart
    // do require high page sizes to render canvasses correctly.
    // 50K is a good compromise. Fixes UMGB-11
    q.size = 50e3;
  }

  return q;
};

export const HISTOGRAM_TABLE_CHART_OPTIONS = {
  custom: {
    ...PPM_HISTOGRAM_TABLE_CHART_OPTIONS,
    ...REM_HISTOGRAM_TABLE_CHART_OPTIONS,
    ...RIM_HISTOGRAM_TABLE_CHART_OPTIONS,
    ...ESG_HISTOGRAM_TABLE_CHART_OPTIONS,
  },
};
