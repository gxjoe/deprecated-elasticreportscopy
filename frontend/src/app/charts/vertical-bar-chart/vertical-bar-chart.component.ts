import {
  Component,
  OnInit,
  ViewEncapsulation,
  ViewChild,
  Input,
  AfterViewInit
} from '@angular/core';
import { OnIncomingData } from '../../interfaces/on-incoming-data';
import { ElasticService } from '../../services/elastic.service';
import { VERTICAL_BAR_CHART_OPTIONS } from './constants';
import { CustomChartDirective } from '../common/chart';
import {
  colorSchemeVividDomainAsRgbArray,
  lobVsColor,
  shortenedLabelsVsLegendMapKeysVsColors
} from '../common/constants';
import { ChartjsService } from '../../services/chartjs.service';

@Component({
  selector: 'app-vertical-bar-chart',
  templateUrl: './vertical-bar-chart.component.html',
  styleUrls: ['./vertical-bar-chart.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class VerticalBarChart extends CustomChartDirective
  implements OnInit, OnIncomingData {
  @ViewChild('canvas', { static: false })
  canvas;

  @Input()
  canvasw;
  @Input()
  canvash;

  isNormalized: Boolean = false;
  isStacked: Boolean = false;

  _colors = colorSchemeVividDomainAsRgbArray();
  _softer_colors = colorSchemeVividDomainAsRgbArray(0.4);
  _datasets;
  _labels;
  _counts;
  totalCount;

  legendMap: object;

  private _drawnChart;

  height;
  width;

  csvInput?: string;

  public destroyCanvasChart() {
    if (this._drawnChart) {
      this._drawnChart.destroy();
    }

    if (this.legendMap) {
      this.legendMap = undefined;
    }
  }

  constructor(
    public es: ElasticService,
    private chart_service: ChartjsService
  ) {
    super();
  }

  ngOnInit() {
    this.es.app_component.setComponent(this.klass, this);
    Object.assign(this, VERTICAL_BAR_CHART_OPTIONS.custom[this.klass]);

    this.isNormalized = this.klass.toLowerCase().indexOf('normalized') !== -1;
    this.isStacked = this.klass.toLowerCase().indexOf('stacked') !== -1;

    // console.info(this.isNormalized, this.isStacked)
  }

  handleOnIncoming(datasets, labels, options) {
    if (this._drawnChart) {
      this._drawnChart.destroy();
    }

    this._drawnChart = this.chart_service.constructNormalizedVerticalBarChart(
      this.canvas.nativeElement,
      {
        type: '',
        labels: labels,
        datasets: datasets.map((dataset, index) => {
          const { legendMap } = options;
          // starting at non-null because the first label might be an empty one
          //                                    vvv
          const lob_vs_color = lobVsColor(labels[1], index !== 0);

          if (lob_vs_color) {
            return {
              backgroundColor: labels.map(label =>
                lobVsColor(label, index !== 0)
              ),
              ...dataset
            };
          }

          if (legendMap) {
            return {
              backgroundColor: shortenedLabelsVsLegendMapKeysVsColors(
                labels,
                legendMap,
                index
              ),
              ...dataset
            };
          }

          return {
            backgroundColor: options.customColors
              ? options.customColors[index]
              : index === 0
                ? this._colors
                : this._softer_colors,
            ...dataset
          };
        }),
        ...options
      }
    );
  }
}
