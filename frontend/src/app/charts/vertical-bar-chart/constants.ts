import { findByKeyInNestedObject } from '../../elastic/utils';
import {
  hexToRgb_,
  colorSchemeVividDomainAsRgbArray
} from '../common/constants';
import {
  SLICE_LIMIT,
  fromUnixToFormat,
  sortArrayByKey,
  percentageOfBuckets,
  stripHtml,
  sumByMapReduce,
  prettyPercent,
  humanReadableTimeFrame,
  dynamicSort
} from '../utils';
import * as _ from 'lodash';
import {
  onAnimationCompleteTechniques,
  annotationTechniques
} from '../../services/chartjs.service';
import {
  pprintCurrency,
  nthLetterOfAlphabet,
  jsonToKeyValueTable,
  mergeArraysOfObjectsByKey,
  createLegendMapItem,
  uniq,
  sortArrOfStringsAlphabetically,
  pprintLargePrice,
  CURRENCY_PREFIX_ISO
} from '../../utils/helpers/helpers';
import { roundUpToNearestN, roundUp } from '../../utils/helpers/numerical';
import { getValuesOfAffectingFilters } from '../../utils/constants/filteringComponentVsAffectedComponents';
import { CHART_FUNCT } from '../histogram-table-chart/common';
import { CsvService } from 'src/app/services/csv.service';

export const VERTICAL_BAR_CHART_OPTIONS = {
  custom: {
    TotalNoOfOffersPerLoBnormalizedVerticalBarChart: {
      legend: true,
      legendTitle: 'LoB',
      handleIncomingData(inputData) {
        const hits = findByKeyInNestedObject(inputData, 'hits')['hits'];
        const buckets = sortArrayByKey(
          findByKeyInNestedObject(inputData, 'buckets')['buckets'],
          'key'
        );

        const applicable_values = ['Policy issued'];

        this.totalCount = hits.total;
        const result = [];

        const counts = {};
        const labels = [];
        const datasets = [
          {
            label: 'Concluded',
            data: [],
            stack: 1
          },
          {
            label: 'Total',
            data: [],
            stack: 1
          }
        ];

        buckets.slice(0, SLICE_LIMIT).map(item => {
          const stats_buckets = findByKeyInNestedObject(item.status, 'buckets')[
            'buckets'
          ];
          const applicable_bucket = stats_buckets.filter(bucket => {
            return applicable_values.indexOf(bucket.key) !== -1;
          });

          if (applicable_bucket.length > 0) {
            counts[item.key] = {
              Concluded: applicable_bucket[0].doc_count,
              Total: item.doc_count,
              nominator_key: 'Concluded',
              denominator_key: 'Total'
            };

            datasets[0].data.push(
              (applicable_bucket[0].doc_count / item.doc_count) * 100
            );
            // datasets[1].data.push(100); // normalization. So this must be total i.e. 100%

            if (!labels.includes(item.key)) {
              labels.push(item.key);
            }

            result.push({
              name: item.key,
              series: [
                {
                  name: 'Concluded',
                  value: applicable_bucket[0].doc_count
                },
                {
                  name: 'Total',
                  value: item.doc_count
                }
              ]
            });
          }
        });

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: annotationTechniques.hundred_percent_line,
          tickOptions: {
            callback: function(value, _, __) {
              return `${value} %`;
            },
            max: 110
          },
          legendOptions: null,
          onAnimationComplete:
            onAnimationCompleteTechniques.draw_above_bar_chart,
          stackOptions: {
            stacked: false
          }
        });
        this._counts = counts;
      }
    },
    OffersAggregatedByStatus: {
      legend: true,
      legendTitle: 'Offers By Status',
      handleIncomingData(inputData, kwargs) {
        const { app } = kwargs;
        const klass = 'OffersAggregatedByStatus';
        const selectedValues = getValuesOfAffectingFilters(app, klass);
        const buckets = inputData.aggregations.filter_agg.filter_agg.buckets;
        const bucket_count = sumByMapReduce(buckets);

        const max_arr = [];

        const legendMap = {};
        const labels = [];
        const datasets = [
          {
            label: 'All | just_values',
            data: [],
            stack: 1
          }
        ];

        buckets.map((item, ind) => {
          if (selectedValues.length && !selectedValues.includes(item.key)) {
          } else {
            datasets[0].data.push(item.doc_count);
            const letter = nthLetterOfAlphabet(ind).toUpperCase();
            labels.push(letter);
            legendMap[letter] = {
              key: `${item.key}`,
              count: `${item.doc_count}`,
              percentage: `${percentageOfBuckets(item.doc_count, bucket_count)}`
            };
            max_arr.push(item.doc_count);
          }
        });

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: null,
          tickOptions: {
            callback: function(value, ___, __) {
              return `${value}`;
            },
            max: roundUpToNearestN(Math.max.apply(null, max_arr))
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.noop,
          stackOptions: {
            stacked: true
          },
          labelCallback: function(tooltipItem, data) {
            return `${stripHtml(legendMap[tooltipItem.xLabel])}`;
          }
        });
        this.legendMap = jsonToKeyValueTable(legendMap);
      }
    },
    OffersAggregatedByDeclinationReasons: {
      legend: true,
      legendTitle: 'Offers By Declination Reasons',
      handleIncomingData(inputData) {
        const hits = findByKeyInNestedObject(inputData, 'hits')['hits'];
        const declination_reasons =
          inputData.aggregations.declination_reasons
            .declination_reasons_combined.value;
        const transformed_to_buckets = sortArrOfStringsAlphabetically(
          Object.keys(declination_reasons)
        ).map(dkey => {
          return {
            key: dkey,
            doc_count: declination_reasons[dkey]
          };
        });

        const buckets = [...transformed_to_buckets];

        const bucket_count = sumByMapReduce(buckets);

        this.totalCount = hits.total;

        const legendMap = {};
        const labels = [];
        const datasets = [
          {
            label: 'All | just_values',
            data: [],
            stack: 1
          }
        ];

        const max_arr = [];

        buckets.map((item, ind) => {
          datasets[0].data.push(item.doc_count);
          const letter = nthLetterOfAlphabet(ind).toUpperCase();
          labels.push(letter);
          legendMap[letter] = {
            key: `${item.key}`,
            count: `${item.doc_count}`,
            percentage: `${percentageOfBuckets(item.doc_count, bucket_count)}`
          };
          max_arr.push(item.doc_count);
        });

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: null,
          tickOptions: {
            callback: function(value, ___, __) {
              return `${value}`;
            },
            max: roundUpToNearestN(Math.max.apply(null, max_arr))
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.noop,
          stackOptions: {
            stacked: true
          },
          labelCallback: function(tooltipItem, data) {
            return `${stripHtml(legendMap[tooltipItem.xLabel])}`;
          }
        });
        this.legendMap = jsonToKeyValueTable(legendMap);
      }
    },
    OffersAggregatedByLineOfBusiness: {
      legend: true,
      legendTitle: 'Offers By LoB / Details & Perils',
      handleIncomingData(inputData, kwargs) {
        const { app } = kwargs;
        const klass = 'OffersAggregatedByLineOfBusiness';
        const selectedValues = getValuesOfAffectingFilters(app, klass);

        const total_offers = findByKeyInNestedObject(
          inputData,
          'total_deepest'
        );
        const concluded_offers = findByKeyInNestedObject(
          inputData,
          'concluded_deepest'
        );

        const merged_data = mergeArraysOfObjectsByKey(
          total_offers['total_deepest'].buckets.map(b => {
            return {
              key: b.key,
              total_count: b.doc_count
            };
          }),
          concluded_offers['concluded_deepest'].buckets.map(b => {
            return {
              key: b.key,
              concluded_count: b.doc_count
            };
          }),
          0
        );

        merged_data.push({
          key: 'All',
          total_count: sumByMapReduce(total_offers['total_deepest'].buckets),
          concluded_count: sumByMapReduce(
            concluded_offers['concluded_deepest'].buckets
          )
        });

        const legendMap = {};
        const labels = [];
        const datasets = [
          {
            label: 'All of LoB | just_values',
            data: [],
            stack: 1,
            yAxisID: 'normal_y_axis'
          },
          {
            label: 'Concluded | just_values',
            data: [],
            stack: 2,
            yAxisID: 'normal_y_axis'
          },
          {
            label: 'Success Ratio',
            data: [],
            type: 'line',
            yAxisID: 'percentual_y_axis',
            backgroundColor: 'rgba(0,0,0,0)',
            borderColor: '#00ae3b'
          }
        ];

        const max_arr = [];

        merged_data.map((item, ind) => {
          if (
            (selectedValues.length && !selectedValues.includes(item.key)) ||
            !item.key.length
          ) {
          } else {
            // TOTAL
            // const letter = nthLetterOfAlphabet(ind).toUpperCase();

            const label = `${item.key}`;
            datasets[0].data.push(item.total_count);
            max_arr.push(item.total_count);

            labels.push(label);

            // CONCLUDED
            datasets[1].data.push(item.concluded_count);
            max_arr.push(item.concluded_count);

            datasets[2].data.push(
              Math.round((item.concluded_count / item.total_count) * 1e2)
            );

            legendMap[label] = {
              'All of LoB | just_values': createLegendMapItem(
                `${item.key}: Total`,
                item.total_count,
                item.concluded_count,
                item.total_count,
                percentageOfBuckets,
                null
              ),
              'Concluded | just_values': createLegendMapItem(
                `${item.key}: Concluded`,
                item.concluded_count,
                item.concluded_count,
                item.total_count,
                percentageOfBuckets,
                null
              ),
              'Success Ratio': createLegendMapItem(
                `${item.key}`,
                '',
                item.concluded_count,
                item.total_count,
                percentageOfBuckets,
                'success ratio concluded/total'
              )
            };
          }
        });

        this.handleOnIncoming(datasets, labels, {
          percentual_second_y_axis: true,
          annotationTechniques: null,
          tickOptions: {
            callback: function(value, ___, __) {
              return `${value}`;
            },
            max: roundUpToNearestN(Math.max.apply(null, max_arr))
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.just_values,
          stackOptions: {
            stacked: false
          },
          labelCallback: function(tooltipItem, data) {
            const label = data.datasets[tooltipItem.datasetIndex]['label'];
            return `${stripHtml(legendMap[tooltipItem.xLabel][label])}`;
          },
          plugins: {
            // we needed just_values to render numbers but now
            // we want to get rid of the extra suffix
            afterLayout: function(chart) {
              chart.legend.legendItems.forEach(label => {
                // const value = chart.data.datasets[0].data[label.index];
                label.text = label.text.replace(/ \| just_values/g, '');
                return label;
              });
            }
          }
        });

        this.csvInput = new CsvService().chartjsDatasetToCSV(labels, datasets);
        // this.legendMap = jsonToKeyValueTable(legendMap);
      }
    },
    OffersAggregatedByLineOfBusinessVsPremium: {
      legend: true,
      legendTitle: 'Offers By LoB / Details & Perils',
      handleIncomingData(inputData, kwargs) {
        const { app } = kwargs;
        const klass = 'OffersAggregatedByLineOfBusiness';
        const selectedValues = getValuesOfAffectingFilters(app, klass);

        const total_offers = findByKeyInNestedObject(
          inputData,
          'total_deepest'
        );
        const concluded_offers = findByKeyInNestedObject(
          inputData,
          'concluded_deepest'
        );

        const merged_data = mergeArraysOfObjectsByKey(
          total_offers['total_deepest'].buckets.map(b => {
            return {
              key: b.key,
              total_count: pprintLargePrice(
                b.total_deepest.sum,
                CURRENCY_PREFIX_ISO()
              ),
              total_count_raw: b.total_deepest.sum
            };
          }),
          concluded_offers['concluded_deepest'].buckets.map(b => {
            return {
              key: b.key,
              concluded_count: pprintLargePrice(
                b.concluded_deepest.sum,
                CURRENCY_PREFIX_ISO()
              ),
              concluded_count_raw: b.concluded_deepest.sum
            };
          }),
          0
        );

        merged_data.push({
          key: 'All',
          total_count_raw: total_offers['total_deepest'].buckets
            .map(b => b.total_deepest.sum)
            .sumByReduce(),
          total_count: pprintLargePrice(
            total_offers['total_deepest'].buckets
              .map(b => b.total_deepest.sum)
              .sumByReduce(),
            CURRENCY_PREFIX_ISO()
          ),
          concluded_count_raw: concluded_offers['concluded_deepest'].buckets
            .map(b => b.concluded_deepest.sum)
            .sumByReduce(),
          concluded_count: pprintLargePrice(
            concluded_offers['concluded_deepest'].buckets
              .map(b => b.concluded_deepest.sum)
              .sumByReduce(),
            CURRENCY_PREFIX_ISO()
          )
        });

        const legendMap = {};
        const labels = [];
        const datasets = [
          {
            label: 'All of LoB | just_values',
            data: [],
            stack: 1,
            yAxisID: 'normal_y_axis'
          },
          {
            label: 'Concluded | just_values',
            data: [],
            stack: 2,
            yAxisID: 'normal_y_axis'
          },
          {
            label: 'Success Ratio',
            data: [],
            type: 'line',
            yAxisID: 'percentual_y_axis',
            backgroundColor: 'rgba(0,0,0,0)',
            borderColor: '#00ae3b'
          }
        ];

        const max_arr = [];

        merged_data.map((item, ind) => {
          if (
            (selectedValues.length && !selectedValues.includes(item.key)) ||
            !item.key.length
          ) {
          } else {
            // TOTAL
            // const letter = nthLetterOfAlphabet(ind).toUpperCase();

            const label = `${item.key}`;
            datasets[0].data.push(item.total_count_raw);
            max_arr.push(item.total_count_raw);

            labels.push(label);

            // CONCLUDED
            datasets[1].data.push(item.concluded_count_raw);
            max_arr.push(item.concluded_count_raw);

            datasets[2].data.push(
              Math.round(
                (item.concluded_count_raw / item.total_count_raw) * 1e2
              )
            );

            legendMap[label] = {
              'All of LoB | just_values': createLegendMapItem(
                `${item.key}: Total`,
                item.total_count,
                item.concluded_count_raw,
                item.total_count_raw,
                percentageOfBuckets,
                null
              ),
              'Concluded | just_values': createLegendMapItem(
                `${item.key}: Concluded`,
                item.concluded_count,
                item.concluded_count_raw,
                item.total_count_raw,
                percentageOfBuckets,
                null
              ),
              'Success Ratio': createLegendMapItem(
                `${item.key}`,
                '',
                item.concluded_count_raw,
                item.total_count_raw,
                percentageOfBuckets,
                'success ratio concluded/total'
              )
            };
          }
        });

        this.handleOnIncoming(datasets, labels, {
          percentual_second_y_axis: true,
          annotationTechniques: null,
          tickOptions: {
            callback: function(value, ___, __) {
              return `${pprintLargePrice(value, CURRENCY_PREFIX_ISO())}`;
            },
            max: Math.max.apply(null, max_arr)
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.noop,
          stackOptions: {
            stacked: false
          },
          labelCallback: function(tooltipItem, data) {
            const label = data.datasets[tooltipItem.datasetIndex]['label'];

            return `${stripHtml(legendMap[tooltipItem.xLabel][label])}`;
          },
          plugins: {
            // we needed just_values to render numbers but now
            // we want to get rid of the extra suffix
            afterLayout: function(chart) {
              chart.legend.legendItems.forEach(label => {
                // const value = chart.data.datasets[0].data[label.index];
                label.text = label.text.replace(/ \| just_values/g, '');
                return label;
              });
            }
          }
        });
        this.legendMap = undefined; // jsonToKeyValueTable(legendMap);
        this.csvInput = new CsvService().chartjsDatasetToCSV(labels, datasets);
      }
    },

    AcceptedOffersAggregatedByLineOfBusiness: {
      legend: true,
      legendTitle: 'Offers By LoB / Details & Perils',
      handleIncomingData(inputData, kwargs) {
        const { app } = kwargs;
        const klass = 'AcceptedOffersAggregatedByLineOfBusiness';
        const selectedValues = getValuesOfAffectingFilters(app, klass);
        const hits = findByKeyInNestedObject(inputData, 'hits')['hits'];
        const buckets = inputData.aggregations.filter_agg.filter_agg.buckets;
        const bucket_count = sumByMapReduce(buckets);

        this.totalCount = hits.total;

        const legendMap = {};
        const labels = [];
        const datasets = [
          {
            label: 'All | just_values',
            data: [],
            stack: 1
          }
        ];

        const max_arr = [];

        buckets.map((item, ind) => {
          if (selectedValues.length && !selectedValues.includes(item.key)) {
          } else {
            datasets[0].data.push(item.doc_count);
            const letter = nthLetterOfAlphabet(ind).toUpperCase();
            labels.push(letter);
            legendMap[letter] = {
              key: `${item.key}`,
              count: `${item.doc_count}`,
              percentage: `${percentageOfBuckets(item.doc_count, bucket_count)}`
            };
            max_arr.push(item.doc_count);
          }
        });

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: null,
          tickOptions: {
            callback: function(value, ___, __) {
              return `${value}`;
            },
            max: roundUpToNearestN(Math.max.apply(null, max_arr))
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.noop,
          stackOptions: {
            stacked: true
          },
          labelCallback: function(tooltipItem, data) {
            return `${stripHtml(legendMap[tooltipItem.xLabel])}`;
          },
          legendMap: legendMap
        });
        this.legendMap = jsonToKeyValueTable(legendMap);
      }
    },
    DeclinedOffersAggregatedByLineOfBusiness: {
      legend: true,
      legendTitle: 'Offers By LoB / Details & Perils',
      handleIncomingData(inputData, kwargs) {
        const { app } = kwargs;
        const klass = 'AcceptedOffersAggregatedByLineOfBusiness';
        const selectedValues = getValuesOfAffectingFilters(app, klass);
        const buckets = inputData.aggregations.filter_agg.filter_agg.buckets;
        const bucket_count = sumByMapReduce(buckets);

        const legendMap = {};
        const labels = [];
        const datasets = [
          {
            label: 'All | just_values',
            data: [],
            stack: 1
          }
        ];

        const max_arr = [];

        buckets.map((item, ind) => {
          if (selectedValues.length && !selectedValues.includes(item.key)) {
          } else {
            datasets[0].data.push(item.doc_count);
            const letter = nthLetterOfAlphabet(ind).toUpperCase();
            labels.push(letter);
            legendMap[letter] = {
              key: `${item.key}`,
              count: `${item.doc_count}`,
              percentage: `${percentageOfBuckets(item.doc_count, bucket_count)}`
            };

            const specific_declination_reasons = item.specifics.value;
            Object.keys(specific_declination_reasons).map((okey, oind) => {
              legendMap[letter + (oind + 1)] = {
                key: `${okey}`,
                count: `${specific_declination_reasons[okey]}`,
                percentage: `${percentageOfBuckets(
                  specific_declination_reasons[okey],
                  bucket_count
                )}`
              };
            });

            max_arr.push(item.doc_count);
          }
        });

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: null,
          tickOptions: {
            callback: function(value, ___, __) {
              return `${value}`;
            },
            max: roundUpToNearestN(Math.max.apply(null, max_arr))
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.noop,
          stackOptions: {
            stacked: true
          },
          labelCallback: function(tooltipItem, data) {
            return `${stripHtml(legendMap[tooltipItem.xLabel])}`;
          },
          legendMap: legendMap
        });
        this.legendMap = jsonToKeyValueTable(legendMap);
      }
    },

    OffersAggregatedByRisks: {
      legend: true,
      legendTitle: 'Offers By Risks',
      handleIncomingData(inputData) {
        const hits = findByKeyInNestedObject(inputData, 'hits')['hits'];
        const buckets = inputData.aggregations.filter_agg.filter_agg.buckets;
        const bucket_count = sumByMapReduce(buckets);

        this.totalCount = hits.total;

        const legendMap = {};
        const labels = [];
        const datasets = [
          {
            label: 'All | just_values',
            data: [],
            stack: 1
          }
        ];

        const max_arr = [];

        buckets.map((item, ind) => {
          datasets[0].data.push(item.doc_count);
          const letter = nthLetterOfAlphabet(ind).toUpperCase();
          labels.push(letter);
          legendMap[letter] = {
            key: `${item.key}`,
            count: `${item.doc_count}`,
            percentage: `${percentageOfBuckets(item.doc_count, bucket_count)}`
          };
          max_arr.push(item.doc_count);
        });

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: null,
          tickOptions: {
            callback: function(value, ___, __) {
              return `${value}`;
            },
            max: roundUpToNearestN(Math.max.apply(null, max_arr))
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.noop,
          stackOptions: {
            stacked: true
          },
          labelCallback: function(tooltipItem, data) {
            return `${stripHtml(legendMap[tooltipItem.xLabel])}`;
          }
        });
        this.legendMap = jsonToKeyValueTable(legendMap);
      }
    },
    TotalPremiumPerMonthVerticalBarChart: {
      handleIncomingData(inputData) {
        const hits = findByKeyInNestedObject(inputData, 'hits')['hits'];
        const buckets = findByKeyInNestedObject(inputData, 'buckets')[
          'buckets'
        ];

        const applicable_values = ['Policy issued'];

        this.totalCount = hits.total;
        const result = [];

        const counts = {};
        const labels = [];
        const datasets = [
          {
            label: 'Concluded',
            data: [],
            stack: 1
          },
          {
            label: 'Total',
            data: [],
            stack: 1
          }
        ];

        const premiums_as_list = [];

        buckets.slice(0, SLICE_LIMIT).map(item => {
          let total_premium_per_date = 0;

          const stats_buckets = findByKeyInNestedObject(item.status, 'buckets')[
            'buckets'
          ];
          const applicable_bucket = stats_buckets.filter(bucket => {
            total_premium_per_date =
              total_premium_per_date + bucket.premium_sum.value;
            return applicable_values.indexOf(bucket.key) !== -1;
          });

          if (applicable_bucket.length > 0) {
            const pretty_key = fromUnixToFormat(item.key);

            counts[pretty_key] = {
              Concluded: applicable_bucket[0].premium_sum.value,
              Total: total_premium_per_date,
              nominator_key: 'Concluded',
              denominator_key: 'Total'
            };

            datasets[0].data.push(applicable_bucket[0].premium_sum.value);
            datasets[1].data.push(total_premium_per_date);

            premiums_as_list.push(total_premium_per_date);

            if (!labels.includes(pretty_key)) {
              labels.push(pretty_key);
            }

            result.push({
              name: fromUnixToFormat(item.key),
              series: [
                {
                  name: 'Concluded',
                  value: applicable_bucket[0].premium_sum.value
                },
                {
                  name: 'Total',
                  value: total_premium_per_date
                }
              ]
            });
          }
        });

        const total_max = Math.max.apply(null, premiums_as_list);
        const magnitude = 10 ** (`${total_max}`.length - 1);

        this.handleOnIncoming(datasets, labels, {
          tickOptions: {
            callback: function(value, _, __) {
              return `${pprintCurrency(value)}`;
            },
            stepSize: (Math.floor(total_max / magnitude) * magnitude) / 5,
            max: 1.1 * total_max // 110% of max...
          },
          customColors: [
            colorSchemeVividDomainAsRgbArray()[0],
            colorSchemeVividDomainAsRgbArray(0.4)[0]
          ],
          onAnimationComplete:
            onAnimationCompleteTechniques.draw_above_bar_chart,
          stackOptions: { stacked: false }
        });
        this._counts = counts;
      }
    },
    NumberOfOffersPerLobPerRiskClassStackedVerticalBarChart: {
      view: undefined,
      colorScheme: {
        name: 'vivid_green_to_red',
        selectable: true,
        group: 'Ordinal',
        domain: [
          '#FF0000',
          '#FF8D3F',
          '#EDED44',
          '#7CEA76',
          '#0C7F49'
        ].reverse()
      },
      handleIncomingData(inputData) {
        const hits = findByKeyInNestedObject(inputData, 'hits')['hits'];
        const buckets = findByKeyInNestedObject(inputData, 'buckets')[
          'buckets'
        ];

        const applicable_values = ['1', '2', '3', '4', '5'];

        this.totalCount = hits.total;
        const result = [];

        const labels = [];
        let datasets = applicable_values.map(val => {
          return {
            label: val,
            data: [],
            stack: 1,
            hoverBackgroundColor: ''
          };
        });

        buckets.map(item => {
          const risk_class_buckets = findByKeyInNestedObject(
            item.risk_class,
            'buckets'
          )['buckets'];
          const series = [];
          risk_class_buckets.map(bucket => {
            if (applicable_values.indexOf(bucket.key) !== -1) {
              series.push({
                name: bucket.key,
                value: bucket.doc_count
              });
              datasets[parseInt(bucket.key, 10) - 1].data.push(
                bucket.doc_count
              );
            }
          });

          if (!labels.includes(item.key)) {
            labels.push(item.key);
          }

          result.push({
            name: item.key,
            series: sortArrayByKey(series, 'name')
          });
        });

        const domain_map = {
          '1': '#0C7F49',
          '2': '#7CEA76',
          '3': '#EDED44',
          '4': '#FF8D3F',
          '5': '#FF0000'
        };

        const domain_map_as_rgb = {};
        Object.entries(domain_map).forEach(entry => {
          const index = entry[0];
          const val = entry[1];
          domain_map_as_rgb[index] = `rgb(${hexToRgb_(val, false).join(', ')}`;
        });

        datasets = datasets.filter(__set => {
          return __set.data.length > 0;
        });

        const uniques = _.uniqBy(
          result.map(item => {
            return item.series;
          }),
          function(e) {
            return e.name;
          }
        );

        let customColors = [];

        if (uniques && uniques[0]) {
          customColors = uniques[0].map(item => {
            return domain_map_as_rgb[item.name];
          });
        }

        // console.table(datasets);
        // console.table(labels);

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: annotationTechniques.noop,
          tickOptions: {
            callback: function(value, _, __) {
              return `${value}`;
            }
          },
          onAnimationComplete: onAnimationCompleteTechniques.noop,
          stackOptions: {
            stacked: true
          },
          customColors: customColors,
          labelCallback: function(tooltipItem, data) {
            // let label = data.labels[tooltipItem.index + 1] || '';
            const label = tooltipItem.datasetIndex + 1;
            const value =
              data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

            return `Risk Class ${label}: ${value}`;
          }
        });
      }
    },
    NumberOfOffersPerLobPerRiskClassConcludedStackedVerticalBarChart: {
      view: undefined,
      colorScheme: {
        name: 'vivid_green_to_red',
        selectable: true,
        group: 'Ordinal',
        domain: [
          '#FF0000',
          '#FF8D3F',
          '#EDED44',
          '#7CEA76',
          '#0C7F49'
        ].reverse()
      },
      handleIncomingData(inputData) {
        console.info(
          'NumberOfOffersPerLobPerRiskClassConcludedStackedVerticalBarChart',
          inputData
        );

        const hits = findByKeyInNestedObject(inputData, 'hits')['hits'];
        const buckets = findByKeyInNestedObject(inputData, 'buckets')[
          'buckets'
        ];

        const applicable_values = ['1', '2', '3', '4', '5'];

        this.totalCount = hits.total;
        const result = [];

        const labels = [];
        let datasets = applicable_values.map(val => {
          return {
            label: val,
            data: [],
            stack: 1,
            hoverBackgroundColor: ''
          };
        });

        buckets.map(item => {
          const risk_class_buckets = findByKeyInNestedObject(
            item.risk_class,
            'buckets'
          )['buckets'];
          const series = [];
          risk_class_buckets.map(bucket => {
            if (applicable_values.indexOf(bucket.key) !== -1) {
              series.push({
                name: bucket.key,
                value: bucket.doc_count
              });
              datasets[parseInt(bucket.key, 10) - 1].data.push(
                bucket.doc_count
              );
            }
          });

          if (!labels.includes(item.key)) {
            labels.push(item.key);
          }

          result.push({
            name: item.key,
            series: sortArrayByKey(series, 'name')
          });
        });

        const domain_map = {
          '1': '#0C7F49',
          '2': '#7CEA76',
          '3': '#EDED44',
          '4': '#FF8D3F',
          '5': '#FF0000'
        };

        const domain_map_as_rgb = {};
        Object.entries(domain_map).forEach(entry => {
          const index = entry[0];
          const val = entry[1];
          domain_map_as_rgb[index] = `rgb(${hexToRgb_(val, false).join(', ')}`;
        });

        datasets = datasets.filter(__set => {
          return __set.data.length > 0;
        });

        const uniques = _.uniqBy(
          result.map(item => {
            return item.series;
          }),
          function(e) {
            return e.name;
          }
        );

        let customColors = [];

        if (uniques && uniques[0]) {
          customColors = uniques[0].map(item => {
            return domain_map_as_rgb[item.name];
          });
        }

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: annotationTechniques.noop,
          tickOptions: {
            callback: function(value, _, __) {
              return `${value}`;
            }
          },
          onAnimationComplete: onAnimationCompleteTechniques.noop,
          stackOptions: {
            stacked: true
          },
          customColors: customColors,
          labelCallback: function(tooltipItem, data) {
            // let label = data.labels[tooltipItem.index + 1] || '';
            const label = tooltipItem.datasetIndex + 1;
            const value =
              data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

            return `Risk Class ${label}: ${value}`;
          }
        });
      }
    },
    MostRepresentedBusinessPartners: {
      legend: true,
      legendTitle: 'Most Represented Business Partners',
      handleIncomingData(inputData) {
        if (!inputData.aggregations) {
          this.destroyCanvasChart();
          return false;
        }

        const buckets = inputData.aggregations.filter_agg.filter_agg.buckets;

        if (!buckets.length) {
          this.destroyCanvasChart();
          return false;
        }

        const bucket_count = sumByMapReduce(buckets);

        const legendMap = {};
        const labels = [];
        const datasets = [
          {
            label: 'All | just_values',
            data: [],
            stack: 1
          }
        ];

        const max_arr = [];

        buckets.map((item, ind) => {
          datasets[0].data.push(item.doc_count);
          const letter = nthLetterOfAlphabet(ind).toUpperCase();
          labels.push(letter);
          legendMap[letter] = {
            key: `${item.key}`,
            count: `${item.doc_count}`,
            percentage: `${percentageOfBuckets(item.doc_count, bucket_count)}`
          };
          max_arr.push(item.doc_count);
        });

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: null,
          tickOptions: {
            callback: function(value, ___, __) {
              return `${value}`;
            },
            max: roundUpToNearestN(Math.max.apply(null, max_arr))
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.noop,
          stackOptions: {
            stacked: false
          },
          labelCallback: function(tooltipItem, data) {
            return `${stripHtml(legendMap[tooltipItem.xLabel])}`;
          }
        });
        this.legendMap = jsonToKeyValueTable(legendMap);
      }
    },
    MostRepresentedBusinessPartnersVsPremium: {
      legend: true,
      legendTitle: 'Most Represented Business Partners',
      handleIncomingData(inputData) {
        if (!inputData.aggregations) {
          this.destroyCanvasChart();
          return false;
        }

        const buckets = findByKeyInNestedObject(
          inputData.aggregations,
          'buckets'
        ).buckets;

        if (!buckets.length) {
          this.destroyCanvasChart();
          return false;
        }

        const bucket_count = buckets
          .map(item => item.filter_agg.filter_agg.sum)
          .reduce((a, b) => a + b);

        const legendMap = {};
        const labels = [];
        const datasets = [
          {
            label: 'All | just_values',
            data: [],
            stack: 1
          }
        ];

        const max_arr = [];

        buckets.map((item, ind) => {
          const prem_sum = item.filter_agg.filter_agg.sum;

          datasets[0].data.push(prem_sum);
          const letter = nthLetterOfAlphabet(ind).toUpperCase();
          labels.push(letter);
          legendMap[letter] = {
            key: `${item.key}`,
            count: `${pprintLargePrice(prem_sum, CURRENCY_PREFIX_ISO())}`,
            percentage: `${percentageOfBuckets(prem_sum, bucket_count)}`
          };
          max_arr.push(prem_sum);
        });

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: null,
          tickOptions: {
            callback: function(value) {
              return `${pprintLargePrice(value, CURRENCY_PREFIX_ISO())}`;
            },
            max: Math.max.apply(null, max_arr)
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.noop,
          stackOptions: {
            stacked: false
          },
          labelCallback: function(tooltipItem, data) {
            return `${stripHtml(legendMap[tooltipItem.xLabel])}`;
          }
        });
        this.legendMap = jsonToKeyValueTable(legendMap);
      }
    },

    MostRepresentedDistributionChannels: {
      legend: true,
      legendTitle: 'Most Represented Distribution Channels',
      handleIncomingData(inputData) {
        if (!inputData.aggregations) {
          this.destroyCanvasChart();
          return false;
        }

        const buckets = inputData.aggregations.filter_agg.filter_agg.buckets;
        const bucket_count = sumByMapReduce(buckets);

        const legendMap = {};
        const labels = [];
        const datasets = [
          {
            label: 'All | just_values',
            data: [],
            stack: 1
          }
        ];

        const max_arr = [];

        buckets.map((item, ind) => {
          datasets[0].data.push(item.doc_count);
          const letter = nthLetterOfAlphabet(ind).toUpperCase();
          labels.push(letter);
          legendMap[letter] = {
            key: `${item.key}`,
            count: `${item.doc_count}`,
            percentage: `${percentageOfBuckets(item.doc_count, bucket_count)}`
          };
          max_arr.push(item.doc_count);
        });

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: null,
          tickOptions: {
            callback: function(value, ___, __) {
              return `${value}`;
            },
            max: roundUpToNearestN(Math.max.apply(null, max_arr))
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.just_values,
          stackOptions: {
            stacked: false
          },
          labelCallback: function(tooltipItem, data) {
            return `${stripHtml(legendMap[tooltipItem.xLabel])}`;
          }
        });
        this.legendMap = jsonToKeyValueTable(legendMap);
      }
    },
    UnderwriterResponseTimes: {
      legend: true,
      legendTitle: 'Underwriter Response Times',
      handleIncomingData(inputData) {
        if (!inputData.aggregations) {
          this.destroyCanvasChart();
          return false;
        }

        const aggs = inputData.aggregations;
        const num_of_subjects = Object.keys(aggs).length;

        if (num_of_subjects < 1) {
          this.destroyCanvasChart();
          return false;
        }

        const legendMap = {};
        const hoverLegendMap = {};
        const labels = [];
        const datasets = [];

        const max_arr = [];

        Object.keys(aggs).map((o_key, ind) => {
          const innerAggs = aggs[o_key].aggs.values;
          const underwriter_name = (window as any).mappers
            .UnderwritersUsernames[o_key.replace('times_', '')];

          const label = `Aggs for ${o_key} | just_values`;

          datasets.push({
            label: label,
            data: [],
            stack: ind + 1
          });

          Object.keys(innerAggs).map(key => {
            const val = parseInt(innerAggs[key], 10);
            key = `${parseFloat(key)}%`;

            if (!isNaN(val)) {
              datasets[ind].data.push(val);
              if (ind === 0) {
                labels.push(key);
              }
              legendMap[key] = {
                key: `${key}`,
                count: `${humanReadableTimeFrame(val)}`,
                percentage: ` `
              };
              hoverLegendMap[`${label},${key}`] = {
                key: `${underwriter_name}: ${key}-ile`,
                count: `${humanReadableTimeFrame(val)}`,
                percentage: ` `
              };
              max_arr.push(val);
            }
          });
        });

        this.handleOnIncoming(datasets, labels, {
          annotationTechniques: null,
          tickOptions: {
            callback: function(value, ___, __) {
              return `${value}`;
            },
            max: roundUpToNearestN(Math.max.apply(null, max_arr), true)
          },
          legendOptions: null,
          onAnimationComplete: onAnimationCompleteTechniques.just_values,
          stackOptions: {
            stacked: false
          },
          labelCallback: function(tooltipItem, data) {
            const label = data.datasets[tooltipItem.datasetIndex]['label'];
            return `${stripHtml(
              hoverLegendMap[`${label},${tooltipItem.xLabel}`]
            )}`;
          }
        });
        // this.legendMap = jsonToKeyValueTable(legendMap);
        this.legendMap = null;
      }
    }
  }
};
