import { Component, OnInit, Input } from '@angular/core';
import { ElasticService } from '../services/elastic.service';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent implements OnInit {
  @Input() klass;
  @Input() text;
  public checked = false;

  constructor(private es: ElasticService) { }

  get isChecked() {
    return this.checked;
  }

  ngOnInit() {
    this.es.app_component.setComponent(this.klass, this);
  }

  onCheckboxChange($event) {
    this.es.search();
  }

}
