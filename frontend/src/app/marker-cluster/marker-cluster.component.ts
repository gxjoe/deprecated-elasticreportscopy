/// <reference types="@types/googlemaps" />

import { Component, Input, ViewEncapsulation, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-marker-cluster',
  templateUrl: './marker-cluster.component.html',
  styleUrls: ['./marker-cluster.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MarkerClusterComponent extends (google.maps.OverlayView as new () => any) implements OnDestroy {
  private id_;
  private center;
  private div_;
  private span_;
  private listeners_;
  private count;

  @Input() lat: number;
  @Input() lng: number;
  @Input() size: number;
  @Input() map: google.maps.Map;
  @Input() position: google.maps.LatLng;
  @Input() co;

  constructor() {
    super();
  }

  ngOnInit() {
    const values = {
      co: this.co,
      map: this.map,
      lat: this.lat,
      lng: this.lng,
      position: this.position,
      size: this.size
    };

    // Initialization
    this.setValues(values);

    this.center = this.position;
    this.count = this.size;

    switch (true) {
      case this.size <= 45:
        this.size = 45;
        break;
      case this.size > 45 && this.size <= 500:
        this.size = 60;
        break;
      case this.size > 500 && this.size <= 2000:
        this.size = 75;
        break;
      case this.size > 2000:
        this.size = 90;
        break;
    }

    const div = this.div_ = document.createElement('div');
    const span = this.span_ = document.createElement('span');
    div.appendChild(span);

    this.span_.innerHTML = '' + this.count + '';
    div.classList.add('cluster');
    div.classList.add('size' + this.size);
  }

  ngOnDestroy() {
    this.onRemove();
  }

  calculateProjectedContentMetrics(text: string, font: string) {
    const f = font || '13px Roboto, Arial, sans-serif';
    const o = document.createElement('span');
    o.style.width = '220px';
    o.innerText = text;
    o.style.font = f;
    o.style.float = 'left';
    o.style.whiteSpace = 'nowrap';

    document.appendChild(o);

    const ref_ = { 'width': o.scrollWidth, 'height': o.scrollHeight };
    o.parentNode.removeChild(o);

    return ref_;
  }


  calculateProjectedHoverDataLeftOffset(data_width) {
    const self = this.div_;
    const difference = Math.abs(self.scrollWidth - data_width);
    return ((-difference / 2) + 'px');
  }

  onAdd() {
    const pane: any = this.getPanes().overlayLayer;

    const overlayMouseTarget: any = this.getPanes().overlayMouseTarget;

    pane.style['zIndex'] = 50000;
    overlayMouseTarget.style['zIndex'] = 10;

    overlayMouseTarget.appendChild(this.div_);

    const self = this.div_;

    // Ensures the label is redrawn if the text or position is changed.
    const me = this;

    this.listeners_ = [
      google.maps.event.addListener(this, 'position_changed',
        function () {
          me.draw();
        }),
      google.maps.event.addListener(this, 'text_changed',
        function () {
          me.draw();
        })
    ];

    // custom mouse over event added
    google.maps.event.addDomListener(self, 'mouseover',
      function () {
        const alreadyHasChild = Boolean(document.querySelectorAll('#clusterPriceRange' + me.id_));

        let inner_html,
          metrics,
          min_width;


        if (!alreadyHasChild) {
          const priceRangeDiv = document.createElement('div');
          priceRangeDiv.id = 'clusterPriceRange' + me.id_;

          inner_html = '';

          metrics = me.calculateProjectedContentMetrics(inner_html, '');

          min_width = metrics.width + 25;

          priceRangeDiv.style.minWidth = min_width + 'px';
          priceRangeDiv.className = 'cluster-info-window';
          priceRangeDiv.innerHTML = '';

          priceRangeDiv.style.left = me.calculateProjectedHoverDataLeftOffset(metrics.width + 50);

          self.appendChild(priceRangeDiv);
        }

        self.classList.toggle('hover');
      });

    // custom mouseout event added
    google.maps.event.addDomListener(self, 'mouseout',
      function () {
        const element = document.getElementById('clusterPriceRange' + me.id_);
        if (element && element.parentNode) {
          element.parentNode.removeChild(element);
        }

        self.classList.toggle('hover');
      });

    // custom click event added
    google.maps.event.addDomListener(self, 'click', function () {
      console.info(me);
      me.map.setZoom(me.map.getZoom() + 1);

      setTimeout(() => {
        me.map.setCenter(me.center);
      }, 80);
    });

  }

  draw() {
    const projection = this.getProjection();
    const position = projection.fromLatLngToDivPixel(this.get('position'));

    const div = this.div_;
    div.style.left = (position.x - this.size / 2) + 'px';
    div.style.top = (position.y - this.size / 2) + 'px';
  }

  onRemove() {
    try {
      // Label is removed from the map, stop updating its position/text.
      if (this.listeners_) {
        for (let i = 0, I = this.listeners_.length; i < I; ++i) {
          google.maps.event.removeListener(this.listeners_[i]);
        }
      }

      // Remove all listeners from this instance.
      google.maps.event.clearInstanceListeners(this);
      google.maps.event.clearInstanceListeners(this.div_);

      this.div_.parentNode.removeChild(this.div_);
    } catch (e) {
      console.error(e);
    }
  }
}
