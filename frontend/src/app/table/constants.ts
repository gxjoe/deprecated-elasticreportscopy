import { findByKeyInNestedObject } from '../elastic/utils';
import { pprintCurrency, pprintFloat } from '../utils/helpers/helpers';

export const EMPTYISH_DATASET = [{ '1': '2' }];

export const TABLE_OPTIONS = {
  TableExploration: {
    columns: [
      {
        description: 'Total Count',
        content: 0
      }
    ],
    handleIncomingData(inputData) {
      this.dataSet = inputData.hits.hits.map(item => {
        return item._source;
      });
    }
  }
};
