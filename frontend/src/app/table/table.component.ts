import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef
} from '@angular/core';
import { OnIncomingData } from '../interfaces/on-incoming-data';
import { ElasticService } from '../services/elastic.service';
import { TABLE_OPTIONS, EMPTYISH_DATASET } from './constants';
import { Observable } from 'rxjs';
import { map } from 'node_modules/rxjs/operators';
import { findByKeyInNestedObject } from '../elastic/utils';
import { environment } from '../../environments/environment';
import {
  extractRiskFromBusinessActivityName,
  extractRiskFromRiskClassAndPeril,
  extractAlphaBracketsFromColName
} from '../charts/histogram-table-chart/constants';
import { NaceCodes } from '../charts/histogram-table-chart/histogramListsAndCategories';
import {
  regexMatches,
  extractNumberFromString
} from '../utils/helpers/helpers';
import {
  numericalCategoryPrefacedWithLettersStandaloneRegex,
  destringifySemiNumericalCategoryName,
  csvToJSON
} from '../charts/histogram-table-chart/common';
import {
  lobVsColor,
  getKeyByValue,
  hex_with_opacity,
  RISK_PROFILE_COLORS
} from '../charts/common/constants';

const DEFAULT_EMPTYISH_NOTICE =
  'Click the \'refresh\' button above to fetch new data.';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TableComponent
  implements OnInit, OnIncomingData, OnChanges {
  @Input()
  klass;

  @Input()
  csvInput;

  @Input()
  cssClasses;

  @Input()
  customEmptyishInfoNotice;

  @Input() hidePagination;

  @Input() pageSize = 25

  dataSet: any = EMPTYISH_DATASET.slice();
  prefetchOptions: Observable<string[] | object>;
  private mapping: object;

  sorting_needed = true;

  private sortName;
  private sortValue;

  headers = [];

  public headerTooltipCallback: Function | null = el => el;

  constructor(private es: ElasticService, private cd: ChangeDetectorRef) {
  }

  get isEmptyish() {
    return (
      JSON.stringify(this.dataSet) === JSON.stringify(EMPTYISH_DATASET) ||
      this.dataSet.length < 1
    );
  }

  lobVsColor(input) {
    return lobVsColor(input);
  }

  extractAlphaBracketsFromColName(colname) {
    return extractAlphaBracketsFromColName(colname);
  }

  extractRiskFromText(txt) {
    return extractRiskFromBusinessActivityName(txt);
  }

  extractRiskClassFromText(klass) {
    return extractRiskFromRiskClassAndPeril(klass);
  }

  extractBgColorFromIndexInRow(
    data: any,
    obj: { item: string; index: number }
  ) {
    if (!this.klass || !this.klass.includes('GrossRiskProfile')) {
      return 'transparent';
    }

    const dict_key = getKeyByValue(data, obj.item);

    if (
      data.Country &&
      data.Country !== 'All Countries Combined' &&
      dict_key &&
      ['LoB %', 'Premium %', 'SI %', 'Average Rate / Combined Rate %'].includes(
        dict_key
      )
    ) {
      const percentages_bg_colors = {
        PASS: RISK_PROFILE_COLORS.Percentages[0][0],
        DANGER: RISK_PROFILE_COLORS.Percentages[1][0],
        FAIL: RISK_PROFILE_COLORS.Percentages[2][0],

        NONCHALANT: 'transparent'
      };

      const avg_rate_bg_colors = {
        PASS: RISK_PROFILE_COLORS['Average Rate / Combined Rate %'][0][0],
        DANGER: RISK_PROFILE_COLORS['Average Rate / Combined Rate %'][1][0],
        FAIL: RISK_PROFILE_COLORS['Average Rate / Combined Rate %'][2][0],

        NONCHALANT: 'transparent'
      };

      const parsed_percentage = extractNumberFromString(obj.item);

      switch (dict_key) {
        case 'LoB %':
        case 'Premium %':
        case 'SI %':
          if (isNaN(parsed_percentage)) {
            return percentages_bg_colors.NONCHALANT;
          }

          if (parsed_percentage <= 10) {
            return percentages_bg_colors.PASS;
          }

          if (parsed_percentage > 10 && parsed_percentage <= 19) {
            return percentages_bg_colors.DANGER;
          }

          return percentages_bg_colors.FAIL;
        case 'Average Rate / Combined Rate %':
          if (
            isNaN(parsed_percentage) ||
            Math.abs(parsed_percentage) === Infinity
          ) {
            return avg_rate_bg_colors.NONCHALANT;
          }

          if (parsed_percentage > 110) {
            return avg_rate_bg_colors.PASS;
          }

          if (parsed_percentage >= 90 && parsed_percentage <= 110) {
            return avg_rate_bg_colors.DANGER;
          }

          return avg_rate_bg_colors.FAIL;
      }
    }

    return 'transparent';
  }

  extractNumericalTendencyFromIndexInRow(
    data: any,
    obj: { item: string; index: number }
  ): '' | 'tendencyUp' | 'tendencyDown' {
    if (!this.klass.includes('PerRiskScore')
      && !this.klass.includes('minusTimeRange')) {
      return '';
    }

    if (!this.klass.includes('minusTimeRange')) {
      if (!data || !data.deviation || !obj.item) {
        return '';
      }

      if (obj.item !== data.deviation) {
        return '';
      }
    }

    try {
      const num = extractNumberFromString(obj.item);

      switch (true) {
        case num === 0:
          return '';
        case num < 0:
          return 'tendencyDown';
        default:
          return 'tendencyUp';
      }
    } catch (err) {
      return '';
    }
  }

  attemptToStringify(input: any, data: any) {
    try {
      return JSON.stringify({ input, data });
    } catch (err) {
      return '';
    }
  }

  ngOnInit() {
    if (!this.customEmptyishInfoNotice) {
      this.customEmptyishInfoNotice = DEFAULT_EMPTYISH_NOTICE;
    }

    if (!this.klass || this.klass.toLowerCase().includes('histo')) {
      if (this.klass && this.klass.toLowerCase().includes('nacecodes')) {
        this.headerTooltipCallback = el => {
          if (
            !regexMatches(
              numericalCategoryPrefacedWithLettersStandaloneRegex,
              el
            )
          ) {
            return el;
          }
          return `${el}: ${NaceCodes[destringifySemiNumericalCategoryName(el)]
            }`;
        };
      }

      return;
    }

    if (this.klass && this.klass in TABLE_OPTIONS) {
      this.es.app_component.setComponent(this.klass, this);
      Object.assign(this, TABLE_OPTIONS[this.klass]);
      this.handleIncomingData = TABLE_OPTIONS[this.klass].handleIncomingData;
    }

    this.prefetchOptions = this.es
      ._constructRequest('get', {}, '/_mapping')
      .pipe(
        map((res: any) => {
          const indexObj = res[environment.elasticsearch.index_name];
          return indexObj.mappings[Object.keys(indexObj.mappings)[0]]
            .properties;
        })
      )
      .pipe(
        map((dict: any) => {
          const to_ret = {};
          for (const [key, value] of Object.entries(dict)) {
            to_ret[key] = `${findByKeyInNestedObject(value, 'type').type}`;
          }
          return to_ret;
        })
      );

    // this.preload();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.csvInput && changes.csvInput.currentValue) {
      this.sorting_needed = false;
      const csv_values = changes.csvInput.currentValue;

      const { headers, result: dataSet } = csvToJSON(csv_values);

      this.dataSet = dataSet;
      this.headers = headers;

      this.hidePagination = this.hidePagination || this.dataSet.length <= this.pageSize
      // this.cd.detectChanges();
    }
  }

  handleIncomingData() { }

  preload(): void {
    this.prefetchOptions.subscribe(data => {
      this.mapping = data;
    });
  }

  sort(sort: { key: string; value: string }): void {
    this.sortName = sort.key;
    this.sortValue = sort.value;
  }

  search(): void {
    /** filter data **/
    /** sort data **/
    console.warn(this.sortName, this.sortValue);
    if (this.sortName && this.sortValue) {
      this.dataSet = this.dataSet.sort((a, b) =>
        this.sortValue === 'ascend'
          ? a[this.sortName] > b[this.sortName]
            ? 1
            : -1
          : b[this.sortName] > a[this.sortName]
            ? 1
            : -1
      );
    }
  }

  regenerateContent() {
    this.es.search(this.klass);
  }
}
