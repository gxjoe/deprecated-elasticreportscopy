import { regexMatches, CURRENCY_PREFIX_ISO, isString } from '../utils/helpers/helpers';
import { environment } from 'src/environments/environment';
import { emptyBoolMustMatch } from './utils';
import { SuccessfullyParsedXMLResponse } from '../services/auth.service';
import { nullifyGlobalDatetimeFiltersInTimeRangeQueries } from '../charts/histogram-table-chart/queryBuilder';

export enum ElasticFilterSubtypes {
  bool = 'bool',
  range = 'range',
  term = 'term'
}

export enum ElasticRangeOperators {
  gt = 'gt', // >
  lt = 'lt', // <
  gte = 'gte', // >=
  lte = 'lte' // <=
}

export enum ElasticQueryClauses {
  must = 'must',
  should = 'should',
  filter = 'filter',
  must_not = 'must_not'
}

const address_focused_regexes = [
  /offerdistribution/gi,
  /infolevel/gi,
  /address_based/g,
  /hotspot/gi,
  /maps_and_positions/gi,
  /PerConstructionMaterials/gi
];

interface IIndexNameChooserResp {
  query: object;
  index_name: string;
}

export const currencyResolver = (
  stringified_query: string | object,
  return_stringified = false
): string | object => {
  if (typeof stringified_query !== 'string') {
    stringified_query = JSON.stringify(stringified_query);
  }

  if (stringified_query.includes('$CURRENCY$')) {
    stringified_query = stringified_query.replace(
      /\$CURRENCY\$/gm,
      CURRENCY_PREFIX_ISO()
    );
  }

  if (return_stringified) {
    return stringified_query;
  }

  return JSON.parse(stringified_query);
};

export function setCookie(name: string, value: string, days?: number) {
  let expires = '';
  if (days) {
    const date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = '; expires=' + date.toUTCString();
  }
  document.cookie = name + '=' + (value || '') + expires + '; path=/';
}

export function getCookie(name: string) {
  const nameEQ = name + '=';
  const ca = document.cookie.split(';');
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return null;
}

export function eraseCookie(name: string) {
  document.cookie = name + '=; Max-Age=-99999999;';
}

export const authQueryFilter = (index_name?: string) => {
  if (!window['CBNBOOST_QUERY_RESTRICTIONS']) {
    if (!getCookie('x-cbnboost-restrictions')) {
      return emptyBoolMustMatch();
    } else {
      window['CBNBOOST_QUERY_RESTRICTIONS'] = JSON.parse(
        getCookie('x-cbnboost-restrictions')
      );
    }
  }

  const restrictions: SuccessfullyParsedXMLResponse =
    window['CBNBOOST_QUERY_RESTRICTIONS'];
  if (!index_name) {
    return restrictions.userType_based_queries;
  }

  return adddressPathTransformer(
    restrictions.userType_based_queries,
    !index_name.includes('address') ? 'addresses_to_lob' : 'lob_to_addresses'
  );
};

const adddressPathTransformer = (
  _body: any,
  direction: 'lob_to_addresses' | 'addresses_to_lob'
) => {
  const was_array = Array.isArray(_body);
  let replaced;
  if (was_array) {
    _body = JSON.stringify(_body);
  }

  if (direction === 'addresses_to_lob') {
    replaced = _body.replace(
      /address\.locationProperty/g,
      'lob.addresses.locationProperty'
    );
  } else {
    replaced = _body.replace(
      /lob\.addresses\.locationProperty/g,
      'address.locationProperty'
    );
  }

  if (was_array) {
    return JSON.parse(replaced);
  }

  return replaced;
};

export const indexNameChooser = (
  query_name: string,
  fullQuery: any
): IIndexNameChooserResp => {
  const take_addresses_based_on_query_name = address_focused_regexes.some(rx =>
    regexMatches(rx, query_name)
  );

  if (fullQuery) {
    if (isString(fullQuery)) {
      fullQuery = JSON.parse(fullQuery);
    }

    if (!fullQuery.query) {
      fullQuery.query = {};
    }

    if (!fullQuery.query.bool) {
      fullQuery.query.bool = {};
    }

    if (!fullQuery.query.bool.must || !fullQuery.query.bool.must.length) {
      fullQuery.query.bool.must = [];
    }

    fullQuery.query.bool.must = []
      .concat(fullQuery.query.bool.must)
      .concat(authQueryFilter());

    let as_json: string | any = JSON.stringify(fullQuery);

    if (
      as_json.includes('address.locationProperty')
      // as_json.includes('lob.addresses') &&
    ) {
      if (!take_addresses_based_on_query_name) {
        as_json = adddressPathTransformer(as_json, 'addresses_to_lob');
      } else {
        as_json = adddressPathTransformer(as_json, 'lob_to_addresses');
      }
    }

    as_json = currencyResolver(as_json, true) as string;

    as_json = nullifyGlobalDatetimeFiltersInTimeRangeQueries(query_name, as_json);

    if (
      as_json.includes('address.locationProperty') ||
      take_addresses_based_on_query_name
    ) {
      return {
        query: JSON.parse(as_json),
        index_name: environment.elasticsearch.address_based_index_name
      };
    }

    return {
      query: JSON.parse(as_json),
      index_name: environment.elasticsearch.index_name
    };
  }
};
