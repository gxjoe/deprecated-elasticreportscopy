/// <reference types="@types/googlemaps" />

import * as objectHash from 'object-hash';
import * as objectPath from 'object-path';
import { featureCollection, lineString, point } from '@turf/helpers';
import { environment } from '../../environments/environment';
import { SearchResponse } from 'elasticsearch';
import {
  hex_with_opacity,
  pretty_colors,
  semiRandomIndex,
  lobVsColor,
  getOfferColor,
} from '../charts/common/constants';
import { generateSvgIcon, generatePolyline } from '../utils/helpers/gmaps';
import {
  pprintCurrency,
  regexMatches,
  CURRENCY_PREFIX_UTF,
  CURRENCY_PREFIX_ISO,
  flattenArrays,
  sortArrOfStringsAlphabetically,
  getPerilSpecificLocationTSI,
} from '../utils/helpers/helpers';
import { ImapDrawing } from '../interfaces/ImapDrawing';
import moment from 'moment';
import { constructNestedLocationAssessmentQueryString } from '../charts/histogram-table-chart/constants';
import * as B from 'elastic-builder';

const constructLocationPerilPath = (is_lob_based = false) => {
  if (is_lob_based) {
    return 'lob.addresses.locationProperty.locationAssessments';
  }
  return 'address.locationProperty.locationAssessments';
};

export const googleMapsBoundsToESBounds = (
  bounds: google.maps.LatLngBounds
) => {
  const ne = bounds.getNorthEast();
  const sw = bounds.getSouthWest();

  return {
    coordinates: {
      top_left: {
        lat: ne.lat(),
        lon: sw.lng(),
      },
      bottom_right: {
        lat: sw.lat(),
        lon: ne.lng(),
      },
    },
  };
};

export const constructNestedGeoQuery = (path, geo_field) => {
  const obj = {
    nested: {
      path: path,
      query: {
        geo_bounding_box: {},
      },
    },
  };

  obj.nested.query.geo_bounding_box[geo_field] = {};
  return obj;
};

export const constructNestedCustomBoundaryQuery = (
  data: ImapDrawing,
  nested_path,
  nested_field
) => {
  let query;
  if (data.type === 'polygon') {
    const pairs = data.path
      .match(/\(([0-9.,-]+)\)/g)
      .map((el) => el.replace(/[^0-9.,-]+/g, ''));
    const points = [];

    pairs.map((pair) => {
      const [lat, lng] = pair.split(',').map(parseFloat);

      points.push({
        lat: lat,
        lon: lng, // elastic accepts lon and not lng
      });
    });

    query = {
      nested: {
        path: nested_path,
        query: {
          geo_polygon: {
            [nested_field]: {
              points: points,
            },
          },
        },
      },
    };
  } else if (data.type === 'circle') {
    query = {
      nested: {
        path: nested_path,
        query: {
          geo_distance: {
            [nested_field]: {
              lat: data.path.center.lat,
              lon: data.path.center.lng, // elastic accepts lon and not lng
            },
            distance: data.path.radius,
          },
        },
      },
    };
  }
  return query;
};

export const findPath = (a, obj) => {
  for (const key in obj) {
    // for each key in the object obj
    if (obj.hasOwnProperty(key)) {
      // if it's an owned key
      if (a === obj[key]) {
        return key;
      } else if (obj[key] && typeof obj[key] === 'object') {
        // otherwise if the item at this key is also an object
        const path = findPath(a, obj[key]); // search for the item a in that object
        if (path) {
          return key + '.' + path;
        } // if found then the path is this key followed by the result of the search
      }
    }
  }
};

export const deleteByPath = (obj, path) => {
  objectPath.del(obj, path);
};

export const mutateObjectProperty = (
  prop,
  value,
  obj,
  replace = false,
  delete_parent = false
) =>
  obj &&
  obj.constructor === Object &&
  Object.keys(obj).forEach((key) => {
    if (key === prop) {
      if (delete_parent) {
        console.warn('would delete parent', obj);
        return;
      }

      if (!replace) {
        obj[key] = value;
      } else {
        delete obj[key];
        obj[prop] = value;
      }
      return;
    }
    if (Array.isArray(obj[key])) {
      obj[key].map((item) => {
        mutateObjectProperty(prop, value, item, replace, delete_parent);
      });
    } else {
      mutateObjectProperty(prop, value, obj[key], replace, delete_parent);
    }
  });

export const setObjectPropertyByPath = (path, value, schema) => {
  const pList = path.split('.');
  const len = pList.length;
  for (let i = 0; i < len - 1; i++) {
    const elem = pList[i];
    if (!schema[elem]) {
      schema[elem] = {};
    }
    schema = schema[elem];
  }

  schema[pList[len - 1]] = value;
};

export const findByKeyInNestedObject = (data: Object, key: string): any => {
  if (!data) {
    return null;
  }

  if (data.hasOwnProperty(key)) {
    return data;
  }

  for (let i = 0; i < Object.keys(data).length; i++) {
    if (typeof data[Object.keys(data)[i]] === 'object') {
      const obj = findByKeyInNestedObject(data[Object.keys(data)[i]], key);
      if (obj != null) {
        return obj;
      }
    }
  }

  return null;
};

export const bucketsToHeatmapData = (
  buckets: Array<any>
): google.maps.visualization.HeatmapLayer => {
  const prepared_buckets = buckets.map((bucket) => {
    return {
      location: new google.maps.LatLng(
        bucket.centroid.location.lat,
        bucket.centroid.location.lon
      ),
      weight: bucket.count,
    };
  });

  return new google.maps.visualization.HeatmapLayer({
    data: prepared_buckets,
  });
};

export const bucketsToClusterData = (
  map: google.maps.Map,
  buckets: Array<any>
): Array<any> => {
  buckets = buckets['buckets'];

  const prepared_buckets = buckets.map((bucket) => {
    return {
      position: new google.maps.LatLng(
        bucket.centroid.location.lat,
        bucket.centroid.location.lon
      ),
      lat: bucket.centroid.location.lat,
      lng: bucket.centroid.location.lon,
      size: bucket.doc_count,
      cluster_obj: bucket,
      map: map,
    };
  });

  return prepared_buckets;
};

export const groupGeoHits = (
  { mapKlasses, current: currentKlass },
  resp: SearchResponse<any>,
  bounds: google.maps.LatLngBounds,
  isHotspotModeActive: boolean
) => {
  const grouped_hits = {};
  const color_mapping = {};
  const polylines = {};

  const collection = featureCollection([]);
  const lineStringCollection = featureCollection([]);

  switch (currentKlass) {
    case mapKlasses.OfferDistribution:
      resp.hits.hits.map((h: any, ind) => {
        const offer_id = String(h._source.offer.number);

        if (!grouped_hits[offer_id]) {
          grouped_hits[offer_id] = {};
        }

        const lob = h._source.lob;
        const _address = h._source.address;

        if (!grouped_hits[offer_id][lob.id] && _address) {
          grouped_hits[offer_id][lob.id] = [];
        }

        if (_address.coordinates) {
          const coordinates = _address.coordinates
            .split(',')
            .reverse()
            .map(parseFloat);

          if (
            bounds.contains({
              lat: coordinates[1],
              lng: coordinates[0],
            })
          ) {
            let base_color;
            if (offer_id in color_mapping) {
              base_color = color_mapping[offer_id];
            } else {
              base_color = getOfferColor(offer_id);
              color_mapping[offer_id] = base_color;
            }

            const lob_color = hex_with_opacity(base_color, 0.8);
            const address_color = hex_with_opacity(base_color, 1);
            const locationProperty = _address['locationProperty'];

            delete _address['locationProperty'];

            const TSI = isHotspotModeActive
              ? // take the peril specific TSI if any only if
              // in the hotspot mode which is determined via
              // the number of hotspotDivs mirrored as `hotspotModeActive$`
              // in the gmaps service
              getPerilSpecificLocationTSI(h._source).tsi
              : _address[
              `tsiPerLocation_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`
              ];

            const point_ = point(coordinates, {
              position: {
                lat: coordinates[1],
                lng: coordinates[0],
              },
              ids: {
                offer: String(offer_id),
                lob: String(lob.id),
                address: String(_address.id),
              },
              colors: {
                offer: base_color,
                lob: lob_color,
                address: color_mapping[offer_id],
              },
              icon: generateSvgIcon(address_color, 1, TSI || 0),
              meta: {
                icon: generateSvgIcon(address_color, 1, TSI || 0, true),
              },
              address_data: {},
              other_data: {
                LOB: {
                  Client: h._source.offer.client.name,
                  'Share (whole LoB)': `${lob.share}%`,
                  _______: '_______',
                  [`Premium ${CURRENCY_PREFIX_UTF()}`]: pprintCurrency(
                    lob[`premium_${CURRENCY_PREFIX_ISO()}`] / lob.share
                  ),
                  [`Premium ${CURRENCY_PREFIX_UTF()} (share)`]: pprintCurrency(
                    lob[`premium_${CURRENCY_PREFIX_ISO()}`]
                  ),
                  ________: '________',
                  [`TSI ${CURRENCY_PREFIX_UTF()}`]: pprintCurrency(
                    lob[`totalSumInsured_${CURRENCY_PREFIX_ISO()}`]
                  ),
                  [`TSI ${CURRENCY_PREFIX_UTF()} (share)`]: pprintCurrency(
                    lob[
                    `totalSumInsured_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`
                    ]
                  ),
                  _________: '_________',
                  LineOfBusiness: lob.lineOfBusiness,
                  Rating: lob.rating,
                  LocationProperty: locationProperty,
                },

                address: {
                  ...Object.keys(_address)
                    .filter((key) =>
                      [/country/, /city/, /street/, /coordinate/, /id/].some(
                        (rex) => regexMatches(rex, key)
                      )
                    )
                    .reduce((obj, key) => {
                      obj[key] = _address[key];
                      return obj;
                    }, {}),
                  ________: '________',
                  [`SI per Loc ${CURRENCY_PREFIX_UTF()}`]: pprintCurrency(
                    _address[`tsiPerLocation_${CURRENCY_PREFIX_ISO()}`]
                  ),
                  [`SI per Loc ${CURRENCY_PREFIX_UTF()} (share)`]:
                    pprintCurrency(
                      _address[
                      `tsiPerLocation_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`
                      ]
                    ),
                },
              },
            });

            grouped_hits[offer_id][lob.id].push(point_);
            collection.features.push(point_);
          }
        }
      });

      Object.values(grouped_hits).map((_group) => {
        const group = Object.values(_group)[0];
        if (group.length > 1) {
          const line_string = lineString(
            group.map((item) => item.geometry.coordinates),
            {
              color: group[0].properties.colors.address,
            }
          );
          lineStringCollection.features.push(line_string);

          const polyline = generatePolyline(
            line_string.geometry.coordinates,
            group[0].properties.colors.address
          );
          polylines[group[Object.keys(group)[0]].properties.ids.lob] = polyline;
        }
      });
      break;
    case mapKlasses.ClientDistribution:
      resp.hits.hits.map((h) => {
        const offer_id = String(h._source.offer.number);

        if (!grouped_hits[offer_id]) {
          grouped_hits[offer_id] = {};
        }

        const hit = h._source;

        const coordinates = hit.offer.client.coordinates
          .split(',')
          .reverse()
          .map(parseFloat);

        if (
          bounds.contains({
            lat: coordinates[1],
            lng: coordinates[0],
          })
        ) {
          const lob = hit.lob;

          let base_color;
          if (offer_id in color_mapping) {
            base_color = color_mapping[offer_id];
          } else {
            const color_of_lob = lobVsColor(lob.lineOfBusiness);
            base_color = color_of_lob; // pretty_colors[semiRandomIndex(offer_id, pretty_colors.length)];
            color_mapping[offer_id] = color_of_lob;
          }

          const lob_color = hex_with_opacity(base_color, 0.8);

          const point_ = point(coordinates, {
            position: {
              lat: coordinates[1],
              lng: coordinates[0],
            },
            ids: {
              offer: String(offer_id),
              lob: String(offer_id),
            },
            colors: {
              offer: base_color,
              lob: lob_color,
            },
            icon: generateSvgIcon(color_mapping[offer_id], 1, 1),
            meta: {
              icon: generateSvgIcon(color_mapping[offer_id], 1, 1 || 0, true),
            },
            other_data: {
              Offer: hit.offer,
              LOB: {
                Client: h._source.offer.client.name,
                'Share (whole LoB)': `${lob.share}%`,
                _______: '_______',
                [`Premium ${CURRENCY_PREFIX_UTF()}`]: pprintCurrency(
                  lob[`premium_${CURRENCY_PREFIX_ISO()}`] / lob.share
                ),
                [`Premium ${CURRENCY_PREFIX_UTF()} (share)`]: pprintCurrency(
                  lob[`premium_${CURRENCY_PREFIX_ISO()}`]
                ),
                ________: '________',
                [`TSI ${CURRENCY_PREFIX_UTF()}`]: pprintCurrency(
                  lob[`totalSumInsured_${CURRENCY_PREFIX_ISO()}`]
                ),
                [`TSI ${CURRENCY_PREFIX_UTF()} (share)`]: pprintCurrency(
                  lob[
                  `totalSumInsured_${CURRENCY_PREFIX_ISO()}_AdjustedForShare`
                  ]
                ),
                _________: '_________',
                LineOfBusiness: lob.lineOfBusiness,
              },
            },
          });

          grouped_hits[offer_id] = point_;
          collection.features.push(point_);
        }
      });
      break;
  }

  return {
    collection: collection,
    polylines: polylines,
    lineStringCollection: lineStringCollection,
  };
};

export const getNested = (p, o) =>
  p.reduce((xs, x) => (xs && xs[x] ? xs[x] : null), o);

export const hashObject = (obj) => {
  return objectHash(Object.assign({}, obj));
};

export const getHashedKey = (unhashed_key: any) => {
  if (typeof unhashed_key !== 'string') {
    unhashed_key = JSON.stringify(unhashed_key);
  }
  return hashObject({
    uh: environment.memcached_wrapper.endpoint + unhashed_key,
  });
};

export const constructObjectWithNamedProperty = (key, val) => {
  const obj = {};
  obj[key] = val;
  return obj;
};

export const arrayOfStringsToNewLineJSON = (arr) => {
  const index_obj = JSON.stringify({
    index: environment.elasticsearch.index_name,
  });
  const tmp_arr = [];

  arr.map((item) => {
    tmp_arr.push(index_obj);
    tmp_arr.push(item);
  });

  return tmp_arr.join('\n') + '\n';
};

export const constructMedianAgg = (path, field_name) => {
  return {
    nested: {
      path: path,
    },
    aggs: {
      median_agg: {
        filter: {
          bool: {
            must: {
              match_all: {},
            },
          },
        },
        aggs: {
          median_agg_deepest: {
            percentiles: {
              field: field_name,
              percents: [50],
            },
          },
        },
      },
    },
  };
};

export const extractMedianAgg = (resp: SearchResponse<any>) => {
  return findByKeyInNestedObject(resp, 'median_agg_deepest')[
    'median_agg_deepest'
  ].values['50.0'];
};

export const constructNestedCardinalityQuery = (
  name: string,
  path: string,
  field: string
) => {
  return {
    nested: {
      path: path,
    },
    aggs: {
      [name]: {
        terms: {
          field: field,
          size: environment.production ? 90e3 : 10,
        },
        aggs: {
          distinct_tokens: {
            cardinality: {
              field: field,
            },
          },
        },
      },
    },
  };
};

export const constructNestedFilter = (
  path: string,
  field: string,
  value: any,
  kind = 'term'
) => {
  const q_base: any = {
    path: path,
    query: {
      bool: {
        filter: [
          {
            [kind]: {
              [field]: {
                value: value,
              },
            },
          },
        ],
      },
    },
  };

  return q_base;
};

/**
 * Reuses `constructNestedLocationAssessmentQueryString` for all selected location peril
 * risk classes and appends `inject_same_level_query` if needed.
 * @param infoLevel
 * @param inject_same_level_query
 * @returns
 */
export const constructCombinedLocationPerilQuery = (
  infoLevel?,
  inject_same_level_query?: any
) => {
  return {
    bool: {
      should: ((window as any).selected_location_risk_classes || []).map(
        (perilRiskClass: number) => {
          const nestedQueryStringQuery =
            constructNestedLocationAssessmentQueryString(
              perilRiskClass,
              infoLevel
            );

          return {
            nested: {
              path: nestedQueryStringQuery.nested.path,
              query: {
                bool: {
                  must: [
                    ...nestedQueryStringQuery.nested.query.bool.must,
                    {
                      ...(inject_same_level_query
                        ? inject_same_level_query
                        : {
                          match_all: {},
                        }),
                    },
                  ],
                },
              },
            },
          };
        }
      ),
      minimum_should_match: 1,
    },
  };
};

export const constructNestedExistsQuery = (path: string, field: string) => {
  return {
    nested: {
      path: path,
      query: {
        bool: {
          filter: {
            exists: {
              field: field,
            },
          },
        },
      },
    },
  };
};

export const nestedBoolMustOfBoolShoulds = (
  path: string,
  field: string,
  vals: Array<string>
) => {
  return {
    bool: {
      should: vals.map((val) => {
        return {
          nested: constructNestedFilter(path, field, val),
        };
      }),
    },
  };
};

export const emptyBoolMustMatch = () => {
  return {
    bool: {
      must: {
        match_all: {},
      },
    },
  };
};

export const constructNestedRecommendationValueCountAgg = (
  key_factor_kind?: string,
  key_criterion?: string,
  only_top_hits = false,
  recommendation = null,
  include_top_hits = false // Added within UDGB-52 to manually recalculate # of recomms
) => {
  key_factor_kind = key_factor_kind || (window as any).selected_key_factor_kind;
  const location_peril = (window as any).selected_location_peril;

  if (!key_criterion) {
    if (only_top_hits) {
      return {
        nested: {
          path: `address.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteriaAsList`,
        },
        aggs: {
          inside: {
            meta: {
              assessment_recommendation: recommendation,
            },
            top_hits: {
              size: 1e3,
              _source: `address.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteriaAsList.recommendations`,
            },
          },
        },
      };
    }

    return {
      nested: {
        path: `address.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteriaAsList.recommendations`,
      },
      aggs: {
        inside: {
          value_count: {
            field: `address.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteriaAsList.recommendations.statusFeedback.keyword`,
          },
        },
        // Fix UDGB-52 by including inner hits and determining based on these
        // which recommendations should be summed up and which not
        ...(include_top_hits ? {
          inner_hits: {
            top_hits: {
              size: 1e3,
              _source: [
                `address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril}`,
              ],
            },
          },
        } : {})
      },
    };
  }

  if (only_top_hits) {
    return {
      nested: {
        path: `address.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteria.${key_criterion}`,
      },
      aggs: {
        inside: {
          meta: {
            assessment_recommendation: recommendation,
          },
          top_hits: {
            size: 1e3,
            _source: `address.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteria.${key_criterion}.recommendations`,
          },
        },
      },
    };
  }

  return {
    nested: {
      path: `address.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteria.${key_criterion}.recommendations`,
    },
    aggs: {
      inside: {
        value_count: {
          field: `address.locationProperty.locationAssessmentsLevel3.${location_peril}.keyFactors.${key_factor_kind}.keyCriteria.${key_criterion}.recommendations.statusFeedback.keyword`,
        }
      },
    },
  };
};

export const constructNestedRecommendationValueCountAggFactorAgnostic = (
  key_criterion?: string,
  only_top_hits = false,
  recommendation = null,
  include_top_hits = false // If true, fixes UDGB-52
) => {
  /**
   * using `reverse_nested` because we want to fit 3 separate nested aggs into one.
   */
  return {
    nested: {
      path: 'address',
    },
    aggs: {
      go_up_path: {
        reverse_nested: {},
        aggs: {
          probability: constructNestedRecommendationValueCountAgg(
            'probability',
            key_criterion,
            only_top_hits,
            recommendation,
            include_top_hits
          ),
          potential: constructNestedRecommendationValueCountAgg(
            'potential',
            key_criterion,
            only_top_hits,
            recommendation,
            include_top_hits
          ),
          prevention: constructNestedRecommendationValueCountAgg(
            'prevention',
            key_criterion,
            only_top_hits,
            recommendation,
            include_top_hits
          ),
        },
      },
    },
  };
};

export const constructNestedAssessmentTermQuery = (
  assessment_status: string
) => {
  const per_key_factor_kind_query = (key_factor_kind: string) => {
    return {
      nested: {
        path: `address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
          }.keyFactors.${key_factor_kind}.keyCriteriaAsList.recommendations`,
        query: {
          bool: {
            must: {
              term: {
                [`address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
                  }.keyFactors.${key_factor_kind}.keyCriteriaAsList.recommendations.statusFeedback.keyword`]:
                  assessment_status,
              },
            },
          },
        },
      },
    };
  };
  return {
    bool: {
      must: {
        bool: {
          should: (window as any).selected_key_factor_kind
            ? [
              per_key_factor_kind_query(
                (window as any).selected_key_factor_kind
              ),
            ]
            : [
              per_key_factor_kind_query('probability'),
              per_key_factor_kind_query('prevention'),
              per_key_factor_kind_query('potential'),
            ],
        },
      },
    },
  };
};

export const constructNested_CriteriaSpecific_RecommendationStatus_TermQuery = (
  key_factor,
  key_criterion,
  assessment_status
) => {
  return {
    nested: {
      path: `address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
        }.keyFactors.${key_factor}.keyCriteria.${key_criterion}.recommendations`,
      query: {
        bool: {
          must: {
            term: {
              [`address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
                }.keyFactors.${key_factor}.keyCriteria.${key_criterion}.recommendations.statusFeedback.keyword`]:
                assessment_status,
            },
          },
        },
      },
    },
  };
};

export const constructNested_CriteriaSpecific_RecommendationPriority_TermQuery =
  (key_factor, key_criterion, assessment_priority) => {
    return {
      nested: {
        path: `address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
          }.keyFactors.${key_factor}.keyCriteria.${key_criterion}.recommendations`,
        query: {
          bool: {
            must: {
              term: {
                [`address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
                  }.keyFactors.${key_factor}.keyCriteria.${key_criterion}.recommendations.priority.keyword`]:
                  assessment_priority,
              },
            },
          },
        },
      },
    };
  };

export const constructNestedAssessmentRecommendationTermQuery = (
  assessment_recommendation
) => {
  return {
    nested: {
      path: `address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
        }.keyFactors.${(window as any).selected_key_factor_kind
        }.keyCriteriaAsList.recommendations`,
      query: {
        bool: {
          must: {
            term: {
              [`address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
                }.keyFactors.${(window as any).selected_key_factor_kind
                }.keyCriteriaAsList.recommendations.name.keyword`]:
                assessment_recommendation,
            },
          },
        },
      },
    },
  };
};

export const mergeCriteriaSpecificAssessmentRecommendations =
  (): Array<string> => {
    interface RawGroup
      extends Array<{
        criterion: string;
        recommendations: Array<string>;
      }> { }

    const w = window as any;

    let raw_recommendations = {
      probability: flattenArrays(
        (<RawGroup>w.mappers.ProbabilityKeyCriterias_raw).map((group) => {
          return group.recommendations
            .map((recommendation) => {
              return `probability | ${group.criterion} | ${recommendation}`;
            })
            .sort();
        })
      ),
      potential: flattenArrays(
        (<RawGroup>w.mappers.PotentialKeyCriterias_raw).map((group) => {
          return group.recommendations
            .map((recommendation) => {
              return `potential | ${group.criterion} | ${recommendation}`;
            })
            .sort();
        })
      ),
      prevention: flattenArrays(
        (<RawGroup>w.mappers.PreventionKeyCriterias_raw).map((group) => {
          return group.recommendations
            .map((recommendation) => {
              return `prevention | ${group.criterion} | ${recommendation}`;
            })
            .sort();
        })
      ),
    };

    if (w.selected_key_factor_kind) {
      raw_recommendations = sortArrOfStringsAlphabetically(
        raw_recommendations[w.selected_key_factor_kind]
      );
    }

    return sortArrOfStringsAlphabetically(
      flattenArrays(Object.values(raw_recommendations))
    );
  };

export const mergeCriteriaSpecificNegativeAspects = (): Array<string> => {
  interface RawGroup
    extends Array<{
      criterion: string;
      negativeAspects: Array<string>;
    }> { }

  const w = window as any;

  let raw_recommendations = {
    probability: flattenArrays(
      (<RawGroup>w.mappers.ProbabilityKeyCriterias_raw).map((group) => {
        return group.negativeAspects
          .map((aspect) => {
            return `probability | ${group.criterion} | ${aspect}`;
          })
          .sort();
      })
    ),
    potential: flattenArrays(
      (<RawGroup>w.mappers.PotentialKeyCriterias_raw).map((group) => {
        return group.negativeAspects
          .map((aspect) => {
            return `potential | ${group.criterion} | ${aspect}`;
          })
          .sort();
      })
    ),
    prevention: flattenArrays(
      (<RawGroup>w.mappers.PreventionKeyCriterias_raw).map((group) => {
        return group.negativeAspects
          .map((aspect) => {
            return `prevention | ${group.criterion} | ${aspect}`;
          })
          .sort();
      })
    ),
  };

  if (w.selected_key_factor_kind) {
    raw_recommendations = sortArrOfStringsAlphabetically(
      raw_recommendations[w.selected_key_factor_kind]
    );
  }

  return sortArrOfStringsAlphabetically(
    flattenArrays(Object.values(raw_recommendations))
  );
};

export const constructNested_CriteriaSpecificAssessmentRecommendation_TermQuery =
  (key_factor, key_criterion, assessment_recommendation?) => {
    return {
      nested: {
        path: `address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
          }.keyFactors.${key_factor}.keyCriteria.${key_criterion}.recommendations`,
        query: {
          bool: {
            must: {
              term: {
                [`address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
                  }.keyFactors.${key_factor}.keyCriteria.${key_criterion}.recommendations.name.keyword`]:
                  assessment_recommendation,
              },
            },
          },
        },
      },
    };
  };

export const constructNestedAssessmentNegativeAspectRecommendationTermQuery = (
  assessment_recommendation
) => {
  return {
    nested: {
      path: `address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
        }.keyFactors.${(window as any).selected_key_factor_kind}.keyCriteria.${(window as any).selected_key_criterion
        }.negativeAspects.recommendations`,
      query: {
        bool: {
          must: {
            term: {
              [`address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
                }.keyFactors.${(window as any).selected_key_factor_kind
                }.keyCriteria.${(window as any).selected_key_criterion
                }.negativeAspects.recommendations.name.keyword`]:
                assessment_recommendation,
            },
          },
        },
      },
    },
  };
};

export const constructNestedAssessmentNegativeAspectTermQuery = (
  key_factor,
  key_criterion,
  assessment_aspect
) => {
  return {
    nested: {
      path: `address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
        }.keyFactors.${key_factor}.keyCriteria.${key_criterion}.negativeAspects.recommendations`,
      query: {
        bool: {
          must: {
            term: {
              [`address.locationProperty.locationAssessmentsLevel3.${(window as any).selected_location_peril
                }.keyFactors.${key_factor}.keyCriteria.${key_criterion}.negativeAspects.recommendations.negativeAspect.keyword`]:
                assessment_aspect,
            },
          },
        },
      },
    },
  };
};

export const constructNestedTermQuery = (
  path: string,
  field: string,
  value: any
) => {
  return {
    nested: {
      path: path,
      query: {
        term: {
          [field]: {
            value: value,
          },
        },
      },
    },
  };
};

export const constructNestedRangeQuery = (
  path: string,
  field: string,
  params: { gte?: number; lte?: number; gt?: number; lt?: number }
) => {
  // lt or lte could be +/- Infinity
  if (params.lt && !isFinite(params.lt)) {
    params.lt = null;
  }

  if (params.lte && !isFinite(params.lte)) {
    params.lte = null;
  }
  const cleanedParams = Object.keys(params)
    .filter((key) => typeof params[key] !== 'undefined')
    .reduce((obj, key) => {
      obj[key] = params[key];
      return obj;
    }, {});

  return {
    nested: {
      path: path,
      query: {
        range: {
          [field]: {
            ...cleanedParams,
          },
        },
      },
    },
  };
};

export const constructNestedWildcardQuery = (
  path: string,
  field: string,
  value: any
) => {
  return {
    nested: {
      path: path,
      query: {
        wildcard: {
          [field]: value,
        },
      },
    },
  };
};

export const constructScriptedMetricNestedCardinalityQuery = (
  name: string,
  path: string,
  field: string
) => {
  return {
    nested: {
      path: path,
    },
    aggs: {
      [name]: {
        scripted_metric: {
          init_script: 'state.map = [:];',
          map_script: `
              if (doc.containsKey('${path}.${field}')) {
                state.map[doc['${path}.${field}'].value.toString()] = 1;
              }
            `,
          combine_script: `
              def sum = 0;
              for (c in state.map.entrySet()) {
              sum += 1
              }
              return sum
            `,
          reduce_script: `
              def sum = 0;
              for (agg in states) {
                sum += agg;
              }
              return sum;
            `,
        },
      },
    },
  };
};

export const constructScriptedMetricNestedFieldValueMergerQuery = (
  name: string,
  path: string,
  field1: string,
  field2: string,
  dont_nest?
) => {
  if (dont_nest) {
    return {
      scripted_metric: {
        init_script: 'state.map = [:];',
        map_script: `
            def val_1 = doc['${field1}'].value;
            def val_2 = doc['${field2}'].value;

            if (val_1 == val_2) {
              if (state.map.containsKey(val_1)) {
                state.map[val_1] += 1;
              } else {
                state.map[val_1] = 1
              }
            } else {
              if (val_1 == 'N/A') {
                if (state.map.containsKey(val_2)) {
                  state.map[val_2] += 1;
                } else {
                  state.map[val_2] = 1
                }
              } else {
                if (state.map.containsKey(val_1)) {
                  state.map[val_1] += 1;
                } else {
                  state.map[val_1] = 1
                }
              }
            }
          `,
        combine_script: `
            return state.map;
          `,
        // we'll probably have only 1 shard so return just the first states element
        reduce_script: `
            return states[0];
          `,
      },
    };
  }

  return {
    nested: {
      path: path,
    },
    aggs: {
      [name]: {
        scripted_metric: {
          init_script: 'state.map = [:];',
          map_script: `
              def val_1 = doc['${path}.${field1}'].value;
              def val_2 = doc['${path}.${field2}'].value;

              if (val_1 == val_2) {
                if (state.map.containsKey(val_1)) {
                  state.map[val_1] += 1;
                } else {
                  state.map[val_1] = 1
                }
              } else {
                if (val_1 == 'N/A') {
                  if (state.map.containsKey(val_2)) {
                    state.map[val_2] += 1;
                  } else {
                    state.map[val_2] = 1
                  }
                } else {
                  if (state.map.containsKey(val_1)) {
                    state.map[val_1] += 1;
                  } else {
                    state.map[val_1] = 1
                  }
                }
              }
            `,
          combine_script: `
              return state.map;
            `,
          // we'll probably have only 1 shard so return just the first states element
          reduce_script: `
              return states[0];
            `,
        },
      },
    },
  };
};

export const extractDistinctTokensCountFromAgg = (agg: any, field: string) => {
  if (!agg[field].doc_count) {
    return 0;
  }
  const nested_object = agg[field][`${field}_by_id`];
  if (!nested_object.buckets) {
    return nested_object.value;
  }
  return nested_object.buckets
    .map((buck) => buck.distinct_tokens.value)
    .sumByReduce();
};

export const constructScriptedMetricRatioOfSumsAggregation = (
  id_field: string,
  nominator: string,
  denominator: string,
  as_percent?: boolean
): {
  scripted_metric: {
    init_script: string;
    map_script: string;
    combine_script: string;
    reduce_script: string;
  };
} => {
  return {
    scripted_metric: {
      init_script: 'state.map = [:];',
      map_script: `
          if (doc.containsKey('${nominator}') && doc.containsKey('${denominator}') && doc['${denominator}'].value > 0) {
            state.map[doc['${id_field}'].value.toString()] = [:];
            state.map[doc['${id_field}'].value.toString()]['nominator'] = doc['${nominator}'].value;
            state.map[doc['${id_field}'].value.toString()]['denominator'] = doc['${denominator}'].value;
          }
      `,
      combine_script: `
          def sum_nominator = 0.0;
          def sum_denominator = 0.0;

          for (c in state.map.values()) {
            sum_nominator += c.nominator;
            sum_denominator += c.denominator;

          }
          return (sum_nominator / sum_denominator) * ${as_percent ? 100 : 1};
      `,
      reduce_script: `
          def sum = 0.0;
          for (agg in states) {
            sum += agg;
          }
          return sum;
      `,
    },
  };
};

export const sharedWeeklyHistogramConfig = (app_component?) => {
  const defaults = {
    interval: 'week',
    format: 'dd.MM.yyyy',
    offset: '+0d',
    time_zone: '+02:00',
  };

  if (app_component && app_component['OfferCreatedDateDatepickerComponent']) {
    const { start, end } =
      app_component['OfferCreatedDateDatepickerComponent'].tsValues;
    if (!start || !end) {
      (window as any).dateRangeMagnitude = 'month';
      return defaults;
    }

    const start_moment = moment(start, 'YYYY/MM/DD HH:mm:ss');
    const end_moment = moment(end, 'YYYY/MM/DD HH:mm:ss');

    if (end_moment.diff(start_moment, 'days') <= 30) {
      (window as any).dateRangeMagnitude = 'day';
      return {
        ...defaults,
        interval: 'day',
      };
    }

    (window as any).dateRangeMagnitude = 'month';
    return defaults;
  }

  (window as any).dateRangeMagnitude = 'month';
  return defaults;
};

export const nestedMedianAggregation = (path: string, field: string) => {
  return {
    filter: emptyBoolMustMatch(),
    aggs: {
      median_agg_deepest: {
        nested: {
          path: path,
        },
        aggs: {
          median_agg_deepest: {
            percentiles: {
              field: field,
              percents: [50],
            },
          },
        },
      },
    },
  };
};

export const nestedStatsAggregation = (
  path: string,
  field: string,
  operation:
    | 'stats'
    | 'extended_stats'
    | 'min'
    | 'max'
    | 'avg'
    | 'sum' | 'cardinality' = 'stats',
  missing_value = 0
) => {
  return {
    filter: emptyBoolMustMatch(),
    aggs: {
      stats: {
        nested: {
          path: path,
        },
        aggs: {
          aggs: {
            [operation]: {
              field: field,
              missing: missing_value,
            },
          },
        },
      },
    },
  };
};

export const nestedTermsAggregation = (
  name: string,
  path: string,
  field: string,
  includeExclude = {}
) => {
  return {
    nested: {
      path,
    },
    aggs: {
      [name]: {
        terms: {
          field,
          ...includeExclude,
        },
      },
    },
  };
};

export const constructNestedDateRangeQuery = (
  path: string,
  field: string,
  params: {
    gte?: any;
    lte?: any;
    format?: string;
  }
) => {
  return {
    nested: {
      path,
      query: {
        range: {
          [field]: {
            ...params,
          },
        },
      },
    },
  };
};

export const constructSimulatedMultibucketBucketScript = (
  agg1: Object,
  agg2: Object,
  /*
    In the form of
    {
      agg_name: {
        bucket_script: {
          buckets_path: { ...}
  */
  bucket_script_parent: Object,
  /*
    Make sure we don't skip empty (filtered out) buckets
    but rather replace with 0's.
    Similar to `missing` in numeric aggregations,
    yet slightly different. More info at https://stackoverflow.com/q/63921905/8160318
  */
  gap_policy: 'skip' | 'insert_zeros' = 'insert_zeros'
) => {
  // apply the policies throughout all bucket scripts
  Object.keys(bucket_script_parent).map((okey) => {
    if (bucket_script_parent[okey].bucket_script) {
      bucket_script_parent[okey].bucket_script.gap_policy = gap_policy;
    }
  });

  return {
    multi_bucket_wrapper: {
      filters: {
        filters: {
          all: {
            match_all: {},
          },
        },
      },
      aggs: {
        ...agg1,
        ...agg2,
        ...bucket_script_parent,
      },
    },
  };
};

export const constructBucketScriptScriptForPercentualComparison = (
  now_bucket = 'params.now',
  before_bucket = 'params.before'
) => {
  return `return ((${now_bucket} - ${before_bucket})/${now_bucket}) * 100`;
};

interface RatioedBucketScriptConfig {
  filter: Object;
  insideAgg: Object;
  bucketPath: string;
}

export const constructRatioedBucketScript = ({
  nominator_config,
  denominator_config,
}: {
  nominator_config: RatioedBucketScriptConfig;
  denominator_config: RatioedBucketScriptConfig;
}) => {
  const nominator = {
    nominator: {
      filter: nominator_config.filter,
      aggs: {
        inside: nominator_config.insideAgg,
      },
    },
  };

  const denominator = {
    denominator: {
      filter: denominator_config.filter,
      aggs: {
        inside: denominator_config.insideAgg,
      },
    },
  };

  const ratio = {
    ratio: {
      bucket_script: {
        buckets_path: {
          nominator: `nominator>inside>${nominator_config.bucketPath}`,
          denominator: `denominator>inside>${denominator_config.bucketPath}`,
        },
        script: constructRatioedPercentualBucketScriptScript(
          'params.nominator',
          'params.denominator'
        ),
        format: '###.##',
      },
    },
  };

  return {
    multi_bucket_wrapper: {
      filters: {
        filters: {
          all: {
            match_all: {},
          },
        },
      },
      aggs: {
        ...nominator,
        ...denominator,
        ...ratio,
      },
    },
  };
};

// a simple percentual ratio
export const constructRatioedPercentualBucketScriptScript = (
  nominator: string,
  denominator: string
) => {
  return `return (${nominator}/${denominator}) * 100`;
};

