import { mergeDeep, arrayToObject, isString } from '../utils/helpers/helpers';
import * as Immutable from 'immutable';
import { HIGHLIGHTED_JSON_HITS_LIMIT } from '../text-card/constants';
import {
  constructMedianAgg,
  constructScriptedMetricNestedCardinalityQuery,
  constructNestedTermQuery,
  constructNestedWildcardQuery,
  constructScriptedMetricNestedFieldValueMergerQuery,
  constructNestedExistsQuery,
  sharedWeeklyHistogramConfig
} from './utils';
import { histogramQueryConstructors } from '../charts/histogram-table-chart/constants';
import { environment } from 'src/environments/environment';
import { AppComponent } from '../app.component';

export enum DATE_FORMATS {
  ES_DATE_HUMAN_READABLE = 'yyyy/MM/dd HH:mm:ss',
  MOMENT_DATE_HUMAN_READABLE = 'YYYY/MM/DD HH:mm:ss',
  HUMAN_READABLE = 'DD.MM.YYYY'
}

export enum ES_CLUSTERING_PRECISION {
  max = 12,
  min = 1
}

enum CHART_TYPES {
  piechart = 'piechart',
  vertical_bar_chart = 'vertical_bar_chart',
  horizontal_bar_chart = 'horizontal_bar_chart',
  bar_and_linechart = 'bar_and_linechart',
  line_chart = 'line_chart',
  text_chart = 'text_chart',
  table = 'table',
  histogram_table_chart = 'histogram_table_chart'
}

export interface IdatePickerFieldConfig {
  nested: boolean;
  accessor: string;
  path: string;
  field: string;
}

export const DATEPICKERS_TO_FIELDS = Immutable.fromJS({
  OfferCreatedDateDatepicker: {
    nested: true,
    accessor: 'offer.createdDate',
    path: 'offer',
    field: 'offer.createdDate'
  },
  OfferIssuanceDateDatepicker: {
    nested: true,
    accessor: 'offer.issuanceDate',
    path: 'offer',
    field: 'offer.issuanceDate'
  },
  PolicyIssuanceDateDatepicker: {
    nested: true,
    accessor: 'lob.policyIssuance',
    path: 'lob',
    field: 'lob.policyIssuance'
  },
  IsActiveDateDatepicker: {
    lte: {
      nested: true,
      accessor: 'lob.beginDate',
      path: 'lob',
      field: 'lob.beginDate'
    },
    gte: {
      nested: true,
      accessor: 'lob.endDate',
      path: 'lob',
      field: 'lob.endDate'
    }
  },
  InceptionDateDatepicker: {
    nested: true,
    accessor: 'lob.beginDate',
    path: 'lob',
    field: 'lob.beginDate'
  },
  IsExpiringDateDatepicker: {
    nested: true,
    accessor: 'lob.endDate',
    path: 'lob',
    field: 'lob.endDate'
  }
}).toJSON() as {
  [key: string]:
  IdatePickerFieldConfig
  & {
    gte: IdatePickerFieldConfig,
    lte: IdatePickerFieldConfig
  }
};

export enum MAP_KLASSES {
  OfferDistribution = 'OfferDistributionHeatmap',
  ClientDistribution = 'ClientDistributionHeatmap',
  ExperimentalHeatmap = 'ExperimentalHeatmap',
  'ClientDistributionHeatmap' = 'ClientDistributionHeatmap',
  'heatmap' = 'heatmap',
  'clustermap' = 'clustermap',
  OfferHotspotMap = 'OfferHotspotMap'
}

export enum VISUALIZATIONS {
  HEATMAP = MAP_KLASSES.heatmap,
  CLUSTERMAP = MAP_KLASSES.clustermap,
  PIECHART = CHART_TYPES.piechart,
  BAR_AND_LINECHART = CHART_TYPES.bar_and_linechart,
  VERTICAL_BAR_CHART = CHART_TYPES.vertical_bar_chart,
  HORIZONTAL_BAR_CHART = CHART_TYPES.horizontal_bar_chart,
  TEXT_CHART = CHART_TYPES.text_chart,
  LINE_CHART = CHART_TYPES.line_chart,
  TABLE = CHART_TYPES.table,
  HISTOGRAM_TABLE = CHART_TYPES.histogram_table_chart
}

export enum PRECISION_RATIOS {
  'heatmap' = -2,
  'ClientDistributionHeatmap' = -2,
  'OfferDistributionHeatmap' = -2,
  'ExperimentalHeatmap' = -2,
  'OfferHotspotMap' = -2,
  'clustermap' = 4
}

export const DATE_FIELD_QUERIES = Immutable.fromJS([
  {
    nested: {
      path: DATEPICKERS_TO_FIELDS.OfferCreatedDateDatepicker.path,
      query: {
        range: {
          [DATEPICKERS_TO_FIELDS.OfferCreatedDateDatepicker.field]: {
            accessor: [DATEPICKERS_TO_FIELDS.OfferCreatedDateDatepicker.field],
            gte: null,
            lte: null,
            format: DATE_FORMATS.ES_DATE_HUMAN_READABLE
          }
        }
      }
    }
  },
  {
    nested: {
      path: DATEPICKERS_TO_FIELDS.OfferIssuanceDateDatepicker.path,
      query: {
        range: {
          [DATEPICKERS_TO_FIELDS.OfferIssuanceDateDatepicker.field]: {
            accessor: [DATEPICKERS_TO_FIELDS.OfferIssuanceDateDatepicker.field],
            gte: null,
            lte: null,
            format: DATE_FORMATS.ES_DATE_HUMAN_READABLE
          }
        }
      }
    }
  },
  {
    nested: {
      path: DATEPICKERS_TO_FIELDS.PolicyIssuanceDateDatepicker.path,
      query: {
        range: {
          [DATEPICKERS_TO_FIELDS.PolicyIssuanceDateDatepicker.field]: {
            accessor: [
              DATEPICKERS_TO_FIELDS.PolicyIssuanceDateDatepicker.field
            ],
            gte: null,
            lte: null,
            format: DATE_FORMATS.ES_DATE_HUMAN_READABLE
          }
        }
      }
    }
  },
  {
    bool: {
      filter: [
        {
          nested: {
            path: DATEPICKERS_TO_FIELDS.IsActiveDateDatepicker.gte.path,
            query: {
              range: {
                [DATEPICKERS_TO_FIELDS.IsActiveDateDatepicker.gte.field]: {
                  accessor: [
                    DATEPICKERS_TO_FIELDS.IsActiveDateDatepicker.gte.field
                  ],
                  gte: null,

                  format: DATE_FORMATS.ES_DATE_HUMAN_READABLE
                }
              }
            }
          }
        },
        {
          nested: {
            path: DATEPICKERS_TO_FIELDS.IsActiveDateDatepicker.lte.path,
            query: {
              range: {
                [DATEPICKERS_TO_FIELDS.IsActiveDateDatepicker.lte.field]: {
                  accessor: [
                    DATEPICKERS_TO_FIELDS.IsActiveDateDatepicker.lte.field
                  ],
                  lte: null,
                  format: DATE_FORMATS.ES_DATE_HUMAN_READABLE
                }
              }
            }
          }
        },
        {
          nested: {
            path: DATEPICKERS_TO_FIELDS.InceptionDateDatepicker.path,
            query: {
              range: {
                [DATEPICKERS_TO_FIELDS.InceptionDateDatepicker.field]: {
                  accessor: [
                    DATEPICKERS_TO_FIELDS.InceptionDateDatepicker.field
                  ],
                  gte: null,
                  lte: null,
                  format: DATE_FORMATS.ES_DATE_HUMAN_READABLE
                }
              }
            }
          }
        },
        {
          nested: {
            path: DATEPICKERS_TO_FIELDS.IsExpiringDateDatepicker.path,
            query: {
              range: {
                [DATEPICKERS_TO_FIELDS.IsExpiringDateDatepicker.field]: {
                  accessor: [
                    DATEPICKERS_TO_FIELDS.IsExpiringDateDatepicker.field
                  ],
                  gte: null,
                  lte: null,
                  format: DATE_FORMATS.ES_DATE_HUMAN_READABLE
                }
              }
            }
          }
        },
      ]
    }
  }
]).toJSON();

export const BASE_AGG_QUERY_CLAUSE = () =>
  Immutable.fromJS({
    size: 0,
    query: {
      bool: {
        must: [],
        should: [],
        must_not: [],
        filter: DATE_FIELD_QUERIES
      }
    }
  }).toJSON();

export const VISUALIZATION_TYPES = {
  OfferDistributionHeatmap: VISUALIZATIONS.HEATMAP,
  ClientDistributionHeatmap: VISUALIZATIONS.HEATMAP,
  ExperimentalHeatmap: VISUALIZATIONS.HEATMAP,
  OfferHotspotMap: VISUALIZATIONS.HEATMAP,
  OfferDistributionClustermap: VISUALIZATIONS.CLUSTERMAP,
  TotalNoOfOffersPerLoB: VISUALIZATIONS.PIECHART,
  TotalPremiumPerLoB: VISUALIZATIONS.PIECHART,
  TotalNoOfOffersPerLoBnormalizedVerticalBarChart:
    VISUALIZATIONS.VERTICAL_BAR_CHART,
  OffersAggregatedByStatus: VISUALIZATIONS.VERTICAL_BAR_CHART,
  OffersAggregatedByDeclinationReasons: VISUALIZATIONS.VERTICAL_BAR_CHART,
  OffersAggregatedByLineOfBusiness: VISUALIZATIONS.VERTICAL_BAR_CHART,
  OffersAggregatedByLineOfBusinessVsPremium: VISUALIZATIONS.VERTICAL_BAR_CHART,
  AcceptedOffersAggregatedByLineOfBusiness: VISUALIZATIONS.VERTICAL_BAR_CHART,
  DeclinedOffersAggregatedByLineOfBusiness: VISUALIZATIONS.VERTICAL_BAR_CHART,
  OffersAggregatedByRisks: VISUALIZATIONS.VERTICAL_BAR_CHART,
  OffersAggregatedByPerils: VISUALIZATIONS.HORIZONTAL_BAR_CHART,

  OffersSuccessRatioAggregatedByDates: VISUALIZATIONS.LINE_CHART,
  OffersSuccessRatiovsLobsCountAggregatedByDates: VISUALIZATIONS.LINE_CHART,
  OffersSuccessRatioVsPremiumsAggregatedByDates: VISUALIZATIONS.LINE_CHART,

  TotalPremiumPerMonthVerticalBarChart: VISUALIZATIONS.VERTICAL_BAR_CHART,
  NumberOfOffersPerLobPerRiskClassStackedVerticalBarChart:
    VISUALIZATIONS.VERTICAL_BAR_CHART,
  NumberOfOffersPerLobPerRiskClassConcludedStackedVerticalBarChart:
    VISUALIZATIONS.VERTICAL_BAR_CHART,
  AllOffers: VISUALIZATIONS.TEXT_CHART,
  ResponsibleLocalUnderwriter: VISUALIZATIONS.TEXT_CHART,
  MostRepresentedBusinessPartners: VISUALIZATIONS.VERTICAL_BAR_CHART,
  MostRepresentedBusinessPartnersVsPremium: VISUALIZATIONS.VERTICAL_BAR_CHART,
  MostRepresentedDistributionChannels: VISUALIZATIONS.VERTICAL_BAR_CHART,
  UnderwriterResponseTimes: VISUALIZATIONS.VERTICAL_BAR_CHART,
  MinMaxAvgTotalPremiumTextChart: VISUALIZATIONS.TEXT_CHART,
  MinMaxAvgUniqaSharePremiumTextChart: VISUALIZATIONS.TEXT_CHART,
  MinMaxAvgTotalSumInsuredTextChart: VISUALIZATIONS.TEXT_CHART,
  MinMaxOfferUpdatedTextChart: VISUALIZATIONS.TEXT_CHART,
  MinMaxOfferCreatedTextChart: VISUALIZATIONS.TEXT_CHART,
  TableExploration: VISUALIZATIONS.TABLE,

  PolicyIssuedPremiumRatiosByTSI: VISUALIZATIONS.LINE_CHART,
  PolicyIssuedPremiumRatiosVsTime: VISUALIZATIONS.LINE_CHART,

  AggregatedSI_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedPremium_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,
  PremiumVsSIvsLoBScatterPlot: VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedSuperScorePremiums: VISUALIZATIONS.HISTOGRAM_TABLE,

  ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndRiskCode_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndNaceCode_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndOccupancy_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  ESG_AggregatedEmissionsPerOccupancy_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,
  ESG_AggregatedEmissionsPerNaceCode_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedTotalPremiumPerIndustry_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedTotalSumInsuredPerIndustry_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedPremiumsVsTotalSumInsuredPerIndustry_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedLOBCountPerIndustry_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedLocationsCountPerIndustry_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  PPM_GlobalStatsPerBusinessActivity_NoCanvas_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedTotalPremiumPerBusinessActivity_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedTotalOffersPerBusinessActivity_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedTotalSumInsuredPerBusinessActivity_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedLocationsPerBusinessActivity_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedPremiumsVsTotalSumInsuredPerBusinessActivity_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  PPM_GlobalStatsPerNaceCode_NoCanvas_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedTotalPremiumPerNaceCodes_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedTotalOffersPerNaceCodes_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedTotalSumInsuredPerNaceCodes_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedLocationsPerNaceCodes_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedTurnoverVsPremium_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedTurnoverVsTSI_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedTurnoverVsCount_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,

  GrossRiskProfile_colored_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedTotalPremiumPerRegionOfOrigin_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedLOBCountPerRegionOfOrigin_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedTotalSumInsuredPerRegionOfOrigin_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedLocationsCountPerRegionOfOrigin_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedTotalPremiumPerSalesPartner_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedLOBCountPerSalesPartner_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedTotalSumInsuredPerSalesPartner_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedLocationsCountPerSalesPartner_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedTotalPremiumPerAvgRiskClassPerPeril_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedTotalSumInsuredPerAvgRiskClassPerPeril_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedLOBCountPerAvgRiskClassPerPeril_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedGroupedPremiumPerAvgRiskClassPerPeril_LoBBased_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedGroupedPremiumPerAvgRiskClassPerPeril_location_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedAvgPDAPerAvgRiskClassPerPeril_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAvgPDAPerTSIPerPeril_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedBrokerCommission_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedTotalMPremiumPerPeril_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedTotalTPremiumPerPeril_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedRiskScorePerAvgRiskClassPerPeril_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedRiskScoreVsPeril_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedInfoLevelVsRiskClassVsPeril_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedSuperscoresPerLocationsPerIndustry_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedSuperscoresPerRequestFromCountry_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedSuperscoresPerBucketedTSI_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedTSIPerInsuredObject_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedTSIPerConstructionMaterials_location_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  // RISK ENGINEERING BEGIN
  AggregatedAddressesVsIndustryCategory_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedAddressesPerIL_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerScore_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerPML_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerRC_histogram_chart: VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedAddressesPerRC_vsPML_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerRC_vsIL_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerRC_vsScore_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerRC_vsRecommendations_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedAddressesPerIC_vsRecommendations_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerIC_vsPML_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerIC_vsIL_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerIC_vsScore_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedAddressesPerScorePerPML_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerAsIfScorePerPML_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedAddressesPerAsIfScorePerPML_EML_Ratio_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedAddressesPerRiskScore_vsPremiums_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerRiskScore_vsPremiums_IndividualLocations_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedAddressesPerRecommendationKinds_vsPriority_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerRecommendationKinds_vsRecommendations_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerRecommendationKinds_vsLocationCounts_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerRecommendationKinds_vsNegativeCondition_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,

  AggregatedAddressesPerEvaluations_vsProbabilityCriterias_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerEvaluations_vsPotentialCriterias_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  AggregatedAddressesPerEvaluations_vsPreventionCriterias_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  // RISK ENGINEERING END
  // RE INSURANCE BEGIN
  RIM_Property_AggregatedStatsByPML_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  RIM_Property_AggregatedStatsByPMLminusTimeRange_AbsoluteValues_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  RIM_Property_AggregatedStatsByPMLminusTimeRange_PercentualValues_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  RIM_Property_AggregatedStatsByTSI_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  RIM_Property_AggregatedStatsByTSIminusTimeRange_AbsoluteValues_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE,
  RIM_Property_AggregatedStatsByTSIminusTimeRange_PercentualValues_address_based_histogram_chart:
    VISUALIZATIONS.HISTOGRAM_TABLE
};

export const GEO_FIELDS = {
  offers: {
    path: 'address',
    field: 'address.coordinates'
  },
  clients: {
    path: 'offer.client',
    field: 'offer.client.coordinates'
  }
};

export const HEATMAP_VS_GEO_FIELD_SOURCES = {
  [MAP_KLASSES.OfferDistribution]: GEO_FIELDS.offers,
  [MAP_KLASSES.ClientDistribution]: GEO_FIELDS.clients
};

export const GEO_FIELD_NESTED_PATH = 'address';
export const GEO_FIELD_TO_AGGREGATE = 'address.coordinates';

const policyHasBeenIssuedAtSomePoint = [
  constructNestedTermQuery('lob', 'lob.status', 'POLICY_ISSUED'),
  constructNestedTermQuery(
    'lob',
    'lob.status',
    'PROPOSAL_SUBMITTED_TO_CLIENT_SALES_PARTNER'
  ),

  constructNestedTermQuery(
    'offer.internalInfo',
    'offer.internalInfo.state',
    'POLICY_ISSUED'
  ),
  constructNestedTermQuery(
    'offer.internalInfo',
    'offer.internalInfo.state',
    'PROPOSAL_SUBMITTED_TO_CLIENT_SALES_PARTNER'
  )
];

const policyHasBeenDeclinedAtSomePoint = [
  constructNestedWildcardQuery('lob', 'lob.status', 'DECLINED*'),
  constructNestedWildcardQuery(
    'offer.internalInfo',
    'offer.internalInfo.state',
    'DECLINED*'
  )
];

export const isQuerySkippable = (q: String) =>
  q.includes('_histogram_chart') && !q.includes('colored');

export const VIEW_VS_QUERIES_ON_LOAD = {
  efficiency_user_management: [
    'AllOffers',

    'GrossRiskProfile_colored_histogram_chart',

    'MinMaxAvgTotalPremiumTextChart',
    'MinMaxAvgUniqaSharePremiumTextChart',
    'MinMaxAvgTotalSumInsuredTextChart',
    'MinMaxOfferUpdatedTextChart',
    'MinMaxOfferCreatedTextChart',
    'PremiumVsSIvsLoBScatterPlot',

    'AcceptedOffersAggregatedByLineOfBusiness',
    'DeclinedOffersAggregatedByLineOfBusiness',

    'MostRepresentedBusinessPartners',
    'MostRepresentedDistributionChannels',
    'MostRepresentedBusinessPartnersVsPremium',

    'OffersAggregatedByDeclinationReasons',
    'OffersAggregatedByLineOfBusiness',
    'OffersAggregatedByLineOfBusinessVsPremium',
    'OffersAggregatedByPerils',
    'OffersAggregatedByRisks',
    'OffersAggregatedByStatus',

    'OffersSuccessRatioAggregatedByDates',
    'OffersSuccessRatiovsLobsCountAggregatedByDates',
    'OffersSuccessRatioVsPremiumsAggregatedByDates',

    'PolicyIssuedPremiumRatiosByTSI',
    'PolicyIssuedPremiumRatiosVsTime'
  ],
  environmental_social_governance: [
    'AllOffers',
    'MinMaxOfferUpdatedTextChart',
    'MinMaxOfferCreatedTextChart'
  ],
  product_portfolio_management: [
    'AllOffers',
    'MinMaxOfferUpdatedTextChart',
    'MinMaxOfferCreatedTextChart'
  ],
  maps_and_positions: [
    'AllOffers',
    'MinMaxOfferUpdatedTextChart',
    'MinMaxOfferCreatedTextChart'
  ],
  re_insurance_management: [
    'AllOffers',
    'MinMaxOfferUpdatedTextChart',
    'MinMaxOfferCreatedTextChart',
    'MinMaxAvgTotalPremiumTextChart',
    'MinMaxAvgUniqaSharePremiumTextChart',
    'MinMaxAvgTotalSumInsuredTextChart'
  ],
  risk_engineering_management: [
    'AllOffers',
    'MinMaxOfferUpdatedTextChart',
    'MinMaxOfferCreatedTextChart'
  ],

  // a mix of EUM & PPM
  sales_efficiency_management: [
    'AllOffers',

    'GrossRiskProfile_colored_histogram_chart',

    'MinMaxAvgTotalPremiumTextChart',
    'MinMaxAvgUniqaSharePremiumTextChart',
    'MinMaxAvgTotalSumInsuredTextChart',
    'MinMaxOfferUpdatedTextChart',
    'MinMaxOfferCreatedTextChart',
    'PremiumVsSIvsLoBScatterPlot',

    'OffersAggregatedByLineOfBusiness',
    'OffersAggregatedByLineOfBusinessVsPremium',

    'OffersAggregatedByStatus',

    'AcceptedOffersAggregatedByLineOfBusiness',
    'DeclinedOffersAggregatedByLineOfBusiness',
    'OffersAggregatedByDeclinationReasons',

    'OffersAggregatedByPerils',
    'OffersAggregatedByRisks',

    'OffersSuccessRatioAggregatedByDates',
    'OffersSuccessRatiovsLobsCountAggregatedByDates',
    'OffersSuccessRatioVsPremiumsAggregatedByDates',

    'MostRepresentedBusinessPartners',
    'MostRepresentedBusinessPartnersVsPremium',

    'MostRepresentedDistributionChannels' // idea for the future the same graph/table but with respect of the premium volume

    // ... the rest is PPM
  ]
};

export function buildQueries(specific_component?: string | Array<string> | any, app_component?) {
  const replacement_re = /_?Component/g;

  const qs = {
    OfferDistributionHeatmap: () => {
      return {
        size: 0, // if 0, just aggregate
        aggs: {
          filter_agg: {
            nested: {
              path: GEO_FIELDS.offers.path
            },
            aggs: {
              aggs: {
                filter: {
                  nested: {
                    path: GEO_FIELDS.offers.path,
                    query: {
                      geo_bounding_box: {
                        [GEO_FIELDS.offers.field]: {}
                      }
                    }
                  }
                },
                aggs: {
                  distribution: {
                    geohash_grid: {
                      field: GEO_FIELDS.offers.field,
                      precision: 4
                    },
                    aggs: {
                      centroid: {
                        geo_centroid: {
                          field: GEO_FIELDS.offers.field
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        },
        query: {
          bool: {
            must: [
              {
                nested: {
                  path: GEO_FIELDS.offers.path,
                  query: {
                    exists: {
                      field: GEO_FIELDS.offers.field
                    }
                  }
                }
              }
            ],
            should: [],
            filter: DATE_FIELD_QUERIES
          }
        },
        _source: {
          includes: [
            'id',
            'offer.id',
            'offer.number',
            'lob.id',
            'lob.lineOfBusiness',
            'address.i*',
            'address.c*',
            'address.s*',
            'address.t*',
            'offer.client.name',
            'offer.industry',
            'offer.created*',
            'offer.updated*',
            'offer.regionalDirectorate', // region of origin
            'lob.share',
            'lob.premium*',
            'lob.totalSumInsured*',
            'address.locationProperty.locationAssessments.peril',
            'address.locationProperty.locationAssessments.riskClass',
            // for peril limit hotspots
            'lob.rating.ratingToolPerils.*.limitValue_$CURRENCY$_AdjustedForShare'
          ],
          excludes: ['address.geoPoint', 'address.ratingValues']
        }
      };
    },
    ClientDistributionHeatmap: () => {
      return {
        size: 0, // if 0, just aggregate
        aggs: {
          filter_agg: {
            nested: {
              path: GEO_FIELDS.clients.path
            },
            aggs: {
              aggs: {
                filter: {
                  nested: {
                    path: GEO_FIELDS.clients.path,
                    query: {
                      geo_bounding_box: {
                        [GEO_FIELDS.clients.field]: {}
                      }
                    }
                  }
                },
                aggs: {
                  distribution: {
                    geohash_grid: {
                      field: GEO_FIELDS.clients.field,
                      precision: 4
                    },
                    aggs: {
                      centroid: {
                        geo_centroid: {
                          field: GEO_FIELDS.clients.field
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        },
        query: {
          bool: {
            must: [
              {
                nested: {
                  path: GEO_FIELDS.clients.path,
                  query: {
                    exists: {
                      field: GEO_FIELDS.clients.field
                    }
                  }
                }
              }
            ],
            should: [],
            filter: DATE_FIELD_QUERIES
          }
        },
        _source: {
          includes: [
            'id',
            'offer.id',
            'offer.number',
            'offer.client',
            'offer.industry',
            'lob.id',
            'lob.lineOfBusiness',
            'lob.share',
            'lob.premium*',
            'lob.totalSumInsured*'
          ],
          excludes: ['lob.addresses']
        }
      };
    },
    ExperimentalHeatmap: () => {
      return {
        size: 0, // if 0, just aggregate
        aggs: {
          filter_agg: {
            nested: {
              path: GEO_FIELDS.clients.path
            },
            aggs: {
              aggs: {
                filter: {
                  nested: {
                    path: GEO_FIELDS.clients.path,
                    query: {
                      geo_bounding_box: {
                        [GEO_FIELDS.clients.field]: {}
                      }
                    }
                  }
                },
                aggs: {
                  distribution: {
                    geohash_grid: {
                      field: GEO_FIELDS.clients.field,
                      precision: 4
                    },
                    aggs: {
                      centroid: {
                        geo_centroid: {
                          field: GEO_FIELDS.clients.field
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        },
        query: {
          bool: {
            must: [
              {
                nested: {
                  path: GEO_FIELDS.clients.path,
                  query: {
                    exists: {
                      field: GEO_FIELDS.clients.field
                    }
                  }
                }
              }
            ],
            should: [],
            filter: DATE_FIELD_QUERIES
          }
        },
        _source: {
          includes: [
            'id',
            'offer.id',
            'offer.number',
            'offer.client',
            'offer.industry',
            'lob.id',
            'lob.lineOfBusiness',
            'lob.share',
            'lob.premium*',
            'lob.totalSumInsured*'
          ],
          excludes: ['lob.addresses']
        }
      };
    },
    OfferHotspotMap: () => {
      return {
        size: 10e3,
        // empty aggs b/c we won't need them -- we're only fetching via `_source`
        aggs: {},
        query: {
          bool: {
            must: [
              {
                nested: {
                  path: GEO_FIELDS.offers.path,
                  query: {
                    exists: {
                      field: GEO_FIELDS.offers.field
                    }
                  }
                }
              }
            ],
            should: [],
            filter: DATE_FIELD_QUERIES
          }
        },
        sort: {
          'address.tsiPerLocation_$CURRENCY$_AdjustedForShare': {
            order: 'desc',
            nested_path: 'address'
          }
        },
        _source: {
          includes: [
            'id',
            'offer.id',
            'offer.number',
            'lob.id',
            'lob.lineOfBusiness',
            'address.i*',
            'address.c*',
            'address.s*',
            'address.t*',
            'offer.client.name',
            'offer.industry',
            'lob.share',
            'lob.premium*',
            'lob.totalSumInsured*',
            'address.locationProperty.locationAssessments.peril',
            'address.locationProperty.locationAssessments.riskClass',
            // for peril limit hotspots
            'lob.rating.ratingToolPerils.*.limitValue_$CURRENCY$_AdjustedForShare'
          ],
          excludes: ['address.geoPoint', 'address.ratingValues']
        }
      };
    },
    OfferDistributionClustermap: () => {
      return {
        size: 0,
        aggs: {
          filter_agg: {
            filter: {
              // geo_bounding_box: {}
            },
            aggs: {
              distribution: {
                geohash_grid: {
                  field: GEO_FIELD_TO_AGGREGATE,
                  precision: 4
                },

                aggs: {
                  centroid: {
                    geo_centroid: {
                      field: GEO_FIELD_TO_AGGREGATE
                    }
                  },
                  avg_price: {
                    avg: {
                      field: 'Premium'
                    }
                  }
                }
              }
            }
          }
        },
        query: {
          bool: {
            must: [],
            should: [],
            filter: DATE_FIELD_QUERIES
          }
        }
      };
    },
    TotalNoOfOffersPerLoB: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          filter_agg: {
            terms: {
              field: 'Line of Business (LoB)',
              size: 30,
              order: {
                _term: 'asc'
              }
            }
          }
        }
      }),
    TotalPremiumPerLoB: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          filter_agg: {
            terms: {
              field: 'Line of Business (LoB)',
              size: 30,
              order: {
                _term: 'asc'
              }
            },
            aggs: {
              total_premium: {
                sum: {
                  field: 'Premium'
                }
              }
            }
          }
        }
      }),
    TotalNoOfOffersPerLoBnormalizedVerticalBarChart: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          filter_agg: {
            terms: {
              field: 'Line of Business (LoB)',
              size: 20,
              order: {
                _term: 'asc'
              }
            },
            aggs: {
              status: {
                terms: {
                  field: 'Status',
                  size: 5,
                  order: {
                    _term: 'asc'
                  }
                }
              }
            }
          }
        },
        query: {
          bool: {
            should: [
              ...policyHasBeenIssuedAtSomePoint,
              ...policyHasBeenDeclinedAtSomePoint
            ],
            filter: DATE_FIELD_QUERIES
          }
        }
      }),
    OffersAggregatedByStatus: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          filter_agg: {
            nested: {
              path: 'lob'
            },
            aggs: {
              filter_agg: {
                terms: {
                  field: 'lob.status',
                  size: 100,
                  order: {
                    _term: 'asc'
                  }
                }
              }
            }
          }
        }
      }),
    OffersAggregatedByDeclinationReasons: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        query: {
          bool: {
            must: [
              {
                bool: {
                  should: policyHasBeenDeclinedAtSomePoint
                }
              }
            ]
          }
        },
        aggs: {
          declination_reasons: constructScriptedMetricNestedFieldValueMergerQuery(
            'declination_reasons_combined',
            'lob',
            'reasonForDeclining',
            'reasonForDecline'
          )
        }
      }),
    OffersAggregatedByLineOfBusiness: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          total_1: {
            nested: {
              path: 'lob'
            },
            aggs: {
              total_2: {
                filter: {
                  exists: {
                    field: 'lob.id'
                  }
                },
                aggs: {
                  total_deepest: {
                    terms: {
                      field: 'lob.lineOfBusiness',
                      size: 100,
                      order: {
                        _term: 'asc'
                      }
                    }
                  }
                }
              }
            }
          },
          concluded_1: {
            filter: {
              bool: {
                must: [
                  {
                    nested: {
                      path: 'lob',
                      query: {
                        term: {
                          'lob.status': 'POLICY_ISSUED'
                        }
                      }
                    }
                  }
                ]
              }
            },
            aggs: {
              concluded_deepest_1: {
                nested: {
                  path: 'lob'
                },
                aggs: {
                  concluded_deepest: {
                    terms: {
                      field: 'lob.lineOfBusiness',
                      size: 100,
                      order: {
                        _term: 'asc'
                      }
                    }
                  }
                }
              }
            }
          },
          unfiltered_total: {
            nested: {
              path: 'lob'
            },
            aggs: {
              unfiltered_deepest: {
                terms: {
                  field: 'lob.lineOfBusiness',
                  size: 100,
                  order: {
                    _term: 'asc'
                  }
                }
              }
            }
          }
        }
      }),

    OffersAggregatedByLineOfBusinessVsPremium: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          total_1: {
            nested: {
              path: 'lob'
            },
            aggs: {
              total_2: {
                filter: {
                  exists: {
                    field: 'lob.id'
                  }
                },
                aggs: {
                  total_deepest: {
                    terms: {
                      field: 'lob.lineOfBusiness',
                      size: 100,
                      order: {
                        _term: 'asc'
                      }
                    },
                    aggs: {
                      total_deepest: {
                        stats: {
                          field: 'lob.premium_$CURRENCY$'
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          concluded_1: {
            filter: {
              bool: {
                must: [
                  {
                    nested: {
                      path: 'lob',
                      query: {
                        term: {
                          'lob.status': 'POLICY_ISSUED'
                        }
                      }
                    }
                  }
                ]
              }
            },
            aggs: {
              concluded_deepest_1: {
                nested: {
                  path: 'lob'
                },
                aggs: {
                  concluded_deepest: {
                    terms: {
                      field: 'lob.lineOfBusiness',
                      size: 100,
                      order: {
                        _term: 'asc'
                      }
                    },
                    aggs: {
                      concluded_deepest: {
                        stats: {
                          field: 'lob.premium_$CURRENCY$'
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          unfiltered_total: {
            nested: {
              path: 'lob'
            },
            aggs: {
              unfiltered_deepest: {
                stats: {
                  field: 'lob.premium_$CURRENCY$'
                }
              }
            }
          }
        }
      }),
    AcceptedOffersAggregatedByLineOfBusiness: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        query: {
          bool: {
            must: [
              {
                bool: {
                  should: policyHasBeenIssuedAtSomePoint
                }
              }
            ]
          }
        },
        aggs: {
          filter_agg: {
            nested: {
              path: 'lob'
            },
            aggs: {
              filter_agg: {
                terms: {
                  field: 'lob.lineOfBusiness',
                  size: 100,
                  order: {
                    _term: 'asc'
                  }
                }
              }
            }
          }
        }
      }),
    DeclinedOffersAggregatedByLineOfBusiness: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        query: {
          bool: {
            must: [
              {
                bool: {
                  should: policyHasBeenDeclinedAtSomePoint
                }
              }
            ]
          }
        },
        aggs: {
          filter_agg: {
            nested: {
              path: 'lob'
            },
            aggs: {
              filter_agg: {
                terms: {
                  field: 'lob.lineOfBusiness',
                  size: 100,
                  order: {
                    _term: 'asc'
                  }
                },
                aggs: {
                  specifics: constructScriptedMetricNestedFieldValueMergerQuery(
                    'specifics_combined',
                    'lob',
                    'lob.reasonForDeclining',
                    'lob.reasonForDecline',
                    true
                  )
                }
              }
            }
          }
        }
      }),
    OffersAggregatedByRisks: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          filter_agg: {
            nested: {
              path: 'lob.risks'
            },
            aggs: {
              filter_agg: {
                terms: {
                  field: 'lob.risks.risk',
                  size: 1000,
                  order: {
                    _term: 'asc'
                  }
                }
              }
            }
          }
        }
      }),
    OffersAggregatedByPerils: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        // TODO Construct scripted metric agg to count LoBs not lob.risks
        aggs: {
          filter_agg: {
            nested: {
              path: 'lob.risks.perils'
            },
            aggs: {
              filter_agg: {
                terms: {
                  field: 'lob.risks.perils.riskDescription',
                  size: 1000,
                  order: {
                    _count: 'desc'
                  }
                }
              }
            }
          }
        }
      }),
    OffersSuccessRatioAggregatedByDates: () => {
      return {
        size: 0,
        query: {
          bool: {
            must: [],
            should: [],
            must_not: [],
            filter: DATE_FIELD_QUERIES
          }
        },
        aggs: {
          confirmed: {
            filters: {
              filters: {
                all: {
                  bool: {
                    must: {
                      nested: {
                        path: 'lob',
                        query: {
                          term: {
                            'lob.status': 'POLICY_ISSUED'
                          }
                        }
                      }
                    }
                  }
                }
              }
            },
            aggs: {
              development: {
                nested: {
                  path: 'offer'
                },
                aggs: {
                  development: {
                    date_histogram: {
                      field: 'offer.createdDate',
                      ...sharedWeeklyHistogramConfig(app_component)
                    }
                  }
                }
              }
            }
          },
          non_declined: {
            filters: {
              filters: {
                all: {
                  bool: {
                    must_not: {
                      nested: {
                        path: 'lob',
                        query: {
                          term: {
                            'lob.status': 'DECLINED_BY_LOCAL_UNIQA'
                          }
                        }
                      }
                    }
                  }
                }
              }
            },
            aggs: {
              development: {
                nested: {
                  path: 'offer'
                },
                aggs: {
                  development: {
                    date_histogram: {
                      field: 'offer.createdDate',
                      ...sharedWeeklyHistogramConfig(app_component)
                    }
                  }
                }
              }
            }
          }
        }
      };
    },
    OffersSuccessRatiovsLobsCountAggregatedByDates: () => {
      return {
        size: 0,
        query: {
          bool: {
            must: [],
            should: [],
            must_not: [],
            filter: DATE_FIELD_QUERIES
          }
        },
        aggs: {
          confirmed: {
            filters: {
              filters: {
                all: {
                  bool: {
                    must: {
                      nested: {
                        path: 'lob',
                        query: {
                          term: {
                            'lob.status': 'POLICY_ISSUED'
                          }
                        }
                      }
                    }
                  }
                }
              }
            },
            aggs: {
              development: {
                nested: {
                  path: 'offer'
                },
                aggs: {
                  development: {
                    date_histogram: {
                      field: 'offer.createdDate',
                      ...sharedWeeklyHistogramConfig(app_component)
                    },
                    aggs: {
                      offers_count: {
                        reverse_nested: {},
                        aggs: {
                          offers_count: {
                            nested: {
                              path: 'lob'
                            },
                            aggs: {
                              offers_count: {
                                stats: {
                                  field: 'lob.id'
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          non_declined: {
            filters: {
              filters: {
                all: {
                  bool: {
                    must_not: {
                      nested: {
                        path: 'lob',
                        query: {
                          term: {
                            'lob.status': 'DECLINED_BY_LOCAL_UNIQA'
                          }
                        }
                      }
                    }
                  }
                }
              }
            },
            aggs: {
              development: {
                nested: {
                  path: 'offer'
                },
                aggs: {
                  development: {
                    date_histogram: {
                      field: 'offer.createdDate',
                      ...sharedWeeklyHistogramConfig(app_component)
                    },
                    aggs: {
                      offers_count: {
                        reverse_nested: {},
                        aggs: {
                          offers_count: {
                            nested: {
                              path: 'lob'
                            },
                            aggs: {
                              offers_count: {
                                stats: {
                                  field: 'lob.id'
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      };
    },
    OffersSuccessRatioVsPremiumsAggregatedByDates: () => {
      return {
        size: 0,
        query: {
          bool: {
            must: [],
            should: [],
            must_not: [],
            filter: DATE_FIELD_QUERIES
          }
        },
        aggs: {
          confirmed: {
            filters: {
              filters: {
                all: {
                  bool: {
                    must: {
                      nested: {
                        path: 'lob',
                        query: {
                          term: {
                            'lob.status': 'POLICY_ISSUED'
                          }
                        }
                      }
                    }
                  }
                }
              }
            },
            aggs: {
              development: {
                nested: {
                  path: 'offer'
                },
                aggs: {
                  development: {
                    date_histogram: {
                      field: 'offer.createdDate',
                      ...sharedWeeklyHistogramConfig(app_component)
                    },
                    aggs: {
                      premium_sum: {
                        reverse_nested: {},
                        aggs: {
                          premium_sum: {
                            nested: {
                              path: 'lob'
                            },
                            aggs: {
                              premium_sum: {
                                stats: {
                                  field: 'lob.premium_$CURRENCY$'
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          non_declined: {
            filters: {
              filters: {
                all: {
                  bool: {
                    must_not: {
                      nested: {
                        path: 'lob',
                        query: {
                          term: {
                            'lob.status': 'DECLINED_BY_LOCAL_UNIQA'
                          }
                        }
                      }
                    }
                  }
                }
              }
            },
            aggs: {
              development: {
                nested: {
                  path: 'offer'
                },
                aggs: {
                  development: {
                    date_histogram: {
                      field: 'offer.createdDate',
                      ...sharedWeeklyHistogramConfig(app_component)
                    },
                    aggs: {
                      premium_sum: {
                        reverse_nested: {},
                        aggs: {
                          premium_sum: {
                            nested: {
                              path: 'lob'
                            },
                            aggs: {
                              premium_sum: {
                                stats: {
                                  field: 'lob.premium_$CURRENCY$'
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      };
    },
    TotalPremiumPerMonthVerticalBarChart: () => {
      return {
        size: 0,
        aggs: {
          filter_agg: {
            date_histogram: {
              field: 'createdDate',
              interval: '1M',
              time_zone: 'Europe/Berlin',
              min_doc_count: 1
            },
            aggs: {
              status: {
                terms: {
                  field: 'Status',
                  size: 5,
                  order: {
                    premium_sum: 'desc'
                  }
                },
                aggs: {
                  premium_sum: {
                    sum: {
                      field: 'Premium'
                    }
                  }
                }
              }
            }
          }
        },
        query: {
          bool: {
            must: [],
            should: [
              ...policyHasBeenIssuedAtSomePoint,
              ...policyHasBeenDeclinedAtSomePoint
            ],
            must_not: [],
            filter: DATE_FIELD_QUERIES
          }
        }
      };
    },
    NumberOfOffersPerLobPerRiskClassStackedVerticalBarChart: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          filter_agg: {
            terms: {
              field: 'Line of Business (LoB)',
              size: 10,
              order: {
                _term: 'asc'
              }
            },
            aggs: {
              risk_class: {
                terms: {
                  field: 'Risk Class',
                  size: 5,
                  order: {
                    _term: 'asc'
                  }
                }
              }
            }
          }
        }
      }),
    NumberOfOffersPerLobPerRiskClassConcludedStackedVerticalBarChart: () => {
      return {
        size: 0,
        query: {
          bool: {
            must: [],
            should: [],
            must_not: [],
            filter: [
              {
                bool: {
                  should: policyHasBeenIssuedAtSomePoint
                }
              },
              ...DATE_FIELD_QUERIES
            ]
          }
        },
        aggs: {
          filter_agg: {
            terms: {
              field: 'Line of Business (LoB)',
              size: 10,
              order: {
                _term: 'asc'
              }
            },
            aggs: {
              risk_class: {
                terms: {
                  field: 'Risk Class',
                  size: 5,
                  order: {
                    _term: 'asc'
                  }
                }
              }
            }
          }
        }
      };
    },
    AllOffers: () => {
      return {
        m_search_query_lines: [
          {
            index: environment.elasticsearch.index_name,
            query: mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
              size: HIGHLIGHTED_JSON_HITS_LIMIT,
              aggs: {
                unique_offers: constructScriptedMetricNestedCardinalityQuery(
                  'unique_offers_by_id',
                  'offer',
                  'id'
                ),
                unique_lobs: constructScriptedMetricNestedCardinalityQuery(
                  'unique_lobs_by_id',
                  'lob',
                  'id'
                )
              },
              _source: {
                excludes: ['lob.addresses', 'address']
              }
            })
          },
          {
            index: environment.elasticsearch.address_based_index_name,
            query: mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
              size: 0,
              aggs: {
                unique_addresses_address_based: constructScriptedMetricNestedCardinalityQuery(
                  'unique_addresses_address_based_by_id',
                  'address',
                  'id'
                )
              }
            })
          }
        ]
      };
    },
    ResponsibleLocalUnderwriter: () => {
      return {
        elastic_variable: 'lob.localUnderwriters.fullName',
        base: { size: 0 },
        fn: fullNames => {
          return {
            size: 1,
            query: {
              bool: {
                must: [
                  {
                    nested: {
                      path: 'lob.localUnderwriters',
                      query: {
                        terms: {
                          'lob.localUnderwriters.fullName': fullNames
                        }
                      }
                    }
                  }
                ]
              }
            },
            _source: 'lob.localUnderwriters'
          };
        }
      };
    },
    MostRepresentedBusinessPartners: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          filter_agg: {
            nested: {
              path: 'lob.salesPartner'
            },
            aggs: {
              filter_agg: {
                terms: {
                  field: 'lob.salesPartner.name',
                  size: 50,
                  order: {
                    _count: 'desc'
                  }
                }
              }
            }
          }
        }
      }),
    MostRepresentedBusinessPartnersVsPremium: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          filter_agg: {
            nested: {
              path: 'lob.salesPartner'
            },
            aggs: {
              filter_agg: {
                terms: {
                  field: 'lob.salesPartner.name',
                  size: 50,
                  order: {
                    _count: 'desc'
                  }
                },
                aggs: {
                  filter_agg: {
                    reverse_nested: {
                      path: 'lob'
                    },
                    aggs: {
                      filter_agg: {
                        stats: {
                          field: 'lob.premium_$CURRENCY$'
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }),
    MostRepresentedDistributionChannels: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          filter_agg: {
            nested: {
              path: 'offer'
            },
            aggs: {
              filter_agg: {
                terms: {
                  field: 'offer.distributionChannel',
                  size: 50,
                  order: {
                    _count: 'desc'
                  }
                }
              }
            }
          }
        }
      }),
    UnderwriterResponseTimes: () => {
      return {
        elastic_variable: 'lob.localUnderwriters.fullName',
        base: {
          size: 0,
          query: {
            bool: {
              filter: DATE_FIELD_QUERIES
            }
          }
        },
        fn: fullNames => {
          const constructing_fn = full_name => {
            if (!(window as any).mappers) {
              return { size: 0 };
            }
            const uid = (window as any).mappers.Underwriters[full_name];
            return {
              key: `times_${uid}`,
              agg: {
                nested: {
                  path: `offer.responseTimes`
                },
                aggs: {
                  aggs: {
                    percentiles: {
                      field: `offer.responseTimes.${uid}`,
                      percents: [1, 5, 25, 50, 75, 95, 99]
                    }
                  }
                }
              }
            };
          };

          const request = {
            size: 0,
            aggs: {}
          };

          fullNames.map(name => {
            const { key, agg } = constructing_fn(name);
            request.aggs[key] = agg;
          });

          return request;
        }
      };
    },
    MinMaxAvgTotalPremiumTextChart: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          filter_agg: {
            nested: {
              path: 'lob'
            },
            aggs: {
              filter_agg: {
                stats: {
                  field: 'lob.premium_$CURRENCY$'
                }
              }
            }
          },
          median_agg: constructMedianAgg('lob', 'lob.premium_$CURRENCY$')
        }
      }),
    MinMaxAvgUniqaSharePremiumTextChart: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          filter_agg: {
            nested: {
              path: 'lob'
            },
            aggs: {
              filter_agg: {
                stats: {
                  field: 'lob.premium_$CURRENCY$_AdjustedForShare'
                }
              }
            }
          },
          median_agg: constructMedianAgg('lob', 'lob.premium_$CURRENCY$_AdjustedForShare')
        }
      }),
    MinMaxAvgTotalSumInsuredTextChart: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          filter_agg: {
            nested: {
              path: 'lob'
            },
            aggs: {
              filter_agg: {
                stats: {
                  field: 'lob.totalSumInsured_$CURRENCY$_AdjustedForShare'
                }
              }
            }
          },
          median_agg: constructMedianAgg(
            'lob',
            'lob.totalSumInsured_$CURRENCY$_AdjustedForShare'
          )
        }
      }),
    MinMaxOfferUpdatedTextChart: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          filter_agg: {
            nested: {
              path: 'offer'
            },
            aggs: {
              filter_agg: {
                stats: {
                  field: 'offer.updatedDate'
                }
              }
            }
          }
        }
      }),
    MinMaxOfferCreatedTextChart: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        aggs: {
          filter_agg: {
            nested: {
              path: 'offer'
            },
            aggs: {
              filter_agg: {
                stats: {
                  field: 'offer.createdDate'
                }
              }
            }
          }
        }
      }),
    TableExploration: () => {
      return {
        size: 0, // if 0, just aggregate
        query: {
          bool: {
            must: [],
            should: [],
            filter: DATE_FIELD_QUERIES
          }
        },
        _source: {
          excludes: ['lob.addresses', 'address']
        }
      };
    },

    PolicyIssuedPremiumRatiosByTSI: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        size: 4e3,
        query: {
          bool: {
            must: [
              constructNestedTermQuery(
                'lob',
                'lob.lineOfBusiness',
                'Property 2'
              ),
              {
                bool: {
                  should: policyHasBeenIssuedAtSomePoint
                }
              },
              constructNestedExistsQuery(
                'lob.rating',
                'lob.rating.totalPremium*'
              )
            ]
          }
        },
        _source: [
          'lob.rating.totalPremium_*',
          'lob.rating.mTotalPremium_*',
          'offer.id',
          'offer.number',
          'lob.id',
          'lob.totalSumInsured_*'
        ]
      }),
    PolicyIssuedPremiumRatiosVsTime: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        size: 4e3,
        query: {
          bool: {
            must: [
              constructNestedTermQuery(
                'lob',
                'lob.lineOfBusiness',
                'Property 2'
              ),
              {
                bool: {
                  should: policyHasBeenIssuedAtSomePoint
                }
              },
              constructNestedExistsQuery(
                'lob.rating',
                'lob.rating.totalPremium*'
              )
            ]
          }
        },
        // sort: [
        //   {
        //     'offer.createdDate': {
        //       order: 'asc',
        //       nested_path: 'offer'
        //     }
        //   }
        // ],
        _source: [
          'lob.rating.totalPremium_*',
          'lob.rating.mTotalPremium_*',
          'offer.id',
          'offer.number',
          'lob.id',
          'lob.totalSumInsured_*',
          'offer.createdDate'
        ]
      }),

    Only: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        size: 4e3,
        aggs: {
          confirmed: {
            filters: {
              filters: {
                all: {
                  bool: {
                    must: [
                      {
                        bool: {
                          should: policyHasBeenIssuedAtSomePoint
                        }
                      },
                      {
                        nested: {
                          path: 'lob',
                          query: {
                            term: {
                              'lob.lineOfBusiness': 'Property 2'
                            }
                          }
                        }
                      }
                    ]
                  }
                }
              }
            },
            aggs: {
              '1st': {
                filters: {
                  filters: {
                    all: {
                      bool: {
                        must: [
                          {
                            bool: {
                              should: policyHasBeenIssuedAtSomePoint
                            }
                          },
                          {
                            nested: {
                              path: 'lob',
                              query: {
                                term: {
                                  'lob.lineOfBusiness': 'Property 2'
                                }
                              }
                            }
                          }
                        ]
                      }
                    }
                  }
                },
                aggs: {
                  '2nd': {
                    nested: {
                      path: 'lob.rating'
                    },
                    aggs: {
                      last: {
                        histogram: {
                          field: 'lob.rating.adjustedPremiumsRatio',
                          interval: 1,
                          keyed: true
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        },
        query: {
          bool: {
            must: [
              {
                bool: {
                  should: policyHasBeenIssuedAtSomePoint
                }
              },
              {
                nested: {
                  path: 'lob.rating',
                  query: {
                    exists: {
                      field: 'lob.rating.totalPremium*'
                    }
                  }
                }
              }
            ]
          }
        },
        _source: [
          'lob.rating.totalPremium_*',
          'lob.rating.mTotalPremium_*',
          'offer.id',
          'offer.number',
          'lob.id',
          'lob.totalSumInsured_*'
        ]
      }),

    PremiumVsSIvsLoBScatterPlot: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        size: 7e3,
        // query: {
        //   bool: {
        //     filter: [
        //       {
        //         bool: {
        //           must: [
        //             // constructNestedExistsQuery('lob', 'lob.totalSumInsured_$CURRENCY$_*'),
        //             // constructNestedExistsQuery('lob', 'lob.premium_$CURRENCY$*'),
        //             // constructNestedExistsQuery('lob', 'lob.riskClass')
        //           ]
        //         }
        //       }
        //     ]
        //   }
        // },
        _source: [
          'offer.id',
          'offer.number',
          'lob.lineOfBusiness',
          'lob.id',
          'lob.premium_$CURRENCY$*',
          'lob.totalSumInsured_$CURRENCY$_*',
          'lob.riskClass',
          'lob.typeOfBusiness',
          'lob.salesPartner.name',
          'offer.created*',
          'offer.updated*',
          'offer.client.name',
          'offer.regionalDirectorate' // region of origin
        ]
      }),

    AggregatedSuperScorePremiums: () =>
      mergeDeep(BASE_AGG_QUERY_CLAUSE(), true, {
        size: 7e3,
        query: {
          bool: {
            filter: [
              {
                bool: {
                  must: [
                    constructNestedExistsQuery(
                      'lob.rating',
                      'lob.rating.superScore'
                    )
                  ]
                }
              }
            ]
          }
        },
        _source: [
          'offer.id',
          'offer.number',
          'lob.lineOfBusiness',
          'lob.id',
          'lob.premium_$CURRENCY$*',
          'lob.rating.superScore',
          'lob.riskClass',
          'offer.created*',
          'offer.updated*',
          'offer.client.name'
        ]
      }),

    AggregatedSI_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors('AggregatedSI_histogram_chart')
      ),
    AggregatedPremium_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors('AggregatedPremium_histogram_chart')
      ),
    AggregatedTotalPremiumPerIndustry_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalPremiumPerIndustry_histogram_chart'
        )
      ),
    ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndRiskCode_histogram_chart: () => mergeDeep(
      BASE_AGG_QUERY_CLAUSE(),
      true,
      histogramQueryConstructors(
        'ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndRiskCode_histogram_chart'
      )
    ),
    ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndNaceCode_histogram_chart: () => mergeDeep(
      BASE_AGG_QUERY_CLAUSE(),
      true,
      histogramQueryConstructors(
        'ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndNaceCode_histogram_chart'
      )
    ),
    ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndOccupancy_histogram_chart: () => mergeDeep(
      BASE_AGG_QUERY_CLAUSE(),
      true,
      histogramQueryConstructors(
        'ESG_AggregatedPremiumsVsTotalSumInsuredPerRiskCriteriaAndOccupancy_histogram_chart'
      )
    ),
    ESG_AggregatedEmissionsPerOccupancy_histogram_chart: () => mergeDeep(
      BASE_AGG_QUERY_CLAUSE(),
      true,
      histogramQueryConstructors(
        'ESG_AggregatedEmissionsPerOccupancy_histogram_chart'
      )
    ),
    ESG_AggregatedEmissionsPerNaceCode_histogram_chart: () => mergeDeep(
      BASE_AGG_QUERY_CLAUSE(),
      true,
      histogramQueryConstructors(
        'ESG_AggregatedEmissionsPerNaceCode_histogram_chart'
      )
    ),
    AggregatedTotalSumInsuredPerIndustry_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalSumInsuredPerIndustry_histogram_chart'
        )
      ),
    AggregatedPremiumsVsTotalSumInsuredPerIndustry_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedPremiumsVsTotalSumInsuredPerIndustry_histogram_chart'
        )
      ),
    AggregatedLOBCountPerIndustry_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedLOBCountPerIndustry_histogram_chart'
        )
      ),
    AggregatedLocationsCountPerIndustry_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedLocationsCountPerIndustry_histogram_chart'
        )
      ),

    PPM_GlobalStatsPerBusinessActivity_NoCanvas_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'PPM_GlobalStatsPerBusinessActivity_NoCanvas_histogram_chart'
        )
      ),
    AggregatedTotalPremiumPerBusinessActivity_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalPremiumPerBusinessActivity_histogram_chart'
        )
      ),
    AggregatedTotalOffersPerBusinessActivity_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalOffersPerBusinessActivity_histogram_chart'
        )
      ),
    AggregatedTotalSumInsuredPerBusinessActivity_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalSumInsuredPerBusinessActivity_histogram_chart'
        )
      ),
    AggregatedLocationsPerBusinessActivity_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedLocationsPerBusinessActivity_histogram_chart'
        )
      ),
    AggregatedPremiumsVsTotalSumInsuredPerBusinessActivity_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedPremiumsVsTotalSumInsuredPerBusinessActivity_histogram_chart'
        )
      ),

    PPM_GlobalStatsPerNaceCode_NoCanvas_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'PPM_GlobalStatsPerNaceCode_NoCanvas_histogram_chart'
        )
      ),
    AggregatedTotalPremiumPerNaceCodes_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalPremiumPerNaceCodes_histogram_chart'
        )
      ),
    AggregatedTotalOffersPerNaceCodes_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalOffersPerNaceCodes_histogram_chart'
        )
      ),
    AggregatedTotalSumInsuredPerNaceCodes_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalSumInsuredPerNaceCodes_histogram_chart'
        )
      ),
    AggregatedLocationsPerNaceCodes_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedLocationsPerNaceCodes_histogram_chart'
        )
      ),

    AggregatedTurnoverVsPremium_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTurnoverVsPremium_histogram_chart'
        )
      ),
    AggregatedTurnoverVsTSI_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors('AggregatedTurnoverVsTSI_histogram_chart')
      ),
    AggregatedTurnoverVsCount_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors('AggregatedTurnoverVsCount_histogram_chart')
      ),

    GrossRiskProfile_colored_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors('GrossRiskProfile_colored_histogram_chart')
      ),

    AggregatedTotalPremiumPerRegionOfOrigin_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalPremiumPerRegionOfOrigin_histogram_chart'
        )
      ),
    AggregatedLOBCountPerRegionOfOrigin_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedLOBCountPerRegionOfOrigin_histogram_chart'
        )
      ),
    AggregatedTotalSumInsuredPerRegionOfOrigin_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalSumInsuredPerRegionOfOrigin_histogram_chart'
        )
      ),
    AggregatedLocationsCountPerRegionOfOrigin_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedLocationsCountPerRegionOfOrigin_histogram_chart'
        )
      ),

    AggregatedTotalPremiumPerSalesPartner_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalPremiumPerSalesPartner_histogram_chart'
        )
      ),
    AggregatedLOBCountPerSalesPartner_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedLOBCountPerSalesPartner_histogram_chart'
        )
      ),
    AggregatedTotalSumInsuredPerSalesPartner_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalSumInsuredPerSalesPartner_histogram_chart'
        )
      ),
    AggregatedLocationsCountPerSalesPartner_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedLocationsCountPerSalesPartner_histogram_chart'
        )
      ),

    AggregatedTotalPremiumPerAvgRiskClassPerPeril_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalPremiumPerAvgRiskClassPerPeril_histogram_chart'
        )
      ),
    AggregatedTotalSumInsuredPerAvgRiskClassPerPeril_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalSumInsuredPerAvgRiskClassPerPeril_histogram_chart'
        )
      ),
    AggregatedLOBCountPerAvgRiskClassPerPeril_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedLOBCountPerAvgRiskClassPerPeril_histogram_chart'
        )
      ),
    AggregatedGroupedPremiumPerAvgRiskClassPerPeril_LoBBased_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedGroupedPremiumPerAvgRiskClassPerPeril_LoBBased_histogram_chart'
        )
      ),
    AggregatedGroupedPremiumPerAvgRiskClassPerPeril_location_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedGroupedPremiumPerAvgRiskClassPerPeril_location_based_histogram_chart'
        )
      ),

    AggregatedAvgPDAPerAvgRiskClassPerPeril_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAvgPDAPerAvgRiskClassPerPeril_histogram_chart'
        )
      ),
    AggregatedAvgPDAPerTSIPerPeril_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAvgPDAPerTSIPerPeril_histogram_chart'
        )
      ),

    AggregatedBrokerCommission_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors('AggregatedBrokerCommission_histogram_chart')
      ),

    AggregatedTotalMPremiumPerPeril_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalMPremiumPerPeril_histogram_chart'
        )
      ),
    AggregatedTotalTPremiumPerPeril_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTotalTPremiumPerPeril_histogram_chart'
        )
      ),
    AggregatedRiskScorePerAvgRiskClassPerPeril_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedRiskScorePerAvgRiskClassPerPeril_histogram_chart'
        )
      ),
    AggregatedRiskScoreVsPeril_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors('AggregatedRiskScoreVsPeril_histogram_chart')
      ),
    AggregatedInfoLevelVsRiskClassVsPeril_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedInfoLevelVsRiskClassVsPeril_histogram_chart'
        )
      ),

    AggregatedSuperscoresPerLocationsPerIndustry_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedSuperscoresPerLocationsPerIndustry_histogram_chart'
        )
      ),
    AggregatedSuperscoresPerRequestFromCountry_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedSuperscoresPerRequestFromCountry_histogram_chart'
        )
      ),
    AggregatedSuperscoresPerBucketedTSI_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedSuperscoresPerBucketedTSI_histogram_chart'
        )
      ),

    AggregatedTSIPerInsuredObject_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTSIPerInsuredObject_histogram_chart'
        )
      ),

    AggregatedTSIPerConstructionMaterials_location_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedTSIPerConstructionMaterials_location_based_histogram_chart'
        )
      ),

    // RISK ENGINEERING BEGIN
    AggregatedAddressesVsIndustryCategory_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesVsIndustryCategory_histogram_chart'
        )
      ),

    AggregatedAddressesPerIL_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors('AggregatedAddressesPerIL_histogram_chart')
      ),
    AggregatedAddressesPerScore_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerScore_histogram_chart'
        )
      ),
    AggregatedAddressesPerPML_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors('AggregatedAddressesPerPML_histogram_chart')
      ),
    AggregatedAddressesPerRC_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors('AggregatedAddressesPerRC_histogram_chart')
      ),

    AggregatedAddressesPerRC_vsPML_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerRC_vsPML_address_based_histogram_chart'
        )
      ),
    AggregatedAddressesPerRC_vsIL_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerRC_vsIL_address_based_histogram_chart'
        )
      ),
    AggregatedAddressesPerRC_vsScore_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerRC_vsScore_address_based_histogram_chart'
        )
      ),
    AggregatedAddressesPerRC_vsRecommendations_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerRC_vsRecommendations_address_based_histogram_chart'
        )
      ),

    AggregatedAddressesPerIC_vsRecommendations_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerIC_vsRecommendations_address_based_histogram_chart'
        )
      ),
    AggregatedAddressesPerIC_vsPML_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerIC_vsPML_address_based_histogram_chart'
        )
      ),
    AggregatedAddressesPerIC_vsIL_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerIC_vsIL_address_based_histogram_chart'
        )
      ),
    AggregatedAddressesPerIC_vsScore_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerIC_vsScore_address_based_histogram_chart'
        )
      ),

    AggregatedAddressesPerScorePerPML_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerScorePerPML_address_based_histogram_chart',
          DATE_FIELD_QUERIES
        )
      ),
    AggregatedAddressesPerAsIfScorePerPML_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerAsIfScorePerPML_address_based_histogram_chart',
          DATE_FIELD_QUERIES
        )
      ),

    AggregatedAddressesPerAsIfScorePerPML_EML_Ratio_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerAsIfScorePerPML_EML_Ratio_address_based_histogram_chart',
          DATE_FIELD_QUERIES
        )
      ),

    AggregatedAddressesPerRiskScore_vsPremiums_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerRiskScore_vsPremiums_address_based_histogram_chart'
        )
      ),
    AggregatedAddressesPerRiskScore_vsPremiums_IndividualLocations_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerRiskScore_vsPremiums_IndividualLocations_address_based_histogram_chart'
        )
      ),

    AggregatedAddressesPerRecommendationKinds_vsPriority_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerRecommendationKinds_vsPriority_address_based_histogram_chart'
        )
      ),

    AggregatedAddressesPerRecommendationKinds_vsRecommendations_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerRecommendationKinds_vsRecommendations_address_based_histogram_chart'
        )
      ),

    AggregatedAddressesPerRecommendationKinds_vsLocationCounts_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerRecommendationKinds_vsLocationCounts_address_based_histogram_chart'
        )
      ),

    AggregatedAddressesPerRecommendationKinds_vsNegativeCondition_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerRecommendationKinds_vsNegativeCondition_address_based_histogram_chart'
        )
      ),
    AggregatedAddressesPerEvaluations_vsProbabilityCriterias_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerEvaluations_vsProbabilityCriterias_address_based_histogram_chart'
        )
      ),
    AggregatedAddressesPerEvaluations_vsPotentialCriterias_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerEvaluations_vsPotentialCriterias_address_based_histogram_chart'
        )
      ),
    AggregatedAddressesPerEvaluations_vsPreventionCriterias_address_based_histogram_chart: () =>
      mergeDeep(
        BASE_AGG_QUERY_CLAUSE(),
        true,
        histogramQueryConstructors(
          'AggregatedAddressesPerEvaluations_vsPreventionCriterias_address_based_histogram_chart'
        )
      ),
    // RISK ENGINEERING END
    // RE INSURANCE BEGIN
    RIM_Property_AggregatedStatsByPML_address_based_histogram_chart: () => mergeDeep(
      BASE_AGG_QUERY_CLAUSE(),
      true,
      histogramQueryConstructors(
        'RIM_Property_AggregatedStatsByPML_address_based_histogram_chart'
      )
    ),
    RIM_Property_AggregatedStatsByPMLminusTimeRange_AbsoluteValues_address_based_histogram_chart: (the_app_component?: AppComponent) => mergeDeep(
      BASE_AGG_QUERY_CLAUSE(),
      true,
      histogramQueryConstructors(
        'RIM_Property_AggregatedStatsByPMLminusTimeRange_AbsoluteValues_address_based_histogram_chart',
        null,
        the_app_component
      )
    ),
    RIM_Property_AggregatedStatsByPMLminusTimeRange_PercentualValues_address_based_histogram_chart: (the_app_component?: AppComponent) => mergeDeep(
      BASE_AGG_QUERY_CLAUSE(),
      true,
      histogramQueryConstructors(
        'RIM_Property_AggregatedStatsByPMLminusTimeRange_PercentualValues_address_based_histogram_chart',
        null,
        the_app_component
      )
    ),
    RIM_Property_AggregatedStatsByTSI_address_based_histogram_chart: () => mergeDeep(
      BASE_AGG_QUERY_CLAUSE(),
      true,
      histogramQueryConstructors(
        'RIM_Property_AggregatedStatsByTSI_address_based_histogram_chart'
      )
    ),
    RIM_Property_AggregatedStatsByTSIminusTimeRange_AbsoluteValues_address_based_histogram_chart: (the_app_component?: AppComponent) => mergeDeep(
      BASE_AGG_QUERY_CLAUSE(),
      true,
      histogramQueryConstructors(
        'RIM_Property_AggregatedStatsByTSIminusTimeRange_AbsoluteValues_address_based_histogram_chart',
        null,
        the_app_component
      )
    ),
    RIM_Property_AggregatedStatsByTSIminusTimeRange_PercentualValues_address_based_histogram_chart: (the_app_component?: AppComponent) => mergeDeep(
      BASE_AGG_QUERY_CLAUSE(),
      true,
      histogramQueryConstructors(
        'RIM_Property_AggregatedStatsByTSIminusTimeRange_PercentualValues_address_based_histogram_chart',
        null,
        the_app_component
      )
    )

  };

  if (
    typeof specific_component === 'string' &&
    specific_component in VIEW_VS_QUERIES_ON_LOAD
  ) {
    specific_component = VIEW_VS_QUERIES_ON_LOAD[specific_component];
  }

  if (specific_component) {
    if (Array.isArray(specific_component)) {
      const query_map = {};

      Object.keys(qs)
        .filter(okey => specific_component.includes(okey))
        .map(okey => {
          okey = okey.replace(replacement_re, '');
          query_map[okey] = qs[okey](app_component);
        });

      return query_map;
    } else {
      const replaced_key = specific_component.replace(replacement_re, '');

      if (replaced_key.includes('LoBBased_histogram_chart')) {
        console.warn(qs[replaced_key]());
      }

      return {
        [replaced_key]: qs[replaced_key](app_component)
      };
    }
  }

  return arrayToObject(
    Object.keys(qs).map(okey => {
      okey = okey.replace(replacement_re, '');

      return {
        q_name: okey,
        q_itself: qs[okey](app_component)
      };
    }),
    'q_name',
    'q_itself'
  );
}
