import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';
import { ElasticService } from '../services/elastic.service'
import { Observable } from 'rxjs';

@Injectable()
export class ElasticInterceptor implements HttpInterceptor {

    constructor(private auth: ElasticService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({
            setHeaders: {
                Authorization: `Basic ${this.auth._getBearer()}`
            }
        });
        return next.handle(request);
    }
}