import {
  Component,
  OnInit,
  ChangeDetectorRef,
  OnDestroy,
  AfterViewInit
} from '@angular/core';
import { ElasticService } from '../services/elastic.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-progress-indicator',
  templateUrl: './progress-indicator.component.html',
  styleUrls: ['./progress-indicator.component.scss']
})
export class ProgressIndicatorComponent
  implements OnInit, OnDestroy, AfterViewInit {
  constructor(private es: ElasticService, private cd: ChangeDetectorRef) {}

  first_load = true;

  percentage = 0;
  show = true;

  ngOnInit() {}

  ngAfterViewInit() {
    this.es.percentualProgress.subscribe(percentage => {
      this.show = true;
      percentage = parseInt(`${percentage}`, 10);
      this.percentage = percentage;
      if (percentage === 100) {
        setTimeout(() => {
          this.show = false;
          try {
            this.cd.detectChanges();
          } catch (err) {
            console.warn(err);
          }
          if (this.first_load) {
            const selector = document.querySelector('.loading_screen');

            if (!selector) {
              return;
            }
            selector.classList.add(
              'animated',
              'fadeOut',
              'animationDuration2s'
            );

            setTimeout(() => {
              selector.remove();
              this.first_load = false;
            }, 2000);
          }
        }, 1e3);
      }
      try {
        this.cd.detectChanges();
      } catch (err) {
        console.warn(err);
      }
    });
  }

  ngOnDestroy() {
    this.es.percentualProgress.unsubscribe();
    this.es.percentualProgress = new Subject<number>();
  }
}
