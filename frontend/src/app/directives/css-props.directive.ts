import { Directive, Input, ElementRef, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[appCssProps]'
})
export class CssPropsDirective implements OnChanges {
  @Input() appCssProps: any;

  constructor(private element: ElementRef) {}

  ngOnChanges({ appCssProps }: SimpleChanges) {
    if (appCssProps && appCssProps.currentValue) {
      const { style } = this.element.nativeElement;
      for (const [k, v] of Object.entries(appCssProps.currentValue)) {
        style.setProperty(k, v);
      }
    }
  }
}
