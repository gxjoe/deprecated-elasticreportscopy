import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { FilterComponent } from '../utils/higher-order/FilterComponent';
import { ElasticService } from '../services/elastic.service';
import IFilterComponentQueryMeta from '../utils/higher-order/IFilterComponentQueryMeta';
import { ElasticFilterSubtypes, ElasticQueryClauses } from '../elastic/constants';
import { extract_base_path } from '../charts/histogram-table-chart/constants';
import { nestedBoolMustOfBoolShoulds } from '../elastic/utils';
import { uuidv4 } from '../utils/helpers/helpers';
import { SearchResponse } from 'elasticsearch';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss']
})
export class TextInputComponent extends FilterComponent implements OnInit {
  @Input()
  klass;

  @Input()
  heading;

  @Input()
  placeholder;

  @Input()
  elastic_variable;

  @Input() isMultiselect = true;

  selectedValues: string[] = [];

  @Input()
  hasAsyncAutocomplete: string;
  autocompleteSuggestions: Array<string> = [];
  loading = false;

  constructor(private es: ElasticService, private changeDetRef: ChangeDetectorRef) {
    super();
  }

  get valueOf() {
    return this.selectedValues.slice();
  }

  onChange(_) {
    this.es.readyForRefresh();
  }

  ngOnInit() {
    this.es.app_component.setComponent(this.klass, this);
    this.es.app_component.filterComponents.push(this);
  }

  filterQueryBuilder = (): IFilterComponentQueryMeta => {
    const build: IFilterComponentQueryMeta = {
      klass: this.klass,
      elastic_variable: this.elastic_variable,
      subtype: ElasticFilterSubtypes.term,
      operators: [],
      values: [],
      clause: ElasticQueryClauses.must,
      nested: this.elastic_variable.includes('.'),
      finished_query: null
    };

    if (!this.valueOf || !this.valueOf.length) {
      return build;
    }

    build.finished_query = {
      ...nestedBoolMustOfBoolShoulds(extract_base_path(this.elastic_variable), this.elastic_variable, this.valueOf)
    };

    return build;
  }

  onSearchChange(selectedValues: string, _hasAsyncAutocomplete: string): void {
    if (!selectedValues?.length) {
      this.autocompleteSuggestions = [];
      this.loading = false;
      return;
    }

    console.log(`will use autocomplete for...: ${selectedValues}`, _hasAsyncAutocomplete);

    this.loading = true;
    switch (_hasAsyncAutocomplete) {
      case 'handleClientNameLookup':
        this.handleClientNameLookup(selectedValues);
        break;
      case 'handleOfferNumberLookup':
        this.handleOfferNumberLookup(selectedValues)
        break;
    }
  }

  handleClientNameLookup(query_string: string) {
    this.es
      .POST(
        {
          size: 0,
          query: {
            bool: {
              must: [
                {
                  nested: {
                    path: 'offer.client',
                    query: {
                      bool: {
                        should: [
                          // {
                          //   match: {
                          //     'offer.client.name.as_text': {
                          //       query: query_string,
                          //       fuzziness: 3,
                          //       operator: 'and'
                          //     }
                          //   }
                          // }
                          {
                            match_phrase_prefix: {
                              'offer.client.name.as_text': {
                                query: query_string,
                                max_expansions: 50
                              }
                            }
                          }
                        ]
                      }
                    }
                  }
                }
              ]
            }
          },
          aggs: {
            client: {
              nested: {
                path: 'offer.client'
              },
              aggs: {
                name: {
                  terms: {
                    field: 'offer.client.name',
                    size: 10
                  }
                }
              }
            }
          }
        },
        null,
        uuidv4()
      )
      .subscribe((data: { [reponse: string]: SearchResponse<any> }) => {
        try {
          const listOfOptions: Array<string> = (data.response || data).aggregations.client.name.buckets.map(buck => buck.key);
          this.autocompleteSuggestions = listOfOptions;
          this.changeDetRef.detectChanges();
        } catch (err) {
          console.warn(err);
        }
      });
  }

  handleOfferNumberLookup(query_string: string) {
    this.es
      .POST(
        {
          size: 0,
          query: {
            bool: {
              must: [
                {
                  nested: {
                    path: 'offer',
                    query: {
                      bool: {
                        should: [
                          {
                            match_phrase_prefix: {
                              'offer.number': {
                                query: query_string,
                                max_expansions: 50
                              }
                            }
                          }
                        ]
                      }
                    }
                  }
                }
              ]
            }
          },
          aggs: {
            client: {
              nested: {
                path: 'offer'
              },
              aggs: {
                name: {
                  terms: {
                    field: 'offer.number',
                    size: 10
                  }
                }
              }
            }
          }
        },
        null,
        uuidv4()
      )
      .subscribe((data: { [reponse: string]: SearchResponse<any> }) => {
        try {
          const listOfOptions: Array<string> = (data.response || data).aggregations.client.name.buckets.map(buck => buck.key);
          this.autocompleteSuggestions = listOfOptions;
          this.changeDetRef.detectChanges();
        } catch (err) {
          console.warn(err);
        }
      });
  }
}
