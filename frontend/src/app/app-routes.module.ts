import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from './_routing-controllers/shared/shared.module';

import { AppComponent } from './app.component';
import { StandaloneMapsComponent } from './_routing-controllers/standalone-maps/standlone-maps.component';

import { EfficiencyAndUserManagementComponent } from './_routing-controllers/efficiency-and-user-management/efficiency-and-user-management.component';
import { ProductProtfolioManagementComponent } from './_routing-controllers/product-portfolio-management/product-portfolio-management.component';
import { RiskEngineeringComponent } from './_routing-controllers/risk-engineering/risk-engineering.component';
import { SalesEfficiencyManagementComponent } from './_routing-controllers/sales-efficiency-management/sales-efficiency-management.component';
import { ReInsuranceManagementComponent } from './_routing-controllers/re-insurance-management/re-insurance-management.component';
import { EnvironmentalSocialGovernanceComponent } from 'src/app/_routing-controllers/environmental-social-governance/environmental-social-governance.component';

const appRoutes: Routes = [
  {
    path: 'efficiency_user_management',
    component: EfficiencyAndUserManagementComponent
  },
  {
    path: 'product_portfolio_management',
    component: ProductProtfolioManagementComponent
  },
  {
    path: 'maps_and_positions',
    component: StandaloneMapsComponent
  },
  {
    path: 'risk_engineering_management',
    component: RiskEngineeringComponent
  },
  {
    path: 'sales_efficiency_management',
    component: SalesEfficiencyManagementComponent
  },
  {
    path: 're_insurance_management',
    component: ReInsuranceManagementComponent
  },
  {
    path: 'environmental_social_governance',
    component: EnvironmentalSocialGovernanceComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/product_portfolio_management'
  },
  {
    path: '**',
    redirectTo: '/product_portfolio_management'
  }
];

@NgModule({
  declarations: [
    EfficiencyAndUserManagementComponent,
    ProductProtfolioManagementComponent,
    StandaloneMapsComponent,
    RiskEngineeringComponent,
    SalesEfficiencyManagementComponent,
    ReInsuranceManagementComponent,
    EnvironmentalSocialGovernanceComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false
      }
    )
  ],
  exports: [RouterModule],
  providers: [AppComponent, RouterModule]
})
export class AppRoutingModule { }
