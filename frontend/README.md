# CBN DEMO

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.

## Start up

Run `npm install` in home folder.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build --prod` to build the project. The build artifacts will be stored in the `dist/` directory.

## Deploy

Run `(yarn or npm) deploy`.

## Run and Deploy to App Engine

Run `(yarn or npm) build_and_deploy`.

## API Keys

### Frontend Maps

https://console.cloud.google.com/apis/credentials?project=map-for-work-int-01625575

## Client-Side Steps (recommended when onboarding new uniqa users)

### Enabling WebGL in Chrome for use in Deck.gl layers

1. Open Google Chrome.
2. In the address bar, type **chrome://flags/**, and press Enter.
3. Scroll to **Disable WebGL** - Enabling this option prevents web applications from accessing the WebGL API, and click **Enable**
4. Close & reopen Google Chrome

### Frozen pkgs

- immutable
- kdbush

# Releasing a new version

For releasing a new version [release-it](https://github.com/release-it/release-it) is being used. It bumps the version in the package.json, tags a commit, creates a Github release and updates [CHANGELOG](./CHANGELOG.md).

It can be triggered in 2 ways:

- via [Github Action](./.github/workflows/release.yml)
- in command line via `npm run release`. It starts an interactive flow as shown [here](https://github.com/release-it/release-it#release-it-)