#!/bin/bash -xe
# Build frontend
cd frontend/
# Clean node modules
#rm -rf node_modules
npm install --no-audit --no-progress node-sass
npm install --no-audit --no-progress
#ng build --prod
npm run gbuild

cd ..

#Prepare docker folder
rm -rf docker/nginx/app
#mv frontend/dist/material-demo docker/nginx/app
mv frontend/dist docker/nginx/app

if [[ $# = 1 ]] ; then
    # Push to server
    echo "Destination: $1"
    destination=$1
    ssh -o StrictHostKeyChecking=no ${destination} rm -rf /home/docker/er_deploy/*
    scp -r -o StrictHostKeyChecking=no docker/* ${destination}:/home/docker/er_deploy/
    ssh -o StrictHostKeyChecking=no ${destination} << EOF
echo "**** Deploying to docker ****"
cd ~/elastic_reports/
docker-compose down
rm -rf nginx  elasticsearch docker-compose.yml Dockerfile
cp -r ../er_deploy/* .
mkdir -p volume_kib/kibana/conf/
mv kibana.yml volume_kib/kibana/conf/kibana.yml
chmod a+w -R volume_kib/kibana/conf/kibana.yml
chmod a+w -R nginx/logs
chmod a+w -R elasticsearch/logs
if [[ "$PYTHON_REBUILD" = true ]] ; then
  docker-compose up -d --build
else
  docker-compose up -d
fi
echo "**** Deployment done ****"
EOF
fi
