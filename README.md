#### Structure

- docker - contains folder structure for docker and docker-compose.yml file to run containers
- frontend - angular application
- importer - python/flask application for running imports

#### Docker commands

- start containers `docker-compose up -d`
- stop containers `docker-compose down`
- show running containers `docker ps`
- list existing images `docker images`
- show logs `docker logs <container_name>`
- `systemctl status elastic-reports` - show docker service status
- `systemctl start elastic-reports` - start docker service status
- `systemctl stop elastic-reports` - stop docker service status

#### Jenkins build

- Parametrized build:
  - add Boolean parameter: `PYTHON_REBUILD`, description: _Use if python/requirements.txt changed_
  - add String parameter: `DATA_ES_INDEX`, default value: testenv_data
  - add String parameter: `ADDRESSES_ES_INDEX`, default value: testenv_addresses, description: value must include the word "addresses"
  - add String parameter: `USERS_ES_INDEX`, default value: testenv_users
- Execute NodeJS script - makes `npm` available to build angular app
- Execute shell - configure and deploy to server

  ```
  # importer configuration
  cat <<EOF > importer/demo/.env
  BONSAI_URL_LIVE=https://user:user@test-er.uniqa-international.eu:443/es
  MAPS_API_KEY=AIzaSyDoxRYKjfazjxgxrwZuujn0aXbj2Yy9GJ8
  UNIQA_SYNC_URL_PATH_PREFIX=/co
  ES_INDEX_USERS_NAME=$USERS_ES_INDEX
  ES_COLLECTION_NAME=$DATA_ES_INDEX
  ES_ADDRESS_CENTERED_COLLECTION_NAME=$ADDRESSES_ES_INDEX
  MEM_CACHE_ENDPOINT=10.66.104.218
  MEM_CACHE_PORT=9081
  CBN_REST_OFFERS_ENDPOINT=https://m.uniqa-international.eu/cbn-rest/restapi/data/offers/{0}/{1}/{2}?_token=9y5iRq3L7OE6IHpPIFHc0d2j4gYcFf
  CBN_REST_USERS_ENDPOINT=https://m.uniqa-international.eu/cbn-rest/restapi/data/users/{0}?_token=9y5iRq3L7OE6IHpPIFHc0d2j4gYcFf
  EOF
  # frontend configuration
  sed -i "s/index_name: '.*'/index_name: '$DATA_ES_INDEX'/g" frontend/src/environments/environment.prod.ts
  sed -i "s/address_based_index_name: '.*'/address_based_index_name: '$ADDRESSES_ES_INDEX'/g" frontend/src/environments/environment.prod.ts
  # prod only:
  # sed -i "s/test-er.uniqa-international.eu/er.uniqa-international.eu/g" frontend/src/environments/environment.prod.ts
  # ?? sed -i "s#endpoint: 'http://127.0.0.1:5050/memcached_wrapper'#endpoint: 'https://er.uniqa-international.eu/co/memcached_wrapper'#g" frontend/src/environments/environment.prod.ts
  # sed -i "s/defaultCbnToken: '.*'/defaultCbnToken: ''/g" frontend/src/environments/environment.prod.ts


  # docker configuration
  # prod only (using IP because load balancer URL is not working):
  sed -i "s#https://t.uniqa-international.eu#http://10.66.104.198:8080#g" docker/nginx/er_vhost.conf

  chmod +x deploy.sh
  # change IP address to destination server
  ./deploy.sh "docker@10.66.104.218"
  ```

#### Elasticsearch commands

- `curl https://test-er.uniqa-international.eu/es/_cat/indices?v` - show indices
- `curl https://er.uniqa-international.eu/es/prod_users/_search?pretty=true&size=30` - show first 30 entries from index prod_users
- `curl -X DELETE https://test-er.uniqa-international.eu/es/dev__feb_x` - deletes index dev\_\_feb_x
- `curl -XPUT "https://er.uniqa-international.eu/es/prod_data/_settings" -d '{ "index" : { "max_result_window" : 500000 } }' -H 'Content-Type: application/json'` - increase window size before running address import

#### Python/Flask commands

- `curl https://test-er.uniqa-international.eu/co/fetch_all_users` - downloads users as JSON and stores on the disk
- `curl https://test-er.uniqa-international.eu/co/populate/users` - uploads users to Elasticsearch
- `curl https://test-er.uniqa-international.eu/co/populate/rest?start=YYYY/M/D&end=YYYY/M/D&index_name=dev_data` - sync offer data from CBN to Elasticsearch (no leading 0 in month and day!) or form available at https://test-er.uniqa-international.eu/co/html
- `curl https://test-er.uniqa-international.eu/co/populate_addresses/rest?start=YYYY/M/D&end=YYYY/M/D&threads=1&index_name=dev_addresses&lob_index_name=dev_data` - sync addresses data from existing data to Elasticsearch (no leading 0 in month and day!) or form available at https://test-er.uniqa-international.eu/co/html (address index must conatin the word _address_)

#### Server setup:

```
# login as root user
## setup and install docker
# https://docs.docker.com/install/linux/docker-ce/ubuntu/
apt-get update
apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get install docker-ce docker-ce-cli containerd.io

## install docker compose
curl -L https://github.com/docker/compose/releases/download/1.14.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

## setup docker user
#groupadd docker
adduser --home /home/docker --disabled-password --gecos "" --ingroup docker docker

## setup for elasticsearch
sysctl -w vm.max_map_count=262144
# vim /etc/sysctl.conf --> add vm.max_map_count=262144

## setup directory structure
mkdir -p /home/docker/elastic_reports/volume_es
mkdir -p /home/docker/elastic_reports/volume_kib
mkdir -p /home/docker/er_deploy
mkdir -p /home/docker/.ssh
chmod -R 777 /home/docker/elastic_reports/volume_es
chmod -R 777 /home/docker/elastic_reports/volume_kib

cat << EOT >> /home/docker/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDA+6wfIkPPxVxmUdwc+HW060D43srruoigUzdGcki+PhLcrLYeV1kuSUneMvDuUlow8GwHSZ14uLgnd9pdvLvqjomJ2dm05TAbLQ6j/Yhgvc2Hj3iim8fwndRpINvz2PH7/pG25KGzSZK5/qWXJadjHoF//JPjkvMENnRmsf3XahFyP1ysFjSnajn2ECMbnIOlg2yIaFVnkninv9lMkRos8raFJvXxCxJAFMgc96dsjI3gVAKpj9oYXKTS/PH6ogGgvZm13B08FsUGO9x+ySGPTs/Q1TulfBT3HnGlv6CRr/Zd6LZaxV8MFznuHRe5OOgzexJRl/nGliBe5gC9qKJqZWej0cAp4JGNj4o7Hf4dW3juGwmb4N+yq5aYs+iQ+D6o483GWcRDFzGeZ067ZApxJR0Jd291KH6+hjmQxfAGCRqUP8Fb/hGILsB14aA4Ki64oBvBDtPitW790JKxRyCvzJBCPl93z8RIA22BSEEhjb1NevBs5KTW5JMrzTOiDzmkS4/qhwCiM86HhEXRBgu6QMbSYhgOeGaaVsv7EB6nQtb8VsU6/0ffw6F9aWuqV6vBtutJB42ZDbZWOkgzFU++IdrxK5W1WyWfrL3zkaHKPtwWQuxuiegtSB0x4vc3P6Wp2clKhMMvOrGPQ1PXugeg+kzONcVzE1Ytl3dBNiO4YQ== gitlab@insdata.sk
EOT

chown -R docker:docker /home/docker/elastic_reports

# after first start of docker fix permissions
chmod go+rw -R ~/elastic_reports
chown docker:docker -R ~/elastic_reports

# copy docker_health.sh file from http://git.uniqa-international.eu:10080/cbn/elasticReports/blob/master/docker_health.sh into /home/docker/docker_health.sh
# copy import_last_week.sh file from http://git.uniqa-international.eu:10080/cbn/elasticReports/blob/master/import_last_week.sh into /home/docker/import_last_week.sh
chmod +x /home/docker/docker_health.sh
chmod +x /home/docker/import_last_week.sh

# schedule docker health check - restarts docker if elastic exits on OutOfMemoryError
# schedule weekly data import to every Monday 07:00
crontab -e
# add lines at the end
*/5 * * * * /home/docker/docker_health.sh
0 7 * * 1 /home/docker/import_last_week.sh

# startup on server restart
cat << EOT > /etc/systemd/system/elastic-reports.service
[Unit]
Description=Elastic reports
Requires=docker.service
After=docker.service
[Service]
Restart=always
ExecStartPre=/usr/local/bin/docker-compose -f /home/docker/elastic_reports/docker-compose.yml down -v
ExecStart=/usr/local/bin/docker-compose -f /home/docker/elastic_reports/docker-compose.yml up
ExecStop=/usr/local/bin/docker-compose -f /home/docker/elastic_reports/docker-compose.yml down -v
User=docker
Group=docker
[Install]
WantedBy=multi-user.target
EOT

systemctl enable elastic-reports
reboot
```

#### Elasticsearch configurations & links

curl -XPUT 'localhost:9200/prod_data/\_settings' -H 'Content-Type: application/json' -d '{
"index.indexing.slowlog.threshold.index.warn": "10s",
"index.indexing.slowlog.threshold.index.info": "5s",
"index.indexing.slowlog.threshold.index.debug": "500ms",
"index.indexing.slowlog.threshold.index.trace": "1ms",
"index.indexing.slowlog.level": "warn",
"index.indexing.slowlog.source": "500"
}'

curl -XPUT "localhost:9200/prod_data/\_settings" -H 'Content-Type: application/json' -d'{
"index.search.slowlog.threshold.query.warn": "10s",
"index.search.slowlog.threshold.query.info": "5s",
"index.search.slowlog.threshold.query.debug": "2s",
"index.search.slowlog.threshold.query.trace": "1ms",
"index.search.slowlog.threshold.fetch.warn": "1s",
"index.search.slowlog.threshold.fetch.info": "800ms",
"index.search.slowlog.threshold.fetch.debug": "500ms",
"index.search.slowlog.threshold.fetch.trace": "1ms",
"index.search.slowlog.level": "warn"
}'

curl -XPUT "localhost:9200/prod_data/\_settings" -H 'Content-Type: application/json' -d'{
"index.refresh_interval": "5s"
}'

https://er.uniqa-international.eu/es/prod_data/_settings?pretty
https://er.uniqa-international.eu/es/_tasks?pretty&human&detailed
https://er.uniqa-international.eu/es/_nodes/stats?pretty

# 2022 Overhaul

## Test env

https://elastic-report-test.uniqa-cluster.eu/product_portfolio_management?token=L8Rdsun6wFZYrKqB3EtEHyrOySZpY4c2

## Elasticsearch

Raw ES: http://10.66.104.172:5601/
Reverse proxy: https://elastic-report-test.uniqa-cluster.eu/es/

## Kibana

http://10.66.104.172:443/app/kibana

## Startup & Shutdown scripts

```bash
ssh jsorocin@10.66.104.172
p: zDNGGJdUTgB83xqc
```

Startup:

```bash
cd /opt/elastic; docker-compose up -d
```

Shutdown

```bash
cd /opt/elastic; docker-compose down -d
```

_`-d` stands for 'run in the background'_
