#!/bin/bash
echo '----------------------------------------------' &>> _status.log
echo $(date) &>> _status.log
curl -s localhost:9090/status &>> _status.log
echo '-' &>> _status.log

startDate=`date '+%Y/%-m/%-d' -d '-7 days'`
endDate=`date '+%Y/%-m/%-d'`
indexName="prod_data"
addressIndexName="prod_addresses"
host="https://er.uniqa-international.eu"
url="${host}/co/populate/rest?start=${startDate}&end=${endDate}&index_name=${indexName}"
echo "---------------------------------" &>> /home/docker/_import_last_week.log
echo `date "+%F %T"` $url &>> /home/docker/_import_last_week.log
urlAddress="${host}/co/populate_addresses/rest?start=None&end=None&threads=12&index_name=${addressIndexName}&lob_index_name=${indexName}&last_week_only=true"

# Refresh users and trigger data import
curl -s "${host}/es/_cat/indices?v" &>> /home/docker/_import_last_week.log
curl "${host}/co/fetch_all_users"
sleep 180
curl "${host}/co/populate/users"
sleep 180
curl -s $url

# run check for 4 hours
i="48"
while [ $i -gt 0 ]
do
  syncEnd=`docker logs --since "4h" elastic_reports_python_1 2>&1 | grep '\-\-\- ENDING SYNC \-\-\-' | wc -l`
  if [ $syncEnd -gt 0 ]; then
    echo `date "+%F %T"` "Sync finished" &>> /home/docker/_import_last_week.log
    break
  fi
   if [ `curl -s https://er.uniqa-international.eu/es/ | grep "You Know, for Search" | wc -l` -ne 1 ] || [ `docker ps | grep "Up " | wc -l` -ne 5 ];
  then
    echo "Aborting process" &>> /home/docker/_import_last_week.log
    #exit 1;
  fi
  echo `date "+%F %T"` "waiting for sync" &>> /home/docker/_import_last_week.log
  sleep 300
  i=$[$i-1]
done

curl $urlAddress
echo `date "+%F %T"` "Finished" &>> /home/docker/_import_last_week.log
curl -s "${host}/es/_cat/indices?v" &>> /home/docker/_import_last_week.log

curl -s localhost:9090/status &>> _status.log
echo '----------------------------------------------' &>> _status.log
