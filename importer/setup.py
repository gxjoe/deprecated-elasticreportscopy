from distutils.core import setup

setup(
    # Application name:
    name="ElasticStackGX",

    # Version number (initial):
    version="0.1.0",

    # Application author details:
    author="Jozef Sorocin",
    author_email="jozef.sorocin@g-experts.net",

    # Packages
    packages=["demo"],

    # Include additional files into the package
    include_package_data=True,

    # Details
    url="http://pypi.python.org/pypi/ElasticStackGX_v010/",

    #
    # license="LICENSE.txt",
    description="Useful towel-related stuff.",

    # long_description=open("README.txt").read(),

    # Dependent packages (distributions)
    install_requires=[
        "flask",
    ],
)