# Local environment setup

If you already have `pyenv-virtualenv` installed, jump to step 4.
 1. Install `pyenv-virtualenv`, using Homebrew:
   ```bash
   brew install pyenv-virtualenv
   ```

 2. Add the following lines to your shell profile file (should be `~/.bash_profile`, unless you changed your shell):
  ```
  eval "$(pyenv init -)"
  eval "$(pyenv virtualenv-init -)"
  ```

3. Restart your shell
4. Install Python 3.6.0. This is an isolated installation, independent of the system-wide one:
  ```
  pyenv install 3.6.0
  ```
  If this step fails because of a missing library, make sure you have `Xcode` installed (can be found on AppStore) and run the following:
  ```
  xcode-select --install
  ```

4. Navigate to your local copy of this repository
  ```
  cd demo
  ```

5. Create new virtual environment, using Python 3.6.0:
  ```
  pyenv virtualenv 3.6.0 demo
  ```

6. In order to enable automatic environment switching on entering the directory, run the following:
  ```bash
  pyenv local demo
  ```
  This will create a file called `.python-version`

7. install requirements
   ```
   pip install -Iv -r requirements.txt --user
   ```

8. In a separate terminal window, relocate to the ElasticSearch downloaded dir and start the ES server using
  ```
  bin/elasticsearch
  ```

9. Start flask server
  ```
  export FLASK_APP=main.py; export FLASK_DEBUG=1; python -m flask run
  ```

# Run KIBANA

1. Download kibana from https://www.elastic.co/downloads/kibana

2. Set `elasticsearch.url` to point at your Elasticsearch instance

3. Run ```bin/kibana``` and go to ```http://localhost:5601```


# Managed Compute Engine Cluster
1. Restart services
[https://docs.bitnami.com/google/apps/elk/]
```
sudo /opt/bitnami/ctlscript.sh restart (?apache)
```

2. Edit ES config yml
```
vi ~/stack/elasticsearch/config/elasticsearch.yml
```

3. Edit apache http conf
```
nano ~/stack/apache2/conf/bitnami/httpd.conf
nano ~/stack/apache2/conf/httpd.conf
```

4. Add apache headers
```
cd stack/apache2/conf
vi httpd.conf
```
add these right below Headers specifying XFRAME options (https://prnt.sc/lhbk02)
```
    Header always set Access-Control-Allow-Origin "*"
    Header set Access-Control-Allow-Headers "Authorization, Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers"
    Header set Access-Control-Request-Headers "Authorization, Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers"
    Header always set Access-Control-Allow-Methods "GET, POST, PATCH, PUT, DELETE, OPTIONS"
    
```
disable basic auth
```
vi /opt/bitnami/elasticsearch/apache-conf/elasticsearch.conf
```
and restart apache
```
sudo /opt/bitnami/ctlscript.sh restart apache
```


# Install SQL ES plugin
1. ```./bin/elasticsearch-plugin install https://github.com/NLPchina/elasticsearch-sql/releases/download/6.3.1.0/elasticsearch-sql-6.3.1.0.zip```

2. ```sudo /opt/bitnami/ctlscript.sh restart elasticsearch```

3. ```wget https://github.com/NLPchina/elasticsearch-sql/releases/download/5.4.1.0/es-sql-site-standalone.zip```

4. ```unzip es-sql-site-standalone.zip```

5. https://tecadmin.net/install-latest-nodejs-npm-on-debian

6. ```cd site-server```
  
```npm install express --save```

```node node-server.js```

# Docker & Kubernetes
1. ```docker build -t gcr.io/uniquacbnboost/cbn:1```
2. ```gcloud docker -- push gcr.io/uniquacbnboost/cbn-clust:1```
3. ```kubectl run cbn-deploy --image=gcr.io/uniquacbnboost/cbn:1```
4. ```kubectl create -f deployment.yml```