# -*- coding: utf-8 -*-
from __future__ import division

from datetime import datetime as dt
import json
import sys
import re
import logging

from xlsx_parser.parser import parse_files
from cbn_rest_fetch.connector import post_process_offers

from elastic.elastic import ElasticIndexObject, DEFAULT_MAPPING, NESTED_OBJECT_OF_50, EUM_PREPARED_MAPPING
from elastic.utils import successful_request, pp_ws, prepare_mapping, post_process_chunk, \
    dates_to_elastic_datetimes, prepare_address_centered_mapping, last_week_till_today

from kibana.dashboard_visualizations import DIAGRAM_TO_QUERY_MAPPING

from datetime import timedelta, datetime

from multiprocessing.dummy import Pool as ThreadPool
import copy

TOTAL_ADDRESS_COUNT_FILE_PATH = 'data/json/address_count.json'
_MAX_THREADS = 24
_SYNC_MONITORING_INDEX_NAME = 'sync_monitoring'


def split_date_range(start_date, end_date):
    s = None
    e = None
    to_ret = []
    for n in range(int((end_date - start_date).days)):
        if not s:
            s = start_date
        else:
            s += timedelta(1)

        if not e:
            e = start_date + timedelta(1)
        else:
            e += timedelta(1)

        to_ret.append((s, e))
    return to_ret


class Initializer(object):
    def __init__(self,
                 is_production,
                 es_index_name,
                 es_index_object,
                 bonsai_url=None):
        self.is_production = is_production
        self.es_index_name = es_index_name
        self.es_index_object = es_index_object
        self.bonsai_url = bonsai_url

    def populate(self,
                 return_mapping_and_index=None,
                 step=20,
                 start=None,
                 end=None,
                 threads=None,
                 sync_from_existing_index=None,
                 last_week_only=None,
                 ip=None):

        last_week_only = last_week_only == "true"

        if last_week_only:
            start_to_use, end_to_use = last_week_till_today(dt_only=True)
        else:
            syear, smonth, sday = tuple(
                map(int, (start or '2018/8/1').split('/')))
            eyear, emonth, eday = tuple(
                map(int, (end or '2018/8/31').split('/')))

            start_to_use = datetime(year=syear,
                                    month=smonth,
                                    day=sday,
                                    hour=0,
                                    minute=0,
                                    second=0)
            end_to_use = datetime(year=eyear,
                                  month=emonth,
                                  day=eday,
                                  hour=23,
                                  minute=59,
                                  second=59)

        pp_ws(dict(using_start=start_to_use, using_end=end_to_use))

        mapping_ = DEFAULT_MAPPING
        index = None

        if "user" in self.es_index_name:
            index = ElasticIndexObject(index_name=self.es_index_name,
                                       host=self.bonsai_url,
                                       do_not_delete=True)
        else:

            # batch_for_mapping = parse_files(rest_only=True, get_all=True, test=False, limit=1)
            mapping_['mappings']['entry'] = prepare_mapping(
                [copy.copy(NESTED_OBJECT_OF_50)], None,
                True)  # prepare_mapping(batch_for_mapping[0]['data'], 3)

            if "address" in self.es_index_name:
                mapping = prepare_address_centered_mapping(mapping_)
            else:
                mapping = mapping_

            index = ElasticIndexObject(index_name=self.es_index_name,
                                       mapping=mapping,
                                       host=self.bonsai_url,
                                       do_not_delete=True)
        # pp_ws(index)
        # pp_ws(ip)

        sync_monitoring_index = ElasticIndexObject(
            index_name=_SYNC_MONITORING_INDEX_NAME,
            host=self.bonsai_url,
            do_not_delete=True,
            kwargs={"ip": ip})

        if return_mapping_and_index:
            return mapping, index

        if not threads:
            threads = 1
        else:
            threads = int(threads)

        threads = min(threads, _MAX_THREADS)

        pool = ThreadPool(threads)
        logging.info('---------\n using %d threads...\n' % threads)

        steps = split_date_range(start_to_use, end_to_use)
        logging.info('using split range steps')

        def do_chunk(ch):
            if "users" not in self.es_index_name and "dev_" not in self.es_index_name:
                ch = post_process_chunk(ch)

            if len(ch) > 0:
                index.bulk_add(entries_=ch,
                               index_name=self.es_index_name,
                               sync_monitoring_index=sync_monitoring_index)

        def bulk_chunkify(data):
            orig_len = len(data)
            chunks = [data[ii:ii + step] for ii in range(0, len(data), step)]
            for ch in chunks:
                do_chunk(ch)

            logging.info(
                'bulk chunkify got len %d and split the payload to %d' %
                (orig_len, sum([len(ch) for ch in chunks])))
            # chunk_pool.map(do_chunk, chunks)

        def do_pool(_step_range, _sync_from_existing_index=None):
            to_assign = []

            if not _sync_from_existing_index:
                _start, _end = _step_range
                if not end:
                    # print('ERROR... no end for range:')
                    # print(_step_range)
                    return

                step_string = 'using step range: %s - %s' % (_start.strftime(
                    '%d.%m.%Y'), _end.strftime('%d.%m.%Y') if _end else '')

                logging.info(step_string)

                address_count, data_dict = parse_files(
                    rest_only=True,
                    get_all=True,
                    test=False,
                    limit=10000,
                    start=_start,
                    end=_end,
                    index_name=self.es_index_name)

                pp_ws(
                    dict(step_string=step_string,
                         address_count=address_count,
                         data_dict_len=len(data_dict)))

                tmp_json = json.dumps(data_dict)
                tmp_json = re.sub(r"content incl\. housekeeping",
                                  "content incl housekeeping", tmp_json)
                tmp_json = re.sub(r"surred", "sured", tmp_json)
                data_dict = json.loads(tmp_json)

                # when multi-threading, use I/O to keep current track
                # instead of global vars
                # tmp_count = read_file(TOTAL_ADDRESS_COUNT_FILE_PATH, True)
                # tmp_count['count'] += address_count
                # write_file(TOTAL_ADDRESS_COUNT_FILE_PATH, tmp_count, True)
                # time.sleep(1)

                for f in data_dict:
                    to_assign += f['data']

                bulk_chunkify(to_assign)

            else:
                _start, _end = None, None
                if _step_range:
                    _start, _end = _step_range
                    if not end and start:
                        # print('ERROR... no end for range:')
                        # print(_step_range)
                        return

                index_to_fetch_from = ElasticIndexObject(
                    index_name=_sync_from_existing_index,
                    host=self.bonsai_url,
                    do_not_delete=True)

                request_tuples = index_to_fetch_from.iteratively_prepare_all(
                    iteration_step=step,
                    last_week_only=last_week_only,
                    _start=_start,
                    _end=_end)

                print(request_tuples)

                for config in request_tuples:
                    # try:
                    pp_ws(config, space=1, depth=12)
                    method, index_name, params, body = config

                    docs = index_to_fetch_from.instance_.transport.perform_request(
                        method, index_name, params=params,
                        body=body)['hits']['hits']
                    all_docs = [hit['_source'] for hit in docs]

                    _, to_assign = post_process_offers(
                        self.es_index_name,
                        all_docs=all_docs,
                        offer_is_already_lob=True)
                    bulk_chunkify(to_assign)
                    # except Exception as SyncExcp:
                    #    logging.error('Syncing an address chunk for req these params failed')
                    #    pp_ws(config, space=1)
                    #    logging.error(SyncExcp)

            # bulk_chunkify(to_assign)

        def do_pool_from_existing(_step_range, ):
            return do_pool(_step_range, sync_from_existing_index)

        logging.info('--- STARTING SYNC ---')

        if not sync_from_existing_index:
            for i, _ in enumerate(pool.map(do_pool, steps), 1):
                sys.stderr.write('\rdone {0:%}\n'.format(i / len(steps)))

            pool.close()
            pool.join()
        else:
            # do_pool([start_to_use, end_to_use], sync_from_existing_index)

            for i, _ in enumerate(pool.map(do_pool_from_existing, steps), 1):
                sys.stderr.write('\rdone {0:%}\n'.format(i / len(steps)))

            pool.close()
            pool.join()

        logging.info('--- ENDING SYNC ---')

    def populate_users(self, array, users_index_name):
        index = ElasticIndexObject(index_name=users_index_name,
                                   mapping=DEFAULT_MAPPING,
                                   host=self.bonsai_url,
                                   do_not_delete=None)
        index.bulk_add(entries_=array)

    def query_download(self):
        index = self.es_index_object
        searched = index.brute_search(limit=200)
        with open(
                'data/json/es_dump_{0}_{1}.json'.format(
                    index.index_name,
                    dt.now().strftime('%d~%m~%H~%M')), 'w+') as f:
            f.write(json.dumps(searched))

        return successful_request()

    def repopulate_with_es_dump(self):
        index = self.es_index_object

        with open('data/json/anonymized.json') as f:
            dump = [elem for elem in json.loads(f.read())[0]]

        dump = dates_to_elastic_datetimes(dump)
        index.bulk_add(entries_=dump)

        print('using mapping:')
        pp_ws(json.dumps(self.es_index_object.get_mapping()))

        return successful_request()

    def query(self):
        responses = []

        for name, query in DIAGRAM_TO_QUERY_MAPPING.items():
            responses.append(
                {name: self.es_index_object.search(es_query=query)})

        return successful_request(stats=self.es_index_object.get_stats(),
                                  kibana_visualizations=responses)

    def __repr__(self):
        unicode_repr = '<%s at %s>: %s' % (type(self).__name__, hex(
            id(self)), 'Initializer Py Object')

        if sys.version_info[0] < 3:
            return unicode_repr.encode('utf-8')
        else:
            return unicode_repr
