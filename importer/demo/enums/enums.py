from enum import Enum


class Record(Enum):
    OFFER = 1
    USER = 2


class DateTimeFormats(Enum):
    ELASTIC_HUMAN_READABLE = (1, 'yyyy/MM/dd HH:mm:ss')
    PY_FULL_HUMAN_READABLE = (2, '%Y/%m/%d %H:%M:%S')
    ELASTIC_DATE_TIME_NO_MILLIS = (3, "yyyy-MM-dd'T'HH:mm:ssZZ")

    def __str__(self):
        return self.value[1]
