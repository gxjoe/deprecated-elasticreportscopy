import json
from pprint import pprint
import ndjson


def read_file(path, as_json=True):
    with open(path) as f:
        read_ = f.read()

        if as_json:
            try:
                return json.loads(read_)
            except Exception:
                return None

        return read_


def write_file(path, data, as_json=True, as_nd_json=None):
    if not as_nd_json:
        with open(path, 'wt+') as f:
            if not as_nd_json:
                f.write(json.dumps(data) if as_json else data)
    else:
        # dump to file-like objects
        with open(path, 'w') as f:
            ndjson.dump(data, f)


def pp(content, depth=4):
    pprint(content, indent=4, depth=depth)
