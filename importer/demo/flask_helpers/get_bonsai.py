from flask_helpers.is_production import is_production
import os


def get_bonsai_url():
    if is_production(True):
        return True, os.getenv('BONSAI_URL_LIVE')
    return False, os.getenv('BONSAI_URL_LOCALHOST')
