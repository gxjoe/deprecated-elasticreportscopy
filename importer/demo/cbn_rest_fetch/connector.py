import requests
from datetime import datetime as dt
import os
import json
import logging

import copy

from elastic.elastic import ElasticIndexObject

from common.utils import read_file, write_file, pp
from enums.enums import Record

from flask_helpers.get_bonsai import get_bonsai_url

_PAGINATION_LIMIT = 5
_TEST_DUMP_PATH = 'data/json/total_uniqua_users.json'


def dt_to_string(d):
    return d.strftime('%Y%m%d')


def ts_to_string(ts):
    return dt_to_string(dt.fromtimestamp(ts))


def construct_url(start=None, end=None, page=None, record_type=None):
    if record_type == Record.OFFER:
        if isinstance(start, dt):
            start = dt_to_string(start)
        elif isinstance(start, int):
            start = ts_to_string(start)

        if isinstance(end, dt):
            end = dt_to_string(end)
        elif isinstance(end, int):
            end = ts_to_string(end)

        return os.getenv('CBN_REST_OFFERS_ENDPOINT').format(start, end, page)
    else:
        return os.getenv('CBN_REST_USERS_ENDPOINT').format(page)


def perform_offer_request(start, end, page, test=None, ping=None):
    record_url = construct_url(start=start,
                               end=end,
                               page=page,
                               record_type=Record.OFFER)
    logging.info("connecting to %s " % record_url)

    if not test:
        r = requests.get(record_url, verify=False, timeout=60 * 30)
        try:
            json_ = r.json()
        except ValueError:
            logging.exception("connected: yes; json: no; %s " % record_url)
            return []

        if json_.get('success') and "data" in json_ and not json_.get('error'):
            if ping:
                return json_
            logging.info("connected? yes; %s ; size: %d" % (record_url, len(json_['data'])))

            return json_['data']
        else:
            logging.error('error retrieving from %s' % record_url,
                          response=json_)
            return []
    else:
        return read_file(_TEST_DUMP_PATH)


def perform_user_request(page, test=None, ping=None):
    record_url = construct_url(page=page, record_type=Record.USER)
    # print("connecting to %s " % record_url)

    if not test:
        r = requests.get(record_url, verify=False, timeout=60 * 30)
        json_ = r.json()
        if json_.get('success') and "data" in json_ and not json_.get('error'):
            if ping:
                return json_
            return json_['data']
        else:
            logging.error('error retrieving from %s' % record_url,
                          response=json_['error'])
            return []
    else:
        return read_file(_TEST_DUMP_PATH)


def fetch(start,
          end,
          page=1,
          limit=10,
          get_all=None,
          test=None,
          index_name=None):
    counter = 1
    responses = []
    address_count = 0

    limit = min(limit, _PAGINATION_LIMIT) if test else limit

    if get_all:
        if test:
            return perform_offer_request(start, end, page, test=test)

        total_count = perform_offer_request(start,
                                            end,
                                            page,
                                            test=test,
                                            ping=True)['stats']['totalCount']

        while counter < limit + 1:
            response_data = perform_offer_request(start, end, counter, test=test)
            if len(response_data) == 0:
                logging.info('Response data 0 at counter %d, total_count %d' % (counter, total_count))
                break
            responses += response_data
            counter += 1
            if len(responses) >= total_count:
                break

        address_count, new_responses = post_process_offers(
            index_name, responses, False)

        return address_count, new_responses

    return address_count, perform_offer_request(start, end, page, test=test)


def fetch_all_users(limit=10, get_all=None, test=None):
    counter = 1
    responses = []

    limit = min(limit, _PAGINATION_LIMIT) if test else limit

    if get_all:
        if test:
            return perform_user_request(page=counter, test=test)

        total_count = perform_user_request(page=1, test=test,
                                           ping=True)['stats']['totalCount']

        while counter < limit + 1:
            responses += perform_user_request(page=counter, test=test)
            counter += 1
            if len(responses) >= total_count:
                break

        write_file(path=_TEST_DUMP_PATH, data=responses, as_json=True)

        return responses

    return perform_user_request(page=counter, test=test)


def post_process_offers(index_name, all_docs, offer_is_already_lob=None):
    address_count = 0
    new_responses = []

    if not len(all_docs):
        return address_count, new_responses

    _, bonsai_url = get_bonsai_url()
    users_index = ElasticIndexObject(
        index_name=os.getenv('ES_INDEX_USERS_NAME'),
        host=bonsai_url,
        do_not_delete=True)

    logging.info('postprocessing offers begin...')

    for _, resp in enumerate(all_docs):
        if not offer_is_already_lob and not len(resp.get('lobs', [])):
            continue

        if not offer_is_already_lob:
            offer = resp
            lobs = offer['lobs']
            offer.pop('lobs')
        else:
            offer = resp['offer']
            lobs = [resp['lob']]

        if isinstance(offer['salesManagers'], list):
            tmp_sales_managers = offer['salesManagers']
            offer['salesManagers'] = []
            for _, sales_manager in enumerate(tmp_sales_managers):
                if not isinstance(sales_manager, (str, unicode, basestring)):
                    if isinstance(sales_manager, dict):
                        offer['salesManagers'].append(sales_manager)
                    continue

                user_by_username = users_index.get_by_param(
                    'username', sales_manager)
                if user_by_username:
                    offer['salesManagers'].append(user_by_username)
        else:
            offer['salesManagers'] = []

        for _, lob in enumerate(lobs):
            if not offer_is_already_lob:
                if len(lob['localUnderwriters']) and isinstance(
                        lob['localUnderwriters'][0], dict):
                    continue

                underwriter_copy = lob['localUnderwriters']
                lob['localUnderwriters'] = []

                for i_udw, udw in enumerate(underwriter_copy):
                    if not isinstance(udw, (str, unicode, basestring)):
                        continue

                    user_by_username = users_index.get_by_param(
                        'username', udw)
                    if user_by_username:
                        lob['localUnderwriters'].append(user_by_username)
                        # responses[i]['lobs'][i_lob]['localUnderwriters'][i_udw] = user_by_username
                    else:
                        logging.error('no full user (# %s) for offer %s' %
                                      (udw, offer['number']))
                        continue

                    for _ in (lob.get('addresses', []) or []):
                        address_count += 1

                new_responses.append({'offer': offer, 'lob': lob})
                continue

            tmp_addresses = copy.deepcopy(lob.get('addresses', []))

            if not tmp_addresses \
                or not isinstance(tmp_addresses, list) \
                    or len(tmp_addresses) == 0:
                continue

            if offer_is_already_lob and "addresses" in lob:
                del lob['addresses']

            address_count += len(tmp_addresses)

            for address in tmp_addresses:
                new_responses.append({
                    'address': address,
                    'lob': lob,
                    'offer': offer
                })

    logging.info('postprocessing offers end with count %d  to return ...' %
                 len(new_responses))

    return address_count, new_responses
