# -*- coding: utf-8 -*-

from .export_parser import parse_json_export_file


def parse_json_exports():
    json_export_file = parse_json_export_file()

    for vis in json_export_file:
        title, query = vis['_source']['title'], vis['_source']['params']
        print(title, query)


DIAGRAM_TO_QUERY_MAPPING = {
    "Total No. of Offers / LoB": {
        "size": 0,
        "_source": {
            "excludes": []
        },
        "aggs": {
            "2": {
                "terms": {
                    "field": "Line of Business (LoB)",
                    "size": 30,
                    "order": {
                        "_count": "desc"
                    }
                }
            }
        },
        "version": True,
        "stored_fields": [
            "*"
        ],
        "script_fields": {},
        "docvalue_fields": [],
        "query": {
            "bool": {
                "must": [
                    {
                        "match_all": {}
                    },
                    {
                        "match_all": {}
                    }
                ],
                "filter": [],
                "should": [],
                "must_not": []
            }
        },
        "highlight": {
            "pre_tags": [
                "@kibana-highlighted-field@"
            ],
            "post_tags": [
                "@/kibana-highlighted-field@"
            ],
            "fields": {
                "*": {}
            },
            "fragment_size": 2147483647
        }
    },
    "Total Premium / LoB": {
        "size": 0,
        "_source": {
            "excludes": []
        },
        "aggs": {
            "2": {
                "terms": {
                    "field": "Line of Business (LoB)",
                    "size": 20,
                    "order": {
                        "1": "desc"
                    },
                    "missing": "__missing__"
                },
                "aggs": {
                    "1": {
                        "sum": {
                            "field": "Premium"
                        }
                    }
                }
            }
        },
        "version": True,
        "stored_fields": [
            "*"
        ],
        "script_fields": {},
        "docvalue_fields": [],
        "query": {
            "bool": {
                "must": [
                    {
                        "match_all": {}
                    },
                    {
                        "match_all": {}
                    }
                ],
                "filter": [],
                "should": [],
                "must_not": [
                    {
                        "match_phrase": {
                            "Premium": {
                                "query": 0
                            }
                        }
                    }
                ]
            }
        },
        "highlight": {
            "pre_tags": [
                "@kibana-highlighted-field@"
            ],
            "post_tags": [
                "@/kibana-highlighted-field@"
            ],
            "fields": {
                "*": {}
            },
            "fragment_size": 2147483647
        }
    }
}
