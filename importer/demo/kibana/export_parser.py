# -*- coding: utf-8 -*-

import os
import json


def get_full_path(filename):
    return os.path.join(os.getcwd(), 'data/' + filename)


def parse_json_export_file():
    json_export_file = None

    for filename in os.listdir(os.path.join(os.getcwd(), 'data')):
        filename = get_full_path(filename)

        if not filename.endswith('export.json'):
            continue

        with open(filename) as f:
            json_export_file = json.loads(f.read())
            break

    return json_export_file
