# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import print_function
import logging
from elasticsearch import Elasticsearch
from elasticsearch import helpers

from datetime import datetime as dt

import json
import uuid
import sys
import math
import copy

from common.utils import read_file
from data.json.lob_centric_mapping import lob_centric_mapping

from .utils import pp, last_week_till_today
from enums.enums import DateTimeFormats

DEFAULT_MAPPING = {
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 0,
        "analysis": {
            "filter": {
                "word_joiner": {
                    "type": "word_delimiter",
                    "concatenate_all": True
                }
            },
            "tokenizer": {
                "comma_tokenizer": {
                    'type': 'pattern',
                    'pattern': ', '
                }
            },
            'analyzer': {
                'comma_analyzer': {
                    'tokenizer': 'comma_tokenizer'
                },
                'autocomplete_analyzer': {
                    "type": "custom",
                    'tokenizer': 'keyword',
                    'filter': ["lowercase", "word_joiner"]
                },
                "my_analyzer": {
                    "type": "custom",
                    "filter": ["lowercase"],
                    "tokenizer": "whitespace"
                }
            }
        },
        "index": {
            "max_inner_result_window": 100000,
            "max_result_window": 500000,
            "mapping": {
                # "ignore_malformed": True,
                "nested_fields": {
                    "limit": 30000
                },
                "total_fields": {
                    "limit": 50000
                }
            },
            "refresh_interval": "1s"  # "60s"
        }
    },
    "mappings": {
        "entry": {
            "properties": {},
            "_field_names": {
                "enabled": False
            },
        }
    }
}

NESTED_OBJECT_OF_50 = copy.copy(lob_centric_mapping)

EUM_PREPARED_MAPPING = json.loads(
    '{"riskDescription":{"type":"text","null_value":""},"number":{"type":"text","null_value":""},"regionalDirectorate":{"type":"text","null_value":""},"publicTender":{"type":"boolean","null_value":false},"uicbInvolved":{"type":"boolean","null_value":false},"uicbConfirmNeeded":{"type":"boolean","null_value":false},"id":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"coordinates":{"type":"geo_point"},"regionalDirectorate2":{"type":"text","null_value":""},"draft":{"type":"boolean","null_value":false},"coinsured":{"type":"text","null_value":""},"cbOrSme":{"type":"text","null_value":""},"internationalProgram":{"type":"boolean","null_value":false},"crossBorderBusiness":{"type":"boolean","null_value":false},"createdDate":{"type":"date","format":"yyyy/MM/dd HH:mm:ss"},"salesManagers":{"type":"text","null_value":""},"issuanceDate":{"type":"date","format":"yyyy/MM/dd HH:mm:ss"},"lobs":{"dynamic":true,"type":"nested","properties":{"totalSumInsured":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"addresses":{"dynamic":true,"type":"nested","properties":{"city":{"type":"text","null_value":""},"locationProperty":{"dynamic":true,"type":"nested","properties":{"numberOfStories":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"basementFloors":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"buildingType":{"type":"text","null_value":""},"locationAssessments":{"dynamic":true,"type":"nested","properties":{"coordinates":{"type":"geo_point"},"peril":{"type":"text","null_value":""},"id":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"riskClass":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0}}},"coordinates":{"type":"geo_point"},"shapeOfBuilding":{"type":"text","null_value":""},"numberOfBuildings":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"yearOfConstruction":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"roofGeometry":{"type":"text","null_value":""},"constructionMaterial":{"type":"text","null_value":""},"id":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"roofSystem":{"type":"text","null_value":""}}},"ratingValues":{"type":"text","null_value":""},"country":{"type":"text","null_value":""},"region":{"type":"text","null_value":""},"geoPoint":{"dynamic":true,"type":"nested","properties":{"latitude":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"coordinates":{"type":"geo_point"},"id":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"floatitude":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0}}},"zipCode":{"type":"text","null_value":""},"coordinates":{"type":"geo_point"},"street":{"type":"text","null_value":""},"sumInsured":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"houseNumber":{"type":"text","null_value":""},"id":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0}}},"premiumEur":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"share":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"currency":{"type":"text","null_value":""},"coordinates":{"type":"geo_point"},"id":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"reasonForDeclining":{"type":"text","null_value":""},"typeOfBusiness":{"type":"text","null_value":""},"newPolicyNumber":{"type":"text","null_value":""},"riskClass":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"localUnderwriters":{"dynamic":true,"type":"nested","properties":{"username":{"type":"text","null_value":""},"city":{"type":"text","null_value":""},"countryCode":{"type":"text","null_value":""},"roles":{"type":"text","null_value":""},"coordinates":{"type":"geo_point"},"address":{"type":"text","null_value":""},"department":{"type":"text","null_value":""},"organization":{"type":"text","null_value":""},"fullName":{"type":"text","null_value":""},"id":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0}}},"status":{"type":"text","null_value":""},"premium":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"uniqaLeading":{"type":"boolean","null_value":false},"totalSumInsuredEur":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"exchangeRate":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"premiumPrevious":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"lineOfBusiness":{"type":"text","null_value":""},"salesPartner":{"dynamic":true,"type":"nested","properties":{"agentId":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"name":{"type":"text","null_value":""},"coordinates":{"type":"geo_point"},"agentNumber":{"type":"text","null_value":""}}},"risks":{"dynamic":true,"type":"nested","properties":{"risk":{"type":"text","null_value":""},"perils":{"dynamic":true,"type":"nested","properties":{"riskDescription":{"type":"text","null_value":""},"id":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"coordinates":{"type":"geo_point"}}},"coordinates":{"type":"geo_point"},"premiumBasis":{"type":"text","null_value":""},"premiumExclTax":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"sumInsured":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"id":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0}}},"totalPML":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0}}},"distributionChannel":{"type":"text","null_value":""},"industry":{"type":"text","null_value":""},"requestFromCountry":{"type":"text","null_value":""},"updatedDate":{"type":"date","format":"yyyy/MM/dd HH:mm:ss"},"client":{"dynamic":true,"type":"nested","properties":{"city":{"type":"text","null_value":""},"premium":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"name":{"type":"text","null_value":""},"GVpremium":{"type":"text","null_value":""},"country":{"type":"text","null_value":""},"region":{"type":"text","null_value":""},"revenueInEuro":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"zipCode":{"type":"text","null_value":""},"coordinates":{"type":"geo_point"},"nacePrimaryCodes":{"type":"text","null_value":""},"tradeRegisterNumber":{"type":"text","null_value":""},"street":{"type":"text","null_value":""},"houseNumber":{"type":"text","null_value":""},"id":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"naceSecondaryCodes":{"type":"text","null_value":""}}},"premiumInEur":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"salesPartner":{"dynamic":true,"type":"nested","properties":{"agentId":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"name":{"type":"text","null_value":""},"coordinates":{"type":"geo_point"},"agentNumber":{"type":"text","null_value":""}}},"internalInfo":{"dynamic":true,"type":"nested","properties":{"offerLobId":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"created":{"type":"date","format":"yyyy/MM/dd HH:mm:ss"},"offerId":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"state":{"type":"text","null_value":""},"manualStateChangeDate":{"type":"date","format":"yyyy/MM/dd HH:mm:ss"},"coordinates":{"type":"geo_point"},"createdBy":{"type":"text","null_value":""},"action":{"type":"text","null_value":""},"responseTimeInSec":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"id":{"ignore_malformed":true,"coerce":false,"type":"float","null_value":0},"previousState":{"type":"text","null_value":""}}}}'
)


class ElasticIndexObject(object):
    def __init__(self,
                 index_name=None,
                 instance_=None,
                 mapping=None,
                 host=None,
                 port=None,
                 do_not_delete=None,
                 kwargs={}):
        self.es_query = None
        self.geojson_resources = None
        self.kwargs = kwargs

        if not mapping:
            mapping = DEFAULT_MAPPING

        if not instance_:

            if not host:
                self.instance_ = Elasticsearch(hosts=[{
                    "host": host or "127.0.0.1",
                    "port": port or 9200
                }])
            else:
                self.instance_ = Elasticsearch([host])

        self.index_name = index_name

        if mapping is not None:  # host and "localhost" in host and
            if do_not_delete is None:
                self.delete_index()

        # ignore status code 400 (index already exists)
        try:
            self.index_ = self.instance_.indices.get(index=index_name)
        except Exception as e:
            logging.warn('index {0} could not be gotten'.format(index_name))
            logging.warn(self.instance_)
            try:
                self.index_ = self.instance_.indices.create(
                    index=index_name, body=mapping)
            except Exception as excp:
                raise Exception(excp)

    def __repr__(self):
        unicode_repr = '<%s at %s>: %s' % (type(self).__name__, hex(
            id(self)), str(self.index_name))

        if sys.version_info[0] < 3:
            return unicode_repr.encode('utf-8')
        else:
            return unicode_repr

    def get_ip(self):
        return self.kwargs.get('ip')

    def get_instance(self):
        return self.instance_

    def get_mapping(self):
        return self.instance_.transport.perform_request('GET',
                                                        '/{0}/_mapping'.format(
                                                            self.index_name),
                                                        params=None,
                                                        body=None)

    def set_mapping(self, mapping):
        return self.instance_.transport.perform_request('PUT',
                                                        '/{0}/entry'.format(
                                                            self.index_name),
                                                        params=None,
                                                        body=mapping)

    def set_property_search_query(self, es_query=None, **kwargs):
        if not es_query:
            logging.info('no es query but got kwargs...')
            pp(**kwargs)

            must = []
            filter_arr = []
            must_not = []
            sort_ = []

            es_query = {
                "query": {
                    "bool": {
                        "must": must,
                        "filter": filter_arr,
                        "must_not": must_not
                    }
                },
                "sort": sort_
            }

        self.es_query = es_query

    def run(self):
        return self.instance_.search(body=self.es_query)

    def search(self, es_query=None, **params):
        resp = dict()

        if not isinstance(params, dict):
            try:
                params = json.loads(params)
            except ValueError as err:
                logging.error(err)

        self.set_property_search_query(es_query, **params)

        properties = self.run()

        hits, aggregations = properties.get('hits').get(
            'hits'), properties.get('aggregations')

        resp['original_query'] = dict(es_query=es_query, **params)
        resp.update(hits=hits, aggregations=aggregations)
        return resp

    def add_object(self, obj, id_=None):
        self.instance_.index(index=self.index_name,
                             doc_type='entry',
                             id=id_ or uuid.uuid1(),
                             body=obj)

    """
    @ POPULATING METHODS
    """

    def populate(self, to_push):
        self.bulk_add(entries_=to_push, index_name=self.index_name)

    """
    @ entry METHODS
    """

    def list_all_indexes(self):
        return self.instance_.transport.perform_request('GET',
                                                        '/_all',
                                                        params=None,
                                                        body=None)

    def get_mapping(self):
        return self.instance_.transport.perform_request(
            'GET',
            '/%s/_mapping' % self.index_name,
        )

    def get_stats(self, body=None):
        if body:
            body = {'query': body}
        info_json = self.instance_.transport.perform_request(
            'GET', '/{0}'.format(self.index_name), params=None, body=body)

        basic_search = self.instance_.transport.perform_request(
            'GET',
            '/{0}/_search'.format(self.index_name),
            params=None,
            body=body)

        stats_json = self.instance_.transport.perform_request(
            'GET',
            '/{0}/_stats'.format(self.index_name),
            params=None,
            body=body)
        return dict(created_ts_ms=info_json[
            self.index_name]['settings']['index']['creation_date'],
                    hits_count=basic_search['hits']['total'] if not isinstance(
                        basic_search['hits']['total'],
                        dict) else basic_search['hits']['total']['value'],
                    count=stats_json['indices'][
                        self.index_name]['total']['docs']['count'],
                    size_bytes=stats_json['indices'][self.index_name]['total']
                    ['store']['size_in_bytes'])

    def get_by_param(self, x_param, x_val):
        resp = self.instance_.transport.perform_request(
            'GET',
            '/_search',
            params=None,
            body={
                "query": {
                    "bool": {
                        "must": [{
                            "term": {
                                x_param: {
                                    "value": x_val
                                }
                            }
                        }]
                    }
                }
            })
        if resp['hits']['total'] > 0:
            return resp['hits']['hits'][0]['_source']
        else:
            return None

    def iteratively_prepare_all(self,
                                iteration_step=10e3,
                                last_week_only=None,
                                _start=None,
                                _end=None):
        list_of_request_tuples = []

        def start_of_day(dtime):
            return dt(year=dtime.year,
                      month=dtime.month,
                      day=dtime.day,
                      hour=0,
                      minute=0,
                      second=0)

        def end_of_day(dtime):
            return dt(year=dtime.year,
                      month=dtime.month,
                      day=dtime.day,
                      hour=23,
                      minute=59,
                      second=59)

        last_week_only_query = None

        if not last_week_only:
            if not _start and not _end:
                last_week_only_query = {"match_all": {}}
            else:
                last_week_only_query = {
                    'bool': {
                        'should': [{
                            'nested': {
                                "path": "offer",
                                "query": {
                                    "range": {
                                        "offer.updatedDate": {
                                            "format":
                                            str(DateTimeFormats.
                                                ELASTIC_HUMAN_READABLE),
                                            "gte":
                                            start_of_day(_start).strftime(
                                                str(DateTimeFormats.
                                                    PY_FULL_HUMAN_READABLE)),
                                            "lte":
                                            end_of_day(_end).strftime(
                                                str(DateTimeFormats.
                                                    PY_FULL_HUMAN_READABLE))
                                        }
                                    }
                                }
                            }
                        }, {
                            'nested': {
                                "path": "offer",
                                "query": {
                                    "range": {
                                        "offer.createdDate": {
                                            "format":
                                            str(DateTimeFormats.
                                                ELASTIC_HUMAN_READABLE),
                                            "gte":
                                            start_of_day(_start).strftime(
                                                str(DateTimeFormats.
                                                    PY_FULL_HUMAN_READABLE)),
                                            "lte":
                                            end_of_day(_end).strftime(
                                                str(DateTimeFormats.
                                                    PY_FULL_HUMAN_READABLE))
                                        }
                                    }
                                }
                            }
                        }]
                    }
                }
        else:
            gte, lte = last_week_till_today()
            last_week_only_query = {
                'bool': {
                    'should': [{
                        'nested': {
                            "path": "offer",
                            "query": {
                                "range": {
                                    "offer.updatedDate": {
                                        "format":
                                        str(DateTimeFormats.
                                            ELASTIC_HUMAN_READABLE),
                                        "gte":
                                        gte,
                                        "lte":
                                        lte
                                    }
                                }
                            }
                        }
                    }, {
                        'nested': {
                            "path": "offer",
                            "query": {
                                "range": {
                                    "offer.createdDate": {
                                        "format":
                                        str(DateTimeFormats.
                                            ELASTIC_HUMAN_READABLE),
                                        "gte":
                                        gte,
                                        "lte":
                                        lte
                                    }
                                }
                            }
                        }
                    }]
                }
            }

        total_count = self.get_stats(body=last_week_only_query)['hits_count']
        full_needed_pages = int(math.floor(total_count / iteration_step))

        for a in range(0, full_needed_pages):
            logging.info(
                '\tpreparing lob batch GET requests...\t\tNeed to iterate over %d results; current offset is %d'
                % (total_count, a * iteration_step))

            list_of_request_tuples.append(
                ('GET', '/%s/_search' % self.index_name, None, {
                    "from": a * iteration_step,
                    "query": last_week_only_query,
                    "size": iteration_step
                }))

        list_of_request_tuples.append(
            ('GET', '/%s/_search' % self.index_name, None, {
                "from": full_needed_pages * iteration_step,
                "query": last_week_only_query,
                "size": iteration_step
            }))

        # pp('list of request tuples')
        # pp(list_of_request_tuples, depth=20)

        return list_of_request_tuples

    def brute_search(self, limit=500, page=0, only_ids=None):
        body = {"from": page, "size": limit}

        if only_ids:
            body.update(
                dict(query={"ids": {
                    "type": "entry",
                    "values": only_ids
                }}))

        resp = self.instance_.transport.perform_request('GET',
                                                        '/{0}/_search'.format(
                                                            self.index_name),
                                                        params=None,
                                                        body=body)
        return resp['hits']['hits'], None

    @staticmethod
    def chunks(l, n):
        n = max(1, n)
        return (l[i:i + n] for i in range(0, len(l), n))

    def bulk_monitor(self, errors=[]):
        if not len(errors):
            return

        monitoring_actions = []

        for single_error in errors:
            doc_id = single_error['index']['_id']
            caused_by = json.dumps(single_error['index']['error']['caused_by'])
            reason = single_error['index']['error']['reason']
            success = False

            monitoring_actions.append({
                "_index":
                self.index_name,
                "_type":
                "entry",
                "_source":
                dict(doc_id=doc_id,
                     caused_by=caused_by,
                     reason=reason,
                     success=success,
                     ip=self.get_ip(),
                     timestamp=dt.now(),
                     doc_type="lob")
            })
        helpers.bulk(self.instance_, monitoring_actions)

    def bulk_add(self,
                 entries_,
                 type_='entry',
                 index_name=None,
                 sync_monitoring_index=None):
        all_entries = list(self.chunks(entries_, 200))
        logging.info('bulk add got len %d' % len(entries_))
        logging.info(
            'for perf reasons they got split into a parent list of len %d' %
            len(all_entries))
        logging.info('using index %s' % index_name or self.index_name)

        for entries in all_entries:
            actions = []

            for j in entries:
                _index_name = index_name or self.index_name

                if "users" not in _index_name:
                    if "address" in _index_name:
                        j['id'] = int(j['address']['id'])
                        # j['id'] = "address_%s_lob_%s" % (str(j.get('address').get('id')), str(j.get('lob').get('id')))
                    else:
                        j['id'] = int(j['lob']['id'])
                        # j['id'] = "lob_%s" % str(j.get('lob').get('id'))

                actions.append({
                    "_index": _index_name,
                    "_type": type_ or "entry",
                    "_id": j['id'],
                    "_source": j
                })

            logging.info('trying to sync object IDs:')
            logging.info([entry['_id'] for entry in actions])
            if sync_monitoring_index:
                logging.info('ip')
                logging.info(sync_monitoring_index.get_ip())

            try:
                logging.info('starting bulk add')
                resp = helpers.bulk(self.instance_, actions)
                logging.info('successfully synced chunk of len %d' %
                             len(actions))
                logging.info(resp)

            except helpers.BulkIndexError as err:
                logging.exception('ERROR BulkIndexError while bulk_adding')

                sync_monitoring_index.bulk_monitor(err.errors)

                raise Exception(json.dumps(err.errors))
            except Exception as err:
                logging.exception('ERROR Generic Exception while bulk_adding')
                raise err

    """""" """""" """""
    @@             @@
    @  DANGER ZONE  @
    @@             @@
    """ """""" """""" ""

    def delete_by_id(self, uuid_):
        self.instance_.delete(self.index_name, "entry", uuid_)

    def delete_index(self):
        try:
            self.instance_.transport.perform_request('DELETE',
                                                     '/%s' % self.index_name,
                                                     params=None,
                                                     body={})
        except Exception as ex:
            logging.warn(ex)

    def delete_all(self):
        """
        try:
            self.instance_.transport.perform_request('DELETE',
                                                     '/_all',
                                                     params=None, body=None)
        except Exception:
        """
        self.instance_.transport.perform_request(
            'POST',
            '/%s/entry/_delete_by_query' % self.index_name,
            params=None,
            body={"query": {
                "match_all": {}
            }})
