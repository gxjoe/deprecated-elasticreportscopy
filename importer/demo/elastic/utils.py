from __future__ import division
import copy

from flask import jsonify
import re
from pprint import pprint
import json
import sys

from datetime import datetime, timedelta
from dateutil.parser import isoparser
from dateutil.parser import parse as dateutil_parse

from xlsx_parser.geocoder import geocode_address

from enums.enums import DateTimeFormats

PRICE_REGEX = re.compile(r'^-?\d+(.?\d+?)(,\d{1,2})?$')
DATE_REGEX = re.compile(
    r'^(?:(?:31(/|-|.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(/|-|.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(/|-|.)0?2\3?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))$|^(?:0?[1-9]|1\d|2[0-8])(/|-|.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$'
)
ISO_DATE_REGEX = re.compile(
    r'^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}\s?\+\d{4}$')
ISO_DATE_REGEX_PARTIAL = re.compile(
    r'\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}\s?\+\d{4}$')
ISO_DATE_REGEX_STRICT = re.compile(
    r'^\d{4,}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}\s?\+\d{4}$')
CURRENCY_REGEX = re.compile(r'eur|premium|tsi|eml|pml|sumInsured',
                            re.IGNORECASE)

list_of_currencies = [
    "BAM", "BGN", "CZK", "EUR", "HRK", "HUF", "PLN", "RON", "RSD", "UAH", "USD"
]


def is_bool(b):
    return isinstance(b, bool)


def is_list(l):
    return isinstance(l, list)


def is_dict(d):
    return isinstance(d, dict)


def parse_float(inp):
    if isinstance(inp, int):
        return inp

    return float(inp.replace(',', ''))


def invalid_request(reason, code=402, success=False, **kwargs):
    return jsonify(success=success,
                   error=dict(code=code, reason=reason, **kwargs)), code


def successful_request(code=200, success=True, **kwargs):
    return jsonify(code=code, success=success, **kwargs), code


def prepare_mapping(items, use_row_no=None, no_doublecheck=True):
    longest_item = {}

    if not use_row_no or len(items) <= use_row_no:
        for item in items:
            if len(longest_item.keys()) < len(item.keys()):
                longest_item = item
    else:
        longest_item = items[use_row_no]

    if no_doublecheck:
        return longest_item

    to_return = dict()

    for name, value in longest_item.items():
        if is_list(value) and len(value) > 0 and is_dict(value[0]):
            # value = value[0]
            print('will nest')
            to_return[name] = {
                'type': 'nested',
                'properties': {},
                'dynamic': True
            }
            to_return[name]['properties'] = prepare_mapping(value)

            print(name)
            pp_ws(to_return[name])
            continue

        if isinstance(value, dict):
            print('will nest')
            to_return[name] = {
                'type': 'nested',
                'properties': {},
                'dynamic': True
            }
            to_return[name]["properties"] = prepare_mapping([value])
            continue

        if isinstance(value, bool):
            to_return[name] = {"type": "boolean", "null_value": False}
            continue

        if not is_list(value) and \
                not is_dict(value) and \
                (isinstance(value, (int, float, long)) or
                 (isinstance(value, str) and PRICE_REGEX.match(value.encode('utf-8')))):

            if isinstance(value, int):
                to_return[name] = {
                    "type": "float",
                    "coerce": False,
                    "null_value": 0,
                    "ignore_malformed": True
                }
                print('getting an integery match')
                print(name)
                print(value)
                print('\n\n')
            else:
                to_return[name] = {
                    "type": "float",
                    "coerce": False,
                    "null_value": 0.0,
                    "ignore_malformed": True
                }
                print('getting an floaty match')
                print(name)
                print(value)
                print('\n\n')
            continue
        """
        if not isinstance(value, str) and isinstance(value, long):
            to_return[name] = {
                "type": "long"
            }
            continue
        """

        if "%" in name or "percent" in name.lower():
            to_return[name] = {"type": "integer"}
            continue

        if value and not is_list(value) and not is_dict(
                value) and ISO_DATE_REGEX.match(value):
            to_return[name] = {
                "type": "date",
                "format": str(DateTimeFormats.ELASTIC_HUMAN_READABLE)
            }
            continue

        if "coordinates" in name:
            to_return[name] = {
                "type": "geo_point",
                "ignore_malformed": True,
                # "null_value": "0,0"
            }
            continue

        to_return[name] = {
            "type": "keyword",
            "null_value": ""
            # "analyzer": "my_analyzer"
        }

    # to_return['coordinates'] = {
    #     "type": "geo_point"
    # }

    # pp_ws('to_return')
    # pp_ws(to_return)
    return to_return


def prepare_address_centered_mapping(lob_centered_mapping):
    address_centered_mapping = copy.deepcopy(lob_centered_mapping)

    address_centered_mapping['mappings']['entry']['properties']['address'] = \
        address_centered_mapping['mappings']['entry']['properties']['lob']['properties']['addresses']
    del address_centered_mapping['mappings']['entry']['properties']['lob'][
        'properties']['addresses']

    return address_centered_mapping


def has_adjusted_value_for_key(k, parent):
    return '_AdjustedForShare' in k or (k + '_AdjustedForShare') in parent


def iso_dt_to_human_date(iso):
    try:
        parsed = isoparser('T').isoparse(iso).strftime(
            str(DateTimeFormats.PY_FULL_HUMAN_READABLE))
    # handle ValueError: Unused components in ISO string
    except ValueError:
        try:
            parsed = dateutil_parse(iso).strftime(
                str(DateTimeFormats.PY_FULL_HUMAN_READABLE))
        except ValueError as verr2:
            print('datetime parsing error! iso input: ' + str(iso))
            print(verr2)
            return None
    except Exception:
        parsed = None

    return parsed


def last_week_till_today(dt_only=None):
    today = datetime.today()
    today = datetime(year=today.year,
                     month=today.month,
                     day=today.day,
                     hour=23,
                     minute=59,
                     second=59)

    week_ago = today - timedelta(days=7)
    week_ago = datetime(year=week_ago.year,
                        month=week_ago.month,
                        day=week_ago.day,
                        hour=0,
                        minute=0,
                        second=0)

    if dt_only:
        return week_ago, today

    return datetime.strftime(
        week_ago,
        str(DateTimeFormats.PY_FULL_HUMAN_READABLE)), datetime.strftime(
            today, str(DateTimeFormats.PY_FULL_HUMAN_READABLE))


def primitively_valid_coordinates(dict_):
    return "coordinates" in dict_ and len(str(dict_['coordinates'])) > 0


def construct_address(elem):
    return u"{0} {1}, {2}, {3} {4}, {5}".format(
        elem.get('street', '') or '',
        elem.get('houseNumber', '') or '',
        elem.get('city', '') or '',
        elem.get('zipCode', '') or '',
        elem.get('region', '') or '',
        elem.get('country', '') or '')


def is_not_none(entity):
    return entity is not None


def traverse_dict_of_interest(item, entry, failed_items, uniqa_share=None):
    # first assign all price-like entries because some of them will be useful in the next loops
    uniqa_share = float(uniqa_share or ((entry or {}).get('share', -1) / 100)
                        or -1)

    for k, v in entry.items():
        # convert everything to float
        if isinstance(v, (int, float, long)) and not is_bool(v):
            if float(v) == 0.0:
                # print('\n\nwill need to change value to FLOAT\n\n')
                entry[k] = 0.0
            else:
                entry[k] = float(v)

    for k, v in entry.items():
        if v and isinstance(
                v, (str, basestring)) and ISO_DATE_REGEX_PARTIAL.search(v):
            if not ISO_DATE_REGEX.match(v):
                del entry[k]
            else:
                entry[k] = iso_dt_to_human_date(v)

        if CURRENCY_REGEX.search(k) \
                and is_not_none(v) \
                and not has_adjusted_value_for_key(k, entry) \
                and isinstance(v, (int, float, long)) \
                and uniqa_share >= 0:
            entry[k + '_AdjustedForShare'] = v * uniqa_share

        if k == "industry" and v and isinstance(v, list) and len(v):
            entry[k] = [v[0]]  # take the first industry

        if (k == "rating" or k == "address") and v and isinstance(v, dict):
            for rk, rv in v.items():

                if isinstance(entry, dict) \
                        and is_not_none(rv) \
                        and CURRENCY_REGEX.search(rk) \
                        and not has_adjusted_value_for_key(rk, entry[k]) \
                        and isinstance(rv, (int, float, long)) \
                        and uniqa_share >= 0:
                    entry[k][rk + '_AdjustedForShare'] = rv * uniqa_share

        if k in ["rating", "risks"] and v and isinstance(v, dict):
            for rk, rv in v.items():
                for currency in list_of_currencies:
                    if CURRENCY_REGEX.search(rk) \
                            and uniqa_share >= 0 \
                            and v.get("totalPremium_%s_AdjustedForShare" % currency) \
                            and v.get("mTotalPremium_%s_AdjustedForShare" % currency):
                        entry[k + ('_%s_adjustedPremiumsRatio' % currency)] = (
                            v.get("totalPremium_%s_AdjustedForShare" %
                                  currency) /
                            v.get("mTotalPremium_%s_AdjustedForShare" %
                                  currency)) * 100

                if rk == "ratingToolPerils":
                    for rating_peril_key, rating_peril_obj in rv.items():
                        for peril_key, peril_val in rating_peril_obj.items():
                            if CURRENCY_REGEX.search(peril_key) \
                                    and is_not_none(peril_val) \
                                    and entry.get('share', -1) >= 0 \
                                    and not has_adjusted_value_for_key(peril_key, entry[k][rk][rating_peril_key]):
                                entry[k][rk][rating_peril_key][
                                    peril_key +
                                    '_AdjustedForShare'] = peril_val * uniqa_share

                if rk == "insuredObjects":
                    for io_key, io_obj in rv.items():
                        for type_key, type_val in io_obj.items():
                            if CURRENCY_REGEX.search(type_key) \
                                    and is_not_none(type_val) \
                                    and entry.get('share', -1) >= 0 \
                                    and not has_adjusted_value_for_key(type_key, entry[k][rk][io_key]):
                                entry[k][rk][io_key][
                                    type_key +
                                    '_AdjustedForShare'] = type_val * uniqa_share

        if isinstance(v, list) and k == "addresses":
            for index, address_item in enumerate(v):

                if primitively_valid_coordinates(address_item):
                    continue

                address = construct_address(address_item)
                coordinates = geocode_address(address)

                if coordinates != "0,0":
                    entry[k][index]['coordinates'] = coordinates
                else:
                    failed_items.append(item.get('id', "N/A"))
                    continue

            for index, address_item in enumerate(v):
                for a_key, a_item in address_item.items():
                    if isinstance(a_item, dict):
                        for locationPropertyIndex, locationPropertyItem in a_item.items(
                        ):
                            if isinstance(
                                    locationPropertyItem,
                                (int, float,
                                 long)) and not is_bool(locationPropertyItem):
                                if float(locationPropertyItem) == 0.0:
                                    entry[k][index][a_key][
                                        locationPropertyIndex] = 0.0
                                else:
                                    entry[k][index][a_key][
                                        locationPropertyIndex] = float(
                                            locationPropertyItem)

                        entry[k][index][a_key] = traverse_dict_of_interest(
                            entry, a_item, failed_items, uniqa_share)

        if isinstance(v, list) and k in ["risks"]:
            for index, risk_item in enumerate(v):
                for r_key, k_val in risk_item.items():

                    if isinstance(k_val,
                                  (int, float, long)) and not is_bool(k_val):
                        if float(k_val) == 0.0:
                            entry[k][index][r_key] = 0.0
                        else:
                            entry[k][index][r_key] = float(k_val)

                entry[k][index] = traverse_dict_of_interest(
                    entry, risk_item, failed_items, uniqa_share)

                for r_key, k_val in risk_item.items():
                    if isinstance(k_val, list):
                        for risk_peril_index, risk_peril_item in enumerate(
                                k_val):
                            if isinstance(risk_peril_item, dict):
                                entry[k][index][r_key][
                                    risk_peril_index] = traverse_dict_of_interest(
                                        entry, risk_peril_item, failed_items,
                                        uniqa_share)

                if isinstance(risk_item, dict):
                    entry[k][index] = traverse_dict_of_interest(
                        entry, risk_item, failed_items, uniqa_share)

    for k, v in entry.items():
        if k == "rating" and v \
                and isinstance(v, object) \
                and len(v.keys()):
            if v.get('brokerCommission', -1) >= 0 \
                    and v.get('mTotalPremium_AdjustedForShare'):
                if "commissionedMPremiumAdjustedForShare" not in entry[k]:
                    entry[k]['commissionedMPremium_AdjustedForShare'] = (
                        v.get('brokerCommission', 0) /
                        100) * v['mTotalPremium_AdjustedForShare']

            for currency in list_of_currencies:
                if v.get('superScore', -1) >= 0 \
                        and isinstance(entry.get('totalSumInsured_' + currency + '_AdjustedForShare'),
                                       (int, long, float)) \
                        and "superScoredTotalSumInsured_" + currency + "_AdjustedForShare" not in v:
                    entry[k]['superScoredTotalSumInsured_' + currency +
                             '_AdjustedForShare'] = (
                                 v.get('superScore', 0) /
                                 100) * entry['totalSumInsured_' + currency +
                                              '_AdjustedForShare']

    return entry


def progress(count, total, status=''):
    bar_len = 40
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
    sys.stdout.flush(
    )  # As suggested by Rom Ruben (see: http://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console/27871113#comment50529068_27871113)


def post_process_chunk(data):
    failed_items = []
    finished_arr = []

    for ind, item in enumerate(data):
        # progress(ind + 1, len(data), status='postprocessing chunk of length %d' % len(data))
        uniqa_share = (item or {}).get('lob', {}).get('share', -1) / 100

        for key_, val_ in item.items():
            if isinstance(val_, dict):
                item[key_] = traverse_dict_of_interest(item, val_,
                                                       failed_items)

            if isinstance(val_, list) and len(val_) > 0:
                for list_index, list_item in enumerate(val_):
                    if isinstance(list_item, dict):
                        item[key_][list_index] = traverse_dict_of_interest(
                            item, list_item, failed_items, uniqa_share)

                    if isinstance(list_item, list):
                        for inner_list_item_index, inner_list_item in enumerate(
                                list_item):
                            if isinstance(inner_list_item, dict):
                                item[key_][list_index][
                                    inner_list_item_index] = traverse_dict_of_interest(
                                        item, inner_list_item, failed_items,
                                        uniqa_share)

            if isinstance(val_, dict):
                for key, val in val_.items():
                    # if "id" in key.lower() and key + '_kwd' not in item[key_].keys():
                    #   item[key_][key + '_kwd'] = str(int(float(val)))

                    if CURRENCY_REGEX.search(key) \
                            and is_not_none(val) \
                            and not has_adjusted_value_for_key(key, item[key_]) \
                            and (uniqa_share or item.get('lob', {}).get('share', -1)) >= 0 \
                            and isinstance(val, (int, long, float)):
                        for currency in list_of_currencies:
                            if currency in key:
                                item[key_][
                                    key +
                                    '_AdjustedForShare'] = val * uniqa_share

                    if val and isinstance(
                            val,
                        (str,
                         basestring)) and ISO_DATE_REGEX_PARTIAL.search(val):
                        if not ISO_DATE_REGEX.match(val):
                            del val_[key]

                    if isinstance(val, dict) and "zipCode" in val.keys():
                        if not primitively_valid_coordinates(val):
                            address = construct_address(val)
                            coordinates = geocode_address(address)

                            if coordinates != "0,0":
                                item[key_][key]['coordinates'] = coordinates

                    if isinstance(val, list):
                        for i, entry in enumerate(val):
                            if isinstance(entry, dict):
                                item[key_][key][i] = traverse_dict_of_interest(
                                    item, entry, failed_items, uniqa_share)

            finished_arr.append(item)

    with open('failed_accounts_to_geocode', 'w+') as f:
        f.write(json.dumps(failed_items))

    return finished_arr


def dates_to_elastic_datetimes(dump):
    nw = []
    for element in dump:
        copy_ = element
        for k, v in element.items():
            if "date" in k.lower() and len(
                    k.split(' ')) == 2 and len(str(v)) > 0:
                if DATE_REGEX.match(str(v)):
                    # no need for parsing; already in %d.%m format
                    continue

                elif ISO_DATE_REGEX.match(str(v)):
                    copy_[k] = iso_dt_to_human_date(v)

        nw.append(copy_)
    return nw or dump


def pp(content, depth=8):
    pprint(content, indent=4, depth=depth)


def pp_ws(content, depth=8, space=3):
    print('\n' * space)
    pp(content, depth=depth)
    print('\n' * space)
