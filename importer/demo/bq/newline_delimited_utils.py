# -*- coding: utf-8 -*-

import re
from common.utils import pp


def json_to_ndjson(list_obj):
    # TODO
    cleaned_up = []

    for item in list_obj:
        obj = dict()
        for k, v in item['_source'].items():
            key = re.sub('[^A-Za-z0-9]+', '_', k)
            match = re.search("u\'(.+?)'", repr(v))

            str_ = match.group(1) if match else repr(v)

            if str_ == "u''":
                str_ = ""

            obj[key] = str_

        cleaned_up.append(obj)

    pp(cleaned_up[0].keys())
    return cleaned_up
