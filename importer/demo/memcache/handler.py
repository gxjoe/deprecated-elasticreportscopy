from pymemcache.client import base
import os
import hashlib
import logging


class MemCache(object):
    def __init__(self):
        self.client = base.Client((os.getenv('MEM_CACHE_ENDPOINT'),
                                   int(os.getenv('MEM_CACHE_PORT'))))

    @staticmethod
    def hash_str(s):
        return str(int(hashlib.sha1(s).hexdigest(), 16) % (10**10))

    def do_get(self, some_key):
        return self.client.get(some_key)

    def do_set(self, some_key, some_val, duration=60 * 60):
        try:
            self.client.set(some_key, some_val, expire=duration)
        except Exception as DoSetException:
            logging.error(DoSetException)
            pass
