# -*- coding: utf-8 -*-

from flask import Flask, url_for, request, render_template, redirect
from flask_cors import cross_origin, CORS

import os
import urllib
from dotenv import load_dotenv, find_dotenv
from elastic.elastic import ElasticIndexObject, DEFAULT_MAPPING
from elastic.utils import successful_request
from xlsx_parser.parser import parse_files
from common.utils import read_file, pp

import re

import requests
import json
import sys
import msgpack

from cbn_rest_fetch.connector import fetch_all_users
from flask_helpers.get_bonsai import get_bonsai_url

from classes.Initializer import Initializer
import urllib3
from urlparse import urljoin

from memcache.handler import MemCache

import logging
logging.basicConfig(
    format='%(asctime)s %(process)4d %(levelname)s - %(message)s',
    level=logging.INFO)

app = Flask(__name__, template_folder='templates')
app.url_map.strict_slashes = False
load_dotenv(find_dotenv())

POPULATE = True

DOWNLOAD = False
REPOPULATE = False

ES_INDEX_NAME = os.getenv('ES_COLLECTION_NAME')
ES_INDEX_USERS_NAME = os.getenv('ES_INDEX_USERS_NAME')
ES_ADDRESS_CENTERED_COLLECTION_NAME = os.getenv(
    'ES_ADDRESS_CENTERED_COLLECTION_NAME')

urllib3.disable_warnings()


class RelativeSession(requests.Session):
    def __init__(self, base_url):
        super(RelativeSession, self).__init__()
        self.__base_url = base_url

    def request(self, method, url, **kwargs):
        url = urljoin(self.__base_url, url)
        return super(RelativeSession, self).request(method, url, **kwargs)


@app.route('/')
def for_base():
    output = []
    for rule in app.url_map.iter_rules():
        options = {}
        for arg in rule.arguments:
            options[arg] = "[{0}]".format(arg)

        methods = ','.join(rule.methods)
        url = url_for(rule.endpoint, **options)
        line = urllib.unquote("{:50s} {:20s} {}".format(
            rule.endpoint, methods, url))
        output.append(line)

    return successful_request(paths=sorted(output))


@app.route('/html', methods=['GET', 'POST'])
def for_html():
    name = request.args.get('name')
    s = request.args.get('start')
    e = request.args.get('end')
    lob_index_name = request.args.get('lob_index_name')
    last_week_only = request.args.get(
        'last_week_only') != "None" and request.args.get(
            'last_week_only') is not None

    pp(request.args)

    if (s and e) or last_week_only:
        if "address" in name:
            url = "/populate_addresses/rest?start=%s&end=%s&threads=12&index_name=%s&lob_index_name=%s&last_week_only=%s" % (
                s, e, name, lob_index_name, last_week_only)
        else:
            url = "/populate/rest?start=%s&end=%s&threads=12&index_name=%s&last_week_only=%s" % (
                s, e, name, last_week_only)

        if "uniqa-international" in request.base_url:
            url = os.getenv('UNIQA_SYNC_URL_PATH_PREFIX') + url

        return redirect(url)

    return render_template('index.html')


@app.route('/fetch_all_users')
def for_fetch_all_users():
    files = fetch_all_users(limit=30000, get_all=True, test=False)
    return successful_request(data=files)


@app.route('/populate/rest')
def for_populate_with_rest():
    prod, bonsai_url = get_bonsai_url()

    start = request.args.get('start')
    end = request.args.get('end')
    threads = request.args.get('threads')
    index_name = request.args.get('index_name')
    last_week_only = request.args.get('last_week_only')

    initializer = Initializer(is_production=prod,
                              es_index_name=index_name or ES_INDEX_NAME,
                              es_index_object=None,
                              bonsai_url=bonsai_url)

    ip = request.remote_addr

    initializer.populate(step=500,
                         start=start,
                         end=end,
                         threads=threads,
                         last_week_only=last_week_only,
                         ip=ip)
    return successful_request()


@app.route('/populate_addresses/rest')
def for_populate_addresses_rest():
    prod, bonsai_url = get_bonsai_url()

    start = request.args.get('start')
    end = request.args.get('end')
    threads = request.args.get('threads')
    index_name = request.args.get('index_name')
    lob_based_index_name = request.args.get('lob_index_name')
    last_week_only = request.args.get('last_week_only')

    initializer = Initializer(is_production=prod,
                              es_index_name=index_name
                              or ES_ADDRESS_CENTERED_COLLECTION_NAME,
                              es_index_object=None,
                              bonsai_url=bonsai_url)

    initializer.populate(step=500,
                         start=start,
                         end=end,
                         threads=threads,
                         sync_from_existing_index=lob_based_index_name,
                         last_week_only=last_week_only)
    return successful_request()


@app.route('/populate/users')
def for_populate_with_users():
    prod, bonsai_url = get_bonsai_url()
    files = [{
        'name':
        'users',
        'data':
        read_file('data/json/total_uniqua_users.json', as_json=True)
    }]
    initializer = Initializer(is_production=prod,
                              es_index_name=ES_INDEX_USERS_NAME,
                              es_index_object=None,
                              bonsai_url=bonsai_url)

    initializer.populate_users(files[0]['data'], ES_INDEX_USERS_NAME)
    return successful_request()


@app.route('/populate/csv')
def for_populate_with_csv():
    prod, bonsai_url = get_bonsai_url()
    address_count, files = parse_files(csv_only=True,
                                       file_names=["export_multitable.csv"])
    initializer = Initializer(is_production=prod,
                              es_index_name=None,
                              es_index_object=None,
                              bonsai_url=bonsai_url)

    initializer.populate(files)


@app.route('/query')
def for_query():
    prod, bonsai_url = get_bonsai_url()
    index = ElasticIndexObject(index_name='dev_cbn1_list', host=bonsai_url)
    initializer = Initializer(is_production=prod,
                              es_index_name=None,
                              es_index_object=index)
    return initializer.query()


@app.route('/query_download')
def query_download():
    prod, bonsai_url = get_bonsai_url()
    index = ElasticIndexObject(index_name='dev_cbn1_list', host=bonsai_url)
    initializer = Initializer(is_production=prod,
                              es_index_name=None,
                              es_index_object=index)
    return initializer.query_download()


@app.route('/repopulate_with_es_dump')
def for_repopulate_with_es_dump():
    prod, bonsai_url = get_bonsai_url()

    mapping = DEFAULT_MAPPING
    mapping['mappings']['entry']['properties'] = read_file(
        'data/json/live_demo_mapping.json')

    index = ElasticIndexObject(index_name='dev_data',
                               host=bonsai_url,
                               mapping=mapping)
    initializer = Initializer(is_production=prod,
                              es_index_name=None,
                              es_index_object=index)
    return initializer.repopulate_with_es_dump()


@app.route('/bq')
def bq():
    from bq.newline_delimited_utils import json_to_ndjson
    from common.utils import read_file, write_file

    data = read_file('data/json/es_dump_23~07~11~11.json')
    data = json_to_ndjson(data[0][:5000])
    write_file('data/bq/test_dump.json', data, as_nd_json=True)

    return successful_request()


@app.route('/memcached_wrapper', methods=['POST'])
@cross_origin()
def for_memcached_wrapper():
    mem_client = MemCache()

    context = request.get_json()
    query_key = context['query_key']
    url = context['url']
    headers = context['headers']
    payload = context['payload']
    expire = context['expire']

    try:
        cached = mem_client.do_get(query_key)
    except Exception:
        cached = None

    if cached:
        return successful_request(200,
                                  True,
                                  response=msgpack.unpackb(cached),
                                  cached=True,
                                  query_key=query_key)
    else:
        if "ndjson" in headers['Content-Type']:
            payload = ('\n'.join(
                re.sub(r'\\n', ' ', d) for d in payload.split('\n')
                if len(d) > 0)).encode('utf-8') + '\n'
        else:
            payload = json.dumps(payload)

        r = requests.post(url, data=payload, headers=headers)

        if 200 <= r.status_code <= 300:
            dumped = msgpack.packb(r.json())
            if sys.getsizeof(dumped) <= 5e6:
                mem_client.do_set(query_key, dumped, expire)
            else:
                pp('dumped %d' % sys.getsizeof(dumped))
        else:
            pp(r.json())
        return successful_request(r.status_code,
                                  True,
                                  response=r.json(),
                                  cached=False,
                                  query_key=query_key)


@app.route('/memcached_wrapper/flush')
def for_memcached_wrapper_flush():
    mem_client = MemCache()

    mem_client.client.flush_all()
    return successful_request()


if __name__ == '__main__':
    app.run(threaded=True, debug=True)
