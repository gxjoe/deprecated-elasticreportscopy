keyCriteriaItself = {
    "type": "nested",
    "dynamic": True,
    "properties": {
        "asIfScore": {
            "type": "float",
            "ignore_malformed": True,
            "coerce": False,
            "null_value": 0
        },
        "name": {
            "type": "text",
            "fields": {
                "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                }
            }
        },
        "negativeAspects": {
            "type": "nested",
            "dynamic": True,
            "properties": {
                "recommendations": {
                    "type": "nested",
                    "dynamic": True,
                    "properties": {
                        "recommendations": {
                            "type": "nested",
                            "dynamic": True,
                            "properties": {
                                "description": {
                                    "type": "text",
                                    "fields": {
                                        "keyword": {
                                            "type": "keyword",
                                            "ignore_above": 256
                                        }
                                    }
                                },
                                "name": {
                                    "type": "text",
                                    "fields": {
                                        "keyword": {
                                            "type": "keyword",
                                            "ignore_above": 256
                                        }
                                    }
                                },
                                "aspect": {
                                    "type": "text",
                                    "fields": {
                                        "keyword": {
                                            "type": "keyword",
                                            "ignore_above": 256
                                        }
                                    }
                                },
                                "validFrom": {
                                    "type": "text",
                                    "fields": {
                                        "keyword": {
                                            "type": "keyword",
                                            "ignore_above": 256
                                        }
                                    }
                                }
                            }
                        },
                        "selected": {
                            "type": "boolean"
                        }
                    }
                }
            }
        },
        "positiveAspects": {
            "type": "nested",
            "dynamic": True,
            "properties": {
                "description": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "name": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "recommendations": {
                    "type": "nested",
                    "dynamic": True,
                    "properties": {
                        "description": {
                            "type": "text",
                            "fields": {
                                "keyword": {
                                    "type": "keyword",
                                    "ignore_above": 256
                                }
                            }
                        },
                        "name": {
                            "type": "text",
                            "fields": {
                                "keyword": {
                                    "type": "keyword",
                                    "ignore_above": 256
                                }
                            }
                        },
                        "validFrom": {
                            "type": "text",
                            "fields": {
                                "keyword": {
                                    "type": "keyword",
                                    "ignore_above": 256
                                }
                            }
                        },
                        "validTo": {
                            "type": "text",
                            "fields": {
                                "keyword": {
                                    "type": "keyword",
                                    "ignore_above": 256
                                }
                            }
                        }
                    }
                },
                "validFrom": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "validTo": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                }
            }
        },
        "recommendations": {
            "type": "nested",
            "dynamic": True,
            "properties": {
                "priority": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "recommendationSequence": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "scenario": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "statusFeedback": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "assessmentRecommendation": {
                    "type": "nested",
                    "dynamic": True,
                    "properties": {
                        "description": {
                            "type": "text",
                            "fields": {
                                "keyword": {
                                    "type": "keyword",
                                    "ignore_above": 256
                                }
                            }
                        },
                        "name": {
                            "type": "text",
                            "fields": {
                                "keyword": {
                                    "type": "keyword",
                                    "ignore_above": 256
                                }
                            }
                        },
                        "validFrom": {
                            "type": "text",
                            "fields": {
                                "keyword": {
                                    "type": "keyword",
                                    "ignore_above": 256
                                }
                            }
                        }
                    }
                }
            }
        },
        "score": {
            "type": "float",
            "ignore_malformed": True,
            "coerce": False,
            "null_value": 0
        },
        "weight": {
            "type": "float",
            "ignore_malformed": True,
            "coerce": False,
            "null_value": 0
        },
        "type": {
            "type": "text",
            "fields": {
                "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                }
            }
        }
    }
}

localUnderwritersNested = {
    "type": "nested",
    "dynamic": True,
    "properties": {
        "address": {
            "type": "keyword",
            "null_value": ""
        },
        "city": {
            "type": "keyword",
            "null_value": ""
        },
        "countryCode": {
            "type": "keyword",
            "null_value": ""
        },
        "department": {
            "type": "keyword",
            "null_value": ""
        },
        "fullName": {
            "type": "keyword",
            "null_value": ""
        },
        "id": {
            "type": "float",
            "ignore_malformed": True,
            "coerce": False,
            "null_value": 0
        },
        "organization": {
            "type": "keyword",
            "null_value": ""
        },
        "roles": {
            "type": "keyword",
            "null_value": ""
        },
        "username": {
            "type": "keyword",
            "null_value": ""
        }
    }
}

dynamic_template_mappings = dict(
    ratingToolPerils={
        "type": "nested"
    },
    insuredObjects={
        "type": "nested"
    },
    keyCriteria=keyCriteriaItself,
    keyFactors={
        "type": "nested",
        "dynamic": True,
        "properties": {
            "asIfScore": {
                "type": "float",
                "ignore_malformed": True,
                "coerce": False,
                "null_value": 0
            },
            "averageScore": {
                "type": "float",
                "ignore_malformed": True,
                "coerce": False,
                "null_value": 0
            },
            "score": {
                "type": "float",
                "ignore_malformed": True,
                "coerce": False,
                "null_value": 0
            },
            "type": {
                "type": "text",
                "fields": {
                    "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                    }
                }
            },
            "weight": {
                "type": "float",
                "ignore_malformed": True,
                "coerce": False,
                "null_value": 0
            },
            "keyCriteriaAsList": keyCriteriaItself
        }
    },
    level3={
        "type": "nested",
        "dynamic": True,
        "properties": {
            "keyFactors": {
                "type": "nested",
                "dynamic": True
            },
            "peril": {
                "type": "text",
                "fields": {
                    "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                    }
                }
            },
            "riskClass": {
                "type": "float",
                "ignore_malformed": True,
                "coerce": False,
                "null_value": 0
            },
            "totalScore": {
                "type": "float",
                "ignore_malformed": True,
                "coerce": False,
                "null_value": 0
            }
        }
    }
)

lob_centric_mapping = {
    "dynamic_templates": [
        {
            "ratingToolPerils": {
                "mapping": dynamic_template_mappings['ratingToolPerils'],
                "match_mapping_type": "object",
                "path_match": "lob.rating.ratingToolPerils.*"
            }
        },
        {
            "insuredObjects": {
                "mapping": dynamic_template_mappings['insuredObjects'],
                "match_mapping_type": "object",
                "path_match": "lob.risks.insuredObjects.*"
            }
        },

        {
            "keyCriteria": {
                "mapping": dynamic_template_mappings['keyCriteria'],
                "match_mapping_type": "object",
                "path_match": "lob.addresses.locationProperty.locationAssessmentsLevel3.*.keyFactors.*.keyCriteria.*"
            }
        },
        {
            "keyCriteria_address": {
                "mapping": dynamic_template_mappings['keyCriteria'],
                "match_mapping_type": "object",
                "path_match": "address.locationProperty.locationAssessmentsLevel3.*.keyFactors.*.keyCriteria.*"
            }
        },

        {
            "keyFactors": {
                "mapping": dynamic_template_mappings['keyFactors'],
                "match_mapping_type": "object",
                "path_match": "lob.addresses.locationProperty.locationAssessmentsLevel3.*.keyFactors.*"
            }
        },
        {
            "keyFactors_address": {
                "mapping": dynamic_template_mappings['keyFactors'],
                "match_mapping_type": "object",
                "path_match": "address.locationProperty.locationAssessmentsLevel3.*.keyFactors.*"
            }
        },

        {
            "level3": {
                "mapping": dynamic_template_mappings['level3'],
                "match_mapping_type": "object",
                "path_match": "lob.addresses.locationProperty.locationAssessmentsLevel3.*"
            }
        },
        {
            "level3_address": {
                "mapping": dynamic_template_mappings['level3'],
                "match_mapping_type": "object",
                "path_match": "address.locationProperty.locationAssessmentsLevel3.*"
            }
        }
    ],
    "properties": {
        "lob": {
            "type": "nested",
            "dynamic": True,
            "properties": {
                "accountStatus": {
                    "type": "keyword",
                    "null_value": ""
                },
                "addresses": {
                    "type": "nested",
                    "dynamic": True,
                    "properties": {
                        "city": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "coordinates": {
                            "type": "geo_point",
                            "ignore_malformed": True
                        },
                        "country": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "geoPoint": {
                            "type": "nested",
                            "dynamic": True,
                            "properties": {
                                "coordinates": {
                                    "type": "geo_point",
                                    "ignore_malformed": True
                                },
                                "id": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                },
                                "latitude": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                },
                                "longitude": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                }
                            }
                        },
                        "houseNumber": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "id": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "locationProperty": {
                            "type": "nested",
                            "dynamic": True,
                            "properties": {
                                "EML": {
                                    "type": "long"
                                },
                                "PML": {
                                    "type": "long"
                                },
                                "basementFloors": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                },
                                "buildingType": {
                                    "type": "keyword",
                                    "null_value": ""
                                },
                                "constructionMaterial": {
                                    "type": "keyword",
                                    "null_value": ""
                                },
                                "id": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                },
                                "locationAssessments": {
                                    "type": "nested",
                                    "dynamic": True,
                                    "properties": {
                                        "adjustedScore": {
                                            "type": "float"
                                        },
                                        "id": {
                                            "type": "float",
                                            "ignore_malformed": True,
                                            "coerce": False,
                                            "null_value": 0
                                        },
                                        "level": {
                                            "type": "long"
                                        },
                                        "peril": {
                                            "type": "keyword",
                                            "null_value": ""
                                        },
                                        "riskClass": {
                                            "type": "float",
                                            "ignore_malformed": True,
                                            "coerce": False,
                                            "null_value": 0
                                        },
                                        "score": {
                                            "type": "float"
                                        }
                                    }
                                },
                                "locationAssessmentsLevel3": {
                                    "type": "nested",
                                    "dynamic": True,
                                    "properties": {

                                    }
                                },
                                "numberOfBuildings": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                },
                                "numberOfStories": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                },
                                "roofGeometry": {
                                    "type": "keyword",
                                    "null_value": ""
                                },
                                "roofSystem": {
                                    "type": "keyword",
                                    "null_value": ""
                                },
                                "shapeOfBuilding": {
                                    "type": "keyword",
                                    "null_value": ""
                                },
                                "yearOfConstruction": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                }
                            }
                        },
                        "ratingValues": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "region": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "street": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "sumInsured": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "tsiPerLocation": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "premiumBasisPerLocation": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "zipCode": {
                            "type": "keyword",
                            "null_value": ""
                        }
                    }
                },
                "beginDate": {
                    "type": "date",
                    "format": "yyyy/MM/dd HH:mm:ss||yyyy/MM/dd||epoch_millis"
                },
                "currency": {
                    "type": "keyword",
                    "null_value": ""
                },
                "endDate": {
                    "type": "date",
                    "format": "yyyy/MM/dd HH:mm:ss||yyyy/MM/dd||epoch_millis"
                },
                "exchangeRate": {
                    "type": "float"
                },
                "id": {
                    "type": "float",
                    "ignore_malformed": True,
                    "coerce": False,
                    "null_value": 0
                },
                "lineOfBusiness": {
                    "type": "keyword",
                    "null_value": ""
                },
                "localUnderwriters": localUnderwritersNested,
                "newPolicyNumber": {
                    "type": "keyword",
                    "null_value": ""
                },
                "policyIssuance": {
                    "type": "date",
                    "format": "yyyy/MM/dd HH:mm:ss||yyyy/MM/dd||epoch_millis"
                },
                "premium": {
                    "type": "float",
                    "ignore_malformed": True,
                    "coerce": False,
                    "null_value": 0
                },
                "premiumEur": {
                    "type": "float",
                    "ignore_malformed": True,
                    "coerce": False,
                    "null_value": 0
                },
                "premiumEur_AdjustedForShare": {
                    "type": "float"
                },
                "premiumPrevious": {
                    "type": "float",
                    "ignore_malformed": True,
                    "coerce": False,
                    "null_value": 0
                },
                "rating": {
                    "type": "nested",
                    "dynamic": True,
                    "properties": {
                        "adjustedPremiumsRatio": {
                            "type": "float"
                        },
                        "brokerCommission": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "commissionedMPremium_AdjustedForShare": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "deductiblePerils": {
                            "type": "nested",
                            "dynamic": True,
                            "properties": {
                                "id": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                },
                                "peril": {
                                    "type": "keyword",
                                    "null_value": ""
                                },
                                "risks": {
                                    "type": "keyword",
                                    "null_value": ""
                                },
                                "type": {
                                    "type": "keyword",
                                    "null_value": ""
                                },
                                "value": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                }
                            }
                        },
                        "id": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "limitPerils": {
                            "type": "nested",
                            "dynamic": True,
                            "properties": {
                                "id": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                },
                                "peril": {
                                    "type": "keyword",
                                    "null_value": ""
                                },
                                "risks": {
                                    "type": "keyword",
                                    "null_value": ""
                                },
                                "type": {
                                    "type": "keyword",
                                    "null_value": ""
                                },
                                "value": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                }
                            }
                        },
                        "mTotalPremium": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "mTotalPremium_AdjustedForShare": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "perilCalculations": {
                            "type": "nested",
                            "dynamic": True,
                            "properties": {
                                "id": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                },
                                "mTotalPremium": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                },
                                "peril": {
                                    "type": "keyword",
                                    "null_value": ""
                                },
                                "totalPremium": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                }
                            }
                        },
                        "ratingToolPerils": {
                            "type": "nested",
                            "dynamic": True,
                            "properties": {
                            }
                        },
                        "coveredPerils": {
                            "type": "nested",
                            "dynamic": True,
                            "properties": {
                                "id": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                },
                                "avgRiskClass": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                },
                                "avgAsIfScore": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                },
                                "peril": {
                                    "type": "keyword",
                                    "null_value": ""
                                },
                                "avgScore": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                }
                            }
                        },
                        "personalDeviationAuthority": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "status": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "superScore": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "totalPremium": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "totalPremium_AdjustedForShare": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "versionNumber": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        }
                    }
                },
                "reasonForDeclining": {
                    "type": "keyword",
                    "null_value": ""
                },
                "reasonForDecline": {
                    "type": "keyword",
                    "null_value": ""
                },
                "riskClass": {
                    "type": "float",
                    "ignore_malformed": True,
                    "coerce": False,
                    "null_value": 0
                },
                "risks": {
                    "type": "nested",
                    "dynamic": True,
                    "properties": {
                        "id": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "perils": {
                            "type": "nested",
                            "dynamic": True,
                            "properties": {
                                "id": {
                                    "type": "float",
                                    "ignore_malformed": True,
                                    "coerce": False,
                                    "null_value": 0
                                },
                                "riskDescription": {
                                    "type": "keyword",
                                    "null_value": ""
                                }
                            }
                        },
                        "premiumBasis": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "premiumExclTax": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "risk": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "sumInsured": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "insuredObjects": {
                            "type": "nested",
                            "dynamic": True
                        }
                    }
                },
                "salesPartner": {
                    "type": "nested",
                    "dynamic": True,
                    "properties": {
                        "agentId": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "agentNumber": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "name": {
                            "type": "keyword",
                            "null_value": ""
                        }
                    }
                },
                "share": {
                    "type": "float",
                    "ignore_malformed": True,
                    "coerce": False,
                    "null_value": 0
                },
                "status": {
                    "type": "keyword",
                    "null_value": ""
                },
                "totalPML": {
                    "type": "float",
                    "ignore_malformed": True,
                    "coerce": False,
                    "null_value": 0
                },
                "totalSumInsured": {
                    "type": "float",
                    "ignore_malformed": True,
                    "coerce": False,
                    "null_value": 0
                },
                "totalSumInsuredEur": {
                    "type": "float",
                    "ignore_malformed": True,
                    "coerce": False,
                    "null_value": 0
                },
                "totalSumInsuredEur_AdjustedForShare": {
                    "type": "float"
                },
                "typeOfBusiness": {
                    "type": "keyword",
                    "null_value": ""
                },
                "uniqaPML": {
                    "type": "float"
                }
            }
        },
        "offer": {
            "type": "nested",
            "dynamic": True,
            "properties": {
                "cbOrSme": {
                    "type": "keyword",
                    "null_value": ""
                },
                "client": {
                    "type": "nested",
                    "dynamic": True,
                    "properties": {
                        "GVpremium": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "city": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "coordinates": {
                            "type": "geo_point",
                            "ignore_malformed": True
                        },
                        "country": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "houseNumber": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "id": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "nacePrimaryCodes": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "naceSecondaryCodes": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "name": {
                            "type": "keyword",
                            "null_value": "",
                            "fields": {
                                "as_text": {
                                    "type": "text"
                                }
                            }
                        },
                        "premium": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "region": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "revenueInEuro": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "street": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "tradeRegisterNumber": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "zipCode": {
                            "type": "keyword",
                            "null_value": ""
                        }
                    }
                },
                "coinsured": {
                    "type": "nested",
                    "dynamic": True,
                    "properties": {
                        "GVpremium": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "city": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "country": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "houseNumber": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "id": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "nacePrimaryCodes": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "naceSecondaryCodes": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "name": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "premium": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "region": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "revenueInEuro": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "street": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "tradeRegisterNumber": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "zipCode": {
                            "type": "keyword",
                            "null_value": ""
                        }
                    }
                },
                "createdDate": {
                    "type": "date",
                    "format": "yyyy/MM/dd HH:mm:ss"
                },
                "crossBorderBusiness": {
                    "type": "boolean",
                    "null_value": False
                },
                "distributionChannel": {
                    "type": "keyword",
                    "null_value": ""
                },
                "draft": {
                    "type": "boolean",
                    "null_value": False
                },
                "id": {
                    "type": "float",
                    "ignore_malformed": True,
                    "coerce": False,
                    "null_value": 0
                },
                "industry": {
                    "type": "keyword",
                    "null_value": ""
                },
                "internalInfo": {
                    "type": "nested",
                    "dynamic": True,
                    "properties": {
                        "action": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "created": {
                            "type": "date",
                            "format": "yyyy/MM/dd HH:mm:ss"
                        },
                        "createdBy": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "id": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "manualStateChangeDate": {
                            "type": "date",
                            "format": "yyyy/MM/dd HH:mm:ss"
                        },
                        "offerId": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "offerLobId": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "previousState": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "responseTimeInSec": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "state": {
                            "type": "keyword",
                            "null_value": ""
                        }
                    }
                },
                "internationalProgram": {
                    "type": "boolean",
                    "null_value": False
                },
                "issuanceDate": {
                    "type": "keyword",
                    "null_value": ""
                },
                "number": {
                    "type": "keyword",
                    "null_value": ""
                },
                "premiumInEur": {
                    "type": "float",
                    "ignore_malformed": True,
                    "coerce": False,
                    "null_value": 0
                },
                "regionalDirectorate": {
                    "type": "keyword",
                    "null_value": ""
                },
                "regionalDirectorate2": {
                    "type": "keyword",
                    "null_value": ""
                },
                "requestFromCountry": {
                    "type": "keyword",
                    "null_value": ""
                },
                "responseTimes": {
                    "type": "nested",
                    "dynamic": True,
                    "properties": {
                        "u057240": {
                            "type": "long"
                        },
                        "u091314": {
                            "type": "long"
                        },
                        "u092502": {
                            "type": "long"
                        },
                        "u918797": {
                            "type": "long"
                        },
                        "u922690": {
                            "type": "long"
                        },
                        "v00basiq": {
                            "type": "long"
                        },
                        "vab1302": {
                            "type": "long"
                        },
                        "vacag04": {
                            "type": "long"
                        },
                        "vacch04": {
                            "type": "long"
                        },
                        "vaceb01": {
                            "type": "long"
                        },
                        "van9813": {
                            "type": "long"
                        },
                        "van9841": {
                            "type": "long"
                        },
                        "vaq4442": {
                            "type": "long"
                        },
                        "vaxdenk": {
                            "type": "long"
                        },
                        "vbabm71": {
                            "type": "long"
                        },
                        "vbakm27": {
                            "type": "long"
                        },
                        "vbama13": {
                            "type": "long"
                        },
                        "vbamm11": {
                            "type": "long"
                        },
                        "vbani02": {
                            "type": "long"
                        },
                        "vbasa0b": {
                            "type": "long"
                        },
                        "vbash30": {
                            "type": "long"
                        },
                        "vbavoa01": {
                            "type": "long"
                        },
                        "vbays02": {
                            "type": "long"
                        },
                        "vbf2692": {
                            "type": "long"
                        },
                        "vbgvgta1": {
                            "type": "long"
                        },
                        "vbgvkd01": {
                            "type": "long"
                        },
                        "vbgvkm01": {
                            "type": "long"
                        },
                        "vbgvkm02": {
                            "type": "long"
                        },
                        "vbgvmsv1": {
                            "type": "long"
                        },
                        "vbgvsm02": {
                            "type": "long"
                        },
                        "vbgvzvt1": {
                            "type": "long"
                        },
                        "vbh1625": {
                            "type": "long"
                        },
                        "vch5628": {
                            "type": "long"
                        },
                        "vchvba01": {
                            "type": "long"
                        },
                        "vcx7957": {
                            "type": "long"
                        },
                        "vdr7290": {
                            "type": "long"
                        },
                        "vea4888": {
                            "type": "long"
                        },
                        "vee0505": {
                            "type": "long"
                        },
                        "veg4388": {
                            "type": "long"
                        },
                        "veg5626": {
                            "type": "long"
                        },
                        "ven5102": {
                            "type": "long"
                        },
                        "ver8725": {
                            "type": "long"
                        },
                        "vey0958": {
                            "type": "long"
                        },
                        "vgb5123": {
                            "type": "long"
                        },
                        "vgm4622": {
                            "type": "long"
                        },
                        "vhf6196": {
                            "type": "long"
                        },
                        "vhl6737": {
                            "type": "long"
                        },
                        "vhn0167": {
                            "type": "long"
                        },
                        "vidob01": {
                            "type": "long"
                        },
                        "vidsp01": {
                            "type": "long"
                        },
                        "vih2077": {
                            "type": "long"
                        },
                        "vij1402": {
                            "type": "long"
                        },
                        "vjj6296": {
                            "type": "long"
                        },
                        "vjr6075": {
                            "type": "long"
                        },
                        "vjv6718": {
                            "type": "long"
                        },
                        "vkd5198": {
                            "type": "long"
                        },
                        "vld2004": {
                            "type": "long"
                        },
                        "vlm4498": {
                            "type": "long"
                        },
                        "vls0809": {
                            "type": "long"
                        },
                        "vlu3254": {
                            "type": "long"
                        },
                        "vmh2318": {
                            "type": "long"
                        },
                        "vmp2271": {
                            "type": "long"
                        },
                        "vnx7075": {
                            "type": "long"
                        },
                        "vny4604": {
                            "type": "long"
                        },
                        "vpb0615": {
                            "type": "long"
                        },
                        "vpb5126": {
                            "type": "long"
                        },
                        "vplvce01": {
                            "type": "long"
                        },
                        "vplvne01": {
                            "type": "long"
                        },
                        "vplvom02": {
                            "type": "long"
                        },
                        "vplvpj06": {
                            "type": "long"
                        },
                        "vpm9138": {
                            "type": "long"
                        },
                        "vpn2604": {
                            "type": "long"
                        },
                        "vpv9248": {
                            "type": "long"
                        },
                        "vqh3820": {
                            "type": "long"
                        },
                        "vqj6966": {
                            "type": "long"
                        },
                        "vqm7965": {
                            "type": "long"
                        },
                        "vqr1027": {
                            "type": "long"
                        },
                        "vqs6424": {
                            "type": "long"
                        },
                        "vrh2627": {
                            "type": "long"
                        },
                        "vrovts01": {
                            "type": "long"
                        },
                        "vrsvcv01": {
                            "type": "long"
                        },
                        "vsd5703": {
                            "type": "long"
                        },
                        "vse3936": {
                            "type": "long"
                        },
                        "vss2528": {
                            "type": "long"
                        },
                        "vst0661": {
                            "type": "long"
                        },
                        "vtj5167": {
                            "type": "long"
                        },
                        "vtq3681": {
                            "type": "long"
                        },
                        "vuc4965": {
                            "type": "long"
                        },
                        "vud5575": {
                            "type": "long"
                        },
                        "vur3969": {
                            "type": "long"
                        },
                        "vuu0301": {
                            "type": "long"
                        },
                        "vux7817": {
                            "type": "long"
                        },
                        "vvv3555": {
                            "type": "long"
                        },
                        "vyc2859": {
                            "type": "long"
                        },
                        "vyd2219": {
                            "type": "long"
                        },
                        "vyn0828": {
                            "type": "long"
                        },
                        "vyy9403": {
                            "type": "long"
                        },
                        "vzd7698": {
                            "type": "long"
                        },
                        "vzy4571": {
                            "type": "long"
                        }
                    }
                },
                "riskDescription": {
                    "type": "keyword",
                    "null_value": ""
                },
                "salesManagers": localUnderwritersNested,
                "salesPartner": {
                    "type": "nested",
                    "dynamic": True,
                    "properties": {
                        "agentId": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": False,
                            "null_value": 0
                        },
                        "agentNumber": {
                            "type": "keyword",
                            "null_value": ""
                        },
                        "name": {
                            "type": "keyword",
                            "null_value": ""
                        }
                    }
                },
                "subjectiveAssessment": {
                    "type": "float"
                },
                "uicbConfirmNeeded": {
                    "type": "boolean",
                    "null_value": False
                },
                "updatedDate": {
                    "type": "date",
                    "format": "yyyy/MM/dd HH:mm:ss"
                }
            }
        }
    }
}
