var etl = require('etl');

var fs = require('fs');

var JSONStream = require('JSONStream');

var elasticsearch = require('elasticsearch');

//change with your data
var client = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'trace'
});

var readStream = fs.createReadStream('addresses.json'); //change with your filename

readStream
  .pipe(JSONStream.parse('*'))
  .pipe(etl.collect(50))
  .pipe(etl.elastic.index(client, 'hotspots_addresses', 'entry')); //testindex(your index)- testtype your es type
