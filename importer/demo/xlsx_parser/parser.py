# -*- coding: utf-8 -*-

import os
from openpyxl import load_workbook
from datetime import datetime
import logging

from csv_to_dict import csv_to_dict
from elastic.utils import pp, pp_ws, PRICE_REGEX, DATE_REGEX
from cbn_rest_fetch.connector import fetch


def get_full_path(filename, path='data/'):
    return os.path.join(os.getcwd(), path + filename)


def parse_sheet(sheet):
    json_base = []

    header = dict()
    for row in sheet.iter_rows(max_row=1):
        for index, item in enumerate(row):
            header[item.column] = item.value

    for row in sheet.iter_rows(min_row=2):
        json_row = dict()
        for cell in row:

            if isinstance(cell.value, datetime):
                cell.value = cell.value.strftime('%d.%m.%Y')

            if cell and cell.value:
                if isinstance(cell.value, (unicode, basestring)):
                    cell.value = u"" + cell.value
                json_row[header[cell.column]] = cell.value

        json_base.append(json_row)

    return json_base


def parse_csv_rows(rows):
    json_base = []

    header = dict()

    for index, item in enumerate(rows[0]):
        header[str(index)] = item.strip()

    pp_ws(header)

    for row in rows[1:]:
        json_row = dict()
        for index, cell in enumerate(row):
            index = str(index)

            if index not in header:
                pp_ws('%s not in %s' % (index, str(header.keys())))
                continue

            if DATE_REGEX.match(cell):
                # json_row[header[index]] = datetime.strptime(cell, "%d.%m.%y").isoformat()
                json_row[header[index]] = cell
                continue

            if PRICE_REGEX.match(cell):
                try:
                    json_row[header[index]] = float(
                        cell.replace('.', '').replace(',', '.'))
                except Exception:
                    logging.warn('failed to parse %s as price regex' % cell)
                    json_row[header[index]] = cell
                continue

            if index in header and len(header[index]) and len(index) > 0:
                json_row[header[index]] = cell
            else:
                continue

        json_base.append(json_row)

    return json_base


def parse_files(csv_only=None,
                file_names=None,
                rest_only=None,
                get_all=None,
                test=None,
                limit=10,
                start=datetime(year=2018, month=2, day=1),
                end=datetime(year=2018, month=2, day=10),
                index_name=None):
    files = []

    if not rest_only:
        for filename in os.listdir(
                os.path.join(os.getcwd(),
                             'data' if not csv_only else 'data/csv')):
            short_filename = filename
            filename = get_full_path(filename,
                                     'data/' if not csv_only else 'data/csv/')

            if file_names and short_filename not in file_names:
                continue

            if not csv_only:
                if not filename.endswith(
                        'xlsx'
                ) or 'CBN' in filename:  # parse only the 10k export
                    continue

                wb2 = load_workbook(filename,
                                    guess_types=False,
                                    data_only=False)
                sheets = [('List', wb2['List'])
                          ]  # , ('TopAccounts', wb2['Top Accounts'])]
                for sheet_tuple in sheets:
                    name, sheet = sheet_tuple
                    parsed_sheet = parse_sheet(sheet)
                    files.append(dict(name=name.lower(), data=parsed_sheet))
            else:
                if not filename.endswith('.csv'):
                    continue

                rows = csv_to_dict(filename)[:200]

                files.append(dict(name='List', data=parse_csv_rows(rows)))
    else:
        address_count, data = fetch(start,
                                    end,
                                    limit=limit,
                                    get_all=get_all,
                                    test=test,
                                    index_name=index_name)

        files.append(dict(name="from_rest", data=data))

    return address_count, files
