# -*- coding: utf-8 -*-


def csv_to_dict(path):
    with open(path, 'rb') as f:
        return [line.decode('utf-8').split(';') for line in f]
