# -*- coding: utf-8 -*-

import googlemaps
import os
import logging
from common.utils import read_file, write_file
import json

G_MAPS = googlemaps.Client(key=os.getenv('MAPS_API_KEY'))
COORDS_DATA_PATH = 'data/json/addresses_and_coordinates.json'


def geocode_address(address):
    address = address.replace('"', '\"')

    json_coords = read_file(COORDS_DATA_PATH)

    if isinstance(json_coords, dict) and address in json_coords.keys():
        return json_coords[address]

    try:
        try:
            encode = G_MAPS.geocode(address)
        except Exception:
            return '0,0'

        if len(encode) == 0:
            return '0,0'

        geocode_result = encode[0]

        lat_lng = geocode_result['geometry']['location']
        will_return = ",".join(
            format(x, ".4f") for x in [lat_lng['lat'], lat_lng['lng']])

        coords_before = None
        if json_coords:
            coords_before = json_coords
            json_coords[address] = will_return

        try:
            if json_coords:
                json_coords = json.loads(json.dumps(json_coords))
                write_file(COORDS_DATA_PATH, json_coords, as_json=True)

            # test if it can be read properly, i.e. no json error
            read = read_file(COORDS_DATA_PATH, as_json=True)
            if not read:
                raise Exception

        except Exception as error:
            if coords_before:
                write_file(COORDS_DATA_PATH, coords_before, as_json=True)

        return will_return

    except Exception:
        return None
