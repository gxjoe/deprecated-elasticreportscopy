-- first select only the columns you will need, 344 locations
WITH
    all_offers
    AS
    (
        SELECT DISTINCT cartodb_id,
            the_geom,
            offer_id
        FROM "uniqa-admin".gg_slim LIMIT
    
    
    
     4000) , -- find competing spielhallen, 262 locations
 spielhallen_near_each_other AS
(SELECT DISTINCT a.*
FROM all_offers a,
    all_offers b
WHERE ST_DWITHIN(a.the_geom::geography, b.the_geom::geography, 500)
    AND a.cartodb_id != b.cartodb_id )



SELECT a.cartodb_id,
    a.offer_id,
    ST_TRANSFORM(ST_BUFFER(a.the_geom::geography, 500)
::geometry, 4326) AS the_geom,
       ST_TRANSFORM
(ST_BUFFER
(a.the_geom::geography, 500)::geometry, 3857) AS the_geom_webmercator
FROM spielhallen_near_each_other a