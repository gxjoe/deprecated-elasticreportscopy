#!/bin/bash
dh_file=/home/docker/_docker_health.log
#echo "`date "+%F %T"` - running health check" &>> /home/docker/_docker_health.log

if [ `curl -s https://cbn.uniqa-international.eu/ | grep "cbn.uniqa-international.eu/cbnnotavailable" | wc -l` -eq 1 ];
then
  echo "Apache down" &>> $dh_file
  exit 0;
fi

if [ `docker ps -a | grep "elastic_reports" | egrep "Exited \(127|137|1\)" | wc -l` -ne 0 ];
then
  echo "Exited" &>> $dh_file
  #echo "---> uptime" &>> $dh_file
  #docker exec elastic_reports_elasticsearch_1 uptime &>> $dh_file
  #echo "---> tail -10 /opt/bitnami/elasticsearch/logs/gc.log" &>> $dh_file
  #docker exec elastic_reports_elasticsearch_1 tail -10 /opt/bitnami/elasticsearch/logs/gc.log &>> $dh_file
  sleep 5
  docker stats --no-stream > _stats.log
  docker logs --since "3h" elastic_reports_python_1 2>&1 >> /home/docker/_py.log
  docker logs --since "3h" elastic_reports_memcached_1 2>&1 >> /home/docker/_mc.log
  docker inspect elastic_reports_elasticsearch_1 2>&1 >> /home/docker/_inspect_es.log
  (docker ps -a \
  && systemctl restart elastic-reports \
  && echo "`date "+%F %T"` Restarted" \
  && echo "-----------------------------------------") &>> $dh_file
  exit 0;
fi

if [ `curl -s -m 20 https://er.uniqa-international.eu/es/ | grep "You Know, for Search" | wc -l` -ne 1 ];
then
  echo "`date "+%F %T"` app not responding" &>> $dh_file
#  sleep 5;
#  esUp=`docker exec elastic_reports_elasticsearch_1 curl -s localhost:9200/ | grep "You Know, for Search" | wc -l`
#  echo "Search ok: $esUp" &>> $dh_file
#  if [ $esUp -eq 1 ];
#  then
#    echo "ES running, restarting nginx"
#    docker logs --since "2h" elastic_reports_kibana_1 &>> /home/docker/_kib.log
#    docker-compose -f elastic_reports/docker-compose.yml restart -t 20 nginx
#  fi
  sleep 20
  if [ `curl -s -m 30 https://er.uniqa-international.eu/es/ | grep "You Know, for Search" | wc -l` -ne 1 ] && [ `docker ps | grep "elastic_reports" | grep "Up " | wc -l` -eq 5 ];
  then
    echo "ES down, logs writen" &>> $dh_file
    docker ps -a &>> $dh_file
#    echo "---> nginx status" &>> $dh_file
#    docker exec elastic_reports_nginx_1 curl -s 127.0.0.1:8080/status &>> $dh_file
    #echo "---> curl https://er.uniqa-international.eu/es/" &>> $dh_file
    #curl -s -m 10 https://er.uniqa-international.eu/es/ &>> $dh_file
    echo "---> curl https://er.uniqa-international.eu/co/" &>> $dh_file
    curl -s -m 10 https://er.uniqa-international.eu/co/ &>> $dh_file
    echo "---> curl localhost:9200/" &>> $dh_file
    docker exec elastic_reports_elasticsearch_1 curl -s -m 30 localhost:9200/ | grep "You Know, for Search" &>> $dh_file
    #echo "---> ls -la /opt/bitnami/elasticsearch/logs/" &>> $dh_file
    #docker exec elastic_reports_elasticsearch_1 ls -l /opt/bitnami/elasticsearch/logs/ &>> $dh_file
    #echo "---> ls -la /bitnami/elasticsearch/data" &>> $dh_file
    #docker exec elastic_reports_elasticsearch_1 ls -l /bitnami/elasticsearch/data &>> $dh_file
    #echo "---> uptime" &>> $dh_file
    #docker exec elastic_reports_elasticsearch_1 uptime &>> $dh_file
    #echo "---> tail -10 /opt/bitnami/elasticsearch/logs/gc.log" &>> $dh_file
    #docker exec elastic_reports_elasticsearch_1 tail -10 /opt/bitnami/elasticsearch/logs/gc.log &>> $dh_file

    docker logs --since "2h" elastic_reports_python_1 &>> /home/docker/_py.log
    docker logs --since "3h" elastic_reports_memcached_1 2>&1 >> /home/docker/_mc.log
    #docker logs --since "4h" elastic_reports_kibana_1 &>> /home/docker/_kib.log
    #docker stats --no-stream &>> $dh_file

    systemctl restart elastic-reports
    echo "`date "+%F %T"` Restarted" &>> $dh_file
    echo "-----------------------------------------" &>> $dh_file
  fi
fi
